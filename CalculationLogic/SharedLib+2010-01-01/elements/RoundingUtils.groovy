/**
 * A general function that rounds a number to N decimals
 * @param number
 * @param decimalPlaces
 * @return null if the price is null
 */
BigDecimal round(BigDecimal number, int decimalPlaces) {
    if (number == null) {
        return null
    }
    return number.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP)
}


/**
 * Rounds a price to N decimals
 * @param price
 * @param decimalPlaces
 * @return
 */
BigDecimal roundPrice(BigDecimal price, int decimalPlaces) {
    return round(price, decimalPlaces)
}


/**
 * Round a percentage to 2 decimals or upon based on a rounding lookup table name if provided.
 * @param price
 * @return null if the price is null
 */
BigDecimal roundPercent(BigDecimal price) {
    return round(price, 4)
}

/**
 * A general function that rounds up a number to N decimals
 * @param number
 * @param decimalPlaces
 * @return null if the price is null
 */
BigDecimal roundUp(BigDecimal number, int decimalPlaces) {
    if (number == null) {
        return null
    }
    return number.setScale(decimalPlaces, BigDecimal.ROUND_UP)
}