String sku = out.SKU

Map grossPriceDetails = [:]

if (out.IsTopcoProduct) {
    return ["GrossPrice": grossPriceDetails, "MaterialType": "TOPCO"]
} else {
    return getGrossPrice(sku)
}


def getGrossPrice(String sku) {
    int skuLength = sku.length()
    List grossPriceDetails = getSalesPrice(sku)

    if (!grossPriceDetails && skuLength == 11) {
        grossPriceDetails = getSalesPrice(sku.substring(0, 10))
    }

    if (grossPriceDetails) {
        return ["GrossPrice": grossPriceDetails, "MaterialType": "NORMAL"]
    }

    def compoundPrice = getCompoundPrice(sku)

    if (!compoundPrice && skuLength == 11) {
        compoundPrice = getCompoundPrice(sku.substring(0, 10))
    }

    if (compoundPrice) {
        return ["GrossPrice": compoundPrice, "MaterialType": "BOM"]
    }

    return ["GrossPrice": grossPriceDetails, "MaterialType": "N/A"]
}

List getSalesPrice(sku) {
    List grossPriceDetails = []
  
    def grossPriceDetailsRecords = SalesPriceRecord(sku)

    if (grossPriceDetailsRecords) {
      for (grossPriceDetailsRecord in grossPriceDetailsRecords){
          def row = [grossPrice	: grossPriceDetailsRecord.attribute3/100,
                     validFrom   	: grossPriceDetailsRecord.attribute1,
                     validTo   	: grossPriceDetailsRecord.attribute2
                    ]
          grossPriceDetails.add(row)
        }
      }
api.trace("getSalesPrice -- grossPriceDetails",grossPriceDetails)
    return grossPriceDetails
}

List getCompoundPrice(sku) {
  
  def grossPriceDetails = []
    List grossPriceDetailsRecords = compoundPriceRecord(sku)
  
    if (grossPriceDetailsRecords){
      for (grossPriceDetailsRecord in grossPriceDetailsRecords){
        def row = [grossPrice		: grossPriceDetailsRecord.attribute1/100,
                   validFrom   	: grossPriceDetailsRecord.attribute6,
                   validTo   		: grossPriceDetailsRecord.attribute7
                  ]
        grossPriceDetails.add(row)
      }
    }

    return grossPriceDetails
}

List compoundPriceRecord(sku) {
    def filters = [
            Filter.equal("name", "CompoundPrice"),
            Filter.equal("sku", sku),
      		Filter.isNotNull("attribute1")
    ]

    def orderBy = "attribute6" // Valid From - descending 
    def result = api.find("PX10", 0, api.getMaxFindResultsLimit(), orderBy, *filters)
    
    return result
}

List SalesPriceRecord(sku) {
    def filters = [
            Filter.equal("name", "SalesPrice"),
            Filter.equal("sku", sku),
      		Filter.isNotNull("attribute3")
    ]

    def orderBy = "attribute1" // Valid From - descending 
    def result = api.find("PX10", 0, api.getMaxFindResultsLimit(), orderBy, *filters)
    
    return result
}