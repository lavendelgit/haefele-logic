import groovy.transform.Field

//@Field String DM_SIMULATION = 'PriceChangeSimulation'
@Field String DM_SIMULATION = 'LpgSimulation'

@Field Map DM_COLUMNS = [
        MATERIAL           : 'Material',
        PRICING_TYPE       : 'PricingType',
        YEAR               : 'Year',
        CUSTOMER_ID        : 'CustomerId'
]