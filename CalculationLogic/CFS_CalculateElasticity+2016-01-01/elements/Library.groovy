Number getQuantitySold (String sku, validFrom, validTo)
{
    List transactionData
  //api.trace("sku",sku)
  def excludedCustomerIds = out.NetPriceCustomerArticleCombinations?.findAll { it-> it.Material == sku}?.CustomerId
  //api.trace("excludedCustomerIds",excludedCustomerIds)
  
    //get quantity sold from transaction datamart exculding the excludedArticleAndCustomerIdCombination
    Map fields = ['COUNTDISTINCT(InvoiceDateMonth)'         : 'YearMonth',
                  'Material'                        : 'SKU',
                  'SUM(InvoiceQuantity)'            : 'quantity',
                  'SUM(InvoiceQuantity)/COUNTDISTINCT(InvoiceDateMonth)'  : 'avgMonthlyQty'
    ]

    List filters = [Filter.equal("Material", sku),
                    Filter.lessOrEqual("InvoiceDate", validTo),
                    Filter.greaterOrEqual("InvoiceDate", validFrom)]

  if (excludedCustomerIds)
  	filters.add(Filter.notIn("CustomerId", excludedCustomerIds))
  
  api.trace(" **************** filters",filters)
  
    transactionData = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')?.collect()
    api.trace(" **************** transactionData",transactionData)
    return transactionData?transactionData.avgMonthlyQty[0]:null

}

def getNetPriceCustomerArticleCombinations()
{
   Map fields = ['Material'         : 'Material',
                  'CustomerId'      : 'CustomerId'
    ]
//return Constants.DM_COLUMNS
  
    List filters = [Filter.equal(Constants.DM_COLUMNS.PRICING_TYPE,"Net Price")]
  
    def result = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'LpgSimulation', filters, 'DataMart')?.collect()
  
api.trace("result",result)
    return result

}

//Date.parse ("yyyy-MM-dd", transactionDate)