String sku = out.SKU
Map grossPriceDetails = out.GrossPriceDetails
api.local.productElasticity = 0

List productElasticRows = []

if (grossPriceDetails.MaterialType == "BOM" || grossPriceDetails.MaterialType == "NORMAL") {
  	List grossPriceRecords = grossPriceDetails.GrossPrice
    BigDecimal previousGrossPrice
    BigDecimal currentGrossPrice
    BigDecimal previousAvgMonthlyQty
    BigDecimal currentAvgMonthlyQty
    String priceChangeType
    String qtyChangeType

    grossPriceRecords.each { grossPriceRecord ->
      api.trace("grossPriceRecord",grossPriceRecord)
        previousGrossPrice = (previousGrossPrice)?:grossPriceRecord.grossPrice
        currentGrossPrice = grossPriceRecord.grossPrice

        currentAvgMonthlyQty = Library.getQuantitySold(sku, grossPriceRecord.validFrom, grossPriceRecord.validTo)
        previousAvgMonthlyQty = previousAvgMonthlyQty?:currentAvgMonthlyQty

        def row = [
                "sku"           		: out.SKU
                , "grossPrice"         	: currentGrossPrice
                , "validFrom"       	: grossPriceRecord.validFrom
                , "validTo"       		: grossPriceRecord.validTo
                , "avgMonthlyQty"       : currentAvgMonthlyQty
                , "perChangePrice" 	    : 0
                , "perChangeQty"        : 0
                , "priceElasticity" 	: 0
                ,"priceChangeType"       :""
                ,"qtyChangeType"        :""
                ,"elasticityStatus"     :""
        ]

        priceChangeType     = (previousGrossPrice == currentGrossPrice)?"Same":(previousGrossPrice < currentGrossPrice)?"Increase":"Decrease"
        if (previousAvgMonthlyQty && currentAvgMonthlyQty) {
            qtyChangeType = (previousAvgMonthlyQty == currentAvgMonthlyQty) ? "Same" : (previousAvgMonthlyQty < currentAvgMonthlyQty) ? "Increase" : "Decrease"
        } else {
            qtyChangeType = previousAvgMonthlyQty?"Decrease":"Increase"
        }
        row["elasticityStatus"] = getElasticityStatus(priceChangeType,qtyChangeType)
        BigDecimal perChangeInPrice
        BigDecimal perChangeInQuantity

        if (previousGrossPrice && currentGrossPrice && previousGrossPrice != currentGrossPrice) {
            perChangeInPrice     = Math.abs((currentGrossPrice - previousGrossPrice)/previousGrossPrice)
            row["perChangePrice"]   = perChangeInPrice
            row["priceChangeType"]  = priceChangeType
            previousGrossPrice = currentGrossPrice
        }

        if (currentAvgMonthlyQty &&  previousAvgMonthlyQty && currentAvgMonthlyQty != previousAvgMonthlyQty )
            {
                perChangeInQuantity = Math.abs((currentAvgMonthlyQty - previousAvgMonthlyQty) / previousAvgMonthlyQty)
                row["perChangeQty"]     = perChangeInQuantity
                row["qtyChangeType"]    = qtyChangeType
                previousAvgMonthlyQty = currentAvgMonthlyQty
            }

        if (perChangeInPrice && perChangeInQuantity) {
            BigDecimal elasticity   = Math.abs(perChangeInQuantity/perChangeInPrice)
            row["priceElasticity"] = elasticity
        }

        productElasticRows << row
    }

  List priceChangeValues = productElasticRows?.findAll{ Map it-> it.perChangePrice != 0}
  List quantityChangeValues = productElasticRows?.findAll{ Map it-> it.perChangeQty != 0}

  BigDecimal avgPriceChange = priceChangeValues?(priceChangeValues.perChangePrice.sum()/priceChangeValues.size()):null
  BigDecimal avgQuantityChange = quantityChangeValues?(quantityChangeValues.perChangeQty.sum()/quantityChangeValues.size()):null

  if (avgPriceChange && avgQuantityChange) {
      BigDecimal avgElasticity = avgQuantityChange/avgPriceChange
      api.local.productElasticity = avgElasticity
  }
    return productElasticRows
}

def getElasticityStatus(priceChangeType,qtyChangeType)
{
    if ((priceChangeType == "Increase" && qtyChangeType == "Decrease")  || (priceChangeType == "Decrease" && qtyChangeType == "Increase")){
        return "Elastic"
    }

    if ((priceChangeType == "Increase" && qtyChangeType == "Increase")  || (priceChangeType == "Decrease" && qtyChangeType == "Decrease")){
        return "Non Elastic"
    }

    if ((priceChangeType == "Same" && qtyChangeType == "Increase")  || (priceChangeType == "Same" && qtyChangeType == "Decrease")){
        return "Non Elastic"
    }

    if ((priceChangeType == "Increase" && qtyChangeType == "Same")  || (priceChangeType == "Decrease" && qtyChangeType == "Same")){
        return "Non Elastic"
    }

}