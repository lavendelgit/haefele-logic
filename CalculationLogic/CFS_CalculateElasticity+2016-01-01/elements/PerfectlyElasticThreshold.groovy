
if (!api.global.perfectlyElastic) {
    def perfectlyElastic = lib.GeneralSettings.decimalValue("PerfectlyElasticThreshold")

    if (perfectlyElastic == null) {
        api.redAlert("PerfectlyElasticThreshold is missing")
        return null;
    }

    api.global.perfectlyElastic = perfectlyElastic
}

return api.global.perfectlyElastic
