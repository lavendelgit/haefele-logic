List elasticityDetails = out.GetElasticityDetails

elasticityDetails?.forEach { elasticityRecord ->
    //api.trace("elasticityRecord","",elasticityRecord)
    api.addOrUpdate("PX", [
            "name": "ProductElasticityDetails",
            "sku": elasticityRecord.sku,
            "attribute1": elasticityRecord.grossPrice,
            "attribute2": elasticityRecord.validFrom,
            "attribute3": elasticityRecord.validTo,
            "attribute4": elasticityRecord.avgMonthlyQty,
            "attribute5": elasticityRecord.perChangePrice,
            "attribute6": elasticityRecord.perChangeQty,
            "attribute7": elasticityRecord.priceElasticity,
            "attribute8": elasticityRecord.priceChangeType,
            "attribute9": elasticityRecord.qtyChangeType,
            "attribute10": elasticityRecord.elasticityStatus
    ])
}