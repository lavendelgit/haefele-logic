def data = api.getElement("PerMonthData")

def categories = data.collect { d -> d.yearMonth } .takeRight(12)
def values = categories.collect { c -> data.find { d -> d.yearMonth == c } ?.count}
def diff = values.withIndex().collect { value, i ->
	def prevValue = values[i-1]
	return i > 0 ? (value - prevValue)/prevValue * 100 : [:]
}


api.trace("categories", "", categories)
api.trace("values", "", values)
api.trace("diff", "", diff)

def chartDef = [
		chart: [
				type: 'column',
				zoomType: 'x'
		],
		title: [
				text: ''
		],
		xAxis: [[
						title: [
								text: "Year-Month"
						],
						categories: categories
				]],
		yAxis: [[
						title: [
								text: "Number of CRM Projects"
						]
				], [
						title: [
								text: "Difference"
						],
						opposite: true
				]],
		series: [[
						 name: "Number of CRM Projects",
						 data: values,
						 dataLabels: [
								 enabled: true,
								 format: "{point.y:,.0f}"
						 ],
						 tooltip: [
								 headerFormat: '<b>{series.name}</b><br>{point.key}: ',
								 pointFormat: '{point.y:,.0f}'
						 ]
				 ],[
						name: "Difference",
						data: diff,
						type: "line",
						yAxis: 1,
						dataLabels: [
								enabled: true,
								format: "{point.y:,.2f}%"
						],
						tooltip: [
								headerFormat: '<b>{series.name}</b><br>{point.key}: ',
								pointFormat: '{point.y:,.2f}%'
						]
				]]
]

api.buildFlexChart(chartDef)