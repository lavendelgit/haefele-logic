def dmCtx = api.getDatamartContext()
def datamartQuery = dmCtx.newQuery(dmCtx.getTable("CRMProjectsDM"))

datamartQuery.select("YearMonth", "yearMonth")
datamartQuery.select("SalesOffice", "salesOffice")
datamartQuery.select("COUNT(Material)", "count")
datamartQuery.orderBy("yearMonth asc")

if (api.isUserInGroup("ProductGroupManager", api.user().loginName)) {
    def pgm = api.findLookupTableValues("ProductGroupManager")?.find()?.value
    datamartQuery.where(Filter.equal("SM", pgm))
}

def result = dmCtx.executeQuery(datamartQuery)

if (result == null) {
    api.addWarning("Could not get result for Sales query.")
}

api.trace("result.summary", "", result?.getSummary())

result?.getData()?.collect()?.groupBy { r -> r.salesOffice }