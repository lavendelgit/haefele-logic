def create(type) {
    def stacking = [
            grouped: null,
            stacked: "normal",
            percent: "percent"
    ]
    def dataLabelsFormat = [
            grouped: "{point.y:,.0f}",
            stacked: "{point.y:,.0f}",
            percent: "{point.percentage:.0f}%"
    ]
    def tooltipPointFormat = [
            grouped: 'Month: {point.name} <br/>Number of CRM Projects: {point.y}',
            stacked: 'Month: {point.name} <br/>Number of CRM Projects: {point.y}',
            percent: 'Month: {point.name} <br/>Number of CRM Projects: {point.y} ({point.percentage:.0f}%)'

    ]


    def groupedRecords = api.getElement("PerMonthPerSalesOfficeData")

    def categories = groupedRecords.values()*.yearMonth.flatten().toSet().toSorted().takeRight(12)
    def series = groupedRecords
            .collect { salesOffice, records -> [
                    name: salesOffice,
                    data: categories.collect { c -> [
                            name: c,
                            y: records.find {  r -> r.yearMonth == c } ?.count
                    ]},
                    dataLabels: [
                            enabled: true,
                            format: dataLabelsFormat[type]
                    ]
            ]}


    api.trace("categories", "", categories)

    def chartDef = [
            chart: [
                    type: "column",
                    zoomType: "x"
            ],
            title: [
                    text: ""
            ],
            xAxis: [[
                            title: [
                                    text: "Sales Office"
                            ],
                            categories: categories
                    ]],
            yAxis: [[
                            title: [
                                    text: "Number of CRM Project"
                            ]
                    ]],
            plotOptions: [
                    column: [
                            stacking: stacking[type]
                    ]
            ],
            tooltip: [
                    headerFormat: 'Sales Office: {series.name}<br/>',
                    pointFormat: tooltipPointFormat[type]
            ],
            legend: [
                    enabled: true,
                    layout: "vertical",
                    align: "right",
                    verticalAlign: "middle"
            ],
            series: series
    ]

    api.buildFlexChart(chartDef)
}