feedObjects()

def feedObjects() {
    def customers = getAllCustomersToProcess()
    def curCustomer
    customers.each { customerId ->
        curCustomer = api.customer("id", customerId) as Long
        api.emitPersistedObject("C", curCustomer)
    }
}

def getAllCustomersToProcess() {
    def customersToProcess = []
    def ctx = api.getDatamartContext()
    def dataSource = ctx.getRollup("DL_CustomersWithTransactions")
    def query = ctx.newQuery(dataSource)
    query.select("CustomerId", "CustomerId")
    query.select("COUNT(InvoiceDateYear)", "NoOfYears")
    def results
    try {
        results = ctx.streamQuery(query)
        while (results?.next()) {
            def row = results.get()  // current row as map
            lib.TraceUtility.developmentTraceRow("streamQuery", row)
            customersToProcess.add(row["CustomerId"])
        }
    }
    finally {
        results?.close()
    }
    return customersToProcess
}