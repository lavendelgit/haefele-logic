def baseCost = api.getElement("BaseCostPer100Units") as BigDecimal
def units = api.getElement("Units") as BigDecimal

if (baseCost != null && units != null) {
    return (baseCost * units / 100.0).setScale(2, BigDecimal.ROUND_HALF_UP)
}