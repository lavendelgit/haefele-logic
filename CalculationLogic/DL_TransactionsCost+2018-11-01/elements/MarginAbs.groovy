def revenue = api.getElement("Revenue") as BigDecimal
def cost = api.getElement("Cost") as BigDecimal

if (revenue != null && cost != null) {
    return (revenue - cost)
}