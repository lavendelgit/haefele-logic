def sku = api.getElement("ArticleNumber")
def invoiceDate = api.getElement("InvoiceDate")


lib.Find.latestBaseCostRecord(sku,
    Filter.lessOrEqual("attribute1", invoiceDate),
    Filter.greaterOrEqual("attribute2", invoiceDate)
)?.attribute3
