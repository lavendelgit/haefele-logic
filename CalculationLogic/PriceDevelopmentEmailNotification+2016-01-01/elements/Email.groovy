def isTest = api.getElement("IsTestMode")
def pgList = api.findPriceGrids(Date.newInstance(), "Price Development")
if (!pgList || pgList.size() == 0) {
  api.logWarn("The Price Grid object could not be obtained. Check if the LPG with name: Price Development exists or changed")
  api.abortCalculation () 
}

api.trace("pg",pgList)
def priceDevPG = pgList[0]
def actualPGLastRunDate = priceDevPG?.calculationDate
def lookupTableId = api.findLookupTable("PriceDevelopmentNotifier")?.id
final String secondColNm = "LastNotifiedDate"
def filters = [
  Filter.equal("lookupTable.id", lookupTableId),
  Filter.equal("name",secondColNm),
  Filter.greaterOrEqual("attribute1",actualPGLastRunDate)
]
def priceDevelopmentNotifierDetails = api.find("MLTV", *filters)
api.trace("latestStartDate",actualPGLastRunDate + " Status " + priceDevPG?.status)
api.trace("lastprocess",priceDevelopmentNotifierDetails)
if ((!priceDevelopmentNotifierDetails || priceDevelopmentNotifierDetails.size() ==0) && priceDevPG?.status == "READY") {
	api.trace ("PG Run Details", "No notification was released since the last notification.....")
  	def typedId = priceDevPG?.typedId
	//Get the email ids of the users to be notified... 
  	def usersToBeNotified = api.find("U", Filter.isNotEmpty("additionalInfo3"))
	def server = api.vLookup("SurchargeConfig","Server")
	api.trace ("Users to be notified", "List of users to be notified....." + usersToBeNotified)
  
	def noReply = "<i>Bitte antworten Sie nicht auf diese automatisch generierte E-Mail.</i>"
  	def url = "https://$server/classic/priceFxWeb.html?targetPage=priceGridPage&targetPageState="+typedId
  	def link = "<a href=$url>LPG anzeigen</a>"
  	def message = "der monatliche LPG Preisentwicklungslauf ist abgeschlossen. Sie können sich die Entwicklung über den Link anschauen."
  	def userName = ""
  	def emailTemplate = ""
    def tempToBeRemoved = 0
  	def salutation = ""
 	usersToBeNotified?.each {
      	userName = it?.lastName
      	userName = userName?:""
      	salutation = (it?.additionalInfo4 == "Male")? "geehrter Herr": "geehrte Frau"
	    emailTemplate = "Sehr $salutation $userName,<br><br>"+message+"<br><br>$link<br><br><br>$noReply<br><br>Freundlichen Gruß<br>Preismanagement"
   		api.trace(it?.email, "Price development LPG monthly run notification."+ emailTemplate)
	    if (isTest) {
	    		api.trace("sending..............email....." + it?.additionalInfo4, emailTemplate)
              	if (tempToBeRemoved == 0) {
	   					api.sendEmail("vivek.thakare@pricefx.com", "Preisentwicklung LPG monatliche Benachrichtigung.", emailTemplate)
						tempToBeRemoved = 1
              	}
        } else 	{
          			api.sendEmail(it?.email, "Preisentwicklung LPG monatliche Benachrichtigung.", emailTemplate)
        			tempToBeRemoved = 1
        }    
   	}
  	if (tempToBeRemoved ==1) {
    	api.sendEmail("vivek.thakare@pricefx.com", "Preisentwicklung LPG monatliche Benachrichtigung.", emailTemplate)
	}
	def entry = [
    	lookupTableId : lookupTableId,
    	name : secondColNm,
    	attribute1 : actualPGLastRunDate
  	]
  	api.addOrUpdate("MLTV", entry)
}

