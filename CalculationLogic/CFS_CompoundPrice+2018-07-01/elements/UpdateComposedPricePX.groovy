package CFS_CompoundPrice.elements

def sku = api.getElement("SKU")
def bomList = api.getElement("BomList")
def componentsPrices = api.getElement("ComponentPrices")

if (api.isSyntaxCheck()) {
    return
}

if (!componentsPrices) {
    return "It's not a composed product."
}

def validFromPoints = componentsPrices.values()
        .collectMany { prices -> prices.validFrom }
        .toSet()
        .sort()

def validToPoints = componentsPrices.values()
        .collectMany { prices -> prices.validTo }
        .toSet()
        .sort()

api.trace("validFromPoints", "", validFromPoints)
api.trace("validToPoints", "", validToPoints)

def minusOneDay(dateStr) {
    if (!dateStr) {
        return
    }
    def dateFormat = "yyyy-MM-dd"
    def date = api.parseDate(dateFormat, dateStr)
    def cal = api.calendar(date)
    cal.add(Calendar.DATE, -1)
    return cal.getTime().format(dateFormat)
}

for (i = 0; i < validFromPoints.size(); i++) {
    def validFrom = validFromPoints[i]
    def validTo = minusOneDay(validFromPoints[i+1]) ?: validToPoints.last()

    def resultPrice = 0.0
    def missingComponentPrices = []
    def warning = ""

    for (component in bomList) {
        def componentSku = component.label
        def priceUnits = component.additionalInfo2
        if (!priceUnits) {
            continue;
        }
        def componentPrice = componentsPrices[componentSku].find {
            c -> c.validFrom <= validFrom && c.validTo >= validFrom
        }
        if (!componentPrice?.salesPrice) {
            missingComponentPrices.add(componentSku)
        } else {
            api.trace("", componentSku, component.quantity * componentPrice.salesPrice)
            resultPrice += component.quantity * componentPrice.salesPrice
        }
    }

    api.trace("missingComponentPrices", "validFrom: ${validFrom}, validTo: ${validTo}", missingComponentPrices)

    if (!missingComponentPrices.isEmpty()) {
        resultPrice = null
        warning += "Missing price for: ${missingComponentPrices.join(", ")}"
    }

    def compoundPriceRecord = [
            "name": "CompoundPrice",
            "sku": sku,
            "attribute1": resultPrice,
            "attribute2": "EUR",
            "attribute3": warning,
            "attribute4": api.getElement("SupplierId"),
            "attribute5": api.getElement("SupplierName"),
            "attribute6": validFrom,
            "attribute7": validTo
    ]

    api.trace("compoundPriceRecord",compoundPriceRecord)

    api.addOrUpdate("PX10", compoundPriceRecord)
}

