def bomList = api.bomList() ?: []

api.trace("bomList", "", bomList)

def suppliers = api.find("P", 0, bomList.size(), "attribute8", ["attribute8", "attribute9"], true, Filter.in("sku", bomList.label))

