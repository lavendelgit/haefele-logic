def bomList = api.getElement("BomList")

if (bomList == null || bomList.isEmpty()) {
    return
}

def allSalesPrices(sku, currency) {
    def salesPrices = api.find("PX50", 0, 0, "attribute1",
            Filter.equal("name", "S_ZPLP"), Filter.equal("sku", sku), Filter.equal("attribute7", currency))
        ?.collect { r -> [
            type: "ZPLP",
            validFrom: r.attribute1,
            validTo: r.attribute2,
            salesPrice: r.attribute3,
            currency: r.attribute7,
            priceUnits: r.attribute9
        ]} ?: []

    def netPrices = api.find("PX50", 0, 0, "attribute1",
            Filter.equal("name", "S_ZPZ"), Filter.equal("sku", sku), Filter.equal("attribute9", currency))
        ?.collect { r -> [
                type: "ZPZ",
                validFrom: r.attribute1,
                validTo: r.attribute2,
                salesPrice: r.attribute3,
                currency: r.attribute9,
                priceUnits: r.attribute11
        ]} ?: []

    def compoundPrices = api.find("PX", 0, 0, "attribute6",
            Filter.equal("name", "CompoundPrice"), Filter.equal("sku", sku), Filter.equal("attribute2", currency))
        .collect { r -> [
                type: "ZPHT",
                validFrom: r.attribute6,
                validTo: r.attribute7,
                salesPrice: r.attribute1,
                currency: r.attribute2,
                priceUnits: r.attribute6
        ]} ?: []
api.trace("1",salesPrices)
    api.trace("2",netPrices)
    api.trace("3",compoundPrices)
    return (salesPrices + netPrices + compoundPrices).toSorted { a, b -> b.validFrom <=> a.validFrom }
}

def componentsPrices = bomList?.collectEntries { r ->
    def componentSku = r.label;
    def componentPrices = allSalesPrices(componentSku, "EUR")
    return [componentSku, componentPrices]
}

return componentsPrices
