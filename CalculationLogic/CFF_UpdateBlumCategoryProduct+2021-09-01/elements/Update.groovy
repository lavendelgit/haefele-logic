def filters = [
        Filter.equal("name","BlumArtikel"),
        Filter.like("attribute9","blum")
]


List blumRecords = api.find("MLTV", 0,*filters)

blumRecords?.each {it ->
    Map map = [:]
    map.sku = it.sku
    map.category = it.attribute9
    api.addOrUpdate("P",map)
}

/*
5083

4000
5083-4000

.each and .forEach

*/
  