import net.pricefx.common.api.FieldFormatType

String column1 = "Update Frequency"
String column2 = "Start Date"
String column3 = "End Date"
String column4 = "Total Configurations"
String column5 = "Configurations Not Updated"
String column6 = "Configurations Possibly Not Completely Updated"
String column7 = "Up-to-Date Configurations"

List columns = [column1, column2, column3, column4, column5, column6, column7]
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.DATE,
                   FieldFormatType.DATE, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER]

def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Import Configurations Summary', columns, columnTypes)
List updateFrequency = ['Weekly', 'Monthly']
String endDate = out.AutomatedDeliveryDate.format("yyyy-MM-dd")
String monthStartDate = libs.HaefeleCommon.ConfigurationMonitor.getTheStartDateForMonthFilter(out.AutomatedDeliveryDate).format("yyyy-MM-dd")
String weekStartDate = libs.HaefeleCommon.ConfigurationMonitor.getTheStartDateForWeekFilter(out.AutomatedDeliveryDate).format("yyyy-MM-dd")
List weeklyCounts = api.local.frequencyCount.get(updateFrequency[0]).collect { type, count -> return count }
int weeklyTotal = weeklyCounts.sum()
List monthlyCounts = api.local.frequencyCount.get(updateFrequency[1]).collect { type, count -> return count }
int monthlyTotal = monthlyCounts.sum()
resultMatrix.addRow([(column1): updateFrequency[0],
                     (column2): weekStartDate,
                     (column3): endDate,
                     (column4): weeklyTotal,
                     (column5): weeklyCounts[0],
                     (column6): weeklyCounts[1],
                     (column7): weeklyCounts[2]])
resultMatrix.addRow([(column1): updateFrequency[1],
                     (column2): monthStartDate,
                     (column3): endDate,
                     (column4): monthlyTotal,
                     (column5): monthlyCounts[0],
                     (column6): monthlyCounts[1],
                     (column7): monthlyCounts[2]])

return resultMatrix


