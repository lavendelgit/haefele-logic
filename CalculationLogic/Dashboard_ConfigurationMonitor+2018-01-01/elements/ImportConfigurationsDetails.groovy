import net.pricefx.common.api.FieldFormatType

String column1 = "Configuration"
String column2 = "Minimum Expected Records Count"
String column3 = "Received Records Count"
String column4 = "Last Updated Date"
String column5 = "Update Frequency"
String column6 = "Status"
List columns = [column1, column2, column3, column4, column5, column6]
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.DATE,
                   FieldFormatType.TEXT, FieldFormatType.TEXT]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Import Configurations Detail', columns, columnTypes)
Map configurationLabels = out.MonitorConfiguration?.collectEntries { currentRow -> [(currentRow.name): currentRow.attribute3] }
Map configurationCounts = out.CountOfRecordsReceivedOnOrAfterReferenceDate
def updateFrequency = out.UpdateFrequency
BigDecimal status, minimumCount
api.local.frequencyCount = [Monthly: [redcount: 0, yellowcount: 0, greencount: 0],
                            Weekly : [redcount: 0, yellowcount: 0, greencount: 0]]
for (configurationEntity in out.TableName) {
    status = configurationCounts[configurationEntity].Count
    minimumCount = out.Minimumcount[configurationEntity]
    resultMatrix.addRow([(column1): configurationLabels[configurationEntity],
                         (column2): minimumCount,
                         (column3): configurationCounts[configurationEntity].Count,
                         (column4): configurationCounts[configurationEntity].LastUpdateDate?.toString()?.substring(0, 10),
                         (column5): updateFrequency[configurationEntity],
                         (column6): status == 0 ?
                                    resultMatrix.libraryImage("Traffic", "red") :
                                    (status < minimumCount ? resultMatrix.libraryImage("Traffic", "yellow")
                                                           : resultMatrix.libraryImage("Traffic", "green"))
    ])

    def type = updateFrequency[configurationEntity].toString().indexOf("Monthly") > -1 ? "Monthly" : "Weekly"
    if (status == 0) {
        api.local.frequencyCount[type].redcount++
    } else if (status < minimumCount) {
        api.local.frequencyCount[type].yellowcount++
    } else {
        api.local.frequencyCount[type].greencount++
    }
}


resultMatrix.setPreferenceName("Configuration records count received on or after reference date")
return resultMatrix

