/**
 * This class contains the algorithm and which gets the data from transaction. It aggregates two types of data i.e. one at customer summary level and another detail level data
 * to be processed to record impact of wrong categorization in the different level.
 */
def fetchDataFromTransaction(ctx) {
    def dsCCIoM = ctx.getRollup("CCTransactionImpact")
    def detailCCDQuery = ctx.newQuery(dsCCIoM)
    CCTransactionDataComputationUtil.constructQueryForGettingCCDTransRec(detailCCDQuery)

    def detailNonCCDQuery = ctx.newQuery(dsCCIoM)
    CCTransactionDataComputationUtil.constructQueryForGettingNonCCDTransRec(detailNonCCDQuery)

    def summaryQuery = ctx.newQuery(dsCCIoM)
    CCTransactionDataComputationUtil.constructQueryForGettingTransRecSummary(summaryQuery)
    def joinedSQL = CCTransactionDataComputationUtil.getInnerJoinForSummaryNDetailTransRec()

    return ctx.executeSqlQuery(joinedSQL, detailCCDQuery, detailNonCCDQuery, summaryQuery)
}

def printFewRecords(records) {
    int i = 0
    records?.each { it ->
        if (it.t1cid != null && it.t2cid !=null && it.t1cid == it.t2cid) {
            if (++i < 5) {
                api.trace("record " + i, " " + it)
            }
        }
    }
}

def ctx = api.getDatamartContext()
def ccdTransactionSummaryData = fetchDataFromTransaction(ctx)
api.trace("data found", " (" + (ccdTransactionSummaryData?.size()) + ")" )
CCOutputComputation.populateCustomerCategoryRecords(ccdTransactionSummaryData)
printFewRecords(ccdTransactionSummaryData)

return true