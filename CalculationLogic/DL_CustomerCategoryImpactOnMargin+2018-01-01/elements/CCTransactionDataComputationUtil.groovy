def constructQueryForGettingCCDTransRec(query) {
    query.select("CustomerId")
    query.select("InvoiceDateYear")
    query.select("ProdhII", "ProductGroup")
    query.select("Verkaufsbereich3")
    query.select("CustomerPotentialGroup")
    query.select("SUM(p1_Revenue)", "Revenue")
    query.select("SUM(p2_Units)", "Units")
    query.select("SUM(p3_Material)", "ArticleCount")
    query.select("SUM(p4_MarginAbs)", "Margin")
    query.select("SUM(p5_Material)", "TransactionCount")
    def filters = [Filter.in("SalesType", api.local.salesTypeCriteria),
                   Filter.in ("CustomerId", ["0001000008", "0001000003", "0001000021", "0001000027","0001000035" ])]
    query.where(*filters)
    query.orderBy("CustomerId")
}

def constructQueryForGettingNonCCDTransRec(query) {
    query.select("CustomerId")
    query.select("InvoiceDateYear", "InvoiceDateYear1")
    query.select("ProdhII", "ProductGroup1")
    query.select("Verkaufsbereich3", "Verkaufsbereich31")
    query.select("CustomerPotentialGroup", "CustomerPotentialGroup1")
    query.select("SUM(p1_Revenue)", "Revenue1")
    query.select("SUM(p2_Units)", "Units1")
    query.select("SUM(p3_Material)", "ArticleCount1")
    query.select("SUM(p4_MarginAbs)", "Margin1")
    query.select("SUM(p5_Material)", "TransactionCount1")
    def filters = [
            Filter.notIn("SalesType", api.local.salesTypeCriteria),
            Filter.in ("CustomerId", ["0001000008", "0001000003", "0001000021", "0001000027","0001000035" ])
    ]
    query.where(*filters)
    query.orderBy("CustomerId")
}

def constructQueryForGettingTransRecSummary(summaryQuery) {
    summaryQuery.select("CustomerId")
    summaryQuery.select("InvoiceDateYear")
    summaryQuery.select("SUM(p1_Revenue)", "TotalRevenue")
    summaryQuery.select("SUM(p2_Units)", "TotalUnits")
    summaryQuery.select("SUM(p3_Material)", "AllArticleCount")
    summaryQuery.select("SUM(p4_MarginAbs)", "TotalMargin")
    summaryQuery.select("SUM(p5_Material)", "TotalTransactionCount")
    def filters = [
            Filter.in ("CustomerId", ["0001000008", "0001000003", "0001000021", "0001000027","0001000035" ])
    ]
    summaryQuery.where(*filters)
    summaryQuery.orderBy("CustomerId")
}

def getInnerJoinForSummaryNDetailTransRec() {
    String sqlStatement = "SELECT " +
            "T1.customerId as t1cid, T1.invoiceDateYear as t1idy, T1.ProductGroup as t1pg, T1.Verkaufsbereich3 as t1so, T1.CustomerPotentialGroup as t1cpg, " +
            "T1.Revenue as t1r, T1.Units as t1u, T1.ArticleCount as t1ac, T1.Margin as t1mar, T1.TransactionCount as t1tc, " +
            "T2.customerId as t2cid, T2.invoiceDateYear1 as t2idy, T2.ProductGroup1 as t2pg, T2.Verkaufsbereich31 as t2so, T2.CustomerPotentialGroup1 as t2cpg, " +
            "T2.Revenue1 as t2r, T2.Units1 as t2u, T2.ArticleCount1 as t2ac, T2.Margin1 as t2mar, T2.TransactionCount1 as t2tc, " +
            "T3.customerId as t3ci, T3.InvoiceDateYear as t3idy, T3.TotalRevenue as t3tr, T3.TotalUnits as t3tu, T3.AllArticleCount as t3aac, " +
            "T3.TotalMargin as t3tmar, T3.TotalTransactionCount as t3ttc " +
            "FROM T1 " +
            "LEFT OUTER JOIN T2 ON T1.customerId = T2.customerId AND T1.InvoiceDateYear = T2.InvoiceDateYear1 AND T1.ProductGroup = T2.ProductGroup1 AND " +
                "T1.Verkaufsbereich3 = T2.Verkaufsbereich31 AND T1.CustomerPotentialGroup = T2.CustomerPotentialGroup1 " +
            "INNER JOIN T3 ON T3.customerId = T1.customerId And T3.InvoiceDateYear = T1.InvoiceDateYear " +
            "UNION " +
            "SELECT " +
            "T1.customerId as t1cid, T1.invoiceDateYear as t1idy, T1.ProductGroup as t1pg, T1.Verkaufsbereich3 as t1so, T1.CustomerPotentialGroup as t1cpg, " +
            "T1.Revenue as t1r, T1.Units as t1u, T1.ArticleCount as t1ac, T1.Margin as t1mar, T1.TransactionCount as t1tc, " +
            "T2.customerId as t2cid, T2.invoiceDateYear1 as t2idy, T2.ProductGroup1 as t2pg, T2.Verkaufsbereich31 as t2so, T2.CustomerPotentialGroup1 as t2cp, " +
            "T2.Revenue1 as t2r, T2.Units1 as t2u, T2.ArticleCount1 as t2ac, T2.Margin1 as t2mar, T2.TransactionCount1 as t2tc, " +
            "T3.customerId as t3ci, T3.InvoiceDateYear as t3idy, T3.TotalRevenue as t3tr, T3.TotalUnits as t3tu, T3.AllArticleCount as t3aac, " +
            "T3.TotalMargin as t3tmar, T3.TotalTransactionCount as t3ttc " +
            "FROM T1 " +
            "RIGHT OUTER JOIN T2 ON T1.customerId = T2.customerId AND T1.InvoiceDateYear = T2.InvoiceDateYear1 AND T1.ProductGroup = T2.ProductGroup1 AND " +
            "T1.Verkaufsbereich3 = T2.Verkaufsbereich31 AND T1.CustomerPotentialGroup = T2.CustomerPotentialGroup1 " +
            "INNER JOIN T3 ON T3.customerId = T2.customerId AND T3.InvoiceDateYear = T2.InvoiceDateYear1"

    return sqlStatement
}