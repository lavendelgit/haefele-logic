/**
 * This is helper class which has functions required for computation of data for data source. This contains logic of how
 * records are prepared for saving into DB.
 */

import net.pricefx.formulaengine.DatamartRowSet

/*
def addCurrentRecordToCustomer(row, currentCusRecList) {
    def customerRec = [:]
    def crRevenue = row["revenue"], crSalesType = row["salestype"], totalRev = row["totalrevenue"], crMargin = row["margin"]
    def crPotGrp = row["customerpotentialgroup"], computedPotGrp = CCComputationUtil.getComputedPotentialGroup(totalRev)
    customerRec["CCYear"] = row["invoicedateyear"]
    customerRec["CCCustomerId"] = row["customerid"]
    customerRec["CCVerkaufsbereich3"] = row["verkaufsbereich3"]
    customerRec["ProductGroup"] = row["productgroup"]
    customerRec["CCUnitsPurchased"] = row["totalunits"]
    customerRec["CCKundePotentialGruppe"] = crPotGrp
    customerRec["CCTotalRevenue"] = totalRev
    customerRec["CCTotalTransactionCount"] = row["totaltransactioncount"]
    customerRec["CCArticleCount"] = row["allarticlecount"]
    customerRec["CCTotalMargin"] = row["totalmargin"]
    customerRec["CCComputedPotentialGroup"] = computedPotGrp
    customerRec["CCCategorizationMatchingRevenue"] = CCComputationUtil.isCategorizationMatchingRevenue(computedPotGrp, crPotGrp)
    customerRec["CorrectionType"] = CCComputationUtil.getCorrectionType(crPotGrp, computedPotGrp)
    if (customerRec["CCDUmsatz"] > customerRec["NonCCDUmsatz"]) {
        def discPerGain = CCComputationUtil.getDiscountPerGain(crPotGrp, computedPotGrp)
        initializeCCDWithSpecificValues(customerRec,
                CCComputationUtil.getPreCCDUmsatz(crRevenue, crSalesType, crPotGrp),
                CCComputationUtil.getPotentialRevGain(discPerGain, crRevenue),
                CCComputationUtil.getPotentialMarginGain(discPerGain, crMargin),
                discPerGain,
                crMargin,
                0.0,
                row["units"],
                0,
                row["articlecount"],
                0,
                row["transactioncount"], 0)
    } else {
        initializeCCDWithSpecificValues(customerRec, 0, 0, 0, 0.0, crMargin, 0.0, 0, row["units"], 0, row["articlecount"], 0, row["transactioncount"])
    }
    //api.trace("Current Record ", " " + customerRec)
    currentCusRecList.add(customerRec)
}
*/

def getRecordToInsertOrUpdate(customerRec) {
    return [
            "CCYear"                 : customerRec["t3idy"],
            "CustomerId"             : customerRec["t3ci"],
            "CorrectionType"         : "Not Evaluated",
            "CCVerkaufsbereich3"     : customerRec["t1so"] ?: customerRec["t2so"],
            "ProductGroup"           : customerRec["t1pg"] ?: customerRec["t2pg"],
            "CCTotalRevenue"         : new BigDecimal(customerRec["t3tr"]),
            "CCTotalTransactionCount": customerRec["t3ttc"],
            "CCArticleCount"         : customerRec["t3aac"],
            "CCTotalMargin"          : new BigDecimal(customerRec["t3tmar"]),
            "CCUnitsPurchased"       : customerRec["t3tu"],
            "CCKundePotentialGruppe" : customerRec["t1cpg"] ?: customerRec["t2cpg"],
            "IsProcessed"            : false,
            "CCDUmsatz"              : new BigDecimal(customerRec["t1r"] ?: 0.0),
            "NonCCDUmsatz"           : new BigDecimal(customerRec["t2r"] ?: 0.0),
            "CCDMargin"              : new BigDecimal(customerRec["t1mar"] ?: 0.0),
            "OtherMargin"            : new BigDecimal(customerRec["t2mar"] ?: 0.0),
            "CCDMenge"               : customerRec["t1u"] ?: 0,
            "NonCCDMenge"            : customerRec["t2u"] ?: 0,
            "CCDQualifiedArticles"   : customerRec["t1ac"] ?: 0,
            "NonCCDArticles"         : customerRec["t2ac"] ?: 0,
            "CCDTransactionCount"    : customerRec["t1tc"] ?: 0,
            "NonCCDTransactioncount" : customerRec["t2tc"] ?: 0
    ]
}

def addRecordsIntoDB (curCusRecList) {
    // Save the final aggregated records into DB
    def recordToInsert
    def headerRecords = [
        "operationType":"add",
        "textMatchStyle":"exact"
    ]
    def currentRequest, body, result
    curCusRecList.each { custRecord ->
        recordToInsert = getRecordToInsertOrUpdate(custRecord)
        currentRequest = [data: [
                header: headerRecords,
                data : recordToInsert
        ]]
        body = api.jsonEncode(currentRequest)
        api.trace( " Current REquest Printing ", "Body : "+ body)
        result = api.boundCall("haefele", "/loaddata/DMDS" , body, true)
    }
}

/**
 * This method populates the output records computed into the target data source.
 * @param curCusRecList
 */
def populateCustomerCategoryRecords(curCusRecList) {
    // Save the final aggregated records into DB
    DatamartRowSet target = api.getDatamartRowSet("target")
    def recordToInsert
    curCusRecList.each { custRecord ->
        recordToInsert = getRecordToInsertOrUpdate(custRecord)
        if (target) {
            target?.addRow(recordToInsert)
        } else {
                api.trace("Not adding row as Target is null...", recordToInsert.collect())
        }
    }
}