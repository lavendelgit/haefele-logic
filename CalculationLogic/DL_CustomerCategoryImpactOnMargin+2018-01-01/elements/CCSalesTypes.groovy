/**
 * Get all the sales type where customer category discount is applicable. Prepare the list of it so that it can be used
 * for various purposes in the calculations.
 */
if (!api.local.allCCSalesType) {
    api.local.allCCSalesType = [:]
    def filters = [
            Filter.equal("attribute5", "Yes")
    ]
    def salesTypesList = api.findLookupTableValues("SalesTypes", *filters)
    api.local.salesTypeCriteria = []
     salesTypesList.collect().each {
         api.local.salesTypeCriteria.add(it.name)
     }
}

return api.local.allCCSalesTypes