if (!api.global.blumArticles) {
  api.global.blumArticles = api.findLookupTableValues("BlumArtikel") ?.name
}

api.trace("api.global.blumArticles",api.global.blumArticles)
