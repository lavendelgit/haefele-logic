def chart = api.newChartBuilder()
	.newDataTable()
		.getOptions()
			.setInitialDataGrouping(false)
			.setDisableDrilldown(false)
			.back()
		.addSeries()
			.setLabel('Provide the recommendation for correcting wrong customer categorization')
			.setHideDataLabels(true)
			.setDatamart('50.DMR')
			.setCurrency('EUR')
			.addDimFilter('Verkaufsbereich3', 'Berlin')
			.addDimFilter('BrancheCategory', null)
			.addDimFilter('CustomerPotentialGroup', null)
			.addGroupBy('CustomerId')
			.addGroupBy('CustomerName')
			.addGroupBy('Verkaufsbereich3')
			.addGroupBy('BrancheCategory')
			.addGroupBy('CustomerPotentialGroup')
			.addGroupBy('ComputedPotentialGroup')
			.addAdditionalMeasure()
				.setMeasure('p5_THYear3Umsatz')
				.withTotal()
				.back()
			.addAdditionalMeasure()
				.setMeasure('p3_THYear2Umsatz')
				.withTotal()
				.back()
			.addAdditionalMeasure()
				.setMeasure('p4_THY2PerUmsatzContribution')
				.withTotal()
				.back()
			.addAdditionalMeasure()
				.setMeasure('p1_THYear1Umsatz')
				.withTotal()
				.back()
			.addAdditionalMeasure()
				.setMeasure('p2_THY1PerUmsatzContribution')
				.withTotal()
				.back()
			.addAdditionalMeasure()
				.setMeasure('p10_THAgeOfCustomer')
				.withTotal()
				.back()
			.addAdditionalMeasure()
				.setMeasure('p9_THCAGR')
				.withTotal()
				.back()
			.setGeneratedQueryDto('{"datamart":"50.DMR","source":"50.DMR","name":null,"label":"Provide the recommendation for correcting wrong customer categorization","rollup":true,"projections":{"CustomerId":{"alias":"CustomerId","label":"Customer Id","expression":"CustomerId"},"CustomerName":{"alias":"CustomerName","label":"Customer Name","expression":"CustomerName"},"Verkaufsbereich3":{"alias":"Verkaufsbereich3","label":"Verkaufsbereich3","expression":"Verkaufsbereich3"},"BrancheCategory":{"alias":"BrancheCategory","label":"Branche Category","expression":"BrancheCategory"},"CustomerPotentialGroup":{"alias":"CustomerPotentialGroup","label":"Customer Potential Group","expression":"CustomerPotentialGroup"},"ComputedPotentialGroup":{"alias":"ComputedPotentialGroup","label":"Computed Potential Group","expression":"ComputedPotentialGroup"},"m1":{"alias":"m1","label":"Umsatz 2018","expression":"SUM({field})","name":"p5_THYear3Umsatz","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p5_THYear3Umsatz"}},"m2":{"alias":"m2","label":"Umsatz 2019","expression":"SUM({field})","name":"p3_THYear2Umsatz","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p3_THYear2Umsatz"}},"m3":{"alias":"m3","label":"Umsatz 2019 Overall Percentage","expression":"SUM({field})","name":"p4_THY2PerUmsatzContribution","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p4_THY2PerUmsatzContribution"}},"m4":{"alias":"m4","label":"Umsatz 2020","expression":"SUM({field})","name":"p1_THYear1Umsatz","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p1_THYear1Umsatz"}},"m5":{"alias":"m5","label":"Umsatz 2020 Overall Percentage","expression":"SUM({field})","name":"p2_THY1PerUmsatzContribution","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p2_THY1PerUmsatzContribution"}},"m6":{"alias":"m6","label":"Customer Age","expression":"SUM({field})","name":"p10_THAgeOfCustomer","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p10_THAgeOfCustomer"}},"m7":{"alias":"m7","label":"CAGR","expression":"SUM({field})","name":"p9_THCAGR","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p9_THCAGR"}}},"filter":null,"aggregateFilter":null,"sortBy":["CustomerId","CustomerName","Verkaufsbereich3","BrancheCategory","CustomerPotentialGroup","ComputedPotentialGroup"],"options":{"currency":"EUR"}}')
			.back()
		.getDictionary()
			.buildFromOpaqueString('[{"sectionIdx":1,"category":"PROJECTION","key":"BrancheCategory","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Branche Category","defaultValue":"Branche Category"},{"sectionIdx":1,"category":"PROJECTION","key":"ComputedPotentialGroup","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Computed Potential Group","defaultValue":"Computed Potential Group"},{"sectionIdx":1,"category":"PROJECTION","key":"CustomerId","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Customer Id","defaultValue":"Customer Id"},{"sectionIdx":1,"category":"PROJECTION","key":"CustomerName","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Customer Name","defaultValue":"Customer Name"},{"sectionIdx":1,"category":"PROJECTION","key":"CustomerPotentialGroup","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Customer Potential Group","defaultValue":"Customer Potential Group"},{"sectionIdx":1,"category":"PROJECTION","key":"m1","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Measure 1","defaultValue":"∑THYear3Umsatz","value":"Umsatz 2018"},{"sectionIdx":1,"category":"PROJECTION","key":"m2","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Measure 2","defaultValue":"∑Umsatz 2019","value":"Umsatz 2019"},{"sectionIdx":1,"category":"PROJECTION","key":"m3","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Measure 3","defaultValue":"∑Umsatz 2019 Overall % Contribution","value":"Umsatz 2019 Overall Percentage","format":"#.##%"},{"sectionIdx":1,"category":"PROJECTION","key":"m4","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Measure 4","defaultValue":"∑Umsatz 2020","value":"Umsatz 2020"},{"sectionIdx":1,"category":"PROJECTION","key":"m5","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Measure 5","defaultValue":"∑Umsatz 2020 Overall % Contribution","value":"Umsatz 2020 Overall Percentage","format":"#.##%"},{"sectionIdx":1,"category":"PROJECTION","key":"m6","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Measure 6","defaultValue":"∑Customer Age","value":"Customer Age"},{"sectionIdx":1,"category":"PROJECTION","key":"m7","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Measure 7","defaultValue":"∑CAGR","value":"CAGR"},{"sectionIdx":1,"category":"PROJECTION","key":"Verkaufsbereich3","sectionLabel":"Provide the recommendation for correcting wrong customer categorization","categoryLabel":"Projection","keyLabel":"Verkaufsbereich3","defaultValue":"Verkaufsbereich3"}]')
			.back()
		.build()

chart.controllerOptions.editDimFilters=true
return chart