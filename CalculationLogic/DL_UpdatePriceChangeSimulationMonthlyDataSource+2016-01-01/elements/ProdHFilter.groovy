return prodHirecharyFilter()

protected List prodHirecharyFilter() {
    List prodHirecharyFilter = []
  
    String productHirechary1 = api.product("attribute11", out.SKU)?.split(" ")[0]
    String productHirechary2 = api.product("attribute12", out.SKU)?.split(" ")[0]
    String productHirechary3 = api.product("attribute13", out.SKU)?.split(" ")[0]
    String productHirechary4 = api.product("attribute14", out.SKU)?.split(" ")[0]
    String productHirechary5 = api.product("attribute15", out.SKU)?.split(" ")[0]
    String productHirechary6 = api.product("attribute16", out.SKU)?.split(" ")[0]
    String productHirechary7 = api.product("attribute17", out.SKU)?.split(" ")[0]

    if (productHirechary1) {
        prodHirecharyFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH1", productHirechary1), Filter.isNull("ZZPRODH1"))))
    }
    if (productHirechary2) {
        prodHirecharyFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH2", productHirechary2), Filter.isNull("ZZPRODH2"))))
    }
    if (productHirechary3) {
        prodHirecharyFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH3", productHirechary3), Filter.isNull("ZZPRODH3"))))
    }
    if (productHirechary4) {
        prodHirecharyFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH4", productHirechary4), Filter.isNull("ZZPRODH4"))))
    }
    if (productHirechary5) {
        prodHirecharyFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH5", productHirechary5), Filter.isNull("ZZPRODH5"))))
    }
    if (productHirechary6) {
        prodHirecharyFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH6", productHirechary6), Filter.isNull("ZZPRODH6"))))
    }
    if (productHirechary7) {
        prodHirecharyFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH7", productHirechary7), Filter.isNull("ZZPRODH7"))))
    }
    return prodHirecharyFilter
}