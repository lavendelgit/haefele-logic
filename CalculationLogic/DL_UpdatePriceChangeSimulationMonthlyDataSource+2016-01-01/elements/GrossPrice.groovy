return api.local.isNormal ? getNormalArticleGrossPrice() : BigDecimal.ZERO//.SpecialArticleTransactionDetails?.avgprice

protected BigDecimal getNormalArticleGrossPrice () {

    //libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from getNormalArticleGrossPrice', api.jsonEncode(out.GrossPriceDetails))
    BigDecimal grossPrice = out.GrossPriceDetails ? out.GrossPriceDetails.get("${Constants.GROSS_MAP_PRICE_LABEL}") : null
    grossPrice = (grossPrice != 0 && grossPrice != null)? grossPrice : api.local.annualGrossPrice

    //libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from getNormalArticleGrossPrice', api.jsonEncode(out.NormalArticleTransactionDetails[Constants.ALL_CUSTOMER_ID]))
    return grossPrice //?: out.NormalArticleTransactionDetails[Constants.ALL_CUSTOMER_ID]?.grossprice
}