import groovy.transform.Field

@Field final Boolean ACCESS_NEW_TABLES = true
@Field final Boolean USE_CUSTOMER_ID_COLUMN = false
@Field final String MATERIAL_TYPE_BOM = "BOM"
@Field final String MATERIAL_TYPE_NA = "N/A"
@Field final String MATERIAL_TYPE_NORMAL = "NORMAL"
@Field final String MATERIAL_TYPE_INTERNAL = "INTERNAL"
@Field final String MATERIAL_TYPE_X_ARTICLE = "XARTICLE"
@Field final String MATERIAL_TYPE_TOPCO_ARTCLE = "TOPCO"

@Field final String PRICING_TYPE_NO_DISCOUNT = "No Discounts"
@Field final String PRICING_TYPE_PER_DISCOUNT = "Per Discount"
@Field final String PRICING_TYPE_MARKUP = "Markup"
@Field final String PRICING_TYPE_NETPRICE = "Net Price"
@Field final String PRICING_TYPE_ABS_DISCOUNT = "Abs Discount"

@Field final String CURRENCY = "Eur"
@Field final String ALL_CUSTOMER_ID = "All"
@Field final String CONDITION_TYPE_NA = "N/A"
@Field final List CONDITION_TYPES = ["COMMON", "INLAND"]//, "EXPORT"]
@Field final String GROSS_MAP_PRICE_LABEL = "GrossPrice"
@Field final List CONDITION_FETCH_FROM_DS = ["ZRA", "ZRI"]
@Field final String GROSS_MAP_MATERIAL_TYPE_LABEL = "MaterialType"
@Field final Boolean ADD_ROWS_WITH_0_REVENUE_AND_QUANTITY = false
@Field final String CUSTOMER_POTENTIAL_GROUP_CONDITION_TABLE = "A779"
@Field final String CUSTOMER_ID_WITH_NO_TRANSACTION = "TransactionIsMissing"
@Field final String CUSTOMER_AND_ARTICLE_FOCUS_GROUP_CONDITION_TABLE = "A781"

@Field final List CONDITION_CONVERT_CURRENCY = ["ZPA"]
@Field final List DS_FIELDS = ["MATNR", "KOTAB", "KBETR", "KONWA"]
@Field final List PX_FIELDS = ["sku", "KOTAB", "KBETR", "KONWA", "DATAB", "DATBI"]
@Field final Map MONTH_MAP = ["01": "Jan", "02": "Feb", "03": "Mar", "04": "Apr", "05": "May", "06": "Jun", "07": "Jul", "08": "Aug", "09": "Sep", "10": "Oct", "11": "Nov", "12": "Dec"]
@Field final List MONTH_ORDERED_KEYS = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]