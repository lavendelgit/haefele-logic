if (api.local.isNormal) {
    List filter = [Filter.equal("name", "CustomerSalesPrice"),
                   Filter.equal("sku", out.SKU),
                   Filter.lessOrEqual("attribute2", out.TargetDate),
                   Filter.greaterOrEqual("attribute3", out.TargetDate)]

    List result = []
    List fields = ["sku", "attribute1", "attribute2", "attribute3", "attribute4", "attribute13"]
    int maxRecords = api.getMaxFindResultsLimit()
    int start = 0
    while (recs = api.find("PX30", start, maxRecords, "-attribute2", fields, false, *filter)) {
        result.addAll(recs)
        start += recs.size()
    }

    Map customerIdRecords = result?.groupBy({ row -> row.attribute1 })

    List filteredAndSortedRecords = []
    Map filteredZPAPLatestRecords = result?.groupBy({ row -> row.attribute1 })?.collectEntries { customerId, filteredRows ->
        filteredAndSortedRecords << filteredRows[0]
    }

    return filteredAndSortedRecords
}