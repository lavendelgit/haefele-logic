if (!api.local.isXArticle && !api.local.isInternal) {
    return null
}

import net.pricefx.formulaengine.DatamartContext

def ctx = api.getDatamartContext()
def dmTransactions = ctx.getDatamart("TransactionsDM")
Map specialArticleTransactionData

if (api.global.specialArticleTransactionData == null) {
    api.global.specialArticleTransactionData = [:]

    List filters = [Filter.or(Filter.ilike("Material", "%X"), Filter.ilike('Material', "999.11%")),
                    Filter.equal("InvoiceDateYear", out.AnalysisYear),
                    libs.__LIBRARY__.HaefeleSpecific.getTransactionInlandFilters()[0]]

    //libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from SpecialArticleTransactionDetails', api.jsonEncode(filters))

    DatamartContext.Query basicQuery = ctx.newQuery(dmTransactions, true)
    basicQuery.select('Material', 'sku')
    basicQuery.select('InvoiceDateMonth', 'month')
    basicQuery.select("SUM(Revenue)", 'revenuetotal')
    basicQuery.select("SUM(Cost)", 'costtotal')
    basicQuery.select("SUM(InvoiceQuantity)", 'allquantity')
    basicQuery.select("SUM(MarginAbs)", 'allmargin')
    basicQuery.where(*filters)
    basicQuery.having(Filter.or(Filter.notEqual('revenuetotal', 0),
            Filter.notEqual('costtotal', 0),
            Filter.notEqual('allmargin', 0)))

    DatamartContext.Query grossPriceQuery = getTransactionQueryForCaculationOnPositiveRevenue(ctx, dmTransactions, filters)
    def transactionData = ctx.executeSqlQuery(getJoinQuery(), basicQuery, grossPriceQuery)
    cacheTransactionData(transactionData)

}
return api.global.specialArticleTransactionData[out.SKU]

/**
 *
 * @param monthlyModifiedTransactionRows
 * @param monthlyDataMap
 * @param monthlyColumnName
 * @return
 */
protected addMonthlyColumnValues(Map monthlyModifiedTransactionRows, Map monthlyDataMap, String monthlyColumnName) {
    monthlyDataMap?.forEach { key, value ->
        String monthlyTransactionMapKey = "$monthlyColumnName" + key
        monthlyModifiedTransactionRows["$monthlyTransactionMapKey"] = value
    }
}

/**
 *
 * @param transactionData
 * @return
 */
protected cacheTransactionData(def transactionData) {
    Map customerTransactionDetailsMap = transactionData.groupBy({ row -> row.sku })
    customerTransactionDetailsMap?.forEach { sku, skuTransactionRows ->
        Map monthlyModifiedTransactionRows = [:]
        skuTransactionRows.forEach { monthlyTransactionRow ->
            String monthNumber = monthlyTransactionRow.month
            monthlyModifiedTransactionRows.put("quantityMonth" + monthNumber, monthlyTransactionRow.quantity)
            monthlyModifiedTransactionRows.put("revenueMonth" + monthNumber, monthlyTransactionRow.revenue)
            monthlyModifiedTransactionRows.put("marginMonth" + monthNumber, monthlyTransactionRow.margin)
            monthlyModifiedTransactionRows.put("costMonth" + monthNumber, monthlyTransactionRow.customerbasecost)
            monthlyModifiedTransactionRows.put("grossPriceMonth" + monthNumber, monthlyTransactionRow.grossprice)
            monthlyModifiedTransactionRows.put("avgPriceMonth" + monthNumber, monthlyTransactionRow.avgprice)
        }
        monthlyModifiedTransactionRows.sku = out.SKU
        monthlyModifiedTransactionRows.customerId = Constants.ALL_CUSTOMER_ID

        api.global.specialArticleTransactionData.put(sku, monthlyModifiedTransactionRows)
    }
}

/**
 *
 * @return
 */
String getJoinQuery() {
    return """ SELECT T1.sku as sku, SUBSTRING(T1.month, 7, 2) as month, IsNull(T1.revenuetotal,0) as revenue, 
            IsNull(T1.allquantity,0) as quantity, T1.allmargin as margin, IsNull(T1.costtotal,0) as cost ,
            CASE WHEN T1.allquantity = 0 THEN IsNull(T1.revenuetotal, 0)
                 WHEN T1.allquantity <> 0 THEN T1.revenuetotal / T1.allquantity
            END as avgprice,
            CASE WHEN T1.allquantity = 0 THEN IsNull(T2.grossprice,0)
                 WHEN T1.allquantity <> 0 THEN T1.revenuetotal / T1.allquantity
            END as grossprice, 
            CASE WHEN T1.allquantity = 0 THEN T1.costtotal
                 WHEN T1.allquantity <> 0 THEN T1.costtotal / T1.allquantity
            END as customerbasecost,
            IsNull(T2.nonzerorows, 0) as nonzerorows 
            FROM T1 LEFT OUTER JOIN T2 ON T1.sku = T2.sku AND T1.month = T2.month """
}
/**
 *
 * @param ctx
 * @param dmTransactions
 * @param filters
 * @return
 */
DatamartContext.Query getTransactionQueryForCaculationOnPositiveRevenue(DatamartContext ctx, def dmTransactions, List filters) {
    List filtersForGrossPriceNBasePrice = [Filter.greaterThan('Revenue', 0)]
    filtersForGrossPriceNBasePrice.addAll(filters)
    DatamartContext.Query grossPriceBasePriceQuery = ctx.newQuery(dmTransactions, true)
    grossPriceBasePriceQuery.select('Material', 'sku')
    grossPriceBasePriceQuery.select('InvoiceDateMonth', 'month')
    grossPriceBasePriceQuery.select('AVG(SalesPricePer100Units/100)', 'grossprice')
    grossPriceBasePriceQuery.select('COUNT(Material)', 'nonzerorows')
    grossPriceBasePriceQuery.where(*filtersForGrossPriceNBasePrice)

    return grossPriceBasePriceQuery
}

/*
find out gross price and base cost for the rows with +revenue
rest three parameters quantity, umsatz and cost are direct sum of the values.
basically implement the queries in the similar way of normal articles.
 */