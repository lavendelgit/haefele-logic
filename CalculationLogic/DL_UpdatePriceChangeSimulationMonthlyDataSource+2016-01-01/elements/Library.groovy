/**getGrossPrice
 *
 * @param sku
 * @return
 */
protected Map getGrossPrice(sku) {
    int skuLength = sku.length()
    Map grossPriceMap = ["${Constants.GROSS_MAP_PRICE_LABEL}": 0, "${Constants.GROSS_MAP_MATERIAL_TYPE_LABEL}": "${Constants.MATERIAL_TYPE_NA}"]
    String skuWithLength10 = (skuLength == 11) ? sku.substring(0, 10) : null

    return getSalesPrice(sku, skuWithLength10) ?: getCompoundPrice(sku, skuWithLength10) ?: grossPriceMap
}
/**getSalesPrice
 *
 * @param sku
 * @param skuWithLength10
 * @return
 * MK latest Lib changes
 */
protected Map getSalesPrice(String sku, String skuWithLength10) {

    List filter = [Filter.equal("attribute7", "EUR"),
                   Filter.lessOrEqual("attribute1", out.TargetDate),
                   Filter.greaterOrEqual("attribute2", out.TargetDate)]

    BigDecimal salesPrice = lib.Find.latestSalesPriceRecord(sku,*filter)?.attribute3

    if (!salesPrice && skuWithLength10) {
        salesPrice = lib.Find.latestSalesPriceRecord(skuWithLength10, Filter.equal("attribute7", "EUR"))?.attribute3
    }
    return salesPrice ? ["${Constants.GROSS_MAP_PRICE_LABEL}": salesPrice / 100, "${Constants.GROSS_MAP_MATERIAL_TYPE_LABEL}": "${Constants.MATERIAL_TYPE_NORMAL}"] : null
}

/**
 *
 * @param sku
 * @param skuWithLength10
 * @return
 */
protected Map getCompoundPrice(String sku, String skuWithLength10) {
    List filter = [Filter.equal("attribute5", "EUR"),
                   Filter.lessOrEqual("attribute6", out.TargetDate),
                   Filter.greaterOrEqual("attribute7", out.TargetDate)]

    BigDecimal compoundPrice = lib.Find.latestCompoundPriceRecord(sku, *filter)?.attribute1

    if (!compoundPrice && skuWithLength10) {
        compoundPrice = lib.Find.latestCompoundPriceRecord(sku, Filter.equal("attribute5", "EUR"))?.attribute1
    }

    return compoundPrice ? ["${constants.GROSS_MAP_PRICE_LABEL}": compoundPrice / 100, "${Constants.GROSS_MAP_MATERIAL_TYPE_LABEL}": "${Constants.MATERIAL_TYPE_BOM}"] : null
}

/**getBaseCostByLookup
 *
 * @param sku
 * @param targetDate
 * @return
 * MK latest Lib changes
 */
protected BigDecimal getBaseCostByLookup(String sku, String targetDate) {
    List filters = [Filter.equal("attribute16", "EUR"),
                    Filter.lessOrEqual("attribute1", targetDate),
                    Filter.greaterOrEqual("attribute2", targetDate)]
    BigDecimal baseCost = lib.Find.latestBaseCostRecord(sku, *filters)?.attribute3
    baseCost = baseCost ? baseCost / 100 : BigDecimal.ZERO
    return baseCost
}

/**getZPLPriceForNormalArticle
 *
 * @return
 */
protected BigDecimal getZPLPriceForNormalArticle() {
    List filters = [
            Filter.equal("attribute8", "EUR"),
            Filter.lessOrEqual("attribute12", out.TargetDate),
            Filter.greaterOrEqual("attribute13", out.TargetDate),
            Filter.or(Filter.equal("attribute6", "DEX1"), Filter.equal("attribute6", ""))
    ]

    def ZPLCost = getLatestZPLCost(out.SKU, *filters)?.attribute7

    return ZPLCost ? ZPLCost / 100 : null
}

/**
 *
 * @param sku
 * @param additionalFilters
 * @return
 */
protected getLatestZPLCost(String sku, Filter... additionalFilters) {
    List filters = [Filter.equal("name", "S_ZPL"),
                    Filter.equal("sku", sku)]

    if (additionalFilters != null) filters.addAll(additionalFilters)

    return api.find("PX50", 0, 1, "-attribute1", *filters)?.find()
}
/**
 *
 * @param sku
 * @param additionalFilters
 * @return
 * MK Latest Lib changes
 */
protected Map getLatestZNRVCost(String sku, Filter... additionalFilters) {
    BigDecimal purchasePrice
    def targetDate = out.TargetDate

    def filters = [Filter.equal("name", "S_ZNRV"),
                   Filter.equal("sku", sku),
                   Filter.lessOrEqual("attribute1", targetDate),
                   Filter.greaterOrEqual("attribute2", targetDate)]

    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    return api.find("PX50", 0, 1, "-attribute1", *filters)?.find()
}

/**
 * **************************************
 */

/**
 *
 * @param sku
 * @return
 */
protected List getPricingDetails(String sku) {
    List allPricingDetails = []
    api.global.activeConditionsList.each { conditionTableName, activeConditions ->
        activeConditions.each { conditionName ->
            List pricingDetails = getConditionDetails(sku, conditionName, conditionTableName)
            if (pricingDetails) {
                allPricingDetails.addAll(pricingDetails)
            }
        }
    }
    List conditionRecordsZPAP = addZPAPConditions(allPricingDetails)
    if (conditionRecordsZPAP) allPricingDetails.addAll(conditionRecordsZPAP)
    return allPricingDetails
}

protected List getConditionDetails(String sku, String conditionName, String conditionTableName) {
    List allConditionDetails = []
    List conditionRecords = getConditionRecords(sku, conditionName, conditionTableName)
    if (conditionRecords && !Constants.CONDITION_FETCH_FROM_DS.contains(conditionName)) {
        if (Constants.CONDITION_CONVERT_CURRENCY.contains(conditionName)) { //(conditionName == "ZPA") {
            conditionRecords.forEach { row ->
                String currency = row.KONWA
                if (currency != "%" && currency != "EUR") {
                    row.KBETR = convertToEur(currency, row.KBETR)
                }
            }
            /*for (row in conditionRecords) {
                String currency = row.KONWA
                if (currency != "%" && currency != "EUR") {
                    row.KBETR = convertToEur(currency, row.KBETR)
                }
            }TODO verify this for ZPA and then remove the code*/
        }
        def groupedConditionRecords = conditionRecords.groupBy { it -> it.KUNNR }
        for (groupedConditionRecord in groupedConditionRecords) {
            resultSize = groupedConditionRecord.value.size()
            BigDecimal disVal = groupedConditionRecord.value.sum { it.KBETR as BigDecimal }
            String pricingType = api.global.conditionsList[conditionName].pricingType ?: Constants.PRICING_TYPE_PER_DISCOUNT

            if (pricingType == Constants.PRICING_TYPE_NETPRICE) {
                disVal = disVal / 100
            }
            BigDecimal discountValue = (disVal && resultSize > 0) ? (disVal / resultSize) : 0.0

            Map conditionDetails = [
                    pricingType   : pricingType,
                    aggregate     : (resultSize > 1) ? "Yes" : "No",
                    KBETR         : discountValue,
                    conditionTable: conditionTableName,
                    conditionName : conditionName//,
                    //KUNNR         : groupedConditionRecord.key
            ]
            if (groupedConditionRecord.key) {
                conditionDetails.KUNNR = groupedConditionRecord.key
            }
            allConditionDetails << conditionDetails
        }
    } else {
        allConditionDetails = conditionRecords
    }

    return allConditionDetails
}

protected List getConditionRecords(String sku, String conditionName, String conditionTableName) {

    List conditionFilter = buildConditionFilter(sku, conditionName, conditionTableName)
    List fields = Constants.CONDITION_FETCH_FROM_DS.contains(conditionName) ?
            api.global.dataFieldsDSMap[conditionTableName] :
            api.global.dataFieldsPXMap[conditionTableName]

    List records = (Constants.CONDITION_FETCH_FROM_DS.contains(conditionName)) ?
            getConditionRecordsFromDS(conditionName, conditionTableName, conditionFilter, fields) :
            getConditionRecordsFromPX(conditionFilter, fields, conditionTableName)

    return records
}


protected List getConditionRecordsFromPX(List conditionFilter, List fields, String conditionTableName) {
    List result = []
    def start = 0
    while (recs = api.find("PX50", start, api.getMaxFindResultsLimit(), "-DATAB", fields, false, *conditionFilter)) {
        result.addAll(recs)
        start += recs.size()
    }

    return filteredRecords(result, conditionTableName)
}

protected List filteredRecords(List result, String conditionTableName) {
    List commonKeyColmns = ["sku"]
    List keyColumns = api.global.conditionTables[conditionTableName]["additionalKeyColumns"]?.split(",")
    if (keyColumns) commonKeyColmns?.addAll(keyColumns)
    List latestPXfilteredAndSortedRecords = []
    Map pxFilteredRows = result.groupBy({ row -> commonKeyColmns.collect { String colName -> row[colName] }.join(".") })?.collectEntries { key, filteredRecords ->
        latestPXfilteredAndSortedRecords << filteredRecords[0]
    }

    return latestPXfilteredAndSortedRecords
}

protected List getConditionRecordsFromDS(String conditionName, String conditionTableName, List conditionFilter, List fields) {
    List allConditionDetails = []
    def ctx = api.getDatamartContext()
    def ds = ctx.getDataSource("$conditionName")
    def query = ctx.newQuery(ds)
    fields.each { String field ->
        (field == "KBETR") ? query.select("AVG(KBETR)", "KBETR") : query.select(field)
    }
    query.where(*conditionFilter)
    result = ctx.executeQuery(query)?.getData()?.collect()

    result?.each { row ->
        Map conditionDetails = [pricingType   : Constants.PRICING_TYPE_PER_DISCOUNT,
                                aggregate     : "No",
                                KBETR         : row.KBETR,
                                conditionTable: conditionTableName,
                                conditionName : conditionName,
                                KUNNR         : row.KUNNR]
        allConditionDetails << conditionDetails
    }
    return allConditionDetails
}
/**buildConditionFilter
 *
 * @param sku
 * @param conditionName
 * @param conditionTable
 * @return
 */
protected List buildConditionFilter(String sku, String conditionName, String conditionTable) {

    String skuFieldName = Constants.CONDITION_FETCH_FROM_DS.contains(conditionName) ? "MATNR" : "sku"
    List conditionFilter = (api.global.conditionFilterMap[conditionTable][conditionName] as List).clone()
    conditionFilter << out.DateFilter
    String pgFilter = api.global.conditionTables.find { conditionTableRow -> conditionTableRow.key == conditionTable }?.value["pgFilter"]
    conditionFilter << Filter.in("$skuFieldName", [sku, "*"])

    if (pgFilter) {
        conditionFilter.addAll(out.ProdHFilter)
    }
    return conditionFilter
}

/**addZPAPConditions
 *
 * @param allPricingDetails
 * @return
 * MK - Library changes
 */
protected List addZPAPConditions(allPricingDetails) {
    List conditionRecordsZPAP = out.CacheZPAPdata
    List ZPAPConditionRecords = []
    conditionRecordsZPAP?.each { conditionRow ->
        Map conditionDetails = [
                pricingType   : Constants.PRICING_TYPE_NETPRICE,
                aggregate     : "No",
                KBETR         : ((conditionRow.attribute4 ?: 0) as BigDecimal),// / 100,
                conditionTable: conditionRow.attribute13,
                conditionName : "ZPAP",
                KUNNR         : conditionRow.attribute1
        ]
        ZPAPConditionRecords.add(conditionDetails)
    }
    return ZPAPConditionRecords
}

/**
 *
 * @param fromCurrency
 * @param KBETR
 * @return
 */
protected String convertToEur(String fromCurrency, String KBETR) {
    BigDecimal convertedKBETR = KBETR as BigDecimal
    BigDecimal exchangerate = api.global.exchangeRate[fromCurrency] ?: 1

    return ((fromCurrency != "EUR") ? (exchangerate * convertedKBETR) : convertedKBETR) as String
}