out.SKU?.contains("X") ?
        (api.local.isXArticle = true) :
        (out.SKU?.contains("999.11") ? (api.local.isInternal = true) : (api.local.isNormal = true))
