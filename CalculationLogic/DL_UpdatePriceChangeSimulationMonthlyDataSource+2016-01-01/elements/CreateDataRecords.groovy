/*
if (out.IsTopcoArticle) {
    return null
}
*/

api.local.allRows = []

List pricingDetails = (api.local.isXArticle || api.local.isInternal) ? null : Library.getPricingDetails(out.SKU)
Map transactionData = api.local.isNormal ? out.NormalArticleTransactionDetails : api.global.specialArticleTransactionData

if (transactionData) {
    api.local.isNormal ? processNormalArticles(pricingDetails, transactionData) : processSpecialArticles(transactionData[out.SKU])
}
//api.trace("api.local.allRows", api.local.allRows)

protected processNormalArticles(List pricingDetails, Map transactionData) {
    boolean allCustomerIdRowAdded = false
    List customerIdsList = []
    if (api.global.articleFocusGroupMap[out.SKU] || api.global.customerPotentialGroupRecords) {
        getFocusGroupDiscount(pricingDetails, transactionData)
    }

    pricingDetails.each { Map pricingDetail ->
        String customerId = pricingDetail.KUNNR ?: Constants.ALL_CUSTOMER_ID
        String transactionCustomerId = (customerId == Constants.ALL_CUSTOMER_ID) ? customerId : getCustomerId(customerId, transactionData)
        if (transactionCustomerId && transactionCustomerId != Constants.ALL_CUSTOMER_ID) {
            if (!customerIdsList.contains(transactionCustomerId)) {
                customerIdsList << transactionCustomerId
            }

            Map currentCustomerTransactionDetails = transactionData[transactionCustomerId]
            Map pocketPriceDetails = getPocketPriceDetails(pricingDetail, currentCustomerTransactionDetails)
            String secondaryKey = customerId ? pricingDetail?.conditionName + "~" + pricingDetail?.conditionTable + "~" + customerId + "~" + pricingDetail?.KBETR : pricingDetail?.conditionName + "~" + pricingDetail?.conditionTable + "~" + pricingDetail?.KBETR
            addPricingDetailsRow(secondaryKey, transactionCustomerId, currentCustomerTransactionDetails, pocketPriceDetails, pricingDetail)
        }
    }
    Map transactionDataForLeftOverCustomerIds = transactionData.findAll { key, value -> !(key in customerIdsList)
    }
    if (transactionDataForLeftOverCustomerIds) {
        List transationRowsForLeftOverCustomerIds = transactionDataForLeftOverCustomerIds.values() as List
        summarizeTransatctionRowsForLeftOverCustomerIds(transationRowsForLeftOverCustomerIds)
    }
}

protected summarizeTransatctionRowsForLeftOverCustomerIds(List transationRowsForLeftOverCustomerIds) {
    Map allCustomerTransactionDetails = [:]
    allCustomerTransactionDetails.sku = out.SKU
    allCustomerTransactionDetails.customerId = Constants.ALL_CUSTOMER_ID
    int nonZeroBaseCostCount
    Constants.MONTH_MAP.forEach { keyMonthNumber, keyMonthName ->
        nonZeroBaseCostCount = 0
        String quantityMonth = "quantityMonth" + keyMonthNumber
        Integer totalQuantity = transationRowsForLeftOverCustomerIds.sum {
            it["quantityMonth$keyMonthNumber"] ?: 0
        }
        BigDecimal totalRevenue = transationRowsForLeftOverCustomerIds.sum {
            it["revenueMonth$keyMonthNumber"] ?: 0
        }
        BigDecimal totalMargin = transationRowsForLeftOverCustomerIds.sum {
            it["marginMonth$keyMonthNumber"] ?: 0
        }
        BigDecimal totalCost = transationRowsForLeftOverCustomerIds.sum {
            if (it["costMonth$keyMonthNumber"] && it["costMonth$keyMonthNumber"] != 0) {
                nonZeroBaseCostCount++
            }
            it["costMonth$keyMonthNumber"] ?: 0
        }

        boolean isTotalQuantityNonZero = totalQuantity != 0
        allCustomerTransactionDetails["quantityMonth" + keyMonthNumber] = totalQuantity
        allCustomerTransactionDetails["revenueMonth" + keyMonthNumber] = totalRevenue
        allCustomerTransactionDetails["marginMonth" + keyMonthNumber] = totalMargin
        allCustomerTransactionDetails["costMonth" + keyMonthNumber] = nonZeroBaseCostCount ? totalCost / nonZeroBaseCostCount : totalCost
        allCustomerTransactionDetails["avgPriceMonth" + keyMonthNumber] = isTotalQuantityNonZero ? totalRevenue / totalQuantity : totalRevenue
        allCustomerTransactionDetails["grossPriceMonth"+keyMonthNumber] = isTotalQuantityNonZero ? totalRevenue / totalQuantity : totalRevenue
    }
    secondaryKey = Constants.CONDITION_TYPE_NA + "~" + Constants.CONDITION_TYPE_NA + "~" + Constants.ALL_CUSTOMER_ID + "~" + "0"
    Map allCustomerIdPockePriceDetails = getPocketPriceDetailsForNonPricingConditionsCustomer(allCustomerTransactionDetails)
    addPricingDetailsRow(secondaryKey, Constants.ALL_CUSTOMER_ID, allCustomerTransactionDetails, allCustomerIdPockePriceDetails)
}

protected getFocusGroupDiscount(List pricingDetails, Map transactionData) {
    transactionData?.each { customerId, transactionDataRow ->
        if (customerId != Constants.ALL_CUSTOMER_ID) {
            String focusGroup = api.global.articleFocusGroupMap[out.SKU]
            String salesOffice = transactionDataRow["salesoffice"]
            String customerPotentialGroup = transactionDataRow["customerpotentialgroup"]
            if (customerPotentialGroup && (customerPotentialGroup in ["R0", "R1", "R2", "R3"])) {
                String key = salesOffice + "-" + focusGroup + "-" + customerPotentialGroup
                BigDecimal focusGroupRebate = api.global.focusGroupRecords[key] ?: 0.0
                BigDecimal customerGroupRebate = api.global.customerPotentialGroupRecords[customerPotentialGroup] ?: 0.0
                BigDecimal focusAndCustomerGroupRebate = (Math.abs(customerGroupRebate) + Math.abs(focusGroupRebate)) * -1
                String conditionTable = (!focusGroupRebate && customerGroupRebate != null) ? Constants.CUSTOMER_POTENTIAL_GROUP_CONDITION_TABLE : Constants.CUSTOMER_AND_ARTICLE_FOCUS_GROUP_CONDITION_TABLE
                if (focusAndCustomerGroupRebate != null) {
                    Map conditionDetails = [pricingType   : Constants.PRICING_TYPE_PER_DISCOUNT,
                                            aggregate     : "No",
                                            KBETR         : focusAndCustomerGroupRebate,
                                            conditionTable: conditionTable,
                                            conditionName : "ZRPG",
                                            KUNNR         : customerId]
                    pricingDetails.addAll(conditionDetails)
                }
            }
        }
    }
}

protected processSpecialArticles(Map transactionData) {
    if (transactionData) {
        String customerId = transactionData.customerId
        Map pocketPriceDetails = getPocketPriceDetailsForNonPricingConditionsCustomer(transactionData)
        String secondaryKey = Constants.CONDITION_TYPE_NA + "~" + Constants.CONDITION_TYPE_NA + "~" + customerId + "~" + "0"
        addPricingDetailsRow(secondaryKey, customerId, transactionData, pocketPriceDetails)
    }
}

protected addPricingDetailsRow(String secondaryKey, String transactionCustomerId, Map currentCustomerTransactionDetails, Map pocketPriceDetails, Map pricingDetail = null) {
    def targetRowSet = api.getDatamartRowSet("target")
    Map row = createTargetRow(secondaryKey, transactionCustomerId, currentCustomerTransactionDetails, pocketPriceDetails, pricingDetail)
//  libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from processNormalArticles', api.jsonEncode(row))
//    api.trace("******************targetRow", row)
    api.local.allRows << row
    targetRowSet?.addRow(row)
}

protected Map createTargetRow(String secondaryKey, String customerId, Map transactionData, Map pocketPriceDetails, Map pricingDetail = null) {
    String pricingType = (api.local.isXArticle || api.local.isInternal) ? (api.local.isXArticle ? Constants.MATERIAL_TYPE_X_ARTICLE : Constants.MATERIAL_TYPE_INTERNAL) : pricingDetail ? pricingDetail.pricingType : Constants.PRICING_TYPE_NO_DISCOUNT

    Map row = [SecondaryKey  : secondaryKey,
               Material      : out.SKU,
               ConditionName : pricingDetail ? pricingDetail.conditionName : Constants.CONDITION_TYPE_NA,
               ConditionTable: pricingDetail ? pricingDetail.conditionTable : Constants.CONDITION_TYPE_NA,
               Currency      : Constants.CURRENCY,
               CustomerId    : (customerId != Constants.ALL_CUSTOMER_ID) ? customerId : pricingDetail?.KUNNR,
               KBETR         : pricingDetail ? pricingDetail.KBETR : 0,
               MarkUp        : out.Markup,
               MaterialType  : out.MaterialType,
               PricingType   : pricingDetail?.pricingType ?: pricingType,
               PurchasePrice : out.PurchasePrice,
               Year          : out.AnalysisYear,
               ZPL           : out.ZPL]

    addMonthlyColumns(row, pocketPriceDetails, transactionData)//, customerId)
    return row
}

protected String getCustomerId(String customerId, Map transactionData) {
    String customerIdWithPrefix0 = "000" + customerId
    return transactionData[customerId] ? customerId : transactionData[customerIdWithPrefix0] ? customerIdWithPrefix0 : Constants.ADD_ROWS_WITH_0_REVENUE_AND_QUANTITY ? Constants.CUSTOMER_ID_WITH_NO_TRANSACTION : null
}

protected Map getPocketPriceDetails(Map pricingDetail, Map currentCustomerTransactionDetails) {
    Map pocketPriceDetails = [:]

    BigDecimal lastMonthlyGrossPrice = getFirstNonZeroGrossPrice(currentCustomerTransactionDetails)
    BigDecimal lastMonthlyBaseCost = getFirstNonZeroBaseCost(currentCustomerTransactionDetails)
    BigDecimal currentMonthGrossPrice, costMonth, baseCost, pocketMarginPer, pocketMargin, pocketPrice
    Integer quantity
    boolean isValidQuantity = false

    Constants.MONTH_ORDERED_KEYS.each { String keyMonthNumber ->
        quantity = currentCustomerTransactionDetails["quantityMonth" + keyMonthNumber]
        isValidQuantity = quantity != null && quantity != 0
        costMonth = currentCustomerTransactionDetails["costMonth" + keyMonthNumber]
        currentMonthGrossPrice = currentCustomerTransactionDetails["grossPriceMonth" + keyMonthNumber]
        currentMonthGrossPrice = (currentMonthGrossPrice || isValidQuantity) ? currentMonthGrossPrice : (lastMonthlyGrossPrice ?: out.GrossPrice)
        currentMonthGrossPrice = currentMonthGrossPrice ?: BigDecimal.ZERO
        baseCost = costMonth
        baseCost = (baseCost || isValidQuantity) ? baseCost : (lastMonthlyBaseCost ?: out.BaseCost)
        baseCost = baseCost ?: BigDecimal.ZERO

        switch (pricingDetail.pricingType) {
            case Constants.PRICING_TYPE_PER_DISCOUNT:
                pocketPrice = (currentMonthGrossPrice * (1 + pricingDetail.KBETR))
                break
            case Constants.PRICING_TYPE_MARKUP:
                pocketPrice = (1 + pricingDetail.KBETR) * baseCost
                break
            case Constants.PRICING_TYPE_NETPRICE:
                pocketPrice = pricingDetail.KBETR
                break
            case Constants.PRICING_TYPE_ABS_DISCOUNT:
                pocketPrice = currentMonthGrossPrice ? (currentMonthGrossPrice - pricingDetail.KBETR) : 0
                break
        }
        pocketMargin = pocketPrice ? pocketPrice - baseCost : BigDecimal.ZERO
        pocketMarginPer = pocketPrice ? (pocketMargin / pocketPrice) : BigDecimal.ZERO

        pocketPriceDetails.put("pocketPriceMonth" + keyMonthNumber, pocketPrice)
        pocketPriceDetails.put("pocketMarginMonth" + keyMonthNumber, pocketMargin)
        pocketPriceDetails.put("pocketMarginPerMonth" + keyMonthNumber, pocketMarginPer)
        pocketPriceDetails.put("baseCost" + keyMonthNumber, baseCost)
        pocketPriceDetails.put("grossPriceMonth" + keyMonthNumber, currentMonthGrossPrice)
        lastMonthlyGrossPrice = (quantity > 0) ? currentMonthGrossPrice: lastMonthlyGrossPrice
        lastMonthlyBaseCost = (quantity > 0) ? baseCost: lastMonthlyBaseCost
    }
    return pocketPriceDetails
}

protected BigDecimal getFirstNonZeroGrossPrice(Map currentCustomerTransactionDetails) {
    Integer quantity
    String firstNonZeroKey = Constants.MONTH_ORDERED_KEYS.find { String monthNumber ->
        quantity = currentCustomerTransactionDetails["quantityMonth" + monthNumber]?:BigDecimal.ZERO
        quantity > 0 &&
        currentCustomerTransactionDetails["grossPriceMonth" + monthNumber] }
//    api.trace("The first non-zero Gross Price", firstNonZeroKey + "")

    return firstNonZeroKey ? currentCustomerTransactionDetails["grossPriceMonth" + firstNonZeroKey] : BigDecimal.ZERO
}

protected BigDecimal getFirstNonZeroBaseCost(Map currentCustomerTransactionDetails) {
    Integer quantity
    String firstNonZeroKey = Constants.MONTH_ORDERED_KEYS.find { String monthNumber ->
        quantity = currentCustomerTransactionDetails["quantityMonth" + monthNumber]?:BigDecimal.ZERO
        quantity > 0 &&
        currentCustomerTransactionDetails["costMonth" + monthNumber]
    }
//    api.trace("The first non-zero Base Cost", firstNonZeroKey + "")
    if (!firstNonZeroKey) {
        return BigDecimal.ZERO
    }
    BigDecimal baseCost = currentCustomerTransactionDetails["costMonth" + firstNonZeroKey]

    return baseCost
}

protected Map getPocketPriceDetailsForNonPricingConditionsCustomer(Map currentCustomerTransactionDetails) {
    Map pocketPriceDetails = [:]
    BigDecimal lastMonthlyGrossPrice = getFirstNonZeroGrossPrice(currentCustomerTransactionDetails)
    BigDecimal lastMonthlyBaseCost = getFirstNonZeroBaseCost(currentCustomerTransactionDetails)
    BigDecimal currentMonthGrossPrice, revenue, baseCost, pocketMarginPer, pocketMargin, pocketPrice
    Integer quantity
    boolean isValidQuantity = false

    Constants.MONTH_ORDERED_KEYS.each { String keyMonthNumber ->
        quantity = currentCustomerTransactionDetails["quantityMonth" + keyMonthNumber]
        isValidQuantity = quantity != null && quantity != 0
        baseCost = currentCustomerTransactionDetails["costMonth" + keyMonthNumber]
        baseCost = (baseCost || isValidQuantity) ? baseCost : (lastMonthlyBaseCost ?: out.BaseCost)
        baseCost = baseCost ?: BigDecimal.ZERO
        revenue = currentCustomerTransactionDetails["revenueMonth" + keyMonthNumber]
        pocketPrice = quantity ? revenue / quantity : revenue
        pocketMargin = pocketPrice ? pocketPrice - baseCost : BigDecimal.ZERO
        pocketMarginPer = pocketPrice ? (pocketMargin / pocketPrice) : BigDecimal.ZERO
        currentMonthGrossPrice = currentCustomerTransactionDetails["grossPriceMonth" + keyMonthNumber]
        currentMonthGrossPrice = (currentMonthGrossPrice || isValidQuantity) ? currentMonthGrossPrice : (lastMonthlyGrossPrice ?: out.GrossPrice)
        currentMonthGrossPrice = currentMonthGrossPrice ?: BigDecimal.ZERO

        pocketPriceDetails.put("pocketPriceMonth" + keyMonthNumber, pocketPrice ?: BigDecimal.ZERO)
        pocketPriceDetails.put("pocketMarginMonth" + keyMonthNumber, pocketMargin ?: BigDecimal.ZERO)
        pocketPriceDetails.put("pocketMarginPerMonth" + keyMonthNumber, pocketMarginPer ?: BigDecimal.ZERO)
        pocketPriceDetails.put("baseCost" + keyMonthNumber, baseCost ?: BigDecimal.ZERO)
        pocketPriceDetails.put("grossPriceMonth" + keyMonthNumber, currentMonthGrossPrice)
        lastMonthlyGrossPrice = (quantity > 0) ? currentMonthGrossPrice: lastMonthlyGrossPrice
        lastMonthlyBaseCost = (quantity > 0) ? baseCost: lastMonthlyBaseCost
    }

    return pocketPriceDetails
}

protected addMonthlyColumns(Map targetRow, Map pocketPriceDetails, Map transactionData) {//, String customerId) {
    BigDecimal quantity, baseCost, grossPrice
    Constants.MONTH_MAP.forEach { keyMonthNumber, keyMonthName ->
        quantity = transactionData["quantityMonth" + keyMonthNumber]
        baseCost = pocketPriceDetails["baseCost" + keyMonthNumber]
        grossPrice = pocketPriceDetails["grossPriceMonth" + keyMonthNumber]

        targetRow[keyMonthName + "PocketMarginPer"] = pocketPriceDetails["pocketMarginPerMonth" + keyMonthNumber]
        targetRow[keyMonthName + "QtySold"] = quantity?:BigDecimal.ZERO
        targetRow[keyMonthName + "TotalRevenue"] = transactionData["revenueMonth" + keyMonthNumber]?:BigDecimal.ZERO
        targetRow[keyMonthName + "BaseCost"] = baseCost
        targetRow[keyMonthName + "AvgGrossPrice"] = transactionData["avgPriceMonth" + keyMonthNumber]?:BigDecimal.ZERO
        targetRow[keyMonthName + "PocketPrice"] = pocketPriceDetails["pocketPriceMonth" + keyMonthNumber]
        targetRow[keyMonthName + "PocketMargin"] = pocketPriceDetails["pocketMarginMonth" + keyMonthNumber]
        targetRow[keyMonthName + "GrossPrice"] = grossPrice
        targetRow[keyMonthName + "Margin"] = grossPrice - baseCost
        targetRow[keyMonthName + "MarginPer"] = grossPrice ? ((grossPrice - baseCost) / grossPrice) : BigDecimal.ZERO
    }
}

/*
why are we taking lookup value base cost?
we can directly pick the base cost from the transaction.

same goes for gross price...

 */