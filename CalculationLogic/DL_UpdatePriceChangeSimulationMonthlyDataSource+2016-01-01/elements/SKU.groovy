if (api.isDebugMode()) {
//    return out.DebugSku //return "772.03.620"
    return "007.59.400"
}

def product = api.currentItem()
if ((!product || !product.sku)) {
    api.abortCalculation(true)
}
return product?.sku

/*
732.09.993
000.07.115
637.38.341
633.50.551
000.33.431
000.30.900
999.11.235
999.11.270
637.76.354
732.01.321
342.42.663
 */