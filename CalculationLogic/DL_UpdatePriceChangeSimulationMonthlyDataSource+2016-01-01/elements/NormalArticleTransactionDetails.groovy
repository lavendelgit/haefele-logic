List transactionRows = out.NormalArticleTransaction
//api.trace("Normal Article Transaction return value", api.jsonEncode(transactionRows))

return (transactionRows) ? constructCustomerTransactionDetailsMap(transactionRows, out.SKU) : null

Map constructCustomerTransactionDetailsMap(List transactionRows, String sku) {
    if (transactionRows) {
        BigDecimal totalAnnualRevenue = transactionRows.revenue.sum()
        BigDecimal totalQuantity = transactionRows.quantity.sum()
        api.local.annualGrossPrice = (totalQuantity) ? totalAnnualRevenue / totalQuantity : totalAnnualRevenue
//        api.trace("Special Article annualGrossPrice", api.local.annualGrossPrice)
        Map customerTransactionDetailsMap = transactionRows.groupBy({ row -> row.customerid })
        Map customerIdModifiedAllTransactionRows = [:]
//        api.trace("customerTransactionDetailsMap", customerTransactionDetailsMap)
        customerTransactionDetailsMap.forEach { customerId, customerTransactionRows ->
            //api.trace("customerId",customerId)
            Map customerIdModifiedTransactionRows = [:]
            Map monthlyTransactionRows = customerTransactionRows.groupBy({ row -> row.month
            })
            Map monthlyQuantity = [:]
            Map monthlyRevenue = [:]
            Map monthlyMargin = [:]
            Map monthlyCost = [:]
            Map monthlyAvgPrice = [:]
            Map monthlyGrossPrice = [:]
            BigDecimal currentMonthGrossPrice
            monthlyTransactionRows?.forEach { monthNumber, monthTransationRows ->
                monthlyQuantity.put(monthNumber, monthTransationRows.quantity.sum())
                monthlyRevenue.put(monthNumber, monthTransationRows.revenue.sum())
                monthlyMargin.put(monthNumber, monthTransationRows.margin.sum())
                monthlyCost.put(monthNumber, monthTransationRows.customerbasecost.sum())
                monthlyAvgPrice.put(monthNumber, monthTransationRows.avgprice.sum() / monthTransationRows.size())
                currentMonthGrossPrice = monthTransationRows.grossprice.sum() / monthTransationRows.size()
                currentMonthGrossPrice = currentMonthGrossPrice?: api.local.annualGrossPrice
                monthlyGrossPrice.put(monthNumber, currentMonthGrossPrice)
            }

            customerIdModifiedTransactionRows.sku = out.SKU
            customerIdModifiedTransactionRows.customerId = customerId
            customerIdModifiedTransactionRows.customerpotentialgroup = customerTransactionRows[0].customerpotentialgroup
            customerIdModifiedTransactionRows.salesoffice = customerTransactionRows[0].salesoffice

            addMonthlyColumnValues(customerIdModifiedTransactionRows, monthlyQuantity, "quantityMonth")
            addMonthlyColumnValues(customerIdModifiedTransactionRows, monthlyRevenue, "revenueMonth")
            addMonthlyColumnValues(customerIdModifiedTransactionRows, monthlyMargin, "marginMonth")
            addMonthlyColumnValues(customerIdModifiedTransactionRows, monthlyCost, "costMonth")
            addMonthlyColumnValues(customerIdModifiedTransactionRows, monthlyAvgPrice, "avgPriceMonth")
            addMonthlyColumnValues(customerIdModifiedTransactionRows, monthlyGrossPrice, "grossPriceMonth")
//            api.trace("monthlyTransactionRows", customerIdModifiedTransactionRows)

            customerIdModifiedAllTransactionRows.put(customerId, customerIdModifiedTransactionRows)
        }
        return customerIdModifiedAllTransactionRows
    }
    return null
}

protected addMonthlyColumnValues(Map customerIdModifiedTransactionRows, Map monthlyDataMap, String monthlyColumnName) {
    monthlyDataMap?.forEach { key, value ->
        String monthlyTransactionMapKey = "$monthlyColumnName" + key
        customerIdModifiedTransactionRows["$monthlyTransactionMapKey"] = value
    }
}