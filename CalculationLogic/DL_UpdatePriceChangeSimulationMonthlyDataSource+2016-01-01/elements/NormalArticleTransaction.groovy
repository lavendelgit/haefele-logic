import net.pricefx.formulaengine.DatamartContext

def ctx = api.getDatamartContext()
def dmTransactions = ctx.getDatamart("TransactionsDM")
String skuUnderProcess = out.SKU
List commonFilters = getTransactionCommonFiltersForNormalElements(skuUnderProcess)

DatamartContext.Query basicTransactionQuery = getTransactionQueryForMainCalculations(ctx, dmTransactions, commonFilters)
DatamartContext.Query grossPriceQuery = getTransactionQueryForCaculationOnPositiveRevenue(ctx, dmTransactions, commonFilters)
DatamartContext.Query customerPotentialGroupQuery = getCustomerPotentialGroupQueryForMainCalculations(ctx, dmTransactions, commonFilters)
DatamartContext.Query latestCustomerPotentialGroupQuery = getLatestCustomerPotentialGroupQuery(ctx, dmTransactions, commonFilters)

String completeDataFetchQuery = getJoinQuery()
def transactionData = ctx.executeSqlQuery(completeDataFetchQuery, basicTransactionQuery, grossPriceQuery, customerPotentialGroupQuery, latestCustomerPotentialGroupQuery)?.toResultMatrix()

//verifyOutput(basicTransactionQuery, ctx, 'basicTransactionQuery')
//verifyOutput(grossPriceQuery, ctx, 'grossPriceQuery')
//api.trace("The output of TransactionData", api.jsonEncode(transactionData))

return transactionData?.collect()?.entries[0]

List getTransactionCommonFiltersForNormalElements(String sku) {
    List filters = [Filter.equal("Material", sku),
                    Filter.equal("InvoiceDateYear", out.AnalysisYear),
                    libs.__LIBRARY__.HaefeleSpecific.getTransactionInlandFilters()[0]]

    return filters
}

DatamartContext.Query getTransactionQueryForMainCalculations(DatamartContext ctx, def dmTransactions, List filters) {
    DatamartContext.Query basicQuery = ctx.newQuery(dmTransactions, true)
    basicQuery.select('Material', 'sku')
    basicQuery.select('InvoiceDateMonth', 'month')
    basicQuery.select("CustomerId", 'customerid')
    basicQuery.select("SUM(Revenue)", 'revenuetotal')
    basicQuery.select("SUM(Cost)", 'costtotal')
    basicQuery.select("SUM(InvoiceQuantity)", 'allquantity')
    basicQuery.select("SUM(MarginAbs)", 'allmargin')
    basicQuery.where(*filters)
    basicQuery.having(Filter.or(Filter.notEqual('revenuetotal', 0),
            Filter.notEqual('costtotal', 0),
            Filter.notEqual('allmargin', 0)))

    return basicQuery
}

DatamartContext.Query getTransactionQueryForCaculationOnPositiveRevenue(DatamartContext ctx, def dmTransactions, List filters) {
    List filtersForGrossPriceNBasePrice = [Filter.greaterThan('Revenue', 0)]
    filtersForGrossPriceNBasePrice.addAll(filters)
    DatamartContext.Query grossPriceBasePriceQuery = ctx.newQuery(dmTransactions, true)
    grossPriceBasePriceQuery.select('Material', 'sku')
    grossPriceBasePriceQuery.select('InvoiceDateMonth', 'Month')
    grossPriceBasePriceQuery.select("CustomerId", 'customerid')
    grossPriceBasePriceQuery.select('AVG(SalesPricePer100Units/100)', 'grossprice')
    grossPriceBasePriceQuery.select('COUNT(Material)', 'nonzerorows')
    grossPriceBasePriceQuery.where(*filtersForGrossPriceNBasePrice)

    return grossPriceBasePriceQuery
}

DatamartContext.Query getLatestCustomerPotentialGroupQuery(DatamartContext ctx, def dmTransactions, List filters) {
    DatamartContext.Query latestCustomerPotentialGroupQuery = ctx.newQuery(dmTransactions, true)
    latestCustomerPotentialGroupQuery.select("CustomerId", 'customerid')
    latestCustomerPotentialGroupQuery.select("Max(InvoiceId)", 'invoiceid')
    latestCustomerPotentialGroupQuery.where(*filters)
    return latestCustomerPotentialGroupQuery
}

DatamartContext.Query getCustomerPotentialGroupQueryForMainCalculations(DatamartContext ctx, def dmTransactions, List filters) {
    DatamartContext.Query customerPotentialGroupQuery = ctx.newQuery(dmTransactions, true)
    customerPotentialGroupQuery.select("CustomerId", 'customerid')
    customerPotentialGroupQuery.select("invoiceid", 'invoiceid')
    customerPotentialGroupQuery.select("CustomerPotentialGroup", 'customerpotentialgroup')
    customerPotentialGroupQuery.select("SalesOffice", 'salesoffice')
    customerPotentialGroupQuery.where(*filters)
    return customerPotentialGroupQuery
}

String getJoinQuerycompleteDataFetchQueryTest() {
    return """SELECT T1.sku as sku, T1.customerid as customerid, T1.customerpotentialgroup, T1.salesoffice 
            FROM T1 INNER JOIN T2 ON T1.customerid = T2.customerid AND T1.invoiceid = T2.invoiceid"""
}

String getJoinQuery() {
    return """SELECT T1.sku as sku, T1.customerid as customerid, SUBSTRING(T1.month, 7, 2) as month, IsNull(T1.revenuetotal,0) as revenue, 
            IsNull(T1.allquantity,0) as quantity, T1.allmargin as margin, IsNull(T1.costtotal,0) as cost,
            T3.customerpotentialgroup, T3.salesoffice,
            CASE WHEN T1.allquantity = 0 THEN IsNull(T1.revenuetotal, 0)
                 WHEN T1.allquantity <> 0 THEN T1.revenuetotal / T1.allquantity
            END as avgprice,
            CASE WHEN T1.allquantity = 0 THEN IsNull(T2.grossprice,0)
                 WHEN T1.allquantity <> 0 THEN T1.revenuetotal / T1.allquantity
            END as grossprice, 
            CASE WHEN T1.allquantity = 0 THEN T1.costtotal
                 WHEN T1.allquantity <> 0 THEN T1.costtotal / T1.allquantity
            END as customerbasecost,
            IsNull(T2.nonzerorows, 0) as nonzerorows 
            FROM T1 LEFT OUTER JOIN T2 ON T1.sku = T2.sku AND T1.customerid = T2.customerid AND T1.month = T2.month
            LEFT OUTER JOIN T3 ON T3.customerid = T1.customerid 
            INNER JOIN T4 ON T4.customerid = T3.customerid AND T3.invoiceid = T4.invoiceid"""
}

Map constructCustomerTransactionDetailsMap(List transactionRows, String sku) {
    Map customerTransactionDetailsMap = [:]
    if (transactionRows) {
        /*customerTransactionDetailsMap = transactionRows?.collectEntries { transactionRow ->
            [(transactionRow.customerid): transactionRow]
        }*/
        customerTransactionDetailsMap = transactionRows.groupBy({ row -> row.customerid })
        Integer totalQuantity = transactionRows.quantity.sum()
        BigDecimal totalRevenue = transactionRows.revenue.sum()
        BigDecimal totalCost = transactionRows.cost.sum()
        BigDecimal grossPriceAcross = transactionRows.grossprice.sum()
        Integer entryCount = transactionRows.size()
        boolean isTotalQuantityNonZero = totalQuantity != 0
        BigDecimal avgPrice = isTotalQuantityNonZero ? totalRevenue / totalQuantity : totalRevenue
        BigDecimal baseCost = isTotalQuantityNonZero ? totalCost / totalQuantity : totalCost
        //BigDecimal grossPrice = isTotalQuantityNonZero ? totalRevenue / totalQuantity : totalRevenue
        //entryCount ? grossPriceAcross / entryCount : grossPriceAcross

        Map allCustomerTransactionEntry = [sku             : sku,
                                           customerid      : Constants.ALL_CUSTOMER_ID,
                                           grossprice      : avgPrice,
                                           cost            : baseCost,
                                           avgprice        : avgPrice,
                                           revenue         : totalRevenue,
                                           quantity        : totalQuantity,
                                           nonzerorows     : transactionRows.nonzerorows.sum(),
                                           margin          : transactionRows.margin.sum(),
                                           customerbasecost: baseCost]
        customerTransactionDetailsMap.put(Constants.ALL_CUSTOMER_ID, allCustomerTransactionEntry)

        if (Constants.ADD_ROWS_WITH_0_REVENUE_AND_QUANTITY) {
            customerTransactionDetailsMap.put(Constants.CUSTOMER_ID_WITH_NO_TRANSACTION, createTransactionRow(out.SKU, Constants.CUSTOMER_ID_WITH_NO_TRANSACTION))
        }
        /*customerTransactionDetailsMap.iterator().with { iterator ->
            iterator.each { entry ->
                if (entry.value.customerid != Constants.ALL_CUSTOMER_ID && (entry.value.revenue < 0 || entry.value.margin < 0)) {
                    iterator.remove()
                }
            }
        }*/
    }
    return customerTransactionDetailsMap
}

protected Map createTransactionRow(String sku, String customerId, BigDecimal grossPrice = 0, BigDecimal baseCost = 0, BigDecimal avgPrice = 0, BigDecimal revenue = 0, Integer quantity = 0, Integer nonZeroRows = 0) {
    return transactionRow = [
            'sku'        : sku,
            'customerid' : customerId,
            'grossprice' : grossPrice,
            'cost'       : baseCost,
            'avgprice'   : avgPrice,
            'revenue'    : revenue,
            'quantity'   : quantity,
            'nonzerorows': nonZeroRows
    ]
}
//NOTE: This is used for debugging
/*
void verifyOutput(DatamartContext.Query query, DatamartContext ctx, String testingSource) {
    def results = ctx.executeQuery(query).getData().collect()
    //api.trace("results",results)
    //libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from ' + testingSource, api.jsonEncode(results))
}

 */
