api.global.targetRs = api.getDatamartRowSet("target")

api.global.printTrace = true

api.local.isNormal = false
api.local.isXArticle = false
api.local.isInternal = false

initializeConditionTablesMap()
initializeConditionNameMap()
initializeActiveConditionListMap()
initializeConditionFilterMap()
if (!api.global.dataFieldsDSMap) {
    buildDataSourceandPXQueryFields()
}

api.global.exchangeRate = api.findLookupTableValues("ExchangeRate")?.collectEntries {
    [(it.name): it.attribute1]
}
api.global.topco = api.findLookupTableValues("TopCoProducts")*.name as Set

/** initializeConditionNameMap
 *
 * @return
 */
protected initializeConditionNameMap() {
    List filters = [Filter.equal("attribute3", "Yes"),
                    Filter.in("attribute5", Constants.CONDITION_TYPES)
                    ]//,Filter.equal("name","ZRA")]

    api.global.conditionsList = api.findLookupTableValues("ConditionMaster", *filters).collectEntries { it ->
        Map row = [conditionName  : it.name,
                   pxTableName    : it.attribute1,
                   conditionTables: it.attribute2,
                   pricingType    : it.attribute4,
                   accessPXTable  : it.attribute6]
        [(it.name): row]
    }
}

/**initializeConditionTablesMap
 *
 * @return
 */
protected initializeConditionTablesMap() {
    api.global.conditionTables = api.findLookupTableValues("ConditionTableMaster", "-attribute1", Filter.equal("attribute2", "Yes"))?.collectEntries { it ->
        Map row = [
                tableName           : it.name,
                additionalKeyColumns: it.attribute1,
                articleFilter       : it.attribute2,
                pgFilter            : it.attribute4
        ]
        [(it.name): row]
    }
}

/**initializeActiveConditionListMap
 *
 * @return
 */
protected initializeActiveConditionListMap() {
    api.global.activeConditionsList = [:]
    api.global.conditionTables.each { conditionTableRow ->
        String tableName = conditionTableRow.value["tableName"]
        Map activeConditionList = api.global.conditionsList.findAll { it -> it.value["conditionTables"].contains(conditionTableRow.key) && it.value["accessPXTable"] == "Yes" }
        if (activeConditionList) api.global.activeConditionsList.put(tableName, activeConditionList?.keySet())
    }
}

/**initializeConditionFilterMap
 *
 * @return
 */
protected initializeConditionFilterMap() {
    api.global.conditionFilterMap = [:]
    api.global.activeConditionsList?.each { conditionTableName, activeConditions ->
        activeConditions.each { conditionName ->
            String pxTableName = api.global.conditionsList?.find { conditionListRow -> conditionListRow.key == conditionName }?.value["pxTableName"]
            List conditionFilter = [
                    Filter.equal("KOTAB", conditionTableName),
                    Filter.isNotNull('KBETR')
            ]
            if (!Constants.CONDITION_FETCH_FROM_DS.contains(conditionName)) conditionFilter.add(Filter.equal("name", pxTableName))
            if (api.global.conditionFilterMap[conditionTableName] == null) api.global.conditionFilterMap[conditionTableName] = [:]
            api.global.conditionFilterMap[conditionTableName].put(conditionName, conditionFilter)
        }
    }
}

protected buildDataSourceandPXQueryFields() {
    api.global.dataFieldsDSMap = [:]
    api.global.dataFieldsPXMap = [:]

    api.global.conditionTables.each { conditionTableRow ->
        String additionalFields = conditionTableRow.value["additionalKeyColumns"]
        List dsFields = Constants.DS_FIELDS.clone()
        List pxFields = Constants.PX_FIELDS.clone()
        if (additionalFields != null) {
            pxFields.addAll(additionalFields.split(","))
            dsFields.addAll(additionalFields.split(","))
        }
        api.global.dataFieldsDSMap.put(conditionTableRow.key, dsFields)
        api.global.dataFieldsPXMap.put(conditionTableRow.key, pxFields)
    }
}