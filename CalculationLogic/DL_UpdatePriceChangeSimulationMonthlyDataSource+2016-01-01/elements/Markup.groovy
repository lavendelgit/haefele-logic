if (null in [out.GrossPrice, out.BaseCost]) return null

return (out.GrossPrice == 0 || out.BaseCost == 0) ? null : ((out.GrossPrice - out.BaseCost) / out.BaseCost)