def effectiveYear = out.EffectiveYear
def verkaufsbuero = "All"
def branchCategory = "All"

def effectiveYearParam = "Effective Year"
def salesOfficeParam = "Verkaufsbuero"
def brancheCategoryParam = "Branche Category"

return api.dashboard("ZPAPAnalysisPerPGEmbeddedDash")
        .setParam(effectiveYearParam, effectiveYear)
        .setParam(salesOfficeParam, verkaufsbuero)
        .setParam(brancheCategoryParam, branchCategory)
        .showEmbedded()
        .andRecalculateOn(api.dashboardWideEvent("RefreshZPAPPGDetails"))
        .withEventDataAttr("ZA_Verkaufsbuero").asParam(salesOfficeParam)
        .withEventDataAttr("ZA_BrancheCategory").asParam(brancheCategoryParam)
