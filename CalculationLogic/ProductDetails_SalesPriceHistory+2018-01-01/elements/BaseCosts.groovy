def sku = api.product("sku")
def filters = [
        Filter.equal("name", "S_ZPE"),
        Filter.equal("sku", sku)
]
def orderBy = "attribute1" // Valid From (de: Gültig von) - ascending
def fields = [
        "attribute1", // Valid From
        "attribute3"  // Sales Price
]

return api.find("PX50", 0, 0, orderBy, fields, *filters) ?: []
