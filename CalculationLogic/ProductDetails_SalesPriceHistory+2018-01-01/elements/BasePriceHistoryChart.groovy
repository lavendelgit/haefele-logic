def baseCosts = api.getElement("BaseCosts");

def chartDef = [
        chart: [
            type: 'line'
        ],
        title: [
            text: 'Base Cost History'
        ],
        xAxis: [[
                type: 'datetime'
        ]],
        tooltip: [
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%Y-%m-%d}: {point.y:.2f}'
        ],
        series: [[
             name: 'Base Cost',
             data: baseCosts.collect{r -> [
                     api.parseDate("yyyy-MM-dd", r.attribute20),
                     r.attribute15?.toBigDecimal()
             ]}
        ]]
]

api.buildFlexChart(chartDef)