def salesPrices = api.getElement("SalesPrices");

def chartDef = [
        chart: [
            type: 'line'
        ],
        title: [
            text: 'Sales Price History'
        ],
        xAxis: [[
            type: 'datetime'
        ]],
        tooltip: [
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%Y-%m-%d}: {point.y:.2f}'
        ],
        series: [[
            name: 'Sales Price',
            data: salesPrices.collect{r -> [
                    api.parseDate("yyyy-MM-dd", r.attribute1),
                    r.attribute3?.toBigDecimal()
            ]}
        ]]
]

api.buildFlexChart(chartDef)