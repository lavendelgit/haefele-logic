def regularSalesPrice = api.getElement("RegularSalesPricePer100Units")
def focusGroupRebate = api.getElement("FocusGroupDiscount")

if (regularSalesPrice != null && focusGroupRebate != null) {
    return (regularSalesPrice * (1 - focusGroupRebate/100)).setScale(2, BigDecimal.ROUND_HALF_UP)
}

return "0"