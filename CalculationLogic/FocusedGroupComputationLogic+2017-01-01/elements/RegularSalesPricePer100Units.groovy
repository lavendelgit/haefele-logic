def sku = api.getElement("Material")
def invoiceDate = api.getElement("InvoiceDate")

if (api.global.salesPriceRecord?.sku == sku &&
    api.global.salesPriceRecord?.attribute1 <= invoiceDate &&
    api.global.salesPriceRecord?.attribute2 >= invoiceDate) {
    return api.global.salesPriceRecord.attribute3
} else {
    def salesPriceRecord = lib.Find.latestSalesPriceRecord(sku,
            Filter.lessOrEqual("attribute1", invoiceDate),
            Filter.greaterOrEqual("attribute2", invoiceDate))

    if (salesPriceRecord?.attribute3) {
        api.global.salesPriceRecord = salesPriceRecord
        return salesPriceRecord.attribute3
    }

    def compoundPriceRecord = lib.Find.latestCompoundPriceRecord(sku,
            Filter.lessOrEqual("attribute6", invoiceDate),
            Filter.greaterOrEqual("attribute7", invoiceDate))

    if (compoundPriceRecord?.attribute1) {
        return compoundPriceRecord?.attribute1
    }
}