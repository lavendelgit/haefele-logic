if (api.isSyntaxCheck()) return 0

def transactionsCount = api.getElement("LinearRegression")?.count
def minimumTransactionsCount = api.getElement("MinimumTransactionsCount")

if (transactionsCount < minimumTransactionsCount) {
    api.yellowAlert("Transactions count is less than minimum ({$transactionsCount} < ${minimumTransactionsCount})")
}

return transactionsCount;