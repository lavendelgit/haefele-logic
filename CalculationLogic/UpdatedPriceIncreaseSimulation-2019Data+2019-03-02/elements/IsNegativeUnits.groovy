def priceIncreaseImpact = api.getElement("PriceIncreaseImpact")

if (priceIncreaseImpact?.currentUnits == null || priceIncreaseImpact?.newUnits == null) {
    return null
}

return priceIncreaseImpact.currentUnits < 0 || priceIncreaseImpact.newUnits < 0