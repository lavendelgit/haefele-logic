def samePriceTolerance = api.getElement("SamePriceTolerance")
def linearRegression = api.getElement("LinearRegression")

def avgPrice = linearRegression?.avgPrice
def minPrice = linearRegression?.minPrice
def maxPrice = linearRegression?.maxPrice

if (avgPrice == null || minPrice == null || maxPrice == null) {
    return null
}

if (avgPrice == 0) {
    return null
}

def lowerDiff = (avgPrice - minPrice) / avgPrice
def upperDiff = (maxPrice - avgPrice) / avgPrice

api.trace("Avg Price ("+avgPrice+") min Price (" +minPrice+") MaxPrice ("+maxPrice+")")
api.trace("lowerDiff", "", lowerDiff)
api.trace("upperDiff", "", upperDiff)

return lowerDiff < (samePriceTolerance/2.0) && upperDiff < (samePriceTolerance/2.0)