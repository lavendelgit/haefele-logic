def sku = api.product("sku")
def supplierId = api.product("attribute8")

def isSupplierExcluded = (api.global.supplierMaterialToExcludeSalesPriceIncrease[supplierId] != null)
def isSpecificMaterialExcluded = (api.global.materialToExcludeSalesPriceIncrease[sku]?.increaseSalesPrice == "Yes") 

return ( isSpecificMaterialExcluded || isSupplierExcluded) ? true: false
