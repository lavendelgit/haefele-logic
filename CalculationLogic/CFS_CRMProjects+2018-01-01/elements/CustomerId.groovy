package CFS_CRMProject.elements

def customerId = api.getElement("CurrentItem")?.attribute5

customerId = customerId?.replaceAll(/[^0-9]/, "") // remove description

return customerId?.padLeft(10, "0")