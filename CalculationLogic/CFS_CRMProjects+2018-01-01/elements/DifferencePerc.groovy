package CFS_CRMProject.elements

def unitSalesPrice = api.getElement("UnitSalesPrice")
def differenceAbs = api.getElement("DifferenceAbs")

if (unitSalesPrice != null && unitSalesPrice != 0 && differenceAbs != null) {
    return differenceAbs / unitSalesPrice
}
