package CFS_CRMProject.elements

def currentItem = api.currentItem()


if (currentItem == null) {
    currentItem = (api.productExtension("CRMProject") ?: [[:]]).first() // only for test runs
}

return currentItem