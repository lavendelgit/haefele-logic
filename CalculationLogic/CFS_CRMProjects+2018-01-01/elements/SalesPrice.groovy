package CFS_CRMProject.elements

def salesPrice = api.getElement("SalesPriceRecord")?.salesPrice

if (salesPrice == null) {
    api.local.warning = (api.local.warning ?: "") +  "Sales price is missing. "
}

return salesPrice