package CFS_CRMProject.elements

def salesPriceRecord = api.getElement("SalesPriceRecord")

def salesPrice = salesPriceRecord?.salesPrice
def priceUnits = salesPriceRecord?.priceUnits

if (salesPrice != null && priceUnits != null && priceUnits != 0) {
    return salesPrice / priceUnits
}