package CFS_CRMProject.elements

def sku = api.getElement("CurrentItem")?.sku

return lib.Find.currentSalesPriceAnyType(sku)