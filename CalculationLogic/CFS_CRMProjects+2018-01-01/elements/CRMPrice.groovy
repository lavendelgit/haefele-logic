package CFS_CRMProject.elements

def quantity = api.getElement("Quantity")
def revenue = api.getElement("Revenue")

if (!quantity || revenue == null) {
    return
}

return revenue / quantity