package CFS_CRMProject.elements

def crmPrice = api.getElement("CRMPrice")
def unitSalesPrice = api.getElement("UnitSalesPrice")

if (crmPrice != null && unitSalesPrice != null) {
    return unitSalesPrice - crmPrice
}