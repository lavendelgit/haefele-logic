def previousYear = out.PreviousYear
List filters = [Filter.greaterOrEqual("InvoiceDateYear", previousYear)] +
                libs.__LIBRARY__.HaefeleSpecific.getTransactionInlandFilters()
Map field = ['InvoiceDateYear'     : 'Year',
             'SUM(Revenue)'        : 'Revenue',
             'SUM(MarginAbs)'      : 'Margin',
             'SUM(InvoiceQuantity)': 'Quantity',
             'SUM(Cost)'           : 'PurchasePrice',
]
return lib.DBQueries.getDataFromSource(field, 'TransactionsDM', filters, 'DataMart').value