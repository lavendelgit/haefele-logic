return api.findLookupTableValues("Working_Days_In_Deutschland")?.collect {
    [("MonthDate"): (it.name),
     ("2021")     : (it.attribute1),
     ("2022")     : (it.attribute2),
     ("2023")     : (it.attribute3),
     ("2024")     : (it.attribute4),
     ("2025")     : (it.attribute5)]
}

