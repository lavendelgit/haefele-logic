List simulationSummary = out.GetSimulationSummary.collect {
    ["name"      : it.Year,
     "attribute1": it.Revenue,
     "attribute2": it.Margin,
     "attribute3": it.Quantity,
     "attribute4": it.PurchasePrice]
}
Object workingDays = out.workingDays
String currentYear = out.CurrentYear
Map recordToInsert
for (row in simulationSummary) {
    switch (row.name) {
        case out.PreviousYear:
            recordToInsert = row
            break
        case out.CurrentYear:
            String analysisDay = out.AnalysisDate.format("MM-dd")
            Calendar currentDate = api.calendar()
            currentDate.set(Calendar.MONTH, 11)
            currentDate.set(Calendar.DAY_OF_MONTH, 31)
            String lastDay = currentDate.getTime().format("MM-dd")
            def currentWorkingDay = workingDays?.find { it.MonthDate == analysisDay }[currentYear]
            def totalWorkingDays = workingDays?.find { it.MonthDate == lastDay }[currentYear]
            recordToInsert = ["name"      : row.name,
                              "attribute1": row.attribute1 * totalWorkingDays / currentWorkingDay,
                              "attribute2": row.attribute2 * totalWorkingDays / currentWorkingDay,
                              "attribute3": row.attribute3 * totalWorkingDays / currentWorkingDay,
                              "attribute4": row.attribute4 * totalWorkingDays / currentWorkingDay]
            break
        default:
            api.throwException("Not Expected to Execute" + row.name)
    }
    libs.__LIBRARY__.DBQueries.insertIntoPPTable(recordToInsert, 'Simulation_Precalculated_Summary', 'MLTV')
}
