def kundeKlasse = api.getElement("Kunde_Klasse")
def rabattgruppe = api.getElement("Rabattgruppe")
def verkaufsbereich3 = api.getElement("Verkaufsbereich3")
def key = "${kundeKlasse}-${rabattgruppe}-${verkaufsbereich3}"

api.trace("key", null, key)
api.trace("api.global.regionalrabattCache", null, api.global.regionalrabattCache)

def cache = api.global.regionalrabattCache
def result = cache.get(key)

if (result == null) {
  result = api.findLookupTableValues("Regionalrabatt", api.filter("key2", kundeKlasse)
                                     , api.filter("key3", rabattgruppe),
                                     , api.filter("key1", verkaufsbereich3))
  if (result != null) {
    if (result.isEmpty()) {
      result = null
    } else {
      api.trace("result[0]", null, result[0])
      def key1 = result[0].key1
      def key2 = result[0].key2
      def key3 = result[0].key3
      /*
      api.trace("key1-key2-key3", null, key1 + "-" + key2 + "-" + key3)
      api.trace("key1", null, (key1?.equalsIgnoreCase(verkaufsbereich3)))
      api.trace("key2", null, (key2?.equalsIgnoreCase(kundeKlasse)))
      api.trace("key3", null, (key3?.equalsIgnoreCase(rabattgruppe)))
      */
	  if (key1?.equalsIgnoreCase(verkaufsbereich3) && key2?.equalsIgnoreCase(kundeKlasse) && key3?.equalsIgnoreCase(rabattgruppe)) {
        result = result[0].attribute1 as BigDecimal
      } else {
        result = null
      }
    }
  }
  if (result == null) {
    result = -1;
  }
  cache.put(key, result)
}

api.trace("api.global.regionalrabattCache", null, api.global.regionalrabattCache)
if (result == -1) return null
result