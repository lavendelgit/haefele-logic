def kundeKlasse = api.getElement("Kunde_Klasse")
def rabattgruppe = api.getElement("Rabattgruppe")
def key = "${kundeKlasse}-${rabattgruppe}"

api.trace("key", null, key)
api.trace("api.global.fokusrabattCache", null, api.global.fokusrabattCache)

def cache = api.global.fokusrabattCache
def result = cache.get(key)

if (result == null) {
  result = api.findLookupTableValues("Fokusrabatt", api.filter("key1", kundeKlasse), api.filter("key2", rabattgruppe))
  if (result != null) {
    if (result.isEmpty()) {
      result = null
    } else {
       api.trace("result[0]", null, result[0])
      def key1 = result[0].key1
      def key2 = result[0].key2
      api.trace("key1-key2", null, key1 + "-" + key2)
      api.trace("key1", null, (key1?.equalsIgnoreCase(kundeKlasse)))
      api.trace("key2", null, (key2?.equalsIgnoreCase(rabattgruppe)))
	  if (key1?.equalsIgnoreCase(kundeKlasse) && key2?.equalsIgnoreCase(rabattgruppe)) {
      	result = result[0].attribute1 as BigDecimal
      } else {
        result = null
      }
    }
  }
  if (result == null) {
    result = -1;
  }
  cache.put(key, result)
}

api.trace("api.global.fokusrabattCache", null, api.global.fokusrabattCache)
if (result == -1) return null
result