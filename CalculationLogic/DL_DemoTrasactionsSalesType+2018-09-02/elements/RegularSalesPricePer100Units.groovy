def sku = api.getElement("ArticleNumber")
def transactionDate = api.getElement("TransactionDate")

if (api.global.salesPriceRecord?.sku == sku &&
    api.global.salesPriceRecord?.attribute1 <= transactionDate &&
    api.global.salesPriceRecord?.attribute2 >= transactionDate) {
    return api.global.salesPriceRecord.attribute3
} else {
    def salesPriceRecord = lib.Find.latestSalesPriceRecord(sku,
            Filter.lessOrEqual("attribute1", transactionDate),
            Filter.greaterOrEqual("attribute2", transactionDate))

    if (salesPriceRecord?.attribute3) {
        api.global.salesPriceRecord = salesPriceRecord
        return salesPriceRecord.attribute3
    }

    def compoundPriceRecord = lib.Find.latestCompoundPriceRecord(sku,
            Filter.lessOrEqual("attribute6", transactionDate),
            Filter.greaterOrEqual("attribute7", transactionDate))

    if (compoundPriceRecord?.attribute1) {
        return compoundPriceRecord?.attribute1
    }
}