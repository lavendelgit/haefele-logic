def sku = api.getElement("ArticleNumber")
def customerId = api.getElement("CustomerId")
def transactionDate = api.getElement("TransactionDate")

def customerSalesPriceRecord = lib.Find.latestCustomerSalesPriceRecord(sku,
    Filter.equal("attribute4", customerId),
    Filter.lessOrEqual("attribute1", transactionDate),
    Filter.greaterOrEqual("attribute2", transactionDate))


if (customerSalesPriceRecord != null && !customerSalesPriceRecord.isEmpty())
return [
    sku: customerSalesPriceRecord?.sku,
    customerId: customerSalesPriceRecord?.attribute4,
    validFrom: customerSalesPriceRecord?.attribute1,
    validTo: customerSalesPriceRecord?.attribute2,
    price: customerSalesPriceRecord?.attribute3
]