def sku = api.getElement("ArticleNumber")
def customerId = api.getElement("CustomerId")
def focusGroup = api.getElement("FocusGroup")
def transactionDate = api.getElement("TransactionDate")
def lookBackDays = api.getElement("SalesPersonDiscountLookbackDays")
def dateToFilter 

if (transactionDate)
	dateToFilter = Date.parse("yyyy-MM-dd", transactionDate)?.minus(lookBackDays)?.format("yyyy-MM-dd")
else
	dateToFilter = new Date().minus(lookBackDays)?.format("yyyy-MM-dd")
api.trace("Printing the date and difference is indeed 400 days ", "(ToFilter "+dateToFilter +") Other date"+transactionDate)

if (api.global.salesPersonDiscounts == null) {
    def salesPersonDiscounts = [:]
  	
  	def attributes = [
      "attribute4",
      "sku",
      "attribute1",
      "attribute2",
      "attribute19",
      "attribute3"
    ]
  
  	def filters = [
      Filter.equal("name", "S_ZRC"),
      Filter.greaterOrEqual("attribute19", dateToFilter)
    ]
  	
    def stream = api.stream("PX50", "attribute4", attributes, *filters)
    if (!stream) {
        return []
    }

    while (stream.hasNext()) {
        def record = stream.next()
        def customerDiscounts = salesPersonDiscounts[record.attribute2] ?: []
        customerDiscounts.add([
                customerId: record.attribute2,
                sku: record.sku,
                focusGroup: record.attribute3,
                validFrom: record.attribute18,
                validTo: record.attribute19,
                discount: record.attribute13
        ])
        salesPersonDiscounts[record.attribute2] = customerDiscounts
    }

    stream.close()

    api.global.salesPersonDiscounts = salesPersonDiscounts
   // api.trace("cx",salesPersonDiscounts)
}

def discountsForThisCustomer = api.global.salesPersonDiscounts[customerId]
api.trace("discountsForThisCustomer", "", discountsForThisCustomer)
def discountAmount = discountsForThisCustomer
        ?.find { spd -> (spd.sku == sku || (spd.focusGroup != null && spd.focusGroup == focusGroup)) /* &&
                         spd.validFrom <= transactionDate &&
                         spd.validTo >= transactionDate */}
        ?.discount
api.trace("discount",discountAmount)
discountAmount = (discountAmount) ? new BigDecimal(discountAmount).abs().multiply(100) : 0

return discountAmount