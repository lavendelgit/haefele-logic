def transactionDate = api.getElement("InvoiceDate")
if (transactionDate instanceof java.util.Date) {
    transactionDate = transactionDate.format("yyyy-MM-dd")
}

return transactionDate