if (api.isSyntaxCheck()) {
    return
}

def sku = api.getElement("ArticleNumber")
def customerPotentialGroup = api.getElement("CustomerPotentialGroup")?:""
def salesPricePer100Units = api.getElement("SalesPricePer100Units") as BigDecimal

def regularSalesPricePer100Units = api.getElement("RegularSalesPricePer100Units") as BigDecimal
def focusGroupDiscount = api.getElement("FocusGroupDiscount")
def salesPersonDiscount = api.getElement("SalesPersonDiscount") ?: 10.0
def crmProject = api.getElement("CRMProject")

def invoiceDiscount = 5.0
def zrjs = [25.6, 17.0, 13.0]
def customerPotentialDiscounts = Util.customerPotentialDiscounts()

api.local.salesTypeDetails = ""
api.local.debug = ""

if (crmProject?.pricePer100Units != null) {
    api.local.salesTypeDetails = "CRM Nummer"
    return "P44"
}

if (sku?.endsWith("X")) {
    api.local.salesTypeDetails = "X Artikel"
    return "P95"
}

if (salesPricePer100Units == null) {
    api.logWarn("[SalesType] SalesPricePer100Units is not specified", "")
    return
}

if ((!regularSalesPricePer100Units) && api.getElement("TopCoProducts")?.contains(sku)) {
    api.local.salesTypeDetails = "TopCo Artikel"
    return "P96"
}

def applyDiscount(BigDecimal price, BigDecimal ... discountPercentage) {
    if (price == null) {
        return null
    }
    def result = price;
    discountPercentage.each { d ->
        result = (result * (1 - d / 100.0)).setScale(2, BigDecimal.ROUND_HALF_UP)
      	api.trace ("xxxxxxxxx ("+price+")", "result ("+result+") d("+d+")" )
    }
    return result
}

api.trace ("The values are+++++++++++++++++++", " regularSalesPricePer100Units("+regularSalesPricePer100Units+") salesPersonDiscount ("+salesPersonDiscount+") invoiceDiscount("+invoiceDiscount+")")


def prices = [
        P1:  regularSalesPricePer100Units,                                                          // Grundpreis (R0)
        P2:  applyDiscount(regularSalesPricePer100Units, invoiceDiscount),                          // R0 + Invoice discount (5%)
        P3:  applyDiscount(regularSalesPricePer100Units, salesPersonDiscount),                      // R0 + Sales person discount (10%)
        P4:  applyDiscount(regularSalesPricePer100Units, salesPersonDiscount, invoiceDiscount),     // R0 + Sales person discount (10%) + Invoice discount (5%)
        P5:  null, // R1 (8%)
        P7:  null, // R1 (8%) + Sales person discount (10%)
        P8:  null, // R1 (8%) + Sales person discount (10%) + Invoice discount (5%)
        P9:  null, // R2 (13%)
        P10: null, // R2 (13%) + Invoice discount (5%)
        P11: null, // R2 (13%) + Sales person discount (10%)
        P12: null, // R2 (13%) + Sales person discount (10%) + Invoice discount (5%)
        P13: null, // R3 (18%)
        P14: null, // R3 (18%) + Invoice discount (5%)
        P15: null, // R3 (18%) + Sales person discount (10%)
        P16: null, // R3 (18%) + Sales person discount (10%) + Invoice discount (5%)
        P17: null, // Clearing Rabatt (CR or ZRCL)
        P18: null, // CR + 5%
        P19: null, // CR + 10%
        P20: null, // CR + 5% + 10%
        P21: null, // CR + R1 (8%)
        P22: null, // CR + R1 (8%) + 5%
        P23: null, // CR + R1 (8%) + 10%
        P24: null, // CR + R1 (8%) + 5% + 10%
        P25: null, // CR + R2 (13%)
        P26: null, // CR + R2 (13%) + 5%
        P27: null, // CR + R2 (13%) + 10%
        P28: null, // CR + R2 (13%) + 5% + 10%
        P29: null, // CR + R3 (18%)
        P30: null, // CR + R3 (18%) + 5%
        P31: null, // CR + R3 (18%) + 10%
        P32: null, // CR + R3 (18%) + 5% + 10%
        P33: api.getElement("CustomerSalesPrice")?.price, // Customer sales prices (ZPAP)
        P34: null, // Fokusgruppenrabatt (ZRPG)
        P35: null, // ZRPG + 5%
        P36: null, // ZRPG + ZRC
        P37: null, // ZRPG + ZRC + 5%
        P38: null, // ZRPG + ZRCL
        P39: null, // ZRPG + ZRCL + 5%
        P40: null, // ZRPG + ZRCL + ZRC
        P41: null, // ZRPG + ZRCL + ZRC + 5%
        P42: null, // Nettopreise (ZPZ)
        P43: null, // ZPZ + 5%
        P44: crmProject?.pricePer100Units, // CRM Nummer
        P45: api.getElement("OccasionPrice")?.price, // Occasionspreis (ZPS)
        P46: api.getElement("PromotionalSpecialPrice")?.price, // Aktions-Sonderpreis (ZPC)
        P47: api.getElement("FixedPrice")?.price // Fixpreis (ZPF)
    /*
        P48: P1 + ZRJ
        P44: P2 + ZRJ
        ...
        P94: P43 + ZRJ
    */

]

if (customerPotentialGroup && customerPotentialGroup == "R1") {
    prices.P5 = applyDiscount(regularSalesPricePer100Units, 8)
    prices.P6 = applyDiscount(regularSalesPricePer100Units, 8, invoiceDiscount)
    prices.P7 = applyDiscount(regularSalesPricePer100Units, 8, salesPersonDiscount)
    prices.P8 = applyDiscount(regularSalesPricePer100Units, 8, salesPersonDiscount, invoiceDiscount)
}

if (customerPotentialGroup && customerPotentialGroup == "R2") {
    prices.P9  = applyDiscount(regularSalesPricePer100Units, 13)
    prices.P10 = applyDiscount(regularSalesPricePer100Units, 13, invoiceDiscount)
    prices.P11 = applyDiscount(regularSalesPricePer100Units, 13, salesPersonDiscount)
    prices.P12 = applyDiscount(regularSalesPricePer100Units, 13, salesPersonDiscount, invoiceDiscount)
}

if (customerPotentialGroup && customerPotentialGroup == "R3") {
    prices.P13 = applyDiscount(regularSalesPricePer100Units, 18)
    prices.P14 = applyDiscount(regularSalesPricePer100Units, 18, invoiceDiscount)
    prices.P15 = applyDiscount(regularSalesPricePer100Units, 18, salesPersonDiscount)
    prices.P16 = applyDiscount(regularSalesPricePer100Units, 18, salesPersonDiscount, invoiceDiscount)
}


def clearingRebateRecord = api.getElement("ClearingRebate")
def clearingRebate = (clearingRebateRecord?.attribute4 as BigDecimal)?.negate()?.multiply(100)

if (clearingRebate != null) {
    prices.P17 = applyDiscount(regularSalesPricePer100Units, clearingRebate)
    prices.P18 = applyDiscount(regularSalesPricePer100Units, clearingRebate, invoiceDiscount)
    prices.P19 = applyDiscount(regularSalesPricePer100Units, clearingRebate, salesPersonDiscount)
    prices.P20 = applyDiscount(regularSalesPricePer100Units, clearingRebate, invoiceDiscount, salesPersonDiscount)

    if (customerPotentialGroup && customerPotentialGroup == "R1") {
        prices.P21 = applyDiscount(regularSalesPricePer100Units, 8, clearingRebate)
        prices.P22 = applyDiscount(regularSalesPricePer100Units, 8, clearingRebate, invoiceDiscount)
        prices.P23 = applyDiscount(regularSalesPricePer100Units, 8, clearingRebate, salesPersonDiscount)
        prices.P24 = applyDiscount(regularSalesPricePer100Units, 8, clearingRebate, invoiceDiscount, salesPersonDiscount)
    }

    if (customerPotentialGroup && customerPotentialGroup == "R2") {
        prices.P25 = applyDiscount(regularSalesPricePer100Units, 13, clearingRebate)
        prices.P26 = applyDiscount(regularSalesPricePer100Units, 13, clearingRebate, invoiceDiscount)
        prices.P27 = applyDiscount(regularSalesPricePer100Units, 13, clearingRebate, salesPersonDiscount)
        prices.P28 = applyDiscount(regularSalesPricePer100Units, 13, clearingRebate, invoiceDiscount, salesPersonDiscount)
    }

    if (customerPotentialGroup && customerPotentialGroup == "R3") {
        prices.P29 = applyDiscount(regularSalesPricePer100Units, 18, clearingRebate)
        prices.P30 = applyDiscount(regularSalesPricePer100Units, 18, clearingRebate, invoiceDiscount)
        prices.P31 = applyDiscount(regularSalesPricePer100Units, 18, clearingRebate, salesPersonDiscount)
        prices.P32 = applyDiscount(regularSalesPricePer100Units, 18, clearingRebate, invoiceDiscount, salesPersonDiscount)
    }
}

if (focusGroupDiscount != null) {
    prices.P34 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount)
    prices.P35 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, invoiceDiscount)
    prices.P36 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, salesPersonDiscount)
    prices.P37 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, invoiceDiscount, salesPersonDiscount)
}

if (clearingRebate != null && focusGroupDiscount != null) {
    prices.P38 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, clearingRebate)
    prices.P39 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, clearingRebate, invoiceDiscount)
    prices.P40 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, clearingRebate, salesPersonDiscount)
    prices.P41 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, clearingRebate, invoiceDiscount, salesPersonDiscount)
}

def netPriceRecord = api.getElement("NetPrice")
def netPrice = netPriceRecord?.attribute3 as BigDecimal
api.trace("netPriceRecord", "", netPriceRecord)

prices.P42 = netPrice
prices.P43 = applyDiscount(netPrice, invoiceDiscount)

api.trace("prices", "", prices)

prices = prices.sort { e1, e2 -> (e2.key.substring(1) as Integer) <=> (e1.key.substring(1) as Integer) }

def result = Util.findPrice(prices, salesPricePer100Units);
if (result == null) {
    for (zrj in zrjs) {
        def pricesZRJ = (1..47).collectEntries { num ->
            return  [("P" + (47 + num)) : applyDiscount(prices[("P" + num)], zrj)]
        }

        result = Util.findPrice(pricesZRJ, salesPricePer100Units)
        if (result) {
            result.zrj = zrj
            break
        }
    }
}

if (result == null) {
    def priceTypePerPotentialGroup = [
        R0: [ "P2",  "P4", "P18", "P20"],
        R1: [ "P6",  "P8", "P22", "P24"],
        R2: ["P10", "P12", "P26", "P28"],
        R3: ["P14", "P16", "P30", "P32"],
    ]
    def potentialGroupDiscount = customerPotentialDiscounts[customerPotentialGroup]?:0.0
    def tolerance = salesPricePer100Units * 0.005 // 0.5%
  	def salesType
    for (invoiceDiscount = 0.0; invoiceDiscount <= 10.0; invoiceDiscount += 1) {

        if (customerPotentialGroup) {
            prices[priceTypePerPotentialGroup[customerPotentialGroup][0]] = applyDiscount(regularSalesPricePer100Units, potentialGroupDiscount, invoiceDiscount)
            prices[priceTypePerPotentialGroup[customerPotentialGroup][1]] = applyDiscount(regularSalesPricePer100Units, potentialGroupDiscount, salesPersonDiscount, invoiceDiscount)

            if (clearingRebate != null) {
                prices[priceTypePerPotentialGroup[customerPotentialGroup][2]] = applyDiscount(regularSalesPricePer100Units, clearingRebate, invoiceDiscount)
                prices[priceTypePerPotentialGroup[customerPotentialGroup][3]] = applyDiscount(regularSalesPricePer100Units, clearingRebate, invoiceDiscount, salesPersonDiscount)
            }
        }

        if (focusGroupDiscount != null) {
            prices.P35 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, invoiceDiscount)
            prices.P37 = applyDiscount(regularSalesPricePer100Units, focusGroupDiscount, invoiceDiscount, salesPersonDiscount)
        }

        salesType = prices.find { p -> Util.equalWithTolerance(p.value, salesPricePer100Units, tolerance) }?.key

        api.trace("prices", "" + invoiceDiscount, prices.clone())

        if (salesType != null) {
            result = [
                salesType: salesType,
                foundBy: "invoiceDiscountLoop",
                matchedPrice: prices[salesType]
            ]

            break;
        }
    }
}


if (result != null) {
    def salesTypeDetails = [
        P1:  "Grundpreis (R0)",
        P2:  "R0 + Invoice discount (${invoiceDiscount}%)",
        P3:  "R0 + ZRC (${salesPersonDiscount}%)",
        P4:  "R0 + ZRC (${salesPersonDiscount}%) + Invoice discount (${invoiceDiscount}%)",
        P5:  "R1 (8%)",
        P6:  "R1 (8%) + Invoice discount (${invoiceDiscount}%)",
        P7:  "R1 (8%) + ZRC (${salesPersonDiscount}%)",
        P8:  "R1 (8%) + ZRC (${salesPersonDiscount}%) + Invoice discount (${invoiceDiscount}%)",
        P9:  "R2 (13%)",
        P10: "R2 (13%) + Invoice discount (${invoiceDiscount}%)",
        P11: "R2 (13%) + ZRC (${salesPersonDiscount}%)",
        P12: "R2 (13%) + ZRC (${salesPersonDiscount}%) + Invoice discount (${invoiceDiscount}%)",
        P13: "R3 (18%)",
        P14: "R3 (18%) + Invoice discount (${invoiceDiscount}%)",
        P15: "R3 (18%) + ZRC (${salesPersonDiscount}%)",
        P16: "R3 (18%) + ZRC (${salesPersonDiscount}%) + Invoice discount (${invoiceDiscount}%)",
        P17: "Clearing Rabatt (ZRCL)",
        P18: "ZRCL (${clearingRebate}%) + ${invoiceDiscount}%",
        P19: "ZRCL (${clearingRebate}%) + ZRC (${salesPersonDiscount}%)",
        P20: "ZRCL (${clearingRebate}%) + ${invoiceDiscount}% + ZRC (${salesPersonDiscount}%)",
        P21: "ZRCL (${clearingRebate}%) + R1 (8%)",
        P22: "ZRCL (${clearingRebate}%) + R1 (8%) + ${invoiceDiscount}%",
        P23: "ZRCL (${clearingRebate}%) + R1 (8%) + ZRC (${salesPersonDiscount}%)",
        P24: "ZRCL (${clearingRebate}%) + R1 (8%) + ZRC (${salesPersonDiscount}%) + ${invoiceDiscount}%",
        P25: "ZRCL (${clearingRebate}%) + R2 (13%)",
        P26: "ZRCL (${clearingRebate}%) + R2 (13%) + ${invoiceDiscount}%",
        P27: "ZRCL (${clearingRebate}%) + R2 (13%) + ZRC (${salesPersonDiscount}%)",
        P28: "ZRCL (${clearingRebate}%) + R2 (13%) + ZRC (${salesPersonDiscount}%) + ${invoiceDiscount}%",
        P29: "ZRCL (${clearingRebate}%) + R3 (18%)",
        P30: "ZRCL (${clearingRebate}%) + R3 (18%) + ${invoiceDiscount}%",
        P31: "ZRCL (${clearingRebate}%) + R3 (18%) + ZRC (${salesPersonDiscount}%)",
        P32: "ZRCL (${clearingRebate}%) + R3 (18%) + ZRC (${salesPersonDiscount}%) + ${invoiceDiscount}%",
        P33: "ZPAP",
        P34: "ZRPG (${focusGroupDiscount}%)",
        P35: "ZRPG (${focusGroupDiscount}%) + ${invoiceDiscount}%",
        P36: "ZRPG (${focusGroupDiscount}%) + ZRC (${salesPersonDiscount}%)",
        P37: "ZRPG (${focusGroupDiscount}%) + ZRC (${salesPersonDiscount}%) + ${invoiceDiscount}%",
        P38: "ZRPG (${focusGroupDiscount}%) + ZRCL (${clearingRebate}%)",
        P39: "ZRPG (${focusGroupDiscount}%) + ZRCL (${clearingRebate}%) + ${invoiceDiscount}%",
        P40: "ZRPG (${focusGroupDiscount}%) + ZRCL (${clearingRebate}%) + ZRC (${salesPersonDiscount}%)",
        P41: "ZRPG (${focusGroupDiscount}%) + ZRCL (${clearingRebate}%) + ZRC (${salesPersonDiscount}%) + ${invoiceDiscount}%",
        P42: "ZPZ",
        P43: "ZPZ + ${invoiceDiscount}%",
        P44: "CRM Nummer",
        P45: "Occasionspreis (ZPS)",
        P46: "Aktions-Sonderpreis (ZPC)",
        P47: "Fixpreis (ZPF)"
    ]
    def salesTypeDetailsZRJ = (1..47).collectEntries { num ->
        return  [("P" + (47 + num)) : salesTypeDetails[("P" + num)] + " + ZRJ (${result.zrj}%)"]
    }
    salesTypeDetails.putAll(salesTypeDetailsZRJ)
    api.trace("salesTypeDetails","",salesTypeDetails)
    api.local.salesTypeDetails += salesTypeDetails[result.salesType]
    api.local.debug += "Calculated Price: ${result.matchedPrice} Diff: ${new BigDecimal(result.matchedPrice - salesPricePer100Units).abs()} Found by: ${result.foundBy}"
    return result.salesType
} else {
    api.local.salesTypeDetails = "ZPM/ZRJ"
    return "P97"
}

