def focusGroup = api.getElement("FocusGroup")
def customerGroup = api.getElement("CustomerPotentialGroup")?:""
def salesOffice = api.getElement("SalesOffice")
def transactionDate = api.getElement("TransactionDate")
if (api.global.focusGroupDiscount == null) {
    api.global.focusGroupDiscount = api.find("PX50",Filter.equal("name","S_ZRPG"))
                                       .collect { fgd -> [
                                            salesOffice: fgd.attribute6,
                                            focusGroup: fgd.attribute7,
                                            customerGroup: fgd.attribute8,
                                            validFrom: fgd.attribute1,
                                            validTo: fgd.attribute2,
                                            discount: fgd.attribute3
                                       ]}
                                       .groupBy { fgd -> fgd.focusGroup }

    api.trace("api.global.focusGroupDiscount", "", api.global.focusGroupDiscount)
}

return api.global.focusGroupDiscount[focusGroup]
                ?.find { fgd -> fgd.salesOffice == salesOffice &&
                        fgd.customerGroup == customerGroup &&
                        fgd.validFrom <= transactionDate &&
                        fgd.validTo >= transactionDate }
                ?.discount
                ?.multiply(100)
                ?.abs()

