def customerPotentialGroup = api.getElement("CustomerPotentialGroup")

if (!customerPotentialGroup)
	return 0.0;

return Util.customerPotentialDiscounts()[customerPotentialGroup]?.div(100.0) ?: 0.0
