def sku = api.getElement("ArticleNumber")
def transactionDate = api.getElement("TransactionDate")

lib.Find.latestNetPriceRecord(sku,
        Filter.lessOrEqual("attribute1", transactionDate),
        Filter.greaterOrEqual("attribute2", transactionDate))