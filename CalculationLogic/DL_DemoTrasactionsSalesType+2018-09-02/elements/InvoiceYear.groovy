def invoiceYear = api.getElement("InvoiceDate")

if (invoiceYear) {
  invoiceYear = invoiceYear.format("yyyy")
}

return invoiceYear