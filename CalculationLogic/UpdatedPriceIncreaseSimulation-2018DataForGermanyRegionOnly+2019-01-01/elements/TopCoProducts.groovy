if (!api.global.topCoProductToExcludeSalesPriceIncrease) {
    api.global.topCoProductToExcludeSalesPriceIncrease = api.findLookupTableValues("TopCoProducts")
                                          .collectEntries { pp -> [ (pp.name) : [
                                                materialId: pp.name
                                          ]]}
}

api.trace("api.global.topCoProductToExcludeSalesPriceIncrease","",api.global.topCoProductToExcludeSalesPriceIncrease)

return api.global.topCoProductToExcludeSalesPriceIncrease