def linReg = api.getElement("LinearRegression")
def confidenceInterval = api.getElement("ConfidenceInterval")

lib.LinearRegression.infoMatrix(linReg, confidenceInterval)