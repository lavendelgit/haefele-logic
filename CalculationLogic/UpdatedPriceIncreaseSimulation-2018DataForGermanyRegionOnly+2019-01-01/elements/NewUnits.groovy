def transactionsCount = api.getElement("TransactionsCount")
def minimumTransactionsCount = api.getElement("MinimumTransactionsCount")
def isSamePrice = api.getElement("IsSamePrice")
def unitsSold = api.getElement("UnitsSold")
def isNegativeUnits = api.getElement("IsNegativeUnits")
def isMaterialExcludedFromSalesPrice = api.getElement("IsMaterialShieldedFromPriceRise")


if (api.product("sku").startsWith("999")) {
    api.yellowAlert("Internal article (999). Returning units from last year.")
    return unitsSold
}

if (!api.getElement("IsGoodRegression")) {
    api.yellowAlert("Linear regression is not good fit. Returning units from last year.")
    return unitsSold
}

if (api.product("sku").endsWith("X")) {
    api.yellowAlert("X Article. Returning units from last year.")
    return unitsSold
}

if (transactionsCount < minimumTransactionsCount) {
    api.yellowAlert("Too few transactions. Ignoring linear regression and returning units from last year.")
    return unitsSold
}

if (isSamePrice) {
    api.yellowAlert("Invoice prices are (nearly) the same. Ignoring linear regression and returning units from last year.")
    return unitsSold
}

if (isNegativeUnits) {
    api.yellowAlert("Negative units from linear regression. Returning units from last year.")
    return unitsSold
}

if (api.getElement("IsExtremeImpact")) {
    api.yellowAlert("The increase would cause an extreme impact (" +
             api.formatNumber("#.##%", api.getElement("PriceIncreaseImpact").expectedImpactPerc) + "). " +
            "Returning units from last year.")
    return unitsSold
}

if (isMaterialExcludedFromSalesPrice) {
	api.yellowAlert("Material not affected by sales price increase hence returning units from last year.");
    return unitsSold
}

def isTopCoProduct = api.getElement("IsTopCoProduct")
if (isTopCoProduct) {
    api.yellowAlert("Material not affected by sales price increase as its a Top Co Product, returning last year units");
    return unitsSold
}

return api.getElement("PriceIncreaseImpact").newUnits