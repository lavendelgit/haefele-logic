def transactionsCount = api.getElement("TransactionsCount")
def minimumTransactionsCount = api.getElement("MinimumTransactionsCount")
def unitsSoldLastYear = api.getElement("UnitsSold")

if (api.product("sku").startsWith("999")) {
    api.yellowAlert("Internal article (999). Returning units from last year.")
    return unitsSoldLastYear
}

if (!api.getElement("IsGoodRegression")) {
    api.yellowAlert("Linear regression is not good fit. Returning units from last year.")
    return unitsSoldLastYear
}

if (api.product("sku").endsWith("X")) {
    api.yellowAlert("X Article. Returning units from last year.")
    return unitsSoldLastYear
}

if (transactionsCount < minimumTransactionsCount) {
    api.yellowAlert("Too few transactions. Ignoring linear regression and returning units from last year.")
    return unitsSoldLastYear
}

if (api.getElement("IsSamePrice")) {
    api.yellowAlert("Invoice prices are (nearly) the same. Ignoring linear regression and returning units from last year.")
    return unitsSoldLastYear
}

if (api.getElement("IsNegativeUnits")) {
    api.yellowAlert("Negative units from linear regression. Returning units from last year.")
    return unitsSoldLastYear
}

if (api.getElement("IsExtremeImpact")) {
    api.yellowAlert("The increase would cause an extreme impact (" +
            api.formatNumber("#.##%", api.getElement("PriceIncreaseImpact").expectedImpactPerc) + "). " +
            "Returning units from last year.")
    return unitsSoldLastYear
}

def isMaterialExcludedFromSalesPrice = api.getElement("IsMaterialShieldedFromPriceRise")
if (isMaterialExcludedFromSalesPrice) {
    api.yellowAlert("Material not affected by sales price increase through exclusion list configuration, returning last year units");
    return unitsSoldLastYear
}

def isTopCoProduct = api.getElement("IsTopCoProduct")
if (isTopCoProduct) {
    api.yellowAlert("Material not affected by sales price increase as its a Top Co Product, returning last year units");
    return unitsSoldLastYear
}

return api.getElement("PriceIncreaseImpact").currentUnits
