def transactionCount = api.getElement("TransactionsCount")
def minimumTransactionsCount = api.getElement("MinimumTransactionsCount")

return (transactionCount == null || transactionCount == 0 || (minimumTransactionsCount && transactionCount < minimumTransactionsCount))