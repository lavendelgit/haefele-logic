def revenue = api.getElement("Revenue")
def expectedImpactAmount = api.getElement("ExpectedImpactAmount")

if (expectedImpactAmount != null && revenue > 0) {
    return expectedImpactAmount / revenue
} else {
    return 0.0
}

