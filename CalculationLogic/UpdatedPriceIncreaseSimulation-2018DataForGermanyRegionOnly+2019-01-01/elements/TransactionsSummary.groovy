def ctx = api.getDatamartContext()
def dm1 = ctx.getDataSource("TransactionSummaryPerArticlePerMonth")
def q1 = ctx.newQuery(dm1)
q1.selectAll(true)

def sku = api.product("sku")
def currentYear = api.calendar().get(Calendar.YEAR);

def filters = [
    Filter.equal("Material", sku),
    //Filter.greaterThan("Units", 0),
    //Filter.greaterThan("Revenue", 0),
    Filter.greaterOrEqual("YearMonth", "2018-01-01"),
    Filter.lessThan("YearMonth", "2019-01-01")
]

q1.select("Material")
q1.select("p1_SalesPricePer100Units", "avgPrice")
q1.select("p2_SalesPricePer100Units", "minPrice")
q1.select("p3_SalesPricePer100Units", "maxPrice")
q1.select("p4_Revenue", "revenue")	
q1.select("p5_Units", "quantity")
q1.select("p6_MarginAbs", "marginAbs")
q1.where(*filters)

def sql = """
    SELECT
		Material AS Material,
        AVG(T1.avgPrice) AS avgPrice,
        MIN(T1.minPrice) AS minPrice,
        MAX(T1.maxPrice) AS maxPrice,
        COUNT(1) AS count,
		SUM(T1.revenue) AS revenue,
		SUM(T1.quantity) AS quantity,
		SUM(T1.marginAbs) AS marginAbs
    """

sql += " FROM T1 GROUP BY Material"

api.trace("sql", "", sql)

def result = ctx.executeSqlQuery(sql, q1)

def returnObj = result?.collect{ r -> [
		Material : r.material,
		avgPrice : r.avgprice,
        minPrice : r.minprice,
        maxPrice : r.maxprice,
        count : r.count,
        revenue : r.revenue,
        quantity : r.quantity,
        marginAbs : r.marginabs
    ]
}?.find() ?: [:]

api.trace("What is result", " Result: "+ returnObj)

return returnObj