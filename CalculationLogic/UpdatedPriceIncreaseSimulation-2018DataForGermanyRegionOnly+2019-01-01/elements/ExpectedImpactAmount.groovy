def currentRevenue = api.getElement("CurrentRevenue")
def newRevenue = api.getElement("NewRevenue")

if (currentRevenue == null || newRevenue == null) {
    return
}

return newRevenue - currentRevenue