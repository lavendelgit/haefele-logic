def target = api.getDatamartRowSet("target")
def result = api.stream("PX", null /* sort-by*/,
        ["sku","attribute1","attribute2","attribute3","attribute4","zpap.attribute13"],
        Filter.equal("name", "CustomerSalesPrice"))

if (!result) {
    result?.close()
    return;
}

def count = 0;

while (result.hasNext()) {
    def zpap = result.next()
    def row = ["SourceID": zpap.attribute13,
               "Material": zpap.sku,
               "customerId" : zpap.attribute1,
               "ValidFrom" : zpap.attribute2,
               "ValidTo" : zpap.attribute3,
               "CustomerSalesPrice" : zpap.attribute4]
    target.addRow(row)
    count++
}

api.trace("count", "", count)

result?.close()