def ten = api.getElement("Sku")
return ten != null && ten.length() >= 10 ? ten[0..9] : ""