if (api.isSyntaxCheck()) return
return ""

def conditionName = out.ConditionName
def conditionTable = out.ConditionTable

return  getCondiDetails(out.SKU,conditionName,conditionTable)


def getCondiDetails(String sku, String conditionName, String conditionTable)
{
    def secKey = out.SecondaryKey
    def skuFilterStr = out.ArticleFilter?.replace("sku",sku).split(",")?:[sku]
    def addPGFilter = out.ProductGroupSearch
    def generalLib = libs.__LIBRARY__.GeneralUtility
    def targetDate = out.TargetDate

    def conditionDetails = [:]
  	def skuFieldName = (conditionName != "ZRA")?"sku":"MATNR"
    def filters = [
            Filter.equal("KOTAB",conditionTable)
    ]
  
  	if (conditionName != "ZRA") filters << Filter.equal("name", "S_"+conditionName)
  
  	if (skuFilterStr)
  		filters << Filter.in("$skuFieldName",skuFilterStr)

    out.KeyColumns.each { cols ->
        filters.add(Filter.equal(cols,secKey[cols]))
    }
    
    filters.add(generalLib.getFilterConditionForDateRange("DATAB", "DATBI", targetDate, targetDate))

   def result = getConditionRecords(conditionName, filters,addPGFilter) 
  
    def skuMap = result?.findAll { it-> it.sku == sku}
    result = skuMap?:result
  
  if (addPGFilter && addPGFilter == "Yes") {
    result = filterDataForProductHierarchy(result)
  }

    if (result && result.size() > 0) {
        def resultSize = result.size()

        if (resultSize > 1) result = result?.findAll {it.KBETR != null} // filter null values of KBETR
      	def disVal = result.sum{it.KBETR as BigDecimal}
      	def pricingType = out.ConditionRow?.attribute4?:Library.PRICING_TYPE_PER_DISCOUNT
        def discountValue = (disVal && resultSize > 0 ) ? (disVal / resultSize):0.0
        if (conditionName == "ZRA" && (pricingType == Library.PRICING_TYPE_PER_DISCOUNT || pricingType == Library.PRICING_TYPE_MARKUP)) {
			discountValue = discountValue?discountValue/100:0.00
        }
      api.trace("discountValue",discountValue)
      	conditionDetails.put("pricingType",pricingType)
        conditionDetails.put("aggregate",resultSize > 1? "Yes":"No")
        conditionDetails.put("discount",discountValue)
    } else {
  	api.addWarning("Conditiona Data for $sku,  Condition $conditionName for Condition Table $conditionTable is not available.")
    api.abortCalculation()
    }

  api.trace("conditionDetails",conditionDetails)
    return conditionDetails
}

def getConditionRecords(conditionName, filters,addPGFilter) 
{
  def result = []
  def fields
  
  if (conditionName != "ZRA")
  {
  	def start = 0
    fields = (addPGFilter && addPGFilter == "Yes")?["sku","KBETR","KONWA","ZZPRODH1","ZZPRODH2","ZZPRODH3","ZZPRODH4","ZZPRODH5","ZZPRODH6","ZZPRODH7"]:["sku","KBETR","KONWA"]
    while (recs = api.find("PX50", start, api.getMaxFindResultsLimit(), null,["sku","KBETR","KONWA"], *filters)) {
        result.addAll(recs)
        start += recs.size()
    }   
  }
  else {
    fields = (addPGFilter && addPGFilter == "Yes")?["MATNR","KBETR","KONWA","ZZPRODH1","ZZPRODH2","ZZPRODH3","ZZPRODH4","ZZPRODH5","ZZPRODH6","ZZPRODH7"]:["MATNR","KBETR","KONWA"]    
    def ctx = api.getDatamartContext()
        def ds = ctx.getDataSource("ZRA")

        def query = ctx.newQuery(ds, false)
        for (key in fields) {
            query.select(key)
        }
        query.where(*filters)
    api.trace("query",query)
        result = ctx.executeQuery(query)?.getData()?.collect()
  }
  
  return result
}

def filterDataForProductHierarchy (conditionResult)
{
  conditionResult.sort
    def filteredConditionRecords = conditionResult
    def prodHierarchyColumnValues = []

    for (int j=0;j<7;j++) {
        def prodHierarchyColumn = out["ProdH"+ (j+1)]?.split(" ")[0]
        if (prodHierarchyColumn){
            prodHierarchyColumnValues[j] = prodHierarchyColumn
        }
    }

    for (int i=0;i<prodHierarchyColumnValues.size();i++) {
        if (prodHierarchyColumnValues[i]) {
            def filteredConditionRecordsTemp = filteredConditionRecords.findAll { it -> it["ZZPRODH" + (i+1)] == prodHierarchyColumnValues[i]}
            if (filteredConditionRecordsTemp) {
                filteredConditionRecords = filteredConditionRecordsTemp
            } else {
            	filteredConditionRecords = null
                break
            }
        }
    }

  return filteredConditionRecords
}

