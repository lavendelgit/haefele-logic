def revenueImpact

if (out.expRevenue_min != null && out.totRevenue != null)
{
  revenueImpact = out.expRevenue_min - out.totRevenue
}
//api.trace("out.NewPocketPriceMinPercentage",out.NewPocketPriceMinPercentage)
return revenueImpact ?: null