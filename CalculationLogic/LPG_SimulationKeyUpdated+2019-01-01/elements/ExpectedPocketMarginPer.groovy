def expectPocketmargin = out.ExpectedPocketMargin

if (expectPocketmargin && expectPocketmargin != null)
	return expectPocketmargin
else
	return out.ExpectedPocketMarginPerDefault?:null

//return expectPocketmargin?:out.ExpectedPocketMarginPerDefault?:null