def exPocketMarginPer = out.ExpectedPocketMarginPer as BigDecimal

def discount = out.Discount?:0

def grossPrice
if (exPocketMarginPer && exPocketMarginPer != 0)
{
  if (out.isPerDiscount)
    grossPrice = (out.NewCostPrice_Optimal / (1 - exPocketMarginPer))/(1+discount)
  else
    grossPrice = out.NewCostPrice_Optimal /(1-exPocketMarginPer)
 
}	

return grossPrice