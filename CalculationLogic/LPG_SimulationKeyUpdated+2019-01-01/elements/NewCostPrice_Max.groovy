def increaseByPer = out.IncreasePercentage_Max

if (increaseByPer && out.PercentageIncreaseType == "Landing Price")
	return out.BaseCost + (out.BaseCost * increaseByPer)/100
else
  return out.BaseCost