def revenueImpact

if (out.expRevenue_Optimal != null && out.totRevenue != null)
{
  revenueImpact = out.expRevenue_Optimal - out.totRevenue
}
//api.trace("out.NewPocketPriceMinPercentage",out.NewPocketPriceMinPercentage)
return revenueImpact ?: null