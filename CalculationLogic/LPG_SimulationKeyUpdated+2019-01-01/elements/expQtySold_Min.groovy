import java.math.BigDecimal

if (null in [out.totRevenue, out.NewPocketPrice_Min]) {
 return null
}
else {
    def qty = (out.totRevenue / Math.abs(out.NewPocketPrice_Min)) as BigDecimal

    return qty.setScale(0, RoundingMode.CEILING)
}