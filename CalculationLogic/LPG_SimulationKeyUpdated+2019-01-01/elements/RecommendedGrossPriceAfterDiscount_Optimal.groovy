def exPocketMarginPer = out.ExpectedPocketMarginPer as BigDecimal

if (out.isPerDiscount && (exPocketMarginPer && exPocketMarginPer != 0))
{
  def grossPrice = out.NewCostPrice_Optimal / (1 - exPocketMarginPer)

  return grossPrice
}