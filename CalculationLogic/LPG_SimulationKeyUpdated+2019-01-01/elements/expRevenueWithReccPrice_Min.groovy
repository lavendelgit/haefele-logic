def exPocketMarginPer = out.ExpectedPocketMarginPer as BigDecimal

if (exPocketMarginPer && exPocketMarginPer != 0)
{
  def qty = out.getTranasactionData?.quantity
  
  if (out.isPerDiscount)
  	return qty? (qty * out.RecommendedGrossPriceAfterDiscount_Min) : null
  else
  	return qty? (qty * out.RecommendedGrossPrice_Min) : null
}