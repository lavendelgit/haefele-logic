def grossPrice = out.GrossPriceDetails?.GrossPrice

if (grossPrice && grossPrice > 0) {
  return grossPrice/100
} else {
  	api.addWarning("Gross price for ${out.SKU} is not available.")
    api.abortCalculation()
}