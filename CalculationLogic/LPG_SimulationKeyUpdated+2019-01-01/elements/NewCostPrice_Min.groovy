def increaseByPer = out.IncreasePercentage_Min

if (out.PercentageIncreaseType == "Landing Price") {
  api.trace("****","${out.BaseCost} + (${out.BaseCost} * $increaseByPer)/100")
	return out.BaseCost + (out.BaseCost * increaseByPer)/100
}
else
  return out.BaseCost