import java.math.BigDecimal

if (null in [out.totRevenue, out.NewPocketPrice_Optimal]) {
 return null
}
else {
    def qty = (out.totRevenue / Math.abs(out.NewPocketPrice_Optimal)) as BigDecimal

    return qty.setScale(0, RoundingMode.CEILING)
}