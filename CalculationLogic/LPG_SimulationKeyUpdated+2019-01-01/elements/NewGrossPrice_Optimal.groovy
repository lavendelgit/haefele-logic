def increaseByPer = out.IncreasePercentage_Optimal

if (out.PercentageIncreaseType != "Landing Price")
	return out.GrossPrice + (out.GrossPrice * increaseByPer)/100
else
  return out.GrossPrice
