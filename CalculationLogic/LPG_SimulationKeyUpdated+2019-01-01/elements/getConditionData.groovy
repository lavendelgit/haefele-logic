if (api.isSyntaxCheck()) return

def conditionDetails = [:]
def pricingType = out.ConditionRow?.attribute4?:Library.PRICING_TYPE_PER_DISCOUNT
def discountValue = out.SecondaryKey?.KBETR as BigDecimal
//api.trace("discountValue",discountValue)
if (!discountValue)
	discountValue = 0.00

if (discountValue && out.ConditionName == "ZRA" && (pricingType == Library.PRICING_TYPE_PER_DISCOUNT || pricingType == Library.PRICING_TYPE_MARKUP))
{
  discountValue = discountValue/100
}

conditionDetails.put("pricingType",pricingType)
conditionDetails.put("aggregate","No")//filteredSkuRecordsSize > 1? "Yes":"No")
conditionDetails.put("discount",discountValue?:0)
//api.trace("conditionDetails",conditionDetails)
return conditionDetails

/*
api.logInfo("****************getConditionData", "getConditionData")
return  getCondiDetails(out.SKU, out.ConditionName, out.ConditionTable)

def getCondiDetails(String sku, String conditionName, String conditionTable)
{
  api.logInfo("****************getCondiDetails", "getCondiDetails")
  def conditionDetails = [:]
  def conditionRecords = getConditionRecords(sku, conditionName,conditionTable)
  
  //filter the records for sku and key columns
  def filteredSkuRecords = filterSkuBasedRecords(conditionRecords, conditionName, sku)
  
  // filter the records for Product Hierarchy
   if (filteredSkuRecords && filteredSkuRecords.size() > 0) {
        def filteredSkuRecordsSize = filteredSkuRecords.size()

        if (filteredSkuRecordsSize > 1) filteredSkuRecords = filteredSkuRecords?.findAll {it.KBETR != null} // filter null values of KBETR
      	def disVal = filteredSkuRecords.sum{it.KBETR as BigDecimal}
      	def pricingType = out.ConditionRow?.attribute4?:Library.PRICING_TYPE_PER_DISCOUNT
        def discountValue = (disVal && filteredSkuRecordsSize > 0 ) ? (disVal / filteredSkuRecordsSize):0.0
        if (conditionName == "ZRA" && (pricingType == Library.PRICING_TYPE_PER_DISCOUNT || pricingType == Library.PRICING_TYPE_MARKUP)) {
			discountValue = discountValue?discountValue/100:0.00
        }
      api.trace("discountValue",discountValue)
      	conditionDetails.put("pricingType",pricingType)
        conditionDetails.put("aggregate",filteredSkuRecordsSize > 1? "Yes":"No")
        conditionDetails.put("discount",discountValue)
    } else {
  	api.addWarning("Conditiona Data for $sku,  Condition $conditionName for Condition Table $conditionTable is not available.")
    api.abortCalculation()
    }

  api.trace("conditionDetails",conditionDetails)
    return conditionDetails
  
  
}

def getConditionRecords(String sku, String conditionName, String conditionTable)
{
  api.logInfo("****************trying to get data cache matrix logic", "step1")
  	return libs.SharedLib.CacheUtils.getOrSetMap(api.global, conditionName, [sku, conditionName, conditionTable], { _sku, _conditionName, _conditionTable ->
        //get bulk material from product master
        try{
          	api.logInfo("****************trying to acess LPG logic and not from matrix logic", "step2")
            return getConditionRecordsFromCache( _sku, _conditionName, _conditionTable)
        }
        catch (any) {
            api.trace("getConditionRecordsFromCache", "error", any)
            return null
        }
    })
}

def filterSkuBasedRecords (List conditionRecords, String conditionName, String sku)
{
  	//filter sku base records
  def secKey = out.SecondaryKey
    def skuFieldName 	= (conditionName != "ZRA")?"sku":"MATNR"    
    def skuFilterList 	= out.ArticleFilter.split(",")
    api.trace("skuFilterList.size()",skuFilterList.size())
    if (skuFilterList.size() == 2) {
      skuFilteredRecords = conditionRecords.findAll { it -> it["$skuFieldName"] == sku || it["$skuFieldName"] == "*"}
    } else {
      skuFilteredRecords = conditionRecords.findAll { it -> it["$skuFieldName"] == sku }
    }
  
  // filter records for key columns
  out.KeyColumns.each { cols ->
          //filters.add(Filter.equal(cols,secKey[cols]))      
    skuFilteredRecords = skuFilteredRecords.findAll { it -> it["$cols"] == secKey[cols] }
      }
  
  api.trace("skuFilteredRecords",skuFilteredRecords)
  return skuFilteredRecords
}

def getConditionRecordsFromCache(String sku, String conditionName, String conditionTable)
{
  api.logInfo("****************getting data from LPG logic and not from matrix logic", "step3")
  def result 	= []
  def pgFilter	= out.ProductGroupSearch
  
  def conditionFilter = buildConditionFilter (conditionName, conditionTable)
  List fields
  
  if (conditionName != "ZRA") {
      fields = (pgFilter && pgFilter=="Yes")?["sku","KOTAB","KBETR","ZZPRODH1","ZZPRODH2","ZZPRODH3","ZZPRODH4","ZZPRODH5","ZZPRODH6","ZZPRODH7"]:["sku","KOTAB","KBETR"]
    } else {
      fields = (pgFilter && pgFilter == "Yes")?["MATNR","KOTAB","KBETR", "ZZPRODH1","ZZPRODH2","ZZPRODH3","ZZPRODH4","ZZPRODH5","ZZPRODH6","ZZPRODH7"]:["MATNR","KOTAB","KBETR"]
    }
  
  out.KeyColumns?.each{ cols->
            fields << cols
          }
  api.trace("fields",fields)
  
    if (conditionName != "ZRA") {
      //pick data from PX table
      def start = 0
      while (recs = api.find("PX50", start, api.getMaxFindResultsLimit(), "sku",fields, true ,*conditionFilter)) {
        result.addAll(recs)
        start += recs.size()
      }

    } else {
      def ctx = api.getDatamartContext()
      def ds = ctx.getDataSource("ZRA")

      def query = ctx.newQuery(ds, false)
      for (key in fields) {
        query.select(key)
      }
      query.where(*conditionFilter)
      //api.trace("query",query)
      result = ctx.executeQuery(query)?.getData()?.collect()
    }
	
  //api.trace("result",result)
  return result
}

def buildConditionFilter (conditionName, conditionTable)
{
  def generalLib = libs.__LIBRARY__.GeneralUtility
  def targetDate = out.TargetDate

  def conditionFilter = [
    Filter.equal("KOTAB",conditionTable),
    Filter.isNotNull('KBETR')
  ]    

  if (conditionName != "ZRA")
  conditionFilter.add(Filter.equal("name", "S_"+conditionName))

  conditionFilter.add(generalLib.getFilterConditionForDateRange("DATAB", "DATBI", targetDate, targetDate))    

  api.trace("conditionFilter",conditionFilter)

  return conditionFilter
}
*/