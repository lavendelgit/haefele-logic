import groovy.transform.Field
@Field final String PRICING_TYPE_PER_DISCOUNT = "Per Discount"
@Field final String PRICING_TYPE_MARKUP = "Markup"
@Field final String PRICING_TYPE_NETPRICE = "Net Price"
@Field final String PRICING_TYPE_ABS_DISCOUNT = "Abs Discount"
@Field final String PRICING_TYPE_MARKUP_DISCOUNT = "Markup / Per Discount"


def getLatestZNRVCost(String sku, Filter... additionalFilters)
{
    def targetDate = out.TargetDate

    def filters = [
            Filter.equal("name", "SurchargeCostPrice"),
            Filter.equal("sku", sku),
            Filter.lessOrEqual("attribute1",targetDate),
            Filter.greaterOrEqual("attribute2",targetDate)
    ]

    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute1"
    def salePrice = api.find("PX20", 0, 1, orderBy, *filters)
    def result = [:]

    if (salePrice[0]) {
        result = salePrice[0]
    }

    api.trace("Library.getLatestZNRVCost", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}

def getPricingTypeMap()
{
    if (!api.global.PricingTypeMap)
    {
        def pricingTypeMap = [:]
        pricingTypeMap.add("ZCX","Discount")
        pricingTypeMap.add("ZMC","Discount")
        pricingTypeMap.add("ZLC%","Discount")
        pricingTypeMap.add("ZLC","Net Price")
        pricingTypeMap.add("ZPA","Net Price")
        pricingTypeMap.add("ZPC","Net Price")
        pricingTypeMap.add("ZPF","Net Price")


        //Mark up
        //
    }

    return api.global.PricingTypeMap
}

/**
 * Weightage
 * @param keys
 * @return
 */
def Weightage(String... keys) {
  def weightage = 0 as Integer
  keys.eachWithIndex { key, index ->
    def weightageIndex = index ? 2.power(index) : 1
    weightage += (("DEFAULT".equals(key) ? 0 : weightageIndex))
  }
  return weightage
}