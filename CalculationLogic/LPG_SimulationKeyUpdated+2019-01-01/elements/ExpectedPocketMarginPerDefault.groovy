def ph3 = out.ProdH3

// cache PP table data

if (!api.global.ExpectedPockeMarginPPRows) {
  api.global.ExpectedPockeMarginPPRows = api.findLookupTableValues("ExpectedCustomerMarginSimulation").collectEntries {
        [(it.name): it.attribute1]
    }
}

def expectedPocketMarginPer = api.global.ExpectedPockeMarginPPRows?.find {it-> it.key == ph3}
  
return expectedPocketMarginPer?expectedPocketMarginPer.value:null