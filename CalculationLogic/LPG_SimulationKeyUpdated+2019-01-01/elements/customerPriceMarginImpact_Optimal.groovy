def diffPM = out.NewPocketMargin_Optimal - out.PocketMargin
if (diffPM < 0)
	return api.attributedResult(diffPM).withTextColor("#fc5c65").withTextDecoration("underline").withBackgroundColor("#FFFF00")
    //api.redAlert("")

return diffPM