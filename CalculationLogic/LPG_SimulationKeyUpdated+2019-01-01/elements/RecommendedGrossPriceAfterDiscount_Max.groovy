def exPocketMarginPer = out.ExpectedPocketMarginPer as BigDecimal

if (out.isPerDiscount && (exPocketMarginPer && exPocketMarginPer != 0))
{
  def grossPrice = out.NewCostPrice_Max / (1 - exPocketMarginPer)

  return grossPrice
}