def percentageType = ["Landing Price", "Gross Price"]

def param = api.option("Increase",percentageType)
def r = api.getParameter("Increase")
if (r != null && r.getValue() == null) {
    r.setValue("Landing Price")
    r.setRequired(true)
}

return param