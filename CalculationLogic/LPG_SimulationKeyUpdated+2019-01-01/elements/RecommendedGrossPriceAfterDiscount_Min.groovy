def exPocketMarginPer = out.ExpectedPocketMarginPer as BigDecimal

if (out.isPerDiscount && (exPocketMarginPer && exPocketMarginPer != 0))
{
  def grossPrice = out.NewCostPrice_Min / (1 - exPocketMarginPer)

  return grossPrice
}