def qty = out.getTranasactionData?.quantity
def sku = out.SKU
def year = out.Year
def custId = out.CustomerId

if (!qty)
{
  if (custId)
	api.addWarning("Transaction Data for $sku and customer $custId is not available for Year $year.")
  else
    api.addWarning("Transaction Data for $sku is not available for Year $year.")

    return null
}
return qty