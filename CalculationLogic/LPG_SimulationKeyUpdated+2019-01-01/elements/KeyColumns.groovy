def keyColumns = out.ConditionTableRow?.attribute1// + ",KBETR"

if (keyColumns)
	keyColumns +=  ",KBETR"
else
  keyColumns =  "KBETR"

//api.trace("keyColumns",keyColumns)

return keyColumns?.split(",")
