def increaseByPer = out.IncreasePercentage_Optimal

if (increaseByPer && out.PercentageIncreaseType == "Landing Price")
	return out.BaseCost + (out.BaseCost * increaseByPer)/100
else
  return out.BaseCost