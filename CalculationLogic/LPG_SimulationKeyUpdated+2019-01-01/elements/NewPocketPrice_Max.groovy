if (out.isPerDiscount)
    return (out.NewGrossPrice_Max * (1 + out.Discount))
else if (out.isMarkup)
    return ( 1 + out.MarkupPer) * out.BaseCost
else if (out.isAbsDiscount)
    return (out.NewGrossPrice_Max + out.DiscountVal)
else if (out.isNetPrice)
    return (out.NetPrice)