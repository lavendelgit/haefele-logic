def sku 	= out.SKU
def custId 	= out.CustomerId
def year 	= out.Year

def skuTransactionData

if (api.global.transactionData == null)
	api.global.transactionData = [:]

def fields = ['InvoiceDateYear' 	: 'Year',
              'Material'  			: 'SKU',
              'SUM(Revenue)'  		: 'revenue',
              'SUM(InvoiceQuantity)': 'quantity',
			  'CustomerId'			: 'CustomerId',
              'AVG(SalesPricePer100Units)' : 'avgPrice'
]


def filters = [Filter.equal("InvoiceDateYear",out.Year),
              Filter.equal("Material",sku)]


//api.trace("skuTransactionData",skuTransactionData)


if (!api.global.transactionData[sku]) {
  api.global.transactionData = [:]
  skuTransactionData = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')?.collect()
  api.global.transactionData[sku] = skuTransactionData?:null
  	
}
def result 	= [:]

if (api.global.transactionData[sku])
{
  skuTransactionData = api.global.transactionData[sku]
  
  if (custId) {
    
    result = skuTransactionData.findAll{ it-> it.CustomerId == custId || it.CustomerId == "000"+custId}
    
    return result?result[0]:null
    
  } else {    
    def avgPrice = skuTransactionData.sum{it.avgPrice as BigDecimal}
    def resultSize = skuTransactionData.size()
    
    result.Year = out.Year
    result.SKU 	= sku
    result.revenue 	= skuTransactionData.sum{it.revenue as BigDecimal}
    result.quantity = skuTransactionData.sum{it.quantity as BigDecimal}
    result.avgPrice = (avgPrice / resultSize)

    
  }
}

return result

/*
if (!api.global.transactionData[sku]) {
	api.global.transactionData = [:]
	skuTransactionData = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')?.collect()
	if (skuTransactionData)
		api.global.transactionData.put(sku,skuTransactionData)
}
if (api.global.transactionData[sku])
	{
		skuTransactionData = api.global.transactionData[sku]
		api.trace("skuTransactionData", skuTransactionData)


		def transactionData = skuTransactionData
		def result = []

		if (custId) {
			filteredTransactionData = skuTransactionData.findAll{ it-> it.CustomerId == custId || it.CustomerId == "000"+custId}
		} else {
			result = skuTransactionData
		}

		//if (transactionData.size() > 1) {
			//add the qty
			//add the revenue
			//avg gross price total gp/
			//result.revenue
			//result.quantity
			//result.avgPrice

		//}
		
				api.trace("transactionData",transactionData)
				return transactionData
	}
*/

/*def dmCtx = api.getDatamartContext()
def table = dmCtx.getTable("TransactionSummaryPerArticleDM")
def query = dmCtx.newQuery(table)

def filters = [
        Filter.equal("Material", sku),
        Filter.equal("InvoiceYear", out.Year)
]

query.select("COUNT(1)", "count")
query.select("p1_SalesPricePer100Units", "avgPrice")
query.select("p4_Revenue", "revenue")
query.select("p2_Units", "quantity")
query.select("p3_MarginAbs", "marginAbs")
query.where(*filters)

dmCtx.executeQuery(query)?.getData()?.collect()
*/