def exPocketMarginPer = out.ExpectedPocketMarginPer as BigDecimal

if (exPocketMarginPer && exPocketMarginPer != 0)
{
  if (null in [out.totRevenue, out.RecommendedGrossPrice_Min]) {
   return null
  }
  else {
      def qty = (out.totRevenue / Math.abs(out.RecommendedGrossPrice_Min)) as BigDecimal

      return qty.setScale(0, RoundingMode.CEILING)
  }
}