def revenueImpact

if (out.expRevenue_Max != null && out.totRevenue != null)
{
  revenueImpact = out.expRevenue_Max - out.totRevenue
}
//api.trace("out.NewPocketPriceMinPercentage",out.NewPocketPriceMinPercentage)
return revenueImpact ?: null