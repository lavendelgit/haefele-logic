def condiTableRows = api.findLookupTableValues("ConditionTableMaster",Filter.equal("attribute2","Yes"))

def param = api.option("ConditionTable",condiTableRows)
def r = api.getParameter("ConditionTable")
if (r != null && r.getValue() == null) {
    r.setValue("A004")
    r.setRequired(true)
}

return param