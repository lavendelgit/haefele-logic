def key = api.getSecondaryKey()
  //"ZPZ~DE~765" -- 001.26.605
  //"ZPF~1203875~1764.94"//api.getSecondaryKey() -- 826.13.910

if (key == null) {
    api.addWarning("Can not calculate the Pricelist, because seccondary key is not available")
}

def keyCols = out.KeyColumns
api.trace("keyCols",keyCols as List)

def parts = key?.tokenize("~")

if (!keyCols)
    return [condiName: parts[0]]
else {
    def result = [:]
    result.put("condiName",parts[0])
    Integer i = 1
    keyCols?.each{ colName ->
        result.put(colName,parts[i])
        i++
    }
  api.trace("result",result)
    return result
}


/*
999.11.131

ZLC~A005~8353301
ZLC~A005~2798269
ZLCPer~A005~8084524
ZLCPer~A005~8328880
	
ZLCPer~A005~8355705
ZLCPer~A005~9260435
ZLCPer~A005~6
ZRI~RS0
ZRI~DBA~805008
*/