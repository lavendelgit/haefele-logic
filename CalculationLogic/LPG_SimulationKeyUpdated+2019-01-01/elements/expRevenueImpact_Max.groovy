def exPocketMarginPer = out.expRevenueWithReccPrice_Max as BigDecimal

if (out.totRevenue && exPocketMarginPer && exPocketMarginPer != 0)
{
  def revenueImpact = exPocketMarginPer - out.totRevenue
  return revenueImpact ?: null
}