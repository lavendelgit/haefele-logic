if (out.isPerDiscount){
    return out.GrossPrice * out.Discount?:0.00
}
else if (out.isAbsDiscount) {  
  return (out.getConditionData?.discount) as BigDecimal
}
