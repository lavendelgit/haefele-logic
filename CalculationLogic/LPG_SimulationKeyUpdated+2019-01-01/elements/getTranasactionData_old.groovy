return ""

def sku = out.SKU
def custId = out.CustomerId
def year = out.Year

def fields = ['InvoiceDateYear' 	: 'Year',
              'Material'  			: 'SKU',
              'SUM(Revenue)'  		: 'revenue',
              'SUM(InvoiceQuantity)': 'quantity',
              'AVG(SalesPricePer100Units)' : 'avgPrice'
]


def filters = [Filter.equal("InvoiceDateYear",out.Year),
              Filter.equal("Material",sku)]

if (custId) {
  	fields << ['CustomerId'		: 'CustomerId']
	filters << Filter.like("CustomerId","%"+custId)
}

//api.trace("filters",filters)

def tr = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')?.collect()
api.trace("tr",tr)

return tr[0]

/*def dmCtx = api.getDatamartContext()
def table = dmCtx.getTable("TransactionSummaryPerArticleDM")
def query = dmCtx.newQuery(table)

def filters = [
        Filter.equal("Material", sku),
        Filter.equal("InvoiceYear", out.Year)
]

query.select("COUNT(1)", "count")
query.select("p1_SalesPricePer100Units", "avgPrice")
query.select("p4_Revenue", "revenue")
query.select("p2_Units", "quantity")
query.select("p3_MarginAbs", "marginAbs")
query.where(*filters)

dmCtx.executeQuery(query)?.getData()?.collect()
*/