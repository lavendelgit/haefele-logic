def sku = out.SKU

def filter = [
        Filter.equal("attribute5","EUR"),
        Filter.lessOrEqual("attribute1", out.TargetDate),
        Filter.greaterOrEqual("attribute2", out.TargetDate)
]

def baseCost = lib.Find.latestBaseCostRecord(sku, *filter)?.attribute3

if (baseCost && baseCost > 0) {
  return baseCost/100
} else {
  api.addWarning("Base cost for ${out.SKU} is not available.")
  api.abortCalculation()
}