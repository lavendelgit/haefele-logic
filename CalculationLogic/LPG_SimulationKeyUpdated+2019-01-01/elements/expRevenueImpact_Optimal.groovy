def exPocketMarginPer = out.expRevenueWithReccPrice_Optimal as BigDecimal
api.trace("exPocketMarginPer",exPocketMarginPer)

if (out.totRevenue && exPocketMarginPer && exPocketMarginPer != 0)
{
  def revenueImpact = exPocketMarginPer - out.totRevenue
  return revenueImpact ?: null
}