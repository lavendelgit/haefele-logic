def multipleColors = api.booleanUserEntry("Colors")

if (multipleColors) {
    return lib.Colors.palette()
} else {
    return ['#a6cee3']
}
