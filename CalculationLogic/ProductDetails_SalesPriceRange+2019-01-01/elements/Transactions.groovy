if (api.isSyntaxCheck()) {
    return [data:[], summary:[:]]
}

def productFilter = api.getElement("ProductFilter")
def groupBy = api.getElement("GroupBy")
def dmCtx = api.getDatamartContext()
def datamartQuery = dmCtx.newQuery(dmCtx.getTable("TransactionsDM"),false)

def calendar = api.calendar()
calendar.add(Calendar.MONTH, -12)
calendar.set(Calendar.DAY_OF_MONTH, 1)
def yearAgo = calendar.getTime()


datamartQuery.select("InvoiceDate", "invoiceDate")
datamartQuery.select("SalesPricePer100Units", "salesPricePer100Units")
datamartQuery.select("Units", "units")
datamartQuery.select(groupBy, "groupBy")
datamartQuery.orderBy("invoiceDate asc")


datamartQuery.where(Filter.greaterOrEqual("InvoiceDate", yearAgo))
datamartQuery.where(Filter.isNotNull("SalesPricePer100Units"))
datamartQuery.where(Filter.notEqual("SalesPricePer100Units", 0))
datamartQuery.where(Filter.equal("Material", api.getElement("SKU")))

def result = dmCtx.executeQuery(datamartQuery)

result?.calculateSummaryStatistics()

return [
        data: result?.getData()?.collect(),
        summary: result?.getSummary()
]

