package Dashboard_CustomerSalesPricesPerCustomerDiscountGroup.elements

def resultMatrix = api.newMatrix("Discount Group","Count of ZPAPs")
resultMatrix.setTitle("Number of ZPAPs per Customer Discount Group - table")
resultMatrix.setPreferenceName("MLZperCDG_Table")
resultMatrix.setColumnFormat("Count of ZPAPs", FieldFormatType.NUMERIC)

def zpapsPerCustomerDiscountGroup = api.getElement("ZPAPsPerCustomerDiscountGroup")

def total = 0;

for (row in zpapsPerCustomerDiscountGroup) {
    resultMatrix.addRow([
            "Discount Group" : row.discountGroup,
            "Count of ZPAPs" : row.count
    ])
    total += row.count
}

resultMatrix.addRow([
        "Discount Group": resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of ZPAPs": resultMatrix.styledCell(total).withCSS("font-weight: bold")
])

return resultMatrix
