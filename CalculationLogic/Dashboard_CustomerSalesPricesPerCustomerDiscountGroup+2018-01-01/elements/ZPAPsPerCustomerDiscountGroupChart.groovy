package Dashboard_CustomerSalesPricesPerCustomerDiscountGroup.elements

def zpapsPerCustomerDiscountGroup = api.getElement("ZPAPsPerCustomerDiscountGroup")

def categories = zpapsPerCustomerDiscountGroup.collect { e -> e.discountGroup}
def data = zpapsPerCustomerDiscountGroup.collect { e -> e.count};

def chartDef = [
        chart: [
            type: 'column'
        ],
        title: [
            text: 'ZPAPs per Discount Group'
        ],
        xAxis: [[
            title: [
                    text: "Discount Group"
            ],
            categories: categories
        ]],
        yAxis: [[
            title: [
                text: "Number of ZPAPs"
            ]
        ]],
        tooltip: [
            headerFormat: 'Discount Group: {point.key}<br/>',
            pointFormat: 'Number of ZPAPs: {point.y}'
        ],
        legend: [
            enabled: false
        ],
        series: [[
             data: data,
             dataLabels: [
                 enabled: true,
             ]
        ]]
]

api.buildFlexChart(chartDef)