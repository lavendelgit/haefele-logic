if (!api.currentItem()) {
    return
}

def salesPrice = api.currentItem()?.attribute3

if (salesPrice instanceof String) {
    return salesPrice.replaceAll(",",".") as BigDecimal
}

return salesPrice