import net.pricefx.server.dto.calculation.ConfiguratorEntry

List simulationInputOptions = libs.HaefeleCommon.Simulation.getPriceChangeExceptionInputsReferenceName(libs.HaefeleCommon.Simulation.INPUT_CONFIGURATION_EXCEPTION)

List matrixRows = getMatrixRows()

Map inputs = [:]
ConfiguratorEntry inputConfigurator = Library.createInputConfiguratorWithHiddenStoredValue("Exceptions")

libs.HaefeleCommon.Simulation.EXCEPTION_COLUMNS.each { String inputName, Map inputDef ->
    switch (inputDef.type) {
        case "DatamartFilter":
            Library.createDatamartFilterBuilder(inputConfigurator, inputName, inputDef.label, Constants.DM_SIMULATION, []) {
                selValue -> inputs[inputName] = selValue
            }
            break
        case "Percent":
            Library.createPercentField(inputConfigurator, inputName, inputDef.label, null, false, false, true) {
                selValue -> inputs[inputName] = selValue
            }
            break
        case "Text":
            Library.createTextField(inputConfigurator, inputName, inputDef.label, null, false, false, true) {
                selValue -> inputs[inputName] = selValue
            }
            break
        case "Option":
            Library.createDropdown(inputConfigurator, inputName, inputDef.label, null, simulationInputOptions, false, false, true) {
                selValue -> inputs[inputName] = selValue
            }
            break
    }
}
Library.createBooleanField(inputConfigurator, "AddRow", "Click here to save the Excecption.", false, false, false, false)
Library.createExceptionMatrixInput(inputConfigurator, 'ExceptionMatrix', 'All Exceptions', matrixRows, inputs)

return inputConfigurator

protected List getMatrixRows() {
    def inpMatrixValue = api.input('ExceptionMatrix')
    return ((inpMatrixValue instanceof List) ? inpMatrixValue : []) as List
}