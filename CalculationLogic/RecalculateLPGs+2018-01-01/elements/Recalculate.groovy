def priceGrids = api.getElement("LPGs")

priceGrids.each { pg ->
    api.logInfo("Scheduling LPG recalculation", pg.label)
    actionBuilder
            .addLivePriceGridAction(pg.label)
            .setCalculate(true)
}