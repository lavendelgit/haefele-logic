def params = [:]
return popupConfiguratorEntry("Configurator_MonthlySimulationExceptionConditions", "AdditionalExceptions", "Exceptions", params)

Object popupConfiguratorEntry(String configuratorLogic, String configuratorName, String label, Map params) {
    if (!configuratorName || !configuratorLogic) {
        api.throwException("Configurator name / logic not specified")
    }

    def configurator = api.configurator(configuratorName, configuratorLogic)

    def conf = api.getParameter(configuratorName)
    if (conf != null && conf.getValue() == null) {
        params["configurator"] = configuratorName
        conf.setLabel(label)
        conf.setValue(params)
    }
    return configurator
}
