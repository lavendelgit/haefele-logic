import net.pricefx.common.api.FieldFormatType

def resultMatrix

resultMatrix = getResultMatrix("Simulation Inputs", out.SimulationConfigurationInput, resultMatrix)
//below has to be in loop
resultMatrix = getResultMatrix("Exceptions Inputs", out.Exception["Monthly Exception Configuration"], resultMatrix)

if (resultMatrix) {
    resultMatrix.setPreferenceName("Configuration Input List")

    resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("ConfigurationInputDetails-ED"))
            .withColValueAsEventDataAttr(Constants.RESULT_MATRIX_COLUMN_NAMES[0], "configuartionInputType")
            .withColValueAsEventDataAttr(Constants.RESULT_MATRIX_COLUMN_NAMES[1], "configuartionInputName")

}

return resultMatrix


protected ResultMatrix getResultMatrix(String configurationInputType, String configurationInputName, ResultMatrix resultMatrix) {

    def dashUtl = lib.DashboardUtility
    if (!resultMatrix) resultMatrix = dashUtl.newResultMatrix('Configuration Inputs', Constants.RESULT_MATRIX_COLUMN_NAMES, Constants.RESULT_MATRIX_COLUMN_TYPES)
    resultMatrix.addRow([
            (Constants.RESULT_MATRIX_COLUMN_NAMES[0]): configurationInputType,
            (Constants.RESULT_MATRIX_COLUMN_NAMES[1]): configurationInputName
    ])

    return resultMatrix
}

/*
String simulationConfigurationInput = out.SimulationConfigurationInput
String exceptionConfigurationInput = out.Exception["Monthly Exception Configuration"]

List configurationInputs = [simulationConfigurationInput, exceptionConfigurationInput]
return Library.getPriceChangePercentageConfiguration(configurationInputs)

 */
