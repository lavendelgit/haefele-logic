List configurationInputOptions = libs.HaefeleCommon.Simulation.getPriceChangeExceptionInputsReferenceName(libs.HaefeleCommon.Simulation.INPUT_CONFIGURATION_SIMULATION)
//api.trace("configurationInputOptions",configurationInputOptions)
return selectBoxEntry('ConfigurationInput', 'Simulation Configuration Input', configurationInputOptions)

Object selectBoxEntry(String name, String label, List values, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.option(name, values)
    }
}

Object newInput(String name, String label, boolean isRequired, defaultValue, Closure inputCreator) {
    def input = inputCreator.call(name)
    def param = api.getParameter(name)
    if (param) {
        param.setLabel(label)
        param.setRequired(isRequired)
        if (param.getValue() == null && defaultValue != null) {
            param.setValue(defaultValue)
        }
    }
    return input
}