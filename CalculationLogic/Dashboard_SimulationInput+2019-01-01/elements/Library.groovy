protected List getPriceChangePercentageConfiguration(List configurationName) {
    List filters = [Filter.in("key2", configurationName)]

    return api.findLookupTableValues(Constants.INPUTS.SIMULATION_INPUT, ["key1","key2","key3","attribute1","attribute2","attribute3"], null, *filters)
}

protected String toPercent(BigDecimal numberValue) {
    return (numberValue != null) ? libs.SharedLib.RoundingUtils.round(numberValue, Constants.DECIMAL_PLACE.PERCENT) + ' %' : ''
}

protected String generateSection(String title, String configurationName, Map configurationInputMap, int width) {
    StringBuilder htmlBuilder = new StringBuilder()

    Map COLORS = Constants.SUMMARY_COLORS
    htmlBuilder.append("<div style='float: left;width: ${width}%;padding: 5px;height: 100%;margin: 3px;background-color:${COLORS.BG_COLOR};'>")
    htmlBuilder.append("<table style='width: ${width}%;padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR};background-color:${COLORS.BG_COLOR};'>")
    htmlBuilder.append("<tr><td style='text-align: center;color:${COLORS.TITLE};font-size: 15px' colspan=4> $configurationName</td></tr>" +
            "<tr style='font-weight:bold;color:${COLORS.LABEL};font-size:12px;padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'><td style='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>Price Change Month</td><td style='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>Gross Price % Change</td><td style='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>Purchase Price % Change</td><td>Quantity % Change</td>" +
            "</tr>")
    configurationInputMap?.each { monthName, configurationInputsDetails ->
        htmlBuilder.append("<tr style='color:${COLORS.VALUE};font-size:12px;padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>")
        htmlBuilder.append("<td style='font-weight:bold;color:${COLORS.VALUE};font-size:12px;padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>${monthName}</td>")
        htmlBuilder.append("<td style ='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR};text-align: right'>${configurationInputsDetails.grossPriceChange}</td>")
        htmlBuilder.append("<td style ='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR};text-align: right'>${configurationInputsDetails.purchasePriceChange}</td>")
        htmlBuilder.append("<td style ='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR};text-align: right'>${configurationInputsDetails.quantityPriceChange}</td>")
//font-size: 12px;text-align: right;padding: 3px 4px;
        htmlBuilder.append(("</tr>"))

    }
    htmlBuilder.append("</table></div)>")
    return htmlBuilder.toString()
}

protected Map createConfigurationInputMap (List configurationInputData) {
    Map configurationInputMap =[:]

    configurationInputData.forEach { configurationInputRow ->
        String monthName = configurationInputRow.key3
        BigDecimal grossPrice = configurationInputRow.attribute1 as BigDecimal
        BigDecimal purchasePrice = configurationInputRow.attribute2 as BigDecimal
        BigDecimal quantityPrice = configurationInputRow.attribute3 as BigDecimal
        Map row = [
                monthName          : monthName,
                monthNumber        : Constants.MONTHS_LIST[monthName],
                grossPriceChange   : toPercent(grossPrice * 100),
                purchasePriceChange: toPercent(purchasePrice * 100),
                quantityPriceChange: toPercent(quantityPrice * 100)]
        configurationInputMap.put(monthName, row)
    }
    configurationInputMap = configurationInputMap.sort { it.value.monthNumber }

    return configurationInputMap
}