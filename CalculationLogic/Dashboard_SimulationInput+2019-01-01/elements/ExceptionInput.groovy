return ""
String exceptionConfigurationInput = out.Exception["Monthly Exception Configuration"]
List exceptionMonthlyInputs = out.FetchConfigurationsInputs

def htmlPortlet = api.newController()
Map summaryData = api.local.SummaryData ?: [:]
StringBuilder htmlBuilder = new StringBuilder()
htmlBuilder.append("<div class=>")

List exceptionsInputs = exceptionMonthlyInputs?.findAll { it -> it.key1 == "Exceptions Inputs" }
Map exceptionInputMap = Library.createConfigurationInputMap(exceptionsInputs)
htmlBuilder.append("<div>")
htmlBuilder.append(Library.generateSection('EXCEPTION INPUT', exceptionConfigurationInput, exceptionInputMap, 100))
htmlBuilder.append("</div>")

htmlPortlet.addHTML(htmlBuilder.toString())
return htmlPortlet

/*
protected String generateSection(String title, String configurationName, Map configurationInputMap, int width) {
    StringBuilder htmlBuilder = new StringBuilder()

    Map COLORS = Constants.SUMMARY_COLORS
    htmlBuilder.append("<div style='float: left;width: ${width}%;padding: 5px;height: 100%;margin: 3px;background-color:${COLORS.BG_COLOR};'>")
    htmlBuilder.append("<table style='width: ${width}%;padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR};background-color:${COLORS.BG_COLOR};'>")
    htmlBuilder.append("<tr><td style='text-align: center;color:${COLORS.TITLE};font-size: 15px' colspan=4> $configurationName</td></tr>" +
            "<tr style='font-weight:bold;color:${COLORS.LABEL};font-size:12px;padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'><td style='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>Price Change Month</td><td style='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>Gross Price % Change</td><td style='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>Purchase Price % Change</td><td>Quantity % Change</td>" +
            "</tr>")
    configurationInputMap?.each { monthName, configurationInputsDetails ->
        htmlBuilder.append("<tr style='color:${COLORS.VALUE};font-size:12px;padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>")
        htmlBuilder.append("<td style='font-weight:bold;color:${COLORS.VALUE};font-size:12px;padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR}'>${monthName}</td>")
        htmlBuilder.append("<td style ='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR};text-align: right'>${configurationInputsDetails.grossPriceChange}</td>")
        htmlBuilder.append("<td style ='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR};text-align: right'>${configurationInputsDetails.purchasePriceChange}</td>")
        htmlBuilder.append("<td style ='padding:5px;border-style:solid;border-width:2px;border-color:${COLORS.BORDER_COLOR};text-align: right'>${configurationInputsDetails.quantityPriceChange}</td>")

        htmlBuilder.append(("</tr>"))

    }
    htmlBuilder.append("</table></div)>")
    return htmlBuilder.toString()
}

protected Map createConfigurationInputMap (List configurationInputData) {
    Map configurationInputMap =[:]

    configurationInputData.forEach { configurationInputRow ->
        String monthName = configurationInputRow.key3
        BigDecimal grossPrice = configurationInputRow.attribute1 as BigDecimal
        BigDecimal purchasePrice = configurationInputRow.attribute2 as BigDecimal
        BigDecimal quantityPrice = configurationInputRow.attribute3 as BigDecimal
        Map row = [
                monthName          : monthName,
                monthNumber        : Constants.MONTHS_LIST[monthName],
                grossPriceChange   : Library.toPercent(grossPrice * 100),
                purchasePriceChange: Library.toPercent(purchasePrice * 100),
                quantityPriceChange: Library.toPercent(quantityPrice * 100)]
        configurationInputMap.put(monthName, row)
    }
    configurationInputMap = configurationInputMap.sort { it.value.monthNumber }

    return configurationInputMap
}

 */