return ""
String simulationConfigurationInput = out.SimulationConfigurationInput
List exceptionMonthlyInputs = out.FetchConfigurationsInputs

List simulationInputs = exceptionMonthlyInputs?.findAll { it -> it.key1 == "Simulation Inputs" }
Map simulationInputMap = Library.createConfigurationInputMap(simulationInputs)

def htmlPortlet = api.newController()
Map summaryData = api.local.SummaryData ?: [:]
StringBuilder htmlBuilder = new StringBuilder()
htmlBuilder.append("<div>")
htmlBuilder.append(Library.generateSection('SIMULATION INPUT', simulationConfigurationInput, simulationInputMap, 100))
htmlBuilder.append("</div>")

htmlPortlet.addHTML(htmlBuilder.toString())
return htmlPortlet