import net.pricefx.common.api.FieldFormatType
import net.pricefx.server.dto.calculation.ResultMatrix

/**
 * All the common charts or dashboard related utility functions that can be re-used across different charts or dashboards
 * will be part of this library
 */

/**
 * Creates the result Matrix for your project it will accelerate development.
 * @param title
 * @param columnNames
 * @param columnTypes
 * @return
 */
def newResultMatrix(String title, List columnNames, List columnTypes) {
    if (!columnNames || !columnTypes || columnNames.size() != columnTypes.size())
        return null

    def resultMatrix = api.newMatrix(columnNames)
    resultMatrix.setTitle(title)
    int i = 0
    columnTypes.each {
        resultMatrix.setColumnFormat(columnNames[i], columnTypes[i])
        i++
    }
    return resultMatrix
}

def highlightCellWithBlue(ResultMatrix resultMatrix, def valueObject) {
    return resultMatrix.styledCell(valueObject, "#3765A4", null, "bold")
}

//red color= #f44340, green = #2e7d10
def boldHighlightWith(ResultMatrix resultMatrix, def valueObject, def color = "#3765A4") {
    return resultMatrix.styledCell(valueObject, color, null, "bold")
}

def redTrafficLightImage(ResultMatrix resultMatrix) {
    return resultMatrix.libraryImage("Traffic", "red")
}

def yellowTrafficLightImage(ResultMatrix resultMatrix) {
    return resultMatrix.libraryImage("Traffic", "yellow")
}

def greenTrafficLightImage(ResultMatrix resultMatrix) {
    return resultMatrix.libraryImage("Traffic", "green")
}

def triggerEventFromDashboard(ResultMatrix resultMatrix, String eventName, String columnName, String attribute) {
    resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent(eventName)).withColValueAsEventDataAttr(columnName, attribute)
}

def centerAlignment(ResultMatrix resultMatrix, def valueObject, def color = "black") {
    return resultMatrix.styledCell(valueObject, color, null, null, "center")
  //styledCell(Object value, String textColor, String bgColor, String weight, String alignment)
}

def boldHighlightWithCenterAlignment(ResultMatrix resultMatrix, def valueObject, def color = "#3765A4") {
    return resultMatrix.styledCell(valueObject, color, null, "bold","center")
}