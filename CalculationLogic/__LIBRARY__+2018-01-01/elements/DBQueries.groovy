import net.pricefx.formulaengine.DatamartContext
import net.pricefx.formulaengine.DatamartRowSet

/**
 * The library utility class which is responsible for having automated the most commonly required queries. This library
 * will be added with more functions as more common cases for quering DB are done. Idea is the element logic remains
 * as small as possible.
 */

/**
 * This method takes care of invoking any roll-up or DS or DM and returning the results of it as a Matrix2D.
 * @param fieldMap
 * @param sourceName
 * @param filters
 * @return empty map if no rows are fetched otherwise the data with key as the alias and value of the respective row
 */
def getDataFromSource(Map fieldMap, String sourceName, List filters, String sourceType) {
    def ctx = api.getDatamartContext()
    def dm = getSourceReference(ctx, sourceType, sourceName)
    def query = ctx.newQuery(dm)
    fieldMap.each { field, alias ->
        query.select(field, alias)
    }
    if (filters) {
        query.where(*filters)
    }
    def result = ctx.executeQuery(query)?.getData()
    result = (result) ?: [:]
    return result
}

def getDataFromSource(Map fieldMap, String sourceName, List filters, List orderByFieldsWithOrder, String sourceType) {
    def ctx = api.getDatamartContext()
    def dm = getSourceReference(ctx, sourceType, sourceName)
    def query = ctx.newQuery(dm)
    fieldMap.each { field, alias ->
        query.select(field, alias)
    }
    if (filters) {
        query.where(*filters)
    }
    if (orderByFieldsWithOrder && orderByFieldsWithOrder.size() > 0) {
        def stringArray = (orderByFieldsWithOrder as String[])
        query.orderBy(stringArray)
    }
    def result = ctx.executeQuery(query)?.getData()
    result = (result) ?: [:]
    return result
}

/**
 *
 * @param fieldMap
 * @param ppTable
 * @param filters
 * @param sortByField
 */
def getDataFromPPTable(Map fieldMap, String ppTable, List filters, String sortByField) {

}

/**
 *  This method gets the first N data rows from the source. The N cannot be more than 20K. If its more then the method
 *  explicitly resets it to 20K.
 * @param fieldMap
 * @param sourceName
 * @param filters
 * @param numberOfRows
 * @param sourceType
 * @param orderByFieldsWithOrder
 * @return
 */
def getFirstNDataRows(Map fieldMap, String sourceName, List filters, Integer numberOfRows, String sourceType, List orderByFieldsWithOrder) {
    def ctx = api.getDatamartContext()
    def dm = getSourceReference(ctx, sourceType, sourceName)
    def query = ctx.newQuery(dm)
    fieldMap.each { field, alias ->
        query.select(field, alias)
    }
    if (filters) {
        query.where(*filters)
    }

    if (orderByFieldsWithOrder && orderByFieldsWithOrder.size() > 0) {
        def stringArray = (orderByFieldsWithOrder as String[])
        query.orderBy(stringArray)
    }
    numberOfRows = (numberOfRows > 0 && numberOfRows < 20001) ? numberOfRows : 20000
    query.setMaxRows(numberOfRows)

    def result = ctx.executeQuery(query)?.getData()
    result = (result) ?: [:]
    return result
}

/**
 * This method takes care of initializing the query appropriately with the set of parameters supplied
 * @param query
 * @param fieldMap
 * @param filters
 * @param orderbyField
 */
def initializeQuery(query, Map fieldMap, List filters, String orderbyField) {
    fieldMap.each { field, alias ->
        query.select(field, alias)
    }
    query.where(*filters)
    query.orderBy(orderbyField)
}

/**
 * This method is used for joining the query and returning the data. As of now it supports upto 4 tables join. If more
 * is required which is strictly to be justified then modify the method to add the fourth list as well.
 * @param joinSourcesMapsList
 * @param joinSQL
 * @return
 */
def executeJoinQueryToFetchResult(List joinSourcesMapsList, String joinSQL) {
    def ctx = api.getDatamartContext()
    def queryList = []
    int i = 0
    joinSourcesMapsList.each { mapOfFields ->
        queryList[i] = ctx.newQuery(getSourceReference(ctx, mapOfFields['SourceType'], mapOfFields['Source']))
        initializeQuery(queryList[i], mapOfFields['Fields'], mapOfFields['Filters'], mapOfFields['OrderBy'])
        i++
    }
    if (i > 4) {
        api.logWarn("Since Query object contains join for more than 4 tables aborting it. " +
                "Do change implementation of this method and then proceed. Join Query is (" + joinSQL + ")")
        api.abortCalculation()
    }
    def result = getResultFromQueries(i, ctx, joinSQL, queryList)
    return getOutputRows(result)
}

def getOutputRows(result) {
    int rowCount = (result) ? result.getRowCount() : 0
    def outputRows = []
    int i = 0
    while (i < rowCount) {
        outputRows.add(result.getRowValues(i))
        i++
    }
    return outputRows
}

def getResultFromQueries(int noOfJoinQueries, DatamartContext ctx, String joinSQL, List queryList) {
    switch (noOfJoinQueries) {
        case 2:
            return ctx.executeSqlQuery(joinSQL, queryList[0], queryList[1])
        case 3:
            return ctx.executeSqlQuery(joinSQL, queryList[0], queryList[1], queryList[2])
        case 4:
            return ctx.executeSqlQuery(joinSQL, queryList[0], queryList[1], queryList[2], queryList[3])
    }
    return null
}

// Sub method to get the source details as per the source type
def getSourceReference(DatamartContext ctx, String sourceType, String source) {
    switch (sourceType) {
        case 'Rollup':
            return ctx.getRollup(source)
        case 'DataSource':
            return ctx.getDataSource(source)
        case 'DataMart':
            return ctx.getDatamart(source)
    }
    return null
}

/**
 * Insert one record into the target data soruce. This is intended to be used by data load where target is defined.
 * @param records
 * @return
 */
def insertRecordIntoTargetRowSet(Map rowToInsert) {
    DatamartRowSet targetRowSet = api.getDatamartRowSet("target")
    if (targetRowSet) {
        targetRowSet.addRow(rowToInsert)
    } else {
        lib.TraceUtility.developmentTrace("Printing Target Record", rowToInsert)
    }
}

/**
 * This method creates a tempTable in-memory.
 * @param sourceTable
 * @param fieldMap
 * @param filters
 * @param tempTableName
 * @return True if tempTable is created otherwise false
 */
def createTempTableFromSourceTable(String sourceTable, Map fieldMap, List filters, String tempTableName) {
    def dmCtx = api.getDatamartContext()
    def sourceDM = dmCtx.getTable(sourceTable)
    def query = dmCtx.newQuery(sourceDM, true)
    fieldMap.each { field, alias ->
        query.select(field, alias)
    }
    if (filters) {
        query.where(*filters)
    }
    def result = dmCtx.executeQuery(query)
    if (result) {
        def ctx = api.getTableContext()
        ctx.createTable(tempTableName, result)   //save the result into memory
        return true
    }
    return false
}

/**
 * gets the data from the already created tempTable using the fieldMap and filter condition.
 * @param tempTable
 * @param fieldMap
 * @param filters
 * @return
 */
def getDataFromTempTable(String tempTable, Map fieldMap, List filters) {
    def tableCtx = api.getTableContext()
    def query = tableCtx.newQuery(tempTable)
    fieldMap.each { field, alias ->
        query.select(field, alias)
    }
    if (filters) {
        StringBuilder builder = new StringBuilder("")
        boolean isFirstTime = true
        fieldMap.each { filter ->
            if (isFirstTime) {
                isFirstTime = false
            } else
                builder.append(' and ')
            builder.append(filter)
        }
        query.where(builder.toString())
        lib.TraceUtility.developmentTrace("Value...", builder.toString())
    }
    def result = tableCtx.executeQuery(query)?.getData()
    result = (result) ?: [:]
    return result
}

/**
 * This method stores the data in the target table using the table name.
 * @param data
 * @return
 */
def storeData(ArrayList data, String dmdsName) {
    def alldata = []
    if (data && data.size() > 0) {
        def dataToSend
        for (oneItem in data) {
            api.global.myHeader = oneItem?.keySet()
            // data to be send are only values in the same order as columns defined above
            dataToSend = oneItem?.values()
            alldata.add(dataToSend)
        }
    }
    def currentDs = api.find("DMDS", Filter.equal("uniqueName", dmdsName))
    def backupAddPath = 'datamart.loaddata/' + currentDs[0].typedId
    doBoundCall(backupAddPath, alldata, api.global.myHeader)
}

// header is important to be sent in a separate element for mass upload
def doBoundCall(path, data, header) {
    def myData = api.jsonEncode(["data": ["header": header, "data": data]])
    def copyResponse = api.boundCall("haefele", path, myData)
    lib.TraceUtility.developmentTrace(".........Respond from bound call.......", copyResponse)
    return copyResponse
}

/**
 * Insert data into the lookup table name
 * @param fieldMap
 * @param ppTableName
 * @param typeCode
 */
def insertIntoPPTable(Map fieldMap, String ppTableName, String typeCode) {
    def pp = api.findLookupTable(ppTableName)
    if (!pp || !typeCode) {
        api.addWarning("Pricing parameter $ppTableName of type $typeCode not found")
        return
    }
    def record = ['lookupTableId'  : pp.id,
                  'lookupTableName': ppTableName]
    fieldMap.each { fieldName, fieldValue ->
        record[fieldName] = fieldValue
    }
    def Obj = api.addOrUpdate(typeCode, record)
    libs.__LIBRARY__.TraceUtility.developmentTrace('The object added in PP Table', Obj)
}

/**
 * This method takes care of add or Update into the PX table.
 * @param fieldMap
 * @param pxTableName
 * @param pxType
 * @return
 */
def insertIntoPXTable(Map fieldMap, String pxTableName, String pxType) {
    def record = ['name': pxTableName]
    fieldMap.each { fieldName, fieldValue ->
        record[fieldName] = fieldValue
    }
    def Obj = api.addOrUpdate(pxType, record)
    libs.__LIBRARY__.TraceUtility.developmentTrace('The object added in PP Table', Obj)
}
/*
api.addOrUpdate("PX3", ["name": "Elasticity", "sku": sku, "attribute1": elasticity])
*/

void deleteRecord (Map rowToDelete) {
    DatamartRowSet targetRowSet = api.getDatamartRowSet("target")
    if (targetRowSet) {
        targetRowSet.deleteRow(rowToDelete)
    } else {
        lib.TraceUtility.developmentTrace("Printing Target Record", rowToDelete)
    }
}




