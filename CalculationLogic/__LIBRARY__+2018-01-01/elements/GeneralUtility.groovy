/**
 * This function gets the year list starting from current year and till user supplied number of years are computed.
 * @param noOfYears
 * @return List containing years in integer form
 */
def getYearsListFromNow(int noOfYears) {
    int currentYear = Integer.parseInt(new java.util.Date().format("yyyy"))
    int i = 0
    def years = []
    while (i < noOfYears) {
        years.add(currentYear - i)
        i++
    }
    return years
}

/**
 * The default value for user entring the number.
 * @param currentEntry
 * @return
 */
def getDefaultNumber(Integer currentEntry) {
    return (currentEntry) ?: 0
}

/**
 * The default value for Big Decimal entry
 * @param decimal
 * @return
 */
def getDefaultDecimal(BigDecimal decimal) {
    return (decimal) ?: 0.0 as BigDecimal
}

/**
 * Find the difference between dates Provided currentDate >= oldDate
 * @param currentDate
 * @param oldDate
 * @return
 */
def getMonthsBetweenDates(Date currentDate, Date oldDate) {
    if (!currentDate || !oldDate)
        return 0

    int yearDiff = Integer.parseInt(currentDate.format("YYYY")) - Integer.parseInt(oldDate.format("YYYY"))
    int monthDiff = Integer.parseInt(currentDate.format("MM")) - Integer.parseInt(oldDate.format("MM")) + 1
    return (yearDiff * 12 + monthDiff)
}

/**
 * The default value for Big Decimal entry
 * @param decimal
 * @return
 */
def getDefaultDate(def value) {
    Date dateToReturn = getDateForString(libs.__LIBRARY__.HaefeleConstants.DEFAULT_ORIGIN_DATE)

    if (value) {
        if (value instanceof  Date) {
            dateToReturn = value
        } else {
            dateToReturn = getDateForString (value)
        }
    }

    return dateToReturn
}

Date getDateForString (String dateInString) {
    return api.parseDate("dd-MM-yyyy", dateInString)
}


/**
 * Get the count of years from 2017 till date
 * @return
 */
def getYearCountFrom2018() {
    def currentDate = api.targetDate()
    if (currentDate)
        return Integer.parseInt(currentDate.format("YYYY")) - 2017
    return 0
}

/**
 * This function returns a filter which will take appropriate input and form the condition & return the filters.
 * @param startDtField
 * @param endDtField
 * @param fromDate
 * @param toDate
 * @return
 */
def getFilterConditionForDateRange(String startDtField, String endDtField, String fromDate, String toDate) {
    Filter filterForVFrmLTESTDT = Filter.and(Filter.lessOrEqual(startDtField, fromDate), Filter.greaterOrEqual(endDtField, fromDate))
    Filter filterForVFrmGTSTDT = Filter.and(Filter.greaterThan(startDtField, fromDate), Filter.lessOrEqual(startDtField, toDate))

    return Filter.or(filterForVFrmLTESTDT, filterForVFrmGTSTDT)
}

/**
 * Utility method to read all rows
 * @param finder
 * @return
 * Example engine usage:
 *
 * List products = readAllRows() { int startRow, int maxRow ->
 *             api.find("P", startRow, maxRow, 'sku', ['sku'], true, articleFilter)
 *}
 */
List readAllRows(Closure finder) {
    int startRow = 0
    List allRows = []
    int maxRow = api.getMaxFindResultsLimit()
    while (result = finder.call(startRow, maxRow)) {
        startRow += result.size()
        allRows.addAll(result)
    }
    return allRows
}