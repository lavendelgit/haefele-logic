def maxDiscountPerc() {
    if (!api.global.maxDiscountPerc) {
        def maxDiscountPerc = libs.__LIBRARY__.GeneralSettings.percentageValue("MaxDiscountPct")

        if (maxDiscountPerc == null) {
            api.addWarning("MaxDiscountPct is missing in GeneralSettings.")
            return null;
        }

        api.global.maxDiscountPerc = maxDiscountPerc
    }

    return api.global.maxDiscountPerc
}

def minMargin() {
    if (!api.global.minMargin) {
        def minMargin = libs.__LIBRARY__.GeneralSettings.percentageValue("MinMarginPct")

        if (minMargin == null) {
            api.addWarning("MinMarginPct is missing in GeneralSettings.")
            return null;
        }

        api.global.minMargin = minMargin
    }

    return api.global.minMargin
}

def minSurcharge() {
    if (!api.global.minSurcharge) {
        def minMarginPerc = minMargin()

        if (minMarginPerc == null) {
            api.addWarning("MinMarginPct is missing in GeneralSettings.")
            return null;
        }

        api.global.minSurcharge = 1 / (1 - minMarginPerc) - 1
    }

    return api.global.minSurcharge
}

def previousYearSalesHistory(sku) {
    if (api.isSyntaxCheck()) {
        return
    }
    def lib = libs.__LIBRARY__
    def currentYear = api.calendar().get(Calendar.YEAR);
    def filterPreviousYear = Filter.equal("YearMonthYear", currentYear - 1)

    def filters = [
            filterPreviousYear,
            *lib.Filter.customersGermany()
    ]

    return lib.Find.yearlySalesHistory(sku, *filters)[0] ?: [:]
}

def avgSalesPriceLast6M(sku) {
    def lib = libs.__LIBRARY__
    def calendar = api.calendar()
    calendar.add(Calendar.MONTH, -6)
    calendar.set(Calendar.DAY_OF_MONTH, 1)
    def sixMonthsAgo = calendar.getTime()

    def filters = [
            Filter.equal("Sku", sku),
            Filter.greaterOrEqual("YearMonth", sixMonthsAgo),
            *lib.Filter.customersGermany()
    ]

    def dmCtx = api.getDatamartContext()
    def salesDM = dmCtx.getTable("SalesHistoryDM")
    def datamartQuery = dmCtx.newQuery(salesDM)

    datamartQuery.select("AVG(Revenue/unitsSold)", "avgSalesPrice")
    datamartQuery.where(filters)
    datamartQuery.setMaxRows(1)

    def result = dmCtx.executeQuery(datamartQuery)

    return result?.getData()?.collect()?.find()?.avgSalesPrice
}

def calculate(String sku, BigDecimal newSalesPrice) {
    if (api.isSyntaxCheck()) {
        return
    }
    def lib = libs.__LIBRARY__
    def warning = ""
    def baseCost = lib.Find.currentBaseCost(sku)
    def currentSalesPriceRecord = lib.Find.currentSalesPriceAnyType(sku)
    def currentSalesPrice = currentSalesPriceRecord?.salesPrice
    def maxDiscountPerc = maxDiscountPerc()
    def minMargin = minMargin()
    def minSurcharge = minSurcharge()
    def salesHistory = previousYearSalesHistory(sku)
    def previousYearQuantity = salesHistory?.unitsSold
    def previousYearRevenue = salesHistory?.revenue
    def previousYearMarginAbs = salesHistory?.marginAbs
    def avgSalesPriceLast6M = avgSalesPriceLast6M(sku)

    def minMarginPrice = null
    def minMarginPriceBeforeDiscounts = null
    def recommendedSalesPrice = null
    def avgSalesPrice = null
    def priceFactor = null
    def quantityCompensatingRevenue = null
    def quantityCompensatingMargin = null
    def quantityIncreaseBasedOnRevenue = null
    def quantityIncreaseBasedOnMargin = null

    if (baseCost != null && minSurcharge != null) {
        minMarginPrice = lib.Rounding.roundMoney(baseCost * (1 + minSurcharge))
    }

    if (minMarginPrice != null && maxDiscountPerc != null) {
        minMarginPriceBeforeDiscounts = (minMarginPrice / (1 - maxDiscountPerc)).setScale(2, BigDecimal.ROUND_HALF_UP)
    }

    if (newSalesPrice != null & minMarginPriceBeforeDiscounts != null) {
        recommendedSalesPrice = newSalesPrice < minMarginPriceBeforeDiscounts ? minMarginPriceBeforeDiscounts : newSalesPrice
        recommendedSalesPrice = lib.Rounding.roundMoney(recommendedSalesPrice)
    }

    if (salesHistory != null && salesHistory.revenue != null
            && salesHistory.unitsSold != null && salesHistory.unitsSold != 0) {
        avgSalesPrice = salesHistory.revenue / salesHistory.unitsSold
    }

    if (avgSalesPrice != null && currentSalesPrice != null && currentSalesPrice != 0) {
        priceFactor = avgSalesPrice/(currentSalesPrice / 100)
    }

    if (recommendedSalesPrice != null && priceFactor != null && previousYearRevenue != null) {
        def expectedUnitSalesPrice = (recommendedSalesPrice/100) * priceFactor
        quantityCompensatingRevenue = (previousYearRevenue / expectedUnitSalesPrice).setScale(0, BigDecimal.ROUND_HALF_UP)
    }

    if (recommendedSalesPrice != null && priceFactor != null && baseCost != null && previousYearMarginAbs) {
        def expectedMarginAbs = recommendedSalesPrice * priceFactor - baseCost
        def expectedMarginAbsPerUnit = expectedMarginAbs/100
        quantityCompensatingMargin = (previousYearMarginAbs / expectedMarginAbsPerUnit).setScale(0, BigDecimal.ROUND_HALF_UP)
    }

    if (quantityCompensatingRevenue != null && previousYearQuantity != null && previousYearQuantity != 0) {
        quantityIncreaseBasedOnRevenue = ((quantityCompensatingRevenue - previousYearQuantity) / previousYearQuantity).toBigDecimal()
    }

    if (quantityCompensatingMargin != null && previousYearQuantity != null && previousYearQuantity != 0) {
        quantityIncreaseBasedOnMargin = ((quantityCompensatingMargin - previousYearQuantity) / previousYearQuantity).toBigDecimal()
    }

    // Warnings

    if (baseCost == null) {
        warning += "Missing base cost. "
    }
    if (currentSalesPrice == null) {
        warning += "Missing CurrentSalesPrice. "
    }
    if (avgSalesPrice == null) {
        warning += "No sales history. "
    }

    return [
            baseCost: baseCost,
            currentSalesPrice: currentSalesPrice,
            salesPriceType: currentSalesPriceRecord?.type,
            newSalesPrice: newSalesPrice,
            maxDiscountPerc: maxDiscountPerc,
            minMargin: minMargin,
            minSurcharge: minSurcharge,
            minMarginPrice: minMarginPrice,
            minMarginPriceBeforeDiscounts: minMarginPriceBeforeDiscounts,
            priceFactor: priceFactor,
            recommendedSalesPrice: recommendedSalesPrice,
            avgSalesPrice: avgSalesPrice?.setScale(2, BigDecimal.ROUND_HALF_UP),
            avgSalesPriceLast6M: avgSalesPriceLast6M?.setScale(2, BigDecimal.ROUND_HALF_UP),
            previousYearQuantity: previousYearQuantity,
            quantityCompensatingRevenue: quantityCompensatingRevenue,
            quantityCompensatingMargin: quantityCompensatingMargin,
            quantityIncreaseBasedOnRevenue: quantityIncreaseBasedOnRevenue,
            quantityIncreaseBasedOnMargin: quantityIncreaseBasedOnMargin,
            warning: warning
    ]
}