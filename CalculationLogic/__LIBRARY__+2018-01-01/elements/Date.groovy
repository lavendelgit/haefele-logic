def yearAgoFirstOfMonth() {
    def calendar = api.calendar()
    calendar.add(Calendar.MONTH, -12)
    calendar.set(Calendar.DAY_OF_MONTH, 1)

    return calendar.getTime()
}

def previousYear() {
    def currentYear = api.calendar().get(Calendar.YEAR);
    return currentYear - 1
}