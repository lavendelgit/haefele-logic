def customer(label) {
    def attrs = [
            "Sales Office"           : "attribute3",
            "Customer Class"         : "attribute5",
            "Customer Discount Group": "attribute6"
    ]
    return attrs[label]
}

def product(label) {
    def attrs = [
            "Supplier Id"             : "attribute8",
            "Supplier Name"           : "attribute9",
    ]
    return attrs[label]
}