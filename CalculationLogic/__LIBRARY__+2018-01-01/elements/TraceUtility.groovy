/**
 * This library is for api.trace and not delete them once you are done with development...
 * 1) Its suggested to have element ForDevelopment and initialize the global variable api.global.printTrace to True
 *     there. Note that if you are using DataFeeder or Target DS based input and you want to skip that. You can have
 *     another flag called AvoidDuringDevelopment for user inputs and initialize them to fixed value in this element.
 * 2) Also if you want to print a List or Map use that without converting them to string. So use your situation specific
 *     developmentTrace () method. Note that if you add ' "<Some or Empty String>" + List' or
 *     ' "<Some or Empty String>" + Map' the argument will be printed. Otherwise the performance issues will still exist
 * 3) The third argument defines basically how many elements will be printed. By Default the value is 10.
 * Basically the debugging efforts should be re-used to use it at later point of time as well.
 */
/**
 * This method takes care of printing individual key and value in the API trace only during development.
 * @param title
 * @param mapToPrint : Map that you got it.
 * @param maxPrint : default is 10
 * @return
 */
def developmentTrace(String title, Map mapToPrint, int maxPrint = 10) {
    if (api.global.printTrace) {
        int i = 0
        api.trace(title + "Start", " Map Printing only first  (" + maxPrint + ") of (" + mapToPrint?.size() + ")")
        mapToPrint.each { key, value ->
            if (i <= maxPrint) {
                api.trace(title + i, "Key(" + key + ") Value (" + api.jsonEncode(value) + ")")
            }
            i++
        }
    }
}

/**
 * This method takes care of printing individual elements in list in the API trace only during development.
 * @param title
 * @param listToPrint
 * @param maxPrint : default is 10
 * @return
 */
def developmentTrace(String title, List listToPrint, int maxPrint = 10) {
    if (api.global.printTrace) {
        int i = 0
        api.trace(title + "Start", " List Printing only first (" + maxPrint + ") of (" + listToPrint?.size() + ")")
        listToPrint?.each { it ->
            if (i <= maxPrint) {
                api.trace(title + i, "Value (" + api.jsonEncode(it) + ")")
            }
            i++
        }
    }
}

/**
 * This method prints the complete map as Map  (not individual elements)
 * @param title
 * @param mapToPrint
 * @return
 */
def developmentTraceRow(String title, Map mapToPrint, boolean continueFromPreviousCount = true) {
    if (api.global.printTrace) {
        def suffix = ""
        if (continueFromPreviousCount) {
            api.global.mapRowCount = (api.global.mapRowCount) ? api.global.mapRowCount++ : 0
            suffix = "" + api.global.mapRowCount
        }
        api.trace(title + " $suffix", "Value (" + mapToPrint + ")")
    }
}

/**
 * This method takes care of printing the list as list (not individual elements)
 * @param title
 * @param listToPrint
 * @return
 */
def developmentTraceRow(String title, List listToPrint, boolean continueFromPreviousCount = true) {
    if (api.global.printTrace) {
        def suffix = ""
        if (continueFromPreviousCount) {
            api.global.listRowCount = (api.global.mapRowCount) ? api.global.listRowCount++ : 0
            suffix = "" + api.global.listRowCount
        }
        api.trace(title + " $suffix", "Value (" + listToPrint + ")")
    }
}
/**
 * This method takes care of printing the map in the API trace only during development.

 * Basically the debugging efforts should be re-used to use it at later point of time as well.
 * @param title
 * @param arguments
 */
def developmentTrace(String title, Object argument) {
    if (api.global.printTrace) {
        api.trace(title, "Value (" + argument + ")")
        api.logInfo(title, "Value (" + argument + ")")
    }
}

/**
 * Prints the exception string equivalent
 * @param title
 * @param exception
 * @return
 */
def developmentTraceException(String title, Exception exception) {
    if (api.global.printTrace) {
       // def message = exception.getStackTrace()
        StringBuilder builder = new StringBuilder("")
        builder.append(exception.toString())
      /*  int i = 0
        while (i < message.length) {
            builder.append('__' + message[i].toString())
            i++
        }*/
        api.trace(title, "Value (" + builder.toString() + ")")
        api.logWarn(" The exception details are... ", builder.toString())
    }
}

/**
 * Print all the elements in the list..
 * @param title
 * @param data
 * @return
 */
def developmentTraceList(String title, List data) {
    if (api.global.printTrace) {
        StringBuilder builder = new StringBuilder("")
        int i = 0
        builder.append("size + ${data.size()}")
        while (i < data.size()) {
            builder.append('__' + data.get(i))
            i++
        }
        api.trace(title, "Value (" + builder.toString() + ")")
    }
}
