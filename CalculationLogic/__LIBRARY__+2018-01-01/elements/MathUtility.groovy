import java.math.RoundingMode

/**
 * This method helps in taking care that when divisor is zero for BigDecimal input it simply returns the dividend
 * as if behaving the divisor is 1.
 * @param dividend
 * @param divisor
 * @param scale
 */
def divide(BigDecimal dividend, BigDecimal divisor, int scale = 2) {
    if (!divisor)
        return (dividend).setScale(scale, RoundingMode.HALF_UP)

    return (dividend / divisor).setScale(scale, RoundingMode.HALF_UP)
}

/**
 * This method helps in taking care that when divisor is zero for Double input it simply returns the dividend
 * as if behaving the divisor is 1.
 * @param dividend
 * @param divisor
 * @param scale
 * @return
 */
def divide(Double dividend, BigDecimal divisor, int scale = 4) {
    if (!divisor) {
        if (dividend instanceof BigDecimal) {
            return ((dividend) ?: 0.0).round(scale)
        }
        return 0.0
    }

    dividend = (dividend) ?: 0.0
    return (dividend / divisor).round(scale)
}

def percentage(String completeValue, String achievedValue, int scale = 4) {
    return percentage(completeValue as BigDecimal, achievedValue as BigDecimal, scale)
}

/**
 * This method helps in finding percentage of one value with respect to other value for BigDecimal input
 * it simply returns the achievedValue*100 as % if complete value is zero.
 * @param completeValue
 * @param achievedValue
 * @param scale
 * @return
 */
def percentage(BigDecimal completeValue, BigDecimal achievedValue, int scale = 4) {
    if (!completeValue)
        return (((achievedValue) ?: new BigDecimal(0.0)) * 100).setScale(scale, RoundingMode.HALF_UP)

    achievedValue = (achievedValue) ?: new BigDecimal(0.0)
    return (achievedValue / completeValue).setScale(scale, RoundingMode.HALF_UP)
}

/**
 * This method computed the CAGR for any BigDecimal values given the standard 3 parameters.
 * @param endingValue
 * @param startingValue
 * @param numberOfYears
 * @return
 */
def cagr(BigDecimal endingValue, BigDecimal startingValue, int numberOfYears, int scale=4) {
    if (!(startingValue) || numberOfYears==0)
        return 0
    BigDecimal mantissaComputation = endingValue / startingValue
    double powerValue = 1.0 / numberOfYears
    double powerOutput = Math.pow(mantissaComputation, powerValue)
    BigDecimal computedResult = 0.0
    if (powerOutput.toString() != "NaN") {
        computedResult = powerOutput - 1
        computedResult.setScale(scale, RoundingMode.HALF_UP)
    }
    return computedResult
}

/* TODO: Test cases for cagr, do not delete
api.trace("Reduced growth in future", " Case 1 (" + cagr(1013.33 as BigDecimal, 1868.00 as BigDecimal, 2) + ")")
api.trace("Reduced growth in future", " Case 2 (" + cagr(4502.42 as BigDecimal, 2312.00 as BigDecimal, 2) + ")")
api.trace("Reduced growth in future", " Case 2 (" + cagr(-4 as BigDecimal, 0.5 as BigDecimal, 2) + ")")
*/