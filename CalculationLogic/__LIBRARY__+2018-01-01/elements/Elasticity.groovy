def calculate(String sku, perfeclyElasticTreshold, Filter... additionalFilters) {
    def dmQueryResult = libs.__LIBRARY__.Find.priceElasticity(sku, additionalFilters)
    def records = dmQueryResult?.getData()?.collect()
                        .findAll { r -> r.quantity != null && r.quantity > 0 && r.price != null && r.price != 0};

    if (!records || records.size() < 2) {
        return null;
    }

    def minPriceRecord = records?.min { r -> r.price }
    def maxPriceRecord = records?.max { r -> r.price }

    def quantityChange = (minPriceRecord.quantity - maxPriceRecord.quantity) / minPriceRecord.quantity
    def priceChange = (minPriceRecord.price - maxPriceRecord.price) / minPriceRecord.price

    def elasticity = null;

    if (priceChange == 0) {
        elasticity = null; // can't divide by 0
    } else if (quantityChange != 0) {
        elasticity = Math.abs(quantityChange / priceChange)
    } else {
        elasticity = perfeclyElasticTreshold
    }

    api.trace("records", "", records)
    api.trace("minPriceRecord", "", minPriceRecord)
    api.trace("maxPriceRecord", "", maxPriceRecord)
    api.trace("quantityChange", "", quantityChange)
    api.trace("priceChange", "", priceChange)

    return elasticity?.toBigDecimal()
}

def name(BigDecimal value, BigDecimal perfectlyElasticThreshold) {
    if (value == null) {
        return "Not enough information"
    }
    if (value >= perfectlyElasticThreshold) {
        return "Perfectly elastic"
    } else if (value > 1) {
        return "Relatively elastic"
    } else if (value == 1) {
        return "Unitary elastic"
    } else if (value > 0) {
        return "Relatively inelastic"
    } else if (value == 0) {
        return "Perfectly inelastic"
    } else {
        throw new IllegalArgumentException("Unsupported elasticity value")
    }
}

def color (BigDecimal value, BigDecimal perfectlyElasticTreshold) {
    if (value == null) {
        return "#45aaf2" // blue
    }
    if (value >= perfectlyElasticTreshold) {
        return "#fc5c65" // red
    } else if (value >= 1) {
        return "#fed330" // yellow
    } else {
        return "#26de81" // green
    }
}

def find(sku) {
    api.find("PX3", 0, 1, "sku", Filter.equal("name", "Elasticity"), Filter.equal("sku", sku))?.find()?.attribute1
}