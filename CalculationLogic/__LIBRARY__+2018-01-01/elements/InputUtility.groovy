/**
 * Creates a combo-box or a multi-select box
 * @param name
 * @param label
 * @param values
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object multiSelectBoxEntry(String name, String label, List values, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.options(name, values)
    }
}

/**
 * Creates a single-select box
 * @param name
 * @param label
 * @param values
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object selectBoxEntry(String name, String label, List values, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.option(name, values)
    }
}

/**
 * Creates a single-select box with Map as one of the input to support Key/Value pair
 * @param name
 * @param label
 * @param valuesTextMap
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object selectBoxEntry(String name, String label, Map valuesTextMap, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        List values = valuesTextMap?.keySet() as List
        def input = api.option(name, values)
        api.getParameter(inputName)?.setConfigParameter("labels", valuesTextMap)
        return input
    }
}

/**
 * Creates a decimal user entry
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object decimalEntry(String name, String label, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.decimalUserEntry(inputName)
    }
}

/**
 * Creates a numeric user entry
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object numericEntry(String name, String label, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.integerUserEntry(inputName)
    }
}

/**
 * Creates a text user entry
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object textEntry(String name, String label, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.stringUserEntry(inputName)
    }
}

/**
 * Creates a date usere entry
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object dateEntry(String name, String label, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.dateUserEntry(inputName)
    }
}

/**
 * Creates a boolean user entry
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object booleanEntry(String name, String label, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.booleanUserEntry(inputName)
    }
}

/**
 * Creates a product filter
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object productFilterEntry(String name, String label, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.filterBuilderUserEntry(inputName, 'P')
    }
}

/**
 * Creates a datamart filter
 * @param name
 * @param label
 * @param datamartName
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object datamartFilterEntry(String name, String label, String datamartName, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.datamartFilterBuilderUserEntry(inputName, datamartName)
    }
}

/**
 * Creates a input entry
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @param inputCreator
 * @return
 */
Object newInput(String name, String label, boolean isRequired, defaultValue, Closure inputCreator) {
    def input = inputCreator.call(name)
    def param = api.getParameter(name)
    if (param) {
        param.setLabel(label)
        param.setRequired(isRequired)
        if (param.getValue() == null && defaultValue != null) {
            param.setValue(defaultValue)
        }
    }
    return input
}