def decimalValue(name) {
    def values = api.findLookupTableValues("GeneralSettings", Filter.equal("name", name))
    if (values[0] && values[0].value) {
        return values[0].value.toBigDecimal()
    }
    return 0
}

def percentageValue(name) {
    return decimalValue(name)?.div(100)
}