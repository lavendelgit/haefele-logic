import groovy.transform.Field

/**
 * Returns the filters to be used whenever any data is fetched corresponding to ZPAP for Haefele
 * @return
 */
def getCustomerSalesPriceFilters() {
    def filters = [
            Filter.notEqual('ProdhII', 'ZI Häfele-interne'),
            Filter.not(Filter.ilike('Material', "999.%")),
            Filter.equal('MatchType', 'HLM')
    ]
    return filters
}

def getCustomerSalesPriceFiltersForAllMatches() {
    def filters = [
            Filter.notEqual('ProdhII', 'ZI Häfele-interne'),
            Filter.not(Filter.ilike('Material', "999.%"))
    ]
    return filters
}

/**
 * This method returns the common transaction DM filters that is recommended for selecting reocrds for
 * Germany region only
 * @return
 */
def getTransactionDMFilters() {
    def filters = [
            Filter.notEqual('ProdhII', "ZI Häfele-interne"),
            Filter.not(Filter.ilike('Material', '999.%')),
            Filter.equal('KundenHandelspartner', '-EXTERN-'),
            Filter.in('SalesOffice', ['DE01', 'DE02', 'DE03', 'DE04', 'DE05', 'DE06', 'DE07', 'DE08',
                                      'DE09', 'DE10', 'DE20', 'DEHQ'])
    ]
    return filters
}

/**
 * This method returns the common transaction DM filters that is recommended for selecting reocrds for
 * Germany region only
 * @return
 */
def getTransactionInlandFilters() {
    def filters = [
            Filter.in('SalesOffice', ['DE01', 'DE02', 'DE03', 'DE04', 'DE05', 'DE06', 'DE07', 'DE08',
                                      'DE09', 'DE10', 'DE20'])
    ]
    return filters
}

Filter getSalesOfficeInlandFilters(String salesOfficeNameField) {
    return Filter.in(salesOfficeNameField, ['Handel', 'München', 'Stuttgart Airport', 'Nürnberg',
                                            'Naumburg', 'Frankfurt', 'Köln', 'Münster', 'Berlin',
                                            'Hannover', 'Kaltenkirchen', 'KBC'])
}
/**
 * This method gets all the products which are active
 * @return
 */
def getProductsFilters() {
    def filters = [
            Filter.notEqual('attribute12', "ZI Häfele-interne"),
            Filter.not(Filter.ilike('sku', "999.%"))
    ]
    return filters
}

/**
 * @param fieldList
 * @param pxTableName
 * @param pxType
 * @param filters
 */
def loadZNRVHugePXTable() {
    if (!api.global.duplicatePxRecords)
        api.global.duplicatePxRecords = []

    List fieldList = ['id', 'sku', 'attribute1', 'attribute2', 'attribute3', 'attribute6', 'attribute7']
    String pxTableName = 'S_ZNRV'
    String pxType = 'PX50'
    List filters = [Filter.equal('name', "${pxTableName}")]
    def stream
    def result = [:]
    try {
        stream = api.stream(pxType, "sku", fieldList, *filters)
        libs.__LIBRARY__.TraceUtility.developmentTrace('Printing current record', 'Testin....')
        def record, fromDateMap, fromDate
        def printed = true
        while (stream?.hasNext()) {
            record = stream.next()
            if (printed) {
                libs.__LIBRARY__.TraceUtility.developmentTraceRow('Printing current record', record)
                printed = false
            }
            fromDateMap = result[record.sku]
            fromDateMap = (fromDateMap) ?: [:]
            fromDate = record.attribute1
            if (!fromDateMap[fromDate]) {
                fromDateMap[fromDate] = record
            } else {
                api.global.duplicatePxRecords.add(record.Id)
            }
            result[record.sku] = fromDateMap
        }
    } finally {
        if (stream) {
            stream.close()
        }
    }
    return result
}

/*
api.global.printTrace = true
loadZNRVHugePXTable()

*/

def deleteDuplicateZNRVEntry(List znrvList) {
    String pxTableName = 'S_ZNRV'
    String pxType = 'PX50'
    List filters
    znrvList.each { recordId ->
        filters = [:]
        filters['id'] = "$recordId"
        filters['name'] = pxTableName
        api.delete(pxType, filters)
    }
}

def getAbsoluteValue(def value) {
    if (value < BigDecimal.ZERO)
        return value * -1

    return value
}

//deleteDuplicateZNRVEntry ([])
@Field final String DEF_PPI_NSPI = 'Needs Sales Price Improvement'
