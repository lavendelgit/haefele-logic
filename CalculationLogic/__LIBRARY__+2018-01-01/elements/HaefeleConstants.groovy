import groovy.transform.Field

@Field final String MIN_CUSTOMER_AGE_IN_MONTHS = 'Minimum_Age_Of_Customers_In_Months'
@Field final String DEFAULT_ORIGIN_DATE = '01-01-2016'