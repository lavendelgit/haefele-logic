def calculatePoints(function, startX, endX, step) {
    def points = []
    for (def x = startX; x <= endX; x += step) {
        points.push([
                x: x.toBigDecimal(),
                y: function(x).toBigDecimal()
        ])
    }
    return points;
}
