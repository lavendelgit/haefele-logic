import java.math.RoundingMode

def targetDateFormatted() {
    return api.targetDate()?.format("yyyy-MM-dd")
}

def earliestSalesPriceRecord(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "S_ZPLP"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "attribute1" // Valid From (de: Gültig von) - asdcending
    def salesPrices = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (salesPrices[0]) {
        result = salesPrices[0]
    }

    api.trace("lib.Find.earliestSalesPriceRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}

def latestSalesPriceRecord(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "S_ZPLP"),
            Filter.like("sku", sku + "%"),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute1" // Valid From (de: Gültig von) - descending
    def salesPrices = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (salesPrices[0]) {
        result = salesPrices[0]
    }
    api.trace("lib.Find.latestSalesPriceRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}


def latestSalesPrice(sku, Filter... additionalFilters) {
    def record = latestSalesPriceRecord(sku, additionalFilters);

    def salesPrice = null

    if (record) {
        salesPrice = record.attribute3 // Sales Price (de: Verkaufspreis (Brutto))
    }

    if (!salesPrice) {
        api.redAlert("Sales Price is missing in PX SalesPrice")
    }

    api.trace("lib.Find.latestSalesPrice", "sku: ${sku}, additionalFilters: ${additionalFilters}", salesPrice)

    return salesPrice
}

def latestNetPriceRecord(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "S_ZPZ"),
            Filter.equal("sku", sku)
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute1" // Valid From (de: Gültig von) - descending
    def records = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (records[0]) {
        result = records[0]
    }

    api.trace("lib.Find.latestNetPriceRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}

def earliestNetPriceRecord(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "S_ZPZ"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "attribute1" // Valid From (de: Gültig von) - asdcending
    def records = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (records[0]) {
        result = records[0]
    }

    api.trace("lib.Find.earliestNetPriceRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}


private currentlyValid(validFromAttributeName = "attribute1", validToAttributeName = "attribute2") {
    return [
            Filter.lessOrEqual(validFromAttributeName, targetDateFormatted()),
            Filter.greaterOrEqual(validToAttributeName, targetDateFormatted())
    ]
}

def currentSalesPriceRecord(sku) {
    return latestSalesPriceRecord(sku, *currentlyValid())
}

def currentSalesPrice(sku) {
    return latestSalesPrice(sku, *currentlyValid())
}

def currentSalesPrice(sku, Filter... additionalFilters) {
    def filters = []
    if (additionalFilters) {
        filters.addAll(additionalFilters)
    }
    filters.addAll(*currentlyValid())

    return latestSalesPrice(sku, *filters)
}

/* ****************************************************************************************************************** */

def compoundPriceRecord(sku) {
    def filters = [
            Filter.equal("name", "CompoundPrice"),
            Filter.equal("sku", sku),
    ]

    def orderBy = "-attribute6" // Valid From - descending 
    def records = api.find("PX10", 0, 1, orderBy, *filters)
    def result = [:]

    if (records[0]) {
        result = records[0]
    }

    api.trace("lib.Find.compoundPriceRecord", "sku: ${sku}", result)

    return result
}

def latestCompoundPriceRecord(sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "CompoundPrice"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }

    def orderBy = "-attribute6" // Valid From - descending 
    def records = api.find("PX10", 0, 1, orderBy, *filters)
    def result = [:]

    if (records[0]) {
        result = records[0]
    }

    api.trace("lib.Find.compoundPriceRecord", "sku: ${sku}", result)

    return result
}


def latestSalesPriceAnyType(String sku, Filter... additionalFilters) {
    def tempSalesPrice = latestSalesPriceRecord(sku, additionalFilters)
    def tempNetPrice = latestNetPriceRecord(sku, additionalFilters)
    def salesPrice = getUnifiedMapForSalesPrice([tempSalesPrice.attribute1,
                                                 tempSalesPrice.attribute2,
                                                 tempSalesPrice.attribute3,
                                                 tempSalesPrice.attribute7,
                                                 tempSalesPrice.attribute9])
    def netPrice = getUnifiedMapForSalesPrice([tempNetPrice.attribute1,
                                               tempNetPrice.attribute2,
                                               tempNetPrice.attribute3,
                                               tempNetPrice.attribute9,
                                               tempNetPrice.attribute11])

    api.trace("1", salesPrice)
    api.trace("2", netPrice)
    def result = [salesPrice, netPrice].max { r -> r?.attribute1 } // attribute1 = valid from
    def salesPriceType = result == salesPrice ? "ZPLP" : "ZPZ"
    if (!result.isEmpty()) {
        return [
                type      : salesPriceType,
                validFrom : result.attribute1,
                validTo   : result.attribute2,
                salesPrice: result.attribute3,
                currency  : result.attribute5,
                priceUnits: result.attribute6
        ]
    }

    def compoundPrice = compoundPriceRecord(sku)

    if (!compoundPrice.isEmpty() && compoundPrice.attribute1 != null) {
        return [
                type      : "ZPHT", // Preis Hauptteil
                validFrom : targetDateFormatted(),
                validTo   : targetDateFormatted(),
                salesPrice: compoundPrice.attribute1,
                currency  : compoundPrice.attribute2,
                priceUnits: 100
        ]
    }

    return [:]
}

def currentSalesPriceAnyType(String sku) {
    def currentlyValid = currentlyValid()
    return latestSalesPriceAnyType(sku, *currentlyValid)
}

Map getUnifiedMapForSalesPrice(List valuesToMap) {
    if (!valuesToMap || valuesToMap.size() < 5) {
        api.throwException("Invalid SalesPrice")
    }

    return [attribute1: valuesToMap[0],
            attribute2: valuesToMap[1],
            attribute3: valuesToMap[2],
            attribute5: valuesToMap[3],
            attribute6: valuesToMap[4]]
}

def earliestSalesPriceAnyType(String sku, Filter... additionalFilters) {
    def tempSalesPrice = earliestSalesPriceRecord(sku, additionalFilters)
    def tempNetPrice = earliestNetPriceRecord(sku, additionalFilters)


    Map salesPrice = getUnifiedMapForSalesPrice([tempSalesPrice.attribute1,
                                                 tempSalesPrice.attribute2,
                                                 tempSalesPrice.attribute3,
                                                 tempSalesPrice.attribute7,
                                                 tempSalesPrice.attribute9])
    Map netPrice = getUnifiedMapForSalesPrice([tempNetPrice.attribute1,
                                               tempNetPrice.attribute2,
                                               tempNetPrice.attribute3,
                                               tempNetPrice.attribute9,
                                               tempNetPrice.attribute11])
    //api.trace("1", salesPrice)
    //api.trace("2", netPrice)
    def result = [salesPrice, netPrice].max { r -> r?.attribute1 } // attribute1 = valid from 
    def salesPriceType = result == salesPrice ? "ZPLP" : "ZPZ"
    if (!result.isEmpty()) {
        return [
                type      : salesPriceType,
                validFrom : result.attribute1,
                validTo   : result.attribute2,
                salesPrice: result.attribute3,
                currency  : result.attribute5,
                priceUnits: result.attribute6
        ]
    }

    def compoundPrice = compoundPriceRecord(sku)

    if (!compoundPrice.isEmpty() && compoundPrice.attribute1 != null) {
        return [
                type      : "ZPHT", // Preis Hauptteil
                validFrom : targetDateFormatted(),
                validTo   : targetDateFormatted(),
                salesPrice: compoundPrice.attribute1,
                currency  : compoundPrice.attribute2,
                priceUnits: 100
        ]
    }

    return [:]
}

/* ****************************************************************************************************************** */

def earliestBaseCostRecord(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "S_ZPE"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "attribute1" // Valid From (de: Gültig von) - ascending
    def baseCosts = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (baseCosts[0]) {
        result = baseCosts[0]
    }

    api.trace("lib.Find.earliestBaseCostRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}

def latestBaseCostRecord(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "S_ZPE"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute1" // Valid From (de: Gültig von) - descending
    def baseCosts = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (baseCosts[0]) {
        result = baseCosts[0]
    }

    api.trace("lib.Find.latestBaseCostRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}

private currentlyValidBaseCost(validFromAttributeName = "attribute1", validToAttributeName = "attribute2") {
    return [
            Filter.lessOrEqual(validFromAttributeName, targetDateFormatted()),
            Filter.greaterOrEqual(validToAttributeName, targetDateFormatted())
    ]
}

def latestBaseCost(String sku, Filter... additionalFilters) {
    def record = latestBaseCostRecord(sku, additionalFilters);

    def baseCost = null

    if (record) {
        baseCost = record.attribute3// Base Cost (de: Einstandspreis)
    }

    if (!baseCost) {
        api.redAlert("Base Cost is missing in PX SalesPrice")
    }

    api.trace("lib.Find.latestBaseCost", "sku: ${sku}, additionalFilters: ${additionalFilters}", baseCost)

    return baseCost
}

def currentBaseCostRecord(sku) {
    return latestBaseCostRecord(sku, *currentlyValidBaseCost())
}

def currentBaseCost(sku) {
    return latestBaseCost(sku, *currentlyValidBaseCost())
}

/* ****************************************************************************************************************** */

def yearlySalesHistory(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("Sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }

    def dmCtx = api.getDatamartContext()
    def salesDM = dmCtx.getTable("SalesHistoryDM")
    def datamartQuery = dmCtx.newQuery(salesDM)

    datamartQuery.select("SUM(Revenue)", "revenue")
    datamartQuery.select("SUM(UnitsSold)", "unitsSold")
    datamartQuery.select("SUM(MarginalContributionAbs)", "marginAbs")
    datamartQuery.select("COUNTDISTINCT(CustomerId)", "customersCount")
    datamartQuery.select("YearMonthYear", "year")
    datamartQuery.where(filters)

    def result = dmCtx.executeQuery(datamartQuery)

    def records = result?.getData()?.collect { r -> r << [year: r.year as Integer] } ?: []

    api.trace("lib.Find.salesHistory", "sku: ${sku}, additionalFilters: ${additionalFilters}", records)

    return records
}

def yearlySalesHistoryV2(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "SalesHistory"),
            Filter.equal("sku", sku)
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "attribute1" // Year 
    def fields = [
            "attribute1", // Year 
            "attribute2", // Customers Count 
            "attribute3", // Units Sold 
            "attribute4", // Revenue 
            "attribute5", // Margin Abs 
            "attribute6", // Margin Perc 
    ]
    def records = api.find("PX8", 0, 0, orderBy, fields, *filters) ?: []

    api.trace("lib.Find.salesHistory", "sku: ${sku}, additionalFilters: ${additionalFilters}", records)

    return records.collect { r ->
        [
                year          : r.attribute1 as Integer,
                customersCount: r.attribute2 as Integer,
                unitsSold     : r.attribute3,
                revenue       : r.attribute4,
                marginAbs     : r.attribute5,
                marginPerc    : r.attribute6
        ]
    }
}

def priceElasticity(String sku, Filter... additionalFilters) {
    def dmCtx = api.getDatamartContext()
    def salesDM = dmCtx.getTable("SalesHistoryDM")
    def datamartQuery = dmCtx.newQuery(salesDM)

    def filters = [
            Filter.equal("Sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }

    datamartQuery.select("AVG(Revenue/UnitsSold)", "price")
    datamartQuery.select("SUM(UnitsSold)", "quantity")
    datamartQuery.select("YearMonthMonth", "month")
    datamartQuery.where(*filters)
    datamartQuery.orderBy("month")
    datamartQuery.setOptions([
            regression: ["quantity", "price"] // y, x 
    ])

    def result = dmCtx.executeQuery(datamartQuery)
    result.calculateSummaryStatistics()

    api.trace("lib.Find.priceElasticity", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result;
}


/* ****************************************************************************************************************** */

def latestCustomerSalesPriceRecord(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "S_ZPAP"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute1" // Valid From (de: Gültig von) - descending
    def records = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (records[0]) {
        result = records[0]
    }

    api.trace("lib.Find.latestCustomerSalesPriceRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}


def customerSalesPrices(targetDate) {
    def result = []

    def filters = [
            Filter.equal("name", "S_ZPAP")
    ]

    if (targetDate instanceof Date) {
        targetDate = targetDate.format("yyyy-MM-dd")
    }

    if (targetDate != null) {
        filters.addAll([
                Filter.lessOrEqual("attribute1", targetDate),   // attribute16 = valid from
                Filter.greaterOrEqual("attribute2", targetDate) // attribute17 = valid to
        ])
    }


    def stream = api.stream("PX", "sku" /* sort-by*/, ["sku", "attribute4"], *filters)

    if (!stream) {
        return []
    }

    while (stream.hasNext()) {
        result.add(stream.next())
    }

    stream.close()

    api.trace("lib.Find.customerSalesPrices", "targetDate: ${targetDate}", "result size: ${result.size()}");

    return result
}

def latestCustomerSalesPrice2Record(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "CustomerSalesPrice2"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute2" // Valid From (de: Gültig von) - descending 
    def records = api.find("PX", 0, 1, orderBy, *filters)
    def result = [:]

    if (records[0]) {
        result = records[0]
    }

    api.trace("lib.Find.latestCustomerSalesPriceRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}

/* ****************************************************************************************************************** */

def latestClearingRebateRecord(String sku, Filter... additionalFilters) {
    def filters = [
            Filter.equal("name", "ClearingRebate"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute2" // Valid From (de: Gültig von) - descending 
    def clearingRebates = api.find("PX8", 0, 1, orderBy, *filters)
    def result = [:]

    if (clearingRebates[0]) {
        result = clearingRebates[0]
    }

    api.trace("lib.Find.latestClearingRebateRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}

def clearingRebates(targetDate) {
    def result = []

    def filters = [
            Filter.equal("name", "ClearingRebate")
    ]

    if (targetDate instanceof Date) {
        targetDate = targetDate.format("yyyy-MM-dd")
    }

    if (targetDate != null) {
        filters.addAll([
                Filter.lessOrEqual("attribute2", targetDate),   // attribute2 = valid from 
                Filter.greaterOrEqual("attribute3", targetDate) // attribute3 = valid to 
        ])
    }


    def stream = api.stream("PX8", "sku" /* sort-by*/, ["sku", "attribute1"], *filters)

    if (!stream) {
        return []
    }

    while (stream.hasNext()) {
        result.add(stream.next())
    }

    stream.close()

    api.trace("lib.Find.clearingRebates", "targetDate: ${targetDate}", "result size: ${result.size()}");

    return result
}

/* ****************************************************************************************************************** */

def futureBaseCostRecord(sku, targetDate) {
    if (targetDate instanceof Date) {
        targetDate = targetDate.format("yyyy-MM-dd")
    }

    def filters = [
            Filter.equal("name", "FutureBaseCost"),
            Filter.equal("sku", sku),
    ]

    def futureBaseCost = api.find("PX3", 0, 1, "sku", *filters)[0]

    if (futureBaseCost) {
        return [
                validFrom: targetDate,
                validTo  : "31-12-9999",
                baseCost : futureBaseCost.attribute1,
                currency : "EUR"
        ]
    }

}

/* ****************************************************************************************************************** */

def focusGroup(sku) {
    if (!api.global.focusGroups) {
        def focusGroups = [:]
        def stream = api.stream("PX", "sku", ["sku", "attribute2"], Filter.equal("name", "FocusGroup"))

        if (!stream) {
            return null
        }

        while (stream.hasNext()) {
            def record = stream.next()
            focusGroups[record.sku] = record.attribute2
        }

        stream.close()
        api.global.focusGroups = focusGroups
        api.trace("api.global.focusGroups", "", api.global.focusGroups)
    }

    return api.global.focusGroups[sku]
}

/* ****************************************************************************************************************** */

def occasionPrice(sku, targetDate) {
    if (targetDate == null) {
        targetDate = api.targetDate()
    }
    if (targetDate instanceof java.util.Date) {
        targetDate = targetDate.format("yyyy-MM-dd")
    }

    if (!api.global.occasionPrices) {
        def occasionPrices = [:]
        def stream = api.stream("PX50", "sku", Filter.equal("name", "S_ZPS"))

        if (!stream) {
            return null
        }

        while (stream.hasNext()) {
            def record = stream.next()
            def productPrices = occasionPrices[record.sku] ?: []
            productPrices.add([
                    sku       : record.sku,
                    validFrom : record.attribute1,
                    validTo   : record.attribute2,
                    price     : record.attribute3,
                    uom       : record.attribute8,
                    currency  : record.attribute7,
                    priceUnits: record.attribute9
            ])
            occasionPrices[record.sku] = productPrices
        }
        stream.close()
        api.global.occasionPrices = occasionPrices
        //api.trace("q",occasionPrices)
    }
    return api.global.occasionPrices[sku]?.find { op -> op.validFrom <= targetDate && op.validTo >= targetDate }
}

/* ****************************************************************************************************************** */

def promotionalSpecialPrice(sku, targetDate) {
    if (targetDate == null) {
        targetDate = api.targetDate()
    }
    if (targetDate instanceof java.util.Date) {
        targetDate = targetDate.format("yyyy-MM-dd")
    }

    if (!api.global.promotionalSpecialPrices) {
        def promotionalSpecialPrices = [:]
        def stream = api.stream("PX50", "sku", Filter.equal("name", "S_ZPC"))

        if (!stream) {
            return null
        }

        while (stream.hasNext()) {
            def record = stream.next()
            def productPrices = promotionalSpecialPrices[record.sku] ?: []
            productPrices.add([
                    sku       : record.sku,
                    validFrom : record.attribute1,
                    validTo   : record.attribute2,
                    price     : record.attribute3,
                    uom       : record.attribute13,
                    currency  : record.attribute12,
                    priceUnits: record.attribute14
            ])
            promotionalSpecialPrices[record.sku] = productPrices
        }

        stream.close()

        api.global.promotionalSpecialPrices = promotionalSpecialPrices
        // api.trace("q",promotionalSpecialPrices)
    }

    return api.global.promotionalSpecialPrices[sku]?.find { op -> op.validFrom <= targetDate && op.validTo >= targetDate }
}

/* ****************************************************************************************************************** */

def fixedPrice(sku, customerId, targetDate) {
    if (targetDate == null) {
        targetDate = api.targetDate()
    }
    if (targetDate instanceof java.util.Date) {
        targetDate = targetDate.format("yyyy-MM-dd")
    }

    if (!api.global.fixedPrices) {
        def fixedPrices = [:]
        def stream = api.stream("PX50", "sku", Filter.equal("name", "S_ZPF"))

        if (!stream) {
            return null
        }

        while (stream.hasNext()) {
            def record = stream.next()
            def productPrices = fixedPrices[record.sku] ?: []
            productPrices.add([
                    sku       : record.sku,
                    customerId: record.attribute4,
                    validFrom : record.attribute1,
                    validTo   : record.attribute2,
                    price     : record.attribute3,
                    uom       : record.attribute10,
                    currency  : record.attribute9,
                    priceUnits: record.attribute11
            ])
            fixedPrices[record.sku] = productPrices
        }

        stream.close()

        api.global.fixedPrices = fixedPrices
        //  api.trace("q",fixedPrices)
    }

    return api.global.fixedPrices[sku]?.find { op ->
        op.customerId == customerId &&
                op.validFrom <= targetDate &&
                op.validTo >= targetDate
    }
}

def loadCustomerPotentialGroupConfig() {
    if (!api.global.customerPotentialGroupMap) {
        api.global.customerPotentialGroupMap = [:]
        def customerPotentialGroups = api.findLookupTableValues("CustomerPotentialGroup")
        customerPotentialGroups?.each {
            api.global.customerPotentialGroupMap[it.name] = [it.name, it.attribute1, it.attribute2, it.attribute3, it.attribute4, it.attribute5]
        }
    }
}
/**
 * This function takes the name of the potential group ad returns its all other details i.e. name and range of the revenues corresponding to it
 * The data is configured in PP table CustomerPotentialGroup
 *
 * @param inputPotentialGroup
 * @return null if we have passed invalid potential group otherwise configured group details
 */
def findCustomerPotentialGroup(inputPotentialGroup) {
    loadCustomerPotentialGroupConfig()
    return api.global.customerPotentialGroupMap[inputPotentialGroup]
}

/**
 * Based on the input Sales Office Code return the code and its name seperated with text VB.
 * @param inputSalesOffice
 */
def getSalesOfficeCode(inputSalesOffice) {
    if (!api.global.codeSalesOfficeMap) {
        api.global.codeSalesOfficeMap = [:]
        def salesOfficeDetails = api.findLookupTableValues("SalesOffices")
        salesOfficeDetails?.each {
            api.global.codeSalesOfficeMap[it.value] = it.name + " VB " + it.value
        }
        api.trace("SalesOffice make or break", "(" + api.global.codeSalesOfficeMap + ")")
    }

    return api.global.codeSalesOfficeMap[inputSalesOffice]
}

/**
 * Find the customer potential group based on the configured range of revenue
 * @param revenue
 * @return customer potential group based on revenue range
 */
def getMatchingCustomerPotentialGroup(revenue) {
    def outcome = "Unknown"
    loadCustomerPotentialGroupConfig()
    revenue = (revenue >= 0) ? revenue : 0
    api.global.customerPotentialGroupMap.each { key, value ->
        if (value && value.size() >= 4) {
            def valueToCompare = new BigDecimal(revenue).setScale(2, RoundingMode.HALF_EVEN)
            def rangeStart = valueToCompare.compareTo(new BigDecimal(value[2]).setScale(2, RoundingMode.HALF_EVEN))
            def rangeEnd = (value[3]) ? valueToCompare.compareTo(new BigDecimal(value[3]).setScale(2, RoundingMode.HALF_EVEN)) : value[3]
            lib.TraceUtility.developmentTrace("Values for ", ["revenue ": valueToCompare, "RangeStart": rangeStart, "rangeEnd": rangeEnd, "value[0]": value[0]])
            if (rangeStart >= 0 && (rangeEnd == null || rangeEnd <= 0)) {
                outcome = value[0]
            }
        }
    }
    return outcome
}

/**
 * It is assumed that the loading of api.global.customerPotentialGroupMap is already taken care of. This is done when method
 * findCustomerPotentialGroup
 * @param revenue
 * @param inputPotGroup
 * @return
 */
def getRevenueAfterRevertingPotentialGroupDiscount(revenue, inputPotGroup) {
    def customerPGConfig = findCustomerPotentialGroup(inputPotGroup)
    def newRev = revenue
    if (customerPGConfig) {
        newRev = newRev + newRev * customerPGConfig.attribute5
    }
    return newRev
}

/**
 * This function computes if there is mismatch of customer category. If so, then identifies if it qualifies for additional discount
 * or correction i.e. reduce discount for customers based on the business given.
 * @param curPotGrp
 * @param compPotGrp
 * @return
 */
def getCorrectionType(def curPotGrp, def compPotGrp) {
    if (!compPotGrp || !curPotGrp) {
        return "Unknown"
    }

    if (curPotGrp == compPotGrp) {
        return "Correct"
    }

    if (curPotGrp?.size() != 2 || compPotGrp?.size() != 2) {
        return "Unknown"
    }

    int curPG = Integer.parseInt(curPotGrp?.substring(1))
    int compPG = Integer.parseInt(compPotGrp?.substring(1))
    if (compPG > curPG) {
        return "Higher"
    }
    return "Wrong"
}