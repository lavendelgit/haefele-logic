import java.math.RoundingMode

def roundDown(BigDecimal price, int decimalPlaces) {
    if (price == null) {
        return null
    }

    return price.setScale(decimalPlaces, BigDecimal.ROUND_DOWN)
}

def roundUp(BigDecimal price, int decimalPlaces) {
    if (price == null) {
        return null
    }

    return price.setScale(decimalPlaces, BigDecimal.ROUND_UP)
}

def roundHalfUp(BigDecimal price, int decimalPlaces) {
    if (price == null) {
        return null
    }

    def factor = (int) Math.pow(10, Math.abs(decimalPlaces));

    return Math.round(price * factor) / factor
}

/**
 * Round money to the nearest half above according to given number of decimal places.
 * For example:
 * - roundUpToHalf(1.23, 1) -> 1.25
 * - roundUpToHalf(501, -1) -> 505
 * - roundUpToHalf(506, -1) -> 510
 *
 * Negative decimalPlaces means that the price will be rounded to n decimal
 */
def roundUpToHalf(BigDecimal price, int decimalPlaces) {
    if (price == null) {
        return null
    }

    def treshold = (Math.pow(10, -decimalPlaces) / 2).toBigDecimal()

    def roundedDown = roundDown(price, decimalPlaces)
    if (price - roundedDown == 0 || price - roundedDown == treshold) {
        return price;
    }
    if (price - roundedDown < treshold) {
        return roundedDown + treshold
    }
    return roundUp(price, decimalPlaces)
}

def roundMoney(BigDecimal price) {

    if (price > 10000.0) {
        return roundUp(price, -2)
    }
    if (price > 5000.0) {
        return roundUpToHalf(price, -2);
    }
    if (price > 1000.0) {
        return roundUp(price, -1)
    }
    if (price > 500.0) {
        return roundUpToHalf(price, -1)
    }
    if (price > 100.0) {
        return roundUp(price, 0)
    }
    if (price > 20.0) {
        return roundUpToHalf(price, 0)
    }
    if (price > 5.0) {
        return roundUp(price, 1)
    }

    return roundHalfUp(price, 2)
}

def roundTo5Cents(BigDecimal value) {
    BigDecimal increment = 0.05
    BigDecimal divided = value.divide(increment, 0, BigDecimal.ROUND_HALF_UP);
    BigDecimal result = divided.multiply(increment);
    return result;
}

def roundUpTo10Cents(BigDecimal value) {
    return value.setScale(1, BigDecimal.ROUND_UP)
}