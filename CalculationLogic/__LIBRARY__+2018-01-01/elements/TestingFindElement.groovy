String methodToTest = /*"earliestBaseCostRecord"*/
/*"latestBaseCostRecord"*/ /*"latestBaseCost"*/
        // "currentBaseCostRecord"
        //"latestSalesPriceRecord"
        //latestNetPriceRecord
        // "earliestNetPriceRecord"
        // "occasionPrice"
        // "promotionalSpecialPrice"
        // "fixedPrice"
        //"customerSalesPrices"
        //"latestCustomerSalesPriceRecord"
       // "latestSalesPrice"
// "latestSalesPriceAnyType"
//  "earliestSalesPriceAnyType"
"currentSalesPrice"
/*def lib = libs.__LIBRARY__
def find = lib.Find*/

switch (methodToTest) {
    case "earliestBaseCostRecord":
        return Find.earliestBaseCostRecord("000.07.115")
        break
    case "latestBaseCostRecord":
        return Find.latestBaseCostRecord("000.07.115")
        break
    case "latestBaseCost":
        return Find.latestBaseCost("000.07.115")
        break
    case "currentBaseCostRecord":
        return Find.currentBaseCostRecord("917.99.506")
        break
    case "latestSalesPriceRecord":
        return Find.latestSalesPriceRecord("000.07.115")
        break
    case "latestNetPriceRecord":
        return Find.latestNetPriceRecord("556.62.603",
                                         Filter.lessOrEqual("attribute13", "2020-03-01"), Filter.greaterOrEqual("attribute14", "2020-03-01"))
        break
    case "earliestNetPriceRecord":
        return Find.earliestNetPriceRecord("000.07.115")
        break
    case "occasionPrice":
        return Find.occasionPrice("000.07.115", "2020-03-01")
        break
    case "promotionalSpecialPrice":
        return Find.promotionalSpecialPrice(" 002.87.042", "2020-03-01")
        break
    case "fixedPrice":
        return Find.fixedPrice("003.50.010", "0002783001", "2020-03-01")
        break
    case "customerSalesPrices":
        return Find.customerSalesPrices("2020-03-01")
        break
    case "latestCustomerSalesPriceRecord":
        return Find.latestCustomerSalesPriceRecord("556.62.603")
        break
    case "latestSalesPrice":
        return Find.latestSalesPrice("003.44.010")
        break
    case "latestSalesPriceAnyType":
        return Find.latestSalesPriceAnyType("000.33.431")
        break
    case "earliestSalesPriceAnyType":
        return Find.earliestSalesPriceAnyType("000.33.431")
        break
    case "currentSalesPrice":
        return Find.currentSalesPrice("000.51.710")
        break
    default:
        throw new RuntimeException('Invalid type specified ' + methodToTest)
}