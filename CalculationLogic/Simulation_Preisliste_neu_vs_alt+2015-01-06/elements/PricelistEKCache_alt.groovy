def pricelistName = api.getElement("Pricelist_alt")
def sku = api.getElement("Sku10")
def key = "${pricelistName}_${sku}"

api.trace("key", null, key)
api.trace("api.global.plEKCache", null, api.global.plEKCache)

def cache = api.global.plEKCache
def result = cache.get(key)

if (result == null) {
  result = api.pricelist(pricelistName, "Ek (aus PM, sonst BoM)", sku)
  if (result == null) {
    result = -1;
  }
  cache.put(key, result)
}

api.trace("api.global.plEKCache", null, api.global.plEKCache)
if (result == -1) return null
result