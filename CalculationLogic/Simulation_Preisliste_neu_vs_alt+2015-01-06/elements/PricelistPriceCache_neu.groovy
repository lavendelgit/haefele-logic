def pricelistName = api.getElement("Pricelist_neu")
def sku = api.getElement("Sku10")
def key = "${pricelistName}_${sku}"

api.trace("key", null, key)
api.trace("api.global.plPriceCache", null, api.global.plPriceCache)

def cache = api.global.plPriceCache
def result = cache.get(key)

if (result == null) {
  result = api.pricelist(pricelistName, "resultPrice", sku)
  if (result == null) {
    result = -1;
  }
  cache.put(key, result)
}

api.trace("api.global.plPriceCache", null, api.global.plPriceCache)
if (result == -1) return null
result