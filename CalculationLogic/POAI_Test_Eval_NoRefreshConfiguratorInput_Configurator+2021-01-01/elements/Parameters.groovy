def configurator = api.createConfiguratorEntry()

configurator.createParameter(
        api.inputBuilderFactory()
                .createRowLayout("Row")
                .addInput(
                        api.inputBuilderFactory()
                                .createStringUserEntry("StringEntry")
                                .setTextMask("Enter some string :)")
                                .setLabel("A string entry without refresh")
                                .setNoRefresh(true)
                                .buildContextParameter()
                )
                .addInput(
                        api.inputBuilderFactory()
                                .createOptionEntry("OptionEntry")
                                .setPlaceholderText("Select an option :)")
                                .setLabel("A required option entry without refresh")
                                .setOptions(["blue_pill", "red_pill"])
                                .setLabels([blue_pill: "Blue pill", red_pill: "Red Pill"])
                                .setRequired(true)
                                .setNoRefresh(true)
                                .buildContextParameter()
                )
                .buildContextParameter()
)

configurator.createParameter(
        api.inputBuilderFactory()
                .createCollapseLayout("Collapse")
                .addInput(
                        api.inputBuilderFactory()
                                .createOptionEntry("AnotherOptionEntry")
                                .setPlaceholderText("Select an option :)")
                                .setLabel("An option without refresh in a collapse")
                                .setOptions(["blue_pill", "red_pill"])
                                .setLabels([blue_pill: "Blue pill", red_pill: "Red Pill"])
                                .setNoRefresh(true)
                                .buildContextParameter()
                )
                .buildContextParameter()
)
return configurator
