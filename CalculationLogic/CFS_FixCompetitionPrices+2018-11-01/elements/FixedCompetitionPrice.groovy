def competitionPrice = api.getElement("CompetitionPrice") as String

return competitionPrice
        ?.replaceAll(",", "")
        ?.replaceAll("€", "")
        ?.toBigDecimal()
