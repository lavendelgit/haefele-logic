Map currentItem = api.currentItem()
String customerIdHEINR = out.HEINR ? currentItem.get(out.HEINR) : null
String customerIdKUNNR = out.KUNNR ? currentItem.get(out.KUNNR) : null
String customerIdKUNWE = out.KUNWE ? currentItem.get(out.KUNWE) : null
Object customerIdToLookUp = customerIdHEINR ?: (customerIdKUNNR ?: customerIdKUNWE)
if (customerIdToLookUp) {
    return customerIdToLookUp.padLeft(10, "0")
}
return "0000000000"