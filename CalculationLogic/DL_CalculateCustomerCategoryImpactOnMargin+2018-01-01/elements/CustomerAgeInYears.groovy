def currentDate = api.targetDate()
int months = lib.GeneralUtility.getMonthsBetweenDates(currentDate, out.CustomerCreationDate)
return lib.MathUtility.divide (months, 12)