/**
 * This element has all functions doing the computation of output fields.
 */

/**
 * This function computes if there is mismatch of customer category. If so, then identifies if it qualifies for additional discount
 * or correction i.e. reduce discount for customers based on the business given.
 * @param curPotGrp
 * @param compPotGrp
 * @return
 */
def getCorrectionType(def curPotGrp, def compPotGrp) {
    lib.Find.getCorrectionType(curPotGrp, compPotGrp)
}

def getComputedPotentialGroup(Double ageOfCustomer, String oldCPG, def totalRevenue) {
    boolean ignoreCustomerRecord = getIfCustomerRecordToBeIgnored(ageOfCustomer)

    return (ignoreCustomerRecord) ? oldCPG : lib.Find.getMatchingCustomerPotentialGroup(totalRevenue)
}

boolean getIfCustomerRecordToBeIgnored(Double ageOfCustomer) {
    double minimumAgeRequiredInMonths = lib.GeneralSettings.decimalValue(lib.HaefeleConstants.MIN_CUSTOMER_AGE_IN_MONTHS)
    double minimumAgeRequiredInYears = lib.MathUtility.divide (minimumAgeRequiredInMonths, 12)

    return (ageOfCustomer <= minimumAgeRequiredInYears)
}

def isCategorizationMatchingRevenue(def perceivedPotGrp, def curPotGrp) {
    return (perceivedPotGrp == curPotGrp)
}

/**
 * Computes the revenue for transaction before they are qualified for discount due to customer categorization.
 * @param qualifiedRevenue
 * @param custPotentialGroup
 * @return
 */
def getPreCCDUmsatz(qualifiedRevenue, custPotentialGroup) {
    if (qualifiedRevenue == 0.0)
        return qualifiedRevenue

    def newRev = qualifiedRevenue
    def potGrpDetails = lib.Find.findCustomerPotentialGroup(custPotentialGroup)
    if (potGrpDetails)
        newRev = newRev + newRev * potGrpDetails[5]
    return newRev
}

/**
 * This method computes the margin given the old revenue and new revenue and old margin. It simply finds the change
 * in new revenue and adds that to the margin. If the new revenue is reduced the margin will reduce and if it is increased
 * it will increase.
 * @param oldRevenue
 * @param potentialRevenue
 * @param oldMargin
 * @return
 */
def getPotentialMargin(oldRevenue, potentialRevenue, oldMargin) {
    if (oldRevenue == potentialRevenue)
        return oldMargin

    return oldMargin + (potentialRevenue - oldRevenue)
}
/**
 *
 * @param currentTotalRevenue
 * @param curCustPotentialGroup
 * @param newPotentialGroup
 * @return
 */
def getPotentialRevenue(currentTotalRevenue, curCustPotentialGroup, newPotentialGroup) {
    def baseRevenue = getPreCCDUmsatz(currentTotalRevenue, curCustPotentialGroup)
    def potentialRevenue = getPotentialRevenueFromBaseRevenue(baseRevenue, newPotentialGroup)
    return potentialRevenue
}

def getPotentialRevenueFromBaseRevenue(baseRevenue, String newPotentialGroup) {
    if (baseRevenue == 0.0)
        return baseRevenue

    def newRev = baseRevenue
    def potGrpDetails = lib.Find.findCustomerPotentialGroup(newPotentialGroup)
    api.trace("Printing 3", " Potential Group details " + potGrpDetails)
    if (potGrpDetails)
        newRev = newRev - newRev * potGrpDetails[5]
    return newRev
}

/**
 *
 * @param currentPotGrp
 * @param qualifiedPotGrp
 * @return
 */
def getDiscountPerGain(currentPotGrp, qualifiedPotGrp) {
    if (!qualifiedPotGrp)
        return 0
    def curPG = lib.Find.findCustomerPotentialGroup(currentPotGrp)
    def qualPG = lib.Find.findCustomerPotentialGroup(qualifiedPotGrp)
    return (curPG[5] - qualPG[5])
}

/**
 * This returns the percentage value of the revenue that is attributed to CCD. This assumes revenue is always zero or a positive number.
 * @param discountPerGain
 * @param revenue
 * @return
 */
def getPotentialRevGain(discountPerGain, revenue) {
    return revenue * discountPerGain
}

/**
 * The margin changes due to discount percentage gain. If the margins are negative then it returns the updated revenue gain ignoring the
 * value of margin shared with.
 * @param discountPerGain
 * @param margin
 * @param revenue
 * @return
 */
def getPotentialMarginGain(potentialRevGain) {
    return potentialRevGain
}
