/**
 * Since the data for the same customer is distributed across multiple records, common calculations are done only once.
 * The common calculation definition are those calculations which does not use the distribution specific any attribute.
 */

def computeCommonCalculations() {
    if (api.global.currentCustomerId == out.CustomerId) {
        return
    }
    def crPotGrp = out.CCKundePotentialGruppe
    def computedPotGrp = out.CCComputedPotentialGroup
    def totalRevenue = out.CCTotalRevenue
    lib.TraceUtility.developmentTrace("Customer Potential Group", crPotGrp)
    lib.TraceUtility.developmentTrace("Computed Potential Group", computedPotGrp)
    api.global.isCategorizationMatchingReveue = ComputationUtil.isCategorizationMatchingRevenue(computedPotGrp, crPotGrp)

    def subtotalUmsatz = out.SubtotalUmsatz
    def subtotalMargin = out.SubtotalMargin
    api.global.currentCustomerId = out.CustomerId
    boolean shouldComputeDetails = ((crPotGrp && !(crPotGrp.equals("Unknown"))) && (out.CorrectionType.equals("Higher") || out.CorrectionType.equals("Wrong")))
    api.global.discountGainPer = (shouldComputeDetails) ? ComputationUtil.getDiscountPerGain(crPotGrp, computedPotGrp) : 0
    api.global.potentialRevenue = (shouldComputeDetails) ? ComputationUtil.getPotentialRevenue(subtotalUmsatz, crPotGrp, computedPotGrp) : subtotalUmsatz
    api.global.potentialMargin = (shouldComputeDetails) ? ComputationUtil.getPotentialMargin(subtotalUmsatz, api.global.potentialRevenue, subtotalMargin) : subtotalMargin
    api.global.potentialRevenuePer = (shouldComputeDetails) ? lib.MathUtility.percentage(subtotalUmsatz, api.global.potentialRevenue - subtotalUmsatz) : 0
    api.global.potentialMarginPer = (shouldComputeDetails) ? lib.MathUtility.percentage(subtotalMargin, api.global.potentialMargin - subtotalMargin) : 0
    api.global.potentialRevenueGain = (shouldComputeDetails) ? (api.global.potentialRevenue - subtotalUmsatz) : 0
    api.global.potentialMarginGain = (shouldComputeDetails) ? (api.global.potentialMargin - subtotalMargin) : 0
    api.global.subtotalUmsatzBeforeDiscount = (shouldComputeDetails) ? ComputationUtil.getPreCCDUmsatz(subtotalUmsatz, crPotGrp) : subtotalUmsatz
    api.global.subtotalCOGS = api.global.subtotalUmsatzBeforeDiscount - subtotalMargin
    api.global.computedGrossProfit = subtotalUmsatz - api.global.subtotalCOGS

}

def isCategorizationMatchingRevenue() {
    computeCommonCalculations()
    return api.global.isCategorizationMatchingReveue
}

def getDiscountPerGain() {
    computeCommonCalculations()
    return api.global.discountGainPer
}

def getPotentialRevenueGain() {
    computeCommonCalculations()
    return api.global.potentialRevenueGain
}

def getPotentialMarginGain() {
    computeCommonCalculations()
    return api.global.potentialMarginGain
}

def getPotentialRevenue() {
    computeCommonCalculations()
    return api.global.potentialRevenue
}

def getPotentialMargin() {
    computeCommonCalculations()
    return api.global.potentialMargin
}

def getPotentialRevenueGainPer() {
    computeCommonCalculations()
    return api.global.potentialRevenuePer
}

def getPotentialMarginGainPer() {
    computeCommonCalculations()
    return api.global.potentialMarginPer
}

def getSubtotalUmsatzBeforeDiscount() {
    computeCommonCalculations()
    return api.global.subtotalUmsatzBeforeDiscount
}

def getSubtotalCOGS() {
    computeCommonCalculations()
    return api.global.subtotalCOGS
}

def getComputedGrossProfit() {
    computeCommonCalculations()
    return api.global.computedGrossProfit
}
