Date creationDate = lib.GeneralUtility.getDefaultDate(api.dateUserEntry("CustomerCreationDate"))
creationDate = creationDate ?: api.calendar().getTime()

return creationDate