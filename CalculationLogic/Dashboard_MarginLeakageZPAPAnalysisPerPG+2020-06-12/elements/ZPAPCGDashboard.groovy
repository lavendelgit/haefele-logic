def effectiveYear = out.EffectiveYear

def effectiveYearParam = "Effective Year"
def prodhII = "ProdhII"
def prodhIII = "ProdhIII"
def prodhIV = "ProdhIV"

return api.dashboard("ZPAPAnalysisPerCGEmbeddedDash")
        .setParam(effectiveYearParam, effectiveYear)
        .setParam(prodhII, 'All')
        .setParam(prodhIII, 'All')
        .setParam(prodhIV, 'All')
        .showEmbedded()
        .andRecalculateOn(api.dashboardWideEvent("RefreshZPAPPGDetails"))
        .withEventDataAttr("ZA_ProdhII").asParam(prodhII)
        .withEventDataAttr("ZA_ProdhIII").asParam(prodhIII)
        .withEventDataAttr("ZA_ProdhIV").asParam(prodhIV)