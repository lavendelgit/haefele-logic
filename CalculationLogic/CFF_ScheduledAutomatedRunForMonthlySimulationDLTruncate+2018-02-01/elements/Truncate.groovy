import net.pricefx.common.dto.ChainedJobInfo

api.global.printTrace=true

def SIMULATION_LIB_REF = libs.HaefeleCommon.Simulation
def SIMUlATION_MAPPING = [(SIMULATION_LIB_REF.SIMULATION_1): ['Price Change Simulation Result (v1)',
                                                              'Truncate',
                                                              'DM.PriceChangeSimulation_Result_V1'],
                          (SIMULATION_LIB_REF.SIMULATION_2): ['Price Change Simulation Result (v2)',
                                                              'Truncate',
                                                              'DM.PriceChangeSimulation_Result_V2'],
                          (SIMULATION_LIB_REF.SIMULATION_3): ['Price Change Simulation Result (v3)',
                                                              'Truncate',
                                                              'DM.PriceChangeSimulation_Result_V3']]

List filters = [Filter.equal("attributeExtension___status", 'Scheduled')]
List simulationInputOptions = api.findLookupTableValues("SimulationInputs", *filters)?.collect { it -> it.name }

libs.__LIBRARY__.TraceUtility.developmentTrace('11111111111111111111111 simulationInputOptions', api.jsonEncode(simulationInputOptions))
List currentSimulationDetails

simulationInputOptions.each { String name ->
    currentSimulationDetails = SIMUlATION_MAPPING[name]
    libs.__LIBRARY__.TraceUtility.developmentTrace('222222222222222222222222 currentSimulationDetails', api.jsonEncode(currentSimulationDetails))
    actionBuilder
            .addDataLoadAction(currentSimulationDetails[0], currentSimulationDetails[1], currentSimulationDetails[2])
            .setCalculate(true)
}