def recommendedPriceTables = [
        "RecommendedPrice": "Recommended Price",
        "RecommendedPrice2": "Recommended Price 2"
]

api.option("Recommended Price Table", recommendedPriceTables.keySet().toList(), recommendedPriceTables) ?: "RecommendedPrice"
