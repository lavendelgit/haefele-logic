def sku = api.getElement("SKU")

if (!api.global.compoundProducts) {
    api.global.compoundProducts = [:]

    def stream = api.stream("PBOME", "sku", ["sku"]);
    if (!stream) {
        return []
    }

    while (stream.hasNext()) {
        api.global.compoundProducts[stream.next().sku] = true
    }

    stream.close();
}

return api.global.compoundProducts[sku]