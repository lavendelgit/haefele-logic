def recommendedPrice = api.getElement("RecommendedPrice") // per 100 units
def currentSalesPrice = api.getElement("CurrentSalesPrice")

if (recommendedPrice == null || currentSalesPrice == null || currentSalesPrice == 0) {
    return null
}

return (recommendedPrice - currentSalesPrice) / currentSalesPrice