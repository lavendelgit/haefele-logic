def newMarginAbs = api.getElement("NewMarginAbs")
def marginAbsLastYear = api.getElement("MarginAbsLastYear")

if (newMarginAbs == null || marginAbsLastYear == null) {
    return null
}

return newMarginAbs - marginAbsLastYear