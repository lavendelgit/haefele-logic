def newRevenue = api.getElement("NewRevenue")
def revenueLastYear = api.getElement("RevenueLastYear")

if (newRevenue == null || revenueLastYear == null || revenueLastYear == 0)  {
    return null
}

return (newRevenue - revenueLastYear) / revenueLastYear