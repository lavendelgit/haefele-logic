def sku = api.getElement("SKU")
def recommendedPrice = api.getElement("RecommendedPrice")
def isGoodRegression = api.getElement("IsGoodRegression")
def isCompound = api.getElement("IsCompound")

def currentYear = api.calendar().get(Calendar.YEAR);
def previousYear = currentYear - 1

def filters = [
        Filter.equal("Material", sku),
        Filter.equal("InvoiceDateYear", previousYear),
	    Filter.notEqual("InvoicePos", "999999")
]

def dmCtx = api.getDatamartContext()
def table = dmCtx.getTable("TransactionsDM")
def queryResult = null

if (recommendedPrice && isGoodRegression && !isCompound) {
    def query = dmCtx.newQuery(table, false)
    //query.select("SalesPricePer100Units", "salesPrice")
    query.select("Revenue", "revenue")
    query.select("Units", "quantity")
    query.select("MarginAbs", "marginAbs")
    query.select("Cost", "cost")
    query.select("SalesType", "salesType")
    query.select("(RegularSalesPricePer100Units - SalesPricePer100Units) / RegularSalesPricePer100Units", "totalDiscount")
    query.where(*filters)
    queryResult = dmCtx.executeQuery(query)
} else {
    // fetch aggregated values because particular transaction lines won't be needed
    def query = dmCtx.newQuery(table, true)
    query.select("SUM(Revenue)", "revenue")
    query.select("SUM(Units)", "quantity")
    query.select("SUM(MarginAbs)", "marginAbs")
    query.select("SUM(Cost)", "cost")
    query.where(*filters)
    queryResult = dmCtx.executeQuery(query)
}


api.trace(queryResult?.getData()?.collect())

return queryResult?.getData()?.collect()