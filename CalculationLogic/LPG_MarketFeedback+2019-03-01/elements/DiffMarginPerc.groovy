def newMarginAbs = api.getElement("NewMarginAbs")
def marginAbsLastYear = api.getElement("MarginAbsLastYear")

if (newMarginAbs == null || marginAbsLastYear == null || marginAbsLastYear == 0) {
    return null
}

return (newMarginAbs - marginAbsLastYear) / marginAbsLastYear
