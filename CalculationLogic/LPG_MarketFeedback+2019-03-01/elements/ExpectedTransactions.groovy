def salesTypes = api.getElement("SalesTypes")
def recommendedPrice = api.getElement("RecommendedPrice") // per 100 units
def currentSalesPrice = api.getElement("CurrentSalesPrice")
def transactions = api.getElement("Transactions")
def linReg = api.getElement("LinearRegression")
def isGoodRegression = api.getElement("IsGoodRegression")
def isCompound = api.getElement("IsCompound")
def recommendedPriceHigherThanCurrentSalesPrice = (recommendedPrice != null && currentSalesPrice != null && recommendedPrice > currentSalesPrice)

if (!linReg || !recommendedPrice || !isGoodRegression || isCompound || recommendedPriceHigherThanCurrentSalesPrice) {
    return [
        units: api.getElement("UnitsLastYear"),
        revenue: api.getElement("RevenueLastYear"),
        marginAbs: api.getElement("MarginAbsLastYear")
    ]
}

def result = [
    units: 0,
    revenue: 0.0,
    marginAbs: 0.0
]

for (t in transactions) {
    def isModifiablePrice = (salesTypes[t.salesType]?.priceIncreaseAffected == "Yes")
    if (!isModifiablePrice || !(t.revenue > 0) || !(t.quantity > 0) || t.totalDiscount == null || t.cost == null) {
        result.units += (t.quantity ?: 0)
        result.revenue += (t.revenue ?: 0)
        result.marginAbs += (t.marginAbs ?: 0)
        continue
    }
    def newPrice = recommendedPrice * (1 - t.totalDiscount) // per 100 units
    def newUnits = linReg.lra + newPrice * linReg.lrb
    def newRevenue = newUnits * (newPrice / 100)
    def unitCost = t.cost / t.quantity
    def newCost = unitCost * newUnits
    def newMarginAbs = newRevenue - newCost
    /*
    if (newRevenue < t.revenue) {
        api.trace("oldUnits: ${t.quantity}, newUnits: ${newUnits}", "oldRevenue: ${t.revenue}, newRevenue: ${newRevenue}", "salesType: ${t.salesType}, oldSalesPrice: ${t.revenue / t.quantity * 100}, newSalesPrice: ${newPrice}, totalDiscount: ${t.totalDiscount}")
    }
    */
    result.units += newUnits
    result.revenue += newRevenue
    result.marginAbs += newMarginAbs
}

return result



