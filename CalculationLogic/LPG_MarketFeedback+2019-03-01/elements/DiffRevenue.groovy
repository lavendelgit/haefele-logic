def newRevenue = api.getElement("NewRevenue")
def revenueLastYear = api.getElement("RevenueLastYear")

if (newRevenue == null || revenueLastYear == null) {
    return null
}

return newRevenue - revenueLastYear