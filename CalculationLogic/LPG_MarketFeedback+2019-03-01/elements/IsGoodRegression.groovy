def linReg = api.getElement("LinearRegression")
def confidenceInterval = api.getElement("ConfidenceInterval")

if (!linReg?.ols) {
    return false
}

def goodPTreshold = 0.05

def slopeP = linReg.ols?.result?.entries?.get(0)?.P
def interceptP = linReg.ols?.result?.entries?.get(1)?.P

def confidenceIntervalCrossesZero =
        Math.signum(confidenceInterval.slope.lower95) != Math.signum(confidenceInterval.slope.upper95) ||
                Math.signum(confidenceInterval.intercept.lower95) != Math.signum(confidenceInterval.intercept.upper95)

return slopeP < goodPTreshold && interceptP < goodPTreshold && !confidenceIntervalCrossesZero