def recommendedPriceTable = api.getElement("RecommendedPriceTable")


api.find("PX", 0, 1, "attribute1",
        Filter.equal("name", recommendedPriceTable),
        Filter.equal("sku", api.getElement("SKU")))
    ?.find()
    ?.attribute1