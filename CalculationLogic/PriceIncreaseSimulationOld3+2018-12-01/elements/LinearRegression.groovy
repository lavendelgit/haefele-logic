def sku = api.product("sku")
def dmCtx = api.getDatamartContext()
def table = dmCtx.getTable("TransactionsDM")
def query = dmCtx.newQuery(table)

def filters = [
    Filter.equal("Material", sku),
    Filter.greaterThan("Units", 0)
]


query.select("AVG(SalesPricePer100Units)", "price")
query.select("SUM(Units)", "quantity")
query.select("SUM(Revenue)", "revenue")
query.select("InvoiceId", "invoiceId")
query.select("InvoicePos", "invoicePos")
query.where(*filters)
query.orderBy("price")
query.setOptions([
    regression: ["quantity", "price"] // y, x
])

def result = dmCtx.executeQuery(query)
result?.calculateSummaryStatistics()

def summary = result?.getSummary()

def lra = summary?.projections?.quantity?.LRa
def lrb = summary?.projections?.price?.LRb

api.trace("summary", "", summary)

api.trace("LRa", "", lra)
api.trace("LRb", "", lrb)

def ols = result?.getData()
    ?.linearRegression()
    ?.usingDependentVariable("quantity")
    ?.usingIndependentVariables(["price"])
    ?.run()

if (lra?.toString() == "Infinity" || lrb?.toString() == "Infinity") {
    lra = ols?.intercept;
    lrb = ols?.coefficients?.toList()?.first()
}

api.trace("ols", "", ols)
api.trace("R2", "", ols?.r2)
api.trace("P-value", "", ols?.pvalue)
api.trace("intercept", "", ols?.intercept)
api.trace("coefficients", "", ols?.coefficients?.toList())
api.trace("t-test", "", ols?.ttest?.toList())
api.trace("result", "", ols?.getResult())

return [
    lra: lra ?: 0.0,
    lrb: lrb ?: 0.0,
    ols: ols
]
