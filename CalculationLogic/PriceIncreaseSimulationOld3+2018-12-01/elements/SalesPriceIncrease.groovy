if (!api.global.salesPriceIncrease) {
    api.global.salesPriceIncrease = api.findLookupTableValues("SalesPriceIncrease2")
                                       .collectEntries { pp -> [ (pp.name) : [
                                          productHierarchy2: pp.name,
                                          priceIncrease: pp.attribute1,
                                       ]]}
}

return api.global.salesPriceIncrease