def revenue = api.getElement("Revenue")
def newRevenue = api.getElement("NewRevenue")

if (newRevenue == null) {
    return
}

return newRevenue - revenue