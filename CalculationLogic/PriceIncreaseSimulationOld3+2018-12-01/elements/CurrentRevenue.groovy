def invoicesCount = api.getElement("TransactionsCount")
def currentSalesPrice = api.getElement("CurrentSalesPrice")
def currentUnits = api.getElement("CurrentUnits")

if (invoicesCount == null || currentUnits == null || currentSalesPrice == null) {
    return
}

return invoicesCount * currentUnits * currentSalesPrice / 100