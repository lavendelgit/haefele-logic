def newSalesPrice = api.getElement("NewSalesPrice")
def linReg = api.getElement("LinearRegression")

if (newSalesPrice == null) {
    return
}

return linReg.lra + newSalesPrice * linReg.lrb