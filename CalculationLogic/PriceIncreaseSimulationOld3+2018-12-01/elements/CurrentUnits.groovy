def currentSalesPrice = api.getElement("CurrentSalesPrice")
def linReg = api.getElement("LinearRegression")

if (currentSalesPrice == null) {
    return
}

return linReg.lra + currentSalesPrice * linReg.lrb