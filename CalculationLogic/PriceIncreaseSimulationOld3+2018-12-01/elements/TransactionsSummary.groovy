def sku = api.product("sku")
def dmCtx = api.getDatamartContext()
def table = dmCtx.getTable("TransactionsDM")
def query = dmCtx.newQuery(table)

def currentYear = api.calendar().get(Calendar.YEAR);
def previousYear = currentYear - 1

def filters = [
    Filter.equal("Material", sku),
    Filter.greaterThan("Units", 0),
    Filter.equal("InvoiceDateYear", previousYear)
]

query.select("COUNT(1)", "count")
query.select("AVG(SalesPricePer100Units)", "price")
query.select("SUM(Revenue)", "revenue")
query.select("SUM(Units)", "quantity")
query.select("SUM(MarginAbs)", "marginAbs")
query.where(*filters)

dmCtx.executeQuery(query)?.getData()?.collect()?.find()