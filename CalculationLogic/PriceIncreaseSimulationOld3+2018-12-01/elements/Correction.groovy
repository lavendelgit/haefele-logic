def revenue = api.getElement("Revenue") // the real one
def currentRevenue = api.getElement("CurrentRevenue") // based on linear regression

if (!revenue || !currentRevenue) {
    return 0.0
}

(currentRevenue - revenue) / currentRevenue