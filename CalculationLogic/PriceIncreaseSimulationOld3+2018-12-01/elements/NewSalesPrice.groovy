def currentSalesPrice = api.getElement("CurrentSalesPrice")
def salesPriceIncreasePerc = api.getElement("SalesPriceIncreasePerc");

if (currentSalesPrice == null) {
    return
}

return currentSalesPrice * (1 + salesPriceIncreasePerc)