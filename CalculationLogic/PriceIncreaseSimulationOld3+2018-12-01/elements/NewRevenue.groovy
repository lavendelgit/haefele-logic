def invoicesCount = api.getElement("TransactionsCount")
def newSalesPrice = api.getElement("NewSalesPrice")
def newUnits = api.getElement("NewUnits")
def correction = api.getElement("Correction")

if (newSalesPrice == null) {
    return
}

return (invoicesCount * newUnits * newSalesPrice / 100) * (1 + correction)