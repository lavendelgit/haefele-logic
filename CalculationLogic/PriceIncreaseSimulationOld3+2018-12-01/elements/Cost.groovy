def revenue = api.getElement("Revenue") as BigDecimal
def margin = api.getElement("Margin") as BigDecimal

if (revenue == null) {
    api.redAlert("Revenue")
    return
}

if (margin == null) {
    api.redAlert("Margin")
    return
}


revenue - margin