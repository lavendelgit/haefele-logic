/**
 * This is helper class which has functions required for computation of data for data source. This contains logic of how
 * records are prepared for saving into DB.
 */

import net.pricefx.formulaengine.DatamartRowSet

def getRecordToInsertOrUpdate(customerRec) {
    return [
            'CCYear'                 : customerRec['t3idy'],
            'CustomerId'             : customerRec['t3ci'],
            'CCCustomerDetails'      : customerRec['t1nm'],
            'CorrectionType'         : 'Not Evaluated',
            'CCVerkaufsbereich3'     : customerRec['t1so'],
            'CBranchCategory'        : customerRec['t1brc'],
            'CCTotalRevenue'         : customerRec['t3tr'],
            'CCTotalTransactionCount': customerRec['t3ttc'],
            'CCArticleCount'         : customerRec['t3aac'],
            'CCTotalMargin'          : customerRec['t3tmar'],
            'CCUnitsPurchased'       : customerRec['t3tu'],
            'CCKundePotentialGruppe' : customerRec['t1cpg'],
            'IsProcessed'            : false,
            'CCDUmsatz'              : customerRec['t1r'],
            'NonCCDUmsatz'           : customerRec['t2r'],
            'CCDMargin'              : customerRec['t1mar'],
            'OtherMargin'            : customerRec['t2mar'],
            'CCDMenge'               : customerRec['t1u'],
            'NonCCDMenge'            : customerRec['t2u'],
            'CCDQualifiedArticles'   : customerRec['t1ac'],
            'NonCCDArticles'         : customerRec['t2ac'],
            'CCDTransactionCount'    : customerRec['t1tc'],
            'NonCCDTransactioncount' : customerRec['t2tc'],
            'CustomerCreationDate'   : customerRec['t3ccd']
    ]
}

/**
 * This method populates the output records computed into the target data source.
 * @param curCusRecList
 */
def populateCustomerCategoryRecords(def curCusRecList) {
    // Save the final aggregated records into DB
    def recordToInsert
    curCusRecList.each { custRecord ->
        recordToInsert = getRecordToInsertOrUpdate(custRecord)
        lib.TraceUtility.developmentTraceRow("recordToInsert", recordToInsert)
        lib.DBQueries.insertRecordIntoTargetRowSet(recordToInsert)
    }
}