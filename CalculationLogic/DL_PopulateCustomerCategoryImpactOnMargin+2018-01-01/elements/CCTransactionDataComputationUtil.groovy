def getFieldsForGettingCCDTransRec() {
    return [
            'CustomerId'       : 'CustomerId',
            'cust_creatdat'    : 'CustomerCreationDate',
            'Name'             : 'Name',
            'InvoiceDateYear'  : 'InvoiceDateYear',
            'KundeKlasse'      : 'CustomerPotentialGroup',
            'BrancheCategory'  : 'BrancheCategory',
            'Verkaufsbereich3' : 'Verkaufsbereich3',
            'SUM(p1_Revenue)'  : 'Revenue',
            'SUM(p2_Units)'    : 'Units',
            'SUM(p3_Material)' : 'ArticleCount',
            'SUM(p4_MarginAbs)': 'Margin',
            'SUM(p5_Material)' : 'TransactionCount'
    ]
}

def getFieldsForGettingNonCCDTransRec() {
    return [
            'CustomerId'       : 'CustomerId',
            'cust_creatdat'    : 'CustomerCreationDate1',
            'KundeKlasse'      : 'CustomerPotentialGroup1',
            'Name'             : 'Name1',
            'InvoiceDateYear'  : 'InvoiceDateYear1',
            'BrancheCategory'  : 'BrancheCategory1',
            'Verkaufsbereich3' : 'Verkaufsbereich31',
            'SUM(p1_Revenue)'  : 'Revenue1',
            'SUM(p2_Units)'    : 'Units1',
            'SUM(p3_Material)' : 'ArticleCount1',
            'SUM(p4_MarginAbs)': 'Margin1',
            'SUM(p5_Material)' : 'TransactionCount1'
    ]
}

def getFieldsForGettingTransRecSummary() {
    return [
            'CustomerId'       : 'CustomerId',
            'cust_creatdat'    : 'CustomerCreationDate',
            'InvoiceDateYear'  : 'InvoiceDateYear',
            'SUM(p1_Revenue)'  : 'TotalRevenue',
            'SUM(p2_Units)'    : 'TotalUnits',
            'SUM(p3_Material)' : 'AllArticleCount',
            'SUM(p4_MarginAbs)': 'TotalMargin',
            'SUM(p5_Material)' : 'TotalTransactionCount'
    ]
}

def getInnerJoinForSummaryNDetailTransRec() {
    String sqlStatement = 'SELECT ' +
            'T1.customerId as t1cid, T1.Name as t1nm, T1.invoiceDateYear as t1idy, T1.BrancheCategory as t1brc, ' +
            'T1.Verkaufsbereich3 as t1so, T1.CustomerPotentialGroup as t1cpg, ' +
            'IsNull(T1.Revenue, 0.0) as t1r, IsNull(T1.Units,0) as t1u, T1.ArticleCount as t1ac, IsNull(T1.Margin,0.0) as t1mar, T1.TransactionCount as t1tc, ' +
            'IsNull(T2.customerId, T1.customerId) as t2cid, IsNull(T2.Name1, T1.Name) as t2nm1, IsNull(T2.invoiceDateYear1, T1.invoiceDateYear) as t2idy, ' +
            'IsNull(T2.BrancheCategory1, T1.BrancheCategory) as t2brc, IsNull(T2.Verkaufsbereich31, T1.Verkaufsbereich3) as t2so, ' +
            'IsNull(T2.CustomerPotentialGroup1, T1.CustomerPotentialGroup) as t2cpg, ' +
            'IsNull(T2.Revenue1, 0.0) as t2r, IsNull(T2.Units1,0) as t2u, IsNull(T2.ArticleCount1,0) as t2ac, IsNull(T2.Margin1,0.0) as t2mar, IsNull(T2.TransactionCount1,0) as t2tc, ' +
            'T3.customerId as t3ci, T3.CustomerCreationDate t3ccd, T3.InvoiceDateYear as t3idy, T3.TotalRevenue as t3tr, T3.TotalUnits as t3tu, T3.AllArticleCount as t3aac, ' +
            'T3.TotalMargin as t3tmar, T3.TotalTransactionCount as t3ttc ' +
            'FROM T1 ' +
            'LEFT OUTER JOIN T2 ON T1.customerId = T2.customerId AND T1.InvoiceDateYear = T2.InvoiceDateYear1 AND ' +
            'T1.BrancheCategory = T2.BrancheCategory1 AND T1.Verkaufsbereich3 = T2.Verkaufsbereich31 AND T1.CustomerPotentialGroup = T2.CustomerPotentialGroup1 ' +
            'INNER JOIN T3 ON T3.customerId = T1.customerId And T3.InvoiceDateYear = T1.InvoiceDateYear  ' +
            'UNION ' +
            'SELECT ' +
            'IsNull(T1.customerId, T2.customerId) as t1cid, IsNull(T1.Name,T2.Name1)  as t1nm, IsNull(T1.invoiceDateYear, T2.invoiceDateYear1) as t1idy, IsNull(T1.BrancheCategory, T2.BrancheCategory1) as t1brc, ' +
            'IsNull(T1.Verkaufsbereich3, T2.Verkaufsbereich31) as t1so, IsNull(T1.CustomerPotentialGroup, T2.CustomerPotentialGroup1) as t1cpg, ' +
            'IsNull(T1.Revenue,0.0) as t1r, IsNull(T1.Units,0) as t1u, IsNull(T1.ArticleCount,0) as t1ac, IsNull(T1.Margin, 0.0) as t1mar, IsNull(T1.TransactionCount,0) as t1tc, ' +
            'T2.customerId as t2cid, T2.Name1 as t2nm1, T2.invoiceDateYear1 as t2idy, T2.BrancheCategory1 as t2brc, T2.Verkaufsbereich31 as t2so, T2.CustomerPotentialGroup1 as t2cp, ' +
            'IsNull(T2.Revenue1, 0.0) as t2r, IsNull(T2.Units1,0) as t2u, T2.ArticleCount1 as t2ac, IsNull(T2.Margin1, 0.0) as t2mar, T2.TransactionCount1 as t2tc, ' +
            'T3.customerId as t3ci, T3.CustomerCreationDate as t3ccd, T3.InvoiceDateYear as t3idy, T3.TotalRevenue as t3tr, T3.TotalUnits as t3tu, T3.AllArticleCount as t3aac, ' +
            'T3.TotalMargin as t3tmar, T3.TotalTransactionCount as t3ttc ' +
            'FROM T1 ' +
            'RIGHT OUTER JOIN T2 ON T1.customerId = T2.customerId AND T1.InvoiceDateYear = T2.InvoiceDateYear1 AND ' +
            'T1.BrancheCategory = T2.BrancheCategory1 AND T1.Verkaufsbereich3 = T2.Verkaufsbereich31 AND T1.CustomerPotentialGroup = T2.CustomerPotentialGroup1 ' +
            'INNER JOIN T3 ON T3.customerId = T2.customerId AND T3.InvoiceDateYear = T2.InvoiceDateYear1 '

    return sqlStatement
}