/**
 * This class contains the algorithm and which gets the data from transaction. It aggregates two types of data i.e. one at customer summary level and another detail level data
 * to be processed to record impact of wrong categorization in the different level.
 */
def fetchDataFromTransaction(ctx, customerId) {
    def ccdTRFields = CCTransactionDataComputationUtil.getFieldsForGettingCCDTransRec()
    def ccdTRFilter = [
            Filter.in('SalesType', api.local.salesTypeCriteria),
            Filter.equal('CustomerId', customerId)
    ]

    def nonCCDFields = CCTransactionDataComputationUtil.getFieldsForGettingNonCCDTransRec()
    def nonCCDFilters = [
            Filter.or(Filter.isNull("SalesType"), Filter.notIn('SalesType', api.local.salesTypeCriteria)),
            Filter.equal('CustomerId', customerId)
    ]

    def summaryFields = CCTransactionDataComputationUtil.getFieldsForGettingTransRecSummary()
    def summaryFilters = [
            Filter.equal('CustomerId', customerId)
    ]

    def joinedSQL = CCTransactionDataComputationUtil.getInnerJoinForSummaryNDetailTransRec()
    def joinSources = [
            ['Source': 'DL_CCTransactionImpactPerCustGroup', 'SourceType': 'Rollup', 'Fields': ccdTRFields, 'Filters': ccdTRFilter, 'OrderBy': 'CustomerId'],
            ['Source': 'DL_CCTransactionImpactPerCustGroup', 'SourceType': 'Rollup', 'Fields': nonCCDFields, 'Filters': nonCCDFilters, 'OrderBy': 'CustomerId'],
            ['Source': 'DL_CCTransactionImpactPerCustGroup', 'SourceType': 'Rollup', 'Fields': summaryFields, 'Filters': summaryFilters, 'OrderBy': 'CustomerId'],
    ]
    return lib.DBQueries.executeJoinQueryToFetchResult(joinSources, joinedSQL)
}

def getCorrectValue(def currentValue, value1) {
    def output = null
    if (!currentValue) {
        output = (value1) ?: 0.0
    } else {
        output = currentValue + ((value1) ?: 0.0)
    }
    return output
}

def aggregateValues(def Values) {
    def customerRec = [:]
    boolean isFirstTime = true
    Values.each { it ->
        if (isFirstTime) {
            customerRec['t3idy'] = it['t3idy']
            customerRec['t3ci'] = it['t3ci']
            customerRec['t1nm'] = it['t1nm'] ?: it['t2nm1']
            customerRec['t1so'] = it['t1so'] ?: it['t2so']
            customerRec['t1brc'] = it['t1brc'] ?: ((it['t2brc']) ?: '-')
            customerRec['t3tr'] = it['t3tr']
            customerRec['t3ttc'] = it['t3ttc']
            customerRec['t3aac'] = it['t3aac']
            customerRec['t3tmar'] = it['t3tmar']
            customerRec['t3tu'] = it['t3tu']
            customerRec['t1cpg'] = it['t1cpg'] ?: it['t2cpg']
            customerRec['t3ccd'] = it['t3ccd']
        }
        customerRec['t1r'] = getCorrectValue(isFirstTime ? null : customerRec['t1r'], it['t1r'])
        customerRec['t2r'] = getCorrectValue(isFirstTime ? null : customerRec['t2r'], it['t2r'])
        customerRec['t1mar'] = getCorrectValue(isFirstTime ? null : customerRec['t1mar'], it['t1mar'])
        customerRec['t2mar'] = getCorrectValue(isFirstTime ? null : customerRec['t2mar'], it['t2mar'])
        customerRec['t1u'] = getCorrectValue(isFirstTime ? null : customerRec['t1u'], it['t1u'])
        customerRec['t2u'] = getCorrectValue(isFirstTime ? null : customerRec['t2u'], it['t2u'])
        customerRec['t1ac'] = getCorrectValue(isFirstTime ? null : customerRec['t1ac'], it['t1ac'])
        customerRec['t2ac'] = getCorrectValue(isFirstTime ? null : customerRec['t2ac'], it['t2ac'])
        customerRec['t1tc'] = getCorrectValue(isFirstTime ? null : customerRec['t1tc'], it['t1tc'])
        customerRec['t2tc'] = getCorrectValue(isFirstTime ? null : customerRec['t2tc'], it['t2tc'])
        isFirstTime = false
    }
    return customerRec
}

def aggregateRecords(List ccdTransactionSummaryData) {
    def mapOfValues = ccdTransactionSummaryData.groupBy({ transaction -> transaction.t3idy })
    def returnList = []
    def currentValue
    mapOfValues.each { key, Values ->
        currentValue = aggregateValues(Values)
        returnList.add(currentValue)
    }
    return returnList
}

def customerId = out.CurrentCustomerToProcess
def ctx = api.getDatamartContext()
def ccdTransactionSummaryData = fetchDataFromTransaction(ctx, customerId)
lib.TraceUtility.developmentTrace("Old Output found", ccdTransactionSummaryData)
def aggregatedRecords = aggregateRecords(ccdTransactionSummaryData)
lib.TraceUtility.developmentTrace("Output found", aggregatedRecords)
CCOutputComputation.populateCustomerCategoryRecords(aggregatedRecords)
lib.TraceUtility.developmentTrace("Output Data", ccdTransactionSummaryData)

return true