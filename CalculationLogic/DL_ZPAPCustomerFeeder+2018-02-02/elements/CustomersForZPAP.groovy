feedObjects()

def feedObjects() {
    def customers = getAllCustomersToProcess()
    def curCustomer
    customers.each { customerId, value ->
        curCustomer = api.customer("id", customerId) as Long
        api.emitPersistedObject("C", curCustomer)
    }
}

def getAllCustomersToProcess() {
    def filters = [
            Filter.equal("name", "S_ZPAP")]
    		//Filter.in("attribute4",['0001025359', '0001058797', '0001137437', '0001143646'])]
      
    def zpapCustomersStream = api.stream("PX", 'attribute4', ["sku", "attribute4"], *filters)
    def zpapCustomers = [:]
    if (zpapCustomersStream) {
        while (zpapCustomersStream.hasNext()) {
            def zpapEntry = zpapCustomersStream.next()
            zpapCustomers.put(zpapEntry.attribute4,
                              zpapEntry.attribute4)
        }
        zpapCustomersStream.close()
    }

    lib.TraceUtility.developmentTrace("Total number of customers retrieved", (zpapCustomers) ? zpapCustomers.size() + "" : "")
    return zpapCustomers
}





