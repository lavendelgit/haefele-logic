Map queryBuilder = queryBuilder().setGrossPriceColumn(out.GrossPriceColumn)
                                 .setLandingPriceColumn(out.LandingPriceColumn)
                                 .setDataColumns(Constants.DATA_COLUMNS_DEF)
                                 .setDecimalPlaceMap(Constants.DECIMAL_PLACE)
                                 .setChangePercent(out.PriceChangePer)
                                 .setIsLandingPriceChange(out.IsLandingPriceChange)
                                 .setIsGrossPriceChange(out.IsGrossPriceChange)
                                 .setRecommendedGrossMargin(out.ExpectedMarginPer)

List exceptions = out.Exceptions
exceptions.each { Map exception ->
    def priceChangePercent = exception['Price Change Percent']
    if (exception.Filter && priceChangePercent != null) {
        priceChangePercent = priceChangePercent.toBigDecimal() / 100
        queryBuilder.newException()
                    .setDataFilter(exception.Filter)
                    .setChangePercent(priceChangePercent)
                    .addException()
    }
}

return queryBuilder


/**
 * QueryBuilder for exceptions
 * @return
 */
protected Map exceptionsQueryBuilder() {
    Map methods = [:]
    Map definition = [exceptions: []]

    methods += [
            forExceptions                 : { List exceptions ->
                definition.exceptions = exceptions
                return methods
            },
            withBaseChangePercent         : { BigDecimal baseChangePercent ->
                definition.baseChangePercent = baseChangePercent
                return methods
            },
            withBaseRecommendedGrossMargin: { BigDecimal baseRecommendedGrossMargin ->
                definition.baseRecommendedGrossMargin = baseRecommendedGrossMargin
                return methods
            },
            forDataSourceName             : { String dataSourceName ->
                definition.dataSourceName = dataSourceName
                return methods
            },
            addBaseException              : {
                Map baseException = exception().setChangePercent(definition.baseChangePercent)
                                               .setRecommendedGrossMargin(definition.baseRecommendedGrossMargin)
                                               .setType('B')
                                               .getDefinition()
                definition.exceptions += [baseException]

                return methods
            },
            getAggregationExpr            : { String columnName ->
                return "MAX(${columnName})"
            },
            getExceptionQuery             : { Map exception ->
                if (exception.changePercent != null || exception.recommendedGrossMargin != null) {
                    StringBuilder query = new StringBuilder('SELECT ')
                            .append('Material AS ExceptionMaterial,')
                            .append(exception.changePercent != null ? exception.changePercent : definition.baseChangePercent).append(' AS ExceptionChangePercent,')
                            .append(exception.recommendedGrossMargin != null ? exception.recommendedGrossMargin : definition.baseRecommendedGrossMargin).append(' AS ExceptionRecommendedGrossMargin')
                            .append(' FROM ').append(definition.dataSourceName)
                    if (exception.dataFilter) {
                        query.append(' WHERE ').append(exception.dataFilter)
                    }

                    return query.toString()
                }
            },
            getQueryString                : {
                if (definition.exceptions) {
                    //methods.addBaseException()
                    StringBuilder query = new StringBuilder('SELECT ExceptionMaterial AS ExceptionMaterial,')
                            .append(methods.getAggregationExpr('ExceptionChangePercent')).append(' AS ExceptionChangePercent,')
                            .append(methods.getAggregationExpr('ExceptionRecommendedGrossMargin')).append(' AS ExceptionRecommendedGrossMargin')
                            .append(' FROM ((')
                            .append(
                                    definition.exceptions.collect { Map exception ->
                                        methods.getExceptionQuery(exception)
                                    }?.findAll { String strQuery ->
                                        strQuery != null
                                    }?.join(' ) UNION ( ')
                            )
                            .append(' )) ExceptionRows GROUP BY ExceptionMaterial')

                    return query.toString()
                }
            }
    ]
    return methods
}

/**
 * Map to hold exception data
 * @return
 */
protected Map exception() {
    Map methods = [:]
    Map definition = [type: 'E']

    methods += [
            forParent                : { Map parent ->
                definition.parent = parent
                return methods
            },
            setDataFilter            : { String dataFilter ->
                definition.dataFilter = dataFilter
                return methods
            },
            setChangePercent         : { BigDecimal changePercent ->
                definition.changePercent = changePercent
                return methods
            },
            setRecommendedGrossMargin: { BigDecimal recommendedGrossMargin ->
                definition.recommendedGrossMargin = recommendedGrossMargin
                return methods
            },
            setType                  : { String type ->
                definition.type = type ?: 'E'
                return methods
            },
            addException             : {
                return definition.parent.addException(methods.getDefinition())
            },
            getDefinition            : {
                return definition.findAll { param -> param.key != 'parent' }
            }
    ]
    return methods
}

/**
 * Base query builder
 * @return
 */
protected Map queryBuilder() {
    Map methods = [:]
    Map definition = [exceptions: []]

    methods += [
            getBaseDataSourceName                  : {
                return 'T1'
            },
            setGrossPriceColumn                    : { String grossPriceColumn ->
                definition.grossPriceColumn = grossPriceColumn
                return methods
            },
            setLandingPriceColumn                  : { String landingPriceColumn ->
                definition.landingPriceColumn = landingPriceColumn
                return methods
            },
            setDataColumns                         : { Map dataColumns ->
                definition.dataColumns = dataColumns
                return methods
            },
            setDecimalPlaceMap                     : { Map decimalPlaceMap ->
                definition.decimalPlaceMap = decimalPlaceMap
                return methods
            },
            setChangePercent                       : { BigDecimal changePercent ->
                definition.changePercent = changePercent
                return methods
            },
            setExceptions                          : { List exceptions ->
                definition.exceptions = exceptions
                return methods
            },
            addException                           : { Map exception ->
                definition.exceptions.add(exception)
                return methods
            },
            getExceptions                          : {
                return definition.exceptions
            },
            hasExceptions                          : {
                return definition.exceptions?.size() > 0
            },
            newException                           : {
                Map exception = exception().forParent(methods)
                return exception
            },
            getExceptionQueryString                : {
                return exceptionsQueryBuilder().forExceptions(methods.getExceptions())
                                               .forDataSourceName(methods.getBaseDataSourceName())
                                               .withBaseChangePercent(definition.changePercent)
                                               .withBaseRecommendedGrossMargin(definition.recommendedGrossMargin)
                                               .getQueryString()
            },
            setIsLandingPriceChange                : { boolean isLandingPriceChange ->
                definition.isLandingPriceChange = isLandingPriceChange
                return methods
            },
            setIsGrossPriceChange                  : { boolean isGrossPriceChange ->
                definition.isGrossPriceChange = isGrossPriceChange
                return methods
            },
            setRecommendedGrossMargin              : { BigDecimal recommendedGrossMargin ->
                definition.recommendedGrossMargin = recommendedGrossMargin
                return methods
            },
            setShowBreakupByDiscountType           : { boolean showBreakupByDiscountType ->
                definition.showBreakupByDiscountType = showBreakupByDiscountType
                return methods
            },
            getCurrentBaseCostExpr                 : {
                return 'BaseCost'
            },
            getExpectedBaseCostExpr                : {
                if (!definition.expectedBaseCostExpr) {
                    definition.expectedBaseCostExpr = definition.isLandingPriceChange ? "(BaseCost + (BaseCost * ${methods.getChangePercent()}))" : "BaseCost"
                }
                return definition.expectedBaseCostExpr
            },
            getRecommendedBaseCostExpr             : {
                if (definition.recommendedGrossMargin != null && !definition.recommendedBaseCostExpr) {
                    definition.recommendedBaseCostExpr = definition.isLandingPriceChange ? "(BaseCost + (BaseCost * ${methods.getChangePercent()}))" : "BaseCost"
                }
                return definition.recommendedBaseCostExpr
            },
            getCurrentGrossPriceExpr               : {
                return "GrossPrice"
            },
            getExpectedGrossPriceExprForPocketPrice: {
                if (!definition.expectedGrossPriceExprForPocketPrice) {
                    definition.expectedGrossPriceExprForPocketPrice = definition.isGrossPriceChange ? """
                                                (CASE WHEN ConditionName = 'ZRI' AND ConditionTable = 'A617' AND ZPL IS NOT NULL THEN ZPL - (ZPL * ${definition.changePercent}) 
                                                      ELSE (GrossPrice + (GrossPrice * ${methods.getChangePercent()})) 
                                                END)
                                                """ : "GrossPrice"
                }
                return definition.expectedGrossPriceExprForPocketPrice
            },
            getExpectedGrossPriceExpr              : {
                if (!definition.expectedGrossPriceExpr) {
                    definition.expectedGrossPriceExpr = definition.isGrossPriceChange ? "(GrossPrice + (GrossPrice * ${methods.getChangePercent()}))" : "GrossPrice"
                }
                return definition.expectedGrossPriceExpr
            },
            getRecommendedGrossPriceExpr           : {
                if (definition.recommendedGrossMargin != null && !definition.recommendedGrossPriceExpr) {
                    definition.recommendedGrossPriceExpr = "(${methods.getExpectedBaseCostExpr()}/NULLIF(1-${definition.recommendedGrossMargin},0))"
                }
                return definition.recommendedGrossPriceExpr
            },
            getContributionColumnName              : { String discountType, String calculationType ->
                String calculationTypePrefix = calculationType.substring(0, 3).toLowerCase()
                return "${calculationTypePrefix}_${discountType}DiscountRevenueContribution"
            },
            getPocketPriceExprByDiscountType       : { String pricingType, String calculationType ->
                String queryExpr = ""
                if (calculationType == Constants.CALCULATION_TYPES.CURRENT) {
                    switch (pricingType) {
                        case Constants.PRICING_TYPES.MARKUP:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.MARKUP}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.NET_PRICE:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.NET_PRICE}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.ABSOULTE_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.ABSOULTE_DISCOUNT}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.PER_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.PER_DISCOUNT}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.X_ARTICLE:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.X_ARTICLE}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.INTERNAL:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.INTERNAL}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.NO_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.NO_DISCOUNT}' THEN HistoryPocketPrice"
                            break
                    }
                } else {
                    String baseCostExpr = null
                    String grossPriceExpr = null
                    switch (calculationType) {
                        case Constants.CALCULATION_TYPES.CURRENT:
                            baseCostExpr = methods.getCurrentBaseCostExpr()
                            grossPriceExpr = methods.getCurrentGrossPriceExpr()
                            break
                        case Constants.CALCULATION_TYPES.EXPECTED:
                        case Constants.CALCULATION_TYPES.TEMPORARY:
                            baseCostExpr = methods.getExpectedBaseCostExpr()
                            grossPriceExpr = methods.getExpectedGrossPriceExprForPocketPrice()
                            break
                        case Constants.CALCULATION_TYPES.RECOMMEND:
                            baseCostExpr = methods.getRecommendedBaseCostExpr()
                            grossPriceExpr = methods.getRecommendedGrossPriceExpr()
                            break
                    }
                    switch (pricingType) {
                        case Constants.PRICING_TYPES.MARKUP:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.MARKUP}' THEN ${baseCostExpr} * (1 + KBETR)"
                            break
                        case Constants.PRICING_TYPES.NET_PRICE:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.NET_PRICE}' THEN KBETR"
                            break
                        case Constants.PRICING_TYPES.ABSOULTE_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.ABSOULTE_DISCOUNT}' THEN ${grossPriceExpr} - KBETR"
                            break
                        case Constants.PRICING_TYPES.PER_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.PER_DISCOUNT}' THEN ${grossPriceExpr} * (1 + KBETR)"
                            break
                        case Constants.PRICING_TYPES.X_ARTICLE:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.X_ARTICLE}' THEN CASE WHEN QtySold <> 0 THEN ROUND(TotalRevenue / QtySold, ${Constants.ROUNDING_DECIMAL_PLACES}) ELSE TotalRevenue END "
                            break
                        case Constants.PRICING_TYPES.INTERNAL:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.INTERNAL}' THEN CASE WHEN QtySold <> 0 THEN ROUND(TotalRevenue / QtySold, ${Constants.ROUNDING_DECIMAL_PLACES}) ELSE TotalRevenue END "
                            break
                        case Constants.PRICING_TYPES.NO_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.NO_DISCOUNT}' THEN ${grossPriceExpr}"
                            break
                    }
                }
                return queryExpr
            },
            getContributionQuery                   : { String builderContext, String calculationType, String discountType, String pricingType ->
                String queryFragment = ""
                String level3RevColName = methods.getContributionColumnName(discountType, calculationType)
                String level2PocketPriceColName = methods.getContributionColumnName("AvgPP${discountType}", calculationType)
                String level2RevenueColName = methods.getContributionColumnName("AvgRev${discountType}", calculationType)
                String level2PocketMarginColName = methods.getContributionColumnName("Margin${discountType}", calculationType)
                String level2TotalPocketMarginColName = methods.getContributionColumnName("TotalMargin${discountType}", calculationType)
                String pCaseDiscountType = discountType.uncapitalize()
                String uCaseDiscountType = discountType.toUpperCase()
                String calculationTypePrefix = calculationType.substring(0, 3).toLowerCase()
                String uCaseCalculationType = calculationType.toUpperCase()
                String costColumnName = Constants.CALCULATION_TYPE_COSTS[calculationType]

                switch (builderContext) {
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL3:
                        String attributeName = "${calculationTypePrefix}${pCaseDiscountType}DiscountContributionPerRowQuery"
                        if (!definition[attributeName]) {
                            definition[attributeName] = "(CASE ${methods.getPocketPriceExprByDiscountType(pricingType, calculationType)} ELSE 0 END) as ${level3RevColName}"
                        }
                        queryFragment = definition[attributeName]
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                        String attributeName = "${calculationTypePrefix}${pCaseDiscountType}DiscountContributionAggQuery"
                        //String pocketMarginExpr = "CASE WHEN ${level3RevColName} <> 0 THEN ${level3RevColName} - ${costColumnName} ELSE 0 END"
//                        String pocketMarginExpr = "CASE WHEN PricingType = '${pricingType}' THEN ${level3RevColName} - ${costColumnName} ELSE 0 END"
                        String pocketMarginExpr = pricingType in [Constants.PRICING_TYPES.X_ARTICLE, Constants.PRICING_TYPES.INTERNAL, Constants.PRICING_TYPES.NET_PRICE] ?
                                                  "CASE WHEN ${level3RevColName} <> 0 THEN ${level3RevColName} - ${costColumnName} ELSE 0 END" :
                                                  "CASE WHEN PricingType = '${pricingType}' THEN ${level3RevColName} - ${costColumnName} ELSE 0 END"
                        if (!definition[attributeName]) {
                            definition[attributeName] = """AVG(${level3RevColName})                                     as ${level2PocketPriceColName},
                                                           AVG(${level3RevColName} * QtySold)                           as ${level2RevenueColName},
                                                           AVG(${pocketMarginExpr})                                     as ${level2PocketMarginColName},
                                                           AVG((${pocketMarginExpr}) *  QtySold)                        as ${level2TotalPocketMarginColName}"""
                        }
                        queryFragment = definition[attributeName]
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                        String attributeName = "${calculationTypePrefix}${pCaseDiscountType}DiscountContributionQuery"
                        int decimalPlace =  Constants.ROUNDING_DECIMAL_PLACES
                        Map DATA_COLUMNS = definition.dataColumns
                        if (!definition[attributeName]) {
                            definition[attributeName] = """ ROUND(AVG(${level2PocketPriceColName}),${decimalPlace})                                                as '${DATA_COLUMNS[uCaseCalculationType + '_POCKET_PRICE_BY_' + uCaseDiscountType].name}',    
                                                            ROUND(SUM(${level2RevenueColName}),${decimalPlace})                                                    as '${DATA_COLUMNS[uCaseCalculationType + '_REVENUE_BY_' + uCaseDiscountType].name}',    
                                                            ROUND(AVG(${level2PocketMarginColName}),${decimalPlace})                                               as '${DATA_COLUMNS[uCaseCalculationType + '_POCKET_MARGIN_BY_' + uCaseDiscountType].name}',    
                                                            ROUND(SUM(${level2TotalPocketMarginColName}),${decimalPlace})                                          as '${DATA_COLUMNS[uCaseCalculationType + '_TOTAL_POCKET_MARGIN_BY_' + uCaseDiscountType].name}',    
                                                            ROUND(SUM(${level2TotalPocketMarginColName}) / NULLIF(SUM(${level2RevenueColName}),0),${decimalPlace}) as '${DATA_COLUMNS[uCaseCalculationType + '_POCKET_MARGIN_PERCENT_BY_' + uCaseDiscountType].name}'"""
                        }
                        queryFragment = definition[attributeName]
                        break
                }
                return queryFragment
            },
            getMarkupDiscountContributionQuery     : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.MARKUP, Constants.PRICING_TYPES.MARKUP)
            },
            getNetPriceDiscountContributionQuery   : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.NET_PRICE, Constants.PRICING_TYPES.NET_PRICE)
            },
            getAbsoluteDiscountContributionQuery   : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.ABSOULTE_DISCOUNT, Constants.PRICING_TYPES.ABSOULTE_DISCOUNT)
            },
            getPercentDiscountContributionQuery    : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.PER_DISCOUNT, Constants.PRICING_TYPES.PER_DISCOUNT)
            },
            getXArticleContributionQuery           : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.X_ARTICLE, Constants.PRICING_TYPES.X_ARTICLE)
            },
            getInternalContributionQuery           : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.INTERNAL, Constants.PRICING_TYPES.INTERNAL)
            },
            getNonDiscountContributionQuery        : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.NO_DISCOUNT, Constants.PRICING_TYPES.NO_DISCOUNT)
            },
            getChangePercent                       : {
                return methods.hasExceptions() ? "COALESCE(SIMULATION_DATA.ExceptionChangePercent,${definition.changePercent})" : definition.changePercent
            },
            getPriceChangeFragment                 : { String builderContext ->
                String queryFragment = ""
                def changePercent = methods.getChangePercent()
                if (changePercent == null) {
                    return queryFragment
                }
                switch (builderContext) {
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL3:
                        String expectedBaseCostExpr = methods.getExpectedBaseCostExpr()
                        queryFragment = """     ,
                                                ${changePercent}                                                                                            as ChangePercent,  
                                                ${expectedBaseCostExpr}                                                                                     as ExpectedBaseCost,
                                                ${methods.getExpectedGrossPriceExpr()}                                                                      as TmpGrossPrice,              
                                                (CASE
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.MARKUP, Constants.CALCULATION_TYPES.TEMPORARY)}
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.NET_PRICE, Constants.CALCULATION_TYPES.TEMPORARY)} 
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.ABSOULTE_DISCOUNT, Constants.CALCULATION_TYPES.TEMPORARY)} 
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.PER_DISCOUNT, Constants.CALCULATION_TYPES.TEMPORARY)}
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.INTERNAL, Constants.CALCULATION_TYPES.TEMPORARY)} 
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.X_ARTICLE, Constants.CALCULATION_TYPES.TEMPORARY)}
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.NO_DISCOUNT, Constants.CALCULATION_TYPES.TEMPORARY)}
                                                ELSE 
                                                        ${methods.getExpectedGrossPriceExprForPocketPrice()}
                                                END)                                                                                                        as TmpPocketPrice,
                                                ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)}"""
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                        queryFragment = """ ,
                                        AVG(ChangePercent)                                                          as AvgChangePercent,
                                        AVG(ExpectedGrossPrice)                                                     as AvgExpectedGrossPrice,
                                        AVG(ExpectedBaseCost)                                                       as AvgExpectedBaseCost,
                                        AVG(ExpectedBaseCost * QtySold)                                             as TotalExpectedBaseCost,
                                        AVG((ExpectedGrossPrice - ExpectedBaseCost) / NULLIF(ExpectedBaseCost,0))   as AvgExpectedMarkup,
                                        AVG(ExpectedGrossPrice - ExpectedBaseCost)                                  as AvgExpectedMargin,                                
                                        AVG(ExpectedPocketPrice)                                                    as AvgExpectedPocketPrice,
                                        AVG(ExpectedPocketPrice * QtySold)                                          as ExpectedRevenue,
                                        AVG((ExpectedGrossPrice - ExpectedBaseCost) * QtySold)                      as ExpectedMargin,
                                        AVG(ExpectedPocketPrice - ExpectedGrossPrice)                               as AvgExpectedDiscount,                                
                                        AVG(ExpectedPocketPrice - ExpectedBaseCost)                                 as AvgExpectedPocketMargin,
                                        AVG((ExpectedPocketPrice - ExpectedBaseCost) * QtySold)                     as ExpectedPocketMargin,
                                        ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)}"""
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                        Map DECIMAL_PLACE = definition.decimalPlaceMap
                        Map DATA_COLUMNS = definition.dataColumns
                        String grossPriceColumn = definition.grossPriceColumn
                        String landingPriceColumn = definition.landingPriceColumn
                        queryFragment = """,
                                    AVG(AvgChangePercent)                                                           as '${DATA_COLUMNS.PRICE_CHANGE_PERCENT.name}',
                                    AVG(AvgExpectedGrossPrice)                                                      as '${grossPriceColumn}',
                                    AVG(AvgExpectedDiscount)                                                        as '${DATA_COLUMNS.EXPECTED_DISCOUNT.name}',
                                    AVG(AvgExpectedPocketPrice)                                                     as '${DATA_COLUMNS.EXPECTED_POCKET_PRICE.name}',    
                                    ROUND(SUM(ExpectedRevenue), ${Constants.ROUNDING_DECIMAL_PLACES})                                                            as '${DATA_COLUMNS.EXPECTED_REVENUE.name}',  
                                    AVG(AvgExpectedBaseCost)                                                        as '${landingPriceColumn}',   
                                    AVG(AvgExpectedMargin)                                                          as '${DATA_COLUMNS.EXPECTED_MARGIN_PER_UNIT.name}',    
                                    SUM(ExpectedMargin)                                                             as '${DATA_COLUMNS.EXPECTED_MARGIN.name}',                                        
                                    AVG(AvgPocketMargin)                                                            as '${DATA_COLUMNS.POCKET_MARGIN.name}',
                                    AVG(AvgExpectedPocketMargin)                                                    as '${DATA_COLUMNS.EXPECTED_POCKET_MARGIN_PER_UNIT.name}',
                                    AVG(AvgExpectedPocketPrice - AvgBaseCost)/NULLIF(AVG(AvgExpectedPocketPrice),0) as '${DATA_COLUMNS.EXPECTED_POCKET_MARGIN_PERCENT.name}',
                                    SUM(ExpectedPocketMargin)                                                       as '${DATA_COLUMNS.EXPECTED_POCKET_MARGIN.name}',
                                    SUM(TotalExpectedBaseCost)                                                      as '${DATA_COLUMNS.TOTAL_EXPECTED_LANDING_COST.name}',
                                    ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)}"""
                }
                return queryFragment
            },
            getMarginChangeFragment                : { String builderContext ->
                String queryFragment = ""
                BigDecimal recommendedGrossMargin = definition.recommendedGrossMargin
                if (recommendedGrossMargin == null) {
                    return queryFragment
                }
                switch (builderContext) {
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL3:
                        String recommendedGrossPriceExpr = methods.getRecommendedGrossPriceExpr()
                        String recommendedBaseCostExpr = methods.getRecommendedBaseCostExpr()
                        queryFragment = """
                                        ,
                                        ${recommendedGrossMargin}                                                   as RecMargin,
                                        (CASE WHEN PricingType = '${Constants.PRICING_TYPES.PER_DISCOUNT}' THEN ${recommendedGrossPriceExpr}/(1 + KBETR)
                                              ELSE ${recommendedGrossPriceExpr}
                                        END)                                                                        as RecGrossPrice,    
                                        ${recommendedGrossPriceExpr}                                                as RecPocketPrice,
                                        ${recommendedBaseCostExpr}                                                  as RecBaseCost                            
                            """
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                        queryFragment = """,
                                        AVG(RecMargin)                                                                          as AvgRecMargin,
                                        AVG(RecGrossPrice)                                                                      as AvgRecGrossPrice,
                                        AVG(RecPocketPrice)                                                                     as AvgRecPocketPrice,
                                        AVG(RecPocketPrice * QtySold)                                                           as AvgRecRevenue,
                                        AVG((RecPocketPrice * QtySold) - TotalRevenue)                                          as AvgRecRevenueImpact,
                                        AVG((RecPocketPrice - RecBaseCost) * QtySold)                                           as AvgRecPocketMargin,
                                        AVG(TotalRevenue / NULLIF(RecPocketPrice,0))                                            as AvgRecQty"""
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                        Map DECIMAL_PLACE = definition.decimalPlaceMap
                        Map DATA_COLUMNS = definition.dataColumns
                        queryFragment = """,
                                    AVG(AvgRecMargin)               as '${DATA_COLUMNS.RECOMMENDED_MARGIN_PERCENT.name}',
                                    AVG(AvgRecGrossPrice)           as '${DATA_COLUMNS.RECOMMENDED_GROSS_PRICE.name}',
                                    AVG(AvgRecPocketPrice)          as '${DATA_COLUMNS.RECOMMENDED_POCKET_PRICE.name}',
                                    SUM(AvgRecRevenue)              as '${DATA_COLUMNS.REVENUE_USING_RECOMMENDED_PRICE.name}',
                                    SUM(AvgRecRevenueImpact)        as '${DATA_COLUMNS.REVENUE_DELTA_WITH_RECOMMENDED_PRICE.name}',                                    
                                    SUM(AvgRecQty)                  as '${DATA_COLUMNS.EXPECTED_QTY_WITH_RECOMMENDED_PRICE.name}',
                                    SUM(AvgRecPocketMargin)         as '${DATA_COLUMNS.POCKET_MARGIN_USING_RECOMMENDED_PRICE.name}'"""
                        break
                }
                return queryFragment
            },
            getBreakupByDiscountTypeFragment       : { String builderContext ->
                String queryFragment = ""
                if (!definition.showBreakupByDiscountType) {
                    return queryFragment
                }
                switch (builderContext) {
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL3:
                        String recommendedGrossPriceExpr = methods.getRecommendedGrossPriceExpr()
                        String recommendedBaseCostExpr = methods.getRecommendedBaseCostExpr()
                        queryFragment = """
                                        ,
                                        ${recommendedGrossMargin}                                                       as RecMargin,
                                        (CASE WHEN PricingType = '${Constants.PRICING_TYPES.PER_DISCOUNT}' THEN ${recommendedGrossPriceExpr}/(1 + KBETR)
                                              ELSE ${recommendedGrossPriceExpr}
                                        END)                                                                            as RecGrossPrice,    
                                        ${recommendedGrossPriceExpr}                                                    as RecPocketPrice,
                                        ${recommendedBaseCostExpr}                                                      as RecBaseCost                            
                            """
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                        queryFragment = """,
                                        AVG(RecMargin)                                                                  as AvgRecMargin,
                                        AVG(RecGrossPrice)                                                              as AvgRecGrossPrice,
                                        AVG(RecPocketPrice)                                                             as AvgRecPocketPrice,
                                        AVG(RecPocketPrice * QtySold)                                                   as AvgRecRevenue,
                                        AVG((RecPocketPrice * QtySold) - TotalRevenue)                                  as AvgRecRevenueImpact,
                                        AVG((RecPocketPrice - RecBaseCost) * QtySold)                                   as AvgRecPocketMargin,
                                        AVG(TotalRevenue / NULLIF(RecPocketPrice,0))                                    as AvgRecQty"""
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                        Map DECIMAL_PLACE = definition.decimalPlaceMap
                        Map DATA_COLUMNS = definition.dataColumns
                        queryFragment = """,
                                    (AVG(AvgRecMargin) * 100) || ' %'               as '${DATA_COLUMNS.RECOMMENDED_MARGIN_PERCENT.name}',
                                    AVG(AvgRecGrossPrice)                           as '${DATA_COLUMNS.RECOMMENDED_GROSS_PRICE.name}',
                                    AVG(AvgRecPocketPrice)                          as '${DATA_COLUMNS.RECOMMENDED_POCKET_PRICE.name}',
                                    SUM(AvgRecRevenue)                              as '${DATA_COLUMNS.REVENUE_USING_RECOMMENDED_PRICE.name}',
                                    SUM(AvgRecRevenueImpact)                        as '${DATA_COLUMNS.REVENUE_DELTA_WITH_RECOMMENDED_PRICE.name}',                                    
                                    SUM(AvgRecQty)                                  as '${DATA_COLUMNS.EXPECTED_QTY_WITH_RECOMMENDED_PRICE.name}',
                                    SUM(AvgRecPocketMargin)                         as '${DATA_COLUMNS.POCKET_MARGIN_USING_RECOMMENDED_PRICE.name}'"""
                        break
                }
                return queryFragment
            },
            getCalculatePerRowSQLQuery             : {
                String priceChangeQueryFragement = methods.getPriceChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL3)
                String marginChangeQueryFragment = methods.getMarginChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL3)
                String tmpCalc = Constants.CALCULATION_TYPES.TEMPORARY
                String expCalc = Constants.CALCULATION_TYPES.EXPECTED
                String currCalc = Constants.CALCULATION_TYPES.CURRENT
                definition.calculatePerRowSQLQuery = """  
                                            SELECT 
                                                *,
                                                HistoryPocketPrice  * ChangeRatio                                             	                                As ExpectedPocketPrice,
                                                ${methods.getContributionColumnName('Markup', currCalc)}  * ChangeRatio		                                    As ${methods.getContributionColumnName('Markup', expCalc)},
                                                ${methods.getContributionColumnName('NetPrice', currCalc)}  * ChangeRatio	                                    As ${methods.getContributionColumnName('NetPrice', expCalc)},
                                                ${methods.getContributionColumnName('Absolute', currCalc)}  * ChangeRatio	                                    As ${methods.getContributionColumnName('Absolute', expCalc)},
                                                ${methods.getContributionColumnName('Percent', currCalc)}  * ChangeRatio		                                As ${methods.getContributionColumnName('Percent', expCalc)},
                                                ${methods.getContributionColumnName('XARTICLE', currCalc)}  * ChangeRatio	                                    As ${methods.getContributionColumnName('XARTICLE', expCalc)},
                                                ${methods.getContributionColumnName('INTERNAL', currCalc)}  * ChangeRatio                                       As ${methods.getContributionColumnName('INTERNAL', expCalc)},
                                                ${methods.getContributionColumnName('NoDiscounts', currCalc)}  * ChangeRatio                                    As ${methods.getContributionColumnName('NoDiscounts', expCalc)},
                                                TmpGrossPrice  * ChangeRatio																		            As ExpectedGrossPrice,
                                                HistoryPocketMargin                                                                                             As PocketMargin,
                                                (HistoryPocketMargin / NULLIF(PocketPrice,0))                                                                   As PocketMarginPercent
                                           FROM (          
                                                SELECT 
                                                    Material                                                                                                    As Material,
                                                    ProdhII                                                                                                     As ProdhII,
                                                    ProdhIII                                                                                                    As ProdhIII,
                                                    prodh_cm1                                                                                                   As prodh_cm1,
                                                    prodh_cm2                                                                                                   As prodh_cm2,
                                                    SecondaryKey                                                                                                As SecondaryKey,
                                                    CustomerId                                                                                                  as CustomerId,
                                                    ConditionName                                                                                               as ConditionName,
                                                    PricingType                                                                                                 as PricingType,
                                                    TotalRevenue                                                                                                as ActualRevenue,
                                                    coalesce(Material,'-') || '-' || coalesce(CustomerId,'-') || '-' || coalesce(Year,'-')                      as TxnKey,
                                                    (CASE WHEN CustomerId IS NULL THEN T1.TotalRevenue - CUST_DATA.CustomerRevenue ELSE T1.TotalRevenue END)    as TotalRevenue,
                                                    (CASE WHEN CustomerId IS NULL THEN T1.QtySold - CUST_DATA.CustomerQtySold ELSE T1.QtySold END)              as QtySold,    
                                                    CUST_DATA.CustomerPocketMargin                                                                              as CustomerPocketMargin,                             
                                                    AvgGrossPrice                                                                                               as AvgGrossPrice,
                                                    GrossPrice                                                                                                  as GrossPrice,   
                                                    ZPL                                                                                                         as ZPL,   
                                                    KBETR                                                                                                       as KBETR,
                                                    BaseCost                                                                                                    as BaseCost,     
                                                    Margin                                                                                                      as Margin,    
                                                    PocketPrice                                                                                                 as PocketPrice                                                    
                                                    ${priceChangeQueryFragement}    
                                                    ${marginChangeQueryFragment}                       
                                                FROM T1 
                                                LEFT JOIN (    
                                                        SELECT 
                                                            Material                                                                                            AS CustomerMaterial,                               
                                                            SUM((CASE WHEN CustomerId IS NOT NULL THEN QtySold ELSE 0 END))                                     as CustomerQtySold,                        
                                                            SUM((CASE WHEN CustomerId IS NOT NULL THEN TotalRevenue ELSE 0 END))                                as CustomerRevenue,
                                                            SUM((CASE WHEN CustomerId IS NOT NULL THEN PocketMargin ELSE 0 END))                                as CustomerPocketMargin
                                                        FROM (
                                                          SELECT T1.*, ROW_NUMBER() OVER (PARTITION BY Material,CustomerId ORDER BY CustomerId) rownum
                                                          FROM T1
                                                        ) T1    
                                                        WHERE rownum = 1  
                                                        GROUP BY Material
                                                 ) CUST_DATA
                                                 ON T1.Material = CUST_DATA.CustomerMaterial
                                           ) 
                                           JOIN LATERAL
                                           (
                                                SELECT 
                                                   ABS(TmpPocketPrice / NULLIF(PocketPrice,0))                                                                  As ChangeRatio,
                                                   ROUND((TotalRevenue / NULLIF(QtySold,0)), ${Constants.ROUNDING_DECIMAL_PLACES})                                                                 As HistoryPocketPrice                                                   
                                           ) TblRatio   
                                           JOIN LATERAL
                                           (
                                                SELECT
													(HistoryPocketPrice - BaseCost)                                                              				As HistoryPocketMargin,
                                                    ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, currCalc)},
                                                    ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, currCalc)},
                                                    ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, currCalc)},
                                                    ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, currCalc)},
                                                    ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, currCalc)},
                                                    ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, currCalc)},
                                                    ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, currCalc)}
                                           ) TblBreakup           
                """
                if (methods.hasExceptions()) {
                    definition.calculatePerRowSQLQuery += """
                                             LEFT JOIN (
                                                ${methods.getExceptionQueryString()}
                                             ) SIMULATION_DATA
                                             ON T1.Material = SIMULATION_DATA.ExceptionMaterial   
                        """
                }
                return definition.calculatePerRowSQLQuery
            },
            getAggregateByMaterialTxnSQLQuery      : {
                String priceChangeQueryFragement = methods.getPriceChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL2)
                String marginChangeQueryFragment = methods.getMarginChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL2)
                definition.aggregateByMaterialTxnSQLQuery = """ 
                                    SELECT 
                                        Material,
                                        ProdhII,
                                        ProdhIII,
                                        prodh_cm1,
                                        prodh_cm2,
                                        TxnKey,   
                                        COUNT(TxnKey)                       as TxnCount,                             
                                        AVG(GrossPrice)                     as AverageGrossPrice,       
                                        AVG(PocketPrice - GrossPrice)       as AverageDiscount,
                                        AVG(BaseCost)                       as AvgBaseCost,
                                        AVG(BaseCost * QtySold)             as TotalBaseCost,
                                        AVG(Margin)                         as AvgMargin,
                                        AVG(PocketPrice)                    as AvgPocketPrice,
                                        AVG(PocketMargin)                   as AvgPocketMargin,
                                        AVG(PocketPrice * QtySold)          as CurrentRevenue,
                                        AVG(Margin * QtySold)               as CurrentMargin,                             
                                        AVG(QtySold)                        as TotalQuantitySold,
                                        AVG(TotalRevenue)                   as TotalRevenue,
                                        AVG(PocketMargin * QtySold)         as CurrentPocketMargin,
                                        ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)}
                                        ${priceChangeQueryFragement}
                                        ${marginChangeQueryFragment}
                                    FROM ( ${methods.getCalculatePerRowSQLQuery()} )  LEVEL2 GROUP BY Material,TxnKey, ProdhII, ProdhIII, prodh_cm1, prodh_cm2
                        """
                return definition.aggregateByMaterialTxnSQLQuery
            },
            getSQLQuery                            : {
                Map DECIMAL_PLACE = definition.decimalPlaceMap
                Map DATA_COLUMNS = definition.dataColumns
                String priceChangeQueryFragement = methods.getPriceChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL1)
                String marginChangeQueryFragment = methods.getMarginChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL1)
                return """ 
                                SELECT 
                                    Material                                                            as '${DATA_COLUMNS.ARTICLE_NUMBER.name}',
                                    ProdhII                                                             as '${DATA_COLUMNS.PRODH_II.name}',
                                    ProdhIII                                                            as '${DATA_COLUMNS.PRODH_III.name}',
                                    prodh_cm1                                                           as '${DATA_COLUMNS.PRODH_CM1.name}',
                                    prodh_cm2                                                           as '${DATA_COLUMNS.PRODH_CM2.name}',
                                    SUM(TxnCount)                                                       as '${DATA_COLUMNS.TOTAL_TRANSACTIONS.name}',                 
                                    SUM(TotalQuantitySold)                                              as '${DATA_COLUMNS.TOTAL_QTY_SOLD.name}',
                                    ROUND(SUM(TotalRevenue), ${Constants.ROUNDING_DECIMAL_PLACES})      as '${DATA_COLUMNS.TOTAL_REVENUE.name}',                        
                                    AVG(AverageGrossPrice)                                              as '${DATA_COLUMNS.GROSS_PRICE.name}',
                                    AVG(AverageDiscount)                                                as '${DATA_COLUMNS.DISCOUNT.name}',
                                    AVG(AvgPocketPrice)                                                 as '${DATA_COLUMNS.POCKET_PRICE.name}',  
                                    ROUND(SUM(TotalRevenue), ${Constants.ROUNDING_DECIMAL_PLACES})      as '${DATA_COLUMNS.CURRENT_REVENUE.name}',
                                    AVG(AvgBaseCost)                                                    as '${DATA_COLUMNS.LANDING_COST.name}',
                                    AVG(AvgMargin)                                                      as '${DATA_COLUMNS.MARGIN.name}',            
                                    SUM(CurrentMargin)                                                  as '${DATA_COLUMNS.CURRENT_MARGIN.name}',                            
                                    AVG(AvgPocketMargin)                                                as '${DATA_COLUMNS.POCKET_MARGIN.name}',
                                    AVG(AvgPocketPrice - AvgBaseCost)/NULLIF(AVG(AvgPocketPrice),0)     as '${DATA_COLUMNS.POCKET_MARGIN_PERCENT.name}',
                                    SUM(CurrentPocketMargin)                                            as '${DATA_COLUMNS.CURRENT_POCKET_MARGIN.name}',
                                    SUM(TotalBaseCost)                                                  as '${DATA_COLUMNS.TOTAL_LANDING_COST.name}',
                                    ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)}
                                    ${priceChangeQueryFragement}    
                                    ${marginChangeQueryFragment}                                     
                                FROM 
                                 (  
                                    ${methods.getAggregateByMaterialTxnSQLQuery()}
                                 )  LEVEL1 GROUP BY Material, ProdhII, ProdhIII, prodh_cm1, prodh_cm2
                   """
            }
    ]
    return methods
}