BigDecimal expectedMarginPercent = out.ExpectedMarginPercent
return expectedMarginPercent ? expectedMarginPercent / 100 : null