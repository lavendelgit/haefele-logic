import groovy.transform.Field

@Field Map CONDITION_TYPE_FORMULA = [
        ZRI: ""
]

@Field final BigDecimal DEFAULT_PRICE_CHANGE_PERCENT = 0

@Field Map DM_COLUMNS = [
        MATERIAL           : 'Material',
        PRODHII            : 'ProdhII',
        PRODHIII           : 'ProdhIII',
        PRODHIV            : 'ProdhIV',
        PRODHV             : 'ProdhV',
        PRODHVI            : 'ProdhVI',
        PRODHVII           : 'ProdhVII',
        PRODH_CM1          : 'prodh_cm1',
        PRODH_CM2          : 'prodh_cm2',
        PRODH_CM3          : 'prodh_cm3',
        PRODH_CM4          : 'prodh_cm4',
        SECONDARY_KEY      : 'SecondaryKey',
        CONDITION_TYPE     : 'ConditionName',
        CONDITION_TABLE    : 'ConditionTable',
        PRICING_TYPE       : 'PricingType',
        YEAR               : 'Year',
        CUSTOMER_ID        : 'CustomerId',
        GROSS_PRICE        : 'GrossPrice',
        AVERAGE_GROSS_PRICE: 'AvgGrossPrice',
        QUANTITY           : 'QtySoldInland',
        LANDING_PRICE      : 'BaseCost',
        DISCOUNT           : 'KBETR',
        TOTAL_REVENUE      : 'TotalRevenueInland',
        MARKUP             : 'MarkUp',
        MARGIN             : 'Margin',
        POCKET_MARGIN      : 'PocketMargin',
        POCKET_PRICE       : 'PocketPrice',
        ZPL                : 'ZPL'
]

//@Field String DM_SIMULATION = 'PriceChangeSimulation'
@Field String DM_SIMULATION = 'PriceChangeSimulationDM_2020'
//@Field String DM_SIMULATION = 'DMPriceChangeSimulation_2020'
@Field String TABLE_CONDITION_MASTER = 'ConditionMaster'

@Field Map PRICE_CHANGE_ON = [
        LandingPrice: 'Landing Price',
        GrossPrice  : 'Gross Price'
]

@Field Map PRICING_TYPES = [
        MARKUP           : 'Markup',
        NET_PRICE        : 'Net Price',
        PER_DISCOUNT     : 'Per Discount',
        ABSOULTE_DISCOUNT: 'Absolute Discount',
        X_ARTICLE        : 'XARTICLE',
        INTERNAL         : 'INTERNAL',
        NO_DISCOUNT      : 'No Discounts'
]

@Field Map PRICING_TYPE_LABELS = [
        MARKUP           : 'Markup',
        NET_PRICE        : 'Net Price',
        PER_DISCOUNT     : '% Discount',
        ABSOULTE_DISCOUNT: 'Absolute Discount',
        X_ARTICLE        : 'XARTICLE',
        INTERNAL         : 'INTERNAL',
        NO_DISCOUNT      : 'No Discounts'
]

@Field Map DISCOUNT_TYPES = [
        MARKUP           : 'Markup',
        NET_PRICE        : 'NetPrice',
        PER_DISCOUNT     : 'Percent',
        ABSOULTE_DISCOUNT: 'Absolute',
        X_ARTICLE        : 'XARTICLE',
        INTERNAL         : 'INTERNAL',
        NO_DISCOUNT      : 'NoDiscounts'
]

@Field Map CALCULATION_TYPES = [
        CURRENT  : 'Current',
        EXPECTED : 'Expected',
        RECOMMEND: 'Recommend',
        TEMPORARY: 'Temporary'
]

@Field Map CALCULATION_TYPE_COSTS = [
        (CALCULATION_TYPES.CURRENT)  : 'BaseCost',
        (CALCULATION_TYPES.EXPECTED) : 'ExpectedBaseCost',
        (CALCULATION_TYPES.RECOMMEND): 'ExpectedBaseCost'
]

@Field int ROUNDING_DECIMAL_PLACES = 8

@Field Map<String, Integer> DECIMAL_PLACE = [
        QUANTITY: 0,
        MONEY   : 3,
        PERCENT : 2,
        OTHERS  : 2
]

@Field final Map SUMMARY_COLORS = [
        BG_COLOR    : '#F8F8F8',
        BORDER_COLOR: '#fff',
        TITLE       : '#000',
        LABEL       : '#aaa',
        VALUE       : '#69bdd2',
        REC_BG_COLOR: '#DCF6D1',
]

@Field final Map WATERFALL_CONFIGURATION = [
        REVENUE: [
                TITLE       : 'Change in Revenue',
                Y_AXIS_LABEL: 'Revenue'],
        MARGIN : [
                TITLE       : 'Change in Customer Margin',
                Y_AXIS_LABEL: 'Margin']
]

@Field final Map QUERY_BUILDER_CONTEXT = [
        LEVEL3: 'Level3',
        LEVEL2: 'Level2',
        LEVEL1: 'Level1'
]

@Field Map INDICATOR = [positive: [value    : [prefix: "▲",
                                               suffix: ""],
                                   textColor: "#2e7d10",
                                   weight   : "bold"],
                        negative: [value    : [prefix: "▼",
                                               suffix: ""],
                                   textColor: "#f44340",
                                   weight   : "bold"],
                        equal   : [value    : [prefix: "▲",
                                               suffix: ""],
                                   textColor: "#cccccc",
                                   weight   : "bold"]]

@Field Map DATA_COLUMNS_DEF = [
        ARTICLE_NUMBER                                : [name: "Article Number", type: FieldFormatType.TEXT, alias: DM_COLUMNS.MATERIAL, dmColumn: DM_COLUMNS.MATERIAL, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_II                                      : [name: "Prod Hierarchy II", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODHII, dmColumn: DM_COLUMNS.PRODHII, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_III                                     : [name: "Prod Hierarchy III", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODHIII, dmColumn: DM_COLUMNS.PRODHIII, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_IV                                      : [name: "Prod Hierarchy IV", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODHIV, dmColumn: DM_COLUMNS.PRODHIV, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_V                                       : [name: "Prod Hierarchy V", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODHV, dmColumn: DM_COLUMNS.PRODHV, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_VI                                      : [name: "Prod Hierarchy VI", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODHVI, dmColumn: DM_COLUMNS.PRODHVI, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_VII                                     : [name: "Prod Hierarchy VII", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODVII, dmColumn: DM_COLUMNS.PRODHVII, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_CM1                                     : [name: "Prod Cm 1", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODH_CM1, dmColumn: DM_COLUMNS.PRODH_CM1, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_CM2                                     : [name: "Prod Cm 2", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODH_CM2, dmColumn: DM_COLUMNS.PRODH_CM2, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_CM3                                     : [name: "Prod Cm 3", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODH_CM3, dmColumn: DM_COLUMNS.PRODH_CM3, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        PRODH_CM4                                     : [name: "Prod Cm 4", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRODH_CM4, dmColumn: DM_COLUMNS.PRODH_CM4, isDisplayed: true, isDimension: true, isCalculated: false, isKey: true],
        SECONDARY_KEY                                 : [name: "Secondary Key", type: FieldFormatType.TEXT, alias: DM_COLUMNS.SECONDARY_KEY, dmColumn: DM_COLUMNS.SECONDARY_KEY, isDisplayed: false, isDimension: true, isCalculated: false, isKey: true],
        CUSTOMER_ID                                   : [name: "Customer Id", type: FieldFormatType.TEXT, alias: DM_COLUMNS.CUSTOMER_ID, dmColumn: DM_COLUMNS.CUSTOMER_ID, isDisplayed: false, isDimension: true, isCalculated: false, isKey: false],
        CONDITION                                     : [name: "Condition", type: FieldFormatType.TEXT, alias: DM_COLUMNS.CONDITION_TYPE, dmColumn: DM_COLUMNS.CONDITION_TYPE, isDisplayed: true, isDimension: true, isCalculated: false],
        YEAR                                          : [name: "Year", type: FieldFormatType.TEXT, alias: DM_COLUMNS.YEAR, dmColumn: DM_COLUMNS.YEAR, isDisplayed: true, isDimension: true, isCalculated: false],
        PRICING_TYPE                                  : [name: "Pricing Type", type: FieldFormatType.TEXT, alias: DM_COLUMNS.PRICING_TYPE, dmColumn: DM_COLUMNS.PRICING_TYPE, isDisplayed: true, isDimension: true, isCalculated: false],
        CONDITION_TABLE                               : [name: "Condition Table", type: FieldFormatType.TEXT, alias: DM_COLUMNS.CONDITION_TABLE, dmColumn: DM_COLUMNS.CONDITION_TABLE, isDisplayed: true, isDimension: true, isCalculated: false],
        TOTAL_QTY_SOLD                                : [name: "Total Quantity Sold", type: FieldFormatType.NUMERIC, alias: 'QtySold', dmColumn: DM_COLUMNS.QUANTITY, isDisplayed: true, isCalculated: false],
        TOTAL_REVENUE                                 : [name: "Total Revenue", type: FieldFormatType.MONEY, alias: 'TotalRevenue', dmColumn: DM_COLUMNS.TOTAL_REVENUE, isDisplayed: true, isCalculated: false],
        TOTAL_TRANSACTIONS                            : [name: "Total Rows Processed", type: FieldFormatType.MONEY, alias: "TotalTransactions", isDisplayed: true, isCalculated: true],
        GROSS_PRICE                                   : [name: "Average Gross Price", type: FieldFormatType.MONEY, alias: DM_COLUMNS.GROSS_PRICE, dmColumn: DM_COLUMNS.GROSS_PRICE, isDisplayed: true, isCalculated: false],
        DISCOUNT                                      : [name: "Average Discount", type: FieldFormatType.MONEY, alias: DM_COLUMNS.DISCOUNT, dmColumn: DM_COLUMNS.DISCOUNT, isDisplayed: false, isCalculated: false],
        MARKUP                                        : [name: "Average Markup", type: FieldFormatType.MONEY, alias: DM_COLUMNS.MARKUP, dmColumn: DM_COLUMNS.MARKUP, isDisplayed: false, isCalculated: false],
        LANDING_COST                                  : [name: "Landing Cost (ZPE)", type: FieldFormatType.MONEY, alias: DM_COLUMNS.LANDING_PRICE, dmColumn: DM_COLUMNS.LANDING_PRICE, isDisplayed: false, isCalculated: false],
        TOTAL_LANDING_COST                            : [name: "Total Landing Cost (ZPE)", type: FieldFormatType.MONEY, alias: DM_COLUMNS.LANDING_PRICE, dmColumn: DM_COLUMNS.LANDING_PRICE, isDisplayed: false, isCalculated: true],
        AVG_GROSS_PRICE                               : [name: "Txn Gross Price", type: FieldFormatType.MONEY, alias: DM_COLUMNS.AVERAGE_GROSS_PRICE, dmColumn: DM_COLUMNS.AVERAGE_GROSS_PRICE, isDisplayed: false, isCalculated: false],
        MARGIN                                        : [name: "Average Margin", type: FieldFormatType.MONEY, alias: DM_COLUMNS.MARGIN, dmColumn: DM_COLUMNS.MARGIN, isDisplayed: false, isCalculated: false],
        POCKET_PRICE                                  : [name: "Average Customer Price", type: FieldFormatType.MONEY, alias: DM_COLUMNS.POCKET_PRICE, dmColumn: DM_COLUMNS.POCKET_PRICE, isDisplayed: false, isCalculated: false],
        POCKET_MARGIN                                 : [name: "Average Customer Margin", type: FieldFormatType.MONEY, alias: DM_COLUMNS.POCKET_MARGIN, dmColumn: DM_COLUMNS.POCKET_MARGIN, isDisplayed: false, isCalculated: false],
        ZPL                                           : [name: "ZPL", type: FieldFormatType.MONEY, alias: DM_COLUMNS.ZPL, dmColumn: DM_COLUMNS.ZPL, isDisplayed: false, isCalculated: false],
        POCKET_MARGIN_PERCENT                         : [name: "Customer Margin %", type: FieldFormatType.PERCENT, alias: "PocketMarginPercent", isDisplayed: true, isCalculated: true],
        CURRENT_REVENUE                               : [name: "Current Revenue", type: FieldFormatType.MONEY, alias: "CurrentRevenue", isDisplayed: false, isCalculated: true],
        CURRENT_MARGIN                                : [name: "Current Margin", type: FieldFormatType.MONEY, alias: "CurrentMargin", isDisplayed: false, isCalculated: true],
        CURRENT_POCKET_MARGIN                         : [name: "Current Customer Margin", type: FieldFormatType.MONEY, alias: "CurrentPocketMargin", isDisplayed: false, isCalculated: true],
        TOTAL_POCKET_MARGIN                           : [name: "Total Customer Margin", type: FieldFormatType.MONEY, alias: "TotalPocketMargin", isDisplayed: false, isCalculated: true],
        EXPECTED_GROSS_PRICE                          : [name: "Expected Gross Price", type: FieldFormatType.MONEY, alias: "ExpectedGrossPrice", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_PRICE                         : [name: "Expected Customer Price", type: FieldFormatType.MONEY, alias: "ExpectedPocketPrice", isDisplayed: true, isCalculated: true],
        EXPECTED_REVENUE                              : [name: "Expected Revenue", type: FieldFormatType.MONEY, alias: "ExpectedRevenue", isDisplayed: true, isCalculated: true],
        EXPECTED_DISCOUNT                             : [name: "Expected Discount", type: FieldFormatType.MONEY, alias: "ExpectedDiscount", isDisplayed: true, isCalculated: true],
        EXPECTED_MARGIN_PER_UNIT                      : [name: "Expected Margin", type: FieldFormatType.MONEY, alias: "ExpectedMargin", isDisplayed: true, isCalculated: true],
        EXPECTED_MARGIN                               : [name: "Total Expected Margin", type: FieldFormatType.MONEY, alias: "ExpectedMargin", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PER_UNIT               : [name: "Expected Customer Margin", type: FieldFormatType.MONEY, alias: "ExpectedPocketMargin", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PERCENT                : [name: "Expected Customer Margin %", type: FieldFormatType.PERCENT, alias: "ExpectedPocketMarginPercent", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN                        : [name: "Total Expected Customer Margin", type: FieldFormatType.MONEY, alias: "TotalExpectedPocketMargin", isDisplayed: true, isCalculated: true],
        EXPECTED_LANDING_COST                         : [name: "Expected Landing Cost (ZPE)", type: FieldFormatType.MONEY, alias: "ExpectedBaseCost", isDisplayed: true, isCalculated: true],
        TOTAL_EXPECTED_LANDING_COST                   : [name: "Total Expected Landing Cost (ZPE)", type: FieldFormatType.MONEY, alias: "TotalExpectedBaseCost", isDisplayed: true, isCalculated: true],
        RECOMMENDED_GROSS_PRICE                       : [name: "Recommended Gross Price", type: FieldFormatType.MONEY, alias: "RecommendedGrossPrice", isDisplayed: false, isCalculated: true],
        RECOMMENDED_POCKET_PRICE                      : [name: "Recommended Customer Price", type: FieldFormatType.MONEY, alias: "RecommendedPocketPrice", isDisplayed: false, isCalculated: true],
        REVENUE_USING_RECOMMENDED_PRICE               : [name: "Expected Revenue With Recommended Price", type: FieldFormatType.MONEY, alias: "ExpectedRevenueWithRecommendedPrice", isDisplayed: false, isCalculated: true],
        REVENUE_DELTA_WITH_RECOMMENDED_PRICE          : [name: "Recommended Price Revenue Impact", type: FieldFormatType.MONEY, alias: "RecommendedPriceRevenueImpact", isDisplayed: false, isCalculated: true],
        EXPECTED_QTY_WITH_RECOMMENDED_PRICE           : [name: "Expected Quantity With Recommended Price", type: FieldFormatType.MONEY, alias: "ExpectedQuantityWithRecommendedPrice", isDisplayed: false, isCalculated: true],
        RECOMMENDED_MARGIN_PERCENT                    : [name: "Recommended Margin %", type: FieldFormatType.PERCENT, alias: "RecommendedMargin", isDisplayed: false, isCalculated: true],
        PRICE_CHANGE_PERCENT                          : [name: "Price Change %", type: FieldFormatType.PERCENT, alias: "ChangePercent", isDisplayed: false, isCalculated: true],
        POCKET_MARGIN_USING_RECOMMENDED_PRICE         : [name: "Customer Margin With Recommended Price", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginWithRecommendedPrice", isDisplayed: false, isCalculated: true],
        CURRENT_POCKET_PRICE_BY_MARKUP                : [name: "Current Customer Price (Markup)", type: FieldFormatType.MONEY, alias: "CurrentPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_REVENUE_BY_MARKUP                     : [name: "Current Revenue (Markup)", type: FieldFormatType.MONEY, alias: "CurrentRevenueMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_BY_MARKUP               : [name: "Current Pocket Margin (Markup)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_TOTAL_POCKET_MARGIN_BY_MARKUP         : [name: "Current Total Pocket Margin (Markup)", type: FieldFormatType.MONEY, alias: "CurrentTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_PERCENT_BY_MARKUP       : [name: "Current Pocket Margin Percent (Markup)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_PRICE_BY_NETPRICE              : [name: "Current Customer Price (NetPrice)", type: FieldFormatType.MONEY, alias: "CurrentPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_REVENUE_BY_NETPRICE                   : [name: "Current Revenue (NetPrice)", type: FieldFormatType.MONEY, alias: "CurrentRevenueMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_BY_NETPRICE             : [name: "Current Pocket Margin (NetPrice)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_TOTAL_POCKET_MARGIN_BY_NETPRICE       : [name: "Current Total Pocket Margin (NetPrice)", type: FieldFormatType.MONEY, alias: "CurrentTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_PERCENT_BY_NETPRICE     : [name: "Current Pocket Margin Percent (NetPrice)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_PRICE_BY_PERCENT               : [name: "Current Customer Price (Percent)", type: FieldFormatType.MONEY, alias: "CurrentPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_REVENUE_BY_PERCENT                    : [name: "Current Revenue (Percent)", type: FieldFormatType.MONEY, alias: "CurrentRevenueMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_BY_PERCENT              : [name: "Current Pocket Margin (Percent)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_TOTAL_POCKET_MARGIN_BY_PERCENT        : [name: "Current Total Pocket Margin (Percent)", type: FieldFormatType.MONEY, alias: "CurrentTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_PERCENT_BY_PERCENT      : [name: "Current Pocket Margin Percent (Percent)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_PRICE_BY_ABSOLUTE              : [name: "Current Customer Price (Absolute)", type: FieldFormatType.MONEY, alias: "CurrentPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_REVENUE_BY_ABSOLUTE                   : [name: "Current Revenue (Absolute)", type: FieldFormatType.MONEY, alias: "CurrentRevenueMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_BY_ABSOLUTE             : [name: "Current Pocket Margin (Absolute)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_TOTAL_POCKET_MARGIN_BY_ABSOLUTE       : [name: "Current Total Pocket Margin (Absolute)", type: FieldFormatType.MONEY, alias: "CurrentTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_PERCENT_BY_ABSOLUTE     : [name: "Current Pocket Margin Percent (Absolute)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_PRICE_BY_XARTICLE              : [name: "Current Customer Price (XArticle)", type: FieldFormatType.MONEY, alias: "CurrentPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_REVENUE_BY_XARTICLE                   : [name: "Current Revenue (XArticle)", type: FieldFormatType.MONEY, alias: "CurrentRevenueMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_BY_XARTICLE             : [name: "Current Pocket Margin (XArticle)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_TOTAL_POCKET_MARGIN_BY_XARTICLE       : [name: "Current Total Pocket Margin (XArticle)", type: FieldFormatType.MONEY, alias: "CurrentTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_PERCENT_BY_XARTICLE     : [name: "Current Pocket Margin Percent (XArticle)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_PRICE_BY_INTERNAL              : [name: "Current Customer Price (Internal)", type: FieldFormatType.MONEY, alias: "CurrentPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_REVENUE_BY_INTERNAL                   : [name: "Current Revenue (Internal)", type: FieldFormatType.MONEY, alias: "CurrentRevenueMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_BY_INTERNAL             : [name: "Current Pocket Margin (Internal)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_TOTAL_POCKET_MARGIN_BY_INTERNAL       : [name: "Current Total Pocket Margin (Internal)", type: FieldFormatType.MONEY, alias: "CurrentTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_PERCENT_BY_INTERNAL     : [name: "Current Pocket Margin Percent (Internal)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_PRICE_BY_NODISCOUNTS           : [name: "Current Customer Price (No Discounts)", type: FieldFormatType.MONEY, alias: "CurrentPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_REVENUE_BY_NODISCOUNTS                : [name: "Current Revenue (No Discounts)", type: FieldFormatType.MONEY, alias: "CurrentRevenueMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_BY_NODISCOUNTS          : [name: "Current Pocket Margin (No Discounts)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_TOTAL_POCKET_MARGIN_BY_NODISCOUNTS    : [name: "Current Total Pocket Margin (No Discounts)", type: FieldFormatType.MONEY, alias: "CurrentTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        CURRENT_POCKET_MARGIN_PERCENT_BY_NODISCOUNTS  : [name: "Current Pocket Margin Percent (No Discounts)", type: FieldFormatType.MONEY, alias: "CurrentPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],

        EXPECTED_POCKET_PRICE_BY_MARKUP               : [name: "Expected Customer Price (Markup)", type: FieldFormatType.MONEY, alias: "ExpectedPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_REVENUE_BY_MARKUP                    : [name: "Expected Revenue (Markup)", type: FieldFormatType.MONEY, alias: "ExpectedRevenueMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_BY_MARKUP              : [name: "Expected Pocket Margin (Markup)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_TOTAL_POCKET_MARGIN_BY_MARKUP        : [name: "Expected Total Pocket Margin (Markup)", type: FieldFormatType.MONEY, alias: "ExpectedTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PERCENT_BY_MARKUP      : [name: "Expected Pocket Margin Percent (Markup)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_PRICE_BY_NETPRICE             : [name: "Expected Customer Price (NetPrice)", type: FieldFormatType.MONEY, alias: "ExpectedPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_REVENUE_BY_NETPRICE                  : [name: "Expected Revenue (NetPrice)", type: FieldFormatType.MONEY, alias: "ExpectedRevenueMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_BY_NETPRICE            : [name: "Expected Pocket Margin (NetPrice)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_TOTAL_POCKET_MARGIN_BY_NETPRICE      : [name: "Expected Total Pocket Margin (NetPrice)", type: FieldFormatType.MONEY, alias: "ExpectedTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PERCENT_BY_NETPRICE    : [name: "Expected Pocket Margin Percent (NetPrice)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_PRICE_BY_PERCENT              : [name: "Expected Customer Price (Percent)", type: FieldFormatType.MONEY, alias: "ExpectedPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_REVENUE_BY_PERCENT                   : [name: "Expected Revenue (Percent)", type: FieldFormatType.MONEY, alias: "ExpectedRevenueMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_BY_PERCENT             : [name: "Expected Pocket Margin (Percent)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_TOTAL_POCKET_MARGIN_BY_PERCENT       : [name: "Expected Total Pocket Margin (Percent)", type: FieldFormatType.MONEY, alias: "ExpectedTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PERCENT_BY_PERCENT     : [name: "Expected Pocket Margin Percent (Percent)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_PRICE_BY_ABSOLUTE             : [name: "Expected Customer Price (Absolute)", type: FieldFormatType.MONEY, alias: "ExpectedPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_REVENUE_BY_ABSOLUTE                  : [name: "Expected Revenue (Absolute)", type: FieldFormatType.MONEY, alias: "ExpectedRevenueMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_BY_ABSOLUTE            : [name: "Expected Pocket Margin (Absolute)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_TOTAL_POCKET_MARGIN_BY_ABSOLUTE      : [name: "Expected Total Pocket Margin (Absolute)", type: FieldFormatType.MONEY, alias: "ExpectedTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PERCENT_BY_ABSOLUTE    : [name: "Expected Pocket Margin Percent (Absolute)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_PRICE_BY_XARTICLE             : [name: "Expected Customer Price (XArticle)", type: FieldFormatType.MONEY, alias: "ExpectedPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_REVENUE_BY_XARTICLE                  : [name: "Expected Revenue (XArticle)", type: FieldFormatType.MONEY, alias: "ExpectedRevenueMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_BY_XARTICLE            : [name: "Expected Pocket Margin (XArticle)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_TOTAL_POCKET_MARGIN_BY_XARTICLE      : [name: "Expected Total Pocket Margin (XArticle)", type: FieldFormatType.MONEY, alias: "ExpectedTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PERCENT_BY_XARTICLE    : [name: "Expected Pocket Margin Percent (XArticle)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_PRICE_BY_INTERNAL             : [name: "Expected Customer Price (Internal)", type: FieldFormatType.MONEY, alias: "ExpectedPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_REVENUE_BY_INTERNAL                  : [name: "Expected Revenue (Internal)", type: FieldFormatType.MONEY, alias: "ExpectedRevenueMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_BY_INTERNAL            : [name: "Expected Pocket Margin (Internal)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_TOTAL_POCKET_MARGIN_BY_INTERNAL      : [name: "Expected Total Pocket Margin (Internal)", type: FieldFormatType.MONEY, alias: "ExpectedTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PERCENT_BY_INTERNAL    : [name: "Expected Pocket Margin Percent (Internal)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_PRICE_BY_NODISCOUNTS          : [name: "Expected Customer Price (No Discounts)", type: FieldFormatType.MONEY, alias: "ExpectedPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_REVENUE_BY_NODISCOUNTS               : [name: "Expected Revenue (No Discounts)", type: FieldFormatType.MONEY, alias: "ExpectedRevenueMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_BY_NODISCOUNTS         : [name: "Expected Pocket Margin (No Discounts)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_TOTAL_POCKET_MARGIN_BY_NODISCOUNTS   : [name: "Expected Total Pocket Margin (No Discounts)", type: FieldFormatType.MONEY, alias: "ExpectedTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        EXPECTED_POCKET_MARGIN_PERCENT_BY_NODISCOUNTS    : [name: "Expected Pocket Margin Percent (No Discounts)", type: FieldFormatType.MONEY, alias: "ExpectedPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],

        RECOMMEND_POCKET_PRICE_BY_MARKUP              : [name: "Recommend Customer Price (Markup)", type: FieldFormatType.MONEY, alias: "RecommendPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_REVENUE_BY_MARKUP                   : [name: "Recommend Revenue (Markup)", type: FieldFormatType.MONEY, alias: "RecommendRevenueMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_BY_MARKUP             : [name: "Recommend Pocket Margin (Markup)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_TOTAL_POCKET_MARGIN_BY_MARKUP       : [name: "Recommend Total Pocket Margin (Markup)", type: FieldFormatType.MONEY, alias: "RecommendTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_PERCENT_BY_MARKUP     : [name: "Recommend Pocket Margin Percent (Markup)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_PRICE_BY_NETPRICE            : [name: "Recommend Customer Price (NetPrice)", type: FieldFormatType.MONEY, alias: "RecommendPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_REVENUE_BY_NETPRICE                 : [name: "Recommend Revenue (NetPrice)", type: FieldFormatType.MONEY, alias: "RecommendRevenueMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_BY_NETPRICE           : [name: "Recommend Pocket Margin (NetPrice)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_TOTAL_POCKET_MARGIN_BY_NETPRICE     : [name: "Recommend Total Pocket Margin (NetPrice)", type: FieldFormatType.MONEY, alias: "RecommendTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_PERCENT_BY_NETPRICE   : [name: "Recommend Pocket Margin Percent (NetPrice)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_PRICE_BY_PERCENT             : [name: "Recommend Customer Price (Percent)", type: FieldFormatType.MONEY, alias: "RecommendPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_REVENUE_BY_PERCENT                  : [name: "Recommend Revenue (Percent)", type: FieldFormatType.MONEY, alias: "RecommendRevenueMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_BY_PERCENT            : [name: "Recommend Pocket Margin (Percent)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_TOTAL_POCKET_MARGIN_BY_PERCENT      : [name: "Recommend Total Pocket Margin (Percent)", type: FieldFormatType.MONEY, alias: "RecommendTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_PERCENT_BY_PERCENT    : [name: "Recommend Pocket Margin Percent (Percent)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_PRICE_BY_ABSOLUTE            : [name: "Recommend Customer Price (Absolute)", type: FieldFormatType.MONEY, alias: "RecommendPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_REVENUE_BY_ABSOLUTE                 : [name: "Recommend Revenue (Absolute)", type: FieldFormatType.MONEY, alias: "RecommendRevenueMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_BY_ABSOLUTE           : [name: "Recommend Pocket Margin (Absolute)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_TOTAL_POCKET_MARGIN_BY_ABSOLUTE     : [name: "Recommend Total Pocket Margin (Absolute)", type: FieldFormatType.MONEY, alias: "RecommendTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_PERCENT_BY_ABSOLUTE   : [name: "Recommend Pocket Margin Percent (Absolute)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_PRICE_BY_XARTICLE            : [name: "Recommend Customer Price (XArticle)", type: FieldFormatType.MONEY, alias: "RecommendPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_REVENUE_BY_XARTICLE                 : [name: "Recommend Revenue (XArticle)", type: FieldFormatType.MONEY, alias: "RecommendRevenueMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_BY_XARTICLE           : [name: "Recommend Pocket Margin (XArticle)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_TOTAL_POCKET_MARGIN_BY_XARTICLE     : [name: "Recommend Total Pocket Margin (XArticle)", type: FieldFormatType.MONEY, alias: "RecommendTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_PERCENT_BY_XARTICLE   : [name: "Recommend Pocket Margin Percent (XArticle)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_PRICE_BY_INTERNAL            : [name: "Recommend Customer Price (Internal)", type: FieldFormatType.MONEY, alias: "RecommendPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_REVENUE_BY_INTERNAL                 : [name: "Recommend Revenue (Internal)", type: FieldFormatType.MONEY, alias: "RecommendRevenueMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_BY_INTERNAL           : [name: "Recommend Pocket Margin (Internal)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_TOTAL_POCKET_MARGIN_BY_INTERNAL     : [name: "Recommend Total Pocket Margin (Internal)", type: FieldFormatType.MONEY, alias: "RecommendTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_PERCENT_BY_INTERNAL   : [name: "Recommend Pocket Margin Percent (Internal)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_PRICE_BY_NODISCOUNTS         : [name: "Recommend Customer Price (No Discounts)", type: FieldFormatType.MONEY, alias: "RecommendPocketPriceMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_REVENUE_BY_NODISCOUNTS              : [name: "Recommend Revenue (No Discounts)", type: FieldFormatType.MONEY, alias: "RecommendRevenueMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_BY_NODISCOUNTS        : [name: "Recommend Pocket Margin (No Discounts)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_TOTAL_POCKET_MARGIN_BY_NODISCOUNTS  : [name: "Recommend Total Pocket Margin (No Discounts)", type: FieldFormatType.MONEY, alias: "RecommendTotalPocketMarginMarkup", isDisplayed: true, isCalculated: true],
        RECOMMEND_POCKET_MARGIN_PERCENT_BY_NODISCOUNTS: [name: "Recommend Pocket Margin Percent (No Discounts)", type: FieldFormatType.MONEY, alias: "RecommendPocketMarginPercentMarkup", isDisplayed: true, isCalculated: true]
]


@Field Map COLUMN_DATATYPES = DATA_COLUMNS_DEF.values().collectEntries { Map columnDef ->
    [(columnDef.name): columnDef.type]
}