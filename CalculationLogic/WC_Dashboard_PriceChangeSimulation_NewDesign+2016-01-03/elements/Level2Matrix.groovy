return api.getDatamartContext()
          .executeSqlQuery(
                  out.QueryBuilderInstance?.getAggregateByMaterialTxnSQLQuery(),
                  out.BaseDMQuery
          )?.toResultMatrix()