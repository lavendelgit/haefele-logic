package Dashboard_ClearingRebatesPerSalesOffice.elements

def customerSalesOffices = api.getElement("CustomersSalesOffices")
def clearingRebatesPerCustomer = api.getElement("ClearingRebatesPerCustomer")

return customerSalesOffices
        .groupBy { c -> c.attribute3 ?: "Undefined" }
        .collectEntries { salesOffice, customers ->
            [salesOffice, customers.collect { c -> clearingRebatesPerCustomer[c.customerId] ?: 0 }.sum()]
        }
        .sort { e1, e2 -> e1.key <=> e2.key }