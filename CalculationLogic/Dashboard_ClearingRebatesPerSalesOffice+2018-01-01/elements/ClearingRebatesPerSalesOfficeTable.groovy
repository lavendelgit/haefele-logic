package Dashboard_ClearingRebatesPerSalesOffice.elements

def resultMatrix = api.newMatrix("Sales Office","Count of Clearing Rebates")
resultMatrix.setTitle("Count of Clearing Rebates per Customer")
resultMatrix.setColumnFormat("Count of Clearing Rebates", FieldFormatType.NUMERIC)

def clearingRebatesPerSalesOffice = api.getElement("ClearingRebatesPerSalesOffice")

def total = 0;

for (row in clearingRebatesPerSalesOffice) {
    resultMatrix.addRow([
            "Sales Office" : row.key,
            "Count of Clearing Rebates" : row.value
    ])
    total += row.value
}

resultMatrix.addRow([
        "Sales Office": resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of Clearing Rebates": resultMatrix.styledCell(total).withCSS("font-weight: bold")
])

return resultMatrix
