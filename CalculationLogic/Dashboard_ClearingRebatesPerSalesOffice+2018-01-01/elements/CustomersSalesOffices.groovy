package Dashboard_ClearingRebatesPerSalesOffice.elements

def result = []
def customerFilter = api.getElement("CustomerFilter")

def stream = api.stream("C", null, ["customerId","attribute3"], customerFilter)

if (!stream) {
    return []
}

while (stream.hasNext()) {
    result.add(stream.next())
}

stream.close()

return result