package Dashboard_ClearingRebatesPerSalesOffice.elements

def filter = api.customerGroupEntry("Customer Filter")

return filter?.asFilter() ?: Filter.custom("1=1")