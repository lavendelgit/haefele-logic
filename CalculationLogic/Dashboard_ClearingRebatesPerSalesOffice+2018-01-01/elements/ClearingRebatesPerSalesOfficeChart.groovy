package Dashboard_ClearingRebatesPerSalesOffice.elements

def clearingRebatesPerSalesOffice = api.getElement("ClearingRebatesPerSalesOffice")

def categories = clearingRebatesPerSalesOffice.collect { e -> e.key}
def data = clearingRebatesPerSalesOffice.collect { e -> e.value};

def chartDef = [
        chart: [
            type: 'column'
        ],
        title: [
            text: 'Clearing Rebates per Sales Office'
        ],
        xAxis: [[
            title: [
                    text: "Sales Office"
            ],
            categories: categories
        ]],
        yAxis: [[
            title: [
                text: "Number of Clearing Rebates"
            ]
        ]],
        tooltip: [
            headerFormat: 'Sales Office: {point.key}<br/>',
            pointFormat: 'Number of Clearing Rebates: {point.y}'
        ],
        legend: [
            enabled: false
        ],
        series: [[
             data: data,
             dataLabels: [
                 enabled: true,
             ]
        ]]
]

api.buildFlexChart(chartDef)