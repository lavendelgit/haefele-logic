//api.logInfo("&&&&&&&&&&&&&&&&&& Exceptions.ExceptionMatrix &&&&&&&&&&",out.SimulationInputs?.Exceptions?.ExceptionMatrix)


List userConfiguredExceptions = out.SimulationInputs?.Exceptions?.ExceptionMatrix
Map userConfigurationAccessKeys = Constants.EXCEPTION_COLUMNS

return userConfiguredExceptions?.collect { Map exceptionEntry ->
    Library.getInitializedException(exceptionEntry[userConfigurationAccessKeys.MonthlyExceptionConfiguration.label],
            exceptionEntry[userConfigurationAccessKeys.ExceptionKey.label],
            true,
            exceptionEntry[userConfigurationAccessKeys.ExceptionFilter.label])
}


/*
&&&&&&&&&&&&&&&&&& Exceptions.ExceptionMatrix &&&&&&&&&&: 
[{Key=Key1, selected=false, Data Filter=Material = '000.07.115', Monthly Exception Configuration=Exception1}, 
{Key=key2, Data Filter=Material = '000.30.006', Monthly Exception Configuration=Exception2}]

 
******************* exceptionEntry[userConfigurationAccessKeys.MonthlyExceptionConfiguration.label]: Exception1
******************* exceptionEntry[userConfigurationAccessKeys.ExceptionKey.label]: Key1
******************* exceptionEntry[userConfigurationAccessKeys.ExceptionFilter.label]: Material = '000.07.115'
******************* exceptionEntry[userConfigurationAccessKeys.MonthlyExceptionConfiguration.label]: Exception2
******************* exceptionEntry[userConfigurationAccessKeys.ExceptionKey.label]: key2
******************* exceptionEntry[userConfigurationAccessKeys.ExceptionFilter.label]: Material = '000.30.006'

*/

//return (["Key":"1","Data Filter":"LOWER(Material) LIKE LOWER('%000.07.115%')","Monthly Exception Configuration":"Exception1"])