import net.pricefx.common.api.FieldFormatType

def resultMatrix
List internalArticleInputOptions = libs.HaefeleCommon.Simulation.getPriceChangeExceptionInputsReferenceName(libs.HaefeleCommon.Simulation.INPUT_CONFIGURATION_INTERNAL_ARTICLE)

resultMatrix = getResultMatrix(libs.HaefeleCommon.Simulation.INPUT_CONFIGURATION_SIMULATION, out.UserSelectedPercentageChangeInput, resultMatrix)
api.logInfo("+++++++++++++++++ out.SimulationInputs?.Exceptions ++++++",out.SimulationInputs?.Exceptions)
api.logInfo("+++++++++++++++++ out.SimulationInputs?.Exceptions?.ExceptionMatrix ++++++",out.SimulationInputs?.Exceptions?.ExceptionMatrix)
List userConfiguredExceptions = out.SimulationInputs?.Exceptions?.ExceptionMatrix
Map userConfigurationAccessKeys = Constants.EXCEPTION_COLUMNS

internalArticleInputOptions?.forEach { internalArticleInput->
    resultMatrix = getResultMatrix(libs.HaefeleCommon.Simulation.INPUT_CONFIGURATION_INTERNAL_ARTICLE, internalArticleInput, resultMatrix)
}
userConfiguredExceptions?.collect { Map exceptionEntry ->
  String exceptionName = exceptionEntry[userConfigurationAccessKeys.MonthlyExceptionConfiguration.label]
  resultMatrix = getResultMatrix("Exceptions Inputs", exceptionName, resultMatrix)
    
}


if (resultMatrix) {
    resultMatrix.setPreferenceName("Configuration Input List")

    resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("ConfigurationInputDetails-ED"))
            .withColValueAsEventDataAttr(Constants.RESULT_MATRIX_COLUMN_NAMES[0], "configuartionInputType")
            .withColValueAsEventDataAttr(Constants.RESULT_MATRIX_COLUMN_NAMES[1], "configuartionInputName")

}

return resultMatrix


protected ResultMatrix getResultMatrix(String configurationInputType, String configurationInputName, ResultMatrix resultMatrix) {

    def dashUtl = lib.DashboardUtility
    if (!resultMatrix) resultMatrix = dashUtl.newResultMatrix("Configuration Inputs (${out.SimulationName})", Constants.RESULT_MATRIX_COLUMN_NAMES, Constants.RESULT_MATRIX_COLUMN_TYPES)
    resultMatrix.addRow([
            (Constants.RESULT_MATRIX_COLUMN_NAMES[0]): configurationInputType,
            (Constants.RESULT_MATRIX_COLUMN_NAMES[1]): configurationInputName
    ])

    return resultMatrix
}
/*

UserSelectedPercentageChangeInputs : ERROR(@0): 
No signature of method: parsedscript_UserSelectedPercentageChangeInputs_s9NkJhB4NX.getResultMatrix() 
is applicable for argument types: (java.lang.String, java.util.ArrayList, net.pricefx.server.dto.calculation.ResultMatrix) 
values: [Exceptions Inputs, [null, null], ([Configuration Type, Configuration Name]),
([{Configuration Type=Simulation Inputs, Configuration Name=Retain nearly same margin as 2020}])]
*/