Map summaryData = api.local.SummaryData
if (!summaryData) {
    return
}

List seriesData = ChartUtils.getWaterfallSeriesData('Customer Margin', out.Year, summaryData.PocketMargin)

return ChartUtils.generateWaterfallChart(Constants.WATERFALL_CONFIGURATION.MARGIN.TITLE,
                                         Constants.WATERFALL_CONFIGURATION.MARGIN.Y_AXIS_LABEL,
                                         seriesData,
                                         true,
                                         'Customer Margin')
