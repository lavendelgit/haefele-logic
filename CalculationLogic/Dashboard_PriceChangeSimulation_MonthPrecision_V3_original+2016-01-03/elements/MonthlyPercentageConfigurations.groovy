if (out.UserSelectedPercentageChangeInput) {
    String configuartionInputType = "Simulation Inputs"
    String configuartionInputName = out.UserSelectedPercentageChangeInput

    api.dashboard("ConfigurationInputDetails")
            .setParam("Configuration Type", configuartionInputType)
            .setParam("Configuration Name", configuartionInputName)
            .showEmbedded()
            .andRecalculateOn(api.dashboardWideEvent("ConfigurationInputDetails-ED"))
            .withEventDataAttr("configuartionInputType").asParam("Configuration Type")
            .withEventDataAttr("configuartionInputName").asParam("Configuration Name")
}