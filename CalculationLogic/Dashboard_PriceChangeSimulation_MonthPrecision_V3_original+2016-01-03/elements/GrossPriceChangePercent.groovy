String grossPriceChangePercentField = Constants.PRICE_SIMULATION_FIELDS[1]

return Library.getPerMonthPercentageValue(out.SimulationPercentageChangeInput, grossPriceChangePercentField)