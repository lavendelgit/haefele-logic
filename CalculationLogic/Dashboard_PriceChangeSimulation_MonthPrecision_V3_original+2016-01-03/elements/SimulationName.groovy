String simulationName = out.SimulationInputs?.SimulationName

if (simulationName == null) {
    api.abortCalculation()
}

return simulationName