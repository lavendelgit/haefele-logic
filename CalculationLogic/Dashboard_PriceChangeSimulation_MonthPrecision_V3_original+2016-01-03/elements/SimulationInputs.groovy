def params = [:]
//params.put("SimulationName", "Simulation 2")

return inlineConfiguratorEntry("Configurator_AllSimulationInputs", "SimulationInputConfigurator", "Simulation Input Configurator", params)

Object inlineConfiguratorEntry(String configuratorLogic, String configuratorName, String label, Map params) {
    if (!configuratorName || !configuratorLogic) {
        api.throwException("Configurator name / logic not specified")
    }

    def configurator = api.inlineConfigurator(configuratorName, configuratorLogic)

    def conf = api.getParameter(configuratorName)
    if (conf != null && conf.getValue() == null) {
        params["configurator"] = configuratorName
        conf.setLabel(label)
        conf.setValue(params)
    }
    return configurator
}