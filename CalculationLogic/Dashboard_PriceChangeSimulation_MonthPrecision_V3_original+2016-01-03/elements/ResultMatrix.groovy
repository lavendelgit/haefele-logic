import net.pricefx.formulaengine.scripting.Matrix2D

Matrix2D resultData = api.getDatamartContext().executeQuery(QueryBuilderInstance.getSQLQuery())?.getData()

api.local.MatrixData = resultData
net.pricefx.server.dto.calculation.ResultMatrix resultMatrix = null
if (resultData && resultData.size() > 0) {
    if (out.ComputeResultMatrix) {
        resultMatrix = resultData.toResultMatrix()
        resultMatrix.setEnableClientFilter(true)
        resultMatrix.setDisableSorting(false)
        resultMatrix.setTitle("Monthly Price Change Simulation Results")
        resultMatrix.setPreferenceName("DashboardPriceChangeSimulationMatrixMonthly")
        extractSummaryData(resultMatrix)
    } else {
        extractSummaryData(resultData.toResultMatrix())
    }
} else {
    libs.__LIBRARY__.TraceUtility.developmentTrace('Alternateeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee executed', "Expecting this..")
    Map DUMMY_VALUES_OVERALL = ['actual'                : 0.0,
                                'current'               : 0.0,
                                'expected'              : 0.0,
                                'actualInMillion'       : "0.000 M",
                                'currentDelta'          : 0.00,
                                'currentDeltaInMillion' : "0.000 M",
                                'currentDeltaPercent'   : 0.00,
                                'currentInMillion'      : "0.000 M",
                                'currentChangePercent'  : 0.0,
                                'expectedDelta'         : 0.0,
                                'expectedDeltaInMillion': "0.000 M",
                                'expectedDeltaPercent'  : 0.0,
                                'expectedInMillion'     : "0.000 M",
                                'expectedChangePercent' : 0.0]
    Map DUMMY_BREAKUP_REVENUE = ['totalRevenue'         : 0,
                                 'totalMargin'          : 0,
                                 'totalRevenueInMillion': "0.000 M",
                                 'totalMarginInMillion' : "0.000 M",
                                 'marginPercent'        : 0]
    Map DUMMY_PERCENTAGE_CHANGE = ['January'  : 0.0,
                                   'February' : 0.0,
                                   'March'    : 0.0,
                                   'April'    : 0.0,
                                   'May'      : 0.0,
                                   'June'     : 0.0,
                                   'July'     : 0.0,
                                   'August'   : 0.0,
                                   'September': 0.0,
                                   'October'  : 0.0,
                                   'November' : 0.0,
                                   'December' : 0.0]
    Map DUMMY_EXPECTED_BREAKUP = ['totalRevenue'          : 0,
                                  'totalMargin'           : 0,
                                  'totalRevenueIdeal'     : 0,
                                  'totalMarginIdeal'      : 0,
                                  'priceChangePercent'    : DUMMY_PERCENTAGE_CHANGE,
                                  'costChangePercent'     : DUMMY_PERCENTAGE_CHANGE,
                                  'totalRevenueIdealDelta': 0,
                                  'totalMarginIdealDelta' : 0,
                                  'totalRevenueInMillion' : "0.000 M",
                                  'totalMarginInMillion'  : "0.000 M",
                                  'marginPercent'         : 0]

    api.local.SummaryData = ['totalTransactions': 0,
                             'totalQuantity'    : ['actual': 0, 'expected': 0],
                             'totalMaterials'   : 0,
                             'Revenue'          : DUMMY_VALUES_OVERALL,
                             'Cost'             : DUMMY_VALUES_OVERALL,
                             'PocketMargin'     : DUMMY_VALUES_OVERALL,
                             'CURRENT_BREAKUP'  : ['Markup'     : DUMMY_BREAKUP_REVENUE,
                                                   'NetPrice'   : DUMMY_BREAKUP_REVENUE,
                                                   'PerDiscount': DUMMY_BREAKUP_REVENUE,
                                                   'AbsDiscount': DUMMY_BREAKUP_REVENUE,
                                                   'XARTICLE'   : DUMMY_BREAKUP_REVENUE,
                                                   'INTERNAL'   : DUMMY_BREAKUP_REVENUE,
                                                   'NoDiscounts': DUMMY_BREAKUP_REVENUE],
                             'EXPECTED_BREAKUP' : ['Markup'     : DUMMY_EXPECTED_BREAKUP,
                                                   'NetPrice'   : DUMMY_EXPECTED_BREAKUP,
                                                   'PerDiscount': DUMMY_EXPECTED_BREAKUP,
                                                   'AbsDiscount': DUMMY_EXPECTED_BREAKUP,
                                                   'XARTICLE'   : DUMMY_EXPECTED_BREAKUP,
                                                   'INTERNAL'   : DUMMY_EXPECTED_BREAKUP,
                                                   'NoDiscounts': DUMMY_EXPECTED_BREAKUP]]
}

return resultMatrix

protected void extractSummaryData(net.pricefx.server.dto.calculation.ResultMatrix resultMatrix) {
    Map summaryData = [:]
    List entries = resultMatrix?.getEntries()
    if (entries) {
        Map level1 = Constants.DB_FIELDS
        summaryData.totalTransactions = 0
        summaryData.totalQuantity = [actual  : getSummationAcrossMonths(level1.TOTAL_QUANTITY_SOLD, entries),
                                     expected: getSummationAcrossMonths(level1.EXPECTED_QUANTITY_SOLD, entries)]
        summaryData.totalMaterials = entries.size()
        addSeriesAttributes(summaryData, entries, 'Revenue', level1.TOTAL_REVENUE, level1.TOTAL_REVENUE, level1.EXPECTED_REVENUE)
        addSeriesAttributes(summaryData, entries, 'Cost', level1.TOTAL_BASE_COST, level1.TOTAL_BASE_COST, level1.EXPECTED_TOTAL_BASE_COST)
        addSeriesAttributes(summaryData, entries, 'PocketMargin', level1.CURRENT_POCKET_MARGIN, level1.CURRENT_POCKET_MARGIN, level1.EXPECTED_TOTAL_MARGIN)

        Constants.PRICING_TYPES_CONST.each { String discountTypeCode, String pricingType ->
            addBreakupByDiscountTypeSummary(summaryData, entries, pricingType, "Current")
            addBreakupByDiscountTypeSummary(summaryData, entries, pricingType, "Expected")
        }
    }
    api.local.SummaryData = summaryData
    libs.__LIBRARY__.TraceUtility.developmentTrace("Summary Data formed is printed", api.jsonEncode(summaryData))
}

protected BigDecimal getSummationAcrossMonths(String field, List dataSets) {
    BigDecimal totalValue = BigDecimal.ZERO

    Constants.MONTHS_PREFIXES.each { String prefix ->
        totalValue += (dataSets["$prefix$field"]?.sum {
            it ?: BigDecimal.ZERO
        } as BigDecimal)
    }

    return totalValue
}

protected void addSeriesAttributes(Map summaryData,
                                   List entries,
                                   String seriesName,
                                   String actualColumnName,
                                   String currentColumnName,
                                   String expectedColumnName) {
    summaryData[seriesName] = [actual  : getSummationAcrossMonths(actualColumnName, entries),
                               current : getSummationAcrossMonths(currentColumnName, entries),
                               expected: getSummationAcrossMonths(expectedColumnName, entries)]

    addCalculatedAttributes(summaryData[seriesName])
}

protected void addCalculatedAttributes(Map seriesSummaryData) {
    def mathUtils = libs.__LIBRARY__.MathUtility
    seriesSummaryData.actualInMillion = toMillion(seriesSummaryData.actual)
    seriesSummaryData.currentDelta = (seriesSummaryData.current ?: 0) - (seriesSummaryData.actual ?: 0)
    seriesSummaryData.currentDeltaInMillion = toMillion(seriesSummaryData.currentDelta)
    seriesSummaryData.currentDeltaPercent = mathUtils.divide((seriesSummaryData.current - seriesSummaryData.actual), seriesSummaryData.actual) * 100
    seriesSummaryData.currentInMillion = toMillion(seriesSummaryData.current)
    seriesSummaryData.currentChangePercent = mathUtils.divide(seriesSummaryData.current, seriesSummaryData.actual) * 100
    seriesSummaryData.expectedDelta = (seriesSummaryData.expected ?: 0) - (seriesSummaryData.current ?: 0)
    seriesSummaryData.expectedDeltaInMillion = toMillion(seriesSummaryData.expectedDelta)
    seriesSummaryData.expectedDeltaPercent = mathUtils.divide((seriesSummaryData.expected - seriesSummaryData.current), seriesSummaryData.current) * 100
    seriesSummaryData.expectedInMillion = toMillion(seriesSummaryData.expected)
    seriesSummaryData.expectedChangePercent = mathUtils.divide(seriesSummaryData.expected, seriesSummaryData.current) * 100
}

protected String toMillion(moneyValue) {
    return (moneyValue != null) ? libs.SharedLib.RoundingUtils.round((moneyValue / 1000000), Constants.DECIMAL_PLACE.MONEY) + ' M' : ''
}

protected void addBreakupByDiscountTypeSummary(Map summaryData, List entries, String pricingType, String outputType) {
    Map level1 = Constants.DB_FIELDS
    String upperCaseOutputType = outputType.toUpperCase()
    String revenueColumnPrefix = level1["PT_${upperCaseOutputType}_CUSTOMER_REVENUE"]
    //CURRENT_CUSTOMER_REVENUE or EXPECTED_CUSTOMER_REVENUE
    String revenueColumnName = "$revenueColumnPrefix$pricingType"
    String marginColumnPrefix = level1["PT_${upperCaseOutputType}_TOTAL_MARGIN"]
    //PT_CURRENT_TOTAL_MARGIN or PT_EXPECTED_TOTAL_MARGIN
    String marginColumnName = "$marginColumnPrefix$pricingType"
    if (revenueColumnName && marginColumnName) {
        Map sectionMap = summaryData[upperCaseOutputType + "_BREAKUP"] ?: [:]
        sectionMap[pricingType] = [totalRevenue: getSummationAcrossMonths(revenueColumnName, entries),
                                   totalMargin : getSummationAcrossMonths(marginColumnName, entries)]
        Map discountTypeMap = sectionMap[pricingType]

        if (upperCaseOutputType == "EXPECTED") {
            libs.__LIBRARY__.TraceUtility.developmentTrace("The current think which is getting executed.. =====> ", upperCaseOutputType)
            String currentRevenueColumnName = level1["PT_CURRENT_CUSTOMER_REVENUE"] + pricingType
            BigDecimal currentTotalRevenue = summaryData["CURRENT_BREAKUP"][pricingType]?.totalRevenue
            BigDecimal currentTotalMargin = summaryData["CURRENT_BREAKUP"][pricingType]?.totalMargin
            discountTypeMap.totalRevenueIdeal = currentTotalRevenue
            discountTypeMap.totalMarginIdeal = currentTotalMargin
            if (out.GrossPriceChangePercent) {
                Map priceChangePercent = getEffectivePercentageValues(out.GrossPriceChangePercent)
                libs.__LIBRARY__.TraceUtility.developmentTrace("Percentage for revenue changes =====> ", api.jsonEncode(priceChangePercent))
                BigDecimal revenueDelta = getPercentageChangeAmount(currentRevenueColumnName, priceChangePercent, entries)
                discountTypeMap.totalRevenueIdeal = currentTotalRevenue + revenueDelta
                discountTypeMap.totalMarginIdeal = currentTotalMargin + revenueDelta
                discountTypeMap.priceChangePercent = priceChangePercent
            }
            if (out.LandingPriceChangePercent) {
                String currentMarginColumnName = level1["PT_CURRENT_TOTAL_MARGIN"] + pricingType
                Map priceChangePercent = getEffectivePercentageValues(out.LandingPriceChangePercent)
                libs.__LIBRARY__.TraceUtility.developmentTrace("Percentage for margin changes =====> ", api.jsonEncode(priceChangePercent))
                BigDecimal marginDelta = getPercentageChangeAmount(currentMarginColumnName, priceChangePercent, entries)
                discountTypeMap.totalMarginIdeal += marginDelta
                discountTypeMap.costChangePercent = priceChangePercent
            }
            discountTypeMap.totalRevenueIdealDelta = (discountTypeMap.totalRevenue ?: 0) - (discountTypeMap.totalRevenueIdeal ?: 0)
            discountTypeMap.totalMarginIdealDelta = (discountTypeMap.totalMargin ?: 0) - (discountTypeMap.totalMarginIdeal ?: 0)
        }

        discountTypeMap.totalRevenueInMillion = toMillion(discountTypeMap.totalRevenue)
        discountTypeMap.totalMarginInMillion = toMillion(discountTypeMap.totalMargin)
        discountTypeMap.marginPercent = discountTypeMap.totalRevenue ? (discountTypeMap.totalMargin / discountTypeMap.totalRevenue) : 0

        summaryData[upperCaseOutputType + "_BREAKUP"] = sectionMap
    }
}

protected Map getEffectivePercentageValues(Map percentageChanges) {
    BigDecimal percentageTillDate = BigDecimal.ZERO
    Map updatedPercentageMap = Constants.MONTH_NAMES.collectEntries { String month ->
        percentageTillDate += percentageChanges[month]
        return [(month): percentageTillDate]
    }
    //api.trace(" The month values....", api.jsonEncode(updatedPercentageMap))

    return updatedPercentageMap
}

protected BigDecimal getPercentageChangeAmount(String field, Map monthlyPercentChange, List dataSets) {
    BigDecimal totalValue = BigDecimal.ZERO
    BigDecimal currentPercent
    Constants.MONTHS_PREFIXES.each { String prefix ->
        currentPercent = monthlyPercentChange[Constants.MONTHS_PREFIXES_NAMES[prefix]] ?: BigDecimal.ZERO
        totalValue += currentPercent * extactValueFromContainer(dataSets, "$prefix$field")
    }
//    api.trace("The Change amount for dataSets" + field, totalValue+ "")

    return totalValue
}

protected BigDecimal extactValueFromContainer(List dataSet, String key) {
    if (!dataSet) {
        return BigDecimal.ZERO
    }
    return dataSet[key]?.sum {
        it ?: BigDecimal.ZERO
    } as BigDecimal
}

