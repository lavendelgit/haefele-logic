def dmQueryResult = api.getElement("SalesHistoryDM")
def dmData = dmQueryResult?.getData()
def elasticityData = []

if (dmData) {
    def i = 0
    elasticityData = dmData.collect {
        row -> [
            x: row.price?.toBigDecimal(),
            y: row.quantity?.toBigDecimal(),
            name: row.month,
            marker: [
                    enabled: true
            ]
        ]
    }
    .sort { r1, r2 -> r1.x <=> r2.x }
}

api.trace("elasticityData", "", elasticityData)

return elasticityData