def elasticityData = api.getElement("ElasticityData")
def regressionCoefficients = api.getElement("RegressionCoefficients")
def step = api.getElement("Step")
def scale = api.getElement("Scale")

def lra = regressionCoefficients.lra
def lrb = regressionCoefficients.lrb

if (elasticityData.size() < 1) {
    return;
}

def function = { x -> lrb * x + lra }
def startX = elasticityData.first().x.setScale(scale, BigDecimal.ROUND_DOWN) - step
def endX = elasticityData.last().x + step

def regressionData = lib.Line.calculatePoints(function, startX, endX, step).collect {
    p -> p + [
            name: "-",
            marker: [
                    enabled: false
            ]
    ]
}

api.trace("regressionData", "", regressionData)

return regressionData
