def elasticityData = api.getElement("ElasticityData")
def step = api.getElement("Step")
def scale = api.getElement("Scale")

def interpolatedData = []

if (elasticityData.size() > 0) {
    interpolatedData.push(elasticityData[0])
}

for (def i = 1; i < elasticityData.size(); i++) {
    def start = elasticityData[i-1]
    def end = elasticityData[i]
    if (start.x == end.x) {
        interpolatedData.push(end);
        continue;
    }
    def a = (end.y - start.y)/(end.x - start.x)
    def b = start.y - a * start.x
    def function = { x -> a * x + b }
    def startX = start.x.setScale(scale, BigDecimal.ROUND_UP) + step;
    def endX = end.x - step
    interpolatedData.addAll(lib.Line.calculatePoints(function, startX, endX, step).collect {
        p -> p + [
            name: "-",
            marker: [
                enabled: false
            ]
        ]
    })
    interpolatedData.push(end);
}

api.trace("interpolatedData", "", interpolatedData)

return interpolatedData