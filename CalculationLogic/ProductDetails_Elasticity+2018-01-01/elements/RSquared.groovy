def dmQueryResult = api.getElement("SalesHistoryDM")

def ols = dmQueryResult?.getData()
        ?.linearRegression()
        ?.usingDependentVariable("quantity")
        ?.usingIndependentVariables(["price"])
        ?.run()

api.trace("R2", "", ols?.r2)
api.trace("intercept", "", ols?.intercept)
api.trace("coefficients", "", ols?.coefficients?.toList())

return ols?.r2