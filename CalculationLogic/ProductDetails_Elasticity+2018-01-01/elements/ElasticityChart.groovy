def interpolatedData = api.getElement("InterpolatedData")
def regressionData = api.getElement("RegressionData")
def rSquared = api.getElement("RSquared")
def regressionCoefficients = api.getElement("RegressionCoefficients")
def lra = regressionCoefficients.lra
def lrb = regressionCoefficients.lrb

def regressionEquation(lra, lrb) {
    if (lra == null || lrb == null) {
        return ""
    }
    return "y = " + lra + (lrb < 0 ? " - " : " + ") + Math.abs(lrb) + "x"
}

def rSquaredFormatted(value) {
    if (value == null) {
        return ""
    }
    return "R²=" + api.formatNumber("#.##%", value)
}

def chartDef = [
    chart: [
        type: 'line',
        zoomType: 'xy'
    ],
    title: [
        text: 'Elasticity'
    ],
    subtitle: [
        text: api.product("sku") + " " + api.product("label")
    ],
    xAxis: [[
        type: 'linear',
        title: [
            text: "Price"
        ]
    ]],
    yAxis: [[
        title: [
            text: "Units Sold"
        ]
    ]],
    tooltip: [
        headerFormat: "",
        pointFormat: "Units Sold: {point.y:.2f}<br>Price: {point.x:.2f}<br>Month: {point.name}",
        crosshairs: [[width: 1]]
    ],
    series: [[
            name: "Elasticity",
            data: interpolatedData
        ], [
            name: "Linear Regression " + regressionEquation(lra,lrb) + ", " + rSquaredFormatted(rSquared),
            data: regressionData,
            color: "#AA4643"
    ]]
]

api.buildFlexChart(chartDef)