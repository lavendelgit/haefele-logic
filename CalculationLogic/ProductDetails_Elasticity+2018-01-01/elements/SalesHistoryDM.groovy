def sku = api.product("sku")
def filters = [
    *lib.Filter.customersGermany()
]

return lib.Find.priceElasticity(sku, *filters)