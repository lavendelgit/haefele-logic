def step = api.getElement("Step") ?: 0.1
def scale = (int) -Math.log10(step); // 0.1 -> 1, 10 -> -1

return scale