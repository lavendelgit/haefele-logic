def dmQueryResult = api.getElement("SalesHistoryDM")
def summary = dmQueryResult?.getSummary()

def lra = summary?.projections?.quantity?.LRa
def lrb = summary?.projections?.price?.LRb

api.trace("LRa", "", lra)
api.trace("LRb", "", lrb)

return [
    lra: lra,
    lrb: lrb
]