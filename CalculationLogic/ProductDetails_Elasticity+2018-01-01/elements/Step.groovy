def elasticityData = api.getElement("ElasticityData")
def step = 0.1

if (elasticityData.size() > 1) {
    def xRange = elasticityData.last().x - elasticityData.first().x
    step = Math.pow(10, Math.floor(Math.log10(xRange)) - 1) // step is 1 order of magnitude less than xRange
}

if (step == 0) {
    step = 0.1
}

return step