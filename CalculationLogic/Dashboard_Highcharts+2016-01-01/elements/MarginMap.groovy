def cities = api.getElement("Cities")
def data = []
def compare = []
api.local.data.forEach{p->
  def i = [:]
  i.city = p.city
  i.lat = p.lat
  i.lon = p.lon
  i.z = p.margin
  if(cities && cities.contains(p.city)){
    compare.push(i)
  }else{
    data.push(i)
  }
}

def definition = Util.buildDefinition('Margin',data,compare,'#507cff','€')
def chart = api.buildHighmap(definition)
return chart
