def monthFrom = api.getElement("FromMonth")
def monthTo = api.getElement("ToMonth")
def query = api.local.dmCtx.newQuery(api.local.dm)
query.select("CustomerId","customerId")
query.select("Revenue","revenue")
query.select("MarginalContributionAbs","margin")
query.select("UnitsSold","unitsSold")
query.setOptions(["currency":"EUR"])
def select=["SUM(A.revenue) AS revenue","SUM(A.margin) AS margin","SUM(A.unitsSold) AS unitsSold","COUNT(DISTINCT A.customerId) AS customers"]
api.local.select = select.join(",")
def filters = []

if(monthFrom){
  filters.add(Filter.greaterOrEqual("YearMonthMonth",monthFrom))
}
if(monthTo){
  filters.add(Filter.lessOrEqual("YearMonthMonth",monthTo))
}
query.where(*filters)
def result = api.local.dmCtx.executeQuery(query)
if(result){
  api.local.tableContext.createTable(api.local.dmSalesTemp, result)
  api.local.customerList = result.getData()?.collect{
    it.customerId
  }
}