def lookupTableId = api.findLookupTable(api.local.latLong)?.id
def fields = ["name","attribute1","attribute2"]
api.local.tableContext.createTableFromLookupTable(api.local.ppLatLongTemp, lookupTableId, fields)
def filters = [Filter.equal("lookupTable.id",lookupTableId),
              Filter.in("name",api.local.cityList)
             ]
api.trace("filter",filters)
def result
def data = []

try{
  result = api.stream("MLTV","name",fields,*filters)
  if(result){
    while(result.hasNext()){
      def item = result.next() as Map
      data.add(item)
    }
    api.local.tableContext.loadRows(api.local.ppLatLongTemp,data)
  }
}
catch(e){
  api.trace("exception",e.getMessage())
}
finally{
  result?.close()
}