def matrix = api.newMatrix("City",
                           "Revenue", 
                           "Margin", 
                           "Units Sold",					
                           "Customers"               
                          )
matrix.setColumnFormat("City",FieldFormatType.TEXT)
matrix.setColumnFormat("Revenue",FieldFormatType.MONEY_EUR)
matrix.setColumnFormat("Margin",FieldFormatType.MONEY_EUR)
matrix.setColumnFormat("Units Sold",FieldFormatType.NUMERIC)
matrix.setColumnFormat("Customers",FieldFormatType.NUMERIC)
matrix.setEnableClientFilter(true)
matrix.setTitle("Sales Data per City")
api.local.data.each{row->
  def calculationRow = [:]
  calculationRow.put("City",row.city)
  calculationRow.put("Revenue",row.revenue)
  calculationRow.put("Margin",row.margin)
  calculationRow.put("Units Sold",row.unitsSold)
  calculationRow.put("Customers",row.customers)
  matrix.addRow(calculationRow)
}
return matrix