def cities = api.getElement("Cities")
def data = []
def compare = []
api.local.data.forEach{p->
  def i = [:]
  i.city = p.city
  i.lat = p.lat
  i.lon = p.lon
  i.z = p.customers
  if(cities && cities.contains(p.city)){
    compare.push(i)
  }else{
    data.push(i)
  }
}

def definition = Util.buildDefinition('Customers',data,compare,'#ad1a8b','')
def chart = api.buildHighmap(definition)
return chart
