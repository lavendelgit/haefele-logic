def cities = api.getElement("Cities")
def data = []
def compare = []
api.local.data.forEach{p->
  def i = [:]
  i.city = p.city
  i.lat = p.lat
  i.lon = p.lon
  i.z = p.revenue
  if(cities && cities.contains(p.city)){
    compare.push(i)
  }else{
    data.push(i)
  }
}

def definition = Util.buildDefinition('Revenue',data,compare,'#278722','€')
def chart = api.buildHighmap(definition)
return chart
