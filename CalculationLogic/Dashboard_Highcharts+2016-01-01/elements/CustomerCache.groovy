def fields = ["customerId","attribute1","attribute2"]
api.local.tableContext.createTableFromCustomerExtension(api.local.ceCustCityTemp, api.local.custExt, fields)
def filters = [Filter.equal("name",api.local.custExt),
              Filter.in("customerId",api.local.customerList)
             ]
def result
def data = []
try{
  result = api.stream("CX","attribute2",fields,*filters)
  if(result){
    while(result.hasNext()){
      def item = result.next() as Map
      data.add(item)
    }
    api.local.cityList = data.collect{
      it.attribute2
    }
    api.local.tableContext.loadRows(api.local.ceCustCityTemp,data)
    
  }
}
catch(e){
  api.trace("exception",e.getMessage())
}
finally{
  result?.close()
}
api.local.cityList = api.local.cityList.unique()