def month = api.local.dm.getColumnByName("YearMonthMonth")

def monthList = api.local.dmCtx.dimFilterEntry("To-Month", month)
if(monthList!=null){
  return monthList?.value
}