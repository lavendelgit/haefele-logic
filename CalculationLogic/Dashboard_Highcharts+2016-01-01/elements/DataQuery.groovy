if(api.isSyntaxCheck()) return
def query = "SELECT $api.local.select, "+
  "C.name as city, C.attribute1 as lat, C.attribute2 as lon "+
  "FROM $api.local.dmSalesTemp A, $api.local.ceCustCityTemp B, $api.local.ppLatLongTemp C "+
  "WHERE A.customerId=B.customerId AND B.attribute2=C.name "+
  "GROUP BY C.name"
api.trace("query",query)
def data = api.local.tableContext.executeQuery(query).toResultMatrix()
api.local.data = data?.entries