def cities = api.getElement("Cities")
def data = []
def compare = []
api.local.data.forEach{p->
  def i = [:]
  i.city = p.city
  i.lat = p.lat
  i.lon = p.lon
  i.z = p.unitsSold
  if(cities && cities.contains(p.city)){
    compare.push(i)
  }else{
    data.push(i)
  }
}

def definition = Util.buildDefinition('Units Sold',data,compare,'#2a22c7','')
def chart = api.buildHighmap(definition)
return chart
