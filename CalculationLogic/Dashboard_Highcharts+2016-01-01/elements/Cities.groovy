def id = api.findLookupTable(api.local.latLong)?.id
def cities = api.find("MLTV", 0, 0, "name", ["name"], true, Filter.equal("lookupTable.id",id)).name
//change to stream, when we have more than 200 cities
return api.options("Select Cities to compare", cities)