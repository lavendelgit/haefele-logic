def buildDefinition(measure, data, compare, color, currency){
  def definition = [
    chart: [map: 'countries/de/de-all'],
    title: [
      text: measure+' per City'
    ],

    tooltip: [
      pointFormat: '{point.city}<br>' +
      measure+': '+currency+'{point.z}'
    ],

    series: [[
      name: 'Basemap',
      borderColor: '#606060',
      nullColor: 'rgba(200, 200, 200, 0.2)',
      showInLegend: false
    ], [
      type: 'mapbubble',
      dataLabels: [
        enabled: false,
        format: '{point.city}'
      ],
      name: 'Cities',
      data: data,
      maxSize: '15%',
      color: color
    ], [
      type: 'mapbubble',
      dataLabels: [
        enabled: true,
        format: '{point.city}<br>'+currency+'{point.z}',
        color: 'black',
        align: 'left',
        x: 10,
        verticalAlign: 'middle'
      ],
      name: 'Selected',
      data: compare,
      maxSize: '15%',
      color: '#b02027'
    ]]
  ]
  return definition
}