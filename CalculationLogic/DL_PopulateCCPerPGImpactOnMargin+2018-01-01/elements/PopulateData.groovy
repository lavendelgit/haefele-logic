/**
 * This class contains the algorithm and which gets the data from transaction. It aggregates two types of data i.e. one at customer summary level and another detail level data
 * to be processed to record impact of wrong categorization in the different level.
 */
def fetchDataFromTransaction(ctx, customerId) {
    def dsCCIoM = ctx.getRollup("DL_CCTransactionImpactPerProGroup")
    def detailCCDQuery = ctx.newQuery(dsCCIoM)
    CCTransactionDataPerProductGroupAggregationUtil.constructQueryForGettingCCDTransRec(detailCCDQuery, customerId)

    def detailNonCCDQuery = ctx.newQuery(dsCCIoM)
    CCTransactionDataPerProductGroupAggregationUtil.constructQueryForGettingNonCCDTransRec(detailNonCCDQuery, customerId)

    def summaryQuery = ctx.newQuery(dsCCIoM)
    CCTransactionDataPerProductGroupAggregationUtil.constructQueryForGettingTransRecSummary(summaryQuery, customerId)
    def joinedSQL = CCTransactionDataPerProductGroupAggregationUtil.getInnerJoinForSummaryNDetailTransRec()

    return ctx.executeSqlQuery(joinedSQL, detailCCDQuery, detailNonCCDQuery, summaryQuery)
}

def inputCustomerId = api.getElement("CurrentCustomerToProcess")
def ctx = api.getDatamartContext()
def ccdTransactionSummaryData = fetchDataFromTransaction(ctx, inputCustomerId)
//api.trace("data found", " (" + (ccdTransactionSummaryData?.size()) + ")")
CCPerProdGroupOutputComputationUtil.populateCustomerCategoryRecords(ccdTransactionSummaryData)
lib.TraceUtility.developmentTrace ("Computed Customer Category Records", ccdTransactionSummaryData)

return true