/**
 * This is helper class which has functions required for computation of data for data source. This contains logic of how
 * records are prepared for saving into DB.
 */

import net.pricefx.formulaengine.DatamartRowSet

def getRecordToInsertOrUpdate(customerRec) {
    return [
            "CCYear"                 : customerRec["t3idy"],
            "CustomerId"             : customerRec["t3ci"],
            "CCCustomerDetails"      : customerRec["t1nm"] ?: customerRec["t2nm1"],
            "CorrectionType"         : "Not Evaluated",
            "CCVerkaufsbereich3"     : customerRec["t1so"] ?: customerRec["t2so"],
            "Prodh1"                 : customerRec["t1pr1"] ?: customerRec["t2pr1"],
            "Prodh2"                 : customerRec["t1pr2"] ?: customerRec["t2pr2"],
            "Prodh3"                 : customerRec["t1pr3"] ?: customerRec["t2pr3"],
            "CCTotalRevenue"         : new BigDecimal(customerRec["t3tr"]?: 0.0),
            "CCTotalTransactionCount": customerRec["t3ttc"],
            "CCArticleCount"         : customerRec["t3aac"],
            "CCTotalMargin"          : new BigDecimal(customerRec["t3tmar"]?: 0.0),
            "CCUnitsPurchased"       : customerRec["t3tu"],
            "CCKundePotentialGruppe" : customerRec["t1cpg"] ?: customerRec["t2cpg"],
            "IsProcessed"            : false,
            "CCDUmsatz"              : new BigDecimal(customerRec["t1r"] ?: 0.0),
            "NonCCDUmsatz"           : new BigDecimal(customerRec["t2r"] ?: 0.0),
            "CCDMargin"              : new BigDecimal(customerRec["t1mar"] ?: 0.0),
            "OtherMargin"            : new BigDecimal(customerRec["t2mar"] ?: 0.0),
            "CCDMenge"               : customerRec["t1u"] ?: 0,
            "NonCCDMenge"            : customerRec["t2u"] ?: 0,
            "CCDQualifiedArticles"   : customerRec["t1ac"] ?: 0,
            "NonCCDArticles"         : customerRec["t2ac"] ?: 0,
            "CCDTransactionCount"    : customerRec["t1tc"] ?: 0,
            "NonCCDTransactioncount" : customerRec["t2tc"] ?: 0
    ]
}

/**
 * This method populates the output records computed into the target data source.
 * @param curCusRecList
 */
def populateCustomerCategoryRecords(curCusRecList) {
    // Save the final aggregated records into DB
    def recordToInsert
    curCusRecList.each { custRecord ->
        recordToInsert = getRecordToInsertOrUpdate(custRecord)
        lib.DBQueries.insertRecordIntoTargetRowSet (recordToInsert)
    }
}

return "0"