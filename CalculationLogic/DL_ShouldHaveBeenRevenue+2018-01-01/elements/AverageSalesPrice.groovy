def revenue = api.getElement("Revenue")
def unitsSold = api.getElement("UnitsSold")

if (!revenue || !unitsSold) {
    return null
}

return (revenue / unitsSold).toBigDecimal()