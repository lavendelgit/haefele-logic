def sku = api.getElement("Sku")
def yearMonth = api.getElement("YearMonth")

def salesPrices = api.global.salesPrices;
def previousSku = api.global.previousSku

if (salesPrices == null || sku != previousSku) {
    def orderBy = "-attribute1" // Valid From (de: Gültig von) - descending, newest to oldest
    def filters = [
            Filter.equal("name", "S_ZPLP"),
            Filter.equal("sku", sku),
    ]
    salesPrices = (api.find("PX50", 0, orderBy, *filters) ?: []).collect { r ->
        return [
                sku: r.sku,
                validFrom: api.parseDate("yyyy-MM-dd", r.attribute1),
                value: r.attribute3?.toBigDecimal(),
                priceUnits: r.attribute9?.toInteger()
        ]
    }
    api.global.salesPrices = salesPrices
    api.global.previousSku = sku
}


def salesPrice = salesPrices?.find { r -> !yearMonth.before(r.validFrom) }

if (!salesPrice) {
    api.addWarning("No SalesPrice valid at given month. Taking the earliest one.")
    salesPrice = salesPrices?.min { r -> r.validFrom }
}

if (salesPrice) {
    return salesPrice.value / (salesPrice.priceUnits ?: 100)
} else {
    api.addWarning("No Sales Price at all.")
}



