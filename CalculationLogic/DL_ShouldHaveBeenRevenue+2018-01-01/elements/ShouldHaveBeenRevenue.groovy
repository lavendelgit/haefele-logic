def sku = api.getElement("Sku")
def unitsSold = api.getElement("UnitsSold")
def unitSalesPrice = api.getElement("UnitSalesPrice")

if (!unitsSold || !unitSalesPrice) {
    api.addWarning("Returning the actual Revenue.")
    return api.getElement("Revenue")
}

api.logInfo("[ShouldHaveBeenRevenue] sku: ${sku}, unitsSold: ${unitsSold}, unitSalesPrice", unitSalesPrice)

return unitsSold * unitSalesPrice