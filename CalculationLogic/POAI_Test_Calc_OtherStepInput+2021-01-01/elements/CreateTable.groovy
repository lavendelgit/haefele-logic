import net.pricefx.common.api.pa.FieldType

def tab1 = model.inputs('twotab', 'tab1')
def tab1_string_entry = tab1.StringEntry as String
def tab1_option_entry = tab1.OptionEntry as String
if (tab1_option_entry == null) {
    api.throwException("Cannot read state from input set in step 'Calc, Evals' and 'Tab 1' tab. Please fill the required option entry there and save it.")
}

def tab2 = model.inputs('twotab', 'tab2')
def tab2_string_entry = tab2.StringEntry as String
def tab2_option_entry = tab2.OptionEntry as String
if (tab2_option_entry == null) {
    api.throwException("Cannot read state from input set in step 'Calc, Evals' and 'Tab 2' tab. Please fill the required option entry there and save it.")
}

model.addTable("table_from_inputs",
        [
                label : "Table from inputs",
                fields: [
                        [name: "tab_name", label: "Tab Name", type: FieldType.TEXT, key: true],
                        [name: "string_entry", label: "String Entry", type: FieldType.TEXT],
                        [name: "option_entry", label: "Option Entry", type: FieldType.TEXT]
                ]
        ]
)

def dmCtx = api.getDatamartContext()

def query = dmCtx.newSqlQuery()
// source not used but needed by the api
query.addSource(dmCtx.newQuery(model.table("table_from_inputs"), false).selectAll(true))
query.setQuery("""
    SELECT * FROM (
    VALUES ('tab1', '${tab1_string_entry}', '${tab1_option_entry}'),
           ('tab2', '${tab2_string_entry}', '${tab2_option_entry}')
    ) AS t (tab_name, string_entry, option_entry)
""")

return model.loadTable(model.table("table_from_inputs"), query, true)
