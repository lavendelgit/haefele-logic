def material = api.getElement("Material")?:""
def customerId = api.getElement("CustomerId")?:""
def invoicePos = api.getElement("InvoicePos")?:""
def invoiceDate = api.getElement("InvoiceDate")?:""

if (invoiceDate instanceof java.util.Date) {
    invoiceDate = invoiceDate.format("yyyy-MM-dd")
}

if (material !=null)
	material = material.substring(0,10)

if (!api.global.uniqueTransactions)
	api.global.uniqueTransactions = [:]

def salesOffice = api.stringUserEntry("SalesOffice")
def invoiceId = api.getElement("InvoiceId")?:""

def key = material + "_" + customerId + "_" + invoicePos + "_" + invoiceDate
def transactionList = api.global.uniqueTransactions[key]
if (!transactionList)
	transactionList = []
def matchingTransaction = transactionList.find { trans ->
       		(trans.contains(invoiceId))
    	}
if (matchingTransaction && matchingTransaction !=null) {
	salesOffice = "Dup_"+salesOffice
} else {
  	transactionList.add(invoiceId)
	api.global.uniqueTransactions[key] = transactionList
}

api.trace (" Outcome of the check...", " Size "+ transactionList.size() +" salesOffice " + salesOffice)

return salesOffice


