if (api.local.isNormal && !api.global.articleFocusGroupMap) {

    cacheArticleFocusGroup()
    cacheFocusGroupDiscountData()
    // cacheCustomerPotentialDiscount()
}

protected cacheArticleFocusGroup() {
    api.global.articleFocusGroupMap = [:]
    List filter = [Filter.equal("name", "FocusGroup")]
    List result = []
    List fields = ["sku", "attribute2"]
    int maxRecords = api.getMaxFindResultsLimit()
    int start = 0
    while (recs = api.find("PX3", start, maxRecords, "sku", fields, false, *filter)) {
        result.addAll(recs)
        start += recs.size()
    }
    api.global.articleFocusGroupMap = result?.collectEntries { articleFocusGroupRow ->
        [(articleFocusGroupRow.sku): articleFocusGroupRow.attribute2]
    }
}

protected cacheFocusGroupDiscountData() {
    api.global.focusGroupRecords = [:]
  	api.global.customerPotentialGroupRecords = [:]
    List filter = [Filter.equal("name", "S_ZRPG"), Filter.lessOrEqual("attribute1", out.TargetDate),
                   Filter.greaterOrEqual("attribute2", out.TargetDate)]
    List focusGroupRecords = []
    def startRow = 0
    while (rows = api.find("PX50", startRow, api.getMaxFindResultsLimit(), "attribute6", *filter)) {
        focusGroupRecords.addAll(rows)
        startRow += rows.size()
    }
    focusGroupRecords?.forEach { focusGroupRow ->
        if (focusGroupRow.attribute9 == "A781") {
            String key = focusGroupRow.attribute6 + "-" + focusGroupRow.attribute7 + "-" + focusGroupRow.attribute8
            if (!api.global.focusGroupRecords[key]) {
                api.global.focusGroupRecords.put(key, focusGroupRow.attribute3)
            }
        } else {

            api.global.customerPotentialGroupRecords.put(focusGroupRow.attribute8, focusGroupRow.attribute3 ?: 0)
        }
    }
}

/*protected cacheCustomerPotentialDiscount() {
    api.global.customerPotentialGroupRecords = api.findLookupTableValues("CustomerPotentialGroup").collectEntries { [(it.name): it.attribute5] }
}*/

