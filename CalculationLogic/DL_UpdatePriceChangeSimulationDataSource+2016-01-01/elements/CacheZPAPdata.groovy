if (api.local.isNormal) {
    List filter = [Filter.equal("name", "S_ZPAP"),
                   //Filter.in("sku",out.GetArticleList),
                   Filter.equal("sku", out.SKU),
                   Filter.lessOrEqual("attribute1", out.TargetDate),
                   Filter.greaterOrEqual("attribute2", out.TargetDate)]

    List result = []
    List fields = ["sku", "attribute1", "attribute2","attribute3","attribute4", "attribute11"]
    int maxRecords = api.getMaxFindResultsLimit()
    int start = 0
    while (recs = api.find("PX50", start, maxRecords, "-attribute1", fields, false, *filter)) {
        result.addAll(recs)
        start += recs.size()
    }

    return result
    /*api.global.ZPAPConditionsAllArticles = result.groupBy({ row -> row.sku })?.collectEntries { String key, List groupedValues ->
        [(key): groupedValues]
    }
    api.trace("api.global.ZPAPConditionsAllArticles", api.global.ZPAPConditionsAllArticles)
     */
}