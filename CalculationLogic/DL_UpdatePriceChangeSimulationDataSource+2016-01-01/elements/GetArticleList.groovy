return " "//getArticleList()

protected List getArticleList() {
    List productList = []

    List filters = [ Filter.equal("attribute30", "Inland 2020") ]
    int startRow = 0

    while (rows = api.find("P", startRow, api.getMaxFindResultsLimit(),"sku",["sku"], *filters)) {
        productList.addAll(rows)
        startRow += rows.size()
    }
    return productList.sku
}