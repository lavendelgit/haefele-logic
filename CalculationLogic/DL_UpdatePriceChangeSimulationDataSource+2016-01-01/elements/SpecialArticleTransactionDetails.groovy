if (!api.local.isXArticle && !api.local.isInternal) {
    return null
}

import net.pricefx.formulaengine.DatamartContext

def ctx = api.getDatamartContext()
def dmTransactions = ctx.getDatamart("TransactionsDM")
List commonFilters = out.TransactionFilter

Map specialArticleTransactionData

if (api.global.specialArticleTransactionData == null) {
    api.global.specialArticleTransactionData = [:]

    Map fields = ['Material'            : 'sku',
                  'SUM(Revenue)'        : 'revenue',
                  'SUM(InvoiceQuantity)': 'quantity',
                  'SUM(Cost)'           : 'cost']

    List filters = [Filter.or(Filter.ilike("Material", "%X"),Filter.ilike('Material', "999.11%")),
                    Filter.equal("InvoiceDateYear", out.AnalysisYear),
                    libs.__LIBRARY__.HaefeleSpecific.getTransactionInlandFilters()[0]]

    libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from SpecialArticleTransactionDetails', api.jsonEncode(filters))

    transactionData = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')?.collect()
    api.global.specialArticleTransactionData = transactionData?.collectEntries { Map skuDetails ->
        BigDecimal cost = skuDetails.cost
        BigDecimal revenue = skuDetails.revenue
        BigDecimal quantity = skuDetails.quantity
        BigDecimal grossPrice = quantity ? revenue / quantity : revenue
        BigDecimal baseCost = quantity ? cost / quantity : cost
        [(skuDetails.sku): ["quantity": quantity, "cost": baseCost, "revenue": revenue, "avgprice": grossPrice, "customerId": Constants.ALL_CUSTOMER_ID]]
    }

}

return api.global.specialArticleTransactionData[out.SKU]

/*
    gross price VK Per 100 -> If invoice quanity is not zero then its revenue1/ qty
                    else    revenue1
    base cost Ek-Wert ->  If invoice quanity is not zero then its cost/ qty
                    else    cost
 */
