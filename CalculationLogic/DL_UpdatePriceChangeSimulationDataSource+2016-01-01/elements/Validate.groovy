if (api.isSyntaxCheck()) return null
return ""
def targetRs = api.global.targetRs

if (targetRs == null) {
    api.throwException("Target Datamart is NULL")
    api.abortCalculation()
}