def targetRs = api.getDatamartRowSet("target")

api.global.printTrace = true
api.global.targetRs = targetRs
api.local.isXArticle = false
api.local.isInternal = false
api.local.isNormal = false

initializeConditionTablesMap()

initializeConditionNameMap()

api.global.exchangeRate = api.findLookupTableValues("ExchangeRate")?.collectEntries {
    [(it.name): it.attribute1]
}

initializeActiveConditionListMap()
initializeConditionFilterMap()
buildDataSourceandPXQueryFields()


/**initializeConditionTablesMap
 *
 * @return
 */
protected initializeConditionTablesMap() {
    api.global.conditionTables = api.findLookupTableValues("ConditionTableMaster", "-attribute1", Filter.equal("attribute2", "Yes"))?.collectEntries { it ->
        Map row = [
                tableName           : it.name,
                additionalKeyColumns: it.attribute1,
                articleFilter       : it.attribute2,
                pgFilter            : it.attribute4
        ]

        [(it.name): row]
    }
}
/** initializeConditionNameMap
 *
 * @return
 */
protected initializeConditionNameMap() {
    List filters = [Filter.equal("attribute3", "Yes"),
                    Filter.in("attribute5", Constants.CONDITION_TYPES)]
    api.global.conditionsList = api.findLookupTableValues("ConditionMaster", *filters).collectEntries { it ->
        Map row = [
                conditionName  : it.name,
                pxTableName    : it.attribute1,
                conditionTables: it.attribute2,
                pricingType    : it.attribute4,
                accessPXTable  : it.attribute6
        ]
        [(it.name): row]
    }
}
/**initializeActiveConditionListMap
 *
 * @return
 */
protected initializeActiveConditionListMap() {
    api.global.activeConditionsList = [:]
    api.global.conditionTables.each { conditionTableRow ->
        String tableName = conditionTableRow.value["tableName"]
        Map activeConditionList = api.global.conditionsList.findAll { it -> it.value["conditionTables"].contains(conditionTableRow.key) && it.value["accessPXTable"] == "Yes" }
        if (activeConditionList) api.global.activeConditionsList.put(tableName, activeConditionList?.keySet())
    }
}
/**initializeConditionFilterMap
 *
 * @return
 */
protected initializeConditionFilterMap() {
    api.global.conditionFilterMap = [:]
    api.global.conditionTables?.each { conditionTable ->
        List activeConditions = api.global.activeConditionsList[conditionTable.key] as List
        activeConditions.each { conditionName ->
            String pxTableName = api.global.conditionsList?.find { conditionListRow -> conditionListRow.key == conditionName }?.value["pxTableName"]
            List conditionFilter = [
                    Filter.equal("KOTAB", conditionTable.key),
                    Filter.isNotNull('KBETR')
            ]
            if (!Library.CONDITION_FETCH_FROM_DS.contains(conditionName)) conditionFilter.add(Filter.equal("name", pxTableName))
            if (api.global.conditionFilterMap[conditionTable.key] == null) api.global.conditionFilterMap[conditionTable.key] = [:]

            api.global.conditionFilterMap[conditionTable.key].put(conditionName, conditionFilter)
        }
    }
}
/**buildDataSourceandPXQueryFields
 *
 * @return
 */
protected buildDataSourceandPXQueryFields() {
// build fields
    api.global.dataFeildsDSMap = [:]
    api.global.dataFeildsPXMap = [:]

    api.global.conditionTables.each { conditionTableRow ->
        String additionalFeilds = conditionTableRow.value["additionalKeyColumns"]
        List DS_FEILDS = ["MATNR", "KOTAB", "KBETR", "KONWA"]
        List PX_FEILDS = ["sku", "KOTAB", "KBETR", "KONWA"]
        if (additionalFeilds != null) {
            PX_FEILDS.addAll(additionalFeilds?.split(","))
            DS_FEILDS.addAll(additionalFeilds?.split(","))
        }
        api.global.dataFeildsDSMap.put(conditionTableRow.key, DS_FEILDS)
        api.global.dataFeildsPXMap.put(conditionTableRow.key, PX_FEILDS)
    }
}
