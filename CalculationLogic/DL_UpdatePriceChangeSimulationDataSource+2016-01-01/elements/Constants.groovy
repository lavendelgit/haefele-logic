import groovy.transform.Field

@Field final String MATERIAL_TYPE_BOM = "BOM"
@Field final String MATERIAL_TYPE_NA = "N/A"
@Field final String MATERIAL_TYPE_NORMAL = "NORMAL"
@Field final String MATERIAL_TYPE_INTERNAL = "INTERNAL"
@Field final String MATERIAL_TYPE_X_ARTICLE = "XARTICLE"
@Field final String MATERIAL_TYPE_TOPCO_ARTCLE = "TOPCO"
@Field final String PRICING_TYPE_NO_DISCOUNT = "No Discounts"
@Field final String GROSS_MAP_PRICE_LABEL = "GrossPrice"
@Field final String GROSS_MAP_MATERIAL_TYPE_LABEL = "MaterialType"
@Field final String ALL_CUSTOMER_ID = "All"
@Field final String CUSTOMER_ID_WITH_NO_TRANSACTION = "TransactionIsMissing"
@Field final Boolean ADD_ROWS_WITH_0_REVENUE_AND_QUANTITY = false
@Field final String CUSTOMER_POTENTIAL_GROUP_CONDITION_TABLE = "A779"
@Field final String CUSTOMER_AND_ARTICLE_FOCUS_GROUP_CONDITION_TABLE = "A781"
@Field final String CURRENCY = "Eur"
@Field final List CONDITION_TYPES = ["COMMON","INLAND"]
@Field final String CONDITION_TYPE_NA = "N/A"
@Field final List CUSTOMER_POTENTIAL_GROUPS = ["R0", "R1", "R2", "R3"]
//@Field final String CUSTOMER_POTENTIAL_NOT_DEFINED = "Not Defined"