return api.local.isXArticle ? null : getZPLPriceForNormalArticle()

protected BigDecimal getZPLPriceForNormalArticle() {
    List filters = [
            Filter.equal("attribute8", "EUR"),
            Filter.lessOrEqual("attribute12", out.TargetDate),
            Filter.greaterOrEqual("attribute13", out.TargetDate),
            Filter.or(Filter.equal("attribute6", "DEX1"), Filter.equal("attribute6", ""))
    ]

    def ZPLCost = Library.getLatestZPLCost(out.SKU, *filters)?.attribute7

    return ZPLCost ? ZPLCost / 100 : null
}