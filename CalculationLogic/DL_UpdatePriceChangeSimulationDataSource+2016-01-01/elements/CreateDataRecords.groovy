import java.security.ProtectionDomain


List pricingDetails = (api.local.isXArticle || api.local.isInternal) ? null : Library.getPricingDetails(out.SKU)
Map transactionData = api.local.isNormal ? out.NormalArticleTransactionDetails : api.global.specialArticleTransactionData
//return ""
//libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from transactionData', api.jsonEncode(transactionData))

if (transactionData) {
    api.local.isNormal ? processNormalArticles(pricingDetails, transactionData) : processSpecialArticles(transactionData[out.SKU])
}

protected processNormalArticles(List pricingDetails, Map transactionData) {
    if (pricingDetails) {
        boolean allCustomerIdRowAdded = false
        List customerIdsList = []
        if (api.global.articleFocusGroupMap[out.SKU] || api.global.customerPotentialGroupRecords) getFocusGroupDiscount(pricingDetails, transactionData)

        pricingDetails.each { Map pricingDetail ->
            String customerId = pricingDetail.KUNNR ?: Constants.ALL_CUSTOMER_ID
            String transactionCustomerId = (customerId == Constants.ALL_CUSTOMER_ID) ? customerId : getCustomerId(customerId, transactionData)

            if (transactionCustomerId) {
                if (transactionCustomerId != Constants.ALL_CUSTOMER_ID && !customerIdsList.contains(transactionCustomerId)) customerIdsList << transactionCustomerId
                Map currentCustomerTransactionDetails = transactionData[transactionCustomerId]
                Map pocketPriceDetails = getPocketPriceDetails(pricingDetail, currentCustomerTransactionDetails)
                String secondaryKey = customerId ?
                                      pricingDetail?.conditionName + "~" + pricingDetail?.conditionTable + "~" + customerId + "~" + pricingDetail?.KBETR :
                                      pricingDetail?.conditionName + "~" + pricingDetail?.conditionTable + "~" + pricingDetail?.KBETR
                addPricingDetailsRow(secondaryKey, transactionCustomerId, currentCustomerTransactionDetails, pocketPriceDetails, pricingDetail)
            }
        }
        if ((transactionData.size() - 1) != customerIdsList.size()) {
            String secondaryKey = Constants.CONDITION_TYPE_NA + "~" + Constants.CONDITION_TYPE_NA + "~" + Constants.ALL_CUSTOMER_ID + "~" + "0"
            Map allCustomerTransactionDetails = transactionData[Constants.ALL_CUSTOMER_ID]
            Map allCustomerIdPockePriceDetails = getSpecialArticlePocketPriceDetails(allCustomerTransactionDetails)
            addPricingDetailsRow(secondaryKey, Constants.ALL_CUSTOMER_ID, allCustomerTransactionDetails, allCustomerIdPockePriceDetails)
        }
    }
}

protected getFocusGroupDiscount(List pricingDetails, Map transactionData) {
    transactionData?.each { transactionDataRow ->
        if (transactionDataRow.value["customerid"] != Constants.ALL_CUSTOMER_ID) {
            String focusGroup = api.global.articleFocusGroupMap[out.SKU]
            String salesOffice = transactionDataRow.value["salesoffice"]
            String customerPotentialGroup = transactionDataRow.value["customerpotentialgroup"]
          if (customerPotentialGroup && (customerPotentialGroup in Constants.CUSTOMER_POTENTIAL_GROUPS ))
          {
                String key = salesOffice + "-" + focusGroup + "-" + customerPotentialGroup
                BigDecimal focusGroupRebate = api.global.focusGroupRecords[key] ?: 0.0
                BigDecimal customerGroupRebate = api.global.customerPotentialGroupRecords[customerPotentialGroup] ?: 0.0
                BigDecimal focusAndCustomerGroupRebate = (Math.abs(customerGroupRebate) + Math.abs(focusGroupRebate)) * -1
                String conditionTable = (!focusGroupRebate && customerGroupRebate != null) ? Constants.CUSTOMER_POTENTIAL_GROUP_CONDITION_TABLE : Constants.CUSTOMER_AND_ARTICLE_FOCUS_GROUP_CONDITION_TABLE

                if (focusAndCustomerGroupRebate != null) {
                    Map conditionDetails = [
                            pricingType   : Library.PRICING_TYPE_PER_DISCOUNT,
                            aggregate     : "No",
                            KBETR         : focusAndCustomerGroupRebate,
                            conditionTable: conditionTable,
                            conditionName : "ZRPG",
                            KUNNR         : transactionDataRow.value["customerid"]
                    ]
                    pricingDetails.addAll(conditionDetails)
                }
            }
        }
    }
}

protected processSpecialArticles(Map transactionData) {
    String customerId = transactionData.customerId
    Map pocketPriceDetails = getSpecialArticlePocketPriceDetails(transactionData)
    String secondaryKey = Constants.CONDITION_TYPE_NA + "~" + Constants.CONDITION_TYPE_NA + "~" + customerId + "~" + "0"
    addPricingDetailsRow(secondaryKey, customerId, transactionData, pocketPriceDetails)

}

protected addPricingDetailsRow(String secondaryKey, String transactionCustomerId, Map currentCustomerTransactionDetails, Map pocketPriceDetails, Map pricingDetail = null) {
    def targetRowSet = api.getDatamartRowSet("target")
    Map row = createTargetRow(secondaryKey, transactionCustomerId, currentCustomerTransactionDetails, pocketPriceDetails, pricingDetail)
    //libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from processNormalArticles', api.jsonEncode(row))
  api.trace("row",row)
  targetRowSet?.addRow(row)
}

protected Map createTargetRow(String secondaryKey, String customerId, Map transactionData, Map pocketPriceDetails, Map pricingDetail = null) {
    BigDecimal baseCost = (customerId == Constants.ALL_CUSTOMER_ID) ?
            out.BaseCost :
            transactionData.quantity ? transactionData.cost / transactionData.quantity : transactionData.cost
    String pricingType = (api.local.isXArticle || api.local.isInternal) ?
            (api.local.isXArticle ? Constants.MATERIAL_TYPE_X_ARTICLE : Constants.MATERIAL_TYPE_INTERNAL) :
            pricingDetail ? pricingDetail.pricingType : Constants.PRICING_TYPE_NO_DISCOUNT

    Map row = [SecondaryKey      : secondaryKey,
               Material          : out.SKU,
               BaseCost          : baseCost,
               GrossPrice        : (pricingType == Constants.PRICING_TYPE_NO_DISCOUNT) ? transactionData.avgprice : out.GrossPrice,
               PurchasePrice     : out.PurchasePrice,
               MaterialType      : out.MaterialType,
               ZPL               : out.ZPL,
               PricingType       : pricingDetail ? pricingDetail.pricingType : pricingType,
               KBETR             : pricingDetail ? pricingDetail.KBETR : 0,
               Year              : out.AnalysisYear,
               QtySold           : transactionData.quantity,
               TotalRevenue      : transactionData.revenue,
               QtySoldInland     : transactionData.quantity,
               TotalRevenueInland: transactionData.revenue,
               AvgGrossPrice     : transactionData.avgprice,
               ConditionName     : pricingDetail ? pricingDetail.conditionName : Constants.CONDITION_TYPE_NA,
               ConditionTable    : pricingDetail ? pricingDetail.conditionTable : Constants.CONDITION_TYPE_NA,
               Currency          : Constants.CURRENCY,
               MarkUp            : out.Markup,
               Margin            : out.Margin,
               MarginPer         : out.MarginPer,
               CustomerId        : (customerId != Constants.ALL_CUSTOMER_ID) ? customerId : pricingDetail?.KUNNR,
               PocketPrice       : pocketPriceDetails?.pocketPrice,
               PocketMargin      : pocketPriceDetails?.pocketMargin,
               PocketMarginPer   : pocketPriceDetails?.pocketMarginPer]

    return row
}

protected String getCustomerId(String customerId, Map transactionData) {
    String customerIdWithPrefix0 = "000" + customerId
    return transactionData[customerId] ? customerId : transactionData[customerIdWithPrefix0] ? customerIdWithPrefix0 : Constants.ADD_ROWS_WITH_0_REVENUE_AND_QUANTITY ? Constants.CUSTOMER_ID_WITH_NO_TRANSACTION : null
}

protected Map getPocketPriceDetails(Map pricingDetail, Map currentCustomerTransactionDetails) {
    BigDecimal pocketPrice
    BigDecimal pocketMargin
    BigDecimal pocketMarginPer
    BigDecimal grossPrice

    BigDecimal baseCost = currentCustomerTransactionDetails.customerbasecost

    if (pricingDetail?.conditionName == "ZRI" && pricingDetail?.conditionTable == "A617" && out.ZPL && out.ZPL > 0) {
        grossPrice = out.ZPL
    } else {
        grossPrice = out.GrossPrice
    }

    if (pricingDetail) {
        switch (pricingDetail.pricingType) {
            case Library.PRICING_TYPE_PER_DISCOUNT:
                pocketPrice = (grossPrice * (1 + pricingDetail.KBETR))
                break
            case Library.PRICING_TYPE_MARKUP:
                pocketPrice = (1 + pricingDetail.KBETR) * baseCost
                break
            case Library.PRICING_TYPE_NETPRICE:
                pocketPrice = pricingDetail.KBETR
                break
            case Library.PRICING_TYPE_ABS_DISCOUNT:
                pocketPrice = grossPrice ? (grossPrice - pricingDetail.KBETR) : 0
                break
        }
    }
    pocketMargin = pocketPrice - baseCost
    pocketMarginPer = (pocketPrice != 0) ? (pocketMargin / pocketPrice) : null

    Map pocketPriceDetails = [
            pocketPrice    : pocketPrice,
            pocketMargin   : pocketMargin,
            pocketMarginPer: pocketMarginPer
    ]

    return pocketPriceDetails
}

protected Map getSpecialArticlePocketPriceDetails(Map transactionDataRow) {
    BigDecimal baseCost = out.BaseCost
    BigDecimal pocketPrice = transactionDataRow.quantity ?
            transactionDataRow.revenue / transactionDataRow.quantity :
            transactionDataRow.revenue
    BigDecimal pocketMargin = pocketPrice - baseCost
    BigDecimal pocketMarginPer = (pocketPrice != 0) ? (pocketMargin / pocketPrice) : null
    Map pocketPriceDetails = [pocketPrice    : pocketPrice,
                              pocketMargin   : pocketMargin,
                              pocketMarginPer: pocketMarginPer]

    return pocketPriceDetails
}