/*String articleTypeParameterValue = out.IsTopcoProduct ?
                                   Constants.MATERIAL_TYPE_TOPCO_ARTCLE :
                                   api.local.isInternal ? Constants.MATERIAL_TYPE_INTERNAL : Constants.MATERIAL_TYPE_X_ARTICLE
*/

String articleTypeParameterValue = (api.local.isXArticle || api.local.isInternal) ?
  									api.local.isInternal ? Constants.MATERIAL_TYPE_INTERNAL : Constants.MATERIAL_TYPE_X_ARTICLE :
									out.IsTopcoProduct ? Constants.MATERIAL_TYPE_TOPCO_ARTCLE : "N/A"
                                   

return api.local.isNormal ? Library.getGrossPrice(out.SKU) : getGrossPriceDetailsMap(articleTypeParameterValue )

protected getGrossPriceDetailsMap(String articleType) {
    return ["$Constants.GROSS_MAP_PRICE_LABEL": 0, "$Constants.GROSS_MAP_MATERIAL_TYPE_LABEL": "$articleType"]
}