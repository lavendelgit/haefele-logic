import net.pricefx.formulaengine.DatamartContext

if (!api.local.isNormal) {
    return null
}

def ctx = api.getDatamartContext()
def dmTransactions = ctx.getDatamart("TransactionsDM")
String skuUnderProcess = out.SKU
List commonFilters = getTransactionCommonFiltersForNormalElements(skuUnderProcess)
DatamartContext.Query basicTransactionQuery = getTransactionQueryForMainCalculations(ctx, dmTransactions, commonFilters)
DatamartContext.Query grossPriceQuery = getTransactionQueryForCaculationOnPositiveRevenue(ctx, dmTransactions, commonFilters)
DatamartContext.Query customerPotentialGroupQuery = getCustomerPotentialGroupQueryForMainCalculations(ctx, dmTransactions, commonFilters)
DatamartContext.Query latestCustomerPotentialGroupQuery = getLatestCustomerPotentialGroupQuery(ctx, dmTransactions, commonFilters)

String completeDataFetchQuery = getJoinQuery()
List transactionData = ctx.executeSqlQuery(completeDataFetchQuery, basicTransactionQuery, grossPriceQuery, customerPotentialGroupQuery, latestCustomerPotentialGroupQuery)?.toResultMatrix()?.collect()

return constructCustomerTransactionDetailsMap(transactionData?.entries[0], skuUnderProcess)

List getTransactionCommonFiltersForNormalElements(String sku) {
    List filters = [Filter.equal("Material", sku),
                    Filter.equal("InvoiceDateYear", out.AnalysisYear),
           //Filter.notEqual("CustomerPotentialGroup","Not defined"),
                   // Filter.and(Filter.notEqual("CustomerPotentialGroup","Not defined"), Filter.NotEqual("CustomerPotentialGroup"," ")),
                    libs.__LIBRARY__.HaefeleSpecific.getTransactionInlandFilters()[0]]

    return filters
}

DatamartContext.Query getTransactionQueryForMainCalculations(DatamartContext ctx, def dmTransactions, List filters) {
    DatamartContext.Query basicQuery = ctx.newQuery(dmTransactions, true)
    basicQuery.select('Material', 'sku')
    basicQuery.select("CustomerId", 'customerid')
    basicQuery.select("SUM(Revenue)", 'revenuetotal')
    basicQuery.select("SUM(Cost)", 'costtotal')
    basicQuery.select("SUM(InvoiceQuantity)", 'allquantity')
    basicQuery.select("SUM(MarginAbs)", 'allmargin')
    basicQuery.where(*filters)
    basicQuery.having(Filter.or(Filter.notEqual('revenuetotal', 0),
            Filter.notEqual('costtotal', 0),
            Filter.notEqual('allmargin', 0)))
//api.trace("basicQuery",basicQuery)
    return basicQuery
}

DatamartContext.Query getTransactionQueryForCaculationOnPositiveRevenue(DatamartContext ctx, def dmTransactions, List filters) {
    List filtersForGrossPriceNBasePrice = [Filter.greaterThan('Revenue', 0)]
    filtersForGrossPriceNBasePrice.addAll(filters)
    DatamartContext.Query grossPriceBasePriceQuery = ctx.newQuery(dmTransactions, true)
    grossPriceBasePriceQuery.select('Material', 'sku')
    grossPriceBasePriceQuery.select("CustomerId", 'customerid')
    grossPriceBasePriceQuery.select('AVG(SalesPricePer100Units/100)', 'grossprice')
    grossPriceBasePriceQuery.select('COUNT(Material)','nonzerorows')
    grossPriceBasePriceQuery.where(*filtersForGrossPriceNBasePrice)

    return grossPriceBasePriceQuery
}

DatamartContext.Query getLatestCustomerPotentialGroupQuery(DatamartContext ctx, def dmTransactions, List filters) {
    DatamartContext.Query latestCustomerPotentialGroupQuery = ctx.newQuery(dmTransactions, true)
    latestCustomerPotentialGroupQuery.select("CustomerId", 'customerid')
    //latestCustomerPotentialGroupQuery.select("Max(InvoiceDate)", 'invoicedate')
    latestCustomerPotentialGroupQuery.select("Max(InvoiceId)", 'invoiceid')
    latestCustomerPotentialGroupQuery.where(*filters)
    return latestCustomerPotentialGroupQuery
}

DatamartContext.Query getCustomerPotentialGroupQueryForMainCalculations(DatamartContext ctx, def dmTransactions, List filters) {
    DatamartContext.Query customerPotentialGroupQuery = ctx.newQuery(dmTransactions, true)
    customerPotentialGroupQuery.select("CustomerId", 'customerid')
    customerPotentialGroupQuery.select("invoiceid", 'invoiceid')
    //IF(cond, value when true, value when false)
    //customerPotentialGroupQuery.select("IFNULL(CustomerPotentialGroup,'Not defined')",'customerpotentialgroup')
  customerPotentialGroupQuery.select("CustomerPotentialGroup", 'customerpotentialgroup')
    //customerPotentialGroupQuery.select("ISNULL(CustomerPotentialGroup)", 'customerpotentialgroup')
   //customerPotentialGroupQuery.select("COUNT(CustomerPotentialGroup)", 'customerpotentialgroup')
    customerPotentialGroupQuery.select("SalesOffice", 'salesoffice')
    customerPotentialGroupQuery.where(*filters)
    //if (customerPotentialGroupQuery && (customerPotentialGroupQuery in Constants.CUSTOMER_POTENTIAL_GROUPS ) && customerPotentialGroupQuery != "" && customerPotentialGroupQuery != "Not defined")

    api.trace("customerPotentialGroupQuery",customerPotentialGroupQuery)
    return customerPotentialGroupQuery
}

String getJoinQuerycompleteDataFetchQueryTest() {

    return """SELECT T1.sku as sku, T1.customerid as customerid, T1.customerpotentialgroup, T1.salesoffice,
            IF(T1.customerpotentialgroup = "Not defined", "" , T1.customerpotentialgroup ),
            END
            FROM T1 INNER JOIN T2 ON T1.customerid = T2.customerid AND T1.invoiceid = T2.invoiceid"""
}



String getJoinQuery() {
    return """SELECT T1.sku as sku, T1.customerid as customerid, IsNull(T1.revenuetotal,0) as revenue, 
            IsNull(T1.allquantity,0) as quantity, T1.allmargin as margin, IsNull(T1.costtotal,0) as cost,
            T3.customerpotentialgroup , T3.salesoffice,
            CASE WHEN T1.allquantity = 0 THEN IsNull(T1.revenuetotal, 0)
                 WHEN T1.allquantity <> 0 THEN T1.revenuetotal / T1.allquantity
            END as avgprice,
            CASE WHEN T1.allquantity = 0 THEN IsNull(T2.grossprice,0)
                 WHEN T1.allquantity <> 0 THEN T1.revenuetotal / T1.allquantity
            END as grossprice, 
            CASE WHEN T1.allquantity = 0 THEN T1.costtotal
                 WHEN T1.allquantity <> 0 THEN T1.costtotal / T1.allquantity
            END as customerbasecost,
            IsNull(T2.nonzerorows, 0) as nonzerorows 
            FROM T1 LEFT OUTER JOIN T2 ON T1.sku = T2.sku AND T1.customerid = T2.customerid
            LEFT OUTER JOIN T3 ON T3.customerid = T1.customerid 
            INNER JOIN T4 ON T4.customerid = T3.customerid AND T3.invoiceid = T4.invoiceid"""
}

Map constructCustomerTransactionDetailsMap(List transactionRows, String sku) {
    Map customerTransactionDetailsMap = [:]
    api.trace("transactionRows",transactionRows)
    if (transactionRows) {
        customerTransactionDetailsMap = transactionRows?.collectEntries { transactionRow ->
            [(transactionRow.customerid): transactionRow]
        }
        Integer totalQuantity = transactionRows.quantity.sum()
        api.trace("totalQuantity",totalQuantity)
        BigDecimal totalRevenue = transactionRows.revenue.sum()
        BigDecimal totalCost = transactionRows.cost.sum()
        BigDecimal grossPriceAcross = transactionRows.grossprice.sum()
        Integer entryCount = transactionRows.size()
        boolean isTotalQuantityNonZero = totalQuantity != 0
        BigDecimal avgPrice = isTotalQuantityNonZero ? totalRevenue / totalQuantity : totalRevenue
        BigDecimal baseCost = isTotalQuantityNonZero ? totalCost / totalQuantity : totalCost
        //BigDecimal grossPrice = isTotalQuantityNonZero ? totalRevenue / totalQuantity : totalRevenue
       //entryCount ? grossPriceAcross / entryCount : grossPriceAcross

        Map allCustomerTransactionEntry = [sku             : sku,
                                           customerid      : Constants.ALL_CUSTOMER_ID,
                                           grossprice      : avgPrice,
                                           cost            : baseCost,
                                           avgprice        : avgPrice,
                                           revenue         : totalRevenue,
                                           quantity        : totalQuantity,
                                           nonzerorows     : transactionRows.nonzerorows.sum(),
                                           margin          : transactionRows.margin.sum(),
                                           customerbasecost: baseCost]
        customerTransactionDetailsMap.put(Constants.ALL_CUSTOMER_ID, allCustomerTransactionEntry)

        if (Constants.ADD_ROWS_WITH_0_REVENUE_AND_QUANTITY) {
            customerTransactionDetailsMap.put(Constants.CUSTOMER_ID_WITH_NO_TRANSACTION, createTransactionRow(out.SKU, Constants.CUSTOMER_ID_WITH_NO_TRANSACTION))
        }
        /*customerTransactionDetailsMap.iterator().with { iterator ->
            iterator.each { entry ->
                if (entry.value.customerid != Constants.ALL_CUSTOMER_ID && (entry.value.revenue < 0 || entry.value.margin < 0)) {
                    iterator.remove()
                }
            }
        }*/
    }
    api.trace("customerTransactionDetailsMap",customerTransactionDetailsMap)
    return customerTransactionDetailsMap
}

protected Map createTransactionRow(String sku, String customerId, BigDecimal grossPrice = 0, BigDecimal baseCost = 0, BigDecimal avgPrice = 0, BigDecimal revenue = 0, Integer quantity = 0, Integer nonZeroRows = 0) {
    return transactionRow = [
            'sku'        : sku,
            'customerid' : customerId,
            'grossprice' : grossPrice,
            'cost'       : baseCost,
            'avgprice'   : avgPrice,
            'revenue'    : revenue,
            'quantity'   : quantity,
            'nonzerorows': nonZeroRows
    ]
}
//NOTE: This is used for debugging
/*
verifyOutput(basicTransactionQuery, ctx, 'basicTransactionQuery')
verifyOutput(grossPriceQuery, ctx, 'grossPriceQuery')


void verifyOutput(DatamartContext.Query query, DatamartContext ctx, String testingSource) {
    def results = ctx.executeQuery(query).getData().collect()
    api.trace("results",results)
    //libs.__LIBRARY__.TraceUtility.developmentTrace('=================Printing Values============= from ' + testingSource, api.jsonEncode(results))
}

 */

/*
TODO
when the customer id has two types (null or Not Defined) of potential group being specified in the transaction data,
then the same row get picks up twice, which is resulting in more revenue and more quantity. which need to be fixed

Need to try out putting in DM query getCustomerPotentialGroupQueryForMainCalculations the
If condition for customer potential group link for the ref is specified below.
https://pricefx.atlassian.net/wiki/spaces/HELP/pages/2770863184/Datamart+Query+Expressions

Articles to verify the fix
231.66.2521
006.37.098
981.53.182
911.17.033
917.99.503
833.74.096
917.99.506
917.99.507
 */
/*
IF(T1.customerpotentialgroup,0) as customerpotentialgroup,
CASE WHEN T1.customerpotentialgroup = null THEN T1.customerpotentialgroup
END as CustomerPotentialGroup,*/
