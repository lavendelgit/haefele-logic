def product = api.currentItem()
//api.logInfo("########## product", product)
if (!api.isDebugMode() && (!product || !product.sku)) {
    api.abortCalculation(true)
}
//api.logInfo("########## product - 1", api.jsonEncode(product))
return product