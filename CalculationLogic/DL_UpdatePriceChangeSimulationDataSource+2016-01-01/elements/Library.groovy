import groovy.transform.Field

@Field final String PRICING_TYPE_PER_DISCOUNT = "Per Discount"
@Field final String PRICING_TYPE_MARKUP = "Markup"
@Field final String PRICING_TYPE_NETPRICE = "Net Price"
@Field final String PRICING_TYPE_ABS_DISCOUNT = "Abs Discount"
@Field final List CONDITION_FETCH_FROM_DS = ["ZRA", "ZRI"]


List getPricingDetails(String sku) {
    List allPricingDetails = []
    Map conditionTables = api.global.conditionTables
    conditionTables.each { conditionTableRow ->
        List conditions = api.global.activeConditionsList[conditionTableRow.key] as List
        conditions?.each { String conditionName ->
            List pricingDetails = getConditionDetails(conditionName, conditionTableRow.key, sku)
            if (pricingDetails) {
                allPricingDetails.addAll(pricingDetails)
            }
        }
    }
    List conditionRecordsZPAP = addZPAPConditions(allPricingDetails)
    if (conditionRecordsZPAP) allPricingDetails.addAll(conditionRecordsZPAP)
    return allPricingDetails
}

protected List addZPAPConditions(allPricingDetails) {
    List conditionRecordsZPAP = out.CacheZPAPdata
    List ZPAPConditionRecords = []
    conditionRecordsZPAP?.each { conditionRow ->
        Map conditionDetails = [
                pricingType   : PRICING_TYPE_NETPRICE,
                aggregate     : "No",
                KBETR         : ((conditionRow.attribute3?:0) as BigDecimal) / 100,
                conditionTable: conditionRow.attribute11,
                conditionName : "ZPAP",
                KUNNR         : conditionRow.attribute4
        ]
        ZPAPConditionRecords.add(conditionDetails)
    }
    return ZPAPConditionRecords
}

protected List getConditionDetails(String conditionName, String conditionTableName, String sku) {
    List allConditionDetails = []

    List conditionRecords = getConditionRecords(sku, conditionName, conditionTableName)

    if (conditionName != "ZRA" && conditionName != "ZRI" && conditionRecords) {
        if (conditionName == "ZPA" && conditionRecords) {
            for (row in conditionRecords) {
                String currency = row.KONWA
                if (currency != "%" && currency != "EUR") {
                    row.KBETR = convertToEur(currency, row.KBETR)
                }
            }
        }
        def groupedConditionRecords = conditionRecords.groupBy { it -> it.KUNNR }
        for (groupedConditionRecord in groupedConditionRecords) {
            resultSize = groupedConditionRecord.value.size()
            def disVal = groupedConditionRecord.value.sum { it.KBETR as BigDecimal }
            def pricingType = api.global.conditionsList[conditionName].pricingType ?: PRICING_TYPE_PER_DISCOUNT

            if (pricingType == PRICING_TYPE_NETPRICE) {
                disVal = disVal / 100
            }
            def discountValue = (disVal && resultSize > 0) ? (disVal / resultSize) : 0.0

            Map conditionDetails = [
                    pricingType   : pricingType,
                    aggregate     : (resultSize > 1) ? "Yes" : "No",
                    KBETR         : discountValue,
                    conditionTable: conditionTableName,
                    conditionName : conditionName,
                    KUNNR         : groupedConditionRecord.key
            ]
            if (groupedConditionRecord.key) {
                conditionDetails.KUNNR = groupedConditionRecord.key
            }
            allConditionDetails << conditionDetails
        }
    } else {
        allConditionDetails = conditionRecords
    }

    return allConditionDetails
}

protected def getConditionRecords(String sku, String conditionName, String conditionTableName) {
    List result = []

    List conditionFilter = buildConditionFilter(sku, conditionName, conditionTableName)
    List fields = CONDITION_FETCH_FROM_DS.contains(conditionName) ? api.global.dataFeildsDSMap[conditionTableName] : api.global.dataFeildsPXMap[conditionTableName]

    //api.trace("conditionFilter",conditionFilter)
    if (CONDITION_FETCH_FROM_DS.contains(conditionName)) {
        def ctx = api.getDatamartContext()
        def ds = ctx.getDataSource("$conditionName")

        def query = ctx.newQuery(ds)
        fields.each { String field ->
            (field == "KBETR") ? query.select("AVG(KBETR)", "KBETR") : query.select(field)
        }
        query.where(*conditionFilter)
        result = ctx.executeQuery(query)?.getData()?.collect()
    } else {
        def start = 0
        int maxRecords = api.getMaxFindResultsLimit()
        while (recs = api.find("PX50", start, maxRecords, "sku", fields, false, *conditionFilter)) {
            result.addAll(recs)
            start += recs.size()
        }

        return result
    }

    if (result) {
        List allConditionDetails = []
        for (row in result) {
            Map conditionDetails = [
                    pricingType   : PRICING_TYPE_PER_DISCOUNT,
                    aggregate     : "No",
                    KBETR         : row.KBETR,
                    conditionTable: conditionTableName,
                    conditionName : conditionName,
                    KUNNR         : row.KUNNR
            ]
            allConditionDetails << conditionDetails
        }
        return allConditionDetails
    }
}

protected List buildConditionFilter(String sku, String conditionName, String conditionTable) {

    String skuFieldName = CONDITION_FETCH_FROM_DS.contains(conditionName) ? "MATNR" : "sku"
    List conditionFilter = (api.global.conditionFilterMap[conditionTable][conditionName] as List).clone()
    String pgFilter = api.global.conditionTables.find { conditionTableRow -> conditionTableRow.key == conditionTable }?.value["pgFilter"]
    conditionFilter << Filter.in("$skuFieldName", [sku, "*"])

    if (pgFilter) {
        String productHirechary1 = api.product("attribute11", sku)?.split(" ")[0]
        String productHirechary2 = api.product("attribute12", sku)?.split(" ")[0]
        String productHirechary3 = api.product("attribute13", sku)?.split(" ")[0]
        String productHirechary4 = api.product("attribute14", sku)?.split(" ")[0]
        String productHirechary5 = api.product("attribute15", sku)?.split(" ")[0]
        String productHirechary6 = api.product("attribute16", sku)?.split(" ")[0]
        String productHirechary7 = api.product("attribute17", sku)?.split(" ")[0]

        if (productHirechary1) {
            conditionFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH1", productHirechary1), Filter.isNull("ZZPRODH1"))))
        }
        if (productHirechary2) {
            conditionFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH2", productHirechary2), Filter.isNull("ZZPRODH2"))))
        }
        if (productHirechary3) {
            conditionFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH3", productHirechary3), Filter.isNull("ZZPRODH3"))))
        }
        if (productHirechary4) {
            conditionFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH4", productHirechary4), Filter.isNull("ZZPRODH4"))))
        }
        if (productHirechary5) {
            conditionFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH5", productHirechary5), Filter.isNull("ZZPRODH5"))))
        }
        if (productHirechary6) {
            conditionFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH6", productHirechary6), Filter.isNull("ZZPRODH6"))))
        }
        if (productHirechary7) {
            conditionFilter.add(Filter.and(Filter.or(Filter.equal("ZZPRODH7", productHirechary7), Filter.isNull("ZZPRODH7"))))
        }
    }

    return conditionFilter
}

def getLatestZNRVCost(String sku, Filter... additionalFilters) {
    def targetDate = out.TargetDate

    def filters = [
            Filter.equal("name", "S_ZNRV"),
            Filter.equal("sku", sku),
            Filter.lessOrEqual("attribute1", targetDate),
            Filter.greaterOrEqual("attribute2", targetDate)
    ]

    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute1"
    def salePrice = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (salePrice[0]) {
        result = salePrice[0]
    }
    return result
}

def getLatestZPLCost(String sku, Filter... additionalFilters) {
    def targetDate = out.TargetDate

    def filters = [
            Filter.equal("name", "S_ZPL"),
            Filter.equal("sku", sku)
    ]

    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }

    def orderBy = "-attribute1"
    def ZPLRecord = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (ZPLRecord[0]) {
        result = ZPLRecord[0]
    }

    return result
}

String convertToEur(String fromCurrency, String KBETR) {
    BigDecimal convertedKBETR = KBETR as BigDecimal
    BigDecimal exchangerate = api.global.exchangeRate[fromCurrency] ?: 1

    if (fromCurrency != "EUR") {
        convertedKBETR = exchangerate * convertedKBETR
    }
    return convertedKBETR as String
}

protected Map getGrossPrice(sku) {
    int skuLength = sku.length()
    Map grossPriceMap = ["${Constants.GROSS_MAP_PRICE_LABEL}": 0, "${Constants.GROSS_MAP_MATERIAL_TYPE_LABEL}": "${Constants.MATERIAL_TYPE_NA}"]
    String skuWithLength10 = (skuLength == 11) ? sku.substring(0, 10) : null

    return getSalesPrice(sku, skuWithLength10) ?: getCompoundPrice(sku, skuWithLength10) ?: grossPriceMap
}

protected Map getSalesPrice(String sku, String skuWithLength10) {

    List filter = [Filter.equal("attribute7", "EUR"),
                   Filter.lessOrEqual("attribute1", out.TargetDate),
                   Filter.greaterOrEqual("attribute2", out.TargetDate)]

    BigDecimal salesPrice = lib.Find.latestSalesPriceRecord(sku,*filter)?.attribute3

    if (!salesPrice && skuWithLength10) {
        salesPrice = lib.Find.latestSalesPriceRecord(skuWithLength10, Filter.equal("attribute7", "EUR"))?.attribute3
    }

    return salesPrice ? ["${Constants.GROSS_MAP_PRICE_LABEL}": salesPrice / 100, "${Constants.GROSS_MAP_MATERIAL_TYPE_LABEL}": "${Constants.MATERIAL_TYPE_NORMAL}"] : null
}

protected Map getCompoundPrice(String sku, String skuWithLength10) {
    List filter = [Filter.equal("attribute5", "EUR"),
                   Filter.lessOrEqual("attribute6", out.TargetDate),
                   Filter.greaterOrEqual("attribute7", out.TargetDate)]

    BigDecimal compoundPrice = lib.Find.latestCompoundPriceRecord(sku, *filter)?.attribute1

    if (!compoundPrice && skuWithLength10) {
        compoundPrice = lib.Find.latestCompoundPriceRecord(sku, Filter.equal("attribute5", "EUR"))?.attribute1
    }

    return compoundPrice ? ["${constants.GROSS_MAP_PRICE_LABEL}": compoundPrice / 100, "${Constants.GROSS_MAP_MATERIAL_TYPE_LABEL}": "${Constants.MATERIAL_TYPE_BOM}"] : null
}

protected BigDecimal getBaseCostByLookup(String sku, String targetDate) {
    List filters = [Filter.equal("attribute16", "EUR"),
                    Filter.lessOrEqual("attribute1", targetDate),
                    Filter.greaterOrEqual("attribute2", targetDate)]
    BigDecimal baseCost = lib.Find.latestBaseCostRecord(sku, *filters)?.attribute3
    baseCost = baseCost ? baseCost / 100 : BigDecimal.ZERO
    return baseCost
}