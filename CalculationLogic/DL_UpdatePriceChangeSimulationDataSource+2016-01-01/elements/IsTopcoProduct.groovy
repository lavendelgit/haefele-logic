if (!api.global.topco) {
    api.global.topco = api.findLookupTableValues("TopCoProducts")*.name as Set
}

return api.global.topco.contains(out.SKU) ? true : false