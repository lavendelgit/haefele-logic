BigDecimal baseCost = out.NormalArticleTransactionDetails ? out.NormalArticleTransactionDetails[Constants.ALL_CUSTOMER_ID]?.cost : out.SpecialArticleTransactionDetails?.cost

return (baseCost != null && baseCost != 0) ? baseCost : Library.getBaseCostByLookup(out.SKU, out.TargetDate)