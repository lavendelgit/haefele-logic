def effectiveYear = api.local.validityYear

def effectiveYearParam = "Effective Year"
def efficiencyScenario = "Efficiency Category"

return api.dashboard("PPIArticleLevelLossDetails")
        .setParam(effectiveYearParam, effectiveYear)
        .setParam(efficiencyScenario, 'All')
        .showEmbedded()
        .andRecalculateOn(api.dashboardWideEvent("ArticleDetails-ED"))
        .withEventDataAttr("EfficiencyCategory").asParam(efficiencyScenario)