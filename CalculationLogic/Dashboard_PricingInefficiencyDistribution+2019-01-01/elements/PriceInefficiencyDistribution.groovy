import net.pricefx.common.api.FieldFormatType

def validityYear = api.input("Effective Year")
//validityYear = (validityYear) ?: "2020" // Only for testing...
api.local.validityYear = validityYear
def dbFields = ['EfficiencyCategory', 'NumberOfArticles', 'LossToBusiness', 'LossRecovered',
                'OverallImpact', 'TotalTransactions', 'TransactionsParticipated', 'TotalTransactedUnits', 'TransactionUnitsParticipated']
def fields = ['EfficiencyCategory'          : dbFields[0],
              'COUNT(Material)'             : dbFields[1],
              'SUM(PurchasePriceChangeLoss)': dbFields[2],
              'SUM(SalesPriceChangeGain)'   : dbFields[3],
              'SUM(TotalPriceChangeImpact)' : dbFields[4],
              'SUM(TotalTransactions)'      : dbFields[5],
              'SUM(RelevantTransactions)'   : dbFields[6],
              'SUM(TotalTransactedUnits)'   : dbFields[7],
              'SUM(RelevantTransactedUnits)': dbFields[8]
]
def filters = [Filter.equal("Year", validityYear + "-01-01")]
def dataRows = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'PurchasePriceInefficiencyRecordsDM', filters, 'DataMart').collectEntries {
    [(it[dbFields[0]]): (it)]
}
libs.__LIBRARY__.TraceUtility.developmentTraceRow('Entries details', dataRows)

def yearDetails = " (Year-$validityYear) "
def columns = ['Price Change Scenario', 'Impacted Number Of Articles' + yearDetails,
               'Estimated Scope of Improvement' + yearDetails
//               ,'Estimated Loss Recovered' + yearDetails, 'Net Loss' + yearDetails,
//               'Number Of Transactions For Affected Articles' + yearDetails,
//               'Probable Number Of Transactions Impacted For Affected Articles' + yearDetails,
//               'Number Of Transacted Units For Affected Articles' + yearDetails, 'Probable Number Of Transaction Units Impacted For Affected Articles' + yearDetails
]
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.INTEGER,
                   FieldFormatType.MONEY_EUR
//                 , FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR,
//                 FieldFormatType.INTEGER, FieldFormatType.INTEGER,
//                 FieldFormatType.INTEGER, FieldFormatType.INTEGER
]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Details: Purchase Price Change Inefficiencies' + yearDetails,
        columns, columnTypes) // MK

def netLoss
def netLossColor
def currentEntry
libs.HaefeleCommon.PricingInefficiencyCommon?.displayOrder?.each { key, value ->
    currentEntry = dataRows[key]
    netLoss = (currentEntry) ? (currentEntry[dbFields[4]]) : 0.0
    netLossColor = (netLoss && netLoss < 0) ? "#fc5c65" : "#2e7d10"
    netLoss = (netLoss) ? netLoss * -1 : 0.0
    resultMatrix.addRow([
            (columns[0]): value,
            (columns[1]): (currentEntry) ? currentEntry[dbFields[1]] : 0,
            (columns[2]): dashUtl.boldHighlightWith(resultMatrix, (currentEntry) ? currentEntry[dbFields[2]] : 0.0, "#fc5c65"),
/*            (columns[3]): (currentEntry) ? currentEntry[dbFields[3]] : 0.0,
            (columns[4]): dashUtl.boldHighlightWith(resultMatrix, netLoss, netLossColor),
            (columns[5]): (currentEntry) ? currentEntry[dbFields[5]] : 0,
            (columns[6]): (currentEntry) ? currentEntry[dbFields[6]] : 0,
            (columns[7]): (currentEntry) ? currentEntry[dbFields[7]] : 0,
            (columns[8]): (currentEntry) ? currentEntry[dbFields[8]] : 0
*/
    ])
}
resultMatrix.setPreferenceName("PPI Detail View")
resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("ArticleDetails-ED"))
        .withColValueAsEventDataAttr(columns[0], "EfficiencyCategory")

return resultMatrix