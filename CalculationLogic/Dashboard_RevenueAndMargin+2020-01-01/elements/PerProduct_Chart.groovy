String queryName = "QueryDataPerProducts"
String chartTitle = "Products Revenue and Margin %"
Map sqlConfiguration = Configuration.SQL_CONFIGURATION
def categoryName = sqlConfiguration.getAt("productName")
def categoryId = sqlConfiguration.getAt("productId")
def categoryAttribute = api.getElement("BandByForProduct")
String chartSeriesTooltipName = "Product"

return PerCategory_Commons.buildChart(queryName, chartSeriesTooltipName, chartTitle, categoryAttribute, categoryName, categoryId)