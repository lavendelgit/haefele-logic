String queryName = "QueryDataPerProducts"
String chartTitle = 'Products Revenue and Margin Contribution'
String axisTitle = "No. of Products"
Map sqlConfiguration = Configuration.SQL_CONFIGURATION
def categoryName = sqlConfiguration.getAt("productName")
def categoryId = sqlConfiguration.getAt("productId")
Map drilldownLabelType = ChartsConfiguration.REVENUE_AND_MARGIN.drilldownType.product

return RevenueMarginContributionPerCategory_Commons.buildChart(queryName, drilldownLabelType, chartTitle, axisTitle, categoryName, categoryId)