def prepareData(List data, Map drilldownLabelType, String categoryName, String categoryId) {
    Map chartData = [bucketRevenueData  : [],
                     bucketMarginData   : [],
                     drilldownSeriesData: []]
    if (!data) {
        return chartData
    }

    List marginContributionRanking = data.sort(false, { a, b -> (b?.margin as BigDecimal) <=> (a?.margin as BigDecimal) })
    List revenueContributionRanking = data.sort(false, { a, b -> (b?.revenue as BigDecimal) <=> (a?.revenue as BigDecimal) })
    BigDecimal totalRevenue = revenueContributionRanking.sum { (it?.revenue ?: 0) }
    BigDecimal totalMargin = marginContributionRanking.sum { (it?.margin ?: 0) }

    List buckets = api.getElement("Buckets")

    int preMarginIdx = 0
    int preRevenueIdx = 0
    Map revenueBucket, marginBucket
    String preBucketLabel
    for (bucket in buckets) {
        revenueBucket = prepareBucketData(ChartsConfiguration.LABELS.revenue, buckets, bucket, revenueContributionRanking, totalRevenue, preRevenueIdx, preBucketLabel, drilldownLabelType, categoryName, categoryId)
        chartData.bucketRevenueData.add(revenueBucket.bucketData)
        chartData.drilldownSeriesData.add(revenueBucket.dataForRankingGroup)

        marginBucket = prepareBucketData(ChartsConfiguration.LABELS.margin, buckets, bucket, marginContributionRanking, totalMargin, preMarginIdx, preBucketLabel, drilldownLabelType, categoryName, categoryId)
        chartData.bucketMarginData.add(marginBucket.bucketData)
        chartData.drilldownSeriesData.add(marginBucket.dataForRankingGroup)

        preRevenueIdx = revenueBucket.preIndex
        preMarginIdx = marginBucket.preIndex
        preBucketLabel = bucket.label
    }

    return chartData
}

def prepareBucketData(String type, List buckets, Map bucket, List contributionRanking, BigDecimal totalValue, int preIdx, String preBucketLabel, Map drilldownLabelType, String categoryName, String categoryId) {
    def bucketName = bucket?.name as String
    def representingValue = (bucket?.value as BigDecimal) * totalValue
    def fullBucketName = type + bucketName

    def rankingListByPercent = getRankingByPercentValue(contributionRanking.collect { it.getAt(type.toLowerCase()) }, representingValue)
    def bucketRevenueData = generateBucketRowData(rankingListByPercent, representingValue, fullBucketName, buckets, bucket)
    def dataForRankingGroup = generateDataForDrilldownColumnChart(type, contributionRanking, preIdx, rankingListByPercent.Size, preBucketLabel, bucketName, drilldownLabelType, categoryName, categoryId)

    return [dataForRankingGroup: dataForRankingGroup,
            bucketData         : bucketRevenueData,
            preIndex           : rankingListByPercent.Size]
}

Map generateBucketRowData(Map rankingListByPercent, BigDecimal representingValue, String fullBucketName, List buckets, def bucket) {
    Map row = [name        : bucket.label,
               y           : rankingListByPercent?.get("Size"),
               total       : rankingListByPercent?.get("Value"),
               representing: representingValue]

    if (buckets?.last() != bucket) {
        row << [drilldown: fullBucketName]
    }

    return row
}

Map generateDataForDrilldownColumnChart(String type,
                                        List contributionRanking,
                                        int fromIndex,
                                        int toIndex,
                                        String preBucketLabel,
                                        String currentBucketName,
                                        Map drilldownLabelType,
                                        String categoryName,
                                        String categoryId) {
    Map dataForRankingGroup = [:]
    dataForRankingGroup?.name = type.capitalize()
    dataForRankingGroup?.id = type.capitalize() + currentBucketName
    dataForRankingGroup?.tooltip = [headerFormat: '',
                                    pointFormat : generateDrilldownPointFormat(type)]
    List rankingItemList = getRevenueRankingItemListByIndexForColumn(contributionRanking, fromIndex, toIndex, preBucketLabel, drilldownLabelType, categoryName, categoryId)
    dataForRankingGroup?.type = "column"
    dataForRankingGroup?.data = rankingItemList

    return dataForRankingGroup
}

List getRevenueRankingItemListByIndexForColumn(List revenueRanking, int fromIndex, int toIndex, String preBucketLabel, Map drilldownLabelType, String categoryName, String categoryId) {
    List itemList = []
    def constantTop = ChartsConfiguration.REVENUE_AND_MARGIN.constantTop
    int top10Index = (toIndex - fromIndex) > constantTop ? constantTop : (toIndex - fromIndex)
    String item
    if (fromIndex > 0) {
        def drilldownLabel = fromIndex == 1 ? drilldownLabelType.singular : drilldownLabelType.plural
        item = preBucketLabel + " (${fromIndex} ${drilldownLabel})"
        itemList.add(prepareItem(0, fromIndex, revenueRanking, item))
    }
    for (int i = fromIndex; i < (fromIndex + top10Index); i++) {
        itemList.add(prepareTop10Item(revenueRanking.getAt(i), categoryId, categoryName))
    }
    if (top10Index == constantTop && (toIndex - fromIndex) > constantTop) {
        item = "Others (${(toIndex - fromIndex)})"
        itemList.add(prepareItem(fromIndex + top10Index, toIndex, revenueRanking, item))
    }

    return itemList
}

String generateSeriesTooltipFormat(String tooltipCategory, String tooltipTitle) {
    return '<tr><td style="padding:0">' + tooltipTitle + ': {point.y:,.0f}</td><br>' +
            '<td style="padding:0">Total ' + tooltipCategory + ': {point.total:,.0f} ' + ChartsConfiguration.FORMATS.currency + '</td><br>' +
            '<td style="padding:0">Representing ' + tooltipCategory + ': {point.representing:,.0f} ' + ChartsConfiguration.FORMATS.currency + '</td></tr><br>'
}

def getChartDefinition(String title, String axisTitle, List bucketRevenueData, List bucketMarginData, List drilldownSeriesData) {
    def hLib = libs.HighchartsLibrary
    def config = ChartsConfiguration.REVENUE_AND_MARGIN

    def chart = hLib.Chart.newChart()
    def tooltipFooterFormat = config.tooltip.footerFormat
    def tooltipHeaderFormat = config.tooltip.headerFormat

    def tooltipMargin = hLib.Tooltip.newTooltip()
            .setHeaderFormat(tooltipHeaderFormat)
            .setPointFormat(generateSeriesTooltipFormat(config.tooltipLabels.margin, axisTitle))
            .setFooterFormat(tooltipFooterFormat)
            .setUseHTML(config.tooltip.useHTML)

    def tooltipRevenue = hLib.Tooltip.newTooltip()
            .setHeaderFormat(tooltipHeaderFormat)
            .setPointFormat(generateSeriesTooltipFormat(config.tooltipLabels.revenue, axisTitle))
            .setFooterFormat(tooltipFooterFormat)
            .setUseHTML(config.tooltip.useHTML)

    def xAxis = hLib.Axis.newAxis().setType(hLib.ConstConfig.AXIS_TYPES.CATEGORY)

    def yAxis = hLib.Axis.newAxis().setTitle(axisTitle)

    def seriesRevenue = hLib.ColumnSeries.newColumnSeries()
            .setName(ChartsConfiguration.LABELS.revenue)
            .setData(bucketRevenueData)
            .setTooltip(tooltipRevenue)
            .setXAxis(xAxis)
            .setYAxis(yAxis)

    def seriesMargin = hLib.ColumnSeries.newColumnSeries()
            .setName(ChartsConfiguration.LABELS.margin)
            .setData(bucketMarginData)
            .setTooltip(tooltipMargin)
            .setXAxis(xAxis)
            .setYAxis(yAxis)

    List drilldownSeries = []
    def tooltip

    drilldownSeriesData.each {
        tooltip = hLib.Tooltip.newTooltip()
                .setHeaderFormat(it.tooltip.headerFormat)
                .setPointFormat(it.tooltip.pointFormat)
        drilldownSeries.add(hLib.ColumnSeries.newColumnSeries()
                .setName(it.name)
                .setId(it.id)
                .setData(it.data)
                .setTooltip(tooltip))
    }

    chart.setSeries(seriesRevenue, seriesMargin)
            .setTitle(title)
            .setDrilldownSeries(*drilldownSeries)
            .setPlotOptions(config.plotOptions)

    return chart.getHighchartFormatDefinition()
}

def buildChart(String queryName, Map drilldownLabelType, String chartTitle, String yAxisTitle, String categoryName, String categoryId) {
    List queryData = api.getElement(queryName)

    def data = prepareData(queryData, drilldownLabelType, categoryName, categoryId)
    def chartDef = getChartDefinition(chartTitle, yAxisTitle, data.bucketRevenueData, data.bucketMarginData, data.drilldownSeriesData)

    return api.buildHighchart(chartDef)
}

Map getRankingByPercentValue(List ranking, BigDecimal percentValue) {
    Map result = [:]
    BigDecimal totalValue = ranking.sum { (it ?: 0) }
    int size = ranking.size()
    if (percentValue == totalValue) {
        return addSizeAndValue(result, size, totalValue)
    }

    BigDecimal cumulativeValue = 0
    int idx = 0
    while (cumulativeValue < percentValue && idx < size) {
        cumulativeValue += (ranking.getAt(idx))
        idx++
    }

    return addSizeAndValue(result, idx, cumulativeValue)
}

Map addSizeAndValue(Map result, int size, BigDecimal value) {
    result << [Size: size]
    result << [Value: value]

    return result
}

String generateDrilldownPointFormat(String type) {
    String highlightSpan = '<span style="color:{point.color};font-weight: bold">'
    Map pointFormatMap = [begin  : '<b>{point.name}</b><br>',
                          revenue: "Revenue: {point.revenue:,.0f} ${ChartsConfiguration.FORMATS.currency}<br>",
                          margin : "Margin: {point.margin:,.0f} ${ChartsConfiguration.FORMATS.currency}<br>Margin%: {point.marginPercent:,.2f} %"]

    return pointFormatMap.inject("") { result, entry ->
        result += (entry.key == type.toLowerCase()) ? highlightSpan + entry.value + "</span>" : entry.value
    }
}

Map prepareTop10Item(Map revenueRanking, String categoryId, String categoryName) {
    String customer = revenueRanking.getAt(categoryId) + " <br> " + revenueRanking.getAt(categoryName)

    return [name         : customer,
            y            : revenueRanking?.revenue,
            displayData  : revenueRanking?.revenue,
            revenue      : revenueRanking?.revenue,
            margin       : revenueRanking?.margin,
            marginPercent: revenueRanking?.marginPercent]
}

Map prepareItem(int fromIndex, int toIndex, List revenueRanking, String customerName) {
    BigDecimal otherRevenue = 0
    BigDecimal otherMargin = 0
    BigDecimal otherMarginPercent = 0
    for (int i = fromIndex; i < toIndex; i++) {
        otherRevenue += ((revenueRanking?.getAt(i)?.revenue ?: 0) as BigDecimal)
        otherMargin += ((revenueRanking?.getAt(i)?.margin ?: 0) as BigDecimal)
    }
    if (otherRevenue) {
        otherMarginPercent = 100 * (otherMargin / otherRevenue)
    }

    return [name         : customerName,
            y            : otherRevenue,
            displayData  : otherRevenue,
            revenue      : otherRevenue,
            margin       : otherMargin,
            marginPercent: otherMarginPercent]
}