@Field PERIODS = ["Week", "Month", "Quarter", "Year"]

@Field PFX_TEMPLATE_PP_CONFIGURATION = [name        : "PFXTemplate_DB_RevenueAndMargin",
                                        columnLabels: [entryKey: "name",
                                                       value   : "value"]]

@Field BUCKETS_CONFIGURATION = [namePrefix         : "Bucket",
                                labelSuffix        : "%",
                                defaultStartPercent: 0,
                                defaultEndPercent  : 1,
                                defaultNoOfBuckets : 10]