Map getDatamartFieldMap(String datamartName, String owner) {
    def ctx = api.getDatamartContext()
    def dm = ctx.getDatamart(datamartName)
    def fields = [:]

    def fieldList = dm.fc()?.fields
    def ownedFields = fieldList?.findAll {
        it?.owningFC?.toLowerCase() == owner?.toLowerCase() && it?.dimension == true
    }
    for (item in ownedFields) {
        fields << [(item?.name): item?.label]
    }

    return fields
}

String getGroupableFieldLabel(String datamartName, String fieldName) {
    def ctx = api.getDatamartContext()
    def dm = ctx.getDatamart(datamartName)
    def fieldList = dm.fc()?.fields
    def field = fieldList?.find { it?.name?.toLowerCase() == fieldName?.toLowerCase() && it?.dimension == true }
    if (field) {
        return field.label
    }

    return null
}

def createBandByForCategoryInput(String category, String categoryIDFieldName, String optionLabel) {
    Map sqlConfiguration = Configuration.SQL_CONFIGURATION
    String datamartName = sqlConfiguration.datamartName
    Map categoryFields = getDatamartFieldMap(datamartName, category)
    String fieldName = sqlConfiguration.getAt(categoryIDFieldName)
    String fieldLabel = getGroupableFieldLabel(datamartName, fieldName)
    if (categoryIDFieldName) {
        categoryFields += (fieldLabel) ? [(fieldName): fieldLabel] : [(fieldName): fieldName]
    }

    Map sortedCategoryFields = categoryFields.sort { a, b -> a?.value <=> b?.value }
    def selectedByUser = api.option(optionLabel, sortedCategoryFields.keySet() as List, sortedCategoryFields)

    return getSelectedOrDefaultValue(optionLabel, sortedCategoryFields, selectedByUser)
}

def getSelectedOrDefaultValue(String optionLabel, Map sortedCategoryFields, def selected) {
    def param = api.getParameter(optionLabel)
    if (param != null && param?.getValue() == null) {
        param.setValue(sortedCategoryFields.keySet().getAt(0))

        return sortedCategoryFields.keySet().getAt(0)
    }

    return selected
}