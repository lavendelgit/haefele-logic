@Field SQL_CONFIGURATION
SQL_CONFIGURATION = libs.SIP_Dashboards_Commons.ConfigurationUtils.getAdvancedConfiguration([:], libs.SIP_Dashboards_Commons.ConstConfig.DASHBOARDS_ADVANCED_CONFIGURATION_NAME)

Map dashboardPPConfig = ConstConfig.PFX_TEMPLATE_PP_CONFIGURATION

@Field SETTINGS
SETTINGS = api.findLookupTableValues(dashboardPPConfig.name).collectEntries {
    return [(it.getAt(dashboardPPConfig.columnLabels.entryKey)): it.getAt(dashboardPPConfig.columnLabels.value)]
}