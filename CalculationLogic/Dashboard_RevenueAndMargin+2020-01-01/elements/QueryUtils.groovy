Map getBasicQueryDefinition(String categoryIDFieldName) {
    Map sqlConfiguration = Configuration.SQL_CONFIGURATION
    def revenueFieldName = sqlConfiguration.getAt("invoicePrice")
    def marginFieldName = sqlConfiguration.getAt("grossMargin")
    String targetCurrencyCode = api.getElement("TargetCurrencyData").currencyCode

    return [datamartName      : sqlConfiguration.getAt("datamartName"),
            rollup            : true,
            fields            : [revenue      : "SUM(${revenueFieldName})",
                                 margin       : "SUM(${marginFieldName})",
                                 marginPercent: "100*SUM(${marginFieldName})/SUM(${revenueFieldName})"],
            orderBy           : categoryIDFieldName,
            whereFilters      : api.getElement("QueryDefaultFilter"),
            targetCurrencyCode: targetCurrencyCode]
}

Map getQueryDefinition(String categoryAttribute, String categoryIDFieldName, String categoryNameFieldName) {
    Map queryDefinition = getBasicQueryDefinition(categoryIDFieldName)

    if (categoryAttribute) {
        queryDefinition.fields += [(categoryAttribute): categoryAttribute]
    }
    if (categoryIDFieldName) {
        queryDefinition.fields += [(categoryIDFieldName): categoryIDFieldName]
    }
    if (categoryNameFieldName) {
        queryDefinition.fields += [(categoryNameFieldName): categoryNameFieldName]
    }

    return queryDefinition
}