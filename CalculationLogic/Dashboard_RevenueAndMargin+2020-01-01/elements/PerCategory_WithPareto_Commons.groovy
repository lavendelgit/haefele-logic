Map prepareData(List data, int histogramBins) {
    List calculatedData = []

    List sortedData = data?.sort(false, { a, b -> return b.revenue?.toBigDecimal() <=> a.revenue?.toBigDecimal() })
    int bins = histogramBins ?: 1

    int valueDistanceBin = 0
    if (sortedData?.size() > 0) {
        valueDistanceBin = ((sortedData.getAt(0).revenue ?: 0.0) / bins as BigDecimal) + 1
    }
    if (sortedData != null && sortedData?.size() > 0) {
        calculatedData = prepareCalculatedData(bins, sortedData, valueDistanceBin)
    }

    calculatedData.sort { a, b -> return b.countVal <=> a.countVal }
    Map resultData = prepareCumulativeData(calculatedData, valueDistanceBin)

    return [category                  : calculatedData?.label,
            revenueData               : resultData.revenueData,
            cumulativeContributionData: resultData.cumulativeContributionData]
}

def prepareCalculatedData(int bins, List sortedData, BigDecimal valueDistanceBin) {
    List resultData
    List calculatedData = []
    String formatNumber = ChartsConfiguration.PARETO.series.formatNumber
    String currencySymbol = api.getElement("TargetCurrencyData").currencySymbol
    for (int i = 0; i < bins; i++) {
        resultData = getListProductInBin(sortedData, i * valueDistanceBin, (i + 1) * valueDistanceBin)
        calculatedData.add([sumVal  : (resultData?.sum { it.revenue }) ?: 0.0,
                            countVal: (resultData?.size()) ?: 0.0,
                            label   : "${api.formatNumber(formatNumber, i * valueDistanceBin)} $currencySymbol → ${api.formatNumber(formatNumber, (i + 1) * valueDistanceBin)} $currencySymbol"])
    }

    return calculatedData
}

def prepareCumulativeData(List calculatedData, int valueDistanceBin) {
    BigDecimal cumulativeVal = 0
    List revenueData = []
    List cumulativeContributionData = []
    calculatedData.eachWithIndex { item, i ->
        revenueData.add([y          : item?.countVal,
                         projections: [binMin: i * valueDistanceBin,
                                       binMax: (i + 1) * valueDistanceBin],
                         description: item.label,
                         name       : item.label])
        cumulativeVal = cumulativeVal + (item.sumVal ?: 0.0)
        cumulativeContributionData.add([y   : cumulativeContributionPercent(calculatedData?.sumVal, cumulativeVal),
                                        name: item.label])
    }

    return [revenueData               : revenueData,
            cumulativeContributionData: cumulativeContributionData]
}

def getChartDefinition(String title, String axisTitle, def category, def revenueData, def cumulativeContributionData) {
    def hLib = libs.HighchartsLibrary
    def config = ChartsConfiguration.PARETO

    def axis1 = hLib.Axis.newAxis()
            .setTitle(axisTitle)

    def axis2 = hLib.Axis.newAxis()
            .setTitle(config.yAxis.title)
            .setOpposite(config.yAxis.opposite)
            .setMin(config.yAxis.min)
            .setMax(config.yAxis.max)

    def axisX = hLib.Axis.newAxis()
            .setCategories(category)

    def colSeriesTooltip = hLib.Tooltip.newTooltip().setPointFormat('<b>∑ Revenue: {point.description}</b><br>' + axisTitle + ': {point.y}')

    def seriesCol = hLib.ColumnSeries.newColumnSeries()
            .setName(config.series.column.name)
            .setData(revenueData)
            .setIndex(config.series.column.index)
            .setZIndex(config.series.column.zIndex)
            .setYAxis(axis1)
            .setXAxis(axisX)
            .setTooltip(colSeriesTooltip)

    def lineSeriesTooltip = hLib.Tooltip.newTooltip().setPointFormat(config.lineSeriesTooltipPointFormat)

    def seriesLine = hLib.LineSeries.newLineSeries()
            .setName(config.series.line.name)
            .setData(cumulativeContributionData)
            .setIndex(config.series.line.index)
            .setZIndex(config.series.line.zIndex)
            .setYAxis(axis2)
            .setXAxis(axisX)
            .setTooltip(lineSeriesTooltip)

    def tooltip = hLib.Tooltip.newTooltip()
            .setShared(config.tooltip.shared)
            .setHeaderFormat(config.tooltip.headerFormat)

    def chart = hLib.Chart.newChart()
            .setTitle(title)
            .setSeries(seriesCol, seriesLine)
            .setPlotOptions(config.plotOptions)
            .setTooltip(tooltip)

    return chart.getHighchartFormatDefinition()
}

def buildChart(String queryName, String chartTitle, String axisTitle) {
    List dataForPerCategory = api.getElement(queryName)
    Map data = prepareData(dataForPerCategory, Configuration.SETTINGS.histogramBins as int)
    def chartDef = getChartDefinition(chartTitle, axisTitle, data.category, data.revenueData, data.cumulativeContributionData)

    return api.buildHighchart(chartDef)
}

BigDecimal cumulativeContributionPercent(List data, BigDecimal cumVal) {
    BigDecimal total = data?.sum()
    if (!total) {
        return 0
    }

    return ((cumVal ?: 0) / total) * 100
}

List getListProductInBin(List data, BigDecimal fromValue, BigDecimal toValue) {
    List result = []
    for (item in data) {
        if (item.revenue >= fromValue && item.revenue < toValue) {
            result.add(item)
        }
    }

    return result
}