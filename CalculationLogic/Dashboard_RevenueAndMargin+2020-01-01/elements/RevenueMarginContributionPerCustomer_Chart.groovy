String queryName = "QueryDataPerCustomers"
String chartTitle = 'Customers Revenue and Margin Contribution'
String axisTitle = "No. of Customers"
Map sqlConfiguration = Configuration.SQL_CONFIGURATION
def categoryName = sqlConfiguration.getAt("customerName")
def categoryId = sqlConfiguration.getAt("customerId")
Map drilldownLabelType = ChartsConfiguration.REVENUE_AND_MARGIN.drilldownType.customer

return RevenueMarginContributionPerCategory_Commons.buildChart(queryName, drilldownLabelType, chartTitle, axisTitle, categoryName, categoryId)