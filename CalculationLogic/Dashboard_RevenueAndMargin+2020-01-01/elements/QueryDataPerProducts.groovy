Map sqlConfiguration = Configuration.SQL_CONFIGURATION
def productIDFieldName = sqlConfiguration.getAt("productId")
def productNameFieldName = sqlConfiguration.getAt("productName")
def productAttribute = api.getElement("BandByForProduct")

Map queryDefinition = QueryUtils.getQueryDefinition(productAttribute, productIDFieldName, productNameFieldName)

return libs.HighchartsLibrary.QueryModule.queryDatamart(queryDefinition)