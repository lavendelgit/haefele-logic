String optionName = "Time Period"
def selected = api.option(optionName, ConstConfig.PERIODS)
String defaultTimePeriod = "Quarter"

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(optionName, defaultTimePeriod)

return selected