String queryName = "QueryDataPerProducts"
String chartTitle = 'Revenue Pareto (Products)'
String axisTitle = "No. of Products"

return PerCategory_WithPareto_Commons.buildChart(queryName, chartTitle, axisTitle)