Map sqlConfiguration = Configuration.SQL_CONFIGURATION
def customerIDFieldName = sqlConfiguration.getAt("customerId")
def customerNameFieldName = sqlConfiguration.getAt("customerName")
def customerAttribute = api.getElement("BandByForCustomer")

Map queryDefinition = QueryUtils.getQueryDefinition(customerAttribute, customerIDFieldName, customerNameFieldName)

return libs.HighchartsLibrary.QueryModule.queryDatamart(queryDefinition)