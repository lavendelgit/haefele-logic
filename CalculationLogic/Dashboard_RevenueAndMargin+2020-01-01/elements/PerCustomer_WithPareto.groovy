String queryName = "QueryDataPerCustomers"
String chartTitle = 'Revenue Pareto (Customers)'
String axisTitle = "No. of Customers"

return PerCategory_WithPareto_Commons.buildChart(queryName, chartTitle, axisTitle)