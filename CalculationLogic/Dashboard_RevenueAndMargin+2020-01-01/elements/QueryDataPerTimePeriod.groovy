Map sqlConfiguration = Configuration.SQL_CONFIGURATION

def transactionDate = sqlConfiguration.getAt("pricingDate")
def period = api.getElement("TimePeriod")
def periodTime = transactionDate + period

def queryDefinition = QueryUtils.getBasicQueryDefinition(periodTime)
queryDefinition.fields << [period: periodTime]

return libs.HighchartsLibrary.QueryModule.queryDatamart(queryDefinition)