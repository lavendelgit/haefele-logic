Map sqlConfiguration = Configuration.SQL_CONFIGURATION

def productIDFieldName = sqlConfiguration.getAt("productId")
def customerIDFieldName = sqlConfiguration.getAt("customerId")
def transactionDate = sqlConfiguration.getAt("pricingDate")

def customers = api.getElement("Customers")
def products = api.getElement("Products")
def dateFrom = api.getElement("DateFrom")
def dateTo = api.getElement("DateTo")

def revenueFieldName = sqlConfiguration.getAt("invoicePrice")
def marginFieldName = sqlConfiguration.getAt("grossMargin")

Filter genericFilter = api.getElement("GenericFilter")

def queryUtils = libs.SIP_Dashboards_Commons.QueryUtils
List filters = [*queryUtils.generateCustomerProductFilters(products, customers, productIDFieldName, customerIDFieldName),
                *queryUtils.generateDateFromToFilters(dateFrom, dateTo, transactionDate),
                Filter.isNotNull(revenueFieldName),
                Filter.isNotNull(marginFieldName),
                genericFilter]

return filters