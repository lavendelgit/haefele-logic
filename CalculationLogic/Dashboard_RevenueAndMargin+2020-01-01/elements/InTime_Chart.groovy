def hLib = libs.HighchartsLibrary
def config = ChartsConfiguration.IN_TIME

def timeChart = hLib.Chart.newChart()
        .setTitle(config.title)

def dataForInTime = api.getElement("QueryDataPerTimePeriod")
def category = dataForInTime?.period
def revenueData = dataForInTime?.revenue
def marginData = dataForInTime?.marginPercent

def axis1 = hLib.Axis.newAxis().setTitle(config.yAxes.axis1.title)

def axis2 = hLib.Axis.newAxis()
        .setTitle(config.yAxes.axis2.title)
        .setOpposite(config.yAxes.axis2.opposite)

def axisX = hLib.Axis.newAxis().setCategories(category)

def columnTooltip = hLib.Tooltip.newTooltip()
        .setHeaderFormat(config.series.column.tooltip.headerFormat)
        .setPointFormat(config.series.column.tooltip.pointFormat)
        .setUseHTML(config.series.column.tooltip.useHTML)
        .setShared(config.series.column.tooltip.shared)

def seriesCol = hLib.ColumnSeries.newColumnSeries()
        .setName(config.series.column.name)
        .setYAxis(axis1)
        .setXAxis(axisX)
        .setData(revenueData)
        .setTooltip(columnTooltip)

def lineTooltip = hLib.Tooltip.newTooltip()
        .setHeaderFormat(config.series.line.tooltip.headerFormat)
        .setPointFormat(config.series.line.tooltip.pointFormat)
        .setUseHTML(config.series.line.tooltip.useHTML)
        .setShared(config.series.line.tooltip.shared)

def tooltip = hLib.Tooltip.newTooltip()
        .setShared(true)

def seriesLine = hLib.SplineSeries.newSplineSeries()
        .setName(config.series.line.name)
        .setYAxis(axis2)
        .setXAxis(axisX)
        .setData(marginData)
        .setTooltip(lineTooltip)

timeChart.setSeries(seriesCol, seriesLine)
        .setZoomType(hLib.ConstConfig.ZOOM_TYPES.XY)
        .setTooltip(tooltip)
        .setEnableMouseWheelZoomMapNavigation(config.enableMouseWheelZoomMapNavigation)

return api.buildHighchart(timeChart.getHighchartFormatDefinition())