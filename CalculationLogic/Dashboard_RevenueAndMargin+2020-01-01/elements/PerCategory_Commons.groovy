Map prepareData(List dataForPerCategory, String chartSeriesTooltipName, String categoryAttribute, String categoryName, String categoryId) {
    def seriesData = fillSeriesData(dataForPerCategory, chartSeriesTooltipName, categoryAttribute, categoryName, categoryId)
    def revenueValueForPlotLine = findValueForPlotLineRevenue(dataForPerCategory)
    def marginPercentValueForPlotLine = findValueForPlotLineMarginPercent(dataForPerCategory)

    return [seriesData                   : seriesData,
            revenueValueForPlotLine      : revenueValueForPlotLine,
            marginPercentValueForPlotLine: marginPercentValueForPlotLine]
}

List fillSeriesData(List dataForPerCategory, String chartSeriesTooltipName, String categoryAttribute, String categoryName, String categoryId) {
    List seriesData = []
    Map groupedDataForPerCategory = dataForPerCategory.groupBy { it.getAt(categoryAttribute) }
    Set categoryKeys = groupedDataForPerCategory.keySet()
    categoryKeys.take(ChartsConfiguration.PER_CATEGORY.limit).each {
        seriesData.add(generateCategorySeries(groupedDataForPerCategory, chartSeriesTooltipName, it, categoryId, categoryName))
    }

    return seriesData
}

def generateCategorySeries(Map groupedDataForPerCategory, String chartSeriesTooltipName, def categoryKey, String categoryId, String categoryName) {
    List itemList = []
    def filterData = groupedDataForPerCategory.getAt(categoryKey)
    def category
    for (filterDataItem in filterData) {
        category = filterDataItem.getAt(categoryId) + ' - ' + filterDataItem.getAt(categoryName)
        itemList.add([name: category,
                      x   : libs.SharedLib.RoundingUtils.round(filterDataItem?.revenue, 2),
                      y   : libs.SharedLib.RoundingUtils.round(filterDataItem?.marginPercent, 2)])
    }

    def tooltip = libs.HighchartsLibrary.Tooltip.newTooltip()
            .setHeaderFormat(ChartsConfiguration.PER_CATEGORY.tooltipHeaderFormat)
            .setPointFormat(generateSeriesTooltipFormat(chartSeriesTooltipName))

    def categoryData = libs.HighchartsLibrary.ScatterSeries.newScatterSeries()
            .setTooltip(tooltip)
            .setData(itemList)
            .setName(categoryKey ?: 'Others')

    return categoryData
}

String generateSeriesTooltipFormat(String tooltipDimension) {
    Map formats = ChartsConfiguration.FORMATS
    return "${tooltipDimension}: {point.name}<br>Revenue: {point.x:,.2f} ${formats.currency} <br>Margin %: {point.y:,.2f} %"
}

def getChartDefinition(List seriesData, BigDecimal revenueValueForPlotLine, BigDecimal marginPercentValueForPlotLine, String chartTitle) {
    def hLib = libs.HighchartsLibrary
    def config = ChartsConfiguration.PER_CATEGORY

    def numberFormat = config.axes.plotLine.label.numberFormat
    def style = config.axes.plotLine.label.style

    def xAxisPlotLineLabel = [style: style,
                              y    : config.axes.plotLine.label.y,
                              text : api.formatNumber(numberFormat, revenueValueForPlotLine)]

    def yAxisPlotLineLabel = [style: style,
                              x    : config.axes.plotLine.label.x,
                              text : api.formatNumber(numberFormat, marginPercentValueForPlotLine)]

    def yAxisPlotLine = hLib.Axis.newPlotLine()
            .setColor(style.color)
            .setWidth(config.axes.plotLine.width)
            .setValue(marginPercentValueForPlotLine)
            .setLabel(yAxisPlotLineLabel)

    def xAxisPlotLine = hLib.Axis.newPlotLine()
            .setColor(style.color)
            .setWidth(config.axes.plotLine.width)
            .setValue(revenueValueForPlotLine)
            .setLabel(xAxisPlotLineLabel)

    def xAxis = hLib.Axis.newAxis()
            .setTitle(config.axes.xTitle)
            .setPlotLines(xAxisPlotLine)

    def yAxis = hLib.Axis.newAxis()
            .setTitle(config.axes.yTitle)
            .setPlotLines(yAxisPlotLine)

    def legend = hLib.Legend.newLegend().setEnabled(config.legend.enabled)

    seriesData.each { it.setXAxis(xAxis).setYAxis(yAxis) }

    def chart = hLib.Chart.newChart()
            .setTitle(chartTitle)
            .setSeries(*seriesData)
            .setPlotOptions(config.plotOptions)
            .setZoomType(hLib.ConstConfig.ZOOM_TYPES.XY)
            .setLegend(legend)

    return chart.getHighchartFormatDefinition()
}

def buildChart(String queryName, String chartSeriesTooltipName, String chartTitle, def categoryAttribute, String categoryName, def categoryId) {
    def dataForPerCategory = api.getElement(queryName)
    def data = prepareData(dataForPerCategory, chartSeriesTooltipName, categoryAttribute, categoryName, categoryId)
    def seriesData = data.seriesData
    def revenueValueForPlotLine = data.revenueValueForPlotLine
    def marginPercentValueForPlotLine = data.marginPercentValueForPlotLine
    def chartDefinition = getChartDefinition(seriesData, revenueValueForPlotLine, marginPercentValueForPlotLine, chartTitle)

    return api.buildHighchart(chartDefinition)
}

def findValueForPlotLineRevenue(List dataForPerCategory) {
    def ranking = getRanking(dataForPerCategory)
    if (!ranking) {
        return 0
    }
    def boundValue = calculateBoundValue(dataForPerCategory)
    def idx = getIndexOfBoundValue(boundValue, ranking)

    return ranking.getAt(idx - 1)
}

List getRanking(List dataForPerCategory) {
    return (dataForPerCategory?.sort(false, { a, b -> (a?.revenue) <=> (b?.revenue) })).revenue
}

int getIndexOfBoundValue(BigDecimal boundValue, ranking) {
    int idx = 0
    BigDecimal value = 0
    while (value < boundValue) {
        value += ranking.getAt(idx)
        idx++
    }

    return idx
}

BigDecimal calculateBoundValue(List dataForPerCategory) {
    BigDecimal plotFactor = Configuration.SETTINGS.scatterPlotPercent as BigDecimal ?: 0.0

    return ((dataForPerCategory?.sum { it?.revenue ?: 0 }) ?: 0) * plotFactor
}

def findValueForPlotLineMarginPercent(List dataForPerCategory) {
    BigDecimal plotFactor = Configuration.SETTINGS.scatterPlotPercent as BigDecimal ?: 0.0
    def boundValue = ((dataForPerCategory?.sum { it?.margin ?: 0 }) ?: 0) * plotFactor
    def ranking = dataForPerCategory?.sort(false, { a, b -> (a?.margin) <=> (b?.margin) })
    BigDecimal marginValue = 0
    BigDecimal revenueValue = 0
    for (int idx = 0; marginValue < boundValue; idx++) {
        marginValue += (ranking?.get(idx)?.margin)
        revenueValue += (ranking?.get(idx)?.revenue)
    }

    return (revenueValue == 0 ? 0 : (marginValue / revenueValue) * 100)
}

return null