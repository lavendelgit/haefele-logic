//TODO: This element needs descriptive documentation on the behaviour of buckets as stated in https://pricefx.atlassian.net/browse/PFPCS-1430
Map bucketSettings = Configuration.SETTINGS
Map bucketConfiguration = ConstConfig.BUCKETS_CONFIGURATION
int numberOfBuckets = bucketSettings.numberOfBuckets as int ?: bucketConfiguration.defaultNoOfBuckets
BigDecimal rangeStart = bucketSettings.bucketStartPercent as BigDecimal ?: bucketConfiguration.defaultStartPercent
BigDecimal rangeEnd = bucketSettings.bucketEndPercent as BigDecimal ?: bucketConfiguration.defaultEndPercent
BigDecimal bucketSize = (rangeEnd - rangeStart) / numberOfBuckets

List buckets = []
Map bucket
for (int i in 1..numberOfBuckets) {
    bucket = generateBucket(i, rangeStart, rangeEnd, bucketSize, numberOfBuckets, bucketConfiguration)
    buckets.add(bucket)
}

return buckets

Map generateBucket(int bucketIndex,
                   BigDecimal bucketStart,
                   BigDecimal bucketEnd,
                   BigDecimal bucketSize,
                   int numberOfBuckets,
                   Map bucketConfiguration) {
    BigDecimal previousBucketValue = bucketIndex == 1 ? bucketStart : bucketStart + (bucketIndex - 1) * bucketSize
    String previousBucketLabel = "${libs.SharedLib.RoundingUtils.round(previousBucketValue * 100, 2)}${bucketConfiguration.labelSuffix}"
    BigDecimal bucketValue = bucketIndex == numberOfBuckets ? bucketEnd : bucketStart + bucketIndex * bucketSize
    String bucketLabel = "${libs.SharedLib.RoundingUtils.round(bucketValue * 100, 2)}${bucketConfiguration.labelSuffix}"
    return [name : bucketConfiguration.namePrefix + bucketIndex,
            label: "${previousBucketLabel} - ${bucketLabel}",
            value: bucketValue]
}