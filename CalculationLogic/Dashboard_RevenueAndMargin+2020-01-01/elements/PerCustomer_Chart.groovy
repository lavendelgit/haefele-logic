String queryName = "QueryDataPerCustomers"
String chartTitle = "Customers Revenue and Margin %"
Map sqlConfiguration = Configuration.SQL_CONFIGURATION
def categoryName = sqlConfiguration.getAt("customerName")
def categoryId = sqlConfiguration.getAt("customerId")
def categoryAttribute = api.getElement("BandByForCustomer")
String chartSeriesTooltipName = "Customer"

return PerCategory_Commons.buildChart(queryName, chartSeriesTooltipName, chartTitle, categoryAttribute, categoryName, categoryId)