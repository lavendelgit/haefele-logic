def resultMatrix = api.newMatrix("Prodh. II","Month","Count of ZPAPs")
resultMatrix.setTitle("Count of ZPAPs per Prodh. II")
resultMatrix.setColumnFormat("Count of ZPAPs", FieldFormatType.NUMERIC)
resultMatrix.setPreferenceName("CountOfCustomerSalesPricesPerProductHierarchyPerMonth")
resultMatrix.setEnableClientFilter(true)

def data = api.getElement("Data")

for (row in data) {
    resultMatrix.addRow([
        "Prodh. II": row.prodH,
        "Month": row.month,
        "Count of ZPAPs": row.count
    ])
}

return resultMatrix
