def standardRabatt = api.getElement("StandardRabatt")
def regionalrabatt = api.getElement("Regionalrabatt")
def fokusrabatt = api.getElement("Fokusrabatt")

api.trace("standardRabatt", null, standardRabatt)
api.trace("regionalrabatt", null, regionalrabatt)
api.trace("fokusrabatt", null, fokusrabatt)

if (regionalrabatt != null) return regionalrabatt
if (fokusrabatt != null) return fokusrabatt
return standardRabatt