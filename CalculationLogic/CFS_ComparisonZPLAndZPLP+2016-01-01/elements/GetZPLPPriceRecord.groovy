if (api.isSyntaxCheck()) return

def sku = out.sku
//wrong attribute has been used for validfrom and validto
def validFrom = api.currentItem("attribute12")?.format("yyyy-MM-dd")
def validTo = api.currentItem("attribute13")?.format("yyyy-MM-dd")
api.trace("1",validFrom)
api.trace("2",validTo)
def filter = [Filter.equal("attribute7","EUR"),
              Filter.greaterOrEqual("attribute1",validFrom)]

if (validTo != "31/12/9999") {
    filter << Filter.lessOrEqual("attribute2",validTo)
}

def salesPriceRecord = lib.Find.latestSalesPriceRecord(sku,*filter)

if (salesPriceRecord) {
    return salesPriceRecord
}
