if (api.isSyntaxCheck()) {
	return
}

def match

def sku = out.sku
def VKORG = api.currentItem("attribute4")
def KOTAB = api.currentItem("attribute3")
def zplPrice = api.currentItem("attribute7")
def zplValidTo = api.currentItem("attribute13")?.format("yyyy-MM-dd")
def zplValidFrom = api.currentItem("attribute12")?.format("yyyy-MM-dd")
def ZZHDE_KTOKD = api.currentItem("attribute6")

def zplpPrice = out.GetZPLPPriceRecord?.attribute3
def zplpValidFrom = out.GetZPLPPriceRecord?.attribute1
def zplpValidTo = out.GetZPLPPriceRecord?.attribute2


match = (null in [zplpPrice, zplPrice]) ? "N/A" : ((zplpPrice == zplPrice) ? "Yes" : "No")

def row = [
        "lookupTableName": "TempCompZPLPvsZPL", //TempCompZPLPvsZPL
        "key1"           : sku,
        "key2"           : KOTAB,
        "key3"           : VKORG,
        "key4"           : ZZHDE_KTOKD ?: "*",
        "attribute1"     : zplpPrice,
        "attribute4"     : zplpValidFrom,
        "attribute5"     : zplpValidTo,
        "attribute2"     : zplPrice,
        "attribute6"     : zplValidFrom,
        "attribute7"     : zplValidTo,
        "attribute3"     : match
]
api.trace("1",row)
api.logInfo("*** row ***", row)
api.addOrUpdate("MLTV4", row)
