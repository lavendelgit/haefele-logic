def priceIncreaseAffectedSalesTypes = api.getElement("PriceIncreaseAffectedSalesTypes")

api.getElement("SalesHistory")
    ?.groupBy { r ->
        return priceIncreaseAffectedSalesTypes.contains(r.salesType) ? "affected" : "fixed"
    }
    ?.collectEntries { group, records ->
        return [(group) : records.inject([revenue: 0, unitsSold: 0, marginAbs: 0]) {
            result, record ->
                result.revenue += record.revenue ?: 0
                result.unitsSold += record.unitsSold ?: 0
                result.marginAbs += record.marginAbs ?: 0.0
                return result
        }]
    }