if (!api.global.salesTypes) {
    api.global.salesTypes = api.findLookupTableValues("SalesTypes")
                               .collectEntries { pp -> [ (pp.name) : [
                                   description: pp.attribute1,
                                   insideOutsie: pp.attribute2,
                                   priceIncreaseAffected: pp.attribute3
                               ]]}
}

api.trace("api.global.salesTypes","",api.global.salesTypes)

return api.global.salesTypes