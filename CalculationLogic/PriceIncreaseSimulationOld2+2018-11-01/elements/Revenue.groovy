def groupedSalesHistory = api.getElement("GroupedSalesHistory")

return (groupedSalesHistory.affected?.revenue ?: 0.0) +
       (groupedSalesHistory.fixed?.revenue ?: 0.0)