def revenue1 = api.getElement("Revenue1") as BigDecimal
def cost1 = api.getElement("Cost1") as BigDecimal

if (revenue1 == null) {
    api.redAlert("Revenue1")
    return
}

if (cost1 == null) {
    api.redAlert("Cost1")
    return
}


return revenue1 - cost1