def sku = api.product("sku")
def perfectlyElasticThreshold = api.getElement("PerfectlyElasticThreshold")

def elasticityValue = lib.Elasticity.find(sku)

return api.attributedResult(elasticityValue)
          .withBackgroundColor(lib.Elasticity.color(elasticityValue, perfectlyElasticThreshold))