if (!api.global.priceIncreaseAffectedSalesTypes) {
    api.global.priceIncreaseAffectedSalesTypes = api.global.salesTypes
                                                        .findAll { entry -> entry.value.priceIncreaseAffected == "Yes"}
                                                        .keySet()
}

api.global.priceIncreaseAffectedSalesTypes