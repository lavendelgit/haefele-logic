def groupedSalesHistory = api.getElement("GroupedSalesHistory")

return (groupedSalesHistory.affected?.marginAbs ?: 0.0) +
    (groupedSalesHistory.fixed?.marginAbs ?: 0.0)