def groupedSalesHistory = api.getElement("GroupedSalesHistory")
def salesPriceIncreasePerc = api.getElement("SalesPriceIncreasePerc");

api.trace("[Revenue2]", "salesPriceIncreasePerc", salesPriceIncreasePerc)

return (groupedSalesHistory.affected?.revenue ?: 0.0) * (1 + salesPriceIncreasePerc)