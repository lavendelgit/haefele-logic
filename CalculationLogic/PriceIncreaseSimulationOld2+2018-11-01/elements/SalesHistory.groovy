def sku = api.product("sku")

if (!api.global.salesHistory?.containsKey(sku)) {
    def currentYear = api.calendar().get(Calendar.YEAR);
    def previousYear = currentYear - 1
    def batchSkus = (api.getBatchInfo()?.collect{it[0]} ?: [sku]) as Set

    def filters = [
        Filter.in("Material", batchSkus),
        Filter.equal("InvoiceDateYear", previousYear)
    ]

    def dmCtx = api.getDatamartContext()
    def salesDM = dmCtx.getTable("TransactionsDM")
    def datamartQuery = dmCtx.newQuery(salesDM)

    datamartQuery.select("Material", "sku")
    datamartQuery.select("SalesType", "salesType")
    datamartQuery.select("SUM(Revenue)", "revenue")
    datamartQuery.select("SUM(Units)", "unitsSold")
    datamartQuery.select("SUM(MarginAbs)", "marginAbs")
    datamartQuery.where(filters)

    def result = dmCtx.executeQuery(datamartQuery)

    def records = result?.getData()?.collect()?.groupBy { r -> r.sku } ?: []

    api.global.salesHistory = batchSkus.collectEntries { s -> [s, records[s] ?: null] }

    api.trace("api.global.salesHistory", "", api.global.salesHistory)
}

return api.global.salesHistory[sku] ?: [:]