def groupedSalesHistory = api.getElement("GroupedSalesHistory")

return (groupedSalesHistory.affected?.unitsSold ?: 0) +
       (groupedSalesHistory.fixed?.unitsSold ?: 0)
