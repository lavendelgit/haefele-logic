def verkaufsbuero = "All"
def branchCategory = "All"

return api.dashboard("Dashboard_Embedded_RecommendCorrectCPG")
        .setParam("Verkaufsbuero", verkaufsbuero)
        .setParam("Branche Category", branchCategory)
        .showEmbedded()
        .andRecalculateOn(api.dashboardWideEvent("SalesOffBrancheChange-ED"))
        .withEventDataAttr("Verkaufsbuero").asParam("Verkaufsbuero")
        .withEventDataAttr("BrancheCategory").asParam("Branche Category")