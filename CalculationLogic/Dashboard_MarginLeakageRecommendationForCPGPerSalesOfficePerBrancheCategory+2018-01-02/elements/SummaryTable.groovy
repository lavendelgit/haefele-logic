import net.pricefx.common.api.FieldFormatType


def yearList = lib.GeneralUtility.getYearsListFromNow(3)
def columns = ['Verkaufsbuero', 'Branche Category', 'Number of Customers', 'Umsatz ' + yearList[2], 'Umsatz ' + yearList[1], 'Umsatz ' + yearList[0],
               'DB ' + yearList[2], 'DB ' + yearList[1], 'DB ' + yearList[0],]
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.NUMERIC, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR,
                   FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Umsatz & DB Distribution for Customers With Wrong Customer Potential Group',
                                           columns, columnTypes)

def summaryInfo = out.SummaryDataInformation
// Insert the first row with the All Options...
def aggregatedSummary = [0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
summaryInfo?.each {
    aggregatedSummary[0] += it['CustomerCount']
    aggregatedSummary[1] += it['Year3Revenue']
    aggregatedSummary[2] += it['Year2Revenue']
    aggregatedSummary[3] += it['Year1Revenue']
    aggregatedSummary[4] += it['Year3DB']
    aggregatedSummary[5] += it['Year2DB']
    aggregatedSummary[6] += it['Year1DB']
}
resultMatrix.setPreferenceName("MLRecToAWCPG_WrongCustPotGroup")
resultMatrix.addRow(['All',
                     'All',
                     dashUtl.boldHighlightWith(resultMatrix, aggregatedSummary[0]),
                     aggregatedSummary[1],
                     dashUtl.boldHighlightWith(resultMatrix, aggregatedSummary[2]),
                     aggregatedSummary[3],
                     aggregatedSummary[4],
                     dashUtl.boldHighlightWith(resultMatrix, aggregatedSummary[5]),
                     aggregatedSummary[6]])

summaryInfo?.each { it ->
    resultMatrix.addRow([
            (columns[0]): it['Verkaufsbereich3'],
            (columns[1]): it['BrancheCategory'],
            (columns[2]): dashUtl.boldHighlightWith(resultMatrix, it['CustomerCount']),
            (columns[3]): it['Year3Revenue'],
            (columns[4]): dashUtl.boldHighlightWith(resultMatrix, it['Year2Revenue']),
            (columns[5]): it['Year1Revenue'],
            (columns[6]): it['Year3DB'],
            (columns[7]): dashUtl.boldHighlightWith(resultMatrix, it['Year2DB']),
            (columns[8]): it['Year1DB']])
}
resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("SalesOffBrancheChange-ED")).withColValueAsEventDataAttr(columns[0], "Verkaufsbuero").withColValueAsEventDataAttr(columns[1], "BrancheCategory")

return resultMatrix

//api.formatNumber("€###,###,###.##", getNotNullRevenue(it['Year5Revenue'], it['SYear5Revenue']))