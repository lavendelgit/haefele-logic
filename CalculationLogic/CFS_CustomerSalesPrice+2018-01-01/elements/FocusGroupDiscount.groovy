package CFS_CustomerSalesPrice.elements

import com.googlecode.genericdao.search.Filter

import javax.annotation.processing.Filer

def salesOffice = api.getElement("SalesOffice")
def salesOfficeCode = api.findLookupTableValues("SalesOffices", Filter.equal("value", salesOffice))?.find()?.name

def focusGroup = api.productExtension("FocusGroup")?.find()?.attribute2

def discountGroup = api.getElement("CustomerDiscountGroup")
def targetDateFormatted = api.targetDate()?.format("yyyy-MM-dd")


api.trace("salesOffice", "", salesOffice)
api.trace("salesOfficeCode", "", salesOfficeCode)
api.trace("focusGroup", "", focusGroup)
api.trace("discountGroup", "", discountGroup)
api.trace("target",targetDateFormatted)
List filters = [ Filter.equal("attribute6", salesOfficeCode),
                 Filter.equal("attribute7", focusGroup),
                 Filter.equal("attribute8", discountGroup),
                 Filter.lessOrEqual("attribute1", targetDateFormatted),
                 Filter.greaterOrEqual("attribute2", targetDateFormatted)]

return api.find("PX50",Filter.equal("name","S_ZRPG"),*filters)?.find()?.attribute3