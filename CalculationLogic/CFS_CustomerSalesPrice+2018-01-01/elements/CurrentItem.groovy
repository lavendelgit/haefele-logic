package CFS_CustomerSalesPrice.elements

def currentItem = api.currentItem()
if (currentItem == null) {
    currentItem = (api.productExtension("S_ZPAP") ?: [[:]]).first() // only for test runs
}

return currentItem