package CFS_CustomerSalesPrice.elements

def customerSalesPrice = api.getElement("CurrentItem")?.attribute3// Sales Price (de: Verkaufspreis)

if (!customerSalesPrice) {
    api.redAlert("Sales Price is missing in PX CustomerSalesPrice")
    return null;
}

return customerSalesPrice