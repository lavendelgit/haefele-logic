package CFS_CustomerSalesPrice.elements

def discountPerc = api.getElement("DiscountPerc")
def maxDiscountPerc = lib.GeneralSettings.percentageValue("MaxDiscountPct")

if (discountPerc == null) {
    api.redAlert("DiscountPerc")
    return null
}

if (maxDiscountPerc == null) {
    api.redAlert("MaxDiscountPct")
    return null
}


if (discountPerc > maxDiscountPerc) {
    return api.attributedResult("MaxDiscountPct")
              .withBackgroundColor("red")
}

return null;

