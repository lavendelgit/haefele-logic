package CFS_CustomerSalesPrice.elements

def salesPrice = api.getElement("SalesPrice")
def customerSalesPrice = api.getElement("CustomerSalesPrice")

if (salesPrice == null) {
    api.redAlert("SalesPrice")
    return null
}

if (customerSalesPrice == null) {
    api.redAlert("CustomerSalesPrice")
    return null
}

return salesPrice - customerSalesPrice