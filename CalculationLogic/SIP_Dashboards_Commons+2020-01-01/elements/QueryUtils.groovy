import net.pricefx.server.dto.calculation.ResultMatrix

Map generateBaseWaterfallQuery(String dmName, List<Map> waterfallColumns) {
    return [datamartName: dmName,
            rollup      : true,
            fields      : getWaterfallFields(waterfallColumns)]
}

Map prepareWaterfallFiltersDataSet(Map sqlConfiguration, def productsDataSet, def customersDataSet, def dateFromDataSet, def dateToDataSet) {
    String productIDFieldName = sqlConfiguration.productId
    String customerIDFieldName = sqlConfiguration.customerId
    String transactionDate = sqlConfiguration.pricingDate

    return [customerProductFilterData: [products: productsDataSet, customers: customersDataSet, productId: productIDFieldName, customerId: customerIDFieldName],
            dateFilterData           : [dateFrom: dateFromDataSet, dateTo: dateToDataSet, pricingDate: transactionDate]]
}

List getWaterfallQueryResults(String dmName, List<Map> waterfallColumns, String targetCurrencyCode, Map customerProductFilterData, Map dateFilterData, Filter genericFilter) {
    def queryDef = generateBaseWaterfallQuery(dmName, waterfallColumns)
    def customerProductFilters = generateCustomerProductFilters(customerProductFilterData.products,
            customerProductFilterData.customers,
            customerProductFilterData.productId,
            customerProductFilterData.customerId)
    def dateFilters = generateDateFromToFilters(dateFilterData.dateFrom,
            dateFilterData.dateTo,
            dateFilterData.pricingDate)

    queryDef.whereFilters = [*customerProductFilters,
                             *dateFilters,
                             genericFilter]


    queryDef.targetCurrencyCode = targetCurrencyCode

    return libs.HighchartsLibrary.QueryModule.queryDatamart(queryDef)
}

protected Map getWaterfallFields(List waterfallColumns) {
    return waterfallColumns.collectEntries { column -> populateWaterfallFields(column) }
}

protected Map populateWaterfallFields(def column) {
    return column.isSum ? [(column.name): column.name] : getSubElementsStructure(column)
}

protected Map getSubElementsStructure(def column) {
    List elements = column.elements

    if (!elements) {
        return [(column.name): column.name]
    }

    return elements.collectEntries { element -> [(element.name): element.name] }
}


List generateCustomerProductFilters(def productInputElement, def customerInputElement, String productIdColumn, String customerIdColumn) {
    List productIdList = convertProductGroupInputToProductIds(productInputElement)
    List customerIdList = convertCustomerGroupInputToCustomerIds(customerInputElement)

    return [productInputElement ? Filter.in(productIdColumn, productIdList) : null,
            customerInputElement ? Filter.in(customerIdColumn, customerIdList) : null]
}

List generateDateFromToFilters(String dateFromInputElement, String dateToInputElement, String transactionDateColumn) {
    return [dateFromInputElement ? Filter.greaterOrEqual(transactionDateColumn, dateFromInputElement) : null,
            dateToInputElement ? Filter.lessOrEqual(transactionDateColumn, dateToInputElement) : null]
}

List convertProductGroupInputToProductIds(def productGroup) {
    return getFieldsBasedOnInput("P", "sku", productGroup)
}

List convertCustomerGroupInputToCustomerIds(def customerGroup) {
    return getFieldsBasedOnInput("C", "customerId", customerGroup)
}

Map sumQueryResult(List<Map> query) {
    if (!query) {
        return [:]
    }

    return query.getAt(0).keySet().inject([:]) { result, key -> result << [(key): query.getAt(key).sum()] }
}

/**
 * @param queryResult
 * @param matrixDefinition - map contains column labels as keys and column type as values [columnName : columnType]
 * columnName - keys in map are String
 * columnType : values in map are net.pricefx.common.api.FieldFormatType
 * @return ResultMatrix formatted by matrixDefinition.
 */
def createResultMatrix(List queryResult, Map matrixDefinition) {
    if (!queryResult) {
        return new ResultMatrix()
    }

    List columnNames = matrixDefinition.keySet().toList() ?: queryResult?.getAt(0).keySet().toList()
    ResultMatrix table = new ResultMatrix(columnNames)
    matrixDefinition.each { entry -> table.setColumnFormat(entry.key, entry.value) }
    queryResult.each { Map row -> table.addRow(row.values().toList()) }

    return table
}

protected List getFieldsBasedOnInput(String lookupType, String lookupField, def input) {
    if (input == null || !input?.asFilter()) {
        return
    }

    int startRow = 0
    List masterDataElements = [], potentialElements = []

    while (masterDataElements = api.find(lookupType, startRow, api.getMaxFindResultsLimit(), lookupField, [lookupField], input?.asFilter())) {
        startRow += masterDataElements.size()
        potentialElements.addAll(masterDataElements)
    }

    return potentialElements.getAt(lookupField)
}

def generateBreakdownDashboardPeriodTable(Map baseQueryDefinition,
                                          Map queryFieldKeys,
                                          def periodElement,
                                          Map sqlConfiguration) {
    String pricingDateColumnName = sqlConfiguration.pricingDate

    def dmSlice = generateDatamartPeriodSlices(pricingDateColumnName, periodElement)

    Map<String, String> fields = [productId                      : "${sqlConfiguration.productId}",
                                  customerId                     : "${sqlConfiguration.customerId}",
                                  (queryFieldKeys.revenue)       : "SUM(${sqlConfiguration.invoicePrice})",
                                  (queryFieldKeys.margin)        : "SUM(${sqlConfiguration.grossMargin})",
                                  (queryFieldKeys.volume)        : "SUM(${sqlConfiguration.quantity})",
                                  (queryFieldKeys.revenuePerUnit): "SUM(${sqlConfiguration.invoicePrice}) / SUM(${sqlConfiguration.quantity})",
                                  (queryFieldKeys.marginPerUnit) : "SUM(${sqlConfiguration.grossMargin}) / SUM(${sqlConfiguration.quantity})",
                                  (queryFieldKeys.costsPerUnit)  : "SUM(${sqlConfiguration.costs}) / SUM(${sqlConfiguration.quantity})"]

    baseQueryDefinition.fields = fields

    libs.HighchartsLibrary.QueryModule.createInMemoryTables(baseQueryDefinition, dmSlice)
}

void generateBreakdownDashboardInMemoryTables(def productElement,
                                              def customerElement,
                                              def periodElement,
                                              def comparisonPeriodElement,
                                              String targetCurrencyCode,
                                              Map sqlConfiguration) {

    def commonsQueryUtils = libs.SIP_Dashboards_Commons.QueryUtils
    Map baseQueryDef = [datamartName      : "${sqlConfiguration.datamartName}",
                        rollup            : true,
                        whereFilters      : [*commonsQueryUtils.generateCustomerProductFilters(productElement, customerElement, sqlConfiguration.productId, sqlConfiguration.customerId),
                                             Filter.isNotNull(sqlConfiguration.grossMargin),
                                             Filter.isNotNull(sqlConfiguration.invoicePrice),
                                             Filter.isNotNull(sqlConfiguration.quantity)
                        ],
                        targetCurrencyCode: targetCurrencyCode]
    Map t1Fields = [revenue       : "revenue1",
                    margin        : "margin1",
                    volume        : "volume1",
                    revenuePerUnit: "revenuePerUnit1",
                    marginPerUnit : "marginPerUnit1",
                    costsPerUnit  : "costsPerUnit1"]

    generateBreakdownDashboardPeriodTable(baseQueryDef, t1Fields, comparisonPeriodElement, sqlConfiguration)

    Map t2Fields = [revenue       : "revenue2",
                    margin        : "margin2",
                    volume        : "volume2",
                    revenuePerUnit: "revenuePerUnit2",
                    marginPerUnit : "marginPerUnit2",
                    costsPerUnit  : "costsPerUnit2"]

    generateBreakdownDashboardPeriodTable(baseQueryDef, t2Fields, periodElement, sqlConfiguration)
}

protected def generateDatamartPeriodSlices(String pricingDateColumnName, def ... periods) {
    def datamartContext = api.getDatamartContext()

    return datamartContext.newDatamartSlice(pricingDateColumnName, periods)
}

Map getBreakdownDashboardSQLQueries() {
    return [T1Revenue     : """SELECT SUM(revenue1) AS SUM 
                               FROM T1 
                               WHERE T1.volume1 <> 0""",
            T2Revenue     : """SELECT SUM(revenue2) AS SUM 
                               FROM T2
                               WHERE T2.volume2 <> 0""",
            T1Margin      : """SELECT SUM(margin1) AS SUM 
                               FROM T1 
                               WHERE T1.volume1 <> 0""",
            T2Margin      : """SELECT SUM(margin2) AS SUM 
                               FROM T2
                               WHERE T2.volume2 <> 0""",
            commonBusiness: """SELECT * 
                               FROM T1 
                               INNER JOIN T2 
                               ON T1.productId=T2.productId AND T1.customerId=T2.customerId
                               WHERE T1.volume1 <> 0 AND T2.volume2 <> 0""",
            lostBusiness  : """SELECT T1.* 
                               FROM T1 
                               LEFT OUTER JOIN T2 
                               ON T1.productId=T2.productId AND T1.customerId=T2.customerId
                               WHERE T2.productId IS NULL AND T1.volume1 <> 0""",
            newBusiness   : """SELECT T2.* 
                               FROM T1
                               RIGHT OUTER JOIN T2 
                               ON T1.productId=T2.productId AND T1.customerId=T2.customerId
                               WHERE T1.productId IS NULL AND T2.volume2 <> 0"""]
}