List readWaterfallConfiguration() {
    List highLevel = getHighLevelData()

    if (!highLevel) {
        api.throwException("No high level data defined, cannot proceed")
    }

    List subLevel = getSubLevelData()
    Map columnAttributes = libs.SIP_Dashboards_Commons.ConstConfig.WATERFALL_COLUMN_ATTRIBUTES
    List sortedColumns = highLevel.sort { columnA, columnB -> return columnA.getAt(columnAttributes.order) <=> columnB.getAt(columnAttributes.order) }
    Map groupedSubLevel = subLevel?.groupBy { it.getAt(columnAttributes.subLevelParentElementName) }

    return getWaterfallColumns(sortedColumns, groupedSubLevel)
}

protected List getWaterfallColumns(List sortedColumns, Map groupedSubLevel) {
    boolean isPercentBase
    boolean isSubtract
    boolean isSum
    List subElements
    List waterfallColumns = []

    for (column in sortedColumns) {
        Map currentColumnData = [:]
        isSum = isTrue(column.attribute3)
        isSubtract = isTrue(column.attribute6)

        if (isSum && !isSubtract) {
            isPercentBase = isTrue(column.attribute4)
            currentColumnData = getWaterfallColumnStructure(column, null, isSum, null, isPercentBase)
        } else if (isSubtract && !isSum) {
            subElements = getSubLevel(groupedSubLevel?.getAt(column.name))
            currentColumnData = getWaterfallColumnStructure(column, subElements, null, isSubtract)
        } else if (isSubtract && isSum) {
            api.throwException("Column ${column?.name} marked as \"isSum\" and \"isSubtract\"")
        } else {
            subElements = getSubLevel(groupedSubLevel?.getAt(column.name))
            currentColumnData = getWaterfallColumnStructure(column, subElements)
        }
        waterfallColumns.add(currentColumnData)
    }

    return waterfallColumns
}

protected List getSubLevelData() {
    def configuration = libs.SIP_Dashboards_Commons.ConstConfig
    Filter subLevelFilters = getNotEmptyFilter(configuration.WATERFALL_DISABLE_ATTRIBUTE.subLevel)

    return api.findLookupTableValues(configuration.WATERFALL_PP_NAMES.subLevel, subLevelFilters)
}

protected List getHighLevelData() {
    def configuration = libs.SIP_Dashboards_Commons.ConstConfig
    Filter highLevelFilters = getNotEmptyFilter(configuration.WATERFALL_DISABLE_ATTRIBUTE.highLevel)

    return api.findLookupTableValues(configuration.WATERFALL_PP_NAMES.highLevel, highLevelFilters)
}

protected Filter getNotEmptyFilter(String attribute) {
    return Filter.or(
            Filter.isEmpty(attribute),
            Filter.notEqual(attribute, "Yes")
    )
}

protected Map getWaterfallColumnStructure(Map column,
                                          List subElements,
                                          Boolean isSum = null,
                                          Boolean isSubtract = null,
                                          Boolean isPercentBase = null) {

    Map columnAttributes = libs.SIP_Dashboards_Commons.ConstConfig.WATERFALL_COLUMN_ATTRIBUTES
    Map structure = [name : column.getAt(columnAttributes.name),
                     label: column.getAt(columnAttributes.label),
                     order: column.getAt(columnAttributes.order)]

    if (subElements != null) {
        structure << [elements: subElements]
    }

    if (isSum != null) {
        structure << [isSum: isSum]
    }

    if (isSubtract != null) {
        structure << [isSubtract: isSubtract]
    }

    if (isPercentBase != null) {
        structure << [isPercentBase: isPercentBase]
    }

    return structure
}

protected boolean isTrue(String value) {
    return "YES".equalsIgnoreCase(value)
}

protected List getSubLevel(List<Map> waterfallColumns) {
    if (!waterfallColumns) {
        return
    }

    List sortedColumns = waterfallColumns?.sort { columnA, columnB -> return columnA.attribute2 <=> columnB.attribute2 }
    Map columnAttributes = libs.SIP_Dashboards_Commons.ConstConfig.WATERFALL_COLUMN_ATTRIBUTES

    return sortedColumns.collect { column ->
        [name : column?.getAt(columnAttributes.name),
         label: column?.getAt(columnAttributes.label),
         order: column?.getAt(columnAttributes.order)]
    }
}