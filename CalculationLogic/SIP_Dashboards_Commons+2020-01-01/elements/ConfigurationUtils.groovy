Map getAdvancedConfiguration(Map currentConfiguration, String configurationName) {
    String advancedConfigurationString = getAdvancedConfigurationString(configurationName)

    return advancedConfigurationString ? appendAdvancedConfiguration(advancedConfigurationString, currentConfiguration) : currentConfiguration
}

protected Map appendAdvancedConfiguration(String advancedConfigurationString, Map currentConfiguration) {
    def advancedConfigurationObj = api.jsonDecode(advancedConfigurationString)
    currentConfiguration << advancedConfigurationObj

    return currentConfiguration
}

protected String getAdvancedConfigurationString(String configurationName) {
    def advancedConfiguration = api.find("AP", Filter.equal("uniqueName", configurationName))

    return advancedConfiguration?.getAt(0)?.value
}

Map getCommonsAdvancedConfiguration() {
    String configurationName = libs.SIP_Dashboards_Commons.ConstConfig.COMMONS_ADVANCED_CONFIGURATION_NAME

    return libs.SharedLib.CacheUtils.getOrSet(configurationName, [configurationName] as List, { String _configurationName ->
        return getAdvancedConfiguration([:], _configurationName)
    })
}