def createConfiguratorEntry(def inputType, String name, def value) {
    def configuratorEntry = api.createConfiguratorEntry(inputType, name)
    configuratorEntry?.getFirstInput()?.setValueOptions(value)

    return configuratorEntry
}

void setOptionDefaultValue(String optionName, def defaultValue) {
    def param = api.getParameter(optionName)
    if (param != null && param?.getValue() == null) {
        param.setValue(defaultValue)
    }
}

void setConfiguratorEntryDefaultValue(def configuratorEntry, def defaultValue){
    if (configuratorEntry != null && configuratorEntry?.getFirstInput()?.getValue() == null) {
        configuratorEntry.getFirstInput().setValue(defaultValue)
    }
}

String getCurrentQuarter() {
    int currentMonth = Calendar.getInstance().get(Calendar.MONTH)

    return "Q${(currentMonth / 4 as int) + 1}"
}