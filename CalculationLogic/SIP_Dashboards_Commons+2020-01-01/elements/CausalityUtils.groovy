import groovy.transform.Field


@Field CHART_NAMES = [net     : "Net",
                      gross   : "Gross",
                      averages: "Averages",
                      mostUsed: "Most Used"]

@Field REVENUE_BREAKDOWN_SQL = """SELECT
                                    ROUND(PERIOD1.IP) AS 'oldRevenue',
                                    ROUND(COMMON_DATA.VolumeEffect) AS 'volumeEffect',
                                    ROUND(COMMON_DATA.PriceEffect) AS 'priceEffect',
                                    ROUND(COMMON_DATA.MixEffect) AS 'mixEffect',
                                    ROUND(-LOST_DATA.IP) AS 'lostBusiness',
                                    ROUND(NEW_DATA.IP) AS 'newBusiness',
                                    ROUND(PERIOD2.IP) AS 'newRevenue'
                                    FROM (SELECT
                                          SUM((T2.IPpu - T1.IPpu)*T2.V) AS PriceEffect,
                                          (SUM(T2.V)-SUM(T1.V)) * (SUM(T1.IP) / SUM(T1.V)) AS VolumeEffect,
                                          SUM(T1.IPpu * (T2.V/TOTAL.T2V - T1.V/TOTAL.T1V)) * SUM(T2.V) AS MixEffect
                                          FROM T2
                                              INNER JOIN T1 ON T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId,
                                            (SELECT SUM(T1.V) AS T1V, SUM(T2.V) AS T2V
                                              FROM T2 INNER JOIN T1 ON
                                                T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId) TOTAL) COMMON_DATA,
                                        (SELECT SUM(T1.IP) AS ip FROM T1
                                            LEFT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T2.ProductId IS NULL) LOST_DATA,
                                        (SELECT SUM(T2.IP) AS ip FROM T1
                                            RIGHT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T1.ProductId IS NULL) NEW_DATA,
                                        (SELECT SUM(T1.IP) IP FROM T1) PERIOD1,
                                        (SELECT SUM(T2.IP) IP FROM T2) PERIOD2
                                 """

@Field MARGIN_BREAKDOWN_NET_SQL = """SELECT
                                        ROUND(PERIOD1.Margin) AS 'period1Margin',
                                        ROUND(COMMON_DATA.VolumeEffect) AS 'volumeEffect',
                                        ROUND(COMMON_DATA.PriceEffect) AS 'priceEffect',
                                        ROUND(COMMON_DATA.CostEffect) AS 'costEffect',
                                        ROUND(COMMON_DATA.MixEffect) AS 'mixEffect',
                                        ROUND(-LOST_DATA.Margin) AS 'lostBusiness',
                                        ROUND(NEW_DATA.Margin) AS 'newBusiness',
                                        ROUND(PERIOD2.Margin) AS 'period2Margin'
                                        FROM (SELECT
                                                  SUM((T2.InvoicePricePerUnit - T1.InvoicePricePerUnit) * T1.Volume) AS PriceEffect,
                                                  SUM((T2.Volume - T1.Volume) * T1.MarginPerUnit) AS VolumeEffect,
                                                  SUM((T2.CostPerUnit - T1.CostPerUnit) * T1.Volume) AS CostEffect,
                                                  SUM((T2.Volume/TOTAL.T2Volume - T1.Volume/TOTAL.T1Volume) * (T1.MarginPerUnit - (T1.Margin/TOTAL.T1Volume))) * SUM(T1.Volume) AS MixEffect
                                                  FROM T2
                                                     INNER JOIN T1 ON T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId,
                                                     (SELECT SUM(T1.Volume) AS T1Volume, SUM(T2.Volume) AS T2Volume
                                                      FROM T2 INNER JOIN T1 ON
                                                       T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId) TOTAL) COMMON_DATA,
                                              (SELECT SUM(T1.Margin) AS Margin FROM T1
                                                LEFT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T2.ProductId IS NULL) LOST_DATA,
                                              (SELECT SUM(T2.Margin) AS Margin FROM T1
                                               RIGHT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T1.ProductId IS NULL) NEW_DATA,
                                              (SELECT SUM(T1.Margin) Margin FROM T1) PERIOD1,
                                              (SELECT SUM(T2.Margin) Margin FROM T2) PERIOD2
                                 """

@Field MARGIN_BREAKDOWN_GROSS_SQL = """SELECT
                                            ROUND(PERIOD1.Margin) AS 'period1Margin',
                                            ROUND(COMMON_DATA.VolumeEffect) AS 'volumeEffect',
                                            ROUND(COMMON_DATA.PriceEffect) AS 'priceEffect',
                                            ROUND(COMMON_DATA.CostEffect) AS 'costEffect',
                                            ROUND(COMMON_DATA.MixEffect) AS 'mixEffect',
                                            ROUND(-LOST_DATA.Margin) AS 'lostBusiness',
                                            ROUND(NEW_DATA.Margin) AS 'newBusiness',
                                            ROUND(PERIOD2.Margin) AS 'period2Margin'
                                            FROM (SELECT
                                                      SUM((T2.InvoicePricePerUnit - T1.InvoicePricePerUnit) * T2.Volume) AS PriceEffect,
                                                      SUM((T2.Volume - T1.Volume) * T2.MarginPerUnit) AS VolumeEffect,
                                                      SUM((T2.CostPerUnit - T1.CostPerUnit) * T2.Volume) AS CostEffect,
                                                      SUM((T2.Volume/TOTAL.T2Volume - T1.Volume/TOTAL.T1Volume) * (T2.MarginPerUnit - (T2.Margin/TOTAL.T2Volume))) * SUM(T2.Volume) AS MixEffect
                                                      FROM T2
                                                        INNER JOIN T1 ON T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId,
                                                        (SELECT SUM(T1.Volume) AS T1Volume, SUM(T2.Volume) AS T2Volume
                                                          FROM T2 INNER JOIN T1 ON
                                                           T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId) TOTAL) COMMON_DATA,
                                                  (SELECT SUM(T1.Margin) AS Margin FROM T1
                                                    LEFT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T2.ProductId IS NULL) LOST_DATA,
                                                  (SELECT SUM(T2.Margin) AS Margin FROM T1
                                                    RIGHT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T1.ProductId IS NULL) NEW_DATA,
                                                  (SELECT SUM(T1.Margin) Margin FROM T1) PERIOD1,
                                                  (SELECT SUM(T2.Margin) Margin FROM T2) PERIOD2
                                 """
@Field MARGIN_BREAKDOWN_AVERAGES_SQL = """SELECT
                                            ROUND(PERIOD1.Margin) AS 'period1Margin',
                                            ROUND(COMMON_DATA.VolumeEffect) AS 'volumeEffect',
                                            ROUND(COMMON_DATA.PriceEffect) AS 'priceEffect',
                                            ROUND(COMMON_DATA.CostEffect) AS 'costEffect',
                                            ROUND(COMMON_DATA.MixEffect) AS 'mixEffect',
                                            ROUND(-LOST_DATA.Margin) AS 'lostBusiness',
                                            ROUND(NEW_DATA.Margin) AS 'newBusiness',
                                            ROUND(PERIOD2.Margin) AS 'period2Margin'
                                            FROM (
                                                  SELECT
                                                      SUM((T2.InvoicePricePerUnit - T1.InvoicePricePerUnit) * ((T1.Volume+T2.Volume) / 2)) AS PriceEffect,
                                                      SUM((T2.Volume - T1.Volume) * ((T1.MarginPerUnit + T2.MarginPerUnit) / 2)) AS VolumeEffect,
                                                      SUM((T2.CostPerUnit - T1.CostPerUnit) * ((T1.Volume+T2.Volume)/2)) AS CostEffect,
                                                      SUM((T2.Volume/TOTAL.T2Volume - T1.Volume/TOTAL.T1Volume) *
                                                            ((T2.MarginPerUnit - (T2.Margin/TOTAL.T2Volume)) + (T1.MarginPerUnit - (T1.Margin/TOTAL.T1Volume))) / 2) * (SUM(T1.Volume) + SUM(T2.Volume)) / 2 AS MixEffect
                                                      FROM T2
                                                         INNER JOIN T1 ON T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId,
                                                         (SELECT SUM(T1.Volume) AS T1Volume, SUM(T2.Volume) AS T2Volume
                                                          FROM T2 INNER JOIN T1 ON
                                                           T1.ProductId = T2. ProductId and T1.CustomerId = T2.CustomerId) TOTAL) COMMON_DATA,
                                                  (SELECT SUM(T1.Margin) AS Margin FROM T1
                                                    LEFT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T2.ProductId IS NULL) LOST_DATA,
                                                  (SELECT SUM(T2.Margin) AS Margin FROM T1
                                                    RIGHT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T1.ProductId IS NULL) NEW_DATA,
                                                  (SELECT SUM(T1.Margin) Margin FROM T1) PERIOD1,
                                                  (SELECT SUM(T2.Margin) Margin FROM T2) PERIOD2
                                 """

@Field MARGIN_BREAKDOWN_MOSTUSED_SQL = """SELECT
                                            ROUND(PERIOD1.Margin) AS 'period1Margin',
                                            ROUND(COMMON_DATA.VolumeEffect) AS 'volumeEffect',
                                            ROUND(COMMON_DATA.PriceEffect) AS 'priceEffect',
                                            ROUND(COMMON_DATA.CostEffect) AS 'costEffect',
                                            ROUND(COMMON_DATA.MixEffect) AS 'mixEffect',
                                            ROUND(-LOST_DATA.Margin) AS 'lostBusiness',
                                            ROUND(NEW_DATA.Margin) AS 'newBusiness',
                                            ROUND(PERIOD2.Margin) AS 'period2Margin'
                                            FROM (
                                                  SELECT
                                                      SUM((T2.InvoicePricePerUnit - T1.InvoicePricePerUnit) * T2.Volume) AS PriceEffect,
                                                      SUM((T2.Volume - T1.Volume) * T1.MarginPerUnit) - SUM((T2.Volume/TOTAL.T2Volume - T1.Volume/TOTAL.T1Volume) * (T1.MarginPerUnit - (T1.Margin/TOTAL.T1Volume))) * SUM(T2.Volume) AS VolumeEffect,
                                                      SUM((T2.CostPerUnit - T1.CostPerUnit) * T2.Volume) AS CostEffect,
                                                      SUM((T2.Volume/TOTAL.T2Volume - T1.Volume/TOTAL.T1Volume) * (T1.MarginPerUnit - (T1.Margin/TOTAL.T1Volume))) * SUM(T2.Volume) AS MixEffect
                                                      FROM T2
                                                          INNER JOIN T1 ON T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId,
                                                          (SELECT SUM(T1.Volume) AS T1Volume, SUM(T2.Volume) AS T2Volume
                                                              FROM T2 INNER JOIN T1 ON
                                                               T1.ProductId = T2. ProductId AND T1.CustomerId = T2.CustomerId) TOTAL) COMMON_DATA,
                                                  (SELECT SUM(T1.Margin) AS Margin FROM T1
                                                      LEFT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T2.ProductId IS NULL) LOST_DATA,
                                                  (SELECT SUM(T2.Margin) AS Margin FROM T1
                                                      RIGHT OUTER JOIN T2 ON T1.ProductId=T2.ProductId AND T1.CustomerId=T2.CustomerId WHERE T1.ProductId IS NULL) NEW_DATA,
                                                  (SELECT SUM(T1.Margin) Margin FROM T1) PERIOD1,
                                                  (SELECT SUM(T2.Margin) Margin FROM T2) PERIOD2
                                 """

/**
 * Function to get calculated revenue causality for selected products, customers and chosen time periods
 * @param productElement
 * @param customerElement
 * @param periodElement
 * @param comparisonPeriodElement
 * @param targetCurrencyCode
 * @param sqlConfiguration
 * @return
 */
Map getRevenueBreakdownData(def productElement,
                            def customerElement,
                            def periodElement,
                            def comparisonPeriodElement,
                            String targetCurrencyCode,
                            Map sqlConfiguration,
                            Filter genericFilter) {
    def dmCtx = api.getDatamartContext()
    def datamart = dmCtx.getDatamart(sqlConfiguration.datamartName)

    def baseQueryDef = dmCtx.newQuery(datamart, true)
    baseQueryDef.select(sqlConfiguration.productId, "productId")
    baseQueryDef.select(sqlConfiguration.customerId, "customerId")
    baseQueryDef.select("SUM(${sqlConfiguration.invoicePrice})", "IP")
    baseQueryDef.select("SUM(${sqlConfiguration.quantity})", "V")
    baseQueryDef.select("SUM(${sqlConfiguration.invoicePrice}) / SUM(${sqlConfiguration.quantity})", "IPpu")

    baseQueryDef.where(productElement)
    baseQueryDef.where(customerElement)

    baseQueryDef.where(Filter.and(
            Filter.isNotNull(sqlConfiguration.invoicePrice),
            Filter.isNotNull(sqlConfiguration.quantity)))

    baseQueryDef.having(Filter.greaterThan("IP", 0),
            Filter.greaterThan("V", 0))

    if (genericFilter) {
        baseQueryDef.where(genericFilter)
    }

    def query1 = dmCtx.newQuery(baseQueryDef)

    def period1startDate = comparisonPeriodElement.getStartDate()
    def period1endDate = comparisonPeriodElement.getEndDate()
    query1.where(Filter.and(Filter.lessOrEqual(sqlConfiguration.pricingDate, period1endDate), Filter.greaterOrEqual(sqlConfiguration.pricingDate, period1startDate)))
    query1.setOptions([currency: targetCurrencyCode])

    def period2startDate = periodElement.getStartDate()
    def period2endDate = periodElement.getEndDate()
    def query2 = dmCtx.newQuery(baseQueryDef)
    query2.where(Filter.and(Filter.lessOrEqual(sqlConfiguration.pricingDate, period2endDate), Filter.greaterOrEqual(sqlConfiguration.pricingDate, period2startDate)))
    query2.setOptions([currency: targetCurrencyCode])

    return dmCtx.executeSqlQuery(REVENUE_BREAKDOWN_SQL, query1, query2)?.getAt(0)
}

/**
 * Function to get calculated margin causality for selected products, customers, chosen time periods and model
 * @param productElement
 * @param customerElement
 * @param periodElement
 * @param comparisonPeriodElement
 * @param targetCurrencyCode
 * @param sqlConfiguration
 * @param model
 * @return - Map with data with available value fields [period1margin, volumeeffect, priceefect, costeffect, mixeffect, lostbusiness, newbusiness, period2margin]
 */
Map getMarginBreakdownData(def productElement,
                           def customerElement,
                           def periodElement,
                           def comparisonPeriodElement,
                           String targetCurrencyCode,
                           Map sqlConfiguration,
                           String model,
                           Filter genericFilter) {

    def dmCtx = api.getDatamartContext()
    def datamart = dmCtx.getDatamart(sqlConfiguration.datamartName)

    def baseQueryDef = dmCtx.newQuery(datamart, true)
    baseQueryDef.select(sqlConfiguration.productId, "productId")
    baseQueryDef.select(sqlConfiguration.customerId, "customerId")
    baseQueryDef.select("SUM(${sqlConfiguration.grossMargin})", "margin")
    baseQueryDef.select("SUM(${sqlConfiguration.quantity})", "'volume'")
    baseQueryDef.select("SUM(${sqlConfiguration.invoicePrice}) / SUM(${sqlConfiguration.quantity})", "invoicePricePerUnit")
    baseQueryDef.select("SUM(${sqlConfiguration.grossMargin}) / SUM(${sqlConfiguration.quantity})", "marginPerUnit")
    baseQueryDef.select("SUM(${sqlConfiguration.costs}) / SUM(${sqlConfiguration.quantity})", "costPerUnit")

    baseQueryDef.where(productElement)
    baseQueryDef.where(customerElement)

    baseQueryDef.where(Filter.and(
            Filter.isNotNull(sqlConfiguration.grossMargin),
            Filter.isNotNull(sqlConfiguration.invoicePrice),
            Filter.isNotNull(sqlConfiguration.quantity)))

    baseQueryDef.having(Filter.greaterThan("'volume'", 0))

    if (genericFilter) {
        baseQueryDef.where(genericFilter)
    }

    def query1 = dmCtx.newQuery(baseQueryDef)
    def period1startDate = comparisonPeriodElement.getStartDate()
    def period1endDate = comparisonPeriodElement.getEndDate()
    query1.where(Filter.and(Filter.lessOrEqual(sqlConfiguration.pricingDate, period1endDate), Filter.greaterOrEqual(sqlConfiguration.pricingDate, period1startDate)))
    query1.setOptions([currency: targetCurrencyCode])

    def query2 = dmCtx.newQuery(baseQueryDef)
    def period2startDate = periodElement.getStartDate()
    def period2endDate = periodElement.getEndDate()
    query2.where(Filter.and(Filter.lessOrEqual(sqlConfiguration.pricingDate, period2endDate), Filter.greaterOrEqual(sqlConfiguration.pricingDate, period2startDate)))
    query2.setOptions([currency: targetCurrencyCode])

    def causalityUtils = libs.SIP_Dashboards_Commons.CausalityUtils

    switch (model) {
        case causalityUtils.CHART_NAMES.gross:
            return dmCtx.executeSqlQuery(MARGIN_BREAKDOWN_GROSS_SQL, query1, query2)?.getAt(0)
        case causalityUtils.CHART_NAMES.averages:
            return dmCtx.executeSqlQuery(MARGIN_BREAKDOWN_AVERAGES_SQL, query1, query2)?.getAt(0)
        case causalityUtils.CHART_NAMES.mostUsed:
            return dmCtx.executeSqlQuery(MARGIN_BREAKDOWN_MOSTUSED_SQL, query1, query2)?.getAt(0)
        default:
            return dmCtx.executeSqlQuery(MARGIN_BREAKDOWN_NET_SQL, query1, query2)?.getAt(0)
    }
}