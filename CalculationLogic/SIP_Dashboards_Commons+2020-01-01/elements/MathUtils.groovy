BigDecimal getPercentageRatio(BigDecimal value, BigDecimal percentageBase) {
    return value != null && percentageBase ? 100 * value / percentageBase : null
}