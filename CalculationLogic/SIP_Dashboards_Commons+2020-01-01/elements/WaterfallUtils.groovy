Map prepareAbsoluteWaterfallData(Map rawChartData, List waterfallColumns, Map colors, String drilldownName, boolean isDrilldown) {
    Closure valueCalculation = { valueData -> return valueData }
    return prepareWaterfallData(rawChartData, waterfallColumns, colors, drilldownName, isDrilldown, valueCalculation)
}

Map preparePercentageWaterfallData(Map rawChartData, List waterfallColumns, Map colors, String drilldownName, boolean isDrilldown) {
    Map denominatorColumn = getDenominatorColumn(waterfallColumns)
    def denominatorValue = rawChartData?.getAt(denominatorColumn?.name)
    Closure valueCalculation = denominatorValue ? { value -> return libs.SIP_Dashboards_Commons.MathUtils.getPercentageRatio(value, denominatorValue) } : null

    return prepareWaterfallData(rawChartData, waterfallColumns, colors, drilldownName, isDrilldown, valueCalculation)
}

def getDenominatorColumn(List waterfallColumns) {
    return waterfallColumns?.find { it.isPercentBase == true }
}

protected Map prepareWaterfallData(Map rawChartData, List waterfallColumns, Map colors, String drilldownName, boolean isDrilldown, Closure valueCalculation) {
    List mainSeriesData = []
    List drilldownData = []
    if (waterfallColumns && rawChartData.getAt(waterfallColumns[0]?.name) && valueCalculation) {
        mainSeriesData = generateHighLevelData(rawChartData, waterfallColumns, colors, isDrilldown, drilldownName, valueCalculation)
        if (isDrilldown) {
            drilldownData = generateDrilldownData(rawChartData, waterfallColumns, colors, drilldownName, valueCalculation)
        }
    }

    return [mainSeriesData: mainSeriesData,
            drilldownData : drilldownData]
}

protected List generateHighLevelData(Map dataForChart, List waterfallColumns, Map colors, Boolean isSubColumnDrilldown, String drilldownName, Closure valueCalculation) {
    boolean isSum = waterfallColumns[0].isSum ?: false

    return generateLevelData(dataForChart, waterfallColumns, isSum, isSubColumnDrilldown, colors, drilldownName, valueCalculation)
}

protected List generateDrilldownData(Map dataForChart, List waterfallColumns, Map colors, String drilldownName, Closure valueCalculationClosure) {
    List drilldownData = []
    Map columnDrilldownData

    waterfallColumns.eachWithIndex { currentColumn, index ->
        columnDrilldownData = getColumnDrilldownData(dataForChart, currentColumn, waterfallColumns, index, colors, drilldownName, valueCalculationClosure)
        if (columnDrilldownData) {
            drilldownData << columnDrilldownData
        }
    }

    return drilldownData
}

protected List generateLevelData(Map dataForChart, List waterfallColumns, boolean isFirstColumnSum, boolean isSubColumnDrilldown, Map colors, String drilldownName, Closure valueCalculation) {
    def predefinedColor, value
    List levelData = []
    boolean firstSumAlreadyMet = !isFirstColumnSum

    for (Map column : waterfallColumns) {
        if (column.elements && !column.isSum) {
            if (isSubColumnDrilldown) {
                List costs = dataForChart.getAt(column.name)
                BigDecimal costsSum = costs?.sum { it.value }
                value = valueCalculation.call(costsSum)
                levelData += getColumnDataStructure(column.label, value, null, colors, column.name + drilldownName)
            } else {
                for (element in dataForChart.getAt(column.name)) {
                    levelData += getColumnDataStructure(element.label, element.value, null, colors)
                }
            }
        } else {
            value = valueCalculation.call(dataForChart.getAt(column.name))
            predefinedColor = column.isSum ? colors.price : null
            levelData += getColumnDataStructure(column.label, value, predefinedColor, colors, null, firstSumAlreadyMet && column.isSum)
            if (!firstSumAlreadyMet && column.isSum) {
                firstSumAlreadyMet = true
            }
        }
    }

    return levelData
}

protected Map getColumnDrilldownData(Map dataForChart, Map currentColumn, List waterfallColumns, int index, Map colors, String drilldownName, Closure valueCalculationClosure) {
    Map drilldownData = [:]
    Integer previousIsSumColumnIndex, nextSumColumnIndex
    def color, value
    List subData, levelData

    subData = []
    if (!currentColumn.isSum) {
        previousIsSumColumnIndex = 0
        nextSumColumnIndex = waterfallColumns?.size() - 1
        levelData = generateLevelData(dataForChart, waterfallColumns.subList(previousIsSumColumnIndex, index), true, true, colors, drilldownName, valueCalculationClosure)
        subData.addAll(levelData)

        if (currentColumn.elements) {
            dataForChart?.getAt(currentColumn.name).each { it ->
                value = valueCalculationClosure.call(it.value)
                subData += getColumnDataStructure(it.label, value, null, colors)
            }
        } else {
            value = valueCalculationClosure.call(dataForChart?.getAt(currentColumn.name))
            subData += getColumnDataStructure(currentColumn.label, value, color, colors)
        }
        levelData = generateLevelData(dataForChart, waterfallColumns.subList(index + 1, nextSumColumnIndex + 1), false, true, colors, drilldownName, valueCalculationClosure)
        subData.addAll(levelData)
        drilldownData = [name: drilldownName,
                         id  : currentColumn.name + drilldownName,
                         data: subData]
    }

    return drilldownData
}

protected Map getColumnDataStructure(String name, BigDecimal value, String color, Map colors, String drilldownId = null, Boolean isSum = null) {
    Map structure = [name : name,
                     y    : value,
                     color: color]

    if (!color) {
        structure << [color: getColor(value, colors)]
    }

    if (drilldownId) {
        structure << [drilldown: drilldownId]
    }

    if (isSum) {
        structure << [isSum: isSum]
    }

    return structure
}

protected String getColor(BigDecimal value, Map colors) {
    return (value > 0 ? colors.positive : colors.negative)
}