Map getDatamartCurrencyData(String datamartName) {
    def dmCurrency = getDatamartCurrencyCode(datamartName)
    return [currencyCode  : dmCurrency,
            currencySymbol: getCurrencySymbol(dmCurrency)]
}

String getDatamartCurrencyCode(String datamartName) {
    return api.find("DM", Filter.equal("uniqueName", datamartName))?.getAt(0)?.baseCcyCode
}

String getCurrencySymbol(String currencyCode) {
    return currencyCode ? api.vLookup(libs.SIP_Dashboards_Commons.ConstConfig.CURRENCY_SYMBOL_PP_NAME, currencyCode) : ""
}

Map getDatamartCurrencyDataWithFormat(String format, String datamartName) {
    def currencyData = getDatamartCurrencyData(datamartName)
    currencyData.currencyFormat = getFormatWithCurrencySymbol(format, currencyData.currencySymbol)
    return currencyData
}

String getFormatWithCurrencySymbol(String format, String currencySymbol) {
    return "${format} ${currencySymbol}"
}

def getCurrencyUserEntry(String datamartName) {
    def dmCurrencyCode = getDatamartCurrencyCode(datamartName)
    def currentDate = Calendar.getInstance().time
    Filter validityFilter = Filter.and(Filter.and(Filter.lessThan("CcyValidFrom", currentDate),
            Filter.equal("CcyFrom", dmCurrencyCode),
            Filter.greaterOrEqual("CcyValidTo", currentDate)))
    def configuration = libs.SIP_Dashboards_Commons.ConfigurationUtils.getCommonsAdvancedConfiguration()

    return api.datamartFilterBuilderUserEntry("Select currency", configuration.ccyDSName, ["CcyTo"], validityFilter)
}

Map getCurrencyDataFromUserFilter(Filter currencyFilter, String datamartName) {
    if (isDatamartFilterBuilderEntryFilterInvalid(currencyFilter))
        return getDatamartCurrencyData(datamartName)

    def configuration = libs.SIP_Dashboards_Commons.ConfigurationUtils.getCommonsAdvancedConfiguration()
    Map currencyQuery = [datamartName: configuration.ccyDSName,
                         fields      : ["currencyTo": "CcyTo"],
                         whereFilters: currencyFilter,
                         maxRows     : 1]

    String currencyCode = libs.HighchartsLibrary.QueryModule.queryDatasource(currencyQuery)?.getAt(0).currencyTo

    return [currencyCode  : currencyCode,
            currencySymbol: getCurrencySymbol(currencyCode)]
}

protected boolean isDatamartFilterBuilderEntryFilterInvalid(Filter filter) {
    return (!filter || filter.valuesAsList.size() < 2 || filter.valuesAsList.getAt(1).operator == Filter.OP_NULL)
}