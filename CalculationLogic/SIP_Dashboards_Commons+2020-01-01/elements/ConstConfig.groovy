import groovy.transform.Field

@Field WATERFALL_PP_NAMES = [highLevel: "PFXTemplate_DB_Waterfall_HighLevel",
                             subLevel : "PFXTemplate_DB_Waterfall_SubLevel"]

@Field WATERFALL_DISABLE_ATTRIBUTE = [highLevel: "attribute5",
                                      subLevel : "attribute4"]

@Field WATERFALL_COLUMN_ATTRIBUTES = [name                     : "name",
                                      label                    : "attribute1",
                                      order                    : "attribute2",
                                      subLevelParentElementName: "attribute3"]

@Field WATERFALL_COLUMN_COLORS = [base     : [price   : "#0080FF",
                                              negative: "#BF4040",
                                              positive: "#00FF00"],
                                  secondary: [price   : "#1f2a8d",
                                              negative: "#dd0074",
                                              positive: "#99da00"]]

@Field CURRENCY_SYMBOL_PP_NAME = "CurrencySymbols"

@Field DASHBOARDS_ADVANCED_CONFIGURATION_NAME = "SIP_AdvancedConfiguration"

@Field COMMONS_ADVANCED_CONFIGURATION_NAME = "SIP_Commons_AdvancedConfiguration"