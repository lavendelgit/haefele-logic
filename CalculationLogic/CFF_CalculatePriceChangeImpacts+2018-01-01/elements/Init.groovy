def todaysDate = api.targetDate()
def cal = Calendar.getInstance()

if (!cal)
    return []

cal.setTime(todaysDate)
cal.add(Calendar.MONTH, -1)

def lastMonth = cal.get(Calendar.MONTH) + 1
lastMonth = (lastMonth < 10) ? "0" + lastMonth : lastMonth
def lastMonthDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
api.local.PGDataToProcess = [
        '2019': ['PGName': '2019:PP Efficiency Investigation', 'Year': '2019', 'Range': '01/01/2019 to 31/12/2019'],
        '2020': ['PGName': '2020:PP Efficiency Investigation', 'Year': '2020', 'Range': "01/01/2020 to $lastMonthDay/$lastMonth/2020"]
]
