package CFF_RunProductSkuLengthCFS.elements

actionBuilder
        .addCalculatedFieldSetAction('Products Article Number length')
        .setCalculate(true)