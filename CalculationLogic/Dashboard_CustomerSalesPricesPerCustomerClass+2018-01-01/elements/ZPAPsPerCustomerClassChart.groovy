package Dashboard_CustomerSalesPricesPerCustomerClass.elements

def zpapsPerCustomerClass = api.getElement("ZPAPsPerCustomerClass")

def categories = zpapsPerCustomerClass.collect { e -> e.customerClass}
def data = zpapsPerCustomerClass.collect { e -> e.count};

def chartDef = [
        chart: [
            type: 'column'
        ],
        title: [
            text: 'ZPAPs per Customer Class'
        ],
        xAxis: [[
            title: [
                    text: "Customer Class"
            ],
            categories: categories
        ]],
        yAxis: [[
            title: [
                text: "Number of ZPAPs"
            ]
        ]],
        tooltip: [
            headerFormat: 'Customer Class: {point.key}<br/>',
            pointFormat: 'Number of ZPAPs: {point.y}'
        ],
        legend: [
            enabled: false
        ],
        series: [[
             data: data,
             dataLabels: [
                 enabled: true,
             ]
        ]]
]

api.buildFlexChart(chartDef)