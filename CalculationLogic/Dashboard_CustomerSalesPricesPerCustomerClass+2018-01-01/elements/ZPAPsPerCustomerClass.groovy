package Dashboard_CustomerSalesPricesPerCustomerClass.elements

def targetDate = api.targetDate()?.format("yyyy-MM-dd")

def dmCtx = api.getDatamartContext()
def dmQuery = dmCtx.newQuery(dmCtx.getDatamart("CustomerSalesPricesDM"))
Filter salesOfficeFilter = libs.__LIBRARY__.HaefeleSpecific.getSalesOfficeInlandFilters('Verkaufsbereich3')
def filters = [Filter.lessOrEqual("ValidFrom", targetDate),
               Filter.greaterOrEqual("ValidTo", targetDate),
               salesOfficeFilter]

if (api.isUserInGroup("ProductGroupManager", api.user().loginName)) {
    def pgm = api.findLookupTableValues("ProductGroupManager")?.find()?.value
    filters.add(Filter.equal("SM", pgm))
}

dmQuery.select("KundenABCKlasse", "customerClass")
dmQuery.select("COUNT(Material)", "count")
dmQuery.where(*filters)
dmQuery.having(Filter.greaterThan("count", 0))

return dmCtx.executeQuery(dmQuery)?.getData()
            ?.collect { r ->
                r.customerClass = r.customerClass ?: "Undefined"
                return r
            }
            ?.groupBy { r -> r.customerClass }
            ?.collect { e -> [ customerClass: e.key, count: e.value.count.sum() ]} // sum Undefined from null and Undefined from empty string
            ?.sort { r1, r2 -> r1.customerClass <=> r2.customerClass }

