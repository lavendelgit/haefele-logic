package Dashboard_CustomerSalesPricesPerCustomerClass.elements

def resultMatrix = api.newMatrix("Customer Class","Count of ZPAPs")
resultMatrix.setTitle("Number of ZPAPs per Customer Class - table")
resultMatrix.setPreferenceName("MLZperCC_Table")
resultMatrix.setColumnFormat("Count of ZPAPs", FieldFormatType.NUMERIC)

def zpapsPerCustomerClass = api.getElement("ZPAPsPerCustomerClass")

def total = 0;

for (row in zpapsPerCustomerClass) {
    resultMatrix.addRow([
            "Customer Class" : row.customerClass,
            "Count of ZPAPs" : row.count
    ])
    total += row.count;
}

resultMatrix.addRow([
        "Customer Class": resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of ZPAPs": resultMatrix.styledCell(total).withCSS("font-weight: bold")
])


return resultMatrix
