def yearMonth = api.getElement("InvoiceDate")

if (yearMonth) {
  yearMonth = yearMonth.format("yyyy")
}

return yearMonth