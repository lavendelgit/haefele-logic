/**
 * This element will be collapsing the ranges into few range based on following condition.
 * startDate to endDate - Purchase Record & sales record
 *   skip all records with date as null......
 *   identify the purchase price value and sales price value, cur sales price difference and purchase price difference
 *      possible combination: P - Not Null & Same PChange as previous &&  S - Not Null && Same SChange
 *                            P - Not Null & Same PChange as previous, S - Null
 *                            P - Null, S - Not Null & same value continues.
 *    Output: ListOfDateRanges i.e. Start Date, End Date, Purchase Price Details, Sales Price Details on yearly basis as
 *    for Haefele the year starts in January and end is December or respective year as per requirement we need to have
 *    details from January per year.
 */
if (api.local.skipProcessing)
    return []

def createRecord(curStartDate, curEndDate, previousRecord) {
    def validTo = new Date().parse("yyyy-MM-dd HH:mm:ss", curEndDate.format("yyyy-MM-dd") + " 23:59:59")
    return [startDate: curStartDate, endDate: validTo, purchaseRecord: previousRecord?.purchaseRecord, salesRecord: previousRecord?.salesRecord, transactionCount: 0, transactionUnits: 0, pTransactionValue: new BigDecimal(0.0), sTransactionValue: new BigDecimal(0.0), tTransactionValue: new BigDecimal(0.0)]
}

def mergedMap = api.local.mergedPurchaseSalesMap
def currentYearRecordsToProcess
def currentYear
def purchaseChange = 0, salesChange = 0
def curStartDate
def previousRecord
def previousRecordMatchCase
def outputDateRanges = []
def recordNotNull = false

api.trace("The inputs are clear", "Size of Map " + mergedMap.keySet().size())

mergedMap.each { key, value ->
    //initialize parameters
    currentYear = key
    currentYearRecordsToProcess = value
    curStartDate = null
    purchaseChange = salesChange = 0
    previousRecordMatchCase = 0
    //Iterate over the year values and find the various combinations
    api.trace("The first is", " Key " + currentYear + " currentYearRecordsToProcess " + currentYearRecordsToProcess)
    currentYearRecordsToProcess.each { currentRecord ->
        if (currentRecord.date && (currentRecord.purchaseRecord || currentRecord.salesRecord)) {
            recordNotNull = true
            if (!curStartDate) {
                curStartDate = currentRecord.date
                purchaseChange = currentRecord.purchaseRecord?.priceChange
                salesChange = currentRecord.salesRecord?.priceChange
                previousRecordMatchCase = (currentRecord.purchaseRecord && currentRecord.salesRecord) ? 1 :
                        (currentRecord.purchaseRecord && !currentRecord.salesRecord) ? 2 : 3
            } else {
                if (currentRecord.purchaseRecord && currentRecord.salesRecord) { // P - Not Null AND S - Not Null
                    /* if purchase price or sales price is changing OR previously this condition was not there then add previous record & start new record.
                    */
                    if (currentRecord.purchaseRecord.priceChange != purchaseChange || previousRecordMatchCase != 1 ||
                            currentRecord.salesRecord.priceChange != salesChange) {
                        outputDateRanges.add(createRecord(curStartDate, currentRecord.date.minus(1), previousRecord))
                        curStartDate = currentRecord.date
                        purchaseChange = currentRecord.purchaseRecord.priceChange
                        salesChange = currentRecord.salesRecord.priceChange
                    }
                    previousRecordMatchCase = 1
                } else if (currentRecord.purchaseRecord && !currentRecord.salesRecord) { // P - Not Null AND S - Null
                    /* Since sales Record does not exist, check if purchase price is changing or if previously it was different case then add a previous record & start a new
                       record
                     */
                    if (currentRecord.purchaseRecord.priceChange != purchaseChange || previousRecordMatchCase != 2) {
                        outputDateRanges.add(createRecord(curStartDate, currentRecord.date.minus(1), previousRecord))
                        curStartDate = currentRecord.date
                        purchaseChange = currentRecord.purchaseRecord.priceChange
                    }
                    previousRecordMatchCase = 2
                } else {//P - Null, S - Not Null
                    /* Since purchase Record does not exist, check if sales price is changing or if previously it was different case then add a previous record & start a new
                       record
                     */
                    if (currentRecord.salesRecord.priceChange != salesChange || previousRecordMatchCase != 3) {
                        api.trace("The Purchase is null, Sales is not null", " Record " + currentRecord)
                        outputDateRanges.add(createRecord(curStartDate, currentRecord.date.minus(1), previousRecord))
                        curStartDate = currentRecord.date
                        salesChange = currentRecord.salesRecord.priceChange
                    }
                    previousRecordMatchCase = 3
                }
            }
            previousRecord = currentRecord
        }
    }
    if (recordNotNull)
        outputDateRanges.add(createRecord(curStartDate, previousRecord.date, previousRecord))
    recordNotNull = false
}

return outputDateRanges