if (api.local.skipProcessing)
    return 0

def sku = api.getElement("Material")
def salesPriceDetails = api.getElement("MaterialSalesPrices")
int salesPricesCount = (salesPriceDetails) ? salesPriceDetails.size() : 0

if (!salesPricesCount || salesPricesCount == 0) {
    api.trace("Skip Purchase Price Population", "Skipping purchase Price calculation")
    return false
}

int i = 0
def currentPriceRecord
def yearOfCurrentPriceRecord
api.trace("Printing Input", "salesPricesCount" + salesPricesCount + " salesPriceDetails " + salesPriceDetails)
while (i < salesPricesCount) {
    currentPriceRecord = salesPriceDetails.get(i)
    api.trace("Record by Record Processing", " currentPriceRecord " + i + " Values (" + currentPriceRecord)
    MergedMapUtil.populateSalesData(currentPriceRecord)
    i++
}

api.trace("Printing Output", "salesPricesCount" + salesPricesCount + " Merged Records " + api.local.mergedPurchaseSalesMap)

return salesPricesCount