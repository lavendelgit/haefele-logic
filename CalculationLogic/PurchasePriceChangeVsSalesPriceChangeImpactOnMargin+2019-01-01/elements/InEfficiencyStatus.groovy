if (api.local.skipProcessing)
    return "Skipped Article"

def efficiencyStatus = api.getElement("ConsolidatePurchasePriceImpact")
return efficiencyStatus.InEfficiencyStatus