/**
 * This is a Util class dedicated for fetching the sales prices for the articles from the specified Sales Table. All the product extention tables which fetches
 * data for different types of sales prices follow same DB structure and hence this is a common util which can be reused.
 */

def fetchSalesPricesForArticles (String salesTable) {
    def startDt = api.getElement("SupplierPurchaseConfigStartDate")
    def endDt = api.getElement("SupplierPurchaseConfigEndDate")
    def salesPriceDetails = [:]
    def attributes = [
            "sku",
            "attribute1",
            "attribute2",
            "attribute3",
            "attribute5",
            "attribute6"
    ]

    //Populate the map with the details
    def fmFromFilterDate = startDt.format("yyyy-MM-dd")
    def fmToFilterDate = endDt.format("yyyy-MM-dd")
    def fromDateIfEarlierThanStartToDateShouldOverlap = Filter.and(Filter.lessOrEqual("attribute1", fmFromFilterDate),
                Filter.greaterOrEqual("attribute2", fmFromFilterDate))
    def fromDateIfFurtherThanStartShouldOverlap = Filter.and(Filter.greaterThan("attribute1", fmFromFilterDate),
                Filter.lessOrEqual("attribute1", fmToFilterDate))
    def filters = [
            Filter.equal("name", salesTable),
            Filter.or(fromDateIfEarlierThanStartToDateShouldOverlap,
                    fromDateIfFurtherThanStartShouldOverlap
            )
    ]
    def stream = api.stream("PX", "attribute1", attributes, *filters)
    if (!stream) {
        api.trace("No Data Found in $salesTable Stream", "Sorry.........")
        return [:]
    }
    int count = 0
    def record
    def articleSalesPrices
    def currentSPRec
    while (stream.hasNext()) {
        record = stream.next()
        count++
        articleSalesPrices = salesPriceDetails[record.sku] ?: []
        currentSPRec = [
                sku             : record.sku,
                pricePerQuantity: (record.attribute6) ? Integer.parseInt(record.attribute6) : 1,
                validFrom       : (record.attribute1) ? java.util.Date.parse("yyyy-MM-dd", record.attribute1) : "",
                validTo         : (record.attribute2) ? java.util.Date.parse("yyyy-MM-dd", record.attribute2) : "",
                price           : (record.attribute3) ? record.attribute3 : 0.0,
                currency        : record.attribute5,
                priceChange     : -1
        ]
        if (currentSPRec.validFrom) {
            if (currentSPRec.validFrom < startDt)
                currentSPRec.validFrom = startDt
        }
        articleSalesPrices.add(currentSPRec)
        salesPriceDetails[record.sku] = articleSalesPrices
    }
    stream.close()
    api.trace("ZPZ SalesPricesForAllArticles", " Rows Fetchted(" + salesPriceDetails?.size() + ") count (" + count + ")")

    return salesPriceDetails
}
