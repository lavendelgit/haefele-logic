def userStartDateEntry = api.dateUserEntry("Supplier Price Change lookup Start Date")
if (!userStartDateEntry) {
  def userEndDateEntry = api.getElement("SupplierPurchaseConfigEndDate")
  def yearStart = userEndDateEntry.format("yyyy") + "-01-01"
  userStartDateEntry = Date.parse ("yyyy-MM-dd", yearStart)
} else {
  if (!(userStartDateEntry instanceof Date)) {
    	userStartDateEntry = Date.parse ("yyyy-MM-dd", userStartDateEntry)
  }
} 


return userStartDateEntry