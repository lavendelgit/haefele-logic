def purchasePriceCurrentValues = api.getElement("MaterialPurchasePrices")
def compoundArticle = api.getElement("isCompoundElement")
def isNonCompoundArticle = (compoundArticle)?false:true

return ((isNonCompoundArticle) && (purchasePriceCurrentValues && purchasePriceCurrentValues != null && purchasePriceCurrentValues?.size() > 1))
