/**
 * This element iterates over the merged map and get the transaction data to compute the output unit that will be populated and made available
 * for knowning the exact impact of the change in Purchase Price Increase on Sales Price Increase.
 * @Input TransactionSummaryPerArticlePerDay
 * @Output
 */
if (api.local.skipProcessing)
    return []

def sku = api.product("sku")
def dmCtx = api.getDatamartContext()
def table = dmCtx.getDataSource("TransactionSummaryPerArticlePerDay")
def query = dmCtx.newQuery(table)

def startDate = api.getElement("SupplierPurchaseConfigStartDate")
def endDate = api.getElement("SupplierPurchaseConfigEndDate")

def filters = [
        Filter.equal("Material", sku),
        Filter.greaterOrEqual("InvoiceDate", startDate.format("yyyy-MM-dd")),
        Filter.lessOrEqual("InvoiceDate", endDate.format("yyyy-MM-dd"))
]

query.select("InvoiceDate", "TransactionDate")
query.select("p1_Units", "Units")
query.select("p2_Revenue", "Revenue")
query.where(*filters)

dmCtx.executeQuery(query)?.getData()?.collect()