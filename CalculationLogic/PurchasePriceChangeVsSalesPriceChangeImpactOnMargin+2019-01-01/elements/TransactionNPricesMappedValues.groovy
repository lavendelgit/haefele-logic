/**
 *  This element is about merging the transactions found during the investigation time along with the various buckets made for tracking the purchase price and
 *  sales price changes. And during merging compute the purchase price change & sales price change impact on that particular bucket period. By Merging we will
 *  get overall idea of how Purchase Price Rise is compensated by the respective Sales Price Rise if at all its done. Otherwise it will indicate an opportunity
 *  for increasing revenue.
 */
if (api.local.skipProcessing)
    return []

def transactions = api.getElement("TransactionSummary")
def purchasePriceNSalePriceEntriesRange = api.getElement("MergePurchasePriceNSalesPriceChangePeriods")
def transactionDate
def mapEntry
def missingPurchaseNSalesDetails = []

def getPurchasePriceInEuros(purchaseRecord, transElement) {
    if (!(purchaseRecord) || !(purchaseRecord.priceChange)) {
        return 0.0
    }
    def purchasePrice = purchaseRecord.priceChange
    return CurrencyConvertion.getConvertedValues(purchaseRecord.currency, api.local.targetCurrency, transElement.TransactionDate, purchasePrice)
}

def getSalesPriceInEuros(salesRecord, transElement) {
    if (!(salesRecord) || !(salesRecord.priceChange)) {
        return 0.0
    }
    def salesPrice = salesRecord.priceChange
    return CurrencyConvertion.getConvertedValues(salesRecord.currency, api.local.targetCurrency, transElement.TransactionDate, salesPrice)
}

def populateMapEntry(mapEntry, transElement) {
    def units = (transElement.Units) ? transElement.Units : 0
    def revenue = (transElement.Revenue) ? transElement.Revenue : 0.0
    def purchasePrice = getPurchasePriceInEuros(mapEntry.purchaseRecord, transElement)
    def salesPrice = getSalesPriceInEuros(mapEntry.salesRecord, transElement)
    mapEntry.transactionCount++
    mapEntry.transactionUnits += units
    mapEntry.tTransactionValue += revenue
    mapEntry.pTransactionValue += units * purchasePrice
    mapEntry.sTransactionValue += units * salesPrice
}

transactions.each { transElement ->
//    api.trace ("Printing what we are getting out... ", transElement.toString() +" transElement ")
    transactionDate = transElement.TransactionDate
    mapEntry = purchasePriceNSalePriceEntriesRange.find { it.startDate <= transactionDate && transactionDate <= it.endDate }
    if (mapEntry) {
        populateMapEntry(mapEntry, transElement)
    } else {
        missingPurchaseNSalesDetails.add(transactionDate)
    }
}

if (missingPurchaseNSalesDetails.size() > 1) {
    api.yellowAlert("The entry for following transactionDates (" + missingPurchaseNSalesDetails + ") is missing in computed sales and price range ")
}

return purchasePriceNSalePriceEntriesRange