def userEndDateEntry = api.dateUserEntry("Supplier Price Change lookup End Date")
if (!userEndDateEntry) {
	userEndDateEntry = new Date()
} else {
  if (!(userEndDateEntry instanceof Date)) {
    	userEndDateEntry = Date.parse ("yyyy-MM-dd", userEndDateEntry)
  }
} 
return userEndDateEntry