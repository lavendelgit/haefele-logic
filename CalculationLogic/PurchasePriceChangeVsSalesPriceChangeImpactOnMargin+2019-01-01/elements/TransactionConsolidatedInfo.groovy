def computeEfficiencyCategory() {
    def ppChange = out.PurchasePriceInefficiency
    def spChange = out.SalesPriceEfficiency
    if (api.local.salesPriceSource == "No Data" && (ppChange && ppChange > 0))
        return 'PPIncreaseNoInfoOnSP'
    if (ppChange == null || spChange == null)
        return "NotApplicable"
    if (ppChange > 0.0 && spChange > 0.0 && ppChange > spChange)
        return 'PPSPIncreaseButPPMoreThanSP'
    if (ppChange > 0.0 && spChange < 0.0)
        return 'PPIncreaseSPDecrease'
    if (ppChange > 0.0 && spChange == 0.0)
        return 'PPIncreaseSPNoChange'
    if (spChange < 0.0 && ppChange == 0.0)
        return 'SPDecreasedButNoChangeInPP'
    if (ppChange < 0.0 && spChange < 0.0 && ppChange > spChange)
        return 'PPSPDecreasedButSPDecreasedMoreThanPP'

    return "Irrelevant"
}

def computedPurchaseSalesRecords = api.getElement("TransactionNPricesMappedValues")
def transactionDetails = ['TotalTransactions'      : 0,
                          'RelevantTransactions'   : 0,
                          'TotalTransactedUnits'   : 0,
                          'RelevantTransactedUnits': 0,
                          'EfficiencySubCategory'  : 'NotApplicable']

def ppChanged, spChanged
computedPurchaseSalesRecords.each { currentPeriodRecord ->
    lib.TraceUtility.developmentTraceRow("Table details", currentPeriodRecord)
    ppChanged = currentPeriodRecord.purchaseRecord?.priceChange
    spChanged = currentPeriodRecord.salesRecord?.priceChange

    transactionDetails['TotalTransactions'] += currentPeriodRecord.transactionCount
    transactionDetails['TotalTransactedUnits'] += currentPeriodRecord.transactionUnits
    if ((ppChanged != null || spChanged != null) && (ppChanged != 0.0 || spChanged != 0.0)) {
        transactionDetails['RelevantTransactions'] += currentPeriodRecord.transactionCount
        transactionDetails['RelevantTransactedUnits'] += currentPeriodRecord.transactionUnits
    }
}

transactionDetails['EfficiencySubCategory'] = computeEfficiencyCategory()
api.local.consolidatedTransactionData = transactionDetails
return transactionDetails['TotalTransactions']