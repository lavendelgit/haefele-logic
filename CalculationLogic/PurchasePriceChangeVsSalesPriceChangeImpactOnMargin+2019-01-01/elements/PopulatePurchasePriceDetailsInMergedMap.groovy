/**
 *
 */
if (api.local.skipProcessing)
    return 0

def sku = api.getElement("Material")
def purchasePriceForCurrentMaterial = api.global.purchasePriceDetails[sku]
int purchasePricesCount = (purchasePriceForCurrentMaterial) ? purchasePriceForCurrentMaterial.size() : 0

if (!purchasePricesCount || purchasePricesCount == 0) {
    api.trace("Skip Purchase Price Population", "Skipping purchase Price calculation")
    return false
}

int i = 0
def currentPriceRecord
def yearOfCurrentPriceRecord
api.trace("Printing Input", "purchasePricesCount" + purchasePricesCount + " purchasePriceForCurrentMaterial " + purchasePriceForCurrentMaterial)
while (i < purchasePricesCount) {
    currentPriceRecord = purchasePriceForCurrentMaterial.get(i)
    api.trace ("Record by Record Processing", " currentPriceRecord " + i + " Values (" + currentPriceRecord)
    MergedMapUtil.populatePurchaseData(currentPriceRecord)
    i++
}

api.trace("Printing Input", "purchasePricesCount" + purchasePricesCount + " Merged Records " + api.local.mergedPurchaseSalesMap)

return purchasePricesCount