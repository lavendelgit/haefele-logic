/** Based on the user selection or last updated record, find the sales price changes between the date Start date and end Date for the
 *  current selected product/sku. Note that ZPLP is given priority over ZPZ while finding the sales prices.
 */
if (api.local.skipProcessing) {
    api.local.salesPriceSource = "Not Evaluated"
    return api.local.NoDataPrice
}

if (!api.global.zplpSalesPriceDetails) {
    api.global.zplpSalesPriceDetails = SalesPriceFetchUtil.fetchSalesPricesForArticles("SalesPrice")
}

def sku = api.getElement("Material")
def listOfSalesPriceChangesForMaterial = api.global.zplpSalesPriceDetails[sku]
api.local.salesPriceSource = "ZPLP"
if (!listOfSalesPriceChangesForMaterial) {
    listOfSalesPriceChangesForMaterial = api.getElement("ZPZSalesPrice")
    api.trace ("ZPZ lucky change", " "+ listOfSalesPriceChangesForMaterial)
    api.local.salesPriceSource = (listOfSalesPriceChangesForMaterial)? "ZPZ" : "No Data"
}
api.trace("SalesPricesForAArticle", " Article(" + sku + ") SalesPrices (" + listOfSalesPriceChangesForMaterial + ")")
return listOfSalesPriceChangesForMaterial