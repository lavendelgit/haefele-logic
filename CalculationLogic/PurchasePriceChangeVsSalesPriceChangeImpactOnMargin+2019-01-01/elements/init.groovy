api.retainGlobal = true

api.local.NoDataPrice = "No Data"
api.local.NoChangePrice = "No Change"
api.local.ValueChangedPrice = "Changed"

api.local.NoDataOnSalesPrice = "No SP Data"
api.local.NoDataOnPurchasePrice = "No PP Data"

api.local.skipProcessing=false

api.local.targetCurrency="EUR"

api.local.salesPriceSource = "Not Evaluated"