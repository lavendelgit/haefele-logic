/**
 * This util element takes care of streamlining the input data fetched and populating the difference compared to the beginning of the period of data.
 * In streamlining if the start date is before the analysis start window, it will reset the start date to analysis start date.
 * Also sometimes the end date is by default always 31st Dec 9999 for all intervals. This logic corrects end date by resetting it to the next interval's
 * i.e. (start date - 1).
 */

def computePriceChangeNCorrectEndDate(pricesForCurrentArticle, startDt, endDt) {
    def pricesCount = pricesForCurrentArticle?.size()
    if (pricesCount == 1) {
        pricesForCurrentArticle[0].priceChange = 0
        api.local.changedValue = 0
        return api.local.NoChangePrice
    }

    BigDecimal previousPrice = 0
    def returnValue = api.local.NoChangePrice
    def iterationCount = 0
    def currentEntry
    def currentEntryPrice
    while (iterationCount < pricesCount) {
        currentEntry = pricesForCurrentArticle[iterationCount]
        currentEntryPrice = (currentEntry.price) ? currentEntry.price as BigDecimal : 0.0 as BigDecimal
        //Set the price to price per unit so that it becomes consistent.
        if (currentEntry.pricePerQuantity) {
            currentEntryPrice = currentEntry.price = currentEntryPrice / currentEntry.pricePerQuantity
        }
        //Set the price start date to start of duration for this computation in case start date is earlier than input start day..
        if (iterationCount == 0) {
            previousPrice = currentEntryPrice
            currentEntry.priceChange = 0
            if (currentEntry.validFrom < startDt) {
                currentEntry.validFrom = startDt
            }
        } else {
            //Compute the price change from reference start time. Some entries due to incorrect data starts on the same date as start but still is counted as difference.
            if (previousPrice != currentEntryPrice && currentEntry.validFrom > startDt) {
                api.trace("PriceChange Computation", " Current Article count change " + currentEntry.price + " previous Price " + previousPrice)
                currentEntry.priceChange = (currentEntryPrice - previousPrice)
                returnValue = api.local.ValueChangedPrice
            } else {
                currentEntry.priceChange = 0
            }
            previousPrice = currentEntryPrice
        }
        //Intermediate dates can have the end date wrong. This corrects the intermitent date to correct value for evaluation.
        if ((iterationCount + 1) < pricesCount && currentEntry.validTo.format("dd-MM-yyyy").equals("31-12-9999")) {
            def validEndDate = (pricesForCurrentArticle[iterationCount + 1].validFrom.minus(1)).format("dd-MM-yyyy") + " 23:59:59"
            validEndDate = Date.parse("dd-MM-yyyy hh:mm:ss", validEndDate)
            currentEntry.validTo = validEndDate
        }
        //This is for keeping the analysis limited to selected period.
        if (currentEntry.validTo > endDt) {
            currentEntry.validTo = endDt
        }
        iterationCount++
    }

    return returnValue
}

def getCountOfChanges(def inputPriceElementName) {
    def currentPriceChanges = api.getElement(inputPriceElementName)
    int countOfChanges = (currentPriceChanges) ? currentPriceChanges.size() : 0
    return countOfChanges
}