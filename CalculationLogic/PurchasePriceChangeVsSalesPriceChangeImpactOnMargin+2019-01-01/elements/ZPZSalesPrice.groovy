/** Based on the user selection or last updated record, find the sales price changes between the date Start date and end Date for the
 *  current selected product/sku
 */
if (api.local.skipProcessing)
    return api.local.NoDataPrice

if (!api.global.zpzSalesPriceDetails) {
    api.global.zpzSalesPriceDetails = SalesPriceFetchUtil.fetchSalesPricesForArticles("NetPrice")
}

def sku = api.getElement("Material")
return api.global.zpzSalesPriceDetails[sku]
