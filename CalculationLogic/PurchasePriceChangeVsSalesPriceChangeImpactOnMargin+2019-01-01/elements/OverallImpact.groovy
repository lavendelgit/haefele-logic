if (api.local.skipProcessing)
    return 0.0

def efficiencyStatus = api.getElement("ConsolidatePurchasePriceImpact")
return libs.__LIBRARY__.HaefeleSpecific.getAbsoluteValue(efficiencyStatus.profitImpact)