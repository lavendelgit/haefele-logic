/**
 * This element gives clear consumable break of the purchase Price Changes, Sales Price Changes and Transaction changes for easy consumption of user.
 */
if (api.local.skipProcessing)
    return null

final String startDtCol = "Valid From"
final String endDtCol = "Valid To"
final String purPrCol = "Purchase Price"
final String purPrChgCol = "Purchase Price Change"
final String purPrChgImpCol = "Purchase Price change Inefficiency"
final String slPrCol = "Sales Price"
final String slPrChgCol = "Sales Price Change"
final String slPrChgImpCol = "Sales Price Change Efficiency"
final String trCntCol = "Transaction Count"
final String trUntCol = "Transaction Units?"
final String trRevCol = "Transaction Revenue"

def computedPurchaseSalesRecords = api.getElement("TransactionNPricesMappedValues")
computedPurchaseSalesRecords = computedPurchaseSalesRecords?.sort{x,y ->
    return (x.startDate < y.startDate) ? -1 : (x.startDate == y.startDate) ? 0: 1
}

def matrix = api.newMatrix(startDtCol, endDtCol, purPrCol, purPrChgCol, purPrChgImpCol, slPrCol, slPrChgCol, slPrChgImpCol, trCntCol, trUntCol, trRevCol)
matrix.setEnableClientFilter(true)
matrix.setDisableSorting(false)
String sku = api.getElement("Material")
matrix.setTitle("Article: $sku ")
matrix.setColumnFormat(startDtCol, FieldFormatType.DATE)
matrix.setColumnFormat(endDtCol, FieldFormatType.DATE)
matrix.setColumnFormat(purPrCol, FieldFormatType.MONEY)
matrix.setColumnFormat(purPrChgCol, FieldFormatType.MONEY)
matrix.setColumnFormat(purPrChgImpCol, FieldFormatType.MONEY)
matrix.setColumnFormat(slPrCol, FieldFormatType.MONEY)
matrix.setColumnFormat(slPrChgCol, FieldFormatType.MONEY)
matrix.setColumnFormat(slPrChgImpCol, FieldFormatType.MONEY)
matrix.setColumnFormat(trCntCol, FieldFormatType.NUMERIC)
matrix.setColumnFormat(trUntCol, FieldFormatType.NUMERIC)
matrix.setColumnFormat(trRevCol, FieldFormatType.MONEY)

def purPr
def purPrChng
def slPr
def slPrChng

computedPurchaseSalesRecords.each {currentPeriodRecord ->
    def row = [:]
    purPr = (currentPeriodRecord.purchaseRecord?.price) ? currentPeriodRecord.purchaseRecord?.price : 0.0
    purPrChng = (currentPeriodRecord.purchaseRecord?.priceChange) ? currentPeriodRecord.purchaseRecord?.priceChange: 0.0
    slPr = (currentPeriodRecord.salesRecord?.price) ? currentPeriodRecord.salesRecord?.price : 0.0
    slPrChng = (currentPeriodRecord.salesRecord?.priceChange) ? currentPeriodRecord.salesRecord?.priceChange : 0.0
    row.put(startDtCol, currentPeriodRecord.startDate)
    row.put(endDtCol, currentPeriodRecord.endDate)
    row.put(purPrCol, purPr)
    row.put(purPrChgCol,purPrChng)
    row.put(purPrChgImpCol,currentPeriodRecord.pTransactionValue)
    row.put(slPrCol, slPr)
    row.put(slPrChgCol, slPrChng)
    row.put(slPrChgImpCol, currentPeriodRecord.sTransactionValue)
    row.put(trCntCol, currentPeriodRecord.transactionCount)
    row.put(trUntCol, currentPeriodRecord.transactionUnits)
    row.put(trRevCol, currentPeriodRecord.tTransactionValue)
    matrix.addRow(row)
}

return matrix