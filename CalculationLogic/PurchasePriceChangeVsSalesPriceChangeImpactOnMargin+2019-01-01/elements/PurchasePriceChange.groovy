/**
 * This element takes care of streamlining the purcharse price data fetched and populating the purchase price difference compared to the beginning of
 * the period of purchase. In streamlining if the start date is before the analysis start window. It will reset the start date to this date. Also sometimes the end
 * date is by default always 31st Dec 9999 then previous interval end date is reset to the next interval start date - 1.
 */

if (api.local.skipProcessing)
    return api.local.NoDataPrice


def purchasePricesForCurrentArticle = api.getElement("MaterialPurchasePrices")
if (!purchasePricesForCurrentArticle) {
    api.trace("PurchasePriceChange", "No values to determine if price change happened")
    return api.local.NoDataOnPurchasePrice
}

def startDt = api.getElement("SupplierPurchaseConfigStartDate")
def endDt = api.getElement("SupplierPurchaseConfigEndDate")
def changedValue = DataChangePopulationUtil.computePriceChangeNCorrectEndDate(purchasePricesForCurrentArticle, startDt, endDt)
return changedValue
