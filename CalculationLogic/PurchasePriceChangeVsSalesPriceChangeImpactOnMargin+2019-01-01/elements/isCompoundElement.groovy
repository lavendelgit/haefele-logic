if (!api.global.compoundArticlesMap) {
    def attributes = [
            "sku",
            "attribute1",
            "attribute2",
            "attribute6",
            "attribute7"
    ]
    def filters = [
            Filter.equal("name", "CompoundPrice")
    ]

    def stream = api.stream("PX", "sku", attributes, *filters)
    if (!stream) {
        return []
    }

    def currentList = null, currentItem = null
    def compoundPriceSkuMap = [:]
    while (stream.hasNext()) {
        currentItem = stream.next()
        currentList = compoundPriceSkuMap[currentItem.sku] ?: []
        currentList.add([
                price    : currentItem.attribute1,
                currency : currentItem.attribute2,
                startDate: currentItem.attribute6,
                endDate  : currentItem.attribute7])
        compoundPriceSkuMap[currentItem.sku] = currentList
        currentList = null
    }
    stream.close()
    api.global.compoundArticlesMap = compoundPriceSkuMap
}

def sku = api.getElement("Material")
return api.global.compoundArticlesMap[sku]

