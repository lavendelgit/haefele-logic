if (api.local.skipProcessing)
    return 0

def outputData = api.getElement("ConsumableBreakUpOfEfficiencyAnalysis")
def entries = outputData.getEntries()
BigDecimal startingPrice
BigDecimal cumulativeChange = 0.0
int cumulativeCount = 0
BigDecimal purchasePrice, purchasePriceChange
for (entry in entries) {
    purchasePrice = entry["Purchase Price"]
    purchasePriceChange = entry["Purchase Price Change"]
    if (purchasePrice && purchasePrice != 0.0 && !startingPrice) {
        startingPrice = purchasePrice
    }
    if (purchasePriceChange != 0.0) {
        cumulativeChange += purchasePriceChange
        cumulativeCount++
    }
}
api.trace("Moment of Truth", " CC " + cumulativeChange + " Cnt " + cumulativeCount + " SP " + startingPrice)
if (cumulativeCount > 0) {
    return ((cumulativeChange / cumulativeCount) / startingPrice)
}

return 0