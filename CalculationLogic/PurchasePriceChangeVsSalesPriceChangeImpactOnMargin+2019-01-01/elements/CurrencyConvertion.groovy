/**
 * This is generic utility class used for doing the conversion of currency from Source to Destination one provided the value exists. If the entry does not exists then
 * it simply returns the base value and raises alert
 */

def getConvertedValues(def sourceCur, def destinationCur, Date effectiveDate, BigDecimal currencyValue) {
    if (sourceCur == destinationCur) {
        return currencyValue
    }

    sourceCur = (sourceCur) ? sourceCur.toUpperCase() : sourceCur
    destinationCur = (destinationCur) ? destinationCur.toUpperCase() : destinationCur
    def conversionRate = api.global.currencyConverter.find {
        (it.value.sourceCurrency == sourceCur && it.value.destinationCurrency == destinationCur &&
                it.value.effStartDate <= effectiveDate && it.value.effEndDate >= effectiveDate)
    }
    if (!conversionRate) {
        api.logWarn("No conversion rate available from SourceCurrency(" + sourceCur + ") to DestinationCurrency " + destinationCur + " on date ("
                + effectiveDate.format("yyyy-MM-dd") + " so sending the value as is..")
        return currencyValue
    }
    return currencyValue * conversionRate.value.destinationUnit / conversionRate.value.sourceUnit
}


/** a global cache which has keys as Index and values as source currency to destination currency mapping with effective dates..
 *  We need to have effective data structure to deal with number of entries. Since destination currency in this case is always Euro this flat structure is helpful,
 *  however if we want to convert it to variety of target currencies then may be having destination currency as key may make sense.
 */
if (!api.global.currencyConverter) {
    int count = 0
    def listOfConversion = api.findLookupTableValues("CurrencyConvertion")?.collectEntries { entry ->
        [(count++): [sourceCurrency: (entry.key1) ? entry.key1.toUpperCase() : entry.key1, destinationCurrency: (entry.key2) ? entry.key2.toUpperCase() : entry.key2,
                     effStartDate  : new Date().parse("yyyy-MM-dd", entry.key3), effEndDate: new Date().parse("yyyy-MM-dd", entry.attribute1),
                     sourceUnit    : entry.attribute2, destinationUnit: entry.attribute3]]
    }
/*    listOfConversion.each { it ->
        api.trace("Printing Value", " Current Record " + it)
    }
*/ api.global.currencyConverter = listOfConversion
}

return api.global.currencyConverter