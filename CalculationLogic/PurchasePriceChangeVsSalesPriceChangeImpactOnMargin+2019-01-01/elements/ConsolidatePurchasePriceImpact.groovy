/**
 * This element sums up the price change value we calculated across different transaction interval i.e. Purchase Price Change and Sales Price Change
 * and highlights the overall inefficiency caused.
 */
if (api.local.skipProcessing)
    return []

def refineNoInefficencyElements() {
    def ppChange = out.PurchasePriceChange
    def spChange = out.MaterialSalesPriceChange
    def status = ((ppChange == api.local.NoChangePrice || ppChange == api.local.NoDataOnPurchasePrice) &&
            (spChange == api.local.NoChangePrice || spChange == api.local.NoDataOnSalesPrice)) ? "Skipped Article" : "No Inefficiency"
    return status
}

def computedPurchaseSalesRecords = api.getElement("TransactionNPricesMappedValues")

def consolidatedImpact = [transactionCount    : 0, transactionUnits: new BigDecimal(0), purchasePriceInefficiency: new BigDecimal(0),
                          salesPriceEfficiency: new BigDecimal(0), transactionRevenue: new BigDecimal(0), profitImpact: new BigDecimal(0),
                          InEfficiencyStatus  : ""]

computedPurchaseSalesRecords.each { currentPeriodRecord ->
    consolidatedImpact.transactionCount += currentPeriodRecord.transactionCount
    consolidatedImpact.transactionUnits += currentPeriodRecord.transactionUnits
    consolidatedImpact.purchasePriceInefficiency += currentPeriodRecord.pTransactionValue
    consolidatedImpact.salesPriceEfficiency += currentPeriodRecord.sTransactionValue
    consolidatedImpact.transactionRevenue += currentPeriodRecord.tTransactionValue
}

consolidatedImpact.profitImpact = consolidatedImpact.salesPriceEfficiency - consolidatedImpact.purchasePriceInefficiency
consolidatedImpact.InEfficiencyStatus = (consolidatedImpact.profitImpact > 0) ? "Very Efficient Pricing" :
        (consolidatedImpact.profitImpact == 0) ? "No Inefficiency" : "Needs Sales Price Improvement"
consolidatedImpact.InEfficiencyStatus = (consolidatedImpact.profitImpact == 0) ? refineNoInefficencyElements() : consolidatedImpact.InEfficiencyStatus


return consolidatedImpact

