/**
 * This element takes care of streamlining the sales price data by using DataChangePopulationUtil.
 */
if (api.local.skipProcessing)
    return api.local.NoDataPrice

def salesPricesForCurrentArticle = api.getElement("MaterialSalesPrices")
if (!salesPricesForCurrentArticle) {
    api.trace("SalesPriceChange", "No values to determine if price change happened")
    return api.local.NoDataOnSalesPrice
}

def startDt = api.getElement("SupplierPurchaseConfigStartDate")
def endDt = api.getElement("SupplierPurchaseConfigEndDate")
def changedValue = DataChangePopulationUtil.computePriceChangeNCorrectEndDate(salesPricesForCurrentArticle, startDt, endDt)
return changedValue