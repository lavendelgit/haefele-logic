/** Based on the user selection or last updated record, find the purchase price changes between the date Start date and end Date for the
 *  current selected product/sku
 */
//Get the start date and end date.
def startDt = api.getElement("SupplierPurchaseConfigStartDate")
def endDt = api.getElement("SupplierPurchaseConfigEndDate")
def sku = api.getElement("Material")


if (!api.global.purchasePriceDetails) {
    def purchasePriceDetails = [:]
    def attributes = [
            "sku",
            "attribute1",
            "attribute2",
            "attribute3",
            "attribute4",
            "attribute6"
    ]

    //Populate the map with the details
    def fmFromFilterDate = startDt.format("yyyy-MM-dd")
    def fmToFilterDate = endDt.format("yyyy-MM-dd")
    api.trace("Inputs", "fmFromFilterDate " + fmFromFilterDate + " fmToFilterDate " + fmToFilterDate)
    def fromDateIfEarlierThanStartToDateShouldOverlap = Filter.and(Filter.lessOrEqual("attribute1", fmFromFilterDate), Filter.greaterOrEqual("attribute2", fmFromFilterDate))
    def fromDateIfFurtherThanStartShouldOverlap = Filter.and(Filter.greaterThan("attribute1", fmFromFilterDate), Filter.lessOrEqual("attribute1", fmToFilterDate))
    def filters = [
            Filter.equal("name", "SurchargeCostPrice"),
            Filter.or(fromDateIfEarlierThanStartToDateShouldOverlap,
                      fromDateIfFurtherThanStartShouldOverlap
            )
    ]
    def stream = api.stream("PX", "attribute1", attributes, *filters)
    if (!stream) {
        api.trace("No Data Found in SurchargeCostPrice Stream", "Sorry.........")
        return []
    }
    int count = 0
    def record
    def articlePurchasePrices
    def currentPPRec
    def indexOfDecimal
    while (stream.hasNext()) {
        record = stream.next()
        count++
        articlePurchasePrices = purchasePriceDetails[record.sku] ?: []
        indexOfDecimal = record.attribute6.indexOf('.')
        currentPPRec = [
                sku             : record.sku,
                pricePerQuantity: (record.attribute6) ?
                                  Integer.parseInt(indexOfDecimal != -1 ? record.attribute6.substring(0, indexOfDecimal): record.attribute6) : 1,
                currency        : record.attribute3,
                validFrom       : (record.attribute1) ? java.util.Date.parse("yyyy-MM-dd", record.attribute1) : "",
                validTo         : (record.attribute2) ? java.util.Date.parse("yyyy-MM-dd", record.attribute2) : "",
                price           : (record.attribute4) ? record.attribute4 : 0.0,
                priceChange     : -1
        ]
        if (currentPPRec.validFrom) {
            if (currentPPRec.validFrom < startDt) {
                currentPPRec.validFrom = startDt
            }
        }

        articlePurchasePrices.add(currentPPRec)
        purchasePriceDetails[record.sku] = articlePurchasePrices
    }
    stream.close()
    api.trace("PurchasePricesForAllArticles", " Rows Fetchted(" + purchasePriceDetails?.size() + ") count (" + count + ")")
    api.global.purchasePriceDetails = purchasePriceDetails
}

def listOfPurchasePriceChangesForMaterial = api.global.purchasePriceDetails[sku]
api.trace("PurchasePricesForAllArticle", " Article(" + sku + ") purchasePrices (" + listOfPurchasePriceChangesForMaterial + ")")
return listOfPurchasePriceChangesForMaterial