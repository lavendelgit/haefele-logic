if (api.local.skipProcessing)
    return 0

def efficiencyStatus = api.getElement("ConsolidatePurchasePriceImpact")
return efficiencyStatus.purchasePriceInefficiency