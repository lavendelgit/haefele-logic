def shouldContinue = api.getElement("isCurrentMaterialUnderReview")

if (!shouldContinue) {
  api.log ("Skipping material as its not the one with purchase cost change")
  api.local.skipProcessing = true
//  api.abortCalculation ()
}

return !shouldContinue