/**
 * This element is a utility for creating a merged map i.e. Year vs per day of year date, purchase price and sales Price on that day.
 * This merge map will be further used to identify the date intervals and then that will be used to find the proper impacts of how purchase price change
 * was overcomed by sales price change.
 * @param individual purchase or sales record
 * @return It doesnt return anything but populates the single global Merged map meant to record Purchase price
 */
def getYearFromDate(Date inputDate) {
    if (!inputDate)
        return null

    return Integer.parseInt(inputDate.format("yyyy"))
}

def getIntValue(Date inputDate) {
    return inputDate[Calendar.DAY_OF_YEAR]
}

def getMaxDate(int year) {
    new Date().parse("yyyy-MM-dd", year + "-12-31")
}

def getEmptyYearRecord() {
    int i = 0
    def overallRecord = []
    while (i < 366) {
        overallRecord.add([date: null, purchaseRecord: null, salesRecord: null])
        i++
    }
    return overallRecord
}

def getLatestValidEndDate(Date validTo, Date userStatedEndDate) {
    def endDate = new Date()
    def validEndDate = (validTo < userStatedEndDate)?validTo : userStatedEndDate
    return (endDate < validEndDate) ? endDate : validEndDate
}

def getMergedRecordForYear(int fromYear) {
    api.trace("Year Requested", "fromYear (" + fromYear + ")")
    def currentYearRecord = api.local.mergedPurchaseSalesMap[fromYear]
    if (!currentYearRecord) {
        currentYearRecord = getEmptyYearRecord()
        api.local.mergedPurchaseSalesMap[fromYear] = currentYearRecord
    }
    return currentYearRecord
}

def getCurrentEndIndexValue(int fromYear, Date endDate) {
    def curMaxDate = getMaxDate(fromYear)
    return (curMaxDate <= endDate) ? getIntValue(curMaxDate) : getIntValue(endDate)
}

def getDateForIndex (Date startDate, int curStartIdx, int startValueIdx) {
    return startDate + (curStartIdx - startValueIdx)
}

def populateRecordData(currentPriceRecord, boolean isPurchase) {
    //get Date from the record
    Date userStatedEndDate = api.getElement("SupplierPurchaseConfigEndDate")
    Date fromYearDt = currentPriceRecord.validFrom
    Date toYearDt = getLatestValidEndDate(currentPriceRecord.validTo, userStatedEndDate)

    int fromYear = getYearFromDate(fromYearDt)
    int toYear = getYearFromDate(toYearDt)
    api.trace("The Date Ranges ", " fromYear " + fromYear + " toYear " + toYear)
    def currentYearRecord
    int curStartIdx, curEndIdx, startValueIdx
    //Terminate the loop when dates max till this year is populated from the start date
    while (fromYear <= toYear) {
        //See if the record exists for the year already
        currentYearRecord = getMergedRecordForYear(fromYear)
        curStartIdx = getIntValue(fromYearDt) - 1
        curEndIdx = getCurrentEndIndexValue(fromYear, toYearDt)
        startValueIdx = curStartIdx
        api.trace("In the loop..", curStartIdx + " curStartIdx " + curEndIdx + " curEndIdx ")
        while (curStartIdx < curEndIdx) {
            currentYearRecord[curStartIdx].date = getDateForIndex (fromYearDt, curStartIdx, startValueIdx)
            if (isPurchase)
                currentYearRecord[curStartIdx].purchaseRecord = currentPriceRecord
            else
                currentYearRecord[curStartIdx].salesRecord = currentPriceRecord
            curStartIdx++
        }
        api.local.mergedPurchaseSalesMap[fromYear] = currentYearRecord
        //api.trace ("In the loop..", fromYear + " fromYear " + api.local.mergedPurchaseSalesMap [fromYear])
        fromYear++
        fromYearDt = new Date().parse("yyyy-MM-dd", fromYear + "-01-01")
    }
    //api.trace ("The Record details", " currentYearRecord "+currentYearRecord)
}

def populatePurchaseData(currentPriceRecord) {
    populateRecordData(currentPriceRecord, true)
}

def populateSalesData(currentPriceRecord) {
    populateRecordData(currentPriceRecord, false)
}