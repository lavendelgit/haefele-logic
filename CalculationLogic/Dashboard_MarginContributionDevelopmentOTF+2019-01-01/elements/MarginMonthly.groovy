def salesOffice = api.getElement("SalesOffice")
def pgm = api.getElement("ProductGroupManager")

def ctx = api.getDatamartContext()

def dm1 = ctx.getDatamart("SalesHistoryDM")
def q1 = ctx.newQuery(dm1)
q1.select("Sku")
q1.select("Revenue")
q1.select("MarginalContributionAbs")
q1.select("YearMonthMonth")
q1.select("Verkaufsbereich3")

def filters = []

if (!salesOffice || salesOffice == "All") {
    filters.add(Filter.in("verkaufsbereich3", api.getElement("SalesOffices")))
} else {
    filters.add(Filter.in("verkaufsbereich3", salesOffice))
}
filters.add(Filter.isNotNull("YearMonthMonth"))
filters.add(Filter.ilike("YearMonthMonth", "%M%"))
q1.where(*filters)

def dm2 = ctx.getDataSource("Product")
def q2 = ctx.newQuery(dm2)
q2.select("Material")
q2.select("SM")

if (pgm != null) {
   q2.where(Filter.equal("sm", pgm))
}

def sql = """
SELECT YearMonthMonth as yearMonth,
    SUM(Revenue) as revenue,
    SUM(MarginalContributionAbs) as marginAbs
FROM T1
JOIN T2
ON t1.Sku = t2.Material
GROUP BY yearMonth
"""

def queryResult = ctx.executeSqlQuery(sql, q1, q2)

/*
def dmCtx = api.getDatamartContext()
def salesDM = dmCtx.getTable("SalesHistoryDM")
def datamartQuery = dmCtx.newQuery(salesDM)

def filters = [api.getElement("CustomerFilter")]

if (pgm) {
        filters.add(Filter.equal("SM", pgm))
}

datamartQuery.select("SUM(Revenue)", "revenue")
datamartQuery.select("SUM(MarginalContributionAbs)", "marginAbs")
datamartQuery.select("YearMonthMonth", "yearMonth")
datamartQuery.where(filters)

def queryResult = dmCtx.executeQuery(datamartQuery)

if (queryResult == null) {
        api.throwException("Query timeout.");
}
*/

def extractedValue
def result = queryResult?.collect { r ->
        if (!r.yearmonth) {
            api.trace ("Printing values", "Year "+ r.yearmonth)
            api.trace ("Value", "Year "+ r)
            return [
                    year: 0,
                    month: 0,
                    marginPerc: 0.0,
                    marginAbs: 0.0,
                    revenue: 0.0
            ]
        }
        extractedValue = r.yearmonth.substring(0,4)
        def year = r.yearmonth?.substring(0,4).toInteger()
        def month = r.yearmonth?.substring(6).toInteger()
        return [
                year: year,
                month: month,
                marginPerc: r.marginabs / r.revenue,
                marginAbs: r.marginabs,
                revenue: r.revenue
        ]}
        ?.groupBy { it.year.toString() }
        ?.collectEntries { year, margins -> [year, margins.collectEntries{ [it.month, it] }] }

api.trace("result", "", result)

if (!result || result.isEmpty()) {
    api.throwException("No data")
}


return result
