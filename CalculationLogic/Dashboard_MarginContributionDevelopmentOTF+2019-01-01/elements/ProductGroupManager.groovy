def pgms = api.find("P", 0, 0, "attribute5", ["attribute5"], true, Filter.isNotNull("attribute5")).collect { p -> p.attribute5 }

api.option("PGM", pgms)