import net.pricefx.common.api.FieldFormatType

def marginMonthly = api.getElement("MarginMonthly")
def plannedMargin = api.getElement("PlannedMargin")
def months = api.getElement("Months")

def years = marginMonthly.keySet().toList().sort()
def currentYear = years.last()
def firstYear = years.first()
def pastYears = years.take(years.size() - 1)
def lastYear = pastYears?.last()

def currentYearCalcMarginHeader = currentYear + " Hochgerechnet (%)"
def resultMatrix = api.newMatrix(["Monat", *pastYears, "CAGR", currentYearCalcMarginHeader,
                                  "Ist", "Ist vs Soll (%)", "Trend Ist vs Soll",
                                  "Plan", "Ist vs Plan (%)", "Trend Ist vs Plan"])
resultMatrix.setPreferenceName("DBEPercent_OTF")
pastYears.forEach { year -> resultMatrix.setColumnFormat(year, FieldFormatType.PERCENT )}
resultMatrix.setColumnFormat("CAGR", FieldFormatType.PERCENT )
resultMatrix.setColumnFormat(currentYearCalcMarginHeader, FieldFormatType.PERCENT)
resultMatrix.setColumnFormat("Ist", FieldFormatType.PERCENT)
resultMatrix.setColumnFormat("Ist vs Soll (%)", FieldFormatType.PERCENT)
resultMatrix.setColumnFormat("Plan", FieldFormatType.PERCENT)
resultMatrix.setColumnFormat("Ist vs Plan (%)", FieldFormatType.PERCENT)

for (month in months) {
    row = [
        "Monat": month.value,
    ]
    years.forEach { year -> row[year] = marginMonthly.get(year)?.get(month.key)?.marginPerc}
    def firstYearMargin = marginMonthly.get(firstYear)?.get(month.key)?.marginPerc
    def lastYearMargin = marginMonthly.get(lastYear)?.get(month.key)?.marginPerc
    def currentYearMargin = (marginMonthly.get(currentYear)?.get(month.key)?.marginPerc)?:0.0
    if (firstYearMargin != null & firstYearMargin != 0 && lastYearMargin != null) {
        def mantissa = libs.__LIBRARY__.MathUtility.divide(lastYearMargin, firstYearMargin)
        def power = libs.__LIBRARY__.MathUtility.divide(1, (pastYears.size() - 1))
        def cagr = Math.pow(mantissa, power) - 1
        def currentYearCalculatedMargin = lastYearMargin * (1 + cagr)
        def plannedMarginPerc = (plannedMargin?.get(month.key)?.marginPerc)?:0.0
        row["CAGR"] = cagr
        row[currentYear + " Hochgerechnet (%)"] = currentYearCalculatedMargin
        row["Plan"] = plannedMarginPerc
        if (currentYearMargin) {
            row["Ist"] =  currentYearMargin
            row["Ist vs Soll (%)"] = currentYearMargin - currentYearCalculatedMargin
            row["Trend Ist vs Soll"] = Util.trendIcon(resultMatrix, currentYearMargin, currentYearCalculatedMargin)
            row["Ist vs Plan (%)"] = currentYearMargin - plannedMarginPerc
            row["Trend Ist vs Plan"] = Util.trendIcon(resultMatrix, currentYearMargin, plannedMarginPerc)
        }

    }

    resultMatrix.addRow(row)
}

return resultMatrix