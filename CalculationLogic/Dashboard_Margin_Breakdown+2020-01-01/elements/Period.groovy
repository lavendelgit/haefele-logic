Map<String, String> entryConfiguration = Configuration.ENTRY_CONFIGURATION
String year = api.getElement("Year")
def quarter = api.option(entryConfiguration.quarterName, *entryConfiguration.quarterValues)

def calendar = api.getElement("Calendar")
String yearQuarter = "${year}-${quarter}"
String quarterValue = quarter != null ? yearQuarter : year
String currentQuarter = libs.SIP_Dashboards_Commons.ConfiguratorUtils.getCurrentQuarter()

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(entryConfiguration.quarterName, currentQuarter)

return calendar.getTimePeriod(quarterValue)