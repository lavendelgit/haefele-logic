Map<String, String> entryConfiguration = Configuration.ENTRY_CONFIGURATION
String comparisonYear = api.getElement("ComparisonYear")
def period = api.getElement("Period")
def comparisonQuarter = api.option(entryConfiguration.comparisonQuarterName, *entryConfiguration.quarterValues)
def calendar = api.getElement("Calendar")
String currentQuarter = libs.SIP_Dashboards_Commons.ConfiguratorUtils.getCurrentQuarter()

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(entryConfiguration.comparisonQuarterName, currentQuarter)

if (comparisonYear) {
    String comparisonYearQuarter = "${comparisonYear}-${comparisonQuarter}"
    String comparisonQuarterValue = comparisonQuarter != null ? comparisonYearQuarter : comparisonYear

    return calendar.getTimePeriod(comparisonQuarterValue)
} else if (comparisonQuarter == null) {
    return period.add(-1)
} else {
    def prevYear = period.getStartDate()
        .getAt(java.util.Calendar.YEAR)
    String prevYearQuarter = "${prevYear}-${comparisonQuarter}"

    return calendar.getTimePeriod(prevYearQuarter)
}