def buildWaterfallChart(String chartTitle, List<Map> chartData) {
    def hLib = libs.HighchartsLibrary
    Map colors = Configuration.CHART_COLORS
    boolean isPercentageView = api.getElement("ShowAsPercentage")
    String percentageLabel = Configuration.PERCENTAGE_LABEL
    String currencyLabel = api.getElement("TargetCurrencyData").currencyCode
    String yAxisTitle = isPercentageView ? percentageLabel : currencyLabel
    def yAxis = hLib.Axis.newAxis().setTitle(yAxisTitle)
    def xAxis = hLib.Axis.newAxis().setType(hLib.ConstConfig.AXIS_TYPES.CATEGORY)
    def legend = hLib.Legend.newLegend().setEnabled(false)
    def tooltip = hLib.Tooltip.newTooltip().setPointFormat(getTooltipFormat(isPercentageView))

    Map plotOptions = [waterfall: [upColor     : colors.positive,
                                   color       : colors.base,
                                   pointPadding: 0,
                                   dataLabels  : [enabled: true,
                                                  format : getLabelFormat(isPercentageView),
                                                  inside : false],
                                   borderWidth : 0]]
    Map chart = hLib.Chart.newChart().setTitle(chartTitle)
            .setLegend(legend)
            .setTooltip(tooltip)
            .setPlotOptions(plotOptions)

    if (chartData) {
        def series = hLib.WaterfallSeries.newWaterfallSeries()
                .setData(chartData)
                .setXAxis(xAxis)
                .setYAxis(yAxis)

        chart.setSeries(series)
    }

    return api.buildHighchart(chart.getHighchartFormatDefinition())
}

def getChartData(Map chartData) {
    def t1Name = api.getElement("Calendar").getTimePeriodName(api.getElement("ComparisonPeriod"))
    def t2Name = api.getElement("Calendar").getTimePeriodName(api.getElement("Period"))

    BigDecimal mix = chartData.mixEffect?:0
    BigDecimal volume = chartData.volumeEffect?:0
    BigDecimal price = chartData.priceEffect?:0
    BigDecimal cost = chartData.costEffect?:0

    BigDecimal t1_revenue = chartData.period1Margin?:0
    BigDecimal t2_revenue = chartData.period2Margin?:0
    BigDecimal newBusiness = chartData.newBusiness?:0
    BigDecimal lostBusiness = chartData.lostBusiness?:0

    Boolean isPercentageView = api.getElement("ShowAsPercentage")

    BigDecimal percentageBase = t1_revenue
    BigDecimal intersection = t2_revenue - t1_revenue - volume - price - cost - mix - newBusiness - lostBusiness

    String marginKey = "Margin in ${t1Name}"
    Map<String, BigDecimal> fieldMap = [(marginKey)    : t1_revenue,
                                        "Volume"       : volume,
                                        "Price"        : price,
                                        "Mix"          : mix,
                                        "New Business" : newBusiness,
                                        "Lost Business": lostBusiness,
                                        "Cost"         : cost,
                                        "Intersection" : intersection]
    List<Map> data = []
    if (t1_revenue || t2_revenue) {
        data = fieldMap.collect { key, value ->
            prepareAdjustmentStructure(key, getFormattedValue(isPercentageView, percentageBase, value))
        }
        data << prepareFinalPricePointStructure("Margin in ${t2Name}")
    }

    return data
}

String getValueFormat(boolean isPercentageView) {
    def yFormatTypes = libs.HighchartsLibrary.ConstConfig.TOOLTIP_POINT_Y_FORMAT_TYPES
    Map currencyData = api.getElement("TargetCurrencyData")
    String priceWithCurrencySymbol = libs.SIP_Dashboards_Commons.CurrencyUtils.getFormatWithCurrencySymbol(yFormatTypes.ABSOLUTE_PRICE,
            currencyData.currencySymbol)

    return isPercentageView ? yFormatTypes.PERCENTAGE : priceWithCurrencySymbol
}

String getTooltipFormat(boolean isPercentageView) {
    return "<b>${getValueFormat(isPercentageView)}<b>"
}

String getLabelFormat(boolean isPercentageView) {
    return getValueFormat(isPercentageView)
}

BigDecimal getFormattedValue(Boolean isPercentageView, BigDecimal percentageBase, BigDecimal value) {
    if (isPercentageView) {
        return percentageBase ? 100 * ((value ?: 0) / percentageBase) : 0
    }

    return value
}

Map prepareFinalPricePointStructure(String name) {
    return [name: name, isSum: true]
}

Map prepareAdjustmentStructure(String name, BigDecimal value) {
    return [name: name, y: value]
}