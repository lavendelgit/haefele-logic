Map<String, String> chartNames = libs.SIP_Dashboards_Commons.CausalityUtils.CHART_NAMES
Map<String, String> entryConfiguration = Configuration.ENTRY_CONFIGURATION

def model = api.option(entryConfiguration.calculationOptionEntryName, chartNames.values().toList())

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(entryConfiguration.calculationOptionEntryName, chartNames.values().toList().getAt(0))
return model