import groovy.transform.Field

@Field ENTRY_CONFIGURATION = [quarterName               : "Quarter",
                              quarterValues             : ["Q1", "Q2", "Q3", "Q4"],
                              comparisonQuarterName     : "Comparison Quarter",
                              yearName                  : "Year",
                              comparisonYearName        : "Comparison Year",
                              showPercentageEntryName   : "Show Percentage (%)",
                              calculationOptionEntryName: "Calculation Type"]

@Field PERCENTAGE_LABEL = "Percentage (%)"

@Field CHART_COLORS = [positive: "#8bbc21",
                       base    : "#910000"]

@Field MONTHS_BACK_FROM = -3
@Field MONTHS_BACK_TO = -12

@Field DATE_FORMAT = "yyyy-MM-dd"
@Field YEAR_FORMAT = "yyyy"

@Field YEAR_FIELD_SUFFIX = "Year"

@Field SQL_CONFIGURATION
SQL_CONFIGURATION = libs.SIP_Dashboards_Commons.ConfigurationUtils.getAdvancedConfiguration([:], libs.SIP_Dashboards_Commons.ConstConfig.DASHBOARDS_ADVANCED_CONFIGURATION_NAME)