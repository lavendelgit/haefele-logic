Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION
def productElement = api.getElement("Products")
def customerElement = api.getElement("Customers")
def period = api.getElement("Period")
def comparisonPeriod = api.getElement("ComparisonPeriod")
String targetCurrencyCode = api.getElement("TargetCurrencyData").currencyCode
String model = api.getElement("Model")
Filter genericFilter = api.getElement("GenericFilter")

libs.SIP_Dashboards_Commons.CausalityUtils.getMarginBreakdownData(productElement,
    customerElement,
    period,
    comparisonPeriod,
    targetCurrencyCode,
    sqlConfiguration,
    model,
    genericFilter)