def model = api.getElement("Model") ?: "Net"

return prepareChart(model)

def prepareChart(String model) {
    List<Map> chartData = MarginBreakdownDashboardUtils.getChartData(api.getElement("TxDataQuery"))

    return MarginBreakdownDashboardUtils.buildWaterfallChart(model, chartData)
}