def create(List data, String property) {
    def dataLabelsFormat = [
            revenue: "{point.y:,.0f} €",
            marginAbs: "{point.y:,.0f} €",
            marginPerc: "{point.y:,.2f}%",
    ]
    def tooltipPointFormat = [
            revenue: "Revenue: {point.y:,.0f} €",
            marginAbs: "DB: {point.y:,.0f} €",
            marginPerc: "DB: {point.y:,.2f}%",
    ]
    def yAxisTitle = [
            revenue: "Revenue",
            marginAbs: "DB",
            marginPerc: "DB %"
    ]

    def categories = data*.yearMonth

    api.trace("categories", "", categories)

    def chartDef = [
            chart: [
                    type: "column",
                    zoomType: "xy"
            ],
            legend: [
                layout: "horizontal",
                align: "center",
                verticalAlign: "top",
                x: 0,
                y: 0,
                borderWidth: 0
            ],
            credits: [
                    enabled: false
            ],
            title: [
                    text: ""
            ],
            xAxis: [[
                            title: [
                                    text: "Year/Month"
                            ],
                            categories: categories
                    ]],
            yAxis: [[
                            title: [
                                    text: yAxisTitle[property]
                            ],
                            stackLabels: [
                                    enabled: true,
                                    allowOverlap: true
                            ]
                    ]],
            tooltip: [
                    headerFormat: 'Year/Month: {point.x}<br/>',
                    pointFormat: tooltipPointFormat[property]
            ],
            series: [[
                 name: yAxisTitle[property],
                 data: data.collect { r -> r[property] },
                 dataLabels: [
                         enabled: true,
                         format: dataLabelsFormat[property]
                 ]
            ]]
    ]

    return api.buildHighchart(chartDef)
}