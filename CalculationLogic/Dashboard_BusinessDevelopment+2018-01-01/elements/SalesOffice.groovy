def salesOffices = api.getElement("SalesOffices")
api.option("Sales Office", ["All"] + salesOffices) ?: "All"