def salesOffice = api.getElement("SalesOffice")
def pgm = api.getElement("ProductGroupManager")
def customerSector = api.getElement("CustomerSector")

def ctx = api.getDatamartContext()

def dm1 = ctx.getDatamart("SalesHistoryDM")
def q1 = ctx.newQuery(dm1)
q1.select("Sku")
q1.select("Revenue")
q1.select("MarginalContributionAbs")
q1.select("YearMonthMonth")
q1.select("Verkaufsbereich3")

if (!salesOffice || salesOffice == "All") {
    q1.where(Filter.in("verkaufsbereich3", api.getElement("SalesOffices")))
} else {
    q1.where(Filter.in("verkaufsbereich3", salesOffice))
}

if (customerSector) {
    q1.where(Filter.in("Branche", customerSector))
}


def dm2 = ctx.getDataSource("Product")
def q2 = ctx.newQuery(dm2)
q2.select("Material")
q2.select("SM")

if (pgm != null) {
    q2.where(Filter.equal("sm", pgm))
}

def sql = """
SELECT YearMonthMonth as yearMonth,
    SUM(Revenue) as revenue,
    SUM(MarginalContributionAbs) as marginAbs
FROM T1
JOIN T2
ON t1.Sku = t2.Material
GROUP BY yearMonth
ORDER BY yearMonth
"""

def queryResult = ctx.executeSqlQuery(sql, q1, q2)

def result = queryResult?.collect { r ->
        return [
                yearMonth: r.yearmonth,
                marginPerc: r.marginabs / r.revenue * 100,
                marginAbs: r.marginabs,
                revenue: r.revenue
        ]}

api.trace("result", "", result)

if (!result || result.isEmpty()) {
    api.throwException("No data")
}


return result
