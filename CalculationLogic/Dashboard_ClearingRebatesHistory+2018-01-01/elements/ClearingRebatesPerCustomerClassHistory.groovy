def pp = api.findLookupTable("ClearingRebatesPerCustomerClassHistory")

if (!pp) {
    api.addWarning("Pricing parameter not found (ClearingRebatesPerCustomerClassHistory)")
    return
}

def filters = [
        Filter.equal("lookupTable.id", pp.id),
]

def stream = api.stream("MLTV2", null, ["key1", "key2", "attribute1"], *filters)

if (!stream) {
    return []
}

def result = []

while (stream.hasNext()) {
    def row = stream.next();
    api.trace("row", "", row)
    result.add(row)
}

result
    .collect { r -> [
        customerClass: r.key1,
        date: api.parseDate("yyyy-MM-dd", r.key2),
        count: r.attribute1 as Integer
    ]}
    .groupBy { r -> r.customerClass }
