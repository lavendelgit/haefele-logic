def groupedRecords = api.getElement("ClearingRebatesPerCustomerDiscountGroupHistory")

def series = groupedRecords
    .collect { discountGroup, records -> [
        name: discountGroup,
        data: records.collect { r -> [r.date.getTime(), r.count]}
    ]}

api.trace("series", "", series)

def chartDef = [
        chart: [
                type: "column",
                zoomType: "x"
        ],
        title: [
                text: "Clearing Rebates per Customer Discount Group History"
        ],
        xAxis: [[
                        title: [
                                text: "Customer Discount Group"
                        ],
                        type: "datetime"
                ]],
        yAxis: [[
                        title: [
                                text: "Number of Clearing Rebates"
                        ]
                ]],
        tooltip: [
                headerFormat: 'Customer Discount Group: {series.name}<br/>',
                pointFormat: 'Date: {point.x:%Y-%m-%d}<br/> Number of Clearing Rebates: {point.y}'
        ],
        legend: [
                enabled: true
        ],
        series: series
]

api.buildFlexChart(chartDef)
