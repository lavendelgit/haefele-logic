def pp = api.findLookupTable("ClearingRebatesPerSalesOfficeHistory")

if (!pp) {
    api.addWarning("Pricing parameter not found (ClearingRebatesPerSalesOfficeHistory)")
    return
}

def visibleSalesOffices = [
       "Berlin",
       "Frankfurt",
       "Handel",
       "Hannover",
       "Kaltenkirchen",
       "Köln",
       "München",
       "Münster",
       "Naumburg",
       "Nürnberg",
       "Stuttgart Airport"
]

def filters = [
        Filter.equal("lookupTable.id", pp.id),
        Filter.in("key1", visibleSalesOffices)
]

def stream = api.stream("MLTV2", null, ["key1", "key2", "attribute1"], *filters)

if (!stream) {
    return []
}

def result = []

while (stream.hasNext()) {
    def row = stream.next();
    api.trace("row", "", row)
    result.add(row)
}

result
    .collect { r -> [
        salesOffice: r.key1,
        date: api.parseDate("yyyy-MM-dd", r.key2),
        count: r.attribute1 as Integer
    ]}
    .groupBy { r -> r.salesOffice }
