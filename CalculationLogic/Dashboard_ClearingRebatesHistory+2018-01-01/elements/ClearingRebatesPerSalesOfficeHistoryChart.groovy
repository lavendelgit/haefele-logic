def groupedRecords = api.getElement("ClearingRebatesPerSalesOfficeHistory")

def series = groupedRecords
    .collect { salesOffice, records -> [
        name: salesOffice,
        data: records.collect { r -> [r.date.getTime(), r.count]}
    ]}

api.trace("series", "", series)

def chartDef = [
        chart: [
                type: "column",
                zoomType: "x"
        ],
        title: [
                text: "Clearing Rebates per Sales Office History"
        ],
        xAxis: [[
                        title: [
                                text: "Sales Office"
                        ],
                        type: "datetime",
                ]],
        yAxis: [[
                        title: [
                                text: "Number of Clearing Rebates"
                        ]
                ]],
        tooltip: [
                headerFormat: 'Sales Office: {series.name}<br/>',
                pointFormat: 'Date: {point.x:%Y-%m-%d} <br/>Number of Clearing Rebates: {point.y}'
        ],
        legend: [
                enabled: true
        ],
        series: series
]

api.buildFlexChart(chartDef)
