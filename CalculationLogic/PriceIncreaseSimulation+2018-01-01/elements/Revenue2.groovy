def originalRevenue = api.getElement("Revenue") as BigDecimal
def salesPriceIncreasePerc = api.getElement("SalesPriceIncreasePerc") as BigDecimal;

api.trace("[Revenue2]", "originalRevenue", originalRevenue)
api.trace("[Revenue2]", "salesPriceIncreasePerc", salesPriceIncreasePerc)

if (!originalRevenue) {
    api.redAlert("Revenue")
    return
}

originalRevenue * (1 + salesPriceIncreasePerc)