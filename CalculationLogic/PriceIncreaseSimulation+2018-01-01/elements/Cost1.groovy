def originalCost = api.getElement("Cost") as BigDecimal
def supplierPriceIncreasePerc = api.getElement("SupplierPriceIncreasePerc") as BigDecimal

api.trace("[Cost1]", "originalCost", originalCost)
api.trace("[Cost1]", "supplierPriceIncreasePerc", supplierPriceIncreasePerc)
api.logInfo("[PriceIncreaseSimulation][Cost1] originalCost", originalCost)
api.logInfo("[PriceIncreaseSimulation][Cost1] supplierPriceIncreasePerc", supplierPriceIncreasePerc)

if (originalCost == null) {
    api.redAlert("Cost")
    return
}

if (supplierPriceIncreasePerc == null) {
    api.redAlert("SupplierPriceIncreasePerc")
    return
}

return originalCost * (1 + supplierPriceIncreasePerc)
