if (!api.global.supplierPriceIncrease) {
    api.global.supplierPriceIncrease = api.findLookupTableValues("SupplierPriceIncrease")
        .collectEntries { pp -> [ (pp.name) : [
        supplierId: pp.name,
        supplierName: pp.attribute1,
        priceIncrease: pp.attribute2
    ]]}
}

api.trace("api.global.supplierPriceIncrease","",api.global.supplierPriceIncrease)

return api.global.supplierPriceIncrease