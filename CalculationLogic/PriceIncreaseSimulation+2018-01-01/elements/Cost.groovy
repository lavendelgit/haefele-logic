def revenue = api.getElement("SalesHistory").revenue as BigDecimal;
def margin = api.getElement("SalesHistory").marginAbs as BigDecimal;

if (revenue == null) {
    api.redAlert("Revenue")
    return
}

if (margin == null) {
    api.redAlert("Margin")
    return
}


revenue - margin