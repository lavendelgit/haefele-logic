String firstDataLoadLabel = "Price Change Simulation DM 2020"
String firstDataLoadType = "Truncate"
String firstDataLoadTarget = "DM.PriceChangeSimulationDM_2020"

actionBuilder
	.addDataLoadAction(firstDataLoadLabel, firstDataLoadType, firstDataLoadTarget)
		.setCalculate(true)
