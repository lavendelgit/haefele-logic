import net.pricefx.common.api.FieldFormatType

api.trace("api.global.summaryData",api.global.summaryData)

def columns = ['Total Number of Articles', 'Total Number of Deactivated Articles', 'Total number of deactivated articles with active conditions']

def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT]

def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Summary - Active Conditions for Deactivated Articles', columns, columnTypes)

def deactiveArticlesActiveConditionCount = api.formatNumber("###,###,###",api.global.summaryData?.deactiveArticlesActiveConditionCount?:0)
def deactiveArticlesCount = api.formatNumber("###,###,###",api.global.summaryData?.deactiveArticlesCount?:0)

def totoalProductCount = api.formatNumber("###,###,###",api.count("P")?:0)

def htmlPortlet = api.newController()
StringBuilder htmlBuilder = new StringBuilder()
htmlBuilder.append(getHeaderString())
htmlBuilder.append("<div>")

htmlBuilder.append("<div style='float: left;width: 20%;padding-left: 5px;height: 80%;margin: 3px;background-color:#F8F8F8;'>")
htmlBuilder.append("<h3><b>Total Number of Articles</h3></b></div>")

htmlBuilder.append("<div style='float: left;width: 5%;padding-left: 5px;height: 80%;margin: 3px;background-color:#F8F8F8;'>")
htmlBuilder.append("<h3 style='color:#69bdd2;font-weight:bold;text-align: right;padding-right: 5px'>$totoalProductCount</h3></div>")
htmlBuilder.append("</div>")


htmlBuilder.append("</br></br></br></br></br>")
htmlBuilder.append("<div style='float: left;width: 20%;padding-left: 5px;height: 80%;margin: 3px;background-color:#F8F8F8;'>")
htmlBuilder.append("<h3><b>Total Number of Deactivated Articles</b></h3></div>")
htmlBuilder.append("<div style='float: left;width: 5%;padding-left: 5px;height: 80%;margin: 3px;background-color:#F8F8F8;'>")
htmlBuilder.append("<h3 style='color:#69bdd2;font-weight:bold;text-align: right;padding-right: 5px'>$deactiveArticlesCount</h3></div>")

htmlBuilder.append("</br></br></br></br></br>")
htmlBuilder.append("<div style='float: left;width: 20%;padding-left: 5px;height: 80%;margin: 3px;background-color:#F8F8F8;'>")
htmlBuilder.append("<h3><b>Total number of deactivated articles with active conditions</b></h3></div>")
htmlBuilder.append("<div style='float: left;width: 5%;padding-left: 5px;height: 80%;margin: 3px;background-color:#F8F8F8;'>")
htmlBuilder.append("<h3 style='color:#69bdd2;font-weight:bold;text-align: right;padding-right: 5px'>$deactiveArticlesActiveConditionCount</h3></div>")
htmlBuilder.append("</div>")
htmlPortlet.addHTML(htmlBuilder.toString())
return htmlPortlet

protected String getHeaderString() {
    return '<h2>Summary - Active Conditions for Deactivated Articles</h2>'
}