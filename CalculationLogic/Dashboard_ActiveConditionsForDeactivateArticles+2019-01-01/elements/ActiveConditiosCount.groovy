import net.pricefx.common.api.FieldFormatType

List columns = Constants.DM_COLUMNS.values() as List//['Article Number', 'Material Desc', 'Prod Hierarchy I', 'Prod Hierarchy II', 'Prod Hierarchy III', 'ZMC', 'ZPA', 'ZPC', 'ZPF', 'ZPM', 'ZPP', 'ZPS', 'ZPZ', 'ZRA', 'ZRI', 'ZPAP', 'ZRPG', 'Active Condition Count']

def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT,
                   FieldFormatType.TEXT, FieldFormatType.TEXT,
                   FieldFormatType.TEXT, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER, FieldFormatType.INTEGER]

def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Active Conditions Count', columns, columnTypes)
def data = out.ActiveConditionsCountData

data.each { entry ->
    resultMatrix.addRow(
            columns.collectEntries { column ->
                [(column): entry[column]]
            }
    )
}
resultMatrix.setPreferenceName("Active Conditions Count")

/*resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("PPIDistribution-ED"))
        .withColValueAsEventDataAttr(columns[0], "EffectiveYear")
*/
if (data) extractSummaryData(data)

return resultMatrix

def extractSummaryData(List result) {
    api.global.summaryData = ['deactiveArticlesCount'               : result.size(),
                              'deactiveArticlesActiveConditionCount': result?.findAll { row -> row['Active Condition Count'] != 0 }?.size()]
}