def orderByField = ['Material']

List dataRows = libs.__LIBRARY__.DBQueries.getDataFromSource(Constants.DM_COLUMNS, 'ActiveConditionsForDeactivatedArticlesDM', null, orderByField, 'DataMart')?.collect()
return dataRows