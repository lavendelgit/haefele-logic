def ready = "Yes".equals(api.getElement("RfP_PL"))
def mpl = api.getElement("NettoMPL")

if (ready && (mpl == null)) {
  return "Yes"
}

return "No"
