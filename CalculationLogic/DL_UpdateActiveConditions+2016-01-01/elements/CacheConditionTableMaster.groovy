api.global.conditionTableMasterCache = api.global.conditionTableMasterCache ?: api.findLookupTableValues("ConditionTableMaster","-attribute1",Filter.equal("attribute2","Yes"))?.collectEntries { it ->
   [(it.name): [
     // tableName : it.name,
      additionalKeyColumns : it.attribute1,
      articleFilter : it.attribute3?.split(','),
      pgFilter : it.attribute4
  ]]
}
//api.trace("api.global.conditionTableMasterCache",api.global.conditionTableMasterCache)
return