if (!api.global.ZRPGFocusGroupCache) {

    cacheArticleFocusGroup()
    cacheFocusGroupDiscountData()
}

protected cacheArticleFocusGroup() {
    api.global.ZRPGFocusGroupCache = [:]
    List filter = [Filter.equal("name", "FocusGroup")]
    List result = []
    List fields = ["sku", "attribute2"]
    int maxRecords = api.getMaxFindResultsLimit()
    int start = 0
    while (recs = api.find("PX3", start, maxRecords, "sku", fields, false, *filter)) {
        result.addAll(recs)
        start += recs.size()
    }
    api.global.ZRPGFocusGroupCache = result?.collectEntries { articleFocusGroupRow ->
        [(articleFocusGroupRow.sku): articleFocusGroupRow.attribute2]
    }
}





protected cacheFocusGroupDiscountData() {
    api.global.focusGroupRecordsCount = [:]
    List focusGroupRecords = []
   // def table = api.findLookupTable("FocusGroupDiscount")
    def startRow = 0
    while (rows = api.find("PX50", startRow, api.getMaxFindResultsLimit(), "attribute6", Filter.equal("name", "S_ZRPG"))) {
        focusGroupRecords.addAll(rows)
        startRow += rows.size()
    }
    api.global.focusGroupRecordsCount = focusGroupRecords?.groupBy({ row -> row.attribute7 })?.collectEntries { String key, List groupedValues ->
        [(key): groupedValues.size()]
    }
}