return buildPhKeys()
protected buildPhKeys() {

    List productHierarchy = [
            out.DeActivatedProduct?.attribute11.split(" ")[0],
            out.DeActivatedProduct?.attribute12.split(" ")[0],
            out.DeActivatedProduct?.attribute13.split(" ")[0],
            out.DeActivatedProduct?.attribute14.split(" ")[0],
            out.DeActivatedProduct?.attribute15.split(" ")[0],
            out.DeActivatedProduct?.attribute16.split(" ")[0],
            out.DeActivatedProduct?.attribute17.split(" ")[0]
    ]

    List phKeyList = []

    for(int i=7;i>=0;i--){
        String phKey
        productHierarchy.each{
            phKey = phKey ? phKey +"-" + Lib.replaceNull(it) : Lib.replaceNull(it)
            //phKey += Lib.replaceNull(it)+"-"
        }
        api.trace("phKey",phKey)
        phKeyList.add(phKey)
        productHierarchy[i-1] = null
    }
    api.trace("phKeyList",phKeyList.toUnique())
    return phKeyList.toUnique()
}