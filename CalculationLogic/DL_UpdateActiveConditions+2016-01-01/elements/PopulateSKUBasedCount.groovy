if (api.global.pxSKUBasedCount) {
    api.global.skuSpecificConditionNameList.each { conditionName ->
        api.trace("conditionName", conditionName)
        String skuKey = out.DeActivatedProduct?.sku + "-S_" + conditionName
        int skuCount = api.global.pxSKUBasedCount[skuKey] ?: 0
        if (api.local.SKUCount[conditionName] == null) api.local.SKUCount[conditionName] = 0
        api.local.SKUCount[conditionName] += skuCount
    }
}

api.local.SKUCount["ZPAP"] = Lib.getZPAPCount()
api.local.SKUCount["ZRPG"] = Lib.getZRPGCount()