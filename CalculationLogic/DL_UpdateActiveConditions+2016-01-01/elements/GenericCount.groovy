if (!api.global.pxGenericPhBasedCount) {
    api.global.pxGenericPhBasedCount = [:]
    api.global.pxGenericPhBasedCount["ZMC"] = [:]
    api.global.pxGenericPhBasedCount["ZRC"] = [:]

    def records
    List filters = [Filter.equal("sku", "*")]
    filters.add(Filter.or(
            getDateFilterForTable("S_ZMC", "attribute1", "attribute2", "attribute4", "attribute3"),
            getDateFilterForTable("S_ZRC", "attribute1", "attribute2", "attribute27", "attribute3")))


    List fields = ["name", "sku","attribute1","attribute2","attribute3", "attribute4", "attribute7", "attribute8", "attribute9", "attribute10", "attribute11", "attribute12", "attribute13", "attribute6", "attribute14", "attribute15", "attribute16", "attribute17", "attribute18", "attribute23", "attribute21", "attribute24", "attribute19", "attribute20", "attribute22", "attribute26", "attribute27", "attribute28", "attribute29"]
    def recordStream = api.stream("PX50", "name", fields, false, *filters)
    if (recordStream) {
        records = recordStream?.collect()
        recordStream?.close()
    }

    records?.collect().each { it ->
        String phKey = (it.name == "S_ZRC") ? "-------------" : (Lib.replaceNull(it.attribute10) + "-" + Lib.replaceNull(it.attribute11) + "-" + Lib.replaceNull(it.attribute12) + "-" + Lib.replaceNull(it.attribute13) + "-" + Lib.replaceNull(it.attribute14) + "-" + Lib.replaceNull(it.attribute15) + "-" + Lib.replaceNull(it.attribute16))
        String conditionName = it.name?.split("_")[1]
        if (api.global.pxGenericPhBasedCount[conditionName][phKey] == null ) api.global.pxGenericPhBasedCount[conditionName][phKey] = 0
        api.global.pxGenericPhBasedCount[conditionName][phKey] += 1
    }

}

Filter getDateFilterForTable(String tableName, String validFromFieldName, String validToFieldName, String KOTABFieldName, String KBETRFieldName) {
    Filter nameFilter = Filter.equal("name", tableName)
    Filter KOTABFilter = Filter.in(KOTABFieldName, api.global.genericConditionList)
    Filter KBETRFilter = Filter.isNotNull(KBETRFieldName)
    Filter dateFilter = libs.__LIBRARY__.GeneralUtility.getFilterConditionForDateRange(validFromFieldName, validToFieldName, out.TargetDate, out.TargetDate)

    return Filter.and(nameFilter, dateFilter, KOTABFilter, KBETRFilter)
}
