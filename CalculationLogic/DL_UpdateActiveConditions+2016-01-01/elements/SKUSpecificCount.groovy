if (!api.global.pxSKUBasedCount) {
    api.global.pxSKUBasedCount = [:]

    def filters = Filter.or(
            getDateFilterForTable("S_ZMC", "attribute1", "attribute2", "attribute4", "attribute3"),
            getDateFilterForTable("S_ZPC", "attribute1", "attribute2", "attribute4", "attribute3"),
            getDateFilterForTable("S_ZPA", "attribute1", "attribute2", "attribute35", "attribute3"),
            getDateFilterForTable("S_ZPP", "attribute1", "attribute2", "attribute4", "attribute3"),
            getDateFilterForTable("S_ZPZ", "attribute1", "attribute2", "attribute30", "attribute3"),
            getDateFilterForTable("S_ZRC", "attribute1", "attribute2", "attribute27", "attribute3"),
            getDateFilterForTable("S_ZPF", "attribute1", "attribute2", "attribute8", "attribute3"),
            getDateFilterForTable("S_ZPM", "attribute1", "attribute2", "attribute6", "attribute3"),
            getDateFilterForTable("S_ZPS", "attribute1", "attribute2", "attribute6", "attribute3"))

    def result
    List fields = ["name", "sku",]
    def recordStream = api.stream("PX50", "name", fields, false, filters)
    if (recordStream) {
        result = recordStream?.collect()
        recordStream?.close()
    }

    api.global.pxSKUBasedCount = result.groupBy({ row -> row.sku + "-" + row.name })?.collectEntries { String key, List groupedValues ->
        [(key): groupedValues.size()]
    }
    api.trace("api.global.pxSKUBasedCount", api.global.pxSKUBasedCount)

}

Filter getDateFilterForTable(String tableName, String validFromFieldName, String validToFieldName, String KOTABFieldName, String KBETRFieldName) {
    Filter nameFilter = Filter.equal("name", tableName)
    Filter KOTABFilter = Filter.in(KOTABFieldName, api.global.SKUConditionList)
    Filter KBETRFilter = Filter.isNotNull(KBETRFieldName)
    Filter dateFilter = libs.__LIBRARY__.GeneralUtility.getFilterConditionForDateRange(validFromFieldName, validToFieldName, out.TargetDate, out.TargetDate)

    return Filter.and(nameFilter, dateFilter, KOTABFilter, KBETRFilter)
}