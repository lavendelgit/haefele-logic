api.local.SKUCount = [:]
if (api.global.pxGenericPhBasedCount) {
    ["ZRC","ZMC"].each { conditionName ->
        int genericCount = Lib.getGenericCount(conditionName , api.global.pxGenericPhBasedCount)
        if (api.local.SKUCount[conditionName] == null) api.local.SKUCount[conditionName] = 0
        api.local.SKUCount[conditionName] += genericCount
    }
}