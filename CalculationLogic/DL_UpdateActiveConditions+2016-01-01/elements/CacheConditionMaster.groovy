api.global.conditionMasterCache = api.global.conditionMasterCache ?: api.findLookupTableValues("ConditionMaster", Filter.equal("attribute3", "Yes"), Filter.equal("attribute6", "Yes")).collectEntries { it ->
  [(it.name): [
     // conditionName  : it.name,
      pxTableName    : it.attribute1,
      conditionTables: it.attribute2?.replaceAll("\\s","")?.split(','),
      pricingType    : it.attribute4
  ]]
}
api.trace("api.global.conditionMasterCache",api.global.conditionMasterCache)
return