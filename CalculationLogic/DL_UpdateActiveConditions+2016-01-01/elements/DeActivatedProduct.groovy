def product = api.currentItem()
if (!api.isDebugMode() && (!product || !product.sku)) {
  api.abortCalculation(true)
}

if (api.isDebugMode()) {
  def productDetails = api.find("P", 0, 1, "sku", Filter.equal("sku", "987.04.433"))
  product = [:]
  productDetails[0].each { k, v ->
    product.put(k, v)
  }
}

return product
//987.04.433
//372.74.511