Constants.CONDITION_FETCH_FROM_DS.each { conditionName ->
    int skuCount = api.global.dsSKUBasedCount[conditionName]? (api.global.dsSKUBasedCount[conditionName][out.DeActivatedProduct?.sku] ?: 0) :0
    int genericCount = Lib.getGenericCount(conditionName , api.global.dsGenericPhBasedCount)
    api.local.SKUCount[conditionName] = genericCount + skuCount
}
//api.trace("api.local.SKUCount",api.local.SKUCount)