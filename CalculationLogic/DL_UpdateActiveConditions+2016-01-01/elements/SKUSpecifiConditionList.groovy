if (!api.global.SKUConditionList) {
    api.global.SKUConditionList = []
    api.global.skuSpecificConditionNameList = []
    for (skuSpecificCondition in api.global.skuSpecificConditionTable) {
        def skuSpecificTableNamesList = skuSpecificCondition.value?.conditions.keySet()
        if (skuSpecificTableNamesList && !Constants.CONDITION_FETCH_FROM_DS.contains(skuSpecificCondition.key)) {
            api.global.skuSpecificConditionNameList << skuSpecificCondition.key
        }
        api.global.SKUConditionList << skuSpecificTableNamesList
    }
    api.global.SKUConditionList = api.global.SKUConditionList?.flatten()?.unique()
}
//api.trace("api.global.SKUConditionList", api.global.SKUConditionList)
//api.trace("api.global.skuSpecificConditionNameList",api.global.skuSpecificConditionNameList)
return