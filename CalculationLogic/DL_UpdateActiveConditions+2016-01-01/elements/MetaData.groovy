if(!api.global.metaData) {
  api.global.metaData = [:]
  List pxTables = api.global.skuSpecificConditionTable?.collect{it.value.pxTableName}
  for( pxTable in pxTables) {
    api.global.metaData[pxTable] = Lib.cachePXMetaData(pxTable)
  }
  pxTables = api.global.genericConditionTable?.collect{it.value.pxTableName}
  for( pxTable in pxTables) {
    api.global.metaData[pxTable] = Lib.cachePXMetaData(pxTable)
  }

 // api.trace("api.global.metaData",api.global.metaData)
}
