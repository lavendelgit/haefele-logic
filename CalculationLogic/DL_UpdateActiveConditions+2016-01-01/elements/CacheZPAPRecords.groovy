if (!api.global.ZPAPDataCache) {
    api.global.ZPAPDataCache = [:]

    def filters = getDateFilterForTable("S_ZPAP", "attribute1", "attribute2", "attribute11", "attribute3")
    api.trace("filters", filters)
    def result
    List fields = ["sku",]
    def recordStream = api.stream("PX50", "name", fields, false, filters)
    if (recordStream) {
        result = recordStream?.collect()
        recordStream?.close()
    }

    api.global.ZPAPDataCache = result?.groupBy({ row -> row.sku })?.collectEntries { String key, List groupedValues ->
        [(key): groupedValues.size()]
    }
}

Filter getDateFilterForTable(String tableName, String validFromFieldName, String validToFieldName, String KOTABFieldName, String KBETRFieldName) {
    Filter nameFilter = Filter.equal("name", tableName)
    //Filter KOTABFilter = Filter.in(KOTABFieldName, api.global.SKUConditionList)
    Filter KBETRFilter = Filter.isNotNull(KBETRFieldName)
    Filter dateFilter = libs.__LIBRARY__.GeneralUtility.getFilterConditionForDateRange(validFromFieldName, validToFieldName, out.TargetDate, out.TargetDate)

    return Filter.and(nameFilter, dateFilter, KBETRFilter)
}