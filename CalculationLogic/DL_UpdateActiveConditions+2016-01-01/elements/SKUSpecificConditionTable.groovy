if (!api.global.skuSpecificConditionTable) {
  api.global.skuSpecificConditionTable = [:]
  api.global.genericConditionTable = [:]
  api.global.conditionConfig.each { tableName, details ->
    api.global.skuSpecificConditionTable[tableName] = [
        pxTableName: details.pxTableName
    ]
    api.global.genericConditionTable[tableName] = [
        pxTableName: details.pxTableName
    ]
    details.each { condition ->
      if (condition.value instanceof Map) {
        api.global.skuSpecificConditionTable[tableName].conditions = condition.value?.findAll {
          it.value.articleFilter?.contains("sku")
        }
        api.global.genericConditionTable[tableName].conditions = condition.value?.findAll {
          it.value.articleFilter?.contains("*")
        }
      }
    }
  }
}
//api.trace("api.global.skuSpecificConditionTable",api.global.skuSpecificConditionTable)
//api.trace("api.global.genericConditionTable",api.global.genericConditionTable)
return