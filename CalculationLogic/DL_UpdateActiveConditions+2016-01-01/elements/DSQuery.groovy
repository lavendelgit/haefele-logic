if (!api.global.dsSKUBasedCount) {
    api.global.dsSKUBasedCount = [:]
    Constants.CONDITION_FETCH_FROM_DS.each { conditionName ->
        def dmCtx = api.getDatamartContext()
        def dm = dmCtx.getDataSource(conditionName)
        def commonFilter = [
                Filter.notEqual("MATNR", "*"),
                Filter.isNotNull('KBETR')
        ]
        commonFilter << out.DateFilter

        def query = dmCtx.newQuery(dm, true)
        query.select("Count(MATNR)", "Count")
        query.select("MATNR", "MATNR")
        query.where(commonFilter)

        def result = dmCtx.executeQuery(query)?.getData()?.collect()

        Map skuCount = [:]
        result?.each { it ->
            skuCount.put(it.MATNR, it.Count)
        }
        api.global.dsSKUBasedCount.put(conditionName,skuCount)
    }
}

