def cachePXMetaData(String tableName) {
    return api.find("PXAM", Filter.equal("name", tableName))?.collectEntries {
        [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
    }
}

protected int getGenericCount(String conditionName, Map phBasedCountMap) {
    int conditionCount = 0
    List PHkeys = out.ProdHKeys
    api.trace("conditionName", conditionName)
    if (phBasedCountMap[conditionName]) {
        PHkeys?.forEach { phKey ->
            if (phBasedCountMap[conditionName][phKey]) {
                conditionCount += phBasedCountMap[conditionName][phKey]
            }
        }
    }
    return conditionCount
}

protected String replaceNull(String attributeValue) {
    return attributeValue ?: "-"
}

protected int getZPAPCount() {

    if (api.global.ZPAPDataCache && api.global.ZPAPDataCache[out.DeActivatedProduct?.sku]) {
        return api.global.ZPAPDataCache[out.DeActivatedProduct?.sku] ?: 0
    }

    return 0

}

protected int getZRPGCount() {

    if (api.global.ZRPGFocusGroupCache && api.global.ZRPGFocusGroupCache[out.DeActivatedProduct?.sku]) {
        String focusGroup = api.global.ZRPGFocusGroupCache[out.DeActivatedProduct?.sku]
        return (focusGroup && api.global.focusGroupRecordsCount[focusGroup]) ? api.global.focusGroupRecordsCount[focusGroup] : 0
    }

    return 0

}