if (!api.global.conditionConfig) {
  api.global.conditionConfig = [:]


  for (conditionMaster in api.global.conditionMasterCache) {
    Map conditionTableEntry = [:]
    for (conditionTable in conditionMaster.value.conditionTables) {
      tableDetails = api.global.conditionTableMasterCache[conditionTable]
      conditionTableEntry[conditionTable] = tableDetails
    }
    api.global.conditionConfig[conditionMaster.key] = [
        pxTableName    : conditionMaster.value.pxTableName,
        conditionTables: conditionTableEntry
    ]
  }

}
api.trace("api.global.conditionConfig", api.global.conditionConfig)
return