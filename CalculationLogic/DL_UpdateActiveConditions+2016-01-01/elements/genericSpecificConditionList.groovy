if (!api.global.genericConditionList) {
    api.global.genericConditionList = []
    api.global.genericConditionNameList = []
    for (condition in api.global.genericConditionTable) {
        def genericTableNamesList = condition.value?.conditions.keySet()
        if (genericTableNamesList) api.global.genericConditionNameList << condition.key
        api.global.genericConditionList << genericTableNamesList
    }
    api.global.genericConditionList = api.global.genericConditionList?.flatten()?.unique()
}
//api.trace("api.global.genericConditionList", api.global.genericConditionList)
//api.trace("api.global.genericConditionNameList",api.global.genericConditionNameList)
return

