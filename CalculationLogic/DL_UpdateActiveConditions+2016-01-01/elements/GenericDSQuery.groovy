if (!api.global.dsGenericPhBasedCount) {
    api.global.dsGenericPhBasedCount = [:]

    Constants.CONDITION_FETCH_FROM_DS.each { conditionName ->
        def ctx = api.getDatamartContext()
        def dm = ctx.getDataSource(conditionName)
        def commonFilter = [
                Filter.in("MATNR", ["*"]),
                Filter.isNotNull('KBETR')]

        commonFilter << out.DateFilter
        def query = ctx.newQuery(dm, false)
        query.select("MATNR")
        query.select("ZZPRODH1")
        query.select("ZZPRODH2")
        query.select("ZZPRODH3")
        query.select("ZZPRODH4")
        query.select("ZZPRODH5")
        query.select("ZZPRODH6")
        query.select("ZZPRODH7")
        query.where(commonFilter)

        def result = ctx.executeSqlQuery(getSqlQuery(), query)

        Map genericPhCount = [:]
        result?.toResultMatrix()?.collect().entries[0].each { it ->
            genericPhCount.put(it.phkey, it.count)
        }
        api.global.dsGenericPhBasedCount.put(conditionName, genericPhCount)
    }
}

protected String getSqlQuery() {
    def sql = """ 
        SELECT PHKey, Count(PHKey) as Count FROM 
            (
                SELECT 
                    COALESCE(ZZPRODH1,'-') || '-' || COALESCE(ZZPRODH2,'-') || '-' || COALESCE(ZZPRODH3,'-') || '-' || COALESCE(ZZPRODH4,'-') || '-' || COALESCE(ZZPRODH5,'-') || '-' || COALESCE(ZZPRODH6,'-') || '-' || COALESCE(ZZPRODH6,'-') AS PHKey
                FROM T1
            ) IT1  
            Group By PHKey
    """

    return sql
}