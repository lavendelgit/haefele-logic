def ctx = api.getDatamartContext()
def dsCCIoM = ctx.getDataSource("CustomerCategorizationImpactOnMargins")

def query = ctx.newQuery(dsCCIoM)
query.select("CustomerId")
query.select("InvoiceDateYear")
query.select("ProdhII","ProductGroup")
query.select("SalesType")
query.select("Verkaufsbereich3")
query.select("CustomerPotentialGroup")
query.select("p1_Revenue","Revenue")
query.select("p2_Units","Units")
query.select("p3_Material","ArticleCount")
query.select("p4_MarginAbs", "Margin")
def results = ctx.executeQuery(query)

while(results?.next()){
    def row = results?.get()  // current row as map
    api.trace("streamQuery", "row $r", row)
    r++
}
results?.close()