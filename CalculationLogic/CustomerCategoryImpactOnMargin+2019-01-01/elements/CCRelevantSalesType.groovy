/**
 * get all the sales type where customer category discount is applicable.
*/
if (!api.local.allCCSalesType) {
    api.local.allCCSalesType = [:]
    def filters = [
            Filter.equal("attribute5", "Yes")
    ]
    def salesTypesList = api.findLookupTableValues("SalesType", *filters)
    salesTypesList.each { entry ->
        api.local.allCCSalesType[entry.name] = [entry.name, entry.attribute1, entry.attribute5]
        api.trace ("Values ... " + entry.name, entry.attribute1 +" " + entry.attribute5)
    }
    api.trace("Pringing the sales type values", " " + api.local.allCCSalesType.keySet())
}

return api.local.allCCSalesTypes