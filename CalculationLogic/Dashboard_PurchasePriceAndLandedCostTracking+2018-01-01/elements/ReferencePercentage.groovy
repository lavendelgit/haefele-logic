return decimalEntry('ReferencePercentage', 'Reference %', true)

Object decimalEntry(String name, String label, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.decimalUserEntry(inputName)
    }
}

Object newInput(String name, String label, boolean isRequired, defaultValue, Closure inputCreator) {
    def input = inputCreator.call(name)
    def param = api.getParameter(name)
    if (param) {
        param.setLabel(label)
        param.setRequired(isRequired)
        if (param.getValue() == null && defaultValue != null) {
            param.setValue(defaultValue)
        }
    }
    return input
}