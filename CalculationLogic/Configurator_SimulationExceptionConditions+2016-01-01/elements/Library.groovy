import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ContextParameter

/**
 * This is a "hack" to save the input value that is not persisted between logic refreshes on changes
 * @param calculationType
 * @return
 */
protected ConfiguratorEntry createInputConfiguratorWithHiddenStoredValue(String pageName) {
    def inputConfigurator = api.createConfiguratorEntry(InputType.HIDDEN, "calculationType")
    if (pageName) {
        inputConfigurator.getFirstInput().setValue(pageName)
    }

    return inputConfigurator
}


private ConfiguratorEntry createDatamartFilterBuilder(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String dmName, List filters, Closure selValueHandler = null) {
    def filterInput = configuratorEntry.createParameter(InputType.DMFILTERBUILDER, fieldName)
    filterInput.addParameterConfigEntry("dmSourceName", dmName)
    filterInput.addParameterConfigEntry("noRefresh", true)
    filterInput.setLabel(fieldLabel)
               .setRequired(false)
               .setReadOnly(false)


    if (filters) {
        Map filterConfig = [:]
        filterConfig['criteria'] = Filter.and(*filters)
        filterInput.setFilter(filterConfig)
    }

    if (selValueHandler) {
        String strFilter = null
        if (filterInput.getValue() instanceof Map) {
            strFilter = api.filterFromMap(filterInput.getValue()).toQueryString()
        }
        selValueHandler.call(strFilter)
    }

    return configuratorEntry
}


protected ConfiguratorEntry createExceptionMatrixInput(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, List matrixRows, Map inputs) {
    ContextParameter matrix = configuratorEntry.createParameter(InputType.INPUTMATRIX, fieldName)
    Map COLUMNS = Constants.INPUTS
    List columnDefs = COLUMNS.values() as List
    matrix.setLabel(fieldLabel)
    matrix.addParameterConfigEntry('columns', columnDefs.label)
    matrix.addParameterConfigEntry("canModifyRows", true)
    matrix.addParameterConfigEntry("disableRowSelection", true)
    matrix.addParameterConfigEntry("columnType", columnDefs.matrixColType)
    matrix.addParameterConfigEntry("noCellRefresh", true)

    api.logInfo("############ createExceptionMatrixInput : inputs ", api.jsonEncode(inputs))
    inputs.IsUpdate = false
    matrixRows = matrixRows ?: []
    matrixRows = matrixRows.collect { Map matrixRow ->
        if (matrixRow.Key == inputs.ExceptionKey) {
            if (inputs.ExceptionFilter && (inputs.GrossPriceChangeByPercent != null
                    || inputs.PurchasePriceChangeByPercent != null
                    || inputs.QuantityChangeByPercent != null)) {
                COLUMNS.each { String columnName, Map columnDef ->
                    matrixRow[columnDef.label] = inputs[columnName]
                }
            }
            inputs.IsUpdate = true
        }
        return matrixRow
    }

    api.logInfo("############ createExceptionMatrixInput : matrixRows ", api.jsonEncode(matrixRows))

    if (!inputs.IsUpdate) {
        if (inputs.ExceptionKey
                && inputs.ExceptionFilter
                && (inputs.GrossPriceChangeByPercent != null
                || inputs.PurchasePriceChangeByPercent != null
                || inputs.QuantityChangeByPercent != null)) {
            matrixRows.add(
                    COLUMNS.collectEntries { String columnName, Map columnDef ->
                        [(columnDef.label): inputs[columnName]]
                    }
            )
        }
    }

    matrix.setValue(matrixRows)

    return configuratorEntry
}

protected ConfiguratorEntry createPercentField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.USERENTRY, fieldName)
    input.setValueHint('%')
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    return configuratorEntry
}

protected ConfiguratorEntry createTextField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.STRINGUSERENTRY, fieldName)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    return configuratorEntry
}

protected ConfiguratorEntry createHiddenField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.HIDDEN, fieldName)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    return configuratorEntry
}

protected void manageFieldAttributes(ContextParameter input, String fieldLabel, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    input.setRequired(isRequired)
         .setReadOnly(isReadOnly)
         .setLabel(fieldLabel)
         .addParameterConfigEntry("noRefresh", isNoRefresh)

    if (selValueHandler) {
        selValueHandler.call(input.getValue())
    }
    input.setValue('')
}

protected boolean hasFieldValueChanged(String fieldName) {
    return api.input(fieldName) != api.input(getStateFieldName(fieldName))
}

protected String getStateFieldName(String fieldName) {
    return "__${fieldName}__"
}

protected def getPrevFieldValue(String fieldName) {
    return api.input(getStateFieldName(fieldName))
}

