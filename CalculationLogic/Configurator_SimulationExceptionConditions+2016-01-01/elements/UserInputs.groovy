import net.pricefx.server.dto.calculation.ConfiguratorEntry

List matrixRows = getMatrixRows()

Map inputs = [:]
ConfiguratorEntry inputConfigurator = Library.createInputConfiguratorWithHiddenStoredValue("Exceptions")

Constants.INPUTS.each { String inputName, Map inputDef ->
    switch (inputDef.type) {
        case "DatamartFilter":
            Library.createDatamartFilterBuilder(inputConfigurator, inputName, inputDef.label, Constants.DM_SIMULATION, []) {
                selValue -> inputs[inputName] = selValue
            }
            break
        case "Percent":
            Library.createPercentField(inputConfigurator, inputName, inputDef.label, null, false, false, true) {
                selValue -> inputs[inputName] = selValue
            }
            break
        case "Text":
            Library.createTextField(inputConfigurator, inputName, inputDef.label, null, false, false, true) {
                selValue -> inputs[inputName] = selValue
            }
            break
    }
}

Library.createExceptionMatrixInput(inputConfigurator, 'ExceptionMatrix', 'All Exceptions', matrixRows, inputs)

return inputConfigurator

protected List getMatrixRows() {
    def inpMatrixValue = api.input('ExceptionMatrix')
    return ((inpMatrixValue instanceof List) ? inpMatrixValue : []) as List
}