import groovy.transform.Field

@Field String DM_SIMULATION = 'PriceChangeSimulationDM_2020'
@Field Map PAGE_DEF = [name     : 'ExceptionsConfigurator',
                       title    : 'Exceptions',
                       fieldList: [
                               []
                       ]]

@Field Map INPUTS = [
        ExceptionKey                : [name: 'ExceptionKey', type: 'Text', matrixColType: 'Text', label: 'Key'],
        ExceptionFilter             : [name: 'ExceptionFilter', type: 'DatamartFilter', matrixColType: 'Text', label: 'Data Filter'],
        GrossPriceChangeByPercent   : [name: 'GrossPriceChangeByPercent', type: 'Percent', matrixColType: 'Numeric', label: 'Gross Price Change Percent'],
        PurchasePriceChangeByPercent: [name: 'PurchasePriceChangeByPercent', type: 'Percent', matrixColType: 'Numeric', label: 'Purchase Price Change Percent'],
        QuantityChangeByPercent     : [name: 'QuantityChangeByPercent', type: 'Percent', matrixColType: 'Numeric', label: 'Quantity Change Percent']
]

