if (api.isSyntaxCheck()) return [:]

def dmCtx = api.getDatamartContext()
def datamartQuery = dmCtx.newQuery(dmCtx.getTable("TransactionsDM"))
def timeFrame = api.getElement("TimeFrame")
def orderType = api.getElement("OrderType")
def pgm = api.getElement("ProductGroupManager")
def yearAgo = api.getElement("YearAgo")
def salesOffice = api.getElement("SalesOffice")
def customerSector = api.getElement("CustomerSector")

def filters = [
    Filter.greaterThan("Revenue", 0),
    Filter.greaterThan("Units", 0),
    Filter.greaterThan("MarginAbs", 0),
    Filter.greaterThan("Cost", 0),
    Filter.greaterThan("SalesPricePer100Units", 0),
    Filter.in("SalesOffice","DE01", "DE02", "DE03", "DE04", "DE05", "DE06", "DE07", "DE08", "DE09", "DE10", "DE20"),
    Filter.notEqual("InvoicePos", "999999"),
]

if (timeFrame == "Last 12 months") {
    filters.add(Filter.greaterOrEqual("InvoiceDate", yearAgo))
}

if (orderType) {
    filters.add(Filter.equal("OrderType", orderType))
}

if (pgm) {
    filters.add(Filter.equal("SM", pgm))
}

if (salesOffice != "All") {
    filters.add(Filter.equal("Verkaufsbereich3", salesOffice))
}


if (customerSector) {
    filters.add(Filter.in("Branche", customerSector))
}

/*
if (api.isUserInGroup("PGM", api.user().loginName)) {
    def pgm = api.findLookupTableValues("ProductGroupManager", Filter.equal("name", api.user().loginName))?.find()?.value
    api.trace("pgm", "", pgm)
    filters.add(Filter.equal("SM", pgm))
}
*/

datamartQuery.select("YearMonthMonth", "yearMonth")
datamartQuery.select("SalesType", "salesType")
datamartQuery.select("COUNT(CustomerId)", "count")
datamartQuery.select("SUM(Revenue)", "revenue")
datamartQuery.select("SUM(MarginAbs)", "marginAbs")
datamartQuery.orderBy("yearMonth asc")
datamartQuery.where(*filters)

def result = dmCtx.executeQuery(datamartQuery)

if (result == null) {
    api.addWarning("Could not get result for Sales query.")
}

api.trace("result.summary", "", result?.getSummary())

result?.getData()
        ?.collect()
        ?.groupBy { r -> r.salesType ?: "Unknown"}
        ?.sort({ e1, e2 ->
            def num1 = e1.key == "Unknown" ? 1000 : (e1.key.substring(1) as Integer)
            def num2 = e2.key == "Unknown" ? 1000 : (e2.key.substring(1) as Integer)
            return num1 <=> num2
        })