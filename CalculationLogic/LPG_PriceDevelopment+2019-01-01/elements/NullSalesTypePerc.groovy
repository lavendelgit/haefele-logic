def nullCount = api.getElement("Transactions")?.nullCount
def totalCount = api.getElement("Transactions")?.totalCount
api.trace ("Inputs details", nullCount+"insideCount"+totalCount)

if (nullCount != null && totalCount > 0) {
    return nullCount / totalCount
}
