def insideCount = api.getElement("Transactions")?.insideCount
def totalCount = api.getElement("Transactions")?.totalCount
api.trace ("Inputs details", insideCount+"insideCount"+totalCount)

if (insideCount != null && totalCount > 0) {
    return insideCount / totalCount
}
