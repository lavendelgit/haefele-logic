def outsideCount = api.getElement("Transactions")?.outsideCount
def totalCount = api.getElement("Transactions")?.totalCount
api.trace ("Inputs details", outsideCount+"insideCount"+totalCount)


if (outsideCount != null && totalCount > 0) {
    return outsideCount / totalCount
}