def activeCount = 0
def lowest = null
def highest = null

def targetDate = api.targetDate()?.format("yyyy-MM-dd")
def yearAgo = api.getElement("YearAgo")?.format("yyyy-MM-dd")

def stream = api.stream("PX", "attribute3", ["attribute2", "attribute3"],
                        Filter.equal("name", "S_ZPAP"),
                        Filter.equal("sku", api.getElement("SKU")),
                        Filter.lessOrEqual("attribute1", targetDate),
                        Filter.greaterOrEqual("attribute2", targetDate),
                        Filter.in("attribute35", "Kaltenkirchen", "Hannover", "Berlin", "Münster", "Köln", "Frankfurt", "Naumburg", "Nürnberg", "Stuttgart Airport", "München", "Handel"),
                        )

if (!stream) {
    return [:]
}

while (stream.hasNext()) {
    def current = stream.next()
    def currentPrice = current.attribute3
    activeCount++
    if (lowest == null || currentPrice < lowest) {
        lowest = currentPrice
    }
    if (highest == null || currentPrice > highest) {
        highest = currentPrice
    }
}

stream.close()

return [
        activeCount: activeCount,
        lowest     : lowest,
        highest    : highest
]