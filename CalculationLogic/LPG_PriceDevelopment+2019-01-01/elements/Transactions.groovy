if (api.isSyntaxCheck()) {
    return [data:[], summary:[:]]
}
/*
def dmCtx = api.getDatamartContext()
def datamartQuery = dmCtx.newQuery(dmCtx.getTable("TransactionsDM"),false)

datamartQuery.select("SalesPricePer100Units", "salesPrice")
datamartQuery.orderBy("salesPrice asc")

datamartQuery.where(Filter.greaterOrEqual("InvoiceDate", api.getElement("YearAgo")))
datamartQuery.where(Filter.isNotNull("SalesPricePer100Units"))
datamartQuery.where(Filter.notEqual("SalesPricePer100Units", 0))
datamartQuery.where(Filter.equal("Material", api.getElement("SKU")))

def result = dmCtx.executeQuery(datamartQuery)

result?.calculateSummaryStatistics()

return [
        data: result?.getData()?.collect(),
        summary: result?.getSummary()
]

 */
def sku = api.getElement("SKU")
def yearAgo = api.getElement("YearAgo")
def baseSalesPrice = api.getElement("SalesPrice")
def minSalesPrice = baseSalesPrice?.multiply(0.7)

def ctx = api.getDatamartContext()

api.trace("sku","", sku)

def dm1 = ctx.getDataSource("Transactions")
def q1 = ctx.newQuery(dm1)
q1.selectAll(true)
q1.where(Filter.greaterOrEqual("InvoiceDate", yearAgo))
q1.where(Filter.isNotNull("SalesPricePer100Units"))
q1.where(Filter.notEqual("SalesPricePer100Units", 0))
q1.where(Filter.equal("Material", sku))
q1.where(Filter.in("SalesOffice","DE01", "DE02", "DE03", "DE04", "DE05", "DE06", "DE07", "DE08", "DE09", "DE10", "DE20"))

def sql = """
    SELECT 
        MIN(SalesPricePer100Units) as lowestSalesPrice,
        MAX(SalesPricePer100Units) as highestSalesPrice,
        AVG(SalesPricePer100Units) as avgSalesPrice,
        AVG(CASE WHEN CustomerPotentialGroup = 'R0' THEN SalesPricePer100Units END) as avgSalesPriceR0,
        AVG(CASE WHEN CustomerPotentialGroup = 'R1' THEN SalesPricePer100Units END) as avgSalesPriceR1,
        AVG(CASE WHEN CustomerPotentialGroup = 'R2' THEN SalesPricePer100Units END) as avgSalesPriceR2,
        AVG(CASE WHEN CustomerPotentialGroup = 'R3' THEN SalesPricePer100Units END) as avgSalesPriceR3,
        COUNT(1) as totalCount,
        COUNT(CASE WHEN SalesTypeGroup = 'Inside' THEN 1 ELSE NULL END) as insideCount,
        COUNT(CASE WHEN SalesTypeGroup = 'Outside' THEN 1 ELSE NULL END) as outsideCount,
        COUNT(CASE WHEN SalesTypeGroup = 'Unknown' THEN 1 ELSE NULL END) as unknownCount
    """

if (baseSalesPrice) {
    sql += """,
        COUNT(CASE WHEN SalesPricePer100Units > ${baseSalesPrice} THEN 1 ELSE NULL END) as aboveBasePrice,
        COUNT(CASE WHEN SalesPricePer100Units <= ${baseSalesPrice} AND SalesPricePer100Units >= ${minSalesPrice} THEN 1 ELSE NULL END) as insidePriceRange,
        COUNT(CASE WHEN SalesPricePer100Units < ${minSalesPrice} THEN 1 ELSE NULL END) as belowMinPrice
    """
}

sql += "FROM T1"

api.trace("sql", "", sql)

def result = ctx.executeSqlQuery(sql, q1)

return result?.collect { r -> [
        lowestSalesPrice:  r.lowestsalesprice,
        highestSalesPrice: r.highestsalesprice,
        avgSalesPrice:     r.avgsalesprice,
        avgSalesPriceR0:   r.avgsalespricer0,
        avgSalesPriceR1:   r.avgsalespricer1,
        avgSalesPriceR2:   r.avgsalespricer2,
        avgSalesPriceR3:   r.avgsalespricer3,
        totalCount:        r.totalcount,
        insideCount:       r.insidecount,
        outsideCount:      r.outsidecount,
        unknownCount:      r.unknowncount,
        nullCount:         r.totalcount - (r.insidecount + r.outsidecount + r.unknowncount),
        aboveBasePrice:    r.abovebaseprice,
        insidePriceRange:  r.insidepricerange,
        belowMinPrice:     r.belowminprice
]}?.find() ?: [:]