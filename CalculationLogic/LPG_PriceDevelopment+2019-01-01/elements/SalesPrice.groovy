def sku = api.product("sku")
def filter = [
  Filter.equal("attribute7","EUR")
]
lib.Find.currentSalesPrice(sku, *filter)
