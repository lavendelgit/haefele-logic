def unknownCount = api.getElement("Transactions")?.unknownCount
def totalCount = api.getElement("Transactions")?.totalCount
api.trace ("Inputs details", unknownCount+"insideCount"+totalCount)

if (unknownCount != null && totalCount > 0) {
    return unknownCount / totalCount
}