def baseCost = api.getElement("BaseCost")
def salesPrice = api.getElement("SalesPrice")

if (baseCost == null) {
    api.redAlert("ZPE is not defined")
    return null
}

if (salesPrice == null || !salesPrice) {
    api.redAlert("ZPLP is not defined")
    return null
}

return (salesPrice - baseCost) / salesPrice
