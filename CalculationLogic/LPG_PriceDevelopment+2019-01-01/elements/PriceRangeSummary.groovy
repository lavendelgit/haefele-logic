def transactions = api.getElement("Transactions")

def totalCount = transactions.totalCount
def aboveBasePrice = transactions.aboveBasePrice
def insidePriceRange = transactions.insidePriceRange
def belowMinPrice = transactions.belowMinPrice

return [
        aboveCount: aboveBasePrice,
        abovePerc: (aboveBasePrice != null && totalCount) ? (aboveBasePrice / totalCount) : null,
        insideCount: insidePriceRange,
        insidePerc: (insidePriceRange != null && totalCount) ? (insidePriceRange / totalCount) : null,
        belowCount: belowMinPrice,
        belowPerc: (belowMinPrice != null && totalCount) ? (belowMinPrice / totalCount) : null
]