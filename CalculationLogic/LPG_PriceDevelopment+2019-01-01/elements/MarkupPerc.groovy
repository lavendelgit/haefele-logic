def baseCost = api.getElement("BaseCost")
def salesPrice = api.getElement("SalesPrice")

if (baseCost == null) {
    api.redAlert("ZPE is not defined")
    return null
}

if (baseCost == 0) {
    api.redAlert("ZPE is 0")
    return null
}

if (salesPrice == null) {
    api.redAlert("ZPLP is not defined")
    return null
}

return (salesPrice - baseCost) / baseCost
