def sku10 = api.getElement("Sku10")
def key = "${sku10}"

api.trace("key", null, key)
api.trace("api.global.rabattgruppeCache", null, api.global.rabattgruppeCache)

def cache = api.global.rabattgruppeCache
def result = cache.get(key)

if (result == null) {
  result = api.findLookupTableValues("Rabattgruppe", api.filter("key2", sku10))
  if (result != null) {
    if (result.isEmpty()) {
      result = null
    } else {
      api.trace("result[0]", null, result[0])
      def key2 = result[0].key2
      api.trace("key2", null, key2)
      api.trace("key2", null, (key2?.equalsIgnoreCase(sku10)))
	  if (key2?.equalsIgnoreCase(sku10)) {
      	result = result[0].key1
      } else {
        result = null
      }
    }
  }
  if (result == null) {
    result = -1;
  }
  cache.put(key, result)
}

api.trace("api.global.rabattgruppeCache", null, api.global.rabattgruppeCache)
if (result == -1) return null
result