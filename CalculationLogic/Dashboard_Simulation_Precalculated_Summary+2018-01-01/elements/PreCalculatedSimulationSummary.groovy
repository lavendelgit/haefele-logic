return api.findLookupTableValues("Simulation_Precalculated_Summary")?.collect {
    [("Year")         : (it.name),
     ("Revenue")      : (it.attribute1),
     ("Margin")       : (it.attribute2),
     ("Quantity")     : (it.attribute3),
     ("PurchasePrice"): (it.attribute4)]
}
