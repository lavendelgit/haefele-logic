import net.pricefx.common.api.FieldFormatType

def calculatedSummary = out.SimulationCalculation
String column1 = "Pricing Strategy"
String column2 = "Umsatz"
String column3 = "DB"
String column4 = "DB %"
String column5 = "Menge"
String column6 = "EK Wert"
String column7 = "Aufschlag"
String column8 = "Grundpreis"
String column9 = "Rabatt"
String column10 = "VK-Preis"
String column11 = "Grundpreis Erhöhung"
List columns = [column1, column2, column3, column4, column5, column6, column7, column8, column9, column10, column11]
def columnTypes = [FieldFormatType.TEXT,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.PERCENT, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.PERCENT,
                   FieldFormatType.INTEGER, FieldFormatType.PERCENT,
                   FieldFormatType.INTEGER, FieldFormatType.PERCENT]

def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Gross Price Prediction Simulator', columns, columnTypes)
for (rows in calculatedSummary) {
    resultMatrix.addRow([
            (column1) : rows.column1,
            (column2) : rows.column2,
            (column3) : rows.column3,
            (column4) : rows.column4,
            (column5) : rows.column5,
            (column6) : rows.column6,
            (column7) : rows.column7,
            (column8) : rows.column8,
            (column9) : rows.column9,
            (column10): rows.column10,
            (column11): rows.column11,
    ])
}
resultMatrix.setPreferenceName("Gross Price Prediction Simulator")
return resultMatrix