import groovy.transform.Field

@Field final List SIMULATION_SCENARIO = ["Keep The Same Margin", "Keep The Same Margin %"]
@Field final List COLUMN_TITLE = ["Ausgangssituation", "Selber %DB", "Selber DB absolut"]