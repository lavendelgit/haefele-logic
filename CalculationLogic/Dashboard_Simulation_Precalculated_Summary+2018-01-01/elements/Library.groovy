def getSameMarginPercent(def calculatedRow) {
    BigDecimal revenue, margin, marginPercent, quantity, purchasePrice, surcharge, grossPrice, lossDueToDiscounting, retailPrice, basePriceIncreasePercent
    BigDecimal previousRevenue
    quantity = calculatedRow.column5
    purchasePrice = calculatedRow.column6 * (1 + out.PurchasePriceIncrease / 100)
    marginPercent = calculatedRow.column3 / calculatedRow.column2
    retailPrice = purchasePrice / (1-marginPercent)
    previousRevenue = calculatedRow.column2
    revenue = retailPrice
    lossDueToDiscounting = ((out.DiscountingConditionLoss / 100) * revenue) / previousRevenue
    grossPrice = retailPrice + lossDueToDiscounting * retailPrice
    margin = marginPercent * revenue
    basePriceIncreasePercent = (grossPrice - calculatedRow.column8) / calculatedRow.column8
    surcharge = (grossPrice - purchasePrice) / purchasePrice
    api.trace("lossDueToDiscounting", lossDueToDiscounting)
    return [
            column1 : Constants.COLUMN_TITLE[1]  ,
            column2 : revenue,
            column3 : margin,
            column4 : marginPercent,
            column5 : quantity,
            column6 : purchasePrice,
            column7 : surcharge,
            column8 : grossPrice,
            column9 : lossDueToDiscounting,
            column10: retailPrice,
            column11: basePriceIncreasePercent]
}

def getSameMargin(def calculatedRow) {
    BigDecimal revenue, margin, marginPercent, quantity, purchasePrice, surcharge, grossPrice, lossDueToDiscounting, retailPrice, basePriceIncreasePercent
    BigDecimal previousRevenue
    quantity = calculatedRow.column5
    purchasePrice = calculatedRow.column6 * (1 + out.PurchasePriceIncrease / 100)
    margin = calculatedRow.column3
    revenue = purchasePrice + margin
    retailPrice = revenue
    previousRevenue = calculatedRow.column2
    lossDueToDiscounting =  ((out.DiscountingConditionLoss / 100) * revenue) / previousRevenue
    grossPrice = retailPrice / (1 - lossDueToDiscounting)
    marginPercent = margin / revenue
    surcharge = (grossPrice - purchasePrice) / purchasePrice
    basePriceIncreasePercent = (grossPrice - calculatedRow.column8) / calculatedRow.column8

    return [
            column1 : Constants.COLUMN_TITLE[2],
            column2 : revenue,
            column3 : margin,
            column4 : marginPercent,
            column5 : quantity,
            column6 : purchasePrice,
            column7 : surcharge,
            column8 : grossPrice,
            column9 : lossDueToDiscounting,
            column10: retailPrice,
            column11: basePriceIncreasePercent]
}

