def preCalculatedSimulationSummary = out.PreCalculatedSimulationSummary?.find { it.Year == out.AnalysisYear }
BigDecimal revenue, margin, marginPercent, quantity, purchasePrice, surcharge, grossPrice, lossDueToDiscounting, retailPrice, basePriceIncreasePercent
quantity = preCalculatedSimulationSummary.Quantity
purchasePrice = preCalculatedSimulationSummary.PurchasePrice
lossDueToDiscounting = out.DiscountingConditionLoss / 100
grossPrice = preCalculatedSimulationSummary.Revenue / (1 - lossDueToDiscounting)
surcharge = (grossPrice - purchasePrice) / purchasePrice
retailPrice = (1 - lossDueToDiscounting) * grossPrice
revenue = retailPrice
margin = revenue - purchasePrice
marginPercent = margin / revenue
basePriceIncreasePercent = 0
List calculatedRows = []
Map row1 = [:]
row1.column1 = Constants.COLUMN_TITLE[0]+ " ${out.AnalysisYear}"
row1.column2 = revenue
row1.column3 = margin
row1.column4 = marginPercent
row1.column5 = quantity
row1.column6 = purchasePrice
row1.column7 = surcharge
row1.column8 = grossPrice
row1.column9 = lossDueToDiscounting
row1.column10 = retailPrice
row1.column11 = basePriceIncreasePercent
api.trace("row1", row1)
calculatedRows << row1
if (out.PriceSimulationScenario.contains("Keep The Same Margin %")) {
    calculatedRows << Library.getSameMarginPercent(row1)
}
if (out.PriceSimulationScenario.contains("Keep The Same Margin")) {
    calculatedRows << Library.getSameMargin(row1)
}
api.trace("row2", calculatedRows)
return calculatedRows

