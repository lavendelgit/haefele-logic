//SalesPersonDiscount.testSalesPersonDiscount(sku, customerId, focusGroup, transactionDate, lookBackDays)
/*

def testOrder(inputList, first, second, third, fourth, int runCount) {
  int iteration = 0
  while (iteration < runCount) {
	SalesPersonDiscount.testSalesPersonDiscount(inputList[first].sku, inputList[first].customerId, inputList[first].focusGroup, inputList[first].transactionDate, inputList[first].lookBackDays)
	SalesPersonDiscount.testSalesPersonDiscount(inputList[second].sku, inputList[second].customerId, inputList[second].focusGroup, inputList[second].transactionDate, inputList[second].lookBackDays)
	SalesPersonDiscount.testSalesPersonDiscount(inputList[third].sku, inputList[third].customerId, inputList[third].focusGroup, inputList[third].transactionDate, inputList[third].lookBackDays)
	SalesPersonDiscount.testSalesPersonDiscount(inputList[fourth].sku, inputList[fourth].customerId, inputList[fourth].focusGroup, inputList[fourth].transactionDate, inputList[fourth].lookBackDays)
  	iteration++
  }
}

def inputList = [:]
inputList["0"] = [
	sku:"940.84.040",
  	customerId:"0002144416",
  	focusGroup:"",
  	transactionDate:"2018-06-14",
  	lookBackDays:31
]
inputList["1"] = [
	sku:"556.90.614",
  	customerId:"0002144416",
  	focusGroup:"",
  	transactionDate:"2018-06-27",
  	lookBackDays:31
]
inputList["2"] = [
	sku:"",
  	customerId:"0001239631",
  	focusGroup:"HDE27",
  	transactionDate:"2018-06-30",
  	lookBackDays:31
]
inputList["3"] = [
	sku:"550.75.064",
  	customerId:"0001212832",
  	focusGroup:"HDE26",
  	transactionDate:"2018-06-01",
  	lookBackDays:31
]

//Simple Testing
testOrder(inputList, "0", "1", "2", "3", 1)
testOrder(inputList,  "1", "2", "3", "0",1)
testOrder(inputList, "0", "2", "1", "3", 1)
testOrder(inputList, "0", "1", "3", "2", 1)
//Testing for the 1000
//Testing for the 10,000 times

//Assumption for the cases: The data lookup is studied and accordingly 4 records are picked-up for this testing.

//Best Case : All records are coming as incremental customer id with repeat of the customer. Majority of them are found.
//Worst Case : All records are coming as discrete customer id with no repeat and everytime missing information.
//Normal Case: You have mixed records they are coming with customer id grouping. But for some of the customers the records 

*/