def latestCustomerSalesPriceRecord2(String sku, String customerId, String transactionDate) {
    def filters = [
            Filter.equal("name", "CustomerSalesPrice"),
            Filter.equal("sku", sku),
    ]
    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    def orderBy = "-attribute2" // Valid From (de: Gültig von) - descending
    def records = api.find("PX30", 0, 1, orderBy, *filters)
    def result = [:]

    if (records[0]) {
        result = records[0]
      	api.trace ("Record Found...",records[0].attribute1)
    }

    api.trace("lib.Find.latestCustomerSalesPriceRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result
}
