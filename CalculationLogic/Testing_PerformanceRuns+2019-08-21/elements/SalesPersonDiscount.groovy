def testSalesPersonDiscount(sku, customerId, focusGroup, transactionDate, lookBackDays) {

transactionDate = (transactionDate)? Date.parse("yyyy-MM-dd", transactionDate):new Date()
def fromFilterDate = (api.global.fromFilterDate)?Date.parse("yyyy-MM-dd", api.global.fromFilterDate):new Date()
def toFilterDate = (api.global.toFilterDate)?Date.parse("yyyy-MM-dd", api.global.toFilterDate):new Date()
if (api.global.salesPersonDiscounts == null || transactionDate.compareTo(fromFilterDate) < 0 || transactionDate.compareTo(toFilterDate) > 0)  {
    def salesPersonDiscounts = [:]

  	api.global.fromFilterDate = transactionDate.minus(lookBackDays)?.format("yyyy-MM-dd")
    api.global.toFilterDate = transactionDate.plus(lookBackDays)?.format("yyyy-MM-dd")
    api.global.salesPersonDiscounts = salesPersonDiscounts
	fromFilterDate = Date.parse("yyyy-MM-dd", api.global.fromFilterDate)
	toFilterDate = Date.parse("yyyy-MM-dd", api.global.toFilterDate)

  	api.trace("Printing the date range ", "FromFilterDate ("+fromFilterDate.format("yyyy-MM-dd")  +") To Filter date ("+toFilterDate.format("yyyy-MM-dd")+") transactionDate("+transactionDate.format("yyyy-MM-dd")+")")
  	
  	def attributes = [
      "customerId",
      "attribute1",
      "attribute2",
      "attribute3",
      "attribute4",
      "attribute5"
    ]
    fmFromFilterDate = fromFilterDate.format("yyyy-MM-dd")
    fmToFilterDate = toFilterDate.format("yyyy-MM-dd")
  	def fromDateIfEarlierThanStartToDateShouldOverlap = Filter.and(Filter.lessOrEqual("attribute3", fmFromFilterDate), Filter.greaterOrEqual("attribute4",fmToFilterDate))
  	def fromDateIfFurtherThanStartShouldOverlap = Filter.and(Filter.greaterThan("attribute3", fmFromFilterDate), Filter.lessOrEqual("attribute3",fmFromFilterDate), Filter.greaterThan("attribute4",fmFromFilterDate))
  	def filters = [
      Filter.equal("name", "SalesPersonDiscount"),
      Filter.or(fromDateIfEarlierThanStartToDateShouldOverlap, 		
      			fromDateIfFurtherThanStartShouldOverlap
               )
     ]
   
  	def stream = api.stream("CX", "customerId", attributes, *filters)
    if (!stream) {
      	api.trace ("No Data Found in Stream","Sorry.........")
        return []
    }
    int count = 0
  	def record
  	def customerDiscounts
    while (stream.hasNext()) {
        record = stream.next()
      	count++
        customerDiscounts = salesPersonDiscounts[record.customerId] ?: []
        customerDiscounts.add([
                customerId: record.customerId,
                sku: record.attribute1,
                focusGroup: record.attribute2,
                validFrom: (record.attribute3)?Date.parse("yyyy-MM-dd",record.attribute3):"",
                validTo: (record.attribute4)?Date.parse("yyyy-MM-dd",record.attribute4):"",
                discount: record.attribute5
        ])
        salesPersonDiscounts[record.customerId] = customerDiscounts
    }

    stream.close()
    api.trace("salesPersonDiscounts", " Rows Fetchted("+ salesPersonDiscounts.size()+") count ("+count+")")
    api.global.salesPersonDiscounts = salesPersonDiscounts
}

def discountAmount = api.global.salesPersonDiscounts[customerId]
        ?.find { spd ->
          				(spd.sku == sku || spd.focusGroup == focusGroup)  &&
                        (!spd.validFrom || spd.validFrom?.compareTo(transactionDate) <= 0) && 
          				(!spd.validTo || spd.validTo?.compareTo(transactionDate) >=0) }
        ?.discount

discountAmount = (discountAmount) ? new BigDecimal(discountAmount).abs().multiply(100) : 0

return discountAmount
}