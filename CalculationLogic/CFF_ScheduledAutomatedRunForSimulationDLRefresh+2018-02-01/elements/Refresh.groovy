Object dmCtx = api.getDatamartContext()
Object DS = dmCtx.getDataSource("PriceChangeSimulation_2020")
Object dmQueryThis = dmCtx.newQuery(DS, true)
dmQueryThis.select("Count(lastUpdateDate)", 'Count')
Object count = dmCtx.executeQuery(dmQueryThis).getData()
if (count.value == 0) {
   return api.abortCalculation()
}
    String dataLoadLabel = "Price Change Simulation DM 2020"
    String dataLoadType = "Refresh"
    String dataLoadTarget = "DM.PriceChangeSimulationDM_2020"

    actionBuilder
            .addDataLoadAction(dataLoadLabel, dataLoadType, dataLoadTarget)
            .setCalculate(true)


