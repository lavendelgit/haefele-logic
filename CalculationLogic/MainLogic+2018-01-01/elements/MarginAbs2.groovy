def newSalesPrice2 = api.getElement("NewSalesPrice2")
def newBaseCost = api.getElement("NewBaseCost")

if (newSalesPrice2 == null) {
    api.redAlert("NewSalesPrice2")
    return null
}

if (newBaseCost == null) {
    api.redAlert("NewBaseCost")
    return null
}

return newSalesPrice2 - newBaseCost
