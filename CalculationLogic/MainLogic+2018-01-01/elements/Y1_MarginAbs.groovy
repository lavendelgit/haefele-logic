def originalRevenueY1 = api.getElement("SalesHistory").y1.attribute4
def originalMarginY1 = api.getElement("SalesHistory").y1.attribute5
def supplierPriceIncreasePerc = api.getElement("SupplierPriceIncreasePerc")

api.trace("[Y1_MarginAbs]", "originalRevenueY1", originalRevenueY1)
api.trace("[Y1_MarginAbs]", "originalMarginY1", originalMarginY1)
api.trace("[Y1_MarginAbs]", "supplierPriceIncreasePerc", supplierPriceIncreasePerc)

if (!originalRevenueY1) {
    api.redAlert("SalesHistory.y1.attribute4")
    return
}

if (!originalMarginY1) {
    api.redAlert("SalesHistory.y1.attribute5")
    return
}

def originalCostY1 = originalRevenueY1 - originalMarginY1

def revenueY1 = api.getElement("Y1_Revenue") // may include price increase
def costY1 = originalCostY1 * (1 + supplierPriceIncreasePerc)

api.trace("[Y1_MarginAbs]", "originalCostY1", originalCostY1)
api.trace("[Y1_MarginAbs]", "revenueY1", revenueY1)
api.trace("[Y1_MarginAbs]", "costY1", costY1)

return revenueY1 - costY1
