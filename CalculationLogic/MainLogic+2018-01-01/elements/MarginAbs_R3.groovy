def salesPriceR3 = api.getElement("SalesPrice_R3")
def baseCost = api.getElement("CurrentBaseCost")

if (salesPriceR3 == null) {
    api.redAlert("SalesPrice_R3")
    return null
}

if (baseCost == null) {
    api.redAlert("CurrentBaseCost")
    return null
}

return salesPriceR3 - baseCost