def baseCost = api.getElement("CurrentBaseCost")
def minSurcharge = api.global.minSurcharge;

if (baseCost == null) {
    api.redAlert("CurrentBaseCost")
    return null
}

if (minSurcharge == null) {
    api.redAlert("MinSurcharge")
    return null
}

return baseCost * (1 + minSurcharge)