def newSalesPrice1 = api.getElement("NewSalesPrice1")
def newBaseCost = api.getElement("NewBaseCost")

if (newSalesPrice1 == null) {
    api.redAlert("NewSalesPrice1")
    return null
}

if (!newBaseCost) {
    api.redAlert("NewBaseCost")
    return null
}

return newSalesPrice1 / newBaseCost - 1;
