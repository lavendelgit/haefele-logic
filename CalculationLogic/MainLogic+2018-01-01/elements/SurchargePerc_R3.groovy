def salesPriceR3 = api.getElement("SalesPrice_R3")
def baseCost = api.getElement("CurrentBaseCost")

if (!salesPriceR3) {
    api.redAlert("SalesPrice_R3")
    return null
}

if (!baseCost) {
    api.redAlert("CurrentBaseCost")
    return null
}

return salesPriceR3 / baseCost - 1