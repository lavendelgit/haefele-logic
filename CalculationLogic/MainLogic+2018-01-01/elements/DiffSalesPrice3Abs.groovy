def currentSalesPrice = api.getElement("CurrentSalesPrice")
def newSalesPrice3 = api.getElement("NewSalesPrice3")

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (newSalesPrice3 == null) {
    api.redAlert("NewSalesPrice3")
    return null
}

return newSalesPrice3 - currentSalesPrice