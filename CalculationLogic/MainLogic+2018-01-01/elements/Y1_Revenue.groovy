def originalRevenueY1 = api.getElement("SalesHistory").y1.attribute4 // Revenue (de: Umsatz)
def salesPriceIncreasePerc = api.getElement("SalesPriceIncreasePerc"); // for simulation

api.trace("[Y1_Revenue]", "originalRevenueY1", originalRevenueY1)
api.trace("[Y1_Revenue]", "salesPriceIncreasePerc", salesPriceIncreasePerc)

if (!originalRevenueY1) {
    return
}

originalRevenueY1 * (1 + salesPriceIncreasePerc)