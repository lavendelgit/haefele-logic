def avgSalesPrice = api.getElement("AvgCustomerSalesPrice")
def minMarginPrice = api.getElement("MinMarginPrice")

if (avgSalesPrice == null) {
    api.redAlert("AvgCustomerSalesPrice")
    return null
}

if (!minMarginPrice) {
    api.redAlert("MinMarginPrice")
    return null
}

def salesPrice3 = avgSalesPrice * 100.0 / 70.0;

if (salesPrice3 < minMarginPrice) {
    salesPrice3 = minMarginPrice
}

return lib.Rounding.roundMoney(salesPrice3);
