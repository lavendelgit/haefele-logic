def currentSalesPrice = api.getElement("CurrentSalesPrice")
def newSalesPrice2 = api.getElement("NewSalesPrice2")

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (newSalesPrice2 == null) {
    api.redAlert("NewSalesPrice2")
    return null
}

return newSalesPrice2 - currentSalesPrice