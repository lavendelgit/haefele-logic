def sku = api.product("sku")
def filters = [
        Filter.equal("name", "Competition"),
        Filter.equal("sku", sku),
        Filter.greaterOrEqual("attribute2", api.getElement("TargetDateFormatted"))
]
def orderBy = "attribute2" // Valid From (de: Gültig ab) - ascending
def fields = [
        "attribute3": "MAX"
]


def maxCompetitorPrice = api.find("PX6", 0, 1, orderBy, fields, false, *filters)

if (maxCompetitorPrice[0] && maxCompetitorPrice[0].attribute3) {
    return maxCompetitorPrice[0].attribute3
} else {
    api.redAlert("No Competition data");
    return null;
}