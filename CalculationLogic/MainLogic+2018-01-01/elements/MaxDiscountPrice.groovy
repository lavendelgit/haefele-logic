def salesPrice = api.getElement("CurrentSalesPrice")
def maxDiscountPerc = api.getElement("MaxDiscountPerc");

if (salesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (maxDiscountPerc == null) {
    api.redAlert("MaxDiscountPerc")
    return null
}

return salesPrice * (1 - maxDiscountPerc)