def sku = api.product("sku")
def filters = [
        Filter.equal("name", "Competition"),
        Filter.equal("sku", sku),
        Filter.greaterOrEqual("attribute2", api.getElement("TargetDateFormatted"))
]
def orderBy = "attribute2" // Valid From (de: Gültig ab) - ascending
def fields = [
        "attribute3": "MIN"
]


def minCompetitorPrice = api.find("PX6", 0, 1, orderBy, fields, false, *filters)

if (minCompetitorPrice[0] && minCompetitorPrice[0].attribute3) {
    return minCompetitorPrice[0].attribute3
} else {
    api.redAlert("No Competition data");
    return null;
}