def diffMarginAbsAfterMaximumDiscount = api.getElement("DiffMarginAbsAfterMaximumDiscount")
def currentBaseCost = api.getElement("CurrentBaseCost")

if (diffMarginAbsAfterMaximumDiscount == null) {
    api.redAlert("DiffMarginAbsAfterMaximumDiscount")
    return null
}

if (!currentBaseCost) {
    api.redAlert("CurrentBaseCost")
    return null
}

return diffMarginAbsAfterMaximumDiscount / currentBaseCost