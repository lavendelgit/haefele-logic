def currentMarginAbs = api.getElement("CurrentMarginAbs")
def currentSalesPrice = api.getElement("CurrentSalesPrice")

if (currentMarginAbs == null) {
    api.redAlert("CurrentMarginAbs")
    return null
}

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

return currentMarginAbs / currentSalesPrice;
