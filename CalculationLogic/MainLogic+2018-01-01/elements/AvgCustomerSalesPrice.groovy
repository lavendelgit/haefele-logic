def sku = api.product("sku")
def filters = [
        Filter.equal("name", "CustomerSalesPrice"),
        Filter.equal("sku", sku)
]
def orderBy = "attribute2" // Valid From (de: Gültig von) - ascending
def fields = [
        "attribute4": "AVG"
]


def avgSalesPrice = api.find("PX8", 0, 1, orderBy, fields, false, *filters)

if (avgSalesPrice[0] && avgSalesPrice[0].attribute4) {
    return avgSalesPrice[0].attribute4
} else {
    api.redAlert("No rows in CustomerSalesPrice");
    return null;
}