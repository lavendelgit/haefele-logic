def salesPriceIncreaseDate = api.getElement("SalesPriceIncreaseDate");

if (!salesPriceIncreaseDate) {
    api.redAlert("SalesPriceIncreaseDate")
    return null
}

def sku = api.product("sku");
def result = lib.Find.latestBaseCost(sku, Filter.lessThan("attribute1", salesPriceIncreaseDate))

api.logInfo("[MainLogic.OldBaseCost] SKU: ${sku}, salesPriceIncreaseDate: ${salesPriceIncreaseDate}, Result: ", result);

return result