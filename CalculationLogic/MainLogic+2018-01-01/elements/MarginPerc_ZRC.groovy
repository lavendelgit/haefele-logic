def salesPriceZRC = api.getElement("SalesPrice_ZRC")
def marginAbsZRC = api.getElement("MarginAbs_ZRC")

if (!salesPriceZRC) {
    api.redAlert("SalesPrice_ZRC")
    return null
}

if (marginAbsZRC == null) {
    api.redAlert("MarginAbs_ZRC")
    return null
}

return marginAbsZRC / salesPriceZRC