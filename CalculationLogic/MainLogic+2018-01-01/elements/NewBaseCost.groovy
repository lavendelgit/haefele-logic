def newBaseCostRecord = api.getElement("NewBaseCostRecord");

def newBaseCost =  null

if (newBaseCostRecord) {
    newBaseCost = newBaseCostRecord.attribute3 // Base Cost (de: Einstandspreis)
}

if (!newBaseCost) {
    api.redAlert("New Base Cost is missing in PX BaseCost")
}

return newBaseCost