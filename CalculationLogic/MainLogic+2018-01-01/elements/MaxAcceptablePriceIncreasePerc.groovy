def diffMarginAbs = api.getElement("DiffMarginAbs")
def currentBaseCost = api.getElement("CurrentBaseCost")

if (diffMarginAbs == null) {
    api.redAlert("DiffMarginAbs")
    return null
}

if (!currentBaseCost) {
    api.redAlert("CurrentBaseCost")
    return null
}

return diffMarginAbs / currentBaseCost