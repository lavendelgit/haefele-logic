def salesPriceIncreaseDate = api.getElement("SalesPriceIncreaseDate");

if (!salesPriceIncreaseDate) {
    api.redAlert("SalesPriceIncreaseDate")
    return null
}

def sku = api.product("sku");
def result = lib.Find.latestSalesPrice(sku, Filter.lessThan("attribute1", salesPriceIncreaseDate))

api.logInfo("[MainLogic.OldSalesPrice] SKU: ${sku}, salesPriceIncreaseDate: ${salesPriceIncreaseDate}, Result: ", result);

return result