def salesPriceR3 = api.getElement("SalesPrice_R3")
def discountZRC = lib.GeneralSettings.percentageValue("Discount_ZRC_Pct")
def minMarginPrice = api.getElement("MinMarginPrice");

if (salesPriceR3 == null) {
    api.redAlert("SalesPriceR3")
    return null
}

if (discountZRC == null) {
    api.redAlert("Discount_ZRC_Pct")
    return null
}

if (minMarginPrice == null) {
    api.redAlert("MinMarginPrice")
}

def salesPriceZRC = lib.Rounding.roundMoney(salesPriceR3 * (1 - discountZRC))

if (salesPriceZRC < minMarginPrice) {
    api.local.discountWarning = api.local.discountWarning ?: "SalesPrice_ZRC"
    api.redAlert("SalesPrice_ZRC is below MinMarginPrice")
}

return salesPriceZRC