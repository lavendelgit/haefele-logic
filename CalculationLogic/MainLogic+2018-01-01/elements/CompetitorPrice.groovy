def maxDiscountPerc = api.getElement("MaxDiscountPerc");
def maxDiscountPrice = api.getElement("MaxDiscountPrice");
def minCompetitorPrice = api.getElement("MinCompetitorPrice");
def maxCompetitorPrice = api.getElement("MaxCompetitorPrice");
def avgCompetitorPrice = api.getElement("AvgCompetitorPrice");
def salesPrice = api.getElement("CurrentSalesPrice")
def minMarginPrice = api.getElement("MinMarginPrice")

if (maxDiscountPrice == null) {
    api.redAlert("MaxDiscountPrice")
    return null
}

if (maxDiscountPrice == null) {
    api.redAlert("MaxCompetitorPrice")
    return null
}

if (salesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (minMarginPrice == null) {
    api.redAlert("MinMarginPrice")
    return null
}

if (minCompetitorPrice == null) {
    api.redAlert("MinCompetitorPrice")
    return null
}

if (maxCompetitorPrice == null) {
    api.redAlert("MaxCompetitorPrice")
    return null
}

if (avgCompetitorPrice == null) {
    api.redAlert("AvgCompetitorPrice")
    return null
}

def competitorPrice = null;

if (maxDiscountPrice > maxCompetitorPrice) {
    competitorPrice = maxCompetitorPrice
} else if (salesPrice < minCompetitorPrice) {
    competitorPrice = avgCompetitorPrice * (1 + maxDiscountPerc/2)
} else {
    api.redAlert("Rule not defined yet")
    return null
}


if (competitorPrice < minMarginPrice) {
    competitorPrice = minMarginPrice
}

return lib.Rounding.roundMoney(competitorPrice)