def salesPriceIncreaseDate = api.dateUserEntry("Sales Price Increase Date")

if (!salesPriceIncreaseDate) {
    api.redAlert("Sales Price Increase Data user entry not specified")
    api.addWarning("Sales Price Increase Data user entry not specified")
}

return salesPriceIncreaseDate