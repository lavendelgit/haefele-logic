def supplierId = api.getElement("MainSupplierNumber");
def values = api.findLookupTableValues("SupplierPriceIncrease", Filter.equal("name", supplierId))

if (values && values[0] && values[0].attribute2) {
    return values[0].attribute2.toBigDecimal()
} else {
    return 0.0
}
