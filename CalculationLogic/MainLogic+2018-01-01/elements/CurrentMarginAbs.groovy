def currentSalesPrice = api.getElement("CurrentSalesPrice")
def currentBaseCost = api.getElement("CurrentBaseCost")

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (currentBaseCost == null) {
    api.redAlert("CurrentBaseCost")
    return null
}

return currentSalesPrice - currentBaseCost;
