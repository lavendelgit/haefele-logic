def currentMarginAbsAfterMaximumDiscount = api.getElement("CurrentMarginAbsAfterMaximumDiscount")
def oldMarginAbsAfterMaximumDiscount = api.getElement("OldMarginAbsAfterMaximumDiscount")

if (currentMarginAbsAfterMaximumDiscount == null) {
    api.redAlert("CurrentMarginAbsAfterMaximumDiscount")
    return null
}

if (oldMarginAbsAfterMaximumDiscount == null) {
    api.redAlert("OldMarginAbsAfterMaximumDiscount")
    return null
}

return currentMarginAbsAfterMaximumDiscount - oldMarginAbsAfterMaximumDiscount