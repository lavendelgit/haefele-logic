if (!api.global.maxDiscountPerc) {
    def maxDiscountPerc = lib.GeneralSettings.percentageValue("MaxDiscountPct")

    if (maxDiscountPerc == null) {
        api.redAlert("MaxDiscountPct")
        return null;
    }

    api.global.maxDiscountPerc = maxDiscountPerc
}

return api.global.maxDiscountPerc
