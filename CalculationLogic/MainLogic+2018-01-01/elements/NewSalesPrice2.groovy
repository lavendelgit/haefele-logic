// NewSalesPrice2 = roundingFunction(NewBaseCost * (CurrentSurchargePerc + 1))

def newBaseCost = api.getElement("NewBaseCost")
def currentSurchargePerc = api.getElement("CurrentSurchargePerc")

if (newBaseCost == null) {
    api.redAlert("NewBaseCost")
    return null
}

if (currentSurchargePerc == null) {
    api.redAlert("CurrentSurchargePerc")
    return null
}

return lib.Rounding.roundMoney(newBaseCost * (currentSurchargePerc + 1))