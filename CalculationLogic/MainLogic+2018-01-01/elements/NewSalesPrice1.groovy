def currentSalesPrice = api.getElement("CurrentSalesPrice")
def currentBaseCost = api.getElement("CurrentBaseCost")
def newBaseCost = api.getElement("NewBaseCost")

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (currentBaseCost == null) {
    api.redAlert("CurrentBaseCost")
    return null
}

if (newBaseCost == null) {
    api.redAlert("NewBaseCost")
    return null
}

return lib.Rounding.roundMoney(currentSalesPrice + newBaseCost - currentBaseCost)