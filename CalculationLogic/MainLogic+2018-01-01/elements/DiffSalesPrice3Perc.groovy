def currentSalesPrice = api.getElement("CurrentSalesPrice")
def diffSalesPrice3Abs = api.getElement("DiffSalesPrice3Abs")

if (!currentSalesPrice) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (diffSalesPrice3Abs == null) {
    api.redAlert("DiffSalesPrice3Abs")
    return null
}

return diffSalesPrice3Abs / currentSalesPrice