def newBaseCostRecord = api.getElement("NewBaseCostRecord");

def newBaseCostValidFrom = null

if (newBaseCostRecord) {
    newBaseCostValidFrom = newBaseCostRecord.attribute1 // Valid From (de: Gültig von)
}

if (!newBaseCostValidFrom) {
    api.redAlert("Valid From is missing in PX BaseCost")
}

return newBaseCostValidFrom