def salesPriceOrder = api.getElement("SalesPrice_Order")
def marginAbsOrder = api.getElement("MarginAbs_Order")

if (!salesPriceOrder) {
    api.redAlert("SalesPrice_Order")
    return null
}

if (marginAbsOrder == null) {
    api.redAlert("MarginAbs_Order")
    return null
}

return marginAbsOrder / salesPriceOrder