def currentSalesPrice = api.getElement("CurrentSalesPrice")
def diffSalesPrice1Abs = api.getElement("DiffSalesPrice1Abs")

if (!currentSalesPrice) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (diffSalesPrice1Abs == null) {
    api.redAlert("DiffSalesPrice1Abs")
    return null
}

return diffSalesPrice1Abs / currentSalesPrice