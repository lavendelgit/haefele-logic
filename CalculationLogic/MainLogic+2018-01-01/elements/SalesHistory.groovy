import com.googlecode.genericdao.search.Filter

def sku = api.product("sku")
def currentYear = api.calendar().get(Calendar.YEAR);
def numberOfYears = 3;
def fromYear = currentYear - numberOfYears + 1;
def filters = [
        Filter.equal("name", "SalesHistory"),
        Filter.equal("sku", sku),
        Filter.greaterOrEqual("attribute1", fromYear)
]
def orderBy = "-attribute1" // Year

def records = api.find("PX8", 0, numberOfYears, orderBy, *filters)

return [
    "y0": records.find{rec -> rec.attribute1 == currentYear}     ?: [:],
    "y1": records.find{rec -> rec.attribute1 == currentYear - 1} ?: [:],
    "y2": records.find{rec -> rec.attribute1 == currentYear - 2} ?: [:]
]