def sku = api.product("sku")
def filters = [
        Filter.equal("name", "Competition"),
        Filter.equal("sku", sku),
       Filter.greaterOrEqual("attribute2", api.getElement("TargetDateFormatted"))
]
def orderBy = "attribute2" // Valid From (de: Gültig ab) - ascending
def fields = [
        "attribute3": "AVG"
]


def avgCompetitorPrice = api.find("PX6", 0, 1, orderBy, fields, false, *filters)

api.trace("avgCompetitorPrice", "", avgCompetitorPrice)

if (avgCompetitorPrice[0] && avgCompetitorPrice[0].attribute3) {
    return avgCompetitorPrice[0].attribute3
} else {
    api.redAlert("No Competition data");
    return null;
}