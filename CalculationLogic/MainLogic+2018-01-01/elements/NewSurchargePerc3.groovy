def newSalesPrice3 = api.getElement("NewSalesPrice3")
def baseCost = api.getElement("CurrentBaseCost")
def newBaseCost = api.getElement("NewBaseCost")


if (newSalesPrice3 == null) {
    api.redAlert("NewSalesPrice3")
    return null
}

if (newBaseCost) {
    baseCost = newBaseCost
}

if (!baseCost) {
    api.redAlert("CurrentBaseCost")
    return null
}

return newSalesPrice3 / baseCost - 1;
