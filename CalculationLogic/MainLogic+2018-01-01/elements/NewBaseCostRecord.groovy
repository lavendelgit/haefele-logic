def sku = api.product("sku")
def filters = [
        Filter.equal("name", "BaseCost"),
        Filter.equal("sku", sku),
        Filter.greaterThan("attribute1", api.getElement("TargetDateFormatted"))
]
def orderBy = "attribute1" // Valid From (de: Gültig von) - ascending
def baseCosts = api.find("PX10", 0, 1, orderBy, *filters)

if (baseCosts[0]) {
    return baseCosts[0]
}

return [:]
