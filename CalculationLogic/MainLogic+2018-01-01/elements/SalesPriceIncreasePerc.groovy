if (!api.global.salesPriceIncreasePerc) {
    def salesPriceIncreasePerc = 0.0;

    def values = api.findLookupTableValues("SalesPriceIncrease", Filter.equal("name", "SalesPriceIncrease"))
    if (values && values[0] && values[0].value) {
        salesPriceIncreasePerc = values[0].value.toBigDecimal() / 100
    }

    api.global.salesPriceIncreasePerc = salesPriceIncreasePerc
}

return api.global.salesPriceIncreasePerc