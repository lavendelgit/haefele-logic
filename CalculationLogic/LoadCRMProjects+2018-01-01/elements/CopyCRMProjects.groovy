def target = api.getDatamartRowSet("target")
def result = api.stream("PX", null /* sort-by*/,
        ["sku","attribute1","attribute2","attribute3","attribute4","attribute5","attribute6","attribute7",
         "attribute8","attribute9","attribute10","attribute11","attribute12","attribute13","attribute14",
         "attribute15", "attribute16", "attribute17", "attribute19", "attribute20"],
        Filter.equal("name", "CRMProject"))

if (!result) {
    return
}

def count = 0

def getMonthDate (def currentValue, def transactionDate) {
    if (!currentValue || currentValue == "") {
        if (!(transactionDate instanceof Date)) {
            transactionDate = Date.parse ("yyyy-MM-dd", transactionDate)
        }
        return transactionDate.format("yyyy") + "-M" + transactionDate.format("MM")
    }
    return currentValue
}

while (result.hasNext()) {
    def crmProject = result.next()
    def row = ["Material": crmProject.sku,
               "ProjectNumber" : crmProject.attribute1,
               "SalesOffice" : crmProject.attribute2,
               "CRMPacisNo" : crmProject.attribute3,
               "ProjektNrPos" : crmProject.attribute4,
               "CustomerId": crmProject.attribute5,
               "InvoiceId": crmProject.attribute6,
               "Pos": crmProject.attribute7,
               "InvoiceDate": crmProject.attribute8,
               "YearMonth": getMonthDate(crmProject.attribute20, crmProject.attribute8),
               "Quantity": crmProject.attribute10,
               "Revenue": crmProject.attribute11,
               "BaseCost": crmProject.attribute12,
               "MarginAbs": crmProject.attribute13,
               "MarginPerc": crmProject.attribute14,
               "CRMPrice": crmProject.attribute15,
               "SalesPrice": crmProject.attribute16,
               "UnitSalesPrice": crmProject.attribute17,
               "DifferencePerc": crmProject.attribute19?.toBigDecimal()?.multiply(100.0)]
    target.addRow(row)
    count++
}

result.close()