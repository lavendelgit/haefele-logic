import groovy.transform.Field
import net.pricefx.common.api.FieldFormatType


@Field final String TRANSACTION_DATAMART = "TransactionsDM"
@Field final String FLD_MATERIAL = "Material"
@Field final String FLD_CUSTOMER_ID = "CustomerId"
@Field final String FLD_CUSTOMER_NAME = "Name"
@Field final String FLD_VENDOR_NAME = "RegellieferantName"
@Field final String FLD_VENDOR_ID = "RegellieferantNr"
@Field final String FLD_UNITS = "Units"
@Field final String FLD_COST = "Cost"
@Field final String FLD_REVENUE = "Revenue"
@Field final String FLD_INVOICE_QUANTITY = "InvoiceQuantity"
@Field final String FLD_INVOICE_DATE_MONTH = "InvoiceDateMonth"
@Field final String FLD_CM_HIERARCHY3 = "prodh_cm3"
@Field final String FLD_MARGIN_ABS = "MarginAbs"

@Field final Map INPUT_GROUP_BY = [
        "RegellieferantNr": FLD_VENDOR_ID,
        "Material"          : FLD_MATERIAL,
        "CMHierarchy3"      : FLD_CM_HIERARCHY3,
        "CustomerId"        : FLD_CUSTOMER_ID
]

/*@Field final Map AGGR_COLUMN_FORMAT=[

]*/

@Field final String DISP_QUANTITY = "Quantity"
@Field final String DISP_NETSALES = "Net Sales"
@Field final String DISP_COGS = "Cogs"
@Field final String DISP_GP = "GP"
@Field final String DISP_GP_PERCENT = "GP%"
@Field final String DISP_CHG_NETSALES_PERUNT_MIN = "Change in Net Sales per unit and time period Min"
@Field final String DISP_CHG_NETSALES_PERUNT_MAX = "Change in Net Sales per unit and time period Max"
@Field final String DISP_CHG_COGS_PERUNT_MIN = "Change in Cogs per unit and time period Min"
@Field final String DISP_CHG_COGS_PERUNT_MAX = "Change in Cogs per unit and time period Max"


@Field final String DISP_NAME = "Name"
@Field final String DISP_CUSTOMER_ID ="CustomerId"
@Field final String DISP_VENDOR_NAME = "RegellieferantName"
@Field final String DISP_VENDOR_ID = "RegellieferantNr"
@Field final String DISP_MATERIAL = "Material"
@Field final String DISP_CMHIERARCHY3 = "CMHierarchy3"
@Field final String DISP_MONTHLOWER = "MonthLower"
@Field final String DISP_MONTHUPPER = "MonthUpper"
@Field final String DISP_UNITS = "Units"
@Field final String DISP_UNITSLOWER = "UnitsLower"
@Field final String DISP_NETSALESLOWER = "NetSalesLower"
@Field final String DISP_COGSLOWER = "CogsLower"
@Field final String DISP_GROSSPROFITLOWER = "GrossProfitLower"
@Field final String DISP_GROSSPROFITLOWERPERCENT = "GrossProfitLowerPercent"
@Field final String DISP_NETSALESPERUNITLOWER = "NetSalesPerUnitLower"
@Field final String DISP_COGSPERUNITLOWER = "CogsPerUnitLower"
@Field final String DISP_UNITSUPPER = "UnitsUpper"
@Field final String DISP_NETSALESUPPER = "NetSalesUpper"
@Field final String DISP_COGSUPPER = "CogsUpper"
@Field final String DISP_GROSSPROFITUPPER = "GrossProfitUpper"
@Field final String DISP_GROSSPROFITUPPERPERCENT = "GrossProfitUpperPercent"
@Field final String DISP_NETSALESPERUNITUPPER = "NetSalesPerUnitUpper"
@Field final String DISP_COGSPERUNITUPPER = "CogsPerUnitUpper"
@Field final String DISP_CHANGEINNETSALEPERUNIT = "ChangeInNetSalePerUnit"
@Field final String DISP_CHANGEINCOGSPERUNIT = "ChangeInCogsPerUnit"
@Field final String DISP_DELTA_PERCENTAGE = "DELTA %"
@Field final String DISP_GROSSPROFIT = "GrossProfit"
@Field final String DISP_GROSSPROFITPERCENT = "GrossProfitPercent"
@Field final String DISP_ROWCOUNTLOWER = "Lower RowCount"
@Field final String DISP_ROWCOUNTUPPER = "Upper RowCount"
@Field final String DISP_ROWCOUNT = "RowCount"
@Field final String DISP_TOTAL_ROWS = "Total Participating Rows"
@Field final String DISP_MATCHED_ROWS = "Total Matched Rows"

@Field final Map<String, FieldFormatType> AGGR_COL_FORMAT = [
        DISP_NAME                   : FieldFormatType.TEXT,
        DISP_VENDOR_NAME            : FieldFormatType.TEXT,
        DISP_MATERIAL               : FieldFormatType.TEXT,
        DISP_CMHIERARCHY3           : FieldFormatType.TEXT,
        DISP_MONTHLOWER             : FieldFormatType.TEXT,
        DISP_MONTHUPPER             : FieldFormatType.TEXT,
        DISP_UNITS                  : FieldFormatType.INTEGER,
        DISP_NETSALESLOWER          : FieldFormatType.MONEY_EUR,
        DISP_COGSLOWER              : FieldFormatType.MONEY_EUR,
        DISP_GROSSPROFITLOWER       : FieldFormatType.MONEY_EUR,
        DISP_GROSSPROFITLOWERPERCENT: FieldFormatType.PERCENT,
        DISP_NETSALESPERUNITLOWER   : FieldFormatType.MONEY_EUR,
        DISP_COGSPERUNITLOWER       : FieldFormatType.MONEY_EUR,
        DISP_UNITSUPPER             : FieldFormatType.INTEGER,
        DISP_NETSALESUPPER          : FieldFormatType.MONEY_EUR,
        DISP_COGSUPPER              : FieldFormatType.MONEY_EUR,
        DISP_GROSSPROFITUPPER       : FieldFormatType.MONEY_EUR,
        DISP_GROSSPROFITUPPERPERCENT: FieldFormatType.PERCENT,
        DISP_NETSALESPERUNITUPPER   : FieldFormatType.MONEY_EUR,
        DISP_COGSPERUNITUPPER       : FieldFormatType.MONEY_EUR,
        DISP_CHANGEINNETSALEPERUNIT : FieldFormatType.PERCENT,
        DISP_CHANGEINCOGSPERUNIT    : FieldFormatType.PERCENT,
        DISP_DELTA_PERCENTAGE       : FieldFormatType.PERCENT

]
