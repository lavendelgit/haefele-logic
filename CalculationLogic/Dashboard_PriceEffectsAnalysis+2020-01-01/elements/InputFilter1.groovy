
List filter1 = []

def dmFilter = out.GenericDMFilter

if (dmFilter) {
    filter1.add(dmFilter)

}
filter1.add(Filter.isNotNull(AggregateConstants.FLD_UNITS))
filter1.add(Filter.greaterThan(AggregateConstants.FLD_REVENUE,-1))
filter1.add(Filter.greaterThan(AggregateConstants.FLD_MARGIN_ABS,-1))
filter1.add(Filter.equal(AggregateConstants.FLD_INVOICE_DATE_MONTH, out.Month1))


return filter1