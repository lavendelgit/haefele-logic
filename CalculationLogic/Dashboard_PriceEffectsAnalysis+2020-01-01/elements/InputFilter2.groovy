List filter2 = []

def dmFilter = out.GenericDMFilter
if (dmFilter) {
    filter2.add(dmFilter)

}
filter2.add(Filter.isNotNull(AggregateConstants.FLD_UNITS))
filter2.add(Filter.greaterThan(AggregateConstants.FLD_REVENUE,-1))
filter2.add(Filter.greaterThan(AggregateConstants.FLD_MARGIN_ABS,-1))
filter2.add(Filter.equal(AggregateConstants.FLD_INVOICE_DATE_MONTH, out.Month2))

return filter2