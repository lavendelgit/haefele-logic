def dmCtx = api.getDatamartContext()
def dm = dmCtx.getDatamart(AggregateConstants.TRANSACTION_DATAMART)
def t1 = dmCtx.newQuery(dm, true)
AggregateCommon.buildTTestSelect(t1)
               .where(*out.GroupByFilter)
api.trace("Query ttest",t1)
def resultData = dmCtx.executeQuery(t1).getData()
def newResult = AggregateCommon.buildTTestMatrixFromData(resultData, api.global.changUnitData)
newResult?.setDisablePreferences(false)

return newResult