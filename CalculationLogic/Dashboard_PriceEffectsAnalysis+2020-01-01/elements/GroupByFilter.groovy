List filter3 = []

def dmFilter = out.GenericDMFilter
/*
if (!dmFilter) {
    api.throwException("Filter mandatory. Please provide valid filters")
}*/

if (dmFilter) {
    filter3.add(dmFilter)
}
filter3.add(Filter.isNotNull(AggregateConstants.FLD_UNITS))
filter3.add(Filter.greaterThan(AggregateConstants.FLD_REVENUE, -1))
filter3.add(Filter.greaterThan(AggregateConstants.FLD_MARGIN_ABS, -1))

filter3.add(Filter.in(AggregateConstants.FLD_INVOICE_DATE_MONTH, [out.Month1, out.Month2]))


return filter3