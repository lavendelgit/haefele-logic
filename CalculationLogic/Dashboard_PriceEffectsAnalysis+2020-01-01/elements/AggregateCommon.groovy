import net.pricefx.common.api.FieldFormatType
import net.pricefx.formulaengine.DatamartContext
import net.pricefx.server.dto.calculation.ResultMatrix


protected DatamartContext.Query buildAggregateSelectQuery(DatamartContext.Query query) {
    query.select(AggregateConstants.FLD_INVOICE_DATE_MONTH, "InvoiceDateMonth")
         .select(AggregateConstants.FLD_MATERIAL, "Material")
         .select(AggregateConstants.FLD_VENDOR_NAME, "RegellieferantName")
         .select(AggregateConstants.FLD_VENDOR_ID, "RegellieferantNr")

         .select(AggregateConstants.FLD_CM_HIERARCHY3, "CMHierarchy3")

         .select(AggregateConstants.FLD_CUSTOMER_ID, "CustomerID")
         .select(AggregateConstants.FLD_CUSTOMER_NAME, "Name")

    //.select(AggregateConstants.FLD_INVOICE_QUANTITY, 'GrpQuantity')
    //.select(AggregateConstants.FLD_COST, 'GrpCost')
    //.select(AggregateConstants.FLD_REVENUE, 'GrpRevenue')

         .select("SUM(" + AggregateConstants.FLD_UNITS + ")", "Units")
         .select("SUM(" + AggregateConstants.FLD_INVOICE_QUANTITY + ")", 'Invoice Quantity')

         .select("ROUND(SUM(" + AggregateConstants.FLD_COST + "),2)", "Cogs")
         .select("ROUND(SUM(" + AggregateConstants.FLD_REVENUE + "),2)", 'NetSales')

         .select("ROUND(SUM(" + AggregateConstants.FLD_COST + ")/SUM(" + AggregateConstants.FLD_INVOICE_QUANTITY + "),2)", "CostPerUnit")
         .select("ROUND(SUM(" + AggregateConstants.FLD_REVENUE + ")/SUM(" + AggregateConstants.FLD_INVOICE_QUANTITY + "),2)", 'NetSalesPerUnit')
         .select("COUNT(" + AggregateConstants.FLD_MATERIAL + ")", "RowCount")

}


protected DatamartContext.Query buildTTestSelect(DatamartContext.Query query) {

    String baseSelectField = AggregateConstants.INPUT_GROUP_BY.get(out.GroupByField)
    //String baseSelectFieldDispName = AggregateConstants.INPUT_GROUP_BY_DISPLAY_NAME.get(out.GroupByField)
    query.select(baseSelectField, out.GroupByField)
            .select("COUNT(" + baseSelectField + ")", AggregateConstants.DISP_ROWCOUNT)
            .select("SUM(" + AggregateConstants.FLD_UNITS + ")", AggregateConstants.DISP_UNITS)
            .select("ROUND(SUM(" + AggregateConstants.FLD_COST + "),2)", AggregateConstants.DISP_COGS)
            .select("ROUND(SUM(" + AggregateConstants.FLD_REVENUE + "),2)", 'NetSales')
            .select("ROUND(SUM(" + AggregateConstants.FLD_REVENUE + ") - SUM(" + AggregateConstants.FLD_COST + "),2)", AggregateConstants.DISP_GROSSPROFIT)
            .select("ROUND(((SUM(" + AggregateConstants.FLD_REVENUE + ") - SUM(" + AggregateConstants.FLD_COST + "))/SUM(" + AggregateConstants.FLD_REVENUE + ")),2)", AggregateConstants.DISP_GROSSPROFITPERCENT)

    if (out.GroupByField){
        //This additional select is primarily to avoid duplicates on the vendor name or customer name
            if (AggregateConstants.FLD_VENDOR_ID.equals(out.GroupByField)) {
                query.select(AggregateConstants.FLD_VENDOR_NAME,AggregateConstants.FLD_VENDOR_NAME).orderBy(AggregateConstants.FLD_VENDOR_NAME)
            } else if (AggregateConstants.FLD_CUSTOMER_ID.equals(out.GroupByField)) {
                query.select(AggregateConstants.FLD_CUSTOMER_NAME,AggregateConstants.FLD_CUSTOMER_NAME).orderBy(AggregateConstants.FLD_CUSTOMER_NAME)
            }else{
                query.orderBy(out.GroupByField)
            }
        }
return query
}

protected List<String> getMonthYear() {
    Date date = new Date()
    int year = Calendar.getInstance().get(Calendar.YEAR)
    int month = Calendar.getInstance().get(Calendar.MONTH) + 1
    List monthYrLst = new ArrayList<String>()
    for (i in 2016..year) {
        for (j in 1..12) {
            if (year == i && j > month) {
                break
            } else {
                monthYrLst.add(j > 9 ? i + "-M" + j : i + "-M0" + j)
                j++
            }
        }
        i++
    }
    return monthYrLst
}

protected List buildChangeUnitData(ResultMatrix resultMatrix) {
    Map groupedData = resultMatrix?.getEntries()?.groupBy { Map row ->
        row[out.GroupByField]
    }

    return [AggregateConstants.DISP_CHANGEINNETSALEPERUNIT, AggregateConstants.DISP_CHANGEINCOGSPERUNIT, AggregateConstants.DISP_ROWCOUNT]?.collect { String columnName ->
        return groupedData?.collectEntries { String groupBy, List groupedRows ->
            [(groupBy): groupedRows[columnName]]
        }
    }
}

protected ResultMatrix buildTTestMatrixFromData(Matrix2D resultData, List changePerUnitData) {
    ArrayList<FieldFormatType> TTEST_COLUMN_TYPES = [FieldFormatType.TEXT,
                                                     FieldFormatType.INTEGER,
                                                     FieldFormatType.MONEY_EUR,
                                                     FieldFormatType.MONEY_EUR,
                                                     FieldFormatType.MONEY_EUR,
                                                     FieldFormatType.PERCENT,
                                                     FieldFormatType.PERCENT,
                                                     FieldFormatType.PERCENT,
                                                     FieldFormatType.PERCENT,
                                                     FieldFormatType.PERCENT,
                                                     FieldFormatType.INTEGER,
                                                     FieldFormatType.INTEGER]

    ArrayList TTEST_COLUMNS = [out.GroupByField, AggregateConstants.DISP_QUANTITY,
                                 AggregateConstants.DISP_NETSALES,
                                 AggregateConstants.DISP_COGS,
                                 AggregateConstants.DISP_GP,
                                 AggregateConstants.DISP_GP_PERCENT,
                                 AggregateConstants.DISP_CHG_NETSALES_PERUNT_MIN,
                                 AggregateConstants.DISP_CHG_NETSALES_PERUNT_MAX,
                                 AggregateConstants.DISP_CHG_COGS_PERUNT_MIN,
                                 AggregateConstants.DISP_CHG_COGS_PERUNT_MAX,
                                 AggregateConstants.DISP_TOTAL_ROWS,
                                 AggregateConstants.DISP_MATCHED_ROWS]

    if(AggregateConstants.FLD_VENDOR_ID.equals(out.GroupByField)){
        TTEST_COLUMN_TYPES.add(0,FieldFormatType.TEXT)
        TTEST_COLUMNS.add(0,AggregateConstants.DISP_VENDOR_NAME)
    }else  if(AggregateConstants.FLD_CUSTOMER_ID.equals(out.GroupByField)){
        TTEST_COLUMN_TYPES.add(0,FieldFormatType.TEXT)
        TTEST_COLUMNS.add(0,AggregateConstants.DISP_NAME)
    }
    // String baseSelectFieldDispName = AggregateConstants.INPUT_GROUP_BY_DISPLAY_NAME.get(out.GroupByField)
    ResultMatrix newResultMatrix
    if (changePerUnitData?.size() >= 2) {
        def groovyLibrary = libs.TTestCalculation.TTest
        def dashboardUtil = libs.__LIBRARY__.DashboardUtility
        Map netSalesUnitMap = changePerUnitData.get(0)
        Map cogsUnitMap = changePerUnitData.get(1)
        Map rowCountMap = changePerUnitData.get(2)
        newResultMatrix = dashboardUtil.newResultMatrix("TTest Results", TTEST_COLUMNS,
                                                        TTEST_COLUMN_TYPES)
        newResultMatrix.setEnableClientFilter(true)
        List matrixRows = resultData.collect()

        matrixRows.each { Map matrixRow ->
            valueBasedOnDispName = matrixRow.get(out.GroupByField)
            LinkedHashMap row = [
                    (out.GroupByField)                  : valueBasedOnDispName,
                    (AggregateConstants.DISP_QUANTITY)  : matrixRow.Units,
                    (AggregateConstants.DISP_NETSALES)  : matrixRow.NetSales,
                    (AggregateConstants.DISP_COGS)      : matrixRow.Cogs,
                    (AggregateConstants.DISP_GP)        : matrixRow.GrossProfit,
                    (AggregateConstants.DISP_GP_PERCENT): matrixRow.GrossProfitPercent,
                    (AggregateConstants.DISP_TOTAL_ROWS): matrixRow.RowCount
            ]
            netSalesUnitData = netSalesUnitMap.get(valueBasedOnDispName)
            //TODO - CONDITION BASED ON  >1 need to be relooked based on the ttest logic as it doesn't work if size is 1
            if (valueBasedOnDispName && netSalesUnitData && netSalesUnitData.size() > 1) {
                //There are cases where we'll have data for TTest but not the matching data set of T1 and T2 done in aggregation
                Map netSalesUnitTTestMap = groovyLibrary.tTest(netSalesUnitData)
                Map cogsUnitTTestMap = groovyLibrary.tTest(cogsUnitMap?.get(valueBasedOnDispName))

                //row.put( baseSelectFieldDispName , valueBasedOnDispName)
                row.put(AggregateConstants.DISP_CHG_NETSALES_PERUNT_MIN, netSalesUnitTTestMap?.get("lowerLimit"))
                row.put(AggregateConstants.DISP_CHG_NETSALES_PERUNT_MAX, netSalesUnitTTestMap?.get("upperLimit"))
                row.put(AggregateConstants.DISP_CHG_COGS_PERUNT_MIN, cogsUnitTTestMap?.get("lowerLimit"))
                row.put(AggregateConstants.DISP_CHG_COGS_PERUNT_MAX, cogsUnitTTestMap?.get("upperLimit"))
                row.put(AggregateConstants.DISP_MATCHED_ROWS, rowCountMap?.get(valueBasedOnDispName)?.sum())

            }

            if(AggregateConstants.FLD_VENDOR_ID.equals(out.GroupByField)){
                row.put(AggregateConstants.DISP_VENDOR_NAME,matrixRow.get(AggregateConstants.FLD_VENDOR_NAME))
            }else  if(AggregateConstants.FLD_CUSTOMER_ID.equals(out.GroupByField)){
                row.put(AggregateConstants.DISP_NAME,matrixRow.get(AggregateConstants.FLD_CUSTOMER_NAME))
            }


            if (out.HideResultsWithoutTTest) {
                if (row[AggregateConstants.DISP_CHG_NETSALES_PERUNT_MIN] != null) {
                    newResultMatrix.addRow(row)
                }
            } else {
                newResultMatrix.addRow(row)
            }
        }
    }

    return newResultMatrix
}
