import net.pricefx.common.api.FieldFormatType
import net.pricefx.formulaengine.scripting.Matrix2D
import net.pricefx.server.dto.calculation.ResultMatrix

def dmCtx = api.getDatamartContext()
def dm = dmCtx.getDatamart(AggregateConstants.TRANSACTION_DATAMART)

def t1 = dmCtx.newQuery(dm, true)
AggregateCommon.buildAggregateSelectQuery(t1)
               .where(*out.InputFilter1)
api.trace("t1", t1)

def t2 = dmCtx.newQuery(dm, true)
AggregateCommon.buildAggregateSelectQuery(t2)
               .where(*out.InputFilter2)


api.trace("t2", t2)

def sqlQuery = dmCtx.newSqlQuery()
                    .addSource(t1)
                    .addSource(t2)


def combinedQuery = " SELECT T1.Name AS '" + AggregateConstants.DISP_NAME +
        "',T1.CustomerID AS '" + AggregateConstants.DISP_CUSTOMER_ID +
        "',T1.RegellieferantName AS '" + AggregateConstants.DISP_VENDOR_NAME +
        "',T1.RegellieferantNr AS '" + AggregateConstants.DISP_VENDOR_ID +
        "',T1.MATERIAL AS  '" + AggregateConstants.DISP_MATERIAL +
        "',T1.CMHierarchy3 AS '" + AggregateConstants.DISP_CMHIERARCHY3 +
        "',T1.INVOICEDATEMONTH  AS '" + AggregateConstants.DISP_MONTHLOWER +
        "',T2.INVOICEDATEMONTH AS '" + AggregateConstants.DISP_MONTHUPPER +
        "',T1.UNITS AS  '" + AggregateConstants.DISP_UNITSLOWER +
        "', T1.NETSALES  AS  '" + AggregateConstants.DISP_NETSALESLOWER +
        "', T1.COGS AS  '" + AggregateConstants.DISP_COGSLOWER +
        "',T1.NETSALES -T1.COGS '" + AggregateConstants.DISP_GROSSPROFITLOWER +
        "',ROUND(((T1.NETSALES -T1.COGS)/NULLIF(T1.NETSALES,0)),2)   AS '" + AggregateConstants.DISP_GROSSPROFITLOWERPERCENT +
        "' ,T1.NetSalesPerUnit AS '" + AggregateConstants.DISP_NETSALESPERUNITLOWER +
        "',T1.CostPerUnit AS '" + AggregateConstants.DISP_COGSPERUNITLOWER +
        "',T1.RowCount AS '" + AggregateConstants.DISP_ROWCOUNTLOWER +
        "', T2.UNITS AS  '" + AggregateConstants.DISP_UNITSUPPER +
        "', T2.NETSALES AS  'NetSalesUpper" + AggregateConstants.DISP_NETSALESUPPER +
        "', T2.COGS AS  '" + AggregateConstants.DISP_COGSUPPER +
        "', T2.NETSALES -T2.COGS AS '" + AggregateConstants.DISP_GROSSPROFITUPPER +
        "', ROUND(((T2.NETSALES -T2.COGS)/NULLIF(T2.NETSALES,0)),2) AS '" + AggregateConstants.DISP_GROSSPROFITUPPERPERCENT +
        "',T2.NetSalesPerUnit AS '" + AggregateConstants.DISP_NETSALESPERUNITUPPER +
        "',T2.CostPerUnit AS '" + AggregateConstants.DISP_COGSPERUNITUPPER +
        "',T2.RowCount AS '" + AggregateConstants.DISP_ROWCOUNTUPPER +
        "',(T1.RowCount + T2.RowCount) AS '" + AggregateConstants.DISP_ROWCOUNT +
        "',  COALESCE(ROUND(((T2.NetSalesPerUnit-T1.NetSalesPerUnit)/NULLIF(T1.NetSalesPerUnit,0)),2),0) AS '" + AggregateConstants.DISP_CHANGEINNETSALEPERUNIT +
        "', COALESCE(ROUND(((T2.CostPerUnit - T1.CostPerUnit)/NULLIF(T1.CostPerUnit,0)),2),0)   AS '" + AggregateConstants.DISP_CHANGEINCOGSPERUNIT +
        """',
     CASE 
        WHEN (T1.NETSALESPERUNIT > 0 AND T1.COSTPERUNIT > 0 AND ((T2.NETSALESPERUNIT-T1.NETSALESPERUNIT)/T1.NETSALESPERUNIT) > 0) THEN
         ROUND(((((T2.NETSALESPERUNIT-T1.NETSALESPERUNIT)/T1.NETSALESPERUNIT)-((T2.COSTPERUNIT - T1.COSTPERUNIT)/T1.COSTPERUNIT)) / ((T2.NETSALESPERUNIT-T1.NETSALESPERUNIT)/T1.NETSALESPERUNIT)),2)
         ELSE 0 
     END 
     AS '""" + AggregateConstants.DISP_DELTA_PERCENTAGE +
        """'
    
    FROM T1 , T2 
    WHERE T1.CUSTOMERID=T2.CUSTOMERID AND 
    T1.REGELLIEFERANTNR = T2.REGELLIEFERANTNR AND 
    T1.MATERIAL=T2.MATERIAL AND
    T1.CMHIERARCHY3 = T2.CMHIERARCHY3
    ORDER BY T1.CUSTOMERID,T1.RegellieferantNr,T1.MATERIAL,T1.CMHierarchy3 """

def Matrix2D matrix2D = dmCtx.executeSqlQuery(combinedQuery, t1, t2)

def resultMatrix = matrix2D?.toResultMatrix()

/*
def List<String> columns=resultMatrix.getColumns()
columns.each{
    String colName -> resultMatrix.setColumnFormat(colName,AggregateConstants.AGGR_COL_FORMAT.get(colName))

}
 api.trace("dddd",resultMatrix)*/

resultMatrix.setDisablePreferences(false)

//This data will be used for doing t-test calculation
api.global.changUnitData = AggregateCommon.buildChangeUnitData(resultMatrix)
//api.trace("api.global.changUnitData", api.global.changUnitData)
if (out.ShowResultMatrix) {
    return resultMatrix
}
