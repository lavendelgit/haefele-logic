if (api.isSyntaxCheck()) return

def land  = api.currentItem("key1")
def customer_id = api.currentItem("key2")
def invoice_no = api.currentItem("key3")
def umsatz = api.currentItem("attribute1")

return isInvoicePresent(land, customer_id,invoice_no, umsatz) 

//return isInvoicePresent("Norwegen","0008363698","0104724408",umsatz)//1,226.9442083836)

def isInvoicePresent(land, customer_id, invoice_no, umsatz) 
{
    def dmCtx = api.getDatamartContext()
	def table = dmCtx.getTable("TransactionsDM")
	def query = dmCtx.newQuery(table)
  
  	def filters = [
      Filter.equal("Land", land),
      Filter.equal("CustomerId", customer_id),
      Filter.equal("InvoiceId", invoice_no)
  ]

  //query.select("COUNT(1)", "count")
  query.select("Land", "Land")
  query.select("CustomerId", "Customer Id")
  query.select("InvoiceId", "Invoice No")
  query.select("Revenue", "Revenue")
  query.where(*filters)

  def data = dmCtx.executeQuery(query)?.getData()
  //api.trace("data[0].Revenue",data[0].Revenue)
  if (data && data.size() > 0)
  {
    if ( api.formatNumber("##,###.##",data[0].Revenue) == api.formatNumber("##,###.##",umsatz))
    	return "Match"
    else
      return "Mismatch"
  }
  	
  else
    return "Missing"

}