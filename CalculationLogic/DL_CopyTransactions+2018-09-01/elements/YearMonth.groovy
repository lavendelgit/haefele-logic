def yearMonth = api.dateUserEntry("YearMonth")

if (yearMonth instanceof java.util.Date) {
    yearMonth = yearMonth.format("yyyy-MM-dd")
}

return yearMonth