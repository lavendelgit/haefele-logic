def data = api.getElement("Data")
/*
def salesTypes = api.getElement("SalesTypes")

def salesTypeGroup = salesTypes.collectEntries { salesType, desc ->
    if (salesType >= "P17" && salesType <= "P32") {
        return [salesType, "Clearing Rebate"]
    } else if (desc == "ZPAP" || desc == "CRM Nummer") {
        return [salesType, desc]
    } else {
        return [salesType, "Other"]
    }
}
*/
def clearingRebateTypes = api.getElement("ClearingRebateTypes")
def salesTypes = api.findLookupTableValues("SalesTypes")
   .collectEntries { pp -> [ (pp.name) : [
        description: pp.attribute1,
        insideOutside: pp.attribute2,
        priceIncreaseAffected: pp.attribute3
    ]]}


def salesTypeGroup = salesTypes.collectEntries { salesType, meta ->
    if (clearingRebateTypes.contains(salesType)) {
        return [salesType, "Clearing Rebate"]
    } else {
        [salesType, meta.insideOutside]
    }
}

salesTypeGroup["Unknown"] = "Unknown"

api.trace("salesTypeGroup", "", salesTypeGroup)

groupedData = [:].withDefault { key -> [:]}

data?.forEach { salesType, salesTypeData ->
    def group = salesTypeGroup[salesType] ?: "Undefined!"
    def groupData = groupedData[group];
    salesTypeData.forEach { d ->
        def groupMonthData = groupData[d.yearMonth]
        if (!groupMonthData) {
            groupData[d.yearMonth] = [
                    yearMonth: d.yearMonth,
                    count: d.count,
                    revenue: d.revenue,
                    salesType: group
            ]
        } else {
            groupMonthData.count += d.count
            groupMonthData.revenue += d.revenue
        }
    }
}

return groupedData
        .collectEntries { key, value ->
            return [key, value.values()]
        }