import net.pricefx.common.api.FieldFormatType

def activeClearingRebatesCount = api.getElement("ActiveClearingRebatesCount")
def dataGrouped = api.getElement("DataGrouped")

def months = dataGrouped["Clearing Rebate"]*.yearMonth ?: []

def resultMatrix = api.newMatrix("Sales Type", *months)

api.trace(months)

months.forEach { month ->
    resultMatrix.setColumnFormat(month, FieldFormatType.PERCENT)
}

def total = [:]
dataGrouped.forEach { salesType, records ->
    def row = [
            "Sales Type": salesType
    ]
    records.forEach { r ->
        def value = r.count / activeClearingRebatesCount
        row.put(r.yearMonth, value)
        total[r.yearMonth] = (total[r.yearMonth] ?: 0) + value
    }
    resultMatrix.addRow(row)
}

def unused = [
        "Sales Type": resultMatrix.styledCell("Unused")
                                  .withToolTip("Active clearing rebates not used in any transaction")
    ] << total.collectEntries { key, value -> [key, 1 - value]}

resultMatrix.addRow(unused);

return resultMatrix