if (api.isSyntaxCheck()) return [:]

def dmCtx = api.getDatamartContext()
def datamartQuery = dmCtx.newQuery(dmCtx.getTable("TransactionsDM"))
def yearAgo = lib.Date.yearAgoFirstOfMonth()


def filters = [
        Filter.greaterThan("Revenue", 0),
        Filter.greaterThan("Units", 0),
        Filter.greaterThan("MarginAbs", 0),
        Filter.greaterThan("Cost", 0),
        Filter.greaterThan("SalesPricePer100Units", 0),
        Filter.notEqual("InvoicePos", "999999"),
        Filter.greaterOrEqual("InvoiceDate", yearAgo),
        Filter.isNotNull("ClearingRebatePrice"),
   		Filter.in("SalesOffice","DE01", "DE02", "DE03", "DE04", "DE05", "DE06", "DE07", "DE08", "DE09", "DE10", "DE20")
]

datamartQuery.select("YearMonthMonth", "yearMonth")
datamartQuery.select("SalesType", "salesType")
datamartQuery.select("COUNT(CustomerId)", "count")
datamartQuery.select("SUM(Revenue)", "revenue")
datamartQuery.select("SUM(ClearingRebatePotentialRevenue)", "shbRevenue")
datamartQuery.orderBy("yearMonth asc")
datamartQuery.where(*filters)

def result = dmCtx.executeQuery(datamartQuery)

if (result == null) {
    api.addWarning("Could not get result for Sales query.")
}

api.trace("result.summary", "", result?.getSummary())

result?.getData()
        ?.collect()
        ?.groupBy { r -> r.salesType ?: "Unknown"}
        ?.sort({ e1, e2 ->
    def num1 = e1.key == "Unknown" ? 1000 : (e1.key.substring(1) as Integer)
    def num2 = e2.key == "Unknown" ? 1000 : (e2.key.substring(1) as Integer)
    return num1 <=> num2
})