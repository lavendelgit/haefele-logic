def ready = "Yes".equals(api.getElement("RfP_PL"))
def sku = api.getElement("Sku")
def cid = api.getElement("CustomerID")
def mpl = api.getElement("NettoMPL")
def np = api.vLookup("Nettopreise", "Nettopreis", sku, cid)

if (ready && (mpl == null) && np == null) {
  return "Yes"
}

return "No"
