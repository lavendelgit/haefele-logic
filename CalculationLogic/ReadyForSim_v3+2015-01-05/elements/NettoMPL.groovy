def cid = api.getElement("CustomerID")
def sku = api.getElement("Sku")

if (!cid || !sku) {
  return null
}

// find all MPLs for the customer ID
def cas = api.findCustomerAssignments(cid)
for (ca in cas) {
  // for each MPL find the sku
  def plid = ca.assignment.tokenize(".")[0]
  api.trace("Customer " + cid + " found in MPL", null, plid)
  def mplis = api.find("MPLI", 
                       Filter.equal("pricelistId", plid), 
                       Filter.equal("sku", sku))
  
  // if sku found in the MPL, we're done
  if (mplis) {
    api.trace("Sku " + sku + " found in MPL " + plid, null, mplis)
    return true
  } else {
    api.trace("Sku " + sku + " not found in MPL " + plid, null, null)
  }
}

return null