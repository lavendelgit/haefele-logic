def findAll(def type, def fields, def filter) {
  def out = []
  int i = 0
  int s = 200
  while (true) {
    def items = api.find(type, i, s, null, fields, filter)
    out.addAll(items)
    i += s
    if (items.size() < s) {
      break
    }
  }
  return out
}