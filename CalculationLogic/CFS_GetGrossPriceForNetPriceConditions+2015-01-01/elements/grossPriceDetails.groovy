def sku = out.sku

if (out.isTopcoProduct) {
  return ["GrossPrice" : 0, "MaterialType" : "TOPCO"]
} else {
  return getGrossPrice(sku)
}


def getGrossPrice(sku)
{
  def skuLength = sku.length()

  	def salesPrice = getSalesPrice(sku)?.attribute3
  
  //api.trace('salesPrice',salesPrice)
  
    if (!salesPrice && skuLength == 11) {
  		salesPrice = getSalesPrice(sku.substring(0,10))?.attribute3
    }

    if (salesPrice) return ["GrossPrice" : salesPrice, "MaterialType" : "NORMAL"]

    def compoundPrice = getCompoundPrice(sku)?.attribute1

    if (!compoundPrice && skuLength == 11) {
      compoundPrice = getCompoundPrice(sku.substring(0,10))?.attribute1      
    }
  
  	if (compoundPrice) return ["GrossPrice" : compoundPrice, "MaterialType" : "BOM"]
  
  return ["GrossPrice" : 0, "MaterialType" : "N/A"]
}

def getSalesPrice(sku) {
  
  return lib.Find.latestSalesPriceRecord(sku,Filter.equal("attribute7","EUR"))           //Filter.greaterOrEqual("attribute2",out.ValidToDate),

}

def getCompoundPrice(sku) {
  
  return lib.Find.latestCompoundPriceRecord(sku,Filter.equal("attribute5","EUR"))          //Filter.greaterOrEqual("attribute2",out.ValidToDate),

}

