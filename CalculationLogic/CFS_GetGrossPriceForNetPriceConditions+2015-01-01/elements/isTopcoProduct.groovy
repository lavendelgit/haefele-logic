if (!api.global.topco) {
    api.global.topco = api.findLookupTableValues("TopCoProducts")*.name as Set
}

api.trace("api.global.topco",api.global.topco.contains(out.SKU))

if (api.global.topco.contains(out.SKU))
    return true

return false
