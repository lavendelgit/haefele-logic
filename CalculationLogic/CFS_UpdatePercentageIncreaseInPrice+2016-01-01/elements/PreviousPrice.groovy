String pxTableName = out.PxTable
String sku = out.SKU

BigDecimal previousPrice
List attributesColumns = []

String attributeValidFrom = out.PxTableAttributes["validFromAttribute"]

out.PxTableAttributes["keyAttributesName"]?.split(",")?.each { cols ->
    attributesColumns << ["keyColumnName": cols, "keyColumnValue": api.currentItem("$cols")]
}

previousPrice = Library.getPreviousPrice(out.SKU, out.PxTable, attributeValidFrom, "-$attributeValidFrom", attributesColumns)

if (previousPrice) {
    return libs.SharedLib.RoundingUtils.round(previousPrice, 4)
}

return out.CurrentPrice