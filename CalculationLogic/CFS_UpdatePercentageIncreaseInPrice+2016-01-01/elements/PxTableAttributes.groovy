if (!api.global.conditionTableAttributes) {
    api.global.conditionTableAttributes = libs.HaefeleCommon.PriceChangeTracking.getMetadataForConditionTables()
}

return api.global.conditionTableAttributes[out.PxTable]