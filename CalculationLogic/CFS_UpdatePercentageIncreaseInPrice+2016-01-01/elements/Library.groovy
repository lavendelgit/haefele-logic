protected BigDecimal getPreviousPrice(String sku, String pxTableName, String validFromAttribute, String orderBy, List attributeColumns = null) {
    String attributeCost = out.PxTableAttributes["costAttribute"]
    List filters = [Filter.equal("sku", sku),
                    Filter.lessThan("$validFromAttribute", out.ValidFromDate),
                    Filter.equal("name", out.PxTableAttributes["pxTablename"])]

    attributeColumns?.each { Map attributeDetails ->
        String attributeName = attributeDetails.keyColumnName
        String attributeValue = attributeDetails.keyColumnValue
        filters << Filter.equal("$attributeName", "$attributeValue")
    }

    Map previousPriceRow = api.find("PX50", 0, 1, orderBy, *filters)?.find()
    api.local.previousPriceRow = previousPriceRow
    return previousPriceRow ? previousPriceRow["$attributeCost"] : BigDecimal.ZERO

}