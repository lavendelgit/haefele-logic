if (out.CurrentPrice) {
  
  BigDecimal priceDiff = (out.CurrentPrice - out.PreviousPrice)/out.PreviousPrice

  return libs.SharedLib.RoundingUtils.round(priceDiff, 4)

}