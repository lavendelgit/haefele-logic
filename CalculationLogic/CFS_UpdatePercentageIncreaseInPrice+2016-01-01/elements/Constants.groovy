import groovy.transform.Field

@Field String PX_TABLE_FILTER_LABEL = "Select PX Table"
@Field String PX_ZPAP_TABLE = "S_ZPAP"
@Field String PX_ZPF_TABLE = "S_ZPF"
@Field String PX_ZPM_TABLE = "S_ZPM"
@Field String PX_ZPP_TABLE = "S_ZPP"
@Field String PX_ZPA_TABLE = "S_ZPA"
@Field String PX_ZPC_TABLE = "S_ZPC"
@Field String PX_ZPZ_TABLE = "S_ZPZ"
@Field String PX_ZNRV_TABLE = "S_ZNRV"
@Field String PX_ZPE_TABLE = "S_ZPE"

@Field Map PX_TABLE_VALUES = [
        "ZPAP": PX_ZPAP_TABLE,
        "ZPF" : PX_ZPF_TABLE,
        "ZPM" : PX_ZPM_TABLE,
        "ZPP" : PX_ZPP_TABLE,
        "ZPA" : PX_ZPA_TABLE,
        "ZPC" : PX_ZPC_TABLE,
        "ZPZ" : PX_ZPZ_TABLE,
        "ZNRV": PX_ZNRV_TABLE,
        "ZPE" : PX_ZPE_TABLE
]

@Field String VALIDATION_NO_PRICE_CHANGE = "No Price Change"
@Field String VALIDATION_PRICE_REDUCTION = "Price Reduction"
@Field String VALIDATION_PRICE_INCREASE = "Price Increase"