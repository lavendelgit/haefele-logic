import groovy.transform.Field

BigDecimal priceDiff = out.PriceDiff

return (priceDiff < 0) ? Constants.VALIDATION_PRICE_REDUCTION : 
        (priceDiff > 0) ? Constants.VALIDATION_PRICE_INCREASE : Constants.VALIDATION_NO_PRICE_CHANGE 