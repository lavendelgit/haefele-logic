String attributeKBETR = out.PxTableAttributes["costAttribute"]
BigDecimal currentPrice = api.currentItem(attributeKBETR)

return libs.SharedLib.RoundingUtils.round(currentPrice, 4)