if (api.isSyntaxCheck()) {

    def pxTableOptions = api.findLookupTableValues("PriceChangeTrackingMetaConfigurations","key1").collectEntries {
        [(it.key2): it.key1]
    }
    api.trace("pxTableOptions", pxTableOptions)

    api.option("PXTable", pxTableOptions.keySet() as List, pxTableOptions)
    def pxTableParam = api.getParameter("PXTable")
    pxTableParam.setLabel(Constants.PX_TABLE_FILTER_LABEL)
    pxTableParam.setRequired(true)
    return
} else {
    // Executed in NON-syntax check mode
    if (!input.PXTable) {
        api.addWarning("Select PX table")
        return null
    }

    return input.PXTable
}