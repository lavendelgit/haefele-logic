import groovy.transform.Field

@Field final String NOTIFICATION_MESSAGE_TABLE_OPEN = """ <html> <head>
        <style>
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }
        td, th {
        border: 1px solid #dddddd;
        text-align: center; 
        padding: 8px;
         }

        tr:nth-child(even) {
        background-color: #dddddd;
        }
        </style>
        </head>
        <body>
        <h1>Notification For Simulation Dashboard</h1>
          <table>"""

@Field final String NOTIFICATION_MESSAGE_TABLE_CLOSE = """</table>
        </body>
        </html>"""