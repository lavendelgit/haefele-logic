import org.pojava.datetime.DateTime

List simulationNotifyRows = api.findLookupTableValues("SimulationInputs", Filter.equal("attributeExtension___simulation_notify", "Yes"))

simulationNotifyRows?.each { simulationRow ->
    String simulationName = simulationRow["attributeExtension___simulation_inputs"]["SimulationName"]
    String simulationOwner = simulationRow["attributeExtension___simulation_owner"]
    String status = simulationRow["attributeExtension___status"]
    Map simulationInputs = simulationRow["attributeExtension___simulation_inputs"]

    if (status == 'Ready') {
        String messages = ""
        String greetings
        String subject = " Notification For $simulationName Dashboard "

        messages = """<tr><td style=" color: brown; "><b> $simulationName Dashboard is ready for the review</b></td></td></tr>"""
        String noReply = "<i>Please do not reply to this email as it is an automated email from Pricefx</i>"
        String emailTemplate = """ <br> $messages <br><br>$noReply<br>Best Regards,<br>Your Pricing Team!"""

        List users = api.findLookupTableValues("NotificationSubscriptions")
                ?.findAll { it.attribute2 == "Yes" }
                ?.collect { it.name }
        if (!users.contains(simulationOwner)) users << simulationOwner

        Map email = api.find("U", 0, "email", Filter.in("loginName", users))
                ?.collectEntries { currentRow -> [(currentRow.email): currentRow.firstName] }

        email.each {
            greetings = """Hi ${it.value}"""
            api.sendEmail(it.key, subject, greetings + emailTemplate)
        }
        callBoundCallUtilFunction(simulationName, status, simulationInputs, simulationOwner, "No")
    }
}

void callBoundCallUtilFunction(String simulationName, String simStatus, Map simulation_inputs, String user, String notify) {
    Date today = new Date()
    Map simulationRow = [status           : simStatus,
                         schedule_date    : today.format("yyyy-MM-dd'T'HH:mm:ss.SSS"),
                         simulation_owner : user,
                         simulation_inputs: simulation_inputs,
                         simulation_notify: notify]
    libs.HaefeleCommon.BoundCallUtils.addOrUpdateJSONTableData("SimulationInputs", null, "JLTV", [simulationName], [simulationRow])
}