if (api.isDebugMode()) {
    return "000.10.280" //"730.11.107"
}

return api.currentItem()?.sku ?: api.product("sku")