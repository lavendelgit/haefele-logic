BigDecimal convertToEur (String fromCurrency, BigDecimal price)
{
  BigDecimal convertedPrice =  price?:0

  if (fromCurrency != "EUR") {
      if (!api.global.exchangeRate) {
        api.global.exchangeRate = api.findLookupTableValues("ExchangeRate").collectEntries {
                                      [(it.name): it.attribute1]
                                  }
      }
      
      BigDecimal exchangerate = api.global.exchangeRate[fromCurrency]?:1
      convertedPrice = exchangerate * convertedPrice
	}
  return convertedPrice
}