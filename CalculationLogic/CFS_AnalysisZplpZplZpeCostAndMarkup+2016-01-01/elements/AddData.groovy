if (api.isSyntaxCheck()) return

BigDecimal zplMarkup
BigDecimal zplpMarkup
BigDecimal zplMargin
BigDecimal zplpMargin
String zplGreaterThanZpe
String zplpGreaterThanZpe

String sku 				= out.SKU
BigDecimal zplPrice 	= Library.convertToEur(out.GetZPLPriceRecord?.attribute8, out.GetZPLPriceRecord?.attribute7)
String zplValidFrom 	= out.GetZPLPriceRecord?.attribute12
String zplValidTo		= out.GetZPLPriceRecord?.attribute13

BigDecimal zplpPrice 	= Library.convertToEur(out.GetZPLPPriceRecord?.attribute7, out.GetZPLPPriceRecord?.attribute3)
String zplpValidFrom 	= out.GetZPLPPriceRecord?.attribute1
String zplpValidTo		= out.GetZPLPPriceRecord?.attribute2

BigDecimal zpePrice		= out.GetZPEPriceRecord?.attribute3
String zpeValidFrom 	= out.GetZPEPriceRecord?.attribute1
String zpeValidTo		= out.GetZPEPriceRecord?.attribute2



if (zpePrice)
{
  zplMarkup  =  zplPrice ? (zplPrice - zpePrice)/zpePrice:0
  zplpMarkup =  zplpPrice ? (zplpPrice - zpePrice)/zpePrice:0

  zplMargin  =  zplPrice ? (zplPrice - zpePrice)/zplPrice:0
  zplpMargin =  zplpPrice ? (zplpPrice - zpePrice)/zplpPrice:0
  
  zplGreaterThanZpe  = (zplPrice > zpePrice) ? "Yes":"No"
  zplpGreaterThanZpe = (zplpPrice > zpePrice) ? "Yes":"No"
}

def row = [
        "name"	:"ZplZplpPriceAnalysis",
        "sku"	: sku,
        "attribute1":zplpPrice,
  		"attribute2":zplpValidFrom,
  		"attribute3":zplpValidTo,
        "attribute4":zplPrice,
  		"attribute5":zplValidFrom,
  		"attribute6":zplValidTo,
  		"attribute7":zpePrice,
  		"attribute8":zpeValidFrom,
  		"attribute9":zpeValidTo,
  		"attribute10":zplGreaterThanZpe,
        "attribute11":zplpGreaterThanZpe,
		"attribute12":zplMarkup,
		"attribute13":zplpMarkup,
		"attribute14":zplMargin,
		"attribute15":zplpMargin
  
]
api.trace("*** row ***",row)
api.addOrUpdate("PX50", row)



