if (api.isSyntaxCheck()) return

String targetDate = out.TargetDate

List filters = [
        Filter.lessOrEqual("attribute12", targetDate),
        Filter.greaterOrEqual("attribute13", targetDate),
        Filter.or(Filter.equal("attribute6","DEX1"),Filter.equal("attribute6",""),Filter.isNull("attribute6"))
]

def zplRecord = getLatestZPLCost(out.SKU, *filters)

if (zplRecord) {
    return zplRecord
}

return null

def getLatestZPLCost(String sku, Filter... additionalFilters) {
    def targetDate = out.TargetDate

    def filters = [
            Filter.equal("name", "S_ZPL"),
            Filter.equal("sku", sku)
    ]

    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
  
    def orderBy = "-attribute1"
    def ZPLRecord = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (ZPLRecord[0]) {
        result = ZPLRecord[0]
    }

    return result
}
