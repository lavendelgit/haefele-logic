if (api.isSyntaxCheck()) return

def sku = out.SKU
String targetDate = out.TargetDate

def filter = [
        Filter.lessOrEqual("attribute1", targetDate),
        Filter.greaterOrEqual("attribute2", targetDate)
]

def salesPriceRecord = lib.Find.latestSalesPriceRecord(sku, *filter)

if (salesPriceRecord) {
    return salesPriceRecord
}
