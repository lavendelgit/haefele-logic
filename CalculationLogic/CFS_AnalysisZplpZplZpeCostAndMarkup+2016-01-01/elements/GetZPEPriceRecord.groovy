String sku = out.SKU
String targetDate = out.TargetDate

List filters = [
        Filter.equal("attribute16", "EUR"),
        Filter.lessOrEqual("attribute1", targetDate),
        Filter.greaterOrEqual("attribute2", targetDate)
]

def baseCostRecord = lib.Find.latestBaseCostRecord(sku, *filters)

if (baseCostRecord) {
    return baseCostRecord
}

