if (api.isSyntaxCheck()) return []

def dmCtx = api.getDatamartContext()
def salesDM = dmCtx.getTable("SalesHistoryDM")
def datamartQuery = dmCtx.newQuery(salesDM)

def filters = lib.Filter.customersGermany()

/*The Should have Been Revenue was not getting rendered and it was found that this was due to filter condition that we have added here.
THe SalesHistoryDM has no field like SM...  
if (api.isUserInGroup("PGM", api.user().loginName)) {
    def pgm = api.findLookupTableValues("ProductGroupManager")?.find()?.value
    api.trace("pgm", "", pgm)
    filters.add(com.googlecode.genericdao.search.Filter.equal("SM", pgm))
}
*/

datamartQuery.select("SUM(Revenue)", "isRevenue")
datamartQuery.select("SUM(ShouldHaveBeenRevenue)", "shouldHaveBeenRevenue")
datamartQuery.select("SUM(SHBRevenueMinus30Perc)", "shouldHaveBeenRevenueMinus30Perc")
datamartQuery.select("YearMonthMonth", "yearMonth")
datamartQuery.where(*filters)

def result = dmCtx.executeQuery(datamartQuery)

api.trace("result", "", result)

def records = result?.getData()?.collect{ r ->
    r.shouldHaveBeenRevenue = r.shouldHaveBeenRevenue ?: 0
    r.shouldHaveBeenRevenueMinus30Perc = r.shouldHaveBeenRevenueMinus30Perc ?: 0
    r << [
            discrepancy1: (r.isRevenue - r.shouldHaveBeenRevenue) / r.isRevenue * 100,
            discrepancy2: (r.isRevenue - r.shouldHaveBeenRevenueMinus30Perc) / r.isRevenue * 100
    ]
} ?: []

api.trace("records", "", records)

return records