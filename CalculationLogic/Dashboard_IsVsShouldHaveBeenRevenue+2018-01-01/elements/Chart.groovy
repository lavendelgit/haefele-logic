def data = api.getElement("Data")

def categories = data*.yearMonth
def isRevenueData = data*.isRevenue
def shouldHaveBeenRevenueData = data*.shouldHaveBeenRevenue
def shouldHaveBeenRevenueMinus30PercData = data*.shouldHaveBeenRevenueMinus30Perc
def discrepancy1Data = data*.discrepancy1
def discrepancy2Data = data*.discrepancy2

def chartDef = [
        chart: [
                type: 'column',
                zoomType: 'x'
        ],
        title: [
                text: 'Is vs Should Have Been Revenue'
        ],
        xAxis: [[
                        title: [
                                text: "Month"
                        ],
                        categories: categories
                ]],
        yAxis: [[
                        title: [
                                text: "Revenue"
                        ]
                ], [
                        title: [
                                text: "Difference"
                        ],
                        opposite: true
                ]],
        series: [[
                        name: "Is Revenue",
                        data: isRevenueData ,
                        dataLabels: [
                                enabled: true,
                        ]
                 ],
                 [
                        name: "Should Have Been Revenue",
                        data: shouldHaveBeenRevenueData,
                        dataLabels: [
                                enabled: true,
                                format: "{point.y:,.0f}"
                        ]
                 ], [
                         name: "Should Have Been Revenue -30%",
                         data: shouldHaveBeenRevenueMinus30PercData,
                         dataLabels: [
                                 enabled: true,
                                 format: "{point.y:,.0f}"
                         ]
                 ], [
                        name: "Difference IS vs SHB (%)",
                        type: "line",
                        data: discrepancy1Data,
                        yAxis: 1
                 ],
                 [
                         name: "Difference IS vs SHB-30% (%)",
                         type: "line",
                         data: discrepancy2Data,
                         yAxis: 1
                 ]]
]

api.buildFlexChart("IsVsShouldHaveBeenRevenueTemplate", chartDef)