import net.pricefx.common.api.pa.FieldType

model.addTable("my_table",
        [
                label : "My Table",
                fields: [
                        [name: "a_text_field", label: "A Text Field", type: FieldType.TEXT, key: true],
                        [name: "a_numeric_field", label: "A Numeric Field", type: FieldType.NUMBER]
                ]
        ]
)

def dmCtx = api.getDatamartContext()

def query = dmCtx.newSqlQuery()
// source not used but needed by the api
query.addSource(dmCtx.newQuery(model.table("my_table"), false).selectAll(true))
query.setQuery("SELECT * FROM (VALUES ('foo', 1), ('bar', 2)) AS t (a_text_field, a_numeric_field)")

return model.loadTable(model.table("my_table"), query, true)
