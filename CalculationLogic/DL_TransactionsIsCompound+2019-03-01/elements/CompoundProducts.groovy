api.retainGlobal = true;

if (!api.global.compoundProducts) {
    api.global.compoundProducts = [:]

    def stream = api.stream("PBOME", "sku", ["sku"]);
    if (!stream) {
        return []
    }

    while (stream.hasNext()) {
        api.global.compoundProducts[stream.next().sku] = true
    }

    api.trace(api.global.compoundProducts.size())

    stream.close();
}

api.global.compoundProducts