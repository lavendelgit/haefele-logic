def inputSalesOffice = api.getElement("Verkaufsbereich3")
api.trace ("Sales Office", "Value("+inputSalesOffice+")")
def outputSalesOffice = ""
if (inputSalesOffice) {
  outputSalesOffice = lib.Find.getSalesOfficeCode (inputSalesOffice)

}
return outputSalesOffice