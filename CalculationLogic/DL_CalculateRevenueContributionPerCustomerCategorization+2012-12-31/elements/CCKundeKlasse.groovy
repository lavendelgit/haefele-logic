def inputPotentialGroup = api.getElement("KundeKlasse")
def outputCustomerPotentialGroup = lib.Find.findCustomerPotentialGroup(inputPotentialGroup)
if (outputCustomerPotentialGroup?.size() == 5) {
  return   outputCustomerPotentialGroup.get(0) + " " + outputCustomerPotentialGroup.get(1) + " - " + outputCustomerPotentialGroup.get(4)
}
return api.local.notDefinedCustomerDiscountGroup