import java.math.RoundingMode

def revenue = api.getElement("p2_Revenue")
def outcome = api.local.notDefinedCustomerDiscountGroup
if (revenue) {
	outcome = lib.Find.getMatchingCustomerPotentialGroup (revenue)
}

return outcome