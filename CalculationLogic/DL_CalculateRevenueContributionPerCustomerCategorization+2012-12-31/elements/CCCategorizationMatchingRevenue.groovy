def actualCategory = api.getElement("KundeKlasse")
def computedValue = api.getElement("CCComputedPotentialGroup")

return (actualCategory?.contains(computedValue))