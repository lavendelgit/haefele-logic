def currentSalesPrice = api.getElement("SalesPrice")
def discount = api.getElement("Discount")

if (currentSalesPrice == null) {
    api.redAlert("SalesPrice")
    api.logWarn("[ManualDiscount.DiscountedPrice] SalesPrice is null, SKU", api.currentItem()?.sku)
    return null
}

if (discount == null) {
    api.redAlert("Discount")
    api.logWarn("[ManualDiscount.DiscountedPrice] Discount is null, SKU", api.currentItem()?.sku)
    return null
}

def result = lib.Rounding.roundMoney(currentSalesPrice * ( 1 - discount))

return result