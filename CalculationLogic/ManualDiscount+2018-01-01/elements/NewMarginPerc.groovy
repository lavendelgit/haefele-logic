def salesPriceProposal = api.getElement("SalesPriceProposal")
def newMarginAbs = api.getElement("NewMarginAbs")

if (salesPriceProposal == null) {
    api.redAlert("SalesPriceProposal")
    return null
}

if (!newMarginAbs) {
    api.redAlert("NewMarginAbs")
    return null
}

return newMarginAbs / salesPriceProposal;
