def salesPriceProposal = api.getElement("SalesPriceProposal")
def baseCost = api.getElement("BaseCost")

if (salesPriceProposal == null) {
    api.redAlert("SalesPriceProposal")
    return null
}

if (baseCost == null) {
    api.redAlert("BaseCost")
    return null
}

return salesPriceProposal - baseCost;
