def sku = api.getElement("CurrentItem")?.sku
def discount = api.getElement("CurrentItem")?.attribute1

api.logInfo("[ManualDiscount.Discount] SKU: ${sku}, Discount", discount)

if (discount == null) {
    api.local.warning += "Discount is not specified. "
}

return discount
