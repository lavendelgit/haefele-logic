def currentMarginAbs = api.getElement("CurrentMarginAbs")
def currentSalesPrice = api.getElement("SalesPrice")

if (currentMarginAbs == null) {
    api.redAlert("CurrentMarginAbs")
    return null
}

if (currentSalesPrice == null) {
    api.redAlert("SalesPrice")
    return null
}

return currentMarginAbs / currentSalesPrice;
