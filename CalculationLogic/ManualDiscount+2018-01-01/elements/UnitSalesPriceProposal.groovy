def salesPriceProposal = api.getElement("SalesPriceProposal")
def priceUnits = api.getElement("SalesPriceRecord")?.priceUnits ?: 100

if (salesPriceProposal != null) {
    return lib.Rounding.roundUpTo10Cents(salesPriceProposal / priceUnits)
}