def sku = api.getElement("CurrentItem")?.sku
def salesPriceRecord = api.getElement("SalesPriceRecord")

api.logInfo("[ManualDiscount.SalesPrice] SKU: ${sku}, SalesPrice", salesPriceRecord)

if (salesPriceRecord == null || salesPriceRecord.salesPrice == null) {
    api.local.warning +=  "Sales price is missing. "
}

return salesPriceRecord.salesPrice