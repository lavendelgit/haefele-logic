def salesPriceProposal = api.getElement("SalesPriceProposal")
def currentBaseCost = api.getElement("BaseCost")

if (salesPriceProposal == null) {
    api.redAlert("SalesPriceProposal")
    return null
}

if (!currentBaseCost) {
    api.redAlert("BaseCost")
    return null
}

return salesPriceProposal / currentBaseCost - 1