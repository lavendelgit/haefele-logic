if (api.isSyntaxCheck()) return [:]

def sku = api.product("sku")
def discountedSalesPrice = api.getElement("DiscountedSalesPrice")

return lib.BEP.calculate(sku, discountedSalesPrice)