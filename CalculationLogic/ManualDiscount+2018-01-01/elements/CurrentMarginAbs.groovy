def currentSalesPrice = api.getElement("SalesPrice")
def currentBaseCost = api.getElement("BaseCost")

if (currentSalesPrice == null) {
    api.redAlert("SalesPrice")
    return null
}

if (currentBaseCost == null) {
    api.redAlert("BaseCost")
    return null
}

return currentSalesPrice - currentBaseCost;
