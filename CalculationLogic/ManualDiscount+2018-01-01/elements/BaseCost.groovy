def sku = api.getElement("CurrentItem")?.sku
def baseCost = lib.Find.currentBaseCost(sku)

api.logInfo("[ManualDiscount.BaseCost] SKU: ${sku}, BaseCost", baseCost)

if (baseCost == null) {
    api.local.warning +=  "Base cost is missing. "
}

return baseCost