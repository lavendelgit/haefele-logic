def discountedSalesPrice = api.getElement("DiscountedSalesPrice")
def recommendedSalesPrice = api.getElement("BEP").recommendedSalesPrice

if (discountedSalesPrice != null && recommendedSalesPrice != null && discountedSalesPrice < recommendedSalesPrice) {
    api.local.warning += "Discounted price is too low. Please see recommended price. "
}

return lib.Rounding.roundMoney(recommendedSalesPrice)