def currentItem = api.currentItem()

if (currentItem == null) {
    currentItem = (api.productExtension("ManualDiscount") ?: [[:]]).first() // only for test runs
}

return currentItem