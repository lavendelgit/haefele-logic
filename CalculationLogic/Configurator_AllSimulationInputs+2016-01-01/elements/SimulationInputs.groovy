import net.pricefx.server.dto.calculation.ConfiguratorEntry

String simulationName
def simulationCommon = libs.HaefeleCommon.Simulation

List simulationInputOptions = api.findLookupTableValues("SimulationInputs", api.global, ['name'], 'name')?.name

ConfiguratorEntry inputConfigurator = Library.createInputConfiguratorWithHiddenStoredValue("AllSimulationInputs")
Library.createDropdown(inputConfigurator, "SimulationName", "Select Simulation", null, simulationInputOptions, false, false, false) { String selectedSimulation ->
    simulationName = selectedSimulation
}

//Load additional fields only if simulation is selected
if (simulationName) {
    //initilizing to empty map incase of null
    Map inputs = [:]
    Map simulationData = getSavedSimulationData(simulationName)
    Map simStatus = simulationCommon.SIMULATION_STATUS[simulationData.attributeExtension___status?.toUpperCase()] ?: simulationCommon.SIMULATION_STATUS.NEW

    if (!simStatus.allowSimulation) {
        //Library.manageApplySettings(inputConfigurator, true) PLS DON"T DELETE
        inputConfigurator.setMessage("<div style='color:#FF0000;font-size: 12px'>? ${simStatus.info}</div>")
    } else {
        //Library.manageApplySettings(inputConfigurator, false) PLS DON"T DELETE
        inputConfigurator.setMessage("<div style='color:${simStatus.color ?: '#004D43'};font-size: 12px'>*** ${simStatus.info}</div>")
    }
    simulationData = simulationData?.attributeExtension___simulation_inputs ?: [:]
    simulationData.Status = simStatus.name

    simulationCommon.SIMULATION_INPUT_COLUMNS.each { String inputName, Map inputDef ->
        switch (inputDef.type) {
            case "Text":
                Library.createTextField(inputConfigurator, inputName, inputDef.label, simulationData[inputName], false, inputDef.readOnly ?: false, true)
                break
            case "DatamartFilter":
                Library.createDatamartFilterBuilder(inputConfigurator, inputName, inputDef.label, Constants.DM_SIMULATION, simulationData[inputName])
                break
            case "Option":
                List options = inputDef.options
                if (!options && inputDef.optionsLoader) {
                    options = inputDef.optionsLoader()
                }

                Library.createDropdown(inputConfigurator, inputName, inputDef.label, simulationData[inputName], options, false, false, true)
                break
            case "PopupConfigurator":
                Library.createPopupConfigurator(inputConfigurator, inputName, inputDef.label, inputDef.configuratorLogic, simulationData[inputName])
                break
            case "Hidden":
                Library.createHiddenField(inputConfigurator, inputName, inputDef.label, simulationData[inputName], false, inputDef.readOnly ?: false, true)
                break
        }
    }

    if (simStatus.confirmAllowSimulation) {
        Library.createBooleanField(inputConfigurator, "RescheduleSimulation", "Re-run Simulation? Existing data would be overriden.", false, false, false, true)
    }

}

return inputConfigurator


Map getSavedSimulationData(String simulationName) {
    return api.findLookupTableValues("SimulationInputs", Filter.equal('name', simulationName))?.find()
}
