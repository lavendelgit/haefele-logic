import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ContextParameter

/**
 * This is a "hack" to save the input value that is not persisted between logic refreshes on changes
 * @param calculationType
 * @return
 */
protected ConfiguratorEntry createInputConfiguratorWithHiddenStoredValue(String pageName) {
    def inputConfigurator = api.createConfiguratorEntry(InputType.HIDDEN, "calculationType")
    if (pageName) {
        inputConfigurator.getFirstInput().setValue(pageName)
    }

    return inputConfigurator
}

protected ConfiguratorEntry createPopupConfigurator(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String logicName, Map inputs, Closure selValueHandler = null) {
    def filterInput = configuratorEntry.createParameter(InputType.CONFIGURATOR, fieldName)
    filterInput.addParameterConfigEntry("noRefresh", true)
    filterInput.setURL(logicName)
    filterInput.setLabel(fieldLabel)
               .setRequired(false)
               .setReadOnly(false)

    //Pass value to configurator
    if (inputs && (filterInput && !filterInput.getValue()) || !filterInput.getValue()?.initialized) {
        filterInput.setValue(inputs)
    }

    //Collect the configurator value and return
    if (selValueHandler) {
        selValueHandler.call(filterInput.getValue())
    }

    return configuratorEntry
}

private ConfiguratorEntry createDatamartFilterBuilder(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String dmName, Object filters, Closure selValueHandler = null) {
    def filterInput = configuratorEntry.createParameter(InputType.DMFILTERBUILDER, fieldName)
    filterInput.addParameterConfigEntry("dmSourceName", dmName)
    filterInput.addParameterConfigEntry("noRefresh", true)
    filterInput.setLabel(fieldLabel)
               .setRequired(false)
               .setReadOnly(false)


    if (filters) {
        if (filters instanceof List) {
            Map filterConfig = [:]
            filterConfig['criteria'] = Filter.and(*filters)
            filterInput.setFilter(filterConfig)
        } else {
            filterInput.setValue(filters)
        }
    }

    if (selValueHandler) {
        String strFilter = null
        if (filterInput.getValue() instanceof Map) {
            //api.logInfo("filterInput.getValue()",filterInput.getValue())
            strFilter = api.filterFromMap(filterInput.getValue()).toQueryString()
        }
        selValueHandler.call(strFilter)
    }

    return configuratorEntry
}


protected ConfiguratorEntry createPercentField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.USERENTRY, fieldName)
    input.setValueHint('%')
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    return configuratorEntry
}

protected ConfiguratorEntry createDropdown(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, List optionValues, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.OPTION, fieldName)
    input.setValueOptions(optionValues)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    if (defaultValue) {
        input.setValue(defaultValue)
    }

    return configuratorEntry
}

protected ConfiguratorEntry createTextField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.STRINGUSERENTRY, fieldName)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    input.setValue(defaultValue)
    return configuratorEntry
}


protected ConfiguratorEntry createHiddenField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.HIDDEN, fieldName)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
  	input.setValue(defaultValue)
    return configuratorEntry
}

protected void manageFieldAttributes(ContextParameter input, String fieldLabel, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    input.setRequired(isRequired)
         .setReadOnly(isReadOnly)
         .setLabel(fieldLabel)
         .addParameterConfigEntry("noRefresh", isNoRefresh)

    if (selValueHandler) {
        selValueHandler.call(input.getValue())
    }
}

protected ConfiguratorEntry manageApplySettings(ConfiguratorEntry configuratorEntry, boolean disable) {
    ContextParameter input = configuratorEntry.createParameter(InputType.HIDDEN, "DisableApply")
    input.setRequired(disable)
    return configuratorEntry
}

protected ConfiguratorEntry createBooleanField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, boolean defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.BOOLEANUSERENTRY, fieldName)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    if (!input.getValue() && defaultValue != null) {
        input.setValue(defaultValue)
    }
    return configuratorEntry
}



