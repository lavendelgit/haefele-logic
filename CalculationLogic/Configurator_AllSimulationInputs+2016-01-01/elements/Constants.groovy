import groovy.transform.Field

@Field String DM_SIMULATION = 'PriceChangeSimulationDM_2020_Monthly'

@Field Map PAGE_DEF = [name     : 'ExceptionsConfigurator',
                       title    : 'Exceptions',
                       fieldList: [
                               []
                       ]]