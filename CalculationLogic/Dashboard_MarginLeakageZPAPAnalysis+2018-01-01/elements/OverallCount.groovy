if (!api.global.overallCount) {
    def yearOfAnalysis = out.EffectiveDate
    def fields = ['COUNTDISTINCT(customerId)': 'CustomerCount',
                  'COUNTDISTINCT(Material)'  : 'ArticleCount',
                  'COUNT(CustomerSalesPrice)': 'ZPAPCount'
    ]
    def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFilters ()
    def validityStartDate = yearOfAnalysis + "-01-01"
    def validityEndDate = yearOfAnalysis + "-12-31"
    def dbLib = libs.__LIBRARY__.DBQueries
    def generalLib = libs.__LIBRARY__.GeneralUtility
    filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
    libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', yearOfAnalysis)
    api.global.overallCount = dbLib.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart')
}
return api.global.overallCount