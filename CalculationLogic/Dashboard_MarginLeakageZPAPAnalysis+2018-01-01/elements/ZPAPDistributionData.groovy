def yearOfAnalysis = out.EffectiveDate

def fields = ['MatchType'                : 'MatchType',
              'COUNT(CustomerSalesPrice)': 'ZPAPCount',
              'MIN(DiscountPercentage)'  : 'MinDiscountPer',
              'MAX(DiscountPercentage)'  : 'MaxDiscountPer',
              'Avg(DiscountPercentage)'  : 'AvgDiscountPer'
]
def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFiltersForAllMatches()
def validityStartDate = yearOfAnalysis + "-01-01"
def validityEndDate = yearOfAnalysis + "-12-31"
def generalLib = libs.__LIBRARY__.GeneralUtility
filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
return libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart').collectEntries {
    [(it.MatchType): (it)]
}