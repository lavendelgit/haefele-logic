def getValue(def values, def valueKey) {
    def result = values.find { it.key2 == valueKey }
    return ((result) ? result?.attribute1 : 0)
}

if (!api.global.summaryData) {
    def filters = [Filter.equal("key1", 'ZPAPAnalysis'), Filter.equal("key3", out.EffectiveDate)]
    def values = api.findLookupTableValues("PreCalculatedDashboardField", *filters)
    def columns = ['ActiveCustomerCount', 'TransactedMaterialCount', 'CustomerTransacted', 'ZPAPCustomerCount',
                   'ZPAPArticleCount', 'ZPAPCount', 'TotalArticlesCount', 'ZPAPMinDiscountPer', 'ZPAPMaxDiscountPer',
                   'ZPAPAvgDiscountPer', 'ZPAPCountGTPerMaxDiscPer', 'MaterialCountGTPerMaxDiscPer', 'CustomerCountGTPerMaxDiscPer']
    boolean isDataAvailable = (values)
    api.global.summaryData = [
            'ActiveCustomerCount'    : ((isDataAvailable) ? getValue(values, columns[0]) : 0),
            'TransactedMaterialCount': ((isDataAvailable) ? getValue(values, columns[1]) : 0),
            'CustomerTransacted'     : ((isDataAvailable) ? getValue(values, columns[2]) : 0),
            'ZPAPCustomerCount'      : ((isDataAvailable) ? getValue(values, columns[3]) : 0),
            'ZPAPArticleCount'       : ((isDataAvailable) ? getValue(values, columns[4]) : 0),
            'ZPAPCount'              : ((isDataAvailable) ? getValue(values, columns[5]) : 0),
            'TotalArticlesCount'     : ((isDataAvailable) ? getValue(values, columns[6]) : 0),
            'ZPAPMinDiscountPer'     : ((isDataAvailable) ? getValue(values, columns[7]) : 0),
            'ZPAPMaxDiscountPer'     : ((isDataAvailable) ? getValue(values, columns[8]) : 0),
            'ZPAPAvgDiscountPer'     : ((isDataAvailable) ? getValue(values, columns[9]) : 0),
            'ZPAPCountGTPerMaxDiscPer'     : ((isDataAvailable) ? getValue(values, columns[10]) : 0),
            'MaterialCountGTPerMaxDiscPer'     : ((isDataAvailable) ? getValue(values, columns[11]) : 0),
            'CustomerCountGTPerMaxDiscPer'     : ((isDataAvailable) ? getValue(values, columns[12]) : 0),
    ]
}

return api.global.summaryData