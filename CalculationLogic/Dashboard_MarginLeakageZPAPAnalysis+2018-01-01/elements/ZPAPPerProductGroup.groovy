import net.pricefx.common.api.FieldFormatType

def columns = ['Prodh. II', 'Prodh. III', 'Prodh. IV', 'Customer Count', 'Material Count', 'ZPAP Count', 'Min Discount %', 'Max Discount %', 'Avg Discount %']
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('ZPAP Counts Per Product Group', columns, columnTypes)
def totalCustomerCount = 0, totalMaterialCount = 0, totalZPAPCount = 0

def zpapEntriesToDisplay = out.ZPAPEntriesPerPG
def minDiscountPer, maxDiscountPer, avgDiscountPer=0, count = 0
zpapEntriesToDisplay.each {
    minDiscountPer = (minDiscountPer && minDiscountPer < it['MinDiscountPer'])?minDiscountPer : it['MinDiscountPer']
    maxDiscountPer = (maxDiscountPer && maxDiscountPer > it['MaxDiscountPer'])?maxDiscountPer: it['MaxDiscountPer']
    avgDiscountPer += it['AvgDiscountPer']
    count++
}
avgDiscountPer = libs.__LIBRARY__.MathUtility.divide (avgDiscountPer, count)

resultMatrix.addRow(['All', 'All', 'All',
                     dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", api.global.summaryData['CustomerCountGTPerMaxDiscPer'])),
                     dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", api.global.summaryData['MaterialCountGTPerMaxDiscPer'])),
                     dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", api.global.summaryData['ZPAPCountGTPerMaxDiscPer'])),
                     minDiscountPer,
                     maxDiscountPer,
                     dashUtl.boldHighlightWith(resultMatrix, avgDiscountPer)
])

zpapEntriesToDisplay?.each {
    resultMatrix.addRow([
            (columns[0]): it['ProdhII'],
            (columns[1]): it['ProdhIII'],
            (columns[2]): it['ProdhIV'],
            (columns[3]): dashUtl.boldHighlightWith(resultMatrix, (it['CustomerCount'])),
            (columns[4]): dashUtl.boldHighlightWith(resultMatrix, it['ArticleCount']),
            (columns[5]): dashUtl.boldHighlightWith(resultMatrix, it['ZPAPCount']),
            (columns[6]): it['MinDiscountPer'],
            (columns[7]): it['MaxDiscountPer'],
            (columns[8]): dashUtl.boldHighlightWith(resultMatrix, it['AvgDiscountPer'])
    ])
}

resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("RefreshZPAPDetails")).withColValueAsEventDataAttr(columns[0], "ProdhII").withColValueAsEventDataAttr(columns[1], "ProdhIII").withColValueAsEventDataAttr(columns[2], "ProdhIV")

return resultMatrix