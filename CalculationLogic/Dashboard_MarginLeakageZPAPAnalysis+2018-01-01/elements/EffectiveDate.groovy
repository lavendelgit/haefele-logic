def years = libs.__LIBRARY__.GeneralUtility.getYearsListFromNow (libs.__LIBRARY__.GeneralUtility.getYearCountFrom2018())
libs.__LIBRARY__.TraceUtility.developmentTraceRow ("Printing inputs", years)
api.option("Timeframe", years)