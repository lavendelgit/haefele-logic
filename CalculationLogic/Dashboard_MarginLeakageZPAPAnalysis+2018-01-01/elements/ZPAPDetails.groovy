def effectiveDate = out.EffectiveDate
def verkaufsbuero = "All"
def branchCategory = "All"
def prodhII = "All"
def prodhIII = "All"
def prodhIV = "All"

return api.dashboard("LossPerZPAPViolatingMaxPermittedDiscountLimit")
        .setParam("Effective Date", effectiveDate)
        .setParam("Verkaufsbuero", verkaufsbuero)
        .setParam("Branche Category", branchCategory)
        .setParam("Prodh. II", prodhII)
        .setParam("Prodh. III", prodhIII)
        .setParam("Prodh. IV", prodhIV)
        .showEmbedded()
        .andRecalculateOn(api.dashboardWideEvent("RefreshZPAPDetails"))
        .withEventDataAttr("Verkaufsbuero").asParam("Verkaufsbuero")
        .withEventDataAttr("BrancheCategory").asParam("Branche Category")
        .withEventDataAttr("ProdhII").asParam("Prodh. II")
        .withEventDataAttr("ProdhIII").asParam("Prodh. III")
        .withEventDataAttr("ProdhIV").asParam("Prodh. IV")