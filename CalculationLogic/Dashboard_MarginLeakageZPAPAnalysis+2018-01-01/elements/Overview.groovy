import net.pricefx.common.api.FieldFormatType

def columns = ['Title', 'Count', 'Relative %']
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.INTEGER, FieldFormatType.PERCENT]
def dashUtl = libs.__LIBRARY__.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Overview', columns, columnTypes)
def mathLib = libs.__LIBRARY__.MathUtility
def overviewData = out.OverviewData
def activeZPAPCount = overviewData['ZPAPCount']
resultMatrix.addRow([(columns[0]): 'ZPAP Count',
                     (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", activeZPAPCount)),
                     (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(activeZPAPCount, activeZPAPCount))])

def activeCustomerCount = overviewData['ActiveCustomerCount']
resultMatrix.addRow([(columns[0]): 'Active Customers',
                     (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", activeCustomerCount)),
                     (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(activeCustomerCount, activeCustomerCount))])

def countCustomersWithZPAPEntry = overviewData['ZPAPCustomerCount']
resultMatrix.addRow([(columns[0]): 'Customers With ZPAP Entry',
                     (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", countCustomersWithZPAPEntry)),
                     (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(activeCustomerCount, countCustomersWithZPAPEntry))])

def countCustTransWithZPAP = overviewData['CustomerTransacted']
resultMatrix.addRow([(columns[0]): 'ZPAP Customers Who Transacted',
                     (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", countCustTransWithZPAP)),
                     (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(activeCustomerCount, countCustTransWithZPAP))])

def totalArticleCount = overviewData['TotalArticlesCount']
resultMatrix.addRow([(columns[0]): 'Active Materials',
                     (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", totalArticleCount)),
                     (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(totalArticleCount, totalArticleCount))])

def articlesWithZPAP = overviewData['ZPAPArticleCount']
resultMatrix.addRow([(columns[0]): 'Materials With ZPAP Entry',
                     (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", articlesWithZPAP)),
                     (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(totalArticleCount, articlesWithZPAP))])

def zpapMaterialTransacted = overviewData['TransactedMaterialCount']
resultMatrix.addRow([(columns[0]): 'ZPAP Transacted Materials',
                     (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", zpapMaterialTransacted)),
                     (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(totalArticleCount, zpapMaterialTransacted))])

return resultMatrix
