import net.pricefx.common.api.FieldFormatType

def columns = ['Verkaufsbuero', 'Branche Category', 'Customer Count', 'Material Count', 'ZPAP Count',
               'Min Discount %', 'Max Discount %', 'Avg Discount %']
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('ZPAP Counts Per Customer Group', columns, columnTypes)

def zpapEntriesToDisplay = out.ZPAPEntriesPerCG
def minDiscountPer, maxDiscountPer, avgDiscountPer=0, count = 0
zpapEntriesToDisplay.each {
    minDiscountPer = (minDiscountPer && minDiscountPer < it['MinDiscountPer'])?minDiscountPer : it['MinDiscountPer']
    maxDiscountPer = (maxDiscountPer && maxDiscountPer > it['MaxDiscountPer'])?maxDiscountPer: it['MaxDiscountPer']
    avgDiscountPer += it['AvgDiscountPer']
    count++
}
avgDiscountPer = libs.__LIBRARY__.MathUtility.divide (avgDiscountPer, count)
resultMatrix.addRow([
        'All', 'All',
        dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", api.global.summaryData['CustomerCountGTPerMaxDiscPer'])),
        dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", api.global.summaryData['MaterialCountGTPerMaxDiscPer'])),
        dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", api.global.summaryData['ZPAPCountGTPerMaxDiscPer'])),
        minDiscountPer,
        maxDiscountPer,
        dashUtl.boldHighlightWith(resultMatrix, avgDiscountPer)
])
zpapEntriesToDisplay?.each {
    resultMatrix.addRow([
            (columns[0]): it['Verkaufsbereich'],
            (columns[1]): it['BrancheCategory'],
            (columns[2]): dashUtl.boldHighlightWith(resultMatrix, it['CustomerCount']),
            (columns[3]): dashUtl.boldHighlightWith(resultMatrix, it['ArticleCount']),
            (columns[4]): dashUtl.boldHighlightWith(resultMatrix, it['ZPAPCount']),
            (columns[5]): it['MinDiscountPer'],
            (columns[6]): it['MaxDiscountPer'],
            (columns[7]): dashUtl.boldHighlightWith(resultMatrix, it['AvgDiscountPer'])
    ])
}
resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("RefreshZPAPDetails")).withColValueAsEventDataAttr(columns[0], "Verkaufsbuero").withColValueAsEventDataAttr(columns[1], "BrancheCategory")

return resultMatrix