def yearOfAnalysis = out.EffectiveDate

def fields = [
        'Verkaufsbereich3'         : 'Verkaufsbereich',
        'BrancheCategory'          : 'BrancheCategory',
        'COUNTDISTINCT(customerId)': 'CustomerCount',
        'COUNTDISTINCT(Material)'  : 'ArticleCount',
        'COUNT(CustomerSalesPrice)': 'ZPAPCount',
        'MIN(DiscountPercentage)'  : 'MinDiscountPer',
        'MAX(DiscountPercentage)'  : 'MaxDiscountPer',
        'Avg(DiscountPercentage)'  : 'AvgDiscountPer'
]
def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFilters()
def validityStartDate = yearOfAnalysis + "-01-01"
def validityEndDate = yearOfAnalysis + "-12-31"

def generalLib = libs.__LIBRARY__.GeneralUtility
filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', validityStartDate)
return libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart')