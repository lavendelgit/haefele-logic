import net.pricefx.common.api.FieldFormatType

def getTypeTitles(String matchType) {
    if (!matchType)
        return "Unknown"

    def maxDiscount = libs.__LIBRARY__.GeneralSettings.percentageValue("MaxDiscountPct") * 100
    switch (matchType) {
        case 'HPL': return "Within Max Permitted Discount($maxDiscount %)"
        case 'HLM': return "> Max Permitted Discount($maxDiscount %)"
        case 'ASP': return "More Than Sales Price"
        case 'NZE': return "Negative ZPAP Price"
    }
    return "Unknown"
}

def columns = ['Type', 'Count', 'Relative %', 'Min Discount %', 'Max Discount %', 'Avg Discount %']
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.INTEGER, FieldFormatType.PERCENT,
                   FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT]
def dashUtl = libs.__LIBRARY__.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('ZPAP Distribution', columns, columnTypes)

def zpapDistributionData = out.ZPAPDistributionData
def zpapTotal = 0, minDiscount, maxDiscount, averDiscount = 0, count = 0
zpapDistributionData.each { key, zpapEntry ->
    zpapTotal += zpapEntry.ZPAPCount
    minDiscount = (minDiscount && minDiscount < zpapEntry.MinDiscountPer) ? minDiscount : zpapEntry.MinDiscountPer
    maxDiscount = (maxDiscount && maxDiscount > zpapEntry.MaxDiscountPer) ? maxDiscount : zpapEntry.MaxDiscountPer
    averDiscount += zpapEntry.AvgDiscountPer
    count++
}
def mathLib = libs.__LIBRARY__.MathUtility
averDiscount = mathLib.divide(averDiscount as BigDecimal, (count - 1) as BigDecimal) //Excluding Unknown Type
resultMatrix.addRow([
        "Total",
        dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", zpapTotal)),
        dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(zpapTotal, zpapTotal)),
        minDiscount,
        maxDiscount,
        dashUtl.boldHighlightWith(resultMatrix, averDiscount)
])

def orderOfDisplayMatchType = ['HPL', 'HLM', 'ASP', 'NZE', 'Unknown']
def zpapEntry
orderOfDisplayMatchType.each { matchType ->
    zpapEntry = zpapDistributionData[matchType]
    resultMatrix.addRow([
            getTypeTitles(matchType),
            dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", (zpapEntry) ? zpapEntry?.ZPAPCount : 0)),
            dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(zpapTotal, (zpapEntry) ? zpapEntry?.ZPAPCount : 0)),
            zpapEntry?.MinDiscountPer,
            zpapEntry?.MaxDiscountPer,
            dashUtl.boldHighlightWith(resultMatrix, zpapEntry?.AvgDiscountPer)
    ])
}
return resultMatrix