def yearOfAnalysis = api.input("Effective Year")
def verkaufsbuero = api.input("Verkaufsbuero")
def brancheCategory = api.input("Branche Category")

def dataList = libs.HaefeleCommon.ZPAPDashboardDynamic.getZPAPAnalysisSummaryPerPG (yearOfAnalysis, verkaufsbuero, brancheCategory)
return libs.HaefeleCommon.ZPAPDashboardDynamic.getZPAPAnalysisSummaryForEmbeddedDashBoardPerPG(dataList, yearOfAnalysis, [verkaufsbuero, brancheCategory])