def effectiveYear = api.input("Effective Year")
def verkaufsbuero = api.input("Verkaufsbuero")
def brancheCategory = api.input("Branche Category")
def prodII = 'All'
def prodIII = 'All'
def prodIV = 'All'

def effeciveYearParam = "Effective Year"
def salesOfficeParam = "Verkaufsbuero"
def brancheCateParam = "Branche Category"
def prodhIIParam = "Prodh. II"
def prodhIIIParam = "Prodh. III"
def prodhIVParam = "Prodh. IV"

return api.dashboard("LossPerZPAPViolatingMaxPermittedDiscountLimit")
        .setParam(effeciveYearParam, effectiveYear)
        .setParam(salesOfficeParam, verkaufsbuero)
        .setParam(brancheCateParam, brancheCategory)
        .setParam(prodhIIParam, prodII)
        .setParam(prodhIIIParam, prodIII)
        .setParam(prodhIVParam, prodIV)
        .showEmbedded()
        .andRecalculateOn(api.dashboardWideEvent("RefreshZPAPDetails"))
        .withEventDataAttr("Verkaufsbuero").asParam(salesOfficeParam)
        .withEventDataAttr("BrancheCategory").asParam(brancheCateParam)
        .withEventDataAttr("ProdhII").asParam(prodhIIParam)
        .withEventDataAttr("ProdhIII").asParam(prodhIIIParam)
        .withEventDataAttr("ProdhIV").asParam(prodhIVParam)