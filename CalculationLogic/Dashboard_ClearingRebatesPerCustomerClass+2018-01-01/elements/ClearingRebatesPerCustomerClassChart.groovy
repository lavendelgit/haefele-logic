def clearingRebatesPerCustomerClass = api.getElement("ClearingRebatesPerCustomerClass")

def categories = clearingRebatesPerCustomerClass.collect { e -> e.key}
def data = clearingRebatesPerCustomerClass.collect { e -> e.value};

def chartDef = [
        chart: [
            type: 'column'
        ],
        title: [
            text: 'Clearing Rebates per Customer Class'
        ],
        xAxis: [[
            title: [
                    text: "Customer Class"
            ],
            categories: categories
        ]],
        yAxis: [[
            title: [
                text: "Number of Clearing Rebates"
            ]
        ]],
        tooltip: [
            headerFormat: 'Customer Class: {point.key}<br/>',
            pointFormat: 'Number of Clearing Rebates: {point.y}'
        ],
        legend: [
            enabled: false
        ],
        series: [[
             data: data,
             dataLabels: [
                 enabled: true,
             ]
        ]]
]

api.buildFlexChart(chartDef)