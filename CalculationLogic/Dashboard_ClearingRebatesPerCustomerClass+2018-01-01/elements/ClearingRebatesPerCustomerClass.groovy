def customerClasses = api.getElement("CustomersClasses")
def clearingRebatesPerCustomer = api.getElement("ClearingRebatesPerCustomer")

return customerClasses
        .groupBy { c -> c.attribute5 ?: "Undefined" }
        .collectEntries { customerClass, customers ->
            [customerClass, customers.collect { c -> clearingRebatesPerCustomer[c.customerId] ?: 0 }.sum()]
        }
        .findAll { e -> e.value > 0}
        .sort { e1, e2 -> e1.key <=> e2.key }