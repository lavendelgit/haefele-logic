def result = []

def stream = api.stream("C", null, ["customerId", "attribute5"]) // attribute5 = Customer ABC Class

if (!stream) {
    return []
}

while (stream.hasNext()) {
    result.add(stream.next())
}

stream.close()

return result