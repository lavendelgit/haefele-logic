def resultMatrix = api.newMatrix("Customer Class","Count of Clearing Rebates")
resultMatrix.setTitle("Count of Clearing Rebates per Customer Class")
resultMatrix.setColumnFormat("Count of Clearing Rebates", FieldFormatType.NUMERIC)

def clearingRebatesPerCustomerClass = api.getElement("ClearingRebatesPerCustomerClass")

def total = 0;

for (row in clearingRebatesPerCustomerClass) {
    resultMatrix.addRow([
            "Customer Class" : row.key,
            "Count of Clearing Rebates" : row.value
    ])
    total += row.value
}

resultMatrix.addRow([
        "Customer Class": resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of Clearing Rebates": resultMatrix.styledCell(total).withCSS("font-weight: bold")
])

return resultMatrix
