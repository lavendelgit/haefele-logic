import net.pricefx.common.api.FieldFormatType

def getNotNullRevenue (BigDecimal tRevenue, BigDecimal sRevenue) {
    return (tRevenue)?:((sRevenue?:0.0))
}

def verkaufsbuero = api.input("Verkaufsbuero")
def brancheCategory = api.input("Branche Category")

int currentYear = Integer.parseInt(new java.util.Date().format("yyyy"))
def YearForAnalysis=api.input("YearForAnalysis")?:((api.input("YearForAnalysis")?:currentYear))
int yearIndex = currentYear-Integer.parseInt(YearForAnalysis+"")

def fields = ['CustomerId'                  : 'CustomerId',
              'CustomerName'                : 'CustomerName',
              'p1_THYear1Umsatz'            : 'Year1Revenue',
              'p2_THY1PerUmsatzContribution': 'Year1RevContrib',
              'p3_THYear2Umsatz'            : 'Year2Revenue',
              'p4_THY2PerUmsatzContribution': 'Year2RevContrib',
              'p5_THYear3Umsatz'            : 'Year3Revenue',
              'p6_THYear1DB'                : 'Year1DB',
              'p7_THYear2DB'                : 'Year2DB',
              'p8_THYear3DB'                : 'Year3DB',
              'p12_THYear4Umsatz'           : 'Year4Revenue',
              'p13_THYear5Umsatzh'          : 'Year5Revenue',
              'p14_SHYear1Umsatz'           : 'SYear1Revenue',
              'p15_SHYear2Umsatz'           : 'SYear2Revenue',
              'p16_SHYear3Umsatz'           : 'SYear3Revenue',
              'p17_SHYear4Umsatz'           : 'SYear4Revenue',
              'p18_SHYear5Umsatz'           : 'SYear5Revenue',
              'p9_THCAGR'                   : 'CAGR',
              'p10_THAgeOfCustomer'         : 'Age of Customer',
              'Verkaufsbereich3'            : 'Verkaufsbereich3',
              'BrancheCategory'             : 'BrancheCategory',
              'CustomerPotentialGroup'      : 'CustomerPotentialGroup',
              'ComputedPotentialGroup'      : 'ComputedPotentialGroup'
]
api.trace("YearForAnalysis",out.YearForAnalysis)
def filters = []
if (verkaufsbuero && !(verkaufsbuero.equals("All")))
    filters.add(Filter.equal('Verkaufsbereich3', verkaufsbuero))
if (brancheCategory && !(brancheCategory.equals("All")))
    filters.add(Filter.equal('BrancheCategory', brancheCategory))
if (verkaufsbuero && !brancheCategory)
    filters.add(Filter.isNull('BrancheCategory'))
def customersToDisplay = lib.DBQueries.getDataFromSource(fields, 'DB_CustomersWithCorrections', filters, 'Rollup')

def yearList = lib.GeneralUtility.getYearsListFromNow(5)
def columns = ['Customer Name', 'Verkaufsbuero', 'Branche Category', 'Umsatz ' +YearForAnalysis, 'CAGR', 'Customer Contribution to Hafelle Umsatz(%)',
               'Current Potential Group', 'Recommended Potential Group']
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.MONEY_EUR, FieldFormatType.PERCENT, FieldFormatType.PERCENT,
                   FieldFormatType.TEXT, FieldFormatType.TEXT]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('All Customers Recommended For Correction Of Potential Categorization',
        columns, columnTypes)
resultMatrix.setPreferenceName("MLRecToAWCPG_AllCustPotGroup")
customersToDisplay?.each {
    resultMatrix.addRow([
            (columns[0]): dashUtl.boldHighlightWith(resultMatrix, (it['CustomerName'] + '-' + it['CustomerId'])),
            (columns[1]): it['Verkaufsbereich3'],
            (columns[2]): it['BrancheCategory'],
            (columns[3]): getNotNullRevenue(it["Year"+(yearIndex+1)+"Revenue"], it["SYear"+(yearIndex+1)+"Revenue"]),
            (columns[4]): dashUtl.boldHighlightWith(resultMatrix, it['CAGR']),
            (columns[5]): it['Year2RevContrib'],
            (columns[6]): dashUtl.boldHighlightWith(resultMatrix, it['CustomerPotentialGroup']),
            (columns[7]): dashUtl.boldHighlightWith(resultMatrix, it['ComputedPotentialGroup'])
    ])
}

return resultMatrix

