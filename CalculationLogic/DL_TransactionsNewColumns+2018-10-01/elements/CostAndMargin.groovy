if (api.isSyntaxCheck()) return [:]

def invoiceId = api.getElement("InvoiceId")
def invoicePos = api.getElement("InvoicePos")

def costAndMargin = api.global.costAndMargin[invoiceId]

if (!costAndMargin) {
    def dmCtx = api.getDatamartContext()
    def ds = dmCtx.getDataSource("MarginAndCost")
    def dsQuery = dmCtx.newQuery(ds, false)

    def filters = [
        Filter.equal("InvoiceId", invoiceId),
    ]

    dsQuery.select("InvoicePos", "invoicePos")
    dsQuery.select("BaseCost", "baseCost")
    dsQuery.select("MarginAbs", "marginAbs")
    dsQuery.where(filters)

    costAndMargin = dmCtx.executeQuery(dsQuery)?.getData()
                                                          ?.collectEntries { r ->
                                                            [r.invoicePos, [baseCost: r.baseCost, marginAbs: r.marginAbs]]
                                                          } ?: [:]

    api.global.costAndMargin[invoiceId] = costAndMargin
}

return costAndMargin[invoicePos]
