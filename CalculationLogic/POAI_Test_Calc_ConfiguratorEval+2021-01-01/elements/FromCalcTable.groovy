def chart = api.newChartBuilder().newBarLine()

chart.getOptions()
        .setTitle("A title")
        .setXLabel("X axis label")
        .setYLabel("Y axis label")

chart.addSeries()
        .setLabel("Current")
        .setDatamart(model.table("my_table").getSourceName())
        .setAxisX("a_text_field")
        .setAxisY("a_numeric_field")

return chart.build()
