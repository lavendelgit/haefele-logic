def controller = api.newController()

controller.addHTML("""
<h1>Result from this step's calculation</h1>
<ul>
<li>expected: 'calc1 resulted with: a result of the calc'</li>
<li>got: '${model.outputs('sequence', 'python').ResultFromPrevCalc}'</li>
</ul>
""")

return controller
