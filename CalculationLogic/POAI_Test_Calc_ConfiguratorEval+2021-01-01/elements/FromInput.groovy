def controller = api.newController()

controller.addHTML("""
<h1>Input from this tab</h1>
<ul>
<li>string entry: '${api.input("InlineConfigurator")["StringEntry"]}'</li>
<li>option entry: '${api.input("InlineConfigurator")["OptionEntry"]}'</li> 
</ul>
""")

return controller
