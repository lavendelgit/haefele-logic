def tab1 = model.inputs('twotab', 'tab1')
def stringEntry = tab1.StringEntry
def optionEntry = tab1.OptionEntry

if (optionEntry == null) {
    api.throwException(
            "Cannot read state from input set in step 'Calc, Evals' and 'Tab 1' tab. Please fill the required option entry there and save it."
    )
}
def controller = api.newController()

controller.addHTML("""
<h1>Input from 'Calc, Evals' step, 'Tab 1' tab</h1>
<ul>
<li>string entry: '${stringEntry}'</li>
<li>option entry: '${optionEntry}'</li>
</ul>
""")

return controller
