import net.pricefx.common.api.FieldFormatType


//resultMatrix.addRow(['Total Active Customers', notApplicable, notApplicable, totalCustomers, api.formatNumber("€###,###,###", totalRevenue), 1.00, totalMargin, 1.00])
String configurationInputName = out.ConfigurationName
if (api.isDebugMode()) {
    configurationInputName = "Retain nearly same margin as 2020"
}

List columns = ['Month', 'Gross Price % Change', 'Purchase Price % Change', 'Quantity % Change']
List columnTypes = [FieldFormatType.TEXT, FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT]
def dashUtl = lib.DashboardUtility
String title = configurationInputName ? "'$configurationInputName' Details (${out.ConfigurationType})" : 'Configuration Details'
def resultMatrix = dashUtl.newResultMatrix(title, columns, columnTypes)
List MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
List currentRow
Map rowToInsert
if (!configurationInputName) {
    return resultMatrix
}

List simulationInputs = Library.getPriceChangePercentageConfiguration(configurationInputName)
Map monthNameConfigurations = simulationInputs.collectEntries { Map entries ->
    [(entries.key3): [entries.key3,
                      entries.attribute1 ?: BigDecimal.ZERO,
                      entries.attribute2 ?: BigDecimal.ZERO,
                      entries.attribute3 ?: BigDecimal.ZERO]]
}

MONTH_NAMES.each { String monthName ->
    currentRow = monthNameConfigurations[monthName]
    rowToInsert = [(columns[0]): currentRow[0],
                   (columns[1]): api.formatNumber("##.##%", currentRow[1]),
                   (columns[2]): api.formatNumber("##.##%", currentRow[2]),
                   (columns[3]): api.formatNumber("##.##%", currentRow[3])]
    resultMatrix.addRow(rowToInsert)
}
resultMatrix.setPreferenceName("MonthlyConfiguration")

return resultMatrix
