String configurationInputType = out.ConfigurationType
String configurationInputName = out.ConfigurationName

api.logInfo("********* configurationInputType", configurationInputType)
api.logInfo("********* configurationInputName", configurationInputName)

List simulationInputs = Library.getPriceChangePercentageConfiguration(configurationInputName)

//List simulationInputs = exceptionMonthlyInputs?.findAll { it -> it.key1 == configurationInputType }
Map simulationInputMap = Library.createConfigurationInputMap(simulationInputs)

def htmlPortlet = api.newController()
Map summaryData = api.local.SummaryData ?: [:]
StringBuilder htmlBuilder = new StringBuilder()
htmlBuilder.append("<div>")
htmlBuilder.append(Library.generateSection(configurationInputType, configurationInputName, simulationInputMap, 100))
htmlBuilder.append("</div>")

htmlPortlet.addHTML(htmlBuilder.toString())
return htmlPortlet