import groovy.transform.Field

@Field List PRICE_SIMULATION_FIELDS = ['key3', 'attribute1', 'attribute2', 'attribute3']

@Field Map INPUTS = [SIMULATION_INPUT: "MonthlyPricingSimulationInput",
                     EXCEPTION_INPUT : "MonthlyPricingSimulationInput"]

@Field final Map MONTHS_LIST = ["January":"01", "February":"02", "March":"03", "April":"04", "May":"05", "June":"06", "July":"07",
                                "August":"08", "September":"09", "October":"10", "November":"11", "December":"12"]

@Field final Map SUMMARY_COLORS = [BG_COLOR    : '#F8F8F8',
                                   BORDER_COLOR: '#fff',
                                   TITLE       : '#000',
                                   LABEL       : '#aaa',
                                   VALUE       : '#69bdd2',
                                   REC_BG_COLOR: '#DCF6D1',]

@Field Map<String, Integer> DECIMAL_PLACE = [QUANTITY: 0,
                                             MONEY   : 3,
                                             PERCENT : 2,
                                             OTHERS  : 2]

@Field List RESULT_MATRIX_COLUMN_NAMES = ["Configuration Type",'Configuration Name']
@Field List RESULT_MATRIX_COLUMN_TYPES = [FieldFormatType.TEXT, FieldFormatType.TEXT]