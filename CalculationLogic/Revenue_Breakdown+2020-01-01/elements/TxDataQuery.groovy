Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION
def customerElement = api.getElement("Customers")
def productElement = api.getElement("Products")
def period = api.getElement("Period")
def comparisonPeriod = api.getElement("ComparisonPeriod")
String targetCurrencyCode = api.getElement("TargetCurrencyData").currencyCode
Filter genericFilter = api.getElement("GenericFilter")

libs.SIP_Dashboards_Commons.CausalityUtils.getRevenueBreakdownData(productElement, customerElement, period, comparisonPeriod, targetCurrencyCode, sqlConfiguration, genericFilter)