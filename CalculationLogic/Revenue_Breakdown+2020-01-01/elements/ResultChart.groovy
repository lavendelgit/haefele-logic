def hLib = libs.HighchartsLibrary
Boolean displayType = api.getElement("ShowAsPercentage")
List seriesData = api.getElement("ChartData")
Map<String, String> chartConfiguration = Configuration.CHART_CONFIGURATION
boolean isPercentageView = api.getElement("ShowAsPercentage")
String percentageLabel = Configuration.PERCENTAGE_LABEL
String currencyLabel = api.getElement("TargetCurrencyData").currencyCode
String yAxisTitle = isPercentageView ? percentageLabel : currencyLabel
Map yAxis = hLib.Axis.newAxis().setTitle(yAxisTitle)
Map xAxis = hLib.Axis.newAxis().setType(hLib.ConstConfig.AXIS_TYPES.CATEGORY)

String tooltipFormat = getTooltipFormat(displayType)
Map tooltip = hLib.Tooltip.newTooltip().setPointFormat(tooltipFormat)
Map legend = hLib.Legend.newLegend().setEnabled(false)
Map waterfallPlotOptions = [waterfall: [upColor    : chartConfiguration.positive,
                                        color      : chartConfiguration.base,
                                        borderWidth: 0,
                                        dataLabels : [borderWidth: 0,
                                                      enabled    : true,
                                                      inside     : false,
                                                      format     : getLabelFormat(displayType)]]]
Map chart = hLib.Chart.newChart()
        .setTitle(chartConfiguration.chartName)
        .setPlotOptions(waterfallPlotOptions)
        .setTooltip(tooltip)
        .setLegend(legend)

if (seriesData) {
    Map series = hLib.WaterfallSeries.newWaterfallSeries()
            .setData(seriesData)
            .setXAxis(xAxis)
            .setYAxis(yAxis)
    chart.setSeries(series)
}

return api.buildHighchart(chart.getHighchartFormatDefinition())

protected String getTooltipFormat(Boolean shouldShowAsPercentage) {
    return "<b>${getValueFormat(shouldShowAsPercentage)}<b>"
}

protected String getLabelFormat(Boolean shouldShowAsPercentage) {
    return getValueFormat(shouldShowAsPercentage)
}

protected String getValueFormat(Boolean shouldShowAsPercentage) {
    Map formats = libs.HighchartsLibrary.ConstConfig.TOOLTIP_POINT_Y_FORMAT_TYPES
    String currencySymbol = api.getElement("TargetCurrencyData").currencySymbol
    String priceWithCurrencySymbol = libs.SIP_Dashboards_Commons.CurrencyUtils.getFormatWithCurrencySymbol(formats.ABSOLUTE_PRICE,
            currencySymbol)

    return shouldShowAsPercentage ? formats.PERCENTAGE : priceWithCurrencySymbol
}
