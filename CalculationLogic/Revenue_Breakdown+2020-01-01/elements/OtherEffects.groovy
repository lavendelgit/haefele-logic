BigDecimal t1_Revenue = api.getElement("T1_Revenue")
BigDecimal t2_Revenue = api.getElement("T2_Revenue")
BigDecimal lostBusinessEffect = api.getElement("LostBusinessEffect")
BigDecimal priceEffect = api.getElement("PriceEffect")
BigDecimal volumeEffect = api.getElement("VolumeEffect")
BigDecimal mixEffect = api.getElement("MixEffect")
BigDecimal newBusinessEffect = api.getElement("NewBusinessEffect")

return t2_Revenue - (t1_Revenue + lostBusinessEffect + priceEffect + volumeEffect + mixEffect + newBusinessEffect)