Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION
Map<String, String> entryConfiguration = Configuration.ENTRY_CONFIGURATION
String dateColumnName = sqlConfiguration.pricingDate
String yearColumnName = dateColumnName + Configuration.YEAR_FIELD_SUFFIX
def datamartContext = api.getDatamartContext()
String datamartName = sqlConfiguration.datamartName
Map queryDefinition = [datamartName: datamartName,
                       rollup      : true,
                       fields      : [year: "MAX(${dateColumnName})"]]

Map dimData = libs.HighchartsLibrary.QueryModule.getDimFilterEntry(datamartContext, yearColumnName, queryDefinition)
def yearDimFilterEntry = datamartContext.dimFilterEntry(entryConfiguration.yearName, dimData.columnData)
String defaultYear = getDefaultYear(dimData)

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(entryConfiguration.yearName, defaultYear)

return yearDimFilterEntry?.value ?: defaultYear

String getDefaultYear(Map dimData) {
    return dimData?.defaultValue?.year.getAt(0).format(Configuration.YEAR_FORMAT) ?: libs.SharedLib.DateUtils.currentYear()
}