BigDecimal t1Revenue = api.getElement("T1_Revenue")
BigDecimal t2Revenue = api.getElement("T2_Revenue")

if (!t1Revenue && !t2Revenue) {
    return []
}

Map<String, BigDecimal> fieldMap = prepareChartDataFieldMap(t1Revenue)

return prepareChartData(fieldMap, t1Revenue)

Map prepareChartDataFieldMap(BigDecimal t1Revenue) {
    BigDecimal lostBusinessEffect = api.getElement("LostBusinessEffect")
    BigDecimal priceEffect = api.getElement("PriceEffect")
    BigDecimal volumeEffect = api.getElement("VolumeEffect")
    BigDecimal mixEffect = api.getElement("MixEffect")
    BigDecimal otherEffects = api.getElement("OtherEffects")
    BigDecimal newBusinessEffect = api.getElement("NewBusinessEffect")

    def calendar = api.getElement("Calendar")
    def comparisonPeriodName = calendar.getTimePeriodName(api.getElement("ComparisonPeriod"))
    String revenueKey = "Revenue in ${comparisonPeriodName}"

    return [(revenueKey)          : t1Revenue,
            "Lost Business"       : lostBusinessEffect,
            "Price Effect"        : priceEffect,
            "Volume Effect"       : volumeEffect,
            "Portfolio Mix Effect": mixEffect,
            "Other Effects"       : otherEffects,
            "New Business"        : newBusinessEffect]
}

def prepareChartData(LinkedHashMap<String, BigDecimal> fieldMap, BigDecimal t1Revenue) {
    def calendar = api.getElement("Calendar")
    def periodName = calendar.getTimePeriodName(api.getElement("Period"))
    boolean showAsPercentage = api.getElement("ShowAsPercentage")

    List chartData = fieldMap.collect { key, value ->
        return prepareChartDataField(showAsPercentage, value, t1Revenue, key)
    }
    chartData << prepareSumPricePointStructure("Revenue in ${periodName}")

    return chartData
}

def prepareChartDataField(Boolean showAsPercentage, BigDecimal value, BigDecimal t1Revenue, String key) {
    if (showAsPercentage) {
        value = toPercentage(value, t1Revenue)
    }

    return prepareAdjustmentStructure(key, value)
}

protected BigDecimal toPercentage(BigDecimal value, BigDecimal base) {
    return base ? 100 * (value ?: 0) / base : 0.0
}

protected Map prepareAdjustmentStructure(String name, BigDecimal value) {
    return ["name": name, "y": value]
}

protected Map prepareSumPricePointStructure(String name) {
    return ["name": name, "isSum": true]
}
