Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION
Map<String, String> entryConfiguration = Configuration.ENTRY_CONFIGURATION

String dateColumnName = sqlConfiguration.pricingDate
String yearColumnName = dateColumnName + Configuration.YEAR_FIELD_SUFFIX
def cal = api.calendar()

cal.add(java.util.Calendar.MONTH, Configuration.MONTHS_BACK_FROM)
def fromDate = cal.getTime().format(Configuration.DATE_FORMAT)

cal.add(java.util.Calendar.MONTH, Configuration.MONTHS_BACK_TO)
def toDate = cal.getTime().format(Configuration.DATE_FORMAT)

def datamartContext = api.getDatamartContext()
String datamartName = sqlConfiguration.datamartName

Map queryDefinition = [datamartName: datamartName,
                       rollup      : true,
                       fields      : [year: "MIN(${dateColumnName})"],
                       whereFilters: [Filter.greaterOrEqual(dateColumnName, toDate),
                                      Filter.lessOrEqual(dateColumnName, fromDate)]]

Map dimData = libs.HighchartsLibrary.QueryModule.getDimFilterEntry(datamartContext, yearColumnName, queryDefinition)
def yearDimFilterEntry = datamartContext.dimFilterEntry(entryConfiguration.comparisonYearName, dimData.columnData)
String defaultYear = dimData?.defaultValue?.year.getAt(0).format(Configuration.YEAR_FORMAT) ?: libs.SharedLib.DateUtils.currentYear() - 1

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(entryConfiguration.comparisonYearName, defaultYear)

return yearDimFilterEntry?.value ?: defaultYear