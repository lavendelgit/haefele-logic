def getKey(String material, String customerId) {
    if (!material && customerId)
        return "Null#$customerId"

    if (material && !customerId)
        return "$material#Null"

    if (!material && !customerId)
        return "Null#Null"

    int i=0
    while (customerId.substring(i,i+1) == '0') i++
    customerId = (i < customerId.length())?customerId.substring(i):"EmptyNull"

    return "$material#$customerId"
}

if (!api.global.customersWithZPAPEntry) {
    def field = ['ZPAPPreis' : 'ZPAPPreis',
                 'sku'       : 'sku',
                 'CustomerID': 'CustomerID',
                 'Gltigvon'  : 'Gltigvon'
    ]
    def currentDate = api.targetDate()
    def filters = [
            Filter.greaterThan('Gltigbis', currentDate.format('yyyy-MM-dd'))
    ]
    lib.TraceUtility.developmentTrace("Filters.....", filters)

    def data = lib.DBQueries.getDataFromSource(field, 'CustomerSalesPrice_2', filters, 'DataSource')
    def custSkuMapData = [:]
    def currentKey
    def currentEntry
    data.each { it ->
        currentKey = getKey(it.sku, it.CustomerID)
        currentEntry = (custSkuMapData [currentKey]) ?: []
        currentEntry.add([it.ZPAPPreis, it.sku, it.CustomerID, it.Gltigvon.format ("yyyy-MM-dd")])
        custSkuMapData [currentKey] = currentEntry
    }
    api.global.customersWithZPAPEntry = custSkuMapData
    lib.TraceUtility.developmentTrace("Printing the values fetched", api.global.customersWithZPAPEntry)
}

def currentKey = "${out.MATNR}#${out.CustomerId}"
return api.global.customersWithZPAPEntry [currentKey]