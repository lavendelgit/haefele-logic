def matchType = "Not Found"
def customerRecords = out.CustomersWithZPAP
if (customerRecords) {
    boolean isPriceMatch, isDateMatch
    def lastDate, lastPrice
    def dateToMatch = (out.DATAB instanceof Date) ? out.DATAB.format("yyyy-MM-dd") : out.DATAB
    customerRecords.each { it ->
        if (matchType != "Matched") {
            lastPrice = it[0]
            lastDate = it[3]
            isPriceMatch = (lastPrice == out.KBETR)
            isDateMatch = (lastDate == dateToMatch)
            if (isPriceMatch && isDateMatch) {
                matchType = "Matched"
            }
        }
    }
    if (matchType != "Matched") {
        matchType = "MisMatch (${isPriceMatch}, ${isDateMatch}, ${lastPrice}, ${lastDate})"
    }
}
return matchType