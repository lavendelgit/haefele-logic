/**
 * Helper method to get a map of keys and labels for a option input. It works for SIMPLE / MLTV1 type of PP table.
 * @param customerId
 * @return
 */
Map getKeyLabelMap(String ppTableName, String keyField, String valueField, List filters = null) {
    def data = filters ? api.findLookupTableValues(ppTableName, *filters) : api.findLookupTableValues(ppTableName)
    if (data) {
        return data?.collectEntries { [(it.get(keyField)): it.get(valueField)] }
    }
}

/**
 * Helper method to get a map of keys and labels for a option input. It works for SIMPLE / MLTV1 type of PP table.
 * @param customerId
 * @return
 */
Map getKeyLabelMap(String ppTableName, List filters = null) {
    def data = filters ? api.findLookupTableValues(ppTableName, *filters) : api.findLookupTableValues(ppTableName)
    if (data) {
        def column = data[0]?.type == 'SIMPLE' ? 'value' : 'attribute1' // what column values is needed
        return data?.collectEntries { [(it.name): it.get(column)] }
    }
}

/**
 * Helper method to get a list of keys for a option input. It works for SIMPLE / MLTV1 type of PP table.
 * @param customerId
 * @return
 */
List getKeyList(String ppTableName, String keyField, List filters = null) {
    def data = filters ? api.findLookupTableValues(ppTableName, *filters) : api.findLookupTableValues(ppTableName)
    return data?.collect { it.get(keyField) }?.unique()
}

/**
 * Helper method to get a list of keys for a option input. It works for SIMPLE / MLTV1 type of PP table.
 * @param customerId
 * @return
 */
List getKeyList(String ppTableName, List filters = null) {
    def data = filters ? api.findLookupTableValues(ppTableName, *filters) : api.findLookupTableValues(ppTableName)
    return data?.collect { it.name }
}

/**
 * Utility method to read all rows
 * @param finder
 * @return
 * Example engine usage:
 *
 * List products = readAllRows() { int startRow, int maxRow ->
 *             api.find("P", startRow, maxRow, 'sku', ['sku'], true, articleFilter)
 *}
 */
List readAllRows(Closure finder) {
    int startRow = 0
    List allRows = []
    int maxRow = api.getMaxFindResultsLimit()
    while (result = finder.call(startRow, maxRow)) {
        startRow += result.size()
        allRows.addAll(result)
    }
    return allRows
}

List getArticlesForFilter(articleFilter) {
    if (articleFilter) {
        List products = readAllRows() { int startRow, int maxRow ->
            api.find("P", startRow, maxRow, 'sku', ['sku'], true, articleFilter)
        }
        return products ? products.collect { product -> product.sku } : []
    }
    return []
}

List getAnalysisYears() {
    //TODO : Read from DM
    return ['2016', '2017', '2018', '2019', '2020']
}

List getTargetYears() {
    def currentYear = api.calendar().get(Calendar.YEAR)
    List targetYears = []
    for (int i = 0; i < 3; i++) {
        targetYears << currentYear + i
    }
    return targetYears
}

List getConditionTypes() {
    List filters = [Filter.equal('attribute3', 'Yes')]
    def data = api.findLookupTableValues(Constants.TABLE_CONDITION_MASTER, *filters)
    return data?.collect { row ->
        List conditionNameTableList = []
        String conditionName = row.name?.trim()
        String conditionTables = row.attribute2
        if (conditionTables) {
            conditionNameTableList = conditionTables.split(',').collect { String conditionTableName ->
                conditionName + '-' + conditionTableName?.trim()
            }
        }
        return conditionNameTableList
    }?.flatten()?.unique()
}

protected String toPercent(BigDecimal numberValue) {
    return (numberValue != null) ? libs.SharedLib.RoundingUtils.round(numberValue, Constants.DECIMAL_PLACE.PERCENT) + ' %' : ''
}

protected String formatMoney(BigDecimal moneyValue) {
    def roundingUtils = libs.SharedLib.RoundingUtils
    return moneyValue ? api.formatNumber("###,###,###.##", roundingUtils.round(moneyValue, 0)) : ""
}

