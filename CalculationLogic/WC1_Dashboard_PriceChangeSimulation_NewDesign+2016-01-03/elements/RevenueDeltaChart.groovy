Map summaryData = api.local.SummaryData
if (!summaryData) {
    return
}

List seriesData = ChartUtils.getWaterfallSeriesData('Revenue', out.Year, summaryData.Revenue)

return ChartUtils.generateWaterfallChart(Constants.WATERFALL_CONFIGURATION.REVENUE.TITLE,
                                         Constants.WATERFALL_CONFIGURATION.REVENUE.Y_AXIS_LABEL,
                                         seriesData,
                                         true,
                                         'Revenue')