if (!api.local.SummaryData) {
    return
}
return

def htmlPortlet = api.newController()
Map summaryData = api.local.SummaryData ?: [:]
StringBuilder htmlBuilder = new StringBuilder()
htmlBuilder.append(getHeaderString())
htmlBuilder.append("<div>")
htmlBuilder.append(generateSection('ACTUAL', 'SALES DATA',
                                   ['Year'          : out.Year ?: '',
                                    'Total Revenue' : summaryData.Revenue.actualInMillion,
                                    'Rows Processed': summaryData.totalTransactions,
                                    'Total Quantity': libs.SharedLib.RoundingUtils.round(summaryData.totalQuantity, 0),
                                    'Material Count': summaryData.totalMaterials
                                   ], 11))

htmlBuilder.append(generateSection('SIMULATION', 'USER INPUT',
                                   ['Price Change %'     : out.PriceChangePercent != null ? out.PriceChangePercent : 'n/a',
                                    'Price Change On'    : out.PriceChangeOn ? out.PriceChangeName : 'n/a',
                                    '# Expected Margin %': out.ExpectedMarginPercent != null ? out.ExpectedMarginPercent : 'n/a'], 11))

addSeriesSection(htmlBuilder, 'REVENUE', 'Revenue', summaryData.Revenue, 22)
addSeriesSection(htmlBuilder, 'COST', 'Cost', summaryData.Cost, 22)
addMagrinSeriesSection(htmlBuilder, summaryData, 24)

htmlBuilder.append("</div>")
htmlPortlet.addHTML(htmlBuilder.toString())
return htmlPortlet

protected String getHeaderString() {
    return '<h2>SIMULATION SUMMARY</h2>'
}

protected void addMagrinSeriesSection(StringBuilder htmlBuilder, Map summaryData, int width) {
    appendMarginPercent(summaryData, 'currentInMillion', 'current', summaryData.Revenue.current)
    appendMarginPercent(summaryData, 'expectedInMillion', 'expected', summaryData.Revenue.expected)
    if (out.CalculateRecommended) {
        appendMarginPercent(summaryData, 'recommendedInMillion', 'recommended', summaryData.Revenue.recommended)
    }
    addSeriesSection(htmlBuilder, 'CUSTOMER MARGIN', 'Customer Margin', summaryData.PocketMargin, width)
}


protected void appendMarginPercent(Map summaryData, String fieldToUpdate, String demonFieldName, BigDecimal baseValue) {
    if (baseValue) {
        summaryData.PocketMargin[fieldToUpdate] = summaryData.PocketMargin[fieldToUpdate] + " ( " + libs.SharedLib.RoundingUtils.round(((summaryData.PocketMargin[demonFieldName] / baseValue) * 100), Constants.DECIMAL_PLACE.PERCENT) + "% )"
    }
}

protected void addSeriesSection(StringBuilder htmlBuilder, String seriesName, String labelPrefix, Map seriesSummaryData, int width) {

    Map contents = ["${labelPrefix} Total  ( current )"            : seriesSummaryData.currentInMillion,
                    "${labelPrefix} Total  ( expected )"           : seriesSummaryData.expectedInMillion,
                    "${labelPrefix} Delta ( expected - current )"  : seriesSummaryData.expectedDeltaInMillion,
                    "${labelPrefix} Delta ( expected - current ) %": Library.toPercent(seriesSummaryData.expectedDeltaPercent)]
    if (out.CalculateRecommended) {
        contents["# ${labelPrefix} Total  ( recommend )"] = seriesSummaryData.recommendedInMillion
        contents["# ${labelPrefix} Delta  ( recommend - expected )"] = seriesSummaryData.recommendedDeltaInMillion
        contents["# ${labelPrefix} Delta  ( recommend - expected ) %"] = Library.toPercent(seriesSummaryData.recommendedDeltaPercent)
    }

    htmlBuilder.append(generateSection('OUTPUT ANALYSIS', seriesName, contents, width))
}

protected String generateSection(String title, String subtitle, Map contents, int width) {
    StringBuilder htmlBuilder = new StringBuilder()

    Map COLORS = Constants.SUMMARY_COLORS
    htmlBuilder.append("<div style='float: left;width: ${width}%;padding: 5px;height: 100%;margin: 3px;background-color:${COLORS.BG_COLOR};'>")
    htmlBuilder.append("<h5 style='color:${COLORS.TITLE}'>${title}</h5>")
    htmlBuilder.append("<h3 style='color:${COLORS.TITLE}'>${subtitle}</h3>")
    contents.each { String contentTitle, contentValue ->
        String bgColor = contentTitle?.startsWith("#") ? COLORS.REC_BG_COLOR : COLORS.BG_COLOR
        contentTitle = contentTitle?.startsWith("#") ? contentTitle.substring(2) : contentTitle
        htmlBuilder.append("<div style='padding:5px;border-style:solid;border-width:1px;border-color:${COLORS.BORDER_COLOR};background-color:${bgColor};'>")
        htmlBuilder.append("<div style='padding:2px;overflow: auto;white-space: nowrap;color:${COLORS.LABEL};background-color:${bgColor};'><b>${contentTitle}</b></div>")
        htmlBuilder.append("<div style='padding:2px;overflow: auto;white-space: nowrap;font-weight:bold;color:${COLORS.VALUE};font-size:16px;background-color:${bgColor};'>${contentValue ?: '-'}</div>")
        htmlBuilder.append("</div>")
    }
    htmlBuilder.append("</div>")
    return htmlBuilder.toString()
}