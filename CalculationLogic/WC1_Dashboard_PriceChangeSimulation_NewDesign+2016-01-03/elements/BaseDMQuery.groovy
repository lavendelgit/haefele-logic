def ctx = api.getDatamartContext()
def dm = ctx.getDatamart(Constants.DM_SIMULATION)

def dmQuery = ctx.newQuery(dm)
List dmColumnsDef = out.DMProjections
dmColumnsDef.each { Map columnDef ->
    dmQuery.select(columnDef.dmColumn, columnDef.alias)
}
//IThinkThisWasAddedTemporarilyForSelectedView Remove it post testing.
//dmQuery.where(Filter.in('PricingType', ['Markup', 'Net Price', 'Absolute Discount', 'Per Discount', 'XARTICLE', 'INTERNAL', 'N/A']))
if (out.Filters) {
    dmQuery.where(*out.Filters)
}

return dmQuery