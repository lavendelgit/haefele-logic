BigDecimal changePercent = out.PriceChangePercent
return (changePercent != null ? changePercent : Constants.DEFAULT_PRICE_CHANGE_PERCENT) / 100