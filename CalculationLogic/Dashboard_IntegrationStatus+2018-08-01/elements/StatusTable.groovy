import org.joda.time.DateTimeZone
import org.pojava.datetime.DateTime

def masters = ["C", "P"]
def pxList = ["BaseCost", "SalesPrice", "CustomerSalesPrice"]
def dmList = ["SalesHistoryDM"]

def tables = [
        [category: "Master Data", typeCode: "C", name: "Customer", label: "Produktstammdaten", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Master Data", typeCode: "P", name: "Product", label: "Kundenstammdaten", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "S_ZPE", label: "S_ZPE", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "S_ZPLP", label: "Verkaufspreis (ZPLP)", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "S_ZPAP", label: "Customer Sales Price", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "ClearingRebate", label: "Clearing Rebate", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "S_ZPZ", label: "S_ZPZ", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "CRMProject", label: "CRM Projects", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "S_ZPS", label: "S_ZPS", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "S_ZPC", label: "SalesPrice - ZPLP", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "S_ZPF", label: "Fixpreis (ZPF)", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Product Extension", typeCode: "PX", name: "S_ZRC", label: "S_ZRC", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Data Source", name: "SalesHistory2", label: "Sales History", updateMethod: "Automatic", yellow: 28, red: 32],
        [category: "Data Source", name: "Transactions", label: "Transactions", updateMethod: "Automatic", yellow: 28, red: 32],
]

def resultMatrix = api.newMatrix("Source", "Name", "Label", "LastUpdateTime", "LastUpdateDate", "UpdateMethod", "Status")
resultMatrix.setColumnFormat("Source", FieldFormatType.TEXT)
resultMatrix.setColumnFormat("Name", FieldFormatType.TEXT)
resultMatrix.setColumnFormat("Label", FieldFormatType.TEXT)
resultMatrix.setColumnFormat("LastUpdateDate", FieldFormatType.DATETIME)
resultMatrix.setColumnFormat("LastUpdateTime", FieldFormatType.TEXT)
resultMatrix.setPreferenceName("IntegrationStatusTable")

def daysAgo(int numberOfDays) {
    def calendar = api.calendar()
    calendar.add(Calendar.DATE, -numberOfDays)
    return calendar.getTime()
}

tables.each { table ->
    def lastUpdateDate = null
    if (table.category == "Master Data") {
        lastUpdateDate = api.find(table.typeCode, 0, 1, "-lastUpdateDate")?.find()?.lastUpdateDate
    }
    if (table.category == "Product Extension") {
        lastUpdateDate = api.find("PX", 0, 1, "-lastUpdateDate", Filter.equal("name", table.name))?.find()?.lastUpdateDate
    }
    if (table.category == "Customer Extension") {
        lastUpdateDate = api.find("CX", 0, 1, "-lastUpdateDate", Filter.equal("name", table.name))?.find()?.lastUpdateDate
    }
    if (table.category == "Data Source") {
        def ctx = api.getDatamartContext()
        def query = ctx.newQuery(ctx.getDataSource(table.name), true)
        query.select("lastUpdateDate", "date")
        query.orderBy("lastUpdateDate DESC")
        query.setMaxRows(1)
        def result = ctx.executeQuery(query)
        lastUpdateDate = result?.getData()?.find()?.date
    }
    api.trace("lastUpdateDate", table.name, lastUpdateDate)

    if (lastUpdateDate != null) {
        def lastUpdateDateTZ = new DateTime(lastUpdateDate).withZone(api.getTimeZone("Europe/Berlin"))
        def row = [
                "Source"        : table.category,
                "Name"          : table.name,
                "Label"         : table.label,
                "LastUpdateTime": lastUpdateDateTZ.toString("dd/MM/yyyy HH:mm:ss"),
                "LastUpdateDate": lastUpdateDateTZ,
                "UpdateMethod"  : table.updateMethod
        ]
        def date = lastUpdateDateTZ.toDate()
        def yellowDate = daysAgo(table.yellow)
        def redDate = daysAgo(table.red)
        if (date.before(redDate)) {
            row.put("Status", resultMatrix.imageCell("./small_trafficlight_3_red.png"))
        } else if (date.before(yellowDate)) {
            row.put("Status", resultMatrix.imageCell("./small_trafficlight_2_yellow.png"))
        } else {
            row.put("Status", resultMatrix.imageCell("./small_trafficlight_1_green.png"))
        }
        resultMatrix.addRow(row)
    }
}


return resultMatrix