package Dashboard_CustomerSalesPricesPerProduct.elements

def resultMatrix = api.newMatrix("Article Number", "Count of ZPAPs")
resultMatrix.setTitle("Number of ZPAPs per Product - table")
resultMatrix.setPreferenceName("MLZPAPPerProd_Table")
resultMatrix.setColumnFormat("Count of ZPAPs", FieldFormatType.NUMERIC)

def zpapsPerProduct = api.getElement("ZPAPsPerProduct")

def total = 0;

for (row in zpapsPerProduct) {
    resultMatrix.addRow([
            "Article Number": row.sku,
            "Count of ZPAPs": row.count
    ])
    total += row.count;
}
resultMatrix.addRow([
        "Article Number": resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of ZPAPs": resultMatrix.styledCell(total).withCSS("font-weight: bold")
])
return resultMatrix
