def customerSectors = api.findLookupTableValues("CustomerSector")
        .collect { r -> [
        sector: r.name,
        category: r.attribute1
]}
.groupBy { r -> r.category }

def category = api.option("Kundenbranche", customerSectors.keySet() as List)

if (category) {
    return customerSectors[category]*.sector
}



