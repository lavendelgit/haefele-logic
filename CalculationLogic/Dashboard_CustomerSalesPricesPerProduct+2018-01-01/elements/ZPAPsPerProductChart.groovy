package Dashboard_CustomerSalesPricesPerProduct.elements

def zpapsPerProduct = api.getElement("ZPAPsPerProduct");
def top1000 = zpapsPerProduct.take(1000) // turboTreshold

def categories = top1000.collect { e -> e.sku}
def data = top1000.collect { e -> e.count};

def chartDef = [
        chart: [
                type: 'column',
                zoomType: 'x'
        ],
        title: [
                text: 'ZPAPs per Product'
        ],
        xAxis: [[
                        title: [
                                text: "Article Number"
                        ],
                        categories: categories
                ]],
        yAxis: [[
                        title: [
                                text: "Number of ZPAPs"
                        ]
                ]],
        tooltip: [
                headerFormat: 'Article Number: {point.key}<br/>',
                pointFormat: 'Number of ZPAPs: {point.y}'
        ],
        legend: [
                enabled: false
        ],
        series: [[
                         data: data
                 ]]
]

if (zpapsPerProduct?.size() > 1000) {
    chartDef.subtitle = [
        text: 'Top 1000'
    ]
}

api.buildFlexChart(chartDef)