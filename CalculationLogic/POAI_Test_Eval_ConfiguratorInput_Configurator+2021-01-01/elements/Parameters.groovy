def configurator = api.createConfiguratorEntry()

configurator.createParameter(
        api.inputBuilderFactory()
                .createRowLayout("Row")
                .addInput(
                        api.inputBuilderFactory()
                                .createStringUserEntry("StringEntry")
                                .setTextMask("Enter some string :)")
                                .setLabel("A String entry with refresh")
                                .buildContextParameter()
                )
                .addInput(
                        api.inputBuilderFactory()
                                .createOptionEntry("OptionEntry")
                                .setPlaceholderText("Select an option :)")
                                .setLabel("An option entry with refresh")
                                .setOptions(["blue_pill", "red_pill"])
                                .setLabels([blue_pill: "Blue pill", red_pill: "Red Pill"])
                                .buildContextParameter()
                )
                .buildContextParameter()
)

def optionEntry = api.input("OptionEntry")
if (optionEntry) {
    configurator.createParameter(
            api.inputBuilderFactory()
                    .createStringUserEntry("DynamicStringEntry")
                    .setLabel("A dynamically created String entry")
                    .buildContextParameter(),
            { "default value" }
    )
}

configurator.createParameter(
        api.inputBuilderFactory()
                .createCollapseLayout("Collapse")
                .addInput(
                        api.inputBuilderFactory()
                                .createOptionEntry("AnotherOptionEntry")
                                .setPlaceholderText("Select an option :)")
                                .setLabel("An option entry with refresh in a collapse")
                                .setOptions(["blue_pill", "red_pill"])
                                .setLabels([blue_pill: "Blue pill", red_pill: "Red Pill"])
                                .buildContextParameter()
                )
                .buildContextParameter()
)

return configurator
