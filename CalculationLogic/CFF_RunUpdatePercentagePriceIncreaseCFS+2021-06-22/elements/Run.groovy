actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price ZPAP")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price ZPC")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price ZPP")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price ZPM")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price ZPA")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price ZPZ")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price ZNRV")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price ZPE")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("Update Percentage Increase In Price Fixpreis (ZPF)")
        .setCalculate(true)
