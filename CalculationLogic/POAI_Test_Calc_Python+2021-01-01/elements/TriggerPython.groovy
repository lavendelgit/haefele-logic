def inputValue = model.inputs('twotab', 'tab1').StringEntry

if (inputValue == null) {
    api.throwException("Cannot read state from input set in step 'Calc, Evals' and 'Tab 1' tab. Please fill the string entry there and save it.")
}

def inputName = 'anInput'

def script = """
import pyfx
import time

context = pyfx.get_context()
msg = 'This is what I got: ' +  context.user_params()['${inputName}']
context.update_progress(50, msg)
print(msg)
context.update_progress(50, 'will wait 30 seconds now to simulate a realistic python job...')
time.sleep(30)
context.update_progress(100, '... and we are done!')
"""

model.startJobTriggerCalculation(
        "cregistry.pricefx.eu/tom.jorquera/pricefx-python-env/pyfx",
        "prerelease-2",
        api.jsonEncode([script: script, user: [(inputName): inputValue]]),
        "py"
)

return "Job triggered, job messages and logs should contain: This is what I got: ${inputValue}"
