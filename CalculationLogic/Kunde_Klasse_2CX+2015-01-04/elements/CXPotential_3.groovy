def customerId = api.getElement("CustomerId")

api.trace("CustomerId", null, customerId)

def cx2 = api.find("CX", Filter.equal("name", "Potential"),
        Filter.equal("customerId", customerId),
        Filter.equal("attribute2", api.getElement("Year_3")))

if (cx2) {
  return cx2[0].attribute1
}

return null