def customerId = api.getElement("CustomerId")

api.trace("CustomerId", null, customerId)

def cxp = api.find("CX", Filter.equal("name", "Potential"),
        Filter.equal("customerId", customerId),
        Filter.equal("attribute2", api.getElement("Year")))

if (cxp) {
  return cxp[0].attribute1
}

return null