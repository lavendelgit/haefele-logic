def customerId = api.getElement("CustomerId")

api.trace("CustomerId", null, customerId)

def cx1 = api.find("CX", Filter.equal("name", "Potential"),
        Filter.equal("customerId", customerId),
        Filter.equal("attribute2", api.getElement("Year_1")))

if (cx1) {
  return cx1[0].attribute1
}

return null