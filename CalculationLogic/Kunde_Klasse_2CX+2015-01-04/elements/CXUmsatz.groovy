def customerId = api.getElement("CustomerId")

api.trace("CustomerId", null, customerId)

def cxu = api.find("CX", Filter.equal("name", "Jahresumsatz"),
        Filter.equal("customerId", customerId),
        Filter.equal("attribute2", api.getElement("Year_1")))

if (cxu) {
  return cxu[0].attribute1
}

return null