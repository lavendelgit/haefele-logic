def transactions = api.getElement("Transactions")
def regressionData = api.getElement("RegressionData")
def lra = transactions.linReg.lra
def lrb = transactions.linReg.lrb

def regressionEquation(lra, lrb) {
    if (lra == null || lrb == null) {
        return ""
    }
    return "y = " + lra + (lrb < 0 ? " - " : " + ") + Math.abs(lrb) + "x"
}

def rSquaredFormatted(value) {
    if (value == null) {
        return ""
    }
    return "R²=" + api.formatNumber("#.##%", value)
}

def chartDef = [
        chart: [
                type: "scatter",
                zoomType: "xy"
        ],
        title: [
                text: null
        ],
        xAxis: [[
                        title: [
                                text: "Price"
                        ]
                ]],
        yAxis: [[
                        title: [
                                text: "Units"
                        ]
                ]],
        legend: [
                enabled: true
        ],
        series: [[
                name: "Transactions",
                data: transactions.data.collect { d -> [d.price, d.quantity]},
                color: '#2f7ED8',
                marker: [
                        radius: 3,
                        symbol: "circle",
                ],
                tooltip: [
                        pointFormat: 'Price: {point.x:,.0f} €<br>Units: {point.y}'
                ]
            ],[
                name: "Linear Regression (OLS): " + regressionEquation(lra,lrb),
                data: regressionData,
                color: "#AA4643",
                type: "line",
                marker: [
                        enabled: false,
                        symbol: "diamond"
                ],
                tooltip: [
                        headerFormat: 'Linear Regression<br>',
                        pointFormat: 'Price: {point.x:,.0f} €<br>Units: {point.y}'
                ]
        ]]
]

api.buildHighchart(chartDef)