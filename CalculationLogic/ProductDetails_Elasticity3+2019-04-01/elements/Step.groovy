def transactions = api.getElement("Transactions")
def step = 0.1

if (transactions.data.size() > 1) {
    def xRange = transactions.summary.maxPrice - transactions.summary.minPrice
    step = Math.pow(10, Math.floor(Math.log10(xRange)) - 1) // step is 1 order of magnitude less than xRange
}

if (step == 0) {
    step = 0.1
}

return step