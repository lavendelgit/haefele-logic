def transactions = api.getElement("Transactions")
def step = api.getElement("Step")
def scale = api.getElement("Scale")

def lra = transactions?.linReg?.lra
def lrb = transactions?.linReg?.lrb

if (transactions?.data?.size() < 1) {
    return;
}

def function = { x -> lrb * x + lra }
def startX = transactions.summary.minPrice.setScale(scale, BigDecimal.ROUND_DOWN) - step
def endX = transactions.summary.maxPrice + step

def regressionData = lib.Line.calculatePoints(function, startX, endX, step)

api.trace("regressionData", "", regressionData)

return regressionData
