def sku = api.product("sku")
def dmCtx = api.getDatamartContext()
def table = dmCtx.getTable("TransactionsDM")
def query = dmCtx.newQuery(table)
def timeFrame = api.getElement("TimeFrame")

def filters = [
        Filter.equal("Material", sku),
        Filter.greaterThan("Units", 0),
        Filter.greaterThan("Revenue", 0),
]

if (timeFrame == "Last 12 months") {
        filters.add(Filter.greaterOrEqual("InvoiceDate", lib.Date.yearAgoFirstOfMonth()))
}

if (timeFrame == "Previous Year") {
        filters.add(Filter.equal("InvoiceDateYear", lib.Date.previousYear()))
}

query.select("AVG(SalesPricePer100Units)", "price")
query.select("SUM(Units)", "quantity")
query.select("SUM(Revenue)", "revenue")
query.select("InvoiceId", "invoiceId")
query.select("InvoicePos", "invoicePos")
query.where(*filters)
query.orderBy("price")

def result = dmCtx.executeQuery(query)
def data = result?.getData()?.collect()

def ols = result?.getData()
        ?.linearRegression()
        ?.usingDependentVariable("quantity")
        ?.usingIndependentVariables(["price"])
        ?.run()

def lra = ols?.intercept;
def lrb = ols?.coefficients?.toList()?.first()

api.trace("ols", "", ols)
api.trace("R2", "", ols?.r2)
api.trace("P-value", "", ols?.pvalue)
api.trace("intercept", "", ols?.intercept)
api.trace("coefficients", "", ols?.coefficients?.toList())
api.trace("t-test", "", ols?.ttest?.toList())
api.trace("result", "", ols?.getResult())

return [
        data: data,
        linReg: [
            lra: lra ?: 0.0,
            lrb: lrb ?: 0.0,
            ols: ols
        ],
        summary: [
            minPrice: data*.price?.min(),
            maxPrice: data*.price?.max(),
        ]
]
