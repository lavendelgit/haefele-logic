// Lookup into the DS and find the information about the details on customer
def dmCtx = api.getDatamartContext()
def ccImpactPerCustomerGroup = dmCtx.getDataSource("CustomerCategorizationImpact")
def dataSourceQuery = dmCtx.newQuery(ccImpactPerCustomerGroup)
dataSourceQuery.select("CCYear", "CCYear")
dataSourceQuery.select("CustomerId", "CustomerId")
dataSourceQuery.select("CCKundePotentialGruppe", "CustomerPotentialGroup")
dataSourceQuery.select("CCComputedPotentialGroup", "ComputedPotentialGroup")
dataSourceQuery.select("CorrectionType", "CorrectionType")
dataSourceQuery.select("SUM(CCTotalRevenue)", "revenue")
String currentYear = (Integer.parseInt(new java.util.Date().format("yyyy"))-1) + "-01-01"

def filters = [
        Filter.equal("CustomerId", out.CustomerId),
        Filter.equal("CCYear", currentYear)
]
dataSourceQuery.where(filters)
api.local.dataToPopulate = dmCtx.executeQuery(dataSourceQuery)?.getData()?.collect()
lib.TraceUtility.developmentTraceRow ("Printing api.local.dataToPopulate", api.local.dataToPopulate)
if (!api.local.dataToPopulate) {
    api.local.dataToPopulate = [['CustomerPotentialGroup':'Unknown', 'ComputedPotentialGroup':'', 'CorrectionType':'Unknown']]
}

return api.local.dataToPopulate[0]['CustomerPotentialGroup']