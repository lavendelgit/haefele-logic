def controller = api.newController()

controller.addHTML("""
<h1>Input from previous step</h1>
<ul>
<li>tab1 string entry: '${model.inputs("manytabs", "tab1").StringEntry}'</li>
<li>tab2 string entry: '${model.inputs("manytabs", "tab2").InlineConfigurator.StringEntry}'</li>
<li>tab3 string entry: '${model.inputs("manytabs", "tab3").NoRefreshConfigurator.StringEntry}'</li>
<li>tab4 string entry: '${model.inputs("manytabs", "tab4").StringEntry}'</li>
<li>tab5 string entry: '${model.inputs("manytabs", "tab5").StringEntry}'</li>
</ul>
""")

return controller
