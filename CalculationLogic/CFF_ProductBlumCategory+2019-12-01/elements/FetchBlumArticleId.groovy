import net.pricefx.formulaengine.AbstractProducer

Filter articleFilter = Filter.or(
        Filter.like("attribute15", "%blum%"),
        Filter.like("attribute16", "%blum%"),
        Filter.like("attribute17", "%blum%")
)
AbstractProducer.ResultIterator prodhAttributes
List result = []
try {
    prodhAttributes = api.stream("P", null, ["sku"], articleFilter)
    if (!prodhAttributes) {
        return result
    }

    while (prodhAttributes.hasNext()) {
        result.add(prodhAttributes.next())
    }
} catch (Exception exception) {
    api.logWarn("Exception in generateArticleIdsList", exception.getMessage())
}
finally {
    if (prodhAttributes) {
        prodhAttributes.close()
    }
}

api.trace("Id", result)
return result
