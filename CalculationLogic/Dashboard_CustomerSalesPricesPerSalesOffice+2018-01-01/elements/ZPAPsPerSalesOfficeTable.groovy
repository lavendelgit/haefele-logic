package Dashboard_CustomerSalesPricesPerSalesOffice.elements

def resultMatrix = api.newMatrix("Sales Office","Count of ZPAPs")
resultMatrix.setTitle("Number of ZPAPs per Sales Office - table")
resultMatrix.setPreferenceName("MLZPAP_OverviewPerSalesOffice_Table")
resultMatrix.setColumnFormat("Count of ZPAPs", FieldFormatType.NUMERIC)

def zpapsPerSalesOffice = api.getElement("ZPAPsPerSalesOffice")

def total = 0;

for (row in zpapsPerSalesOffice) {
    resultMatrix.addRow([
            "Sales Office" : row.salesOffice,
            "Count of ZPAPs" : row.count
    ])
    total += row.count
}

resultMatrix.addRow([
        "Sales Office": resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of ZPAPs": resultMatrix.styledCell(total).withCSS("font-weight: bold")
])

return resultMatrix
