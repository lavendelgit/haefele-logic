package Dashboard_CustomerSalesPricesPerSalesOffice.elements

import com.googlecode.genericdao.search.Filter

// def customerFilter = api.getElement("CustomerFilter")
/*
def customerFilter = [
        Filter.or(
                Filter.equal("Verkaufsbereich2","Verkaufsbüros Inland (Handwerk)"),
                Filter.equal("Verkaufsbereich2","Handel"),
        ),
        Filter.notEqual("Verkaufsbereich3","Bozen")
]
*/

/*List customerFilter = [Filter.in("Verkaufsbereich3",
                                 ["Berlin",
                                  "Frankfurt",
                                  "Handel",
                                  "Hannover",
                                  "Kaltenkirchen",
                                  "Köln",
                                  "München",
                                  "Münster",
                                  "Naumburg",
                                  "Nürnberg",
                                  "Stuttgart Airport",
                                  "Nicht definiert"])] */

def targetDate = api.targetDate()?.format("yyyy-MM-dd")
Filter salesOfficeFilter = libs.__LIBRARY__.HaefeleSpecific.getSalesOfficeInlandFilters('Verkaufsbereich3')
def filters = [Filter.lessOrEqual("ValidFrom", targetDate),
               Filter.greaterOrEqual("ValidTo", targetDate),
               salesOfficeFilter ]

if (api.isUserInGroup("ProductGroupManager", api.user().loginName)) {
    def pgm = api.findLookupTableValues("ProductGroupManager")?.find()?.value
    filters.add(Filter.equal("SM", pgm))
}


def dmCtx = api.getDatamartContext()
def dmQuery = dmCtx.newQuery(dmCtx.getDatamart("CustomerSalesPricesDM"))

dmQuery.select("Verkaufsbereich3", "salesOffice")
dmQuery.select("COUNT(Material)", "count")
dmQuery.where(*filters)
dmQuery.having(Filter.greaterThan("count", 0))

return dmCtx.executeQuery(dmQuery)?.getData()
            ?.collect { r ->
                r.salesOffice = r.salesOffice ?: "Undefined"
                return r
            }
            ?.groupBy { r -> r.salesOffice }
            ?.collect { e -> [salesOffice: e.key, count: e.value.count.sum()] } // sum Undefined from null and Undefined from empty string
            ?.sort { r1, r2 -> r1.salesOffice <=> r2.salesOffice }

