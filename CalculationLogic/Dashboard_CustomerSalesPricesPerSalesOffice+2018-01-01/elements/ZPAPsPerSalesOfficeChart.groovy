package Dashboard_CustomerSalesPricesPerSalesOffice.elements

def zpapsPerSalesOffice = api.getElement("ZPAPsPerSalesOffice")

def categories = zpapsPerSalesOffice.collect { e -> e.salesOffice}
def data = zpapsPerSalesOffice.collect { e -> e.count};

def chartDef = [
        chart: [
            type: 'column'
        ],
        title: [
            text: 'ZPAPs per Sales Office'
        ],
        xAxis: [[
            title: [
                    text: "Sales Office"
            ],
            categories: categories
        ]],
        yAxis: [[
            title: [
                text: "Number of ZPAPs"
            ]
        ]],
        tooltip: [
            headerFormat: 'Sales Office: {point.key}<br/>',
            pointFormat: 'Number of ZPAPs: {point.y}'
        ],
        legend: [
            enabled: false
        ],
        series: [[
             data: data,
             dataLabels: [
                 enabled: true,
             ]
        ]]
]

api.buildFlexChart(chartDef)