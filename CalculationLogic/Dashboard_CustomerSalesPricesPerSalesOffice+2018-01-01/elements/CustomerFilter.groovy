package Dashboard_CustomerSalesPricesPerSalesOffice.elements

def filter = api.customerGroupEntry("Customer Filter")

return filter != null ? api.datamartFilter(filter) : Filter.custom("1=1")
