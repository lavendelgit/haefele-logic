Map<String, Map> configurationInputsKpi = Configuration.INPUTS.kpi
def selected = api.option(configurationInputsKpi.label, configurationInputsKpi.values.keySet() as List, configurationInputsKpi.values as Map)

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(configurationInputsKpi.label, configurationInputsKpi.defaultValue)

return selected
