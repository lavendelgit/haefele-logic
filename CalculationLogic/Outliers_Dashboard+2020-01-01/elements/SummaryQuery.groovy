Map<String, Map> chartFields = Configuration.CHART_FIELDS
Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION
String targetCurrencyCode = api.getElement("TargetCurrencyData").currencyCode
Map inputValues = api.getElement("UserInputValues")
def queryUtils = libs.SIP_Dashboards_Commons.QueryUtils

Map summaryQueryDef = [datamartName      : sqlConfiguration.datamartName,
                       rollup            : true,
                       fields            : [(chartFields.revenue.label)         : "SUM(${sqlConfiguration.invoicePrice})",
                                            (chartFields.totalMargin.label)     : "SUM(${sqlConfiguration.grossMargin})",
                                            (chartFields.volume.label)          : "SUM(${sqlConfiguration.quantity})",
                                            (chartFields.marginPercentage.label): "SUM(${sqlConfiguration.grossMargin}) / SUM(${sqlConfiguration.invoicePrice})"],
                       whereFilters : [*queryUtils.generateDateFromToFilters(inputValues.dateFrom, inputValues.dateTo, sqlConfiguration?.pricingDate),
                                       inputValues.genericFilter],
                       targetCurrencyCode: targetCurrencyCode]

def result = libs.HighchartsLibrary.QueryModule.queryDatamart(summaryQueryDef)

return result.getAt(0)