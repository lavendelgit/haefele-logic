String optionName = Configuration.INPUTS.date.to.label
def dateToOption = api.dateUserEntry(optionName)
def today = java.util.Calendar.getInstance().getTime()

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(optionName, today)

return dateToOption