List usedColumns = Configuration.RESULT_MATRIX_FIELDS.performingCustomers
Map rawData = api.getElement("CustomerRawData")

return OutliersUtils.getPerformingResultMatrix(rawData, OutliersUtils.overrideResultMatrixColumnsLabels(usedColumns))