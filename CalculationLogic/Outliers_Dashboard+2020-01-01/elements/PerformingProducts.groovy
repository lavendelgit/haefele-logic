List usedColumns = Configuration.RESULT_MATRIX_FIELDS.performingProducts
Map rawData = api.getElement("ProductRawData")

return OutliersUtils.getPerformingResultMatrix(rawData, OutliersUtils.overrideResultMatrixColumnsLabels(usedColumns))