String currencySymbol = api.getElement("TargetCurrencyData").currencySymbol
String currencyFormat = "#,### ${currencySymbol}"
String percentFormat = "#0.00%"
String numericFormat = "#,###"

Map chartFields = Configuration.CHART_FIELDS

return [(chartFields.contributionMarginPercentage.label) : [valueFormat: percentFormat],
        (chartFields.contributionRevenuePercentage.label): [valueFormat: percentFormat],
        (chartFields.revenue.label)                      : [valueFormat: currencyFormat],
        (chartFields.marginPercentage.label)             : [valueFormat: percentFormat],
        (chartFields.volume.label)                       : [valueFormat: numericFormat],
        (chartFields.totalMargin.label)                  : [valueFormat: currencyFormat]]