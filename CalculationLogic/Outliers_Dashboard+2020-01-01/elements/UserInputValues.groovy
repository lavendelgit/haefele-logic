Map<String, Map> configurationInputs = Configuration.INPUTS

return [productGroup         : api.getElement("ProductGroupEntry") ?: configurationInputs.productGroup.defaultValue,
        customerGroup        : api.getElement("CustomerGroupEntry") ?: configurationInputs.customerGroup.defaultValue,
        dateFrom             : api.getElement("DateFromEntry") ?: configurationInputs.date.from.defaultValue,
        dateTo               : api.getElement("DateToEntry") ?: configurationInputs.date.to.defaultValue,
        noOfProductOrCustomer: api.getElement("NoOfProductOrCustomerEntry") as Integer ?: configurationInputs.noOfResults.defaultValue,
        kpi                  : api.getElement("KPIEntry") ?: configurationInputs.kpi.defaultValue,
        genericFilter        : api.getElement("GenericFilter")]