String kpi = api.getElement("SelectedKPI")
Map productData = api.getElement("ProductRawData")
Map<String, Map> chartFields = Configuration.CHART_FIELDS
def commonColumns = Configuration.PIE_CHART_DATA_COMMON_COLUMNS

Map productColumns = OutliersUtils.getLabeledColumnsConfiguration(commonColumns + chartFields.volume)
Map formatConfiguration = api.getElement("ChartValueFormats")

return OutliersUtils.getPieChartData(productData, kpi, productColumns, formatConfiguration)