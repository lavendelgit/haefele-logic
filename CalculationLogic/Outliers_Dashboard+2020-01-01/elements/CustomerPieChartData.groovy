String kpi = api.getElement("SelectedKPI")
Map customerData = api.getElement("CustomerRawData")
def commonColumns = Configuration.PIE_CHART_DATA_COMMON_COLUMNS

Map customerColumns = OutliersUtils.getLabeledColumnsConfiguration(commonColumns)
Map formatConfiguration = api.getElement("ChartValueFormats")

return OutliersUtils.getPieChartData(customerData, kpi, customerColumns, formatConfiguration)