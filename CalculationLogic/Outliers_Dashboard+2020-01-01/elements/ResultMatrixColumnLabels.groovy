String currencySymbol = api.getElement("TargetCurrencyData").currencySymbol
Map chartFields = Configuration.CHART_FIELDS

return [(chartFields.revenue.label)    : [label: "${chartFields.revenue.label} (${currencySymbol})"],
        (chartFields.totalMargin.label): [label: "${chartFields.totalMargin.label} (${currencySymbol})"]]
