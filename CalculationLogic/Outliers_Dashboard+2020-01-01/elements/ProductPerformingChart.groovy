String chartTitle = "Products Performance"
Map pieData = api.getElement("ProductPieChartData")

return OutliersUtils.generatePieChart(chartTitle, pieData)