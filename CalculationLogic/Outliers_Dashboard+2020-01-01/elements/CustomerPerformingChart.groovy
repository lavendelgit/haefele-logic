String chartTitle = "Customers Performance"
Map pieData = api.getElement("CustomerPieChartData")

return OutliersUtils.generatePieChart(chartTitle, pieData)