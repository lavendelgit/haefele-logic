def products = api.getElement("ProductsQuery")
def summary = api.getElement("SummaryQuery")
def kpi = api.getElement("SelectedKPI")

return OutliersUtils.outliersDataProcessing(products, summary, kpi)