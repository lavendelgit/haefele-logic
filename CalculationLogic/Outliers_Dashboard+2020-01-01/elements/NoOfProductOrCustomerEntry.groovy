def noOfResults = Configuration.INPUTS.noOfResults
def selected = api.option(noOfResults.label, noOfResults.values)

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(noOfResults.label, noOfResults.defaultValue)

return selected

