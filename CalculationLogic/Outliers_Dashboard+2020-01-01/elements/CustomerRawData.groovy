def customers = api.getElement("CustomersQuery")
def summary = api.getElement("SummaryQuery")
def kpi = api.getElement("SelectedKPI")

return OutliersUtils.outliersDataProcessing(customers, summary, kpi)