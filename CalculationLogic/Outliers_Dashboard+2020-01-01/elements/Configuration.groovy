import groovy.transform.Field
import net.pricefx.common.api.FieldFormatType

@Field BOLD_FONT_WEIGHT = "bold"

@Field SQL_CONFIGURATION
SQL_CONFIGURATION = libs.SIP_Dashboards_Commons.ConfigurationUtils.getAdvancedConfiguration([:], libs.SIP_Dashboards_Commons.ConstConfig.DASHBOARDS_ADVANCED_CONFIGURATION_NAME)

@Field CHART_CONFIGURATION = [maxDrillDownItemCount: 10,
                              othersGroupName      : "Others"]

@Field RESULT_MATRIX_SUMMARY = [style: [textColor: null,
                                        bgColor  : null,
                                        weight   : BOLD_FONT_WEIGHT],
                                name : "Summary",
                                data : null]

@Field CHART_FIELDS = [:]
CHART_FIELDS << [contributionMarginPercentage : [name        : "margin_contribution_pct",
                                                 label       : "Margin Contribution %",
                                                 resultMatrix: [format: FieldFormatType.PERCENT]],
                 contributionRevenuePercentage: [name        : "revenue_contribution_pct",
                                                 label       : "Revenue Contribution %",
                                                 resultMatrix: [format: FieldFormatType.PERCENT]],
                 name                         : [name        : "name",
                                                 label       : "Name",
                                                 resultMatrix: null],
                 number                       : [name        : "number",
                                                 label       : "Number",
                                                 resultMatrix: null],
                 revenue                      : [name        : "revenue",
                                                 label       : "Revenue",
                                                 resultMatrix: [format: FieldFormatType.MONEY]],
                 marginPercentage             : [name        : "margin_pct",
                                                 label       : "Margin %",
                                                 resultMatrix: [format: FieldFormatType.PERCENT]],
                 volume                       : [name        : "volume",
                                                 label       : "Volume",
                                                 resultMatrix: [format: FieldFormatType.NUMERIC]],
                 totalMargin                  : [name        : "margin",
                                                 label       : "Margin",
                                                 resultMatrix: [format: FieldFormatType.MONEY]]]


@Field EMPTY_STRING = ""

@Field INDICATOR = [high    : [value    : [prefix: "▲",
                                           suffix: EMPTY_STRING],
                               textColor: "#2e7d10",
                               weight   : BOLD_FONT_WEIGHT],
                    low     : [value    : [prefix: "▼",
                                           suffix: EMPTY_STRING],
                               textColor: "#f44340",
                               weight   : BOLD_FONT_WEIGHT],
                    medium  : [value    : [prefix: EMPTY_STRING,
                                           suffix: EMPTY_STRING],
                               textColor: "#7cb5ec",
                               weight   : BOLD_FONT_WEIGHT],
                    negative: [value    : [prefix: EMPTY_STRING,
                                           suffix: EMPTY_STRING],
                               textColor: "#f7a35c",
                               weight   : BOLD_FONT_WEIGHT]]

@Field INPUTS = [:]
INPUTS << [kpi          : [name        : "KPI",
                           label       : "KPI",
                           values      : [(CHART_FIELDS.revenue.name)                      : (CHART_FIELDS.revenue.label),
                                          (CHART_FIELDS.contributionRevenuePercentage.name): (CHART_FIELDS.contributionRevenuePercentage.label),
                                          (CHART_FIELDS.totalMargin.name)                  : (CHART_FIELDS.totalMargin.label),
                                          (CHART_FIELDS.marginPercentage.name)             : (CHART_FIELDS.marginPercentage.label),
                                          (CHART_FIELDS.contributionMarginPercentage.name) : (CHART_FIELDS.contributionMarginPercentage.label)],
                           defaultValue: CHART_FIELDS.revenue.name],
           date         : [from: [name        : "DateFrom",
                                  label       : "Date From",
                                  defaultValue: null,
                                  filter      : "PricingDate"],
                           to  : [name        : "DateTo",
                                  label       : "Date To",
                                  defaultValue: null,
                                  filter      : "PricingDate"]],
           noOfResults  : [name        : "NoOfProductOrCustomer",
                           label       : "Top Product(s) / Customer(s)",
                           values      : [5, 10, 25, 50, 100],
                           defaultValue: 5],
           productGroup : [name        : "ProductGroup",
                           label       : "Product(s)",
                           defaultValue: null],
           customerGroup: [name        : "CustomerGroup",
                           label       : "Customer(s)",
                           defaultValue: null]]

@Field BASE_RESULT_MATRIX_FIELDS = []
BASE_RESULT_MATRIX_FIELDS += [CHART_FIELDS.name,
                              CHART_FIELDS.number,
                              CHART_FIELDS.revenue,
                              CHART_FIELDS.totalMargin,
                              CHART_FIELDS.marginPercentage,
                              CHART_FIELDS.contributionMarginPercentage,
                              CHART_FIELDS.contributionRevenuePercentage]

@Field RESULT_MATRIX_FIELDS = [:]
RESULT_MATRIX_FIELDS << [performingProducts : (BASE_RESULT_MATRIX_FIELDS + (CHART_FIELDS.volume)),
                         performingCustomers: BASE_RESULT_MATRIX_FIELDS]

@Field DATALABEL = [format: "<b>{point.name}: {point.count}</b><br>"]

@Field PIE_CHART_DATA_COMMON_COLUMNS = []
PIE_CHART_DATA_COMMON_COLUMNS += [CHART_FIELDS.revenue,
                                  CHART_FIELDS.contributionRevenuePercentage,
                                  CHART_FIELDS.totalMargin,
                                  CHART_FIELDS.marginPercentage,
                                  CHART_FIELDS.contributionMarginPercentage]

@Field BUCKETS = [high    : "High",
                  medium  : "Medium",
                  low     : "Low",
                  negative: "Negative"]

@Field ORDER = [top   : "DESC",
                bottom: "ASC"]

@Field QUERY_LIMIT_DEFAULT = 10