import groovy.transform.Field

Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION
Map<String, String> chartFields = Configuration.CHART_FIELDS

@Field BASE_QUERY_DEFINITION = [:]

BASE_QUERY_DEFINITION << [datamartName: sqlConfiguration.datamartName,
                          rollup      : true,
                          fields      : [(chartFields.revenue.label)         : "SUM(${sqlConfiguration.invoicePrice})",
                                         (chartFields.marginPercentage.label): "SUM(${sqlConfiguration.grossMargin})/SUM(${sqlConfiguration?.invoicePrice})",
                                         (chartFields.totalMargin.label)     : "SUM(${sqlConfiguration.grossMargin})"]]

@Field PRODUCT_QUERY_FIELD_DEFINITION = [:]

PRODUCT_QUERY_FIELD_DEFINITION << BASE_QUERY_DEFINITION
PRODUCT_QUERY_FIELD_DEFINITION.fields += [(chartFields.name.label)  : "${sqlConfiguration?.productName}",
                                          (chartFields.number.label): "${sqlConfiguration?.productId}",
                                          (chartFields.volume.label): "SUM(${sqlConfiguration?.quantity})"]

@Field CUSTOMER_QUERY_FIELD_DEFINITION = [:]

CUSTOMER_QUERY_FIELD_DEFINITION << BASE_QUERY_DEFINITION
CUSTOMER_QUERY_FIELD_DEFINITION.fields += [(chartFields.name.label)  : "${sqlConfiguration?.customerName}",
                                           (chartFields.number.label): "${sqlConfiguration?.customerId}"]

//Has to be at position 2 in query
@Field THRESHOLD_DEFAULT_QUERY = """SELECT 
                                        MAX(T1.kpi) - ((MAX(T1.kpi) - MIN(T1.kpi)) / 3) AS HighThreshold,
                                        MIN(T1.kpi) + ((MAX(T1.kpi) - MIN(T1.kpi)) / 3) AS LowThreshold
                                    FROM T1 
                                    WHERE T1.kpi > 0"""
//Has to be at position 3 in query
@Field BUCKET_DEFAULT_QUERY = """
		SELECT 
            T1.Item AS item,
            T1.ItemLabel AS itemLabel, 
			T1.KPI AS kpi,
            T1.Revenue AS revenue,
			T1.Margin AS margin,
			T1.Quantity AS quantity,
			T1.revenueContribution AS revenueContribution,
            T1.marginContribution AS marginContribution,
			CASE WHEN T1.KPI < 0 THEN 'Negative'::text
                 WHEN T1.KPI <= T2.LowThreshold THEN 'Low'::text
                 WHEN T1.KPI >= T2.HighThreshold THEN 'High'::text
                 ELSE 'Medium'
            END AS bucket
        FROM T1, T2    
"""

@Field OUTLIERS_QUERY = """
	SELECT 
		ALL_DATA.Item AS item,
		ALL_DATA.ItemLabel AS 'itemLabel',
		ALL_DATA.Bucket AS bucket,
		ALL_DATA.Revenue AS revenue,
		ALL_DATA.Margin AS margin,
		ALL_DATA.Quantity AS quantity,
		ALL_DATA.revenueContribution AS 'revenueContribution',
        ALL_DATA.marginContribution AS 'marginContribution',
		ALL_DATA.itemCount AS 'itemCount',
		ALL_DATA.Margin / ABS(NULLIF(ALL_DATA.Revenue,0)) AS 'marginPercent'
        FROM (
                SELECT * from T4
                UNION ALL
                SELECT * from T5
                UNION ALL
                SELECT * from T6
                UNION ALL
                SELECT * from T7
                UNION ALL
                SELECT * from T8
                UNION ALL
                SELECT * from T9
                WHERE T9.item NOT IN (SELECT T8.item FROM T8)
                UNION ALL
                SELECT * from T10) ALL_DATA
        """