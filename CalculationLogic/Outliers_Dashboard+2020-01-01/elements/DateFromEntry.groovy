String optionName = Configuration.INPUTS.date.from.label
def dateToOption = api.dateUserEntry(optionName)
java.util.Calendar calendar = java.util.Calendar.getInstance()
calendar.add(java.util.Calendar.YEAR, -1)
def todayOneYearBack = calendar.getTime()

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setOptionDefaultValue(optionName, todayOneYearBack)

return dateToOption