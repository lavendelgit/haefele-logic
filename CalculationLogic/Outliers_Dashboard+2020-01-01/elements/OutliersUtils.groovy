import net.pricefx.common.api.FieldFormatType
import net.pricefx.formulaengine.DatamartContext
import net.pricefx.server.dto.calculation.ResultMatrix

def generatePieChart(String chartName, Map pieData) {
    String kpi = api.getElement("SelectedKPI")
    def hLib = libs.HighchartsLibrary
    Map mainSeriesDataLabels = hLib.DataLabel.newDataLabel()
            .setEnabled(true)
            .setFormat(Configuration.DATALABEL.format)

    Map pieDef = hLib.Chart.newChart()
            .setTitle(chartName)
            .setSubtitle(kpi)

    if (pieData.mainSeriesData) {
        Map mainSeries = hLib.PieSeries.newPieSeries()
                .setName(kpi)
                .setData(pieData.mainSeriesData)
                .setColorPerPoint(true)
                .setDataLabels(mainSeriesDataLabels)
                .setTooltip(pieData.mainSeriesTooltip)
        pieDef.setSeries(mainSeries)
    }

    if (pieData.drilldownMaps) {
        pieDef.setDrilldownSeries(*pieData.drilldownMaps)
    }

    Map chartDefinition = pieDef.getHighchartFormatDefinition()

    return api.buildHighchart(chartDefinition)
}

List overrideResultMatrixColumnsLabels(List usedColumns) {
    Map labelsOverride = api.getElement("ResultMatrixColumnLabels")
    List result = usedColumns.collect {
        Map collectResult = it.clone()
        collectResult.label = labelsOverride.getAt(it.label)?.label as String ?: it.label
        return collectResult
    }

    return result
}

List overrideResultMatrixItemsLabels(List items) {
    Map labelsOverride = api.getElement("ResultMatrixColumnLabels")
    return items.collect { Map itemData ->
        itemData.collectEntries { label, itemValue ->
            String itemLabel = labelsOverride.getAt(label)?.label as String ?: label

            return [(itemLabel): itemValue]
        }
    }
}

/**
 * Build net.pricefx.server.dto.calculation.ResultMatrix from net.pricefx.formulaengine.DatamartContext.Query result
 * Parameters:
 *      - queryResult: result after execute query from datamart
 *      - columnSettings: List, list of column settings
 * */
ResultMatrix getResultMatrix(ResultMatrix matrix, List data, Map summary, String nameColumnLabel) {
    Map summaryData = prepareResultMatrixSummaryData(summary, matrix, nameColumnLabel)
    matrix.addRow(summaryData)

    data?.each {
        if (it != null) {
            matrix.addRow(it)
        }
    }

    return matrix
}

Map prepareResultMatrixSummaryData(Map summary, ResultMatrix matrix, String nameColumnLabel) {
    Map labelsOverride = api.getElement("ResultMatrixColumnLabels")
    Map summaryConfiguration = Configuration.RESULT_MATRIX_SUMMARY
    Map summaryStyle = summaryConfiguration.style
    Map summaryData = [:]
    String styleWeight = summaryStyle.weight
    String styleBgColor = summaryStyle.bgColor
    String styleTextColor = summaryStyle.textColor

    summary?.keySet()?.each {
        def label = labelsOverride?.getAt(it)?.label as String ?: it
        summaryData[label] = matrix.styledCell(summary[it], styleTextColor, styleBgColor, styleWeight)
    }

    summaryData[nameColumnLabel] = matrix.styledCell(summaryConfiguration.name, styleTextColor, styleBgColor, styleWeight)

    return summaryData
}

/**
 * Get map of columns to be converted to net.pricefx.server.dto.calculation.ResultMatrix from column settings
 * */
List getColumnsWithResultMatrixType(List usedColumns) {
    return usedColumns.findAll { it.resultMatrix != false }.collect { it.label }
}


/**
 * Apply settings to net.pricefx.server.dto.calculation.ResultMatrix from list of column settings
 * Parameters:
 *      - matrix: net.pricefx.server.dto.calculation.ResultMatrix instance
 *      - settings: List, list of column settings
 * */
void applyResultMatrixFormats(ResultMatrix matrix, List usedColumns) {
    if (!matrix)
        return

    usedColumns.each {
        setMatrixColumnFormat(matrix, it.label, it.resultMatrix?.format)
    }
}

/**
 * Apply settings to net.pricefx.server.dto.calculation.ResultMatrix from column settings
 * Parameters:
 *      - matrix: net.pricefx.server.dto.calculation.ResultMatrix instance
 *      - column: String, column name to set
 *      - settings: FieldFormatType, format settings for one column
 * */
void setMatrixColumnFormat(ResultMatrix matrix, String columnName, FieldFormatType format) {
    if (!matrix || !columnName || !format) {
        return
    }

    matrix.setColumnFormat(columnName, format)
}

ResultMatrix getPerformingResultMatrix(Map processedData, List usedColumns) {
    List matrixColumns = getColumnsWithResultMatrixType(usedColumns)
    ResultMatrix matrix = api.newMatrix(matrixColumns)

    if (!processedData) {
        return matrix
    }

    Map<String, Map> indicator = Configuration.INDICATOR
    String nameColumnLabel = Configuration.CHART_FIELDS.name.label
    List bestPerformanceItems = processedData.bestPerformanceItems
    List worstPerformanceItems = processedData.worstPerformanceItems
    Map performanceItems = [bestData : [items    : overrideResultMatrixItemsLabels(bestPerformanceItems),
                                        indicator: indicator.high],
                            worstData: [items    : overrideResultMatrixItemsLabels(worstPerformanceItems),
                                        indicator: indicator.low]]
    Map summary = processedData.summary

    List indicatorFilledData = fillIndicatorByBucketSize(performanceItems, nameColumnLabel)
    ResultMatrix result = getResultMatrix(matrix, indicatorFilledData, summary, nameColumnLabel)
    applyResultMatrixFormats(result, usedColumns)

    return result
}

/**
 * Utility to fill matrix cell style with best/worst kpi indicator graphic
 * */
List fillIndicatorByBucketSize(Map performanceItems, String nameColumnLabel) {
    if (!performanceItems) {
        return []
    }

    ResultMatrix matrix = api.newMatrix()

    List result = performanceItems.inject([]) { injectResult, performanceGroup ->
        injectResult += assignPerformanceGroupIndicators(performanceGroup.value, matrix, nameColumnLabel)
    }

    return result
}

List assignPerformanceGroupIndicators(Map groupElements, ResultMatrix matrix, String nameColumnLabel) {
    List indicators = []
    Map indicator = groupElements.indicator

    groupElements.items.each {
        indicators.add(fillIndicator(matrix, it, nameColumnLabel, indicator))
    }

    return indicators
}


/**
 * Utility to fill a row with matrix cell style with  indicator graphic
 * */
Map fillIndicator(ResultMatrix matrix, Map item, String nameColumnLabel, Map indicator) {
    item[nameColumnLabel] = createStyledCellWithIndicator(matrix, item, nameColumnLabel, indicator)
    return item
}

def createStyledCellWithIndicator(ResultMatrix matrix, Map item, String nameColumnLabel, Map indicator) {
    return matrix.styledCell("${indicator?.value?.prefix} ${item[nameColumnLabel]} ${indicator?.value?.suffix}",
            indicator?.textColor,
            indicator?.bgColor,
            indicator?.weight)
}

String getSelectedKPI(String userKPIValue) {
    Map<String, Map> inputConfiguration = Configuration.INPUTS

    return inputConfiguration.kpi.values.get(userKPIValue)
}

Map getLabeledColumnsConfiguration(List labelConfiguration) {
    return labelConfiguration?.inject([:]) { result, column ->
        result << [(column?.getAt("label")): column]
        return result
    }
}

/**
 * Utility to prepare piechart data for product and customer performing
 * */
Map getPieChartData(Map rawChartData, String selectedKpi, Map columnsConfiguration, Map formatConfiguration) {
    if (!rawChartData || !selectedKpi) {
        return [data: [], drilldown: []]
    }

    Map tooltip = libs.HighchartsLibrary.Tooltip.newTooltip()
            .setHeaderFormat("{point.key}<br>")
            .setPointFormat(getPointTooltipFormat(columnsConfiguration, selectedKpi))
    Map dataLabel = libs.HighchartsLibrary.DataLabel.newDataLabel()
            .setEnabled(true)
            .setFormat("{point.name}<br>")
            .setColor("contrast")
            .setFontSize("11px")
            .setFontWeight("normal")
            .setTextOutline("1px contrast")

    Map pieChartData = getPieChartDataStructure(rawChartData, formatConfiguration, selectedKpi, tooltip, dataLabel)

    return [mainSeriesData: pieChartData.chartData?.sort {
        -it.y
    }, mainSeriesTooltip  : tooltip, drilldownMaps: pieChartData.drilldown]
}

def getPieChartDataStructure(Map rawChartData, Map formatConfiguration, String selectedKpi, Map tooltip, Map dataLabel) {
    def chartData = []

    def negativeData = prepareChartData(rawChartData.negativeData, rawChartData.averageItemCount, rawChartData.summary, formatConfiguration, selectedKpi, tooltip, dataLabel)
    def lowData = prepareChartData(rawChartData.lowData, rawChartData.averageItemCount, rawChartData.summary, formatConfiguration, selectedKpi, tooltip, dataLabel)
    def mediumData = prepareChartData(rawChartData.mediumData, rawChartData.averageItemCount, rawChartData.summary, formatConfiguration, selectedKpi, tooltip, dataLabel)
    def highData = prepareChartData(rawChartData.highData, rawChartData.averageItemCount, rawChartData.summary, formatConfiguration, selectedKpi, tooltip, dataLabel)

    List drilldown = [lowData.drilldown, mediumData.drilldown, highData.drilldown]

    if (rawChartData.negativeData.itemCount > 0) {
        drilldown << negativeData.drilldown
        chartData << negativeData.mainData
    }
    if (rawChartData.lowData.itemCount > 0) {
        chartData << lowData.mainData
    }
    if (rawChartData.mediumData.itemCount > 0) {
        chartData << mediumData.mainData
    }
    if (rawChartData.highData.itemCount > 0) {
        chartData << highData.mainData
    }

    return [chartData: chartData, drilldown: drilldown]
}

def prepareChartData(Map elementData, int averageItemCount, Map summary, Map formatConfiguration, String selectedKpi, Map tooltip, Map dataLabel) {
    return [mainData : prepareChartMainData(elementData, averageItemCount, formatConfiguration, summary),
            drilldown: prepareDrilldownChartData(elementData, formatConfiguration, selectedKpi, tooltip, dataLabel)]
}

def prepareChartMainData(Map elementData, int averageItemCount, Map formatConfiguration, Map summary) {
    def mainData = getMainSeriesDataStructure(elementData.name,
            elementData.itemCount + averageItemCount,
            elementData.itemCount,
            elementData.textColor,
            elementData.name)

    def itemSummaryTooltip = prepareItemSummaryTooltip(elementData.summary, summary, formatConfiguration)
    mainData << itemSummaryTooltip

    return mainData
}

def prepareDrilldownChartData(Map elementData, Map formatConfiguration, String selectedKpi, Map drilldownTooltip, Map dataLabel) {
    def itemDrilldown = prepareItemDrilldown(elementData.drilldown, selectedKpi, formatConfiguration)

    return prepareDrilldownMap(elementData.name + "Drilldown",
            elementData.name,
            drilldownTooltip,
            dataLabel,
            itemDrilldown)
}

def prepareDrilldownMap(String name, String id, Map tooltip, Map datalabel, List data) {
    return libs.HighchartsLibrary.PieSeries.newPieSeries()
            .setName(name)
            .setId(id)
            .setTooltip(tooltip)
            .setDataLabels(datalabel)
            .setData(data)
}

def getMainSeriesDataStructure(String name, BigDecimal value, int itemCount, String textColor, String drilldownId) {
    return [name: name, y: value, count: itemCount, color: textColor, drilldown: drilldownId]
}

def prepareItemSummaryTooltip(Map itemsSummary, Map summary, Map formatConfiguration) {
    Map<String, Map> chartFields = Configuration.CHART_FIELDS

    BigDecimal totalItemMargin = itemsSummary?.getAt(chartFields.totalMargin.label)
    BigDecimal totalItemRevenue = itemsSummary?.getAt(chartFields.revenue.label)
    BigDecimal totalSummaryMargin = summary?.getAt(chartFields.totalMargin.label)
    BigDecimal totalSummaryRevenue = summary?.getAt(chartFields.revenue.label)

    BigDecimal totalMarginContribution = toPercentage(totalItemMargin, totalSummaryMargin)
    BigDecimal totalRevenueContribution = toPercentage(totalItemRevenue, totalSummaryRevenue)
    BigDecimal totalMarginPercentage = toPercentage(totalItemMargin, totalItemRevenue)

    Map itemSummaryTooltips = itemsSummary.inject([:]) { result, key, value ->
        result << [(key): value]
    }
    itemSummaryTooltips << [(chartFields.contributionMarginPercentage.label) : totalMarginContribution,
                            (chartFields.contributionRevenuePercentage.label): totalRevenueContribution,
                            (chartFields.marginPercentage.label)             : totalMarginPercentage]
    getColumnsNumberFormatting(formatConfiguration, itemSummaryTooltips)

    return itemSummaryTooltips
}

def prepareItemDrilldown(List itemsList, String selectedKpi, Map columnConfiguration) {
    Map<String, Map> chartFields = Configuration.CHART_FIELDS
    List drilldown = []

    for (item in itemsList) {
        drilldown.push(prepareItemDrilldownStructure(item, chartFields, selectedKpi, columnConfiguration))
    }

    return drilldown
}

def getColumnsNumberFormatting(Map formatConfiguration, Map groupMap) {
    formatConfiguration?.each { columnLabel, columnConfig ->
        groupMap << [(columnLabel): getFormattedNumber(columnConfig, groupMap?.getAt(columnLabel))]
    }
}

BigDecimal toPercentage(BigDecimal value, BigDecimal base) {
    return value != null && base ? value / base : 0
}

Map prepareItemDrilldownStructure(Map item, Map columns, String selectedKpi, Map formatConfiguration) {
    Map drilldownItem = [:]

    drilldownItem << item
    drilldownItem.y = item.getAt(selectedKpi)
    drilldownItem.name = getDrilldownItemName(drilldownItem, columns)
    formatConfiguration?.each { columnLabel, columnFormatConfiguration ->
        drilldownItem << [(columnLabel): getFormattedNumber(columnFormatConfiguration, drilldownItem?.getAt(columnLabel))]
    }

    return drilldownItem
}

String getDrilldownItemName(Map drilldownItem, Map columns) {
    String othersGroupName = Configuration.CHART_CONFIGURATION.othersGroupName
    String itemLabel = drilldownItem?.getAt(columns.name.label)
    String itemNumber = drilldownItem?.getAt(columns.number.label)
    
    return othersGroupName == itemLabel ? "<b>${itemLabel}</b>" : "<b>${itemLabel}</b><br>(${itemNumber})"
}

String getFormattedNumber(Map formatConfiguration, BigDecimal value) {
    def isFormatDefined = formatConfiguration?.valueFormat && value != null

    return isFormatDefined ? api.formatNumber(formatConfiguration?.valueFormat, value) : value.toString()
}

String getPointTooltipFormat(Map columnConfig, String kpi) {
    String format = ""
    String key

    for (entry in columnConfig) {
        key = entry.key
        format += "${key == kpi ? '<span style="color:{point.color}">' : ''}${key}: {point.${key}}${key == kpi ? '</span>' : ""}<br>"
    }

    return format
}

def getOutliersDataForAggregation(Map aggregationFields, Map inputValues, Map summary) {
    Map<String, String> chartFields = Configuration.CHART_FIELDS
    Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION

    def dmCtx = api.getDatamartContext()
    def sourceQuery = generateSourceQuery(aggregationFields, dmCtx, summary, inputValues)

    def highTop = getBucketItemsListQuery(Configuration.BUCKETS.high, Configuration.QUERY_LIMIT_DEFAULT, Configuration.ORDER.top)
    def mediumTop = getBucketItemsListQuery(Configuration.BUCKETS.medium, Configuration.QUERY_LIMIT_DEFAULT, Configuration.ORDER.top)
    def lowBottom = getBucketItemsListQuery(Configuration.BUCKETS.low, Configuration.QUERY_LIMIT_DEFAULT, Configuration.ORDER.bottom)
    def negativeBottom = getBucketItemsListQuery(Configuration.BUCKETS.negative, Configuration.QUERY_LIMIT_DEFAULT, Configuration.ORDER.bottom)
    def topList = getItemsListQuery("Top", inputValues.noOfProductOrCustomer, Configuration.ORDER.top)
    def bottomList = getItemsListQuery("Bottom", inputValues.noOfProductOrCustomer, Configuration.ORDER.bottom)
    def others = getOthersQuery()

    def sqlQuery = dmCtx.newSqlQuery()
    sqlQuery.addSource(sourceQuery)
    sqlQuery.addWith(SQLQueryDefinitions.THRESHOLD_DEFAULT_QUERY)
    sqlQuery.addWith(SQLQueryDefinitions.BUCKET_DEFAULT_QUERY)
    sqlQuery.addWith(highTop)
    sqlQuery.addWith(mediumTop)
    sqlQuery.addWith(lowBottom)
    sqlQuery.addWith(negativeBottom)
    sqlQuery.addWith(topList)
    sqlQuery.addWith(bottomList)
    sqlQuery.addWith(others)

    def sql = SQLQueryDefinitions.OUTLIERS_QUERY
    sqlQuery.setQuery(sql)

    dmCtx.executeSqlQuery(sqlQuery)?.collect()
}

DatamartContext.Query generateSourceQuery(Map aggregationFields,
                                          DatamartContext dmCtx,
                                          Map summary,
                                          Map inputValues) {
    Map<String, String> chartFields = Configuration.CHART_FIELDS
    Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION
    String currencySymbol = api.getElement("TargetCurrencyData").currencyCode
    def kpiExpression = getKPIExpression(inputValues.kpi, summary)
    def datamart = dmCtx.getDatamart(sqlConfiguration.datamartName)

    def query = dmCtx.newQuery(datamart, true)

    String aggregation = aggregationFields.get(chartFields.number.label)
    String aggregationLabel = aggregationFields.get(chartFields.name.label)
    //TODO - change back the aliases when https://pricefx.atlassian.net/browse/PFCD-6760
    query.select(aggregation, "item")
    query.select(aggregationLabel, "itemLabel")
    query.select(kpiExpression, "kpi")
    query.select("SUM(${sqlConfiguration.invoicePrice})", "'revenue'")
    query.select("SUM(${sqlConfiguration.grossMargin})", "'margin'")
    query.select("SUM(${sqlConfiguration.quantity})", "'quantity'")
    query.select("SUM(${sqlConfiguration.invoicePrice})/${summary.get(chartFields.revenue.label)}", "'revenueContribution'")
    query.select("SUM(${sqlConfiguration.grossMargin})/${summary.get(chartFields.totalMargin.label)}", "'marginContribution'")

    query.setOptions([currency: currencySymbol])

    if (aggregation == sqlConfiguration.productId) {
        query.where(inputValues.productGroup)
    } else {
        query.where(inputValues.customerGroup)
    }

    query.where(Filter.and(
            Filter.greaterOrEqual(sqlConfiguration.pricingDate, inputValues.dateFrom),
            Filter.lessOrEqual(sqlConfiguration.pricingDate, inputValues.dateTo)))

    query.where(Filter.isNotNull("${sqlConfiguration.invoicePrice}"),
            Filter.isNotNull("${sqlConfiguration.grossMargin}"),
            Filter.isNotNull("${sqlConfiguration.quantity}"))

    if (inputValues.genericFilter) {
        query.where(inputValues.genericFilter)
    }

    query.having(Filter.greaterThan("'revenue'", 0),
            Filter.greaterThan("'quantity'", 0))

    return query
}

String getKPIExpression(String KPI, Map summary) {
    Map<String, String> chartFields = Configuration.CHART_FIELDS
    Map<String, String> sqlConfiguration = Configuration.SQL_CONFIGURATION
    switch (KPI) {
        case chartFields.totalMargin.name:
            return "SUM(${sqlConfiguration.grossMargin})"
        case chartFields.marginPercentage.name:
            return "SUM(${sqlConfiguration.grossMargin})/SUM(${sqlConfiguration.invoicePrice})"
        case chartFields.contributionRevenuePercentage.name:
            return "SUM(${sqlConfiguration.invoicePrice})/${summary.get(chartFields.revenue.label)}"
        case chartFields.contributionMarginPercentage.name:
            return "SUM(${sqlConfiguration.grossMargin})/${summary.get(chartFields.totalMargin.label)}"
        default:
            return "SUM(${sqlConfiguration.invoicePrice})"
    }
}

String getBucketItemsListQuery(String bucket, int limit, String order) {
    return """
		SELECT  
            T3.item,
            T3.itemlabel,
            T3.bucket,
            T3.revenue,
            T3.margin,
            T3.quantity,
            T3.revenueContribution,
            T3.marginContribution,
            1 AS itemCount
		FROM T3
		WHERE T3.bucket = '${bucket}'::text
		ORDER BY t3.kpi ${order}
		LIMIT ${limit}
    """
}

String getItemsListQuery(String bucketName, int limit, String order) {
    return """
		SELECT 
            T1.item,
            T1.itemlabel,
            '${bucketName}'::text AS bucket,
            T1.revenue,
            T1.margin,
            T1.quantity,
            T1.revenueContribution,
            T1.marginContribution,
            1 AS itemCount
		FROM T1
		WHERE t1.kpi IS NOT NULL
		ORDER BY t1.kpi ${order}
		LIMIT ${limit}
"""
}

String getOthersQuery() {
    String othersGroupName = Configuration.CHART_CONFIGURATION.othersGroupName

    return """
		SELECT 
			'${othersGroupName}'::text AS item,
			'${othersGroupName}'::text AS itemlabel,
			T3.bucket AS bucket,
			SUM(T3.revenue) AS revenue,
			SUM(T3.margin) AS margin,
			SUM(T3.quantity) AS quantity,
		    SUM(T3.revenueContribution) AS revenueContribution,
            SUM(T3.marginContribution) AS marginContribution,
			COUNT(DISTINCT T3.item) AS itemCount
		FROM T3
		WHERE
			T3.item NOT IN (SELECT Item FROM T4) AND
			T3.item NOT IN (SELECT Item FROM T5) AND
			T3.item NOT IN (SELECT Item FROM T6) AND
			T3.item NOT IN (SELECT Item FROM T7)
		GROUP BY T3.bucket
    """
}

Map outliersDataProcessing(List data, Map summary, String kpi) {
    if (!data) {
        return
    }

    // Groups - Top, Bottom, High, Medium, Low, Negative,
    def groupedData = data.groupBy { it.bucket }

    Map kpiBucketLists = processBucketsData(groupedData, kpi)
    Map kpiBucketSummary = processBucketsSummaryData(groupedData)
    Map indicator = Configuration.INDICATOR
    def buckets = Configuration.BUCKETS

    int highCount = groupedData.High?.itemCount?.sum() ?: 0
    int mediumCount = groupedData.Medium?.itemCount?.sum() ?: 0
    int lowCount = groupedData.Low?.itemCount?.sum() ?: 0
    int negativeCount = groupedData.Negative?.itemCount?.sum() ?: 0

    int averageItemCount = (negativeCount + lowCount + mediumCount + highCount) / 4
    return [negativeData         : [name     : "Negative",
                                    textColor: indicator.negative.textColor,
                                    itemCount: negativeCount,
                                    drilldown: kpiBucketLists.get(buckets.negative),
                                    summary  : kpiBucketSummary.get(buckets.negative)],
            lowData              : [name     : "Low",
                                    textColor: indicator.low.textColor,
                                    itemCount: lowCount,
                                    drilldown: kpiBucketLists.get(buckets.low),
                                    summary  : kpiBucketSummary.get(buckets.low)],
            mediumData           : [name     : "Medium",
                                    textColor: indicator.medium.textColor,
                                    itemCount: mediumCount,
                                    drilldown: kpiBucketLists.get(buckets.medium),
                                    summary  : kpiBucketSummary.get(buckets.medium)],
            highData             : [name     : "High",
                                    textColor: indicator.high.textColor,
                                    itemCount: highCount,
                                    drilldown: kpiBucketLists.get(buckets.high),
                                    summary  : kpiBucketSummary.get(buckets.high)],
            worstPerformanceItems: kpiBucketLists.worstPerformanceItems,
            bestPerformanceItems : kpiBucketLists.bestPerformanceItems,
            summary              : summary,
            averageItemCount     : averageItemCount]
}

List getSortedByKpi(List data, kpi) {
    return data?.sort {a, b -> a.get(kpi) <=> b.get(kpi)}
}

Map processBucketsSummaryData(Map data) {
    def buckets = Configuration.BUCKETS
    return [(buckets.high)    : transformSummaryData(data.High),
            (buckets.medium)  : transformSummaryData(data.Medium),
            (buckets.low)     : transformSummaryData(data.Low),
            (buckets.negative): transformSummaryData(data.Negative)]
}

Map transformSummaryData(List data) {
    if (!data) {
        return null
    }
    def chartFields = Configuration.CHART_FIELDS
    return [(chartFields.revenue.label)         : data.revenue.sum(),
            (chartFields.totalMargin.label)     : data.margin.sum(),
            (chartFields.marginPercentage.label): data.margin.sum() / data.revenue.sum(),
            (chartFields.volume.label)          : data.quantity.sum()]
}

Map processBucketsData(Map data, String kpi) {
    def buckets = Configuration.BUCKETS

    int productDataSetSize = getCollectionsSize(data.High, data.Medium, data.Low, data.Negative)

    List bottomDataTransformed = data?.Bottom?.collect { transformListData(it) }
    List topDataTransformed = data?.Top?.collect { transformListData(it) }

    Map limitedBestWorstPerformanceLists = getLimitedBestWorstPerformanceLists(productDataSetSize, bottomDataTransformed, topDataTransformed, kpi)

    return [(buckets.high)       : data.High?.collect { transformListData(it) },
            (buckets.medium)     : data.Medium?.collect { transformListData(it) },
            (buckets.low)        : data.Low?.collect { transformListData(it) },
            (buckets.negative)   : data.Negative?.collect { transformListData(it) },
            worstPerformanceItems: limitedBestWorstPerformanceLists.rawWorstPerformanceItems,
            bestPerformanceItems : limitedBestWorstPerformanceLists.rawBestPerformanceItems]
}

int getCollectionsSize(List... collections) {
    return collections.sum { it?.size() ?: 0 }
}

Map getLimitedBestWorstPerformanceLists(int dataSize, List bottomData, List topData, String kpi) {
    Map userInputValues = api.getElement("UserInputValues")
    int userDisplayLimit = userInputValues.noOfProductOrCustomer

    bottomData = getSortedByKpi(bottomData, kpi)
    topData = getSortedByKpi(topData, kpi)

    int dataSizeHalf = dataSize / 2.0

    return userDisplayLimit > dataSizeHalf ?
            [rawWorstPerformanceItems: bottomData?.take(dataSizeHalf)?.reverse(), rawBestPerformanceItems: topData?.take(dataSizeHalf + 1)?.reverse()] :
            [rawWorstPerformanceItems: bottomData?.reverse(), rawBestPerformanceItems: topData?.reverse()]
}

Map transformListData(Map inputData) {
    if (!inputData) {
        return null
    }
    def chartFields = Configuration.CHART_FIELDS

    return [(chartFields.name.label)                         : inputData.itemLabel,
            (chartFields.number.label)                       : inputData.item,
            (chartFields.revenue.label)                      : inputData.revenue,
            (chartFields.totalMargin.label)                  : inputData.margin,
            (chartFields.marginPercentage.label)             : inputData.marginPercent,
            (chartFields.volume.label)                       : inputData.quantity,
            (chartFields.contributionMarginPercentage.label) : inputData.marginContribution,
            (chartFields.contributionRevenuePercentage.label): inputData.revenueContribution]
}