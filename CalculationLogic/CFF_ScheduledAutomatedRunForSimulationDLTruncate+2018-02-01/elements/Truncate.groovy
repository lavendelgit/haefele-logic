String dataLoadLabel = "Price Change Simulation 2020"
String dataLoadType = "Truncate"
String dataLoadTarget = "DMDS.PriceChangeSimulation_2020"

actionBuilder
        .addDataLoadAction(dataLoadLabel, dataLoadType, dataLoadTarget)
        .setCalculate(true)