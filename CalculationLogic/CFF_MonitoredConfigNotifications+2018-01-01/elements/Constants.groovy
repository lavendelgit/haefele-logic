import groovy.transform.Field

@Field final String NOTIFICATION_MESSAGE_TABLE_OPEN = """ <html> <head>
        <style>
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }
        td, th {
        border: 1px solid #dddddd;
        text-align: center; 
        padding: 8px;
         }

        tr:nth-child(even) {
        background-color: #dddddd;
        }
        </style>
        </head>
        <body>
        <h1>Monitored Configurations Records</h1>
          <table>
          <tr>
           <th>Configuration Name</th>
           <th>Minimum Count</th>
           <th>Count of Records Received</th>
           <th>Last Updated date</th>
           <th>Update Frequency</th>
           <th>Observation</th>
         
           </tr>"""
@Field final String NOTIFICATION_MESSAGE_TABLE_CLOSE = """</table>
        </body>
        </html>"""



