import org.pojava.datetime.DateTime

Map configurationsType = out.ConfigurationMonitor?.collectEntries { def currentRow -> [(currentRow.name): currentRow.attribute1] }
List configurationNames = configurationsType.keySet() as List
Map configurationMinimumCounts = out.ConfigurationMonitor?.collectEntries { def currentRow -> [(currentRow.name): currentRow.attribute2] }
Map configurationLabels = out.ConfigurationMonitor?.collectEntries { currentRow -> [(currentRow.name): currentRow.attribute3] }
Map configurationUpdateFrequency = out.ConfigurationMonitor?.collectEntries { currentRow -> [(currentRow.name): currentRow.attribute4] }
Map configurationCounts = libs.HaefeleCommon.ConfigurationMonitor.getCountsOfRecordsReceivedForConfiguration(configurationNames,
                                                                                                             out.AnalysisDate,
                                                                                                             configurationsType,
                                                                                                             configurationUpdateFrequency)
api.trace("Configuration", configurationCounts)
String messages = ""
BigDecimal count, minimumCount, countDifference
def lastUpdateDate, updateFrequency
for (currentConfiguration in configurationNames) {
    def currentTime = out.AnalysisDate
    String oldTimeFormat = configurationCounts[currentConfiguration].LastUpdateDate
    Date lastdate = api.parseDate("yyyy-MM-dd", oldTimeFormat)
    int daysDifference
    if (lastdate && currentTime) {
        daysDifference = currentTime - lastdate
    }
    def Label = configurationLabels[currentConfiguration]
    count = configurationCounts[currentConfiguration].Count
    lastUpdateDate = configurationCounts[currentConfiguration].LastUpdateDate?.toString()?.substring(0, 10)
    minimumCount = configurationMinimumCounts[currentConfiguration]
    updateFrequency = configurationUpdateFrequency[currentConfiguration]
    countDifference = minimumCount - count
    messages += count == 0 ?
                """<tr>
                      <td style="color:brown;"><b>$Label</b></td>
                      <td>$minimumCount</td>
                     <td style="color:red;">$count</td>
                      <td>$lastUpdateDate</td>
                      <td>$updateFrequency</td>
                      <td>Last Update Received $daysDifference Days Before </td>
                     </tr>""" :
                (count < minimumCount ? """<tr>
                      <td style="color:#cccc00;"><b>$currentConfiguration</b></td>
                      <td>$minimumCount</td>
                       <td style="color:#cccc00;">$count</td>
                       <td>$lastUpdateDate</td>
                       <td>$updateFrequency</td>
                       <td>Record difference is $countDifference</td>
                       </tr>""" : "")

}
api.trace("Messages", messages)
if (messages) {

    List users = api.findLookupTableValues("NotificationSubscriptions")
                    ?.findAll { it.attribute1 == "Yes" }
                    ?.collect { it.name }
    api.trace("Users", users)
    Map email = api.find("U", 0, "email", Filter.in("loginName", users))
                   ?.collectEntries { currentRow -> [(currentRow.email): currentRow.firstName] }
    String noReply = "<i>Please do not reply to this email as it is an automated email from Pricefx</i>"
    String emailTemplate = """, <br> ${Constants.NOTIFICATION_MESSAGE_TABLE_OPEN} $messages 
                ${Constants.NOTIFICATION_MESSAGE_TABLE_CLOSE} <br>$noReply<br>Best Regards,<br>Your Pricing Team!"""
    String greetings
    String monthStartDate = libs.HaefeleCommon.ConfigurationMonitor.getTheStartDateForMonthFilter(out.AutomatedDeliveryDate).format("yyyy-MM-dd")
    String weekStartDate = libs.HaefeleCommon.ConfigurationMonitor.getTheStartDateForWeekFilter(out.AutomatedDeliveryDate).format("yyyy-MM-dd")
    def endDate = out.AnalysisDate?.format("yyyy-MM-dd")
    String subject = """Details Of Records Received According to Update Frequency Monthly Between ($monthStartDate & $endDate) and Weekly Between ($weekStartDate & $endDate)"""
    email.each {
        greetings = """Hi ${it.value}"""
        api.sendEmail(it.key, subject, greetings + emailTemplate)
        api.trace("email1", emailTemplate)
    }
}


