def result = []

def stream = api.stream("PX20", "sku" /* sort-by*/,
        ["sku","attribute7"],
        Filter.equal("name", "ManualDiscount"))

if (!stream) {
    return []
}

while (stream.hasNext()) {
    result.add(stream.next())
}

stream.close()

return result