def controller = api.newController()
def lastCalcTime = api.getElement("LastCalculationTime")
def berlinTimeZone = api.getTimeZone("Europe/Berlin")

controller.addHTML("Last Calculation Time: " + lastCalcTime?.withZone(berlinTimeZone)?.toString("yyyy-MM-dd HH:mm:ss"))

controller.addBackendCall("Calculate Manual Discount","/cfsmanager.calculate/193",null,
        "Calculation has been scheduled",
        "An error occurred")

return controller