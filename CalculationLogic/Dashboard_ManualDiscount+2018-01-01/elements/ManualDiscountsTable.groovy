import net.pricefx.common.api.FieldFormatType

def labelTranslations = [
    en: ["Article Number", "Recommended Price"],
    de: ["Art Nr.", "Empfohlener Preis (pro 100 St.)"]
]

api.logInfo("[Dashboard_ManualDiscount][ManualDiscountTable] Locale", api.getLocale())

def columnLabels = labelTranslations[api.getLocale()] ?: labelTranslations.en

def resultMatrix = api.newMatrix(columnLabels)
resultMatrix.setTitle("Manual Discount")
resultMatrix.setColumnFormat(columnLabels[1], FieldFormatType.MONEY)

def records = api.getElement("ManualDiscounts")

for (r in records) {
    resultMatrix.addRow([
            (columnLabels[0]) : r.sku,
            (columnLabels[1]) : r.attribute7 as BigDecimal
    ])
}

return resultMatrix
