def results = api.find("CFS", 0, 1, null, Filter.equal("label", "Manual Discount"))

if (results && results[0]) {
    return api.parseDateTime("yyyy-MM-dd'T'HH:mm:ss",results[0].calculationDate)
}

return null;