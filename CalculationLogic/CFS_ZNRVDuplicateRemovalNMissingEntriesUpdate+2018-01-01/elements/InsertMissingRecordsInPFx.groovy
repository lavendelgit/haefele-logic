api.global.NewPXEntries.each { record ->
    def recordToInsert = [
            "sku"       : "${record.sku}",
            "attribute1": "${record.FromDate}",
            "attribute2": "${record.ToDate}",
            "attribute3": "${record.Currency}",
            "attribute4": "${record.Price}",
            "attribute5": "${record.Source}",
            "attribute6": "100"
    ]
    String pxTableName = 'SurchargeCostPrice'
    String pxType = 'PX20'
    libs.__LIBRARY__.DBQueries.insertIntoPXTable(recordToInsert, pxTableName, pxType)
}
return (api.global.NewPXEntries.size() > 0)

