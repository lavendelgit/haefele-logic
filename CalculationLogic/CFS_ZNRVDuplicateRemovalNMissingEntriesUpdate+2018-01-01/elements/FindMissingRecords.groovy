List fieldList = ['sku', 'FromDate', 'ToDate', 'Currency', 'Price', 'Source']
String dsName = 'ToCompareSurchargeCostPrice_1'

api.global.NewPXEntries = []
def ctx = api.getDatamartContext()
def dm = ctx.getDataSource(dsName)
def query = ctx.newQuery(dm)
fieldList.each { field ->
    query.select(field)
}
def results = ctx.streamQuery(query)
def record, fromDateMap, fromDate
def printed = true
while (results?.next()) {
    record = results.get()
    if (printed) {
        libs.__LIBRARY__.TraceUtility.developmentTraceRow('Printing current DS record', record)
        printed = false
    }
    fromDateMap = api.global.currentZNRVEntries[record.sku]
    if (!fromDateMap) {
        api.global.NewPXEntries.add(record)
    } else {
        fromDate = (record.FromDate) ? fromDate = record.FromDate.format("yyyy-MM-dd") : record.FromDate
        if (!fromDateMap[fromDate]) {
            libs.__LIBRARY__.TraceUtility.developmentTrace('Printing Data Source record read', "Not found entry for $fromDate for ${record.sku}....")
            api.global.NewPXEntries.add(record)
        }
    }
}
results?.close()
return api.global.NewPXEntries.size()


/*
try {
    stream = api.stream(typeCode, "sku", fieldList, *filters)
    libs.__LIBRARY__.TraceUtility.developmentTrace('Printing Data Source record read', 'Testing....')
    def record, fromDateMap, fromDate
    def printed = true
    while (stream?.hasNext()) {
        record = stream.next()
        if (printed) {
            libs.__LIBRARY__.TraceUtility.developmentTraceRow('Printing current DS record', record)
            printed = false
        }
        fromDateMap = api.global.currentZNRVEntries[record.sku]
        if (!fromDateMap) {
            api.global.NewPXEntries.add(record)
        } else {
            fromDate = (record.FromDate) ? fromDate = record.FromDate.format("yyyy-MM-dd") : record.FromDate
            if (!fromDateMap[fromDate]) {
                libs.__LIBRARY__.TraceUtility.developmentTrace('Printing Data Source record read', "Not found entry for $fromDate for ${record.sku}....")
                api.global.NewPXEntries.add(record)
            }
        }
    }
} catch (Exception e) {
    libs.__LIBRARY__.TraceUtility.developmentTrace('Exception occurred', "Exception Details ${e.getMessage()}")
    e.printStackTrace()
}
finally {
    stream?.close()
}

 */