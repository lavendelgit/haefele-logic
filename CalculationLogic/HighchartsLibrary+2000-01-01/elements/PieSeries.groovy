/**
 * Mocks a constructor of a Pie series class that contains all pie chart specific functionality. Extends BaseSeries
 * Available methods are:
 *  - setColorPerPoint( Boolean ) - ( DEPRECATED )
 * @return Map that mocks the pie series object
 */
Map newPieSeries() {
    Map methods = [:]
    def definition = [:]

    definition.type = "pie"
    methods = [
            setColorPerPoint: { Boolean _colorByPoint ->
                definition.colorPerPoint = _colorByPoint
                return methods
            }
    ]
    methods << libs.HighchartsLibrary.BaseSeries.series(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.rawData(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.marker(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stacking(methods, definition)

    return methods
}