/**
 * Mocks a constructor of an "interface" that adds additional Axis related methods.
 * <p>Available methods are:<br>
 *  - setXAxis( Map ) - sets marker "object" that states on which X axis the data should be displayed. Starts from 0.<br>
 *  - setYAxis( Map ) - sets Id that states on which Y axis the data should be displayed. Starts from 0.<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return Map that mocks the base axis series object.
 */
Map axis(Map methods, Map definition) {
    return [
            setXAxis: { Map _xAxis ->
                definition.xAxis = _xAxis.getHighchartFormatDefinition()
                return methods
            },
            setYAxis: { Map _yAxis ->
                definition.yAxis = _yAxis.getHighchartFormatDefinition()
                return methods
            }
    ]
}

/**
 * Mocks a constructor of an "interface" that adds additional Marker related methods.
 * <p>Available methods are:<br>
 *  - setMarker( Map ) - sets the marker that will be displayed on the chart. Requires {@link HighchartsLibrary.elements.Marker} object as parameter.<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return Map that mocks the marker object.
 */
Map marker(Map methods, Map definition) {
    return [
            setMarker: { Map _marker ->
                definition.marker = _marker.getHighchartFormatDefinition()
                return methods
            }
    ]
}

/**
 * Mocks a constructor of an "interface" that adds additional stack related methods.
 * <p>Available methods are:<br>
 *  - setStack ( String ) - Allows grouping series in a stacked chart. It can be anything as long as grouped series stack options match each other<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return Map that mocks the stack object.
 */
Map stack(Map methods, Map definition) {
    return [
            setStack: { int _stack ->
                definition.stack = _stack
                return methods
            }
    ]
}

/**
 * Mocks a constructor of an "interface" that adds additional stacking related methods.
 * <p>Available methods are:<br>
 *  - setStacking( String ) - Allows to stack values of each series on top of each other. Possible values are "normal" to stack by value or "percent"<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return Map that mocks the stacking object.
 */
Map stacking(Map methods, Map definition) {
    return [
            setStacking: { String stackingMode ->
                definition.stacking = stackingMode
                return methods
            }
    ]
}

/**
 * Mocks a constructor of an "interface" that adds additional data related methods.
 * <p>Available methods are:<br>
 *  - setData ( String ) - Allows to add data for series<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return Map that mocks the rawData object.
 */
Map rawData(Map methods, Map definition) {
    return [
            setData: { List _data ->
                definition.data = _data
                return methods
            }
    ]
}
