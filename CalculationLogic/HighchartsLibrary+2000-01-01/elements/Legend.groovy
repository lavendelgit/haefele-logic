/**
 * Mocks a constructor of a "class" that holds all legend related methods.
 * Used for displaying the series legend on the chart.
 * <p>Available methods:<br>
 *  - setLayout( String ) - sets how the legend data are displayed, whether vertical, horizontal or proximity.<br>
 *  - setBorderWidth( Integer ) - sets the legend border width.<br>
 *  - setBackgroundColor( String ) - sets background color, accepts hex or plain text ('red').<br>
 *  - setFloating( Boolean ) - sets whether the legend can be on top of the chart, that way the plot area ignores the legend and can be below it.<br>
 *  - setVerticalAlign( String ) - sets the vertical alignment of the legend, can be bottom, middle or top.<br>
 *  - setXOffset( Integer ) - sets the X legend offset in px.<br>
 *  - setYOffset( Integer ) - sets the Y legend offset in px.<br>
 *  - setEnabled( Boolean ) - sets whether the legend is displayed.<br>
 *  - getHighmapsFormatDefinition() - returns the properly formatted Highcharts definition.<br>
 * @return map that mocks the legend object.
 */
Map newLegend() {
    def methods = [:]
    def definition = [:]

    methods = [
            setLayout                  : { String _layout ->
                definition.layout = _layout
                return methods
            },
            setBorderWidth             : { Integer _borderWidth ->
                definition.borderWidth = _borderWidth
                return methods
            },
            setBackgroundColor         : { String _backgroundColor ->
                definition.backgroundColor = _backgroundColor
                return methods
            },
            setFloating                : { Boolean _floating ->
                definition.floating = _floating
                return methods
            },
            setVerticalAlign           : { String _verticalAlign ->
                definition.verticalAlign = _verticalAlign
                return methods
            },
            setXOffset                 : { Integer _xOffset ->
                definition.x = _xOffset
                return methods
            },
            setYOffset                 : { Integer _yOffset ->
                definition.y = _yOffset
                return methods
            },
            setEnabled                 : { Boolean _enabled ->
                definition.enabled = _enabled
                return methods
            },
            getHighmapsFormatDefinition: {
                return definition
            }
    ]

    return methods
}