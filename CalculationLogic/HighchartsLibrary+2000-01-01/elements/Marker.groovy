/**
 * Mocks a constructor of a "class" that holds all axis related methods.
 * <p>Available methods are:<br>
 *  - setEnabled( boolean ) - Enable or disable the point marker.<br>
 *  - setEnabledThreshold( BigDecimal ) - The threshold for how dense the point markers should be before they are hidden,
 *                                        given that enabled is not defined. The number indicates the horizontal distance
 *                                        between the two closest points in the series, as multiples of the marker.radius.<br>
 *  - setFillColor ( String ) - The fill color of the point marker.<br>
 *  - setHeight ( BigDecimal ) - Image markers only. Set the image width explicitly. When using this option, a width must also be set.<br>
 *  - setLineColor ( String ) - The color of the point marker's outline.<br>
 *  - setLineWidth ( BigDecimal ) - The width of the point marker's outline.<br>
 *  - setRadius ( BigDecimal ) - The radius of the point marker.<br>
 *  - setStates ( Map ) - States for a single point marker.<br>
 *  - getHighchartFormatDefinition() - returns the properly formatted Highcharts definition.<br>
 * @return map that mocks the marker object.
 */
Map newMarker() {
    Map methods = [:]
    Map definition = [:]

    methods = [
            setEnabled                  : { boolean _enabled ->
                definition.enabled = _enabled
                return methods
            },
            setEnabledThreshold         : { BigDecimal _enabledThreshold ->
                definition.enabledThreshold = _enabledThreshold
                return methods
            },
            setFillColor                : { String _fillColor ->
                definition.fillColor = _fillColor
                return methods
            },
            setHeight                   : { BigDecimal _height ->
                definition.height = _height
                return methods
            },
            setLineColor                : { String _lineColor ->
                definition.lineColor = _lineColor
                return methods
            },
            setLineWidth                : { BigDecimal _lineWidth ->
                definition.lineWidth = _lineWidth
                return methods
            },
            setRadius                   : { BigDecimal _radius ->
                definition.radius = _radius
                return methods
            },
            setStates                   : { Map _states ->
                definition.states = _states
                return methods
            },
            setSymbol                   : { String _symbol ->
                definition.symbol = _symbol
                return methods
            },
            setWidth                    : { BigDecimal _width ->
                definition.width = _width
                return methods
            },
            getHighchartFormatDefinition: {
                return definition
            }
    ]

    return methods
}