/**
 * Mocks a constructor of a Heatmap series class.
 * @return Map that mocks the heatmap series object
 */
Map newHeatmapSeries() {
    Map methods = [:]
    def definition = [:]

    definition.type = "heatmap"
    methods << libs.HighchartsLibrary.BaseSeries.series(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.rawData(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.axis(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.marker(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stacking(methods, definition)

    return methods
}