/**
 * Prepares colors used for gradient coloring of chart with sample radial gradient config
 * @param brightnessRatio BigDecimal bigger than 0.
 * @return List of colors with sample radialGradient defined and properly adjusted stops-points
 */
List getGradientColors(BigDecimal brightnessRatio) {
    List colors = libs.HighchartsLibrary.ConstConfig.GRADIENT_COLORS.values().RGB
    return colors.collect {
        return [radialGradient: [cx: 0.5,
                                 cy: 0.3,
                                 r : 0.7],
                stops         : [[0, it],
                                 [1, adjustBrightness(it, brightnessRatio)]]]
    }
}

protected Map parseColorDefinition(String color) {
    def rgb = color.substring(4, color.size() - 1).split(",")
    int red = Integer.parseInt(rgb.getAt(0).trim())
    int green = Integer.parseInt(rgb.getAt(1).trim())
    int blue = Integer.parseInt(rgb.getAt(2).trim())

    return [r: red,
            g: green,
            b: blue]
}

protected String toColorDefinition(Map color) {
    return "rgb(${color.r},${color.g},${color.b})"
}

/**
 * Adjusts brightness of all parts of a color definition by multiplying current color code by brightness ratio.
 * If result is bigger than 255 allowed by RGB space, it is set as 255 instead.
 * @param colorDefinition String with format "rgb(r,g,b)"
 * @param brightnessRatio BigDecimal bigger than 0.
 * @return String with adjusted color definition
 */
protected String adjustBrightness(String colorDefinition, BigDecimal brightnessRatio) {
    if (brightnessRatio <= 0G) {
        return colorDefinition
    }

    Map colorMap = parseColorDefinition(colorDefinition)
    Map adjustedColorMap = colorMap.collectEntries {
        int newColorSpec = it.value * brightnessRatio
        [it.key, (newColorSpec <= 255 ? newColorSpec : 255) as int]
    }

    return toColorDefinition(adjustedColorMap)
}