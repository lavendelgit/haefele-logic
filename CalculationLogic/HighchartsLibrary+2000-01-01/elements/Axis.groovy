/**
 * Mocks a constructor of a "class" that holds all axis related methods.
 * <p>Available methods are:<br>
 *  - setTitle( String ) - sets the title that will be displayed beside the axis.<br>
 *  - setCategories( List<String> ) - sets the list of categories that will be used instead of numeric values. The number of categories has to match the number of data elements in series.<br>
 *  - setMax( BigDecimal ) - sets the maximum value that will be displayed on the axis.<br>
 *  - setMin( BigDecimal ) - sets the minimum value that will be displayed on the axis.<br>
 *  - setLabels( Map ) - sets the labels that will be displayed on axis values. Uses explicit Highchart format, more information can be found in <a href="https://api.highcharts.com/highcharts/xAxis.labels">Highchart API</a>.<br>
 *  - setOpposite( Boolean ) - sets whether the axis should be displayed on the "other" side. That is if the default is left for Y, then it will be displayed on the right.<br>
 *  - setType( String ) - sets the type of the axis. Available types are in {@link HighchartsLibrary.elements.ConstConfig} AXIS_TYPES field.<br>
 *  - setMaxPadding( BigDecimal ) - sets the max padding for the axis. This is relative to the axis length, so if axis is 100px long padding of 0.05 will make it 0.5px longer.
 *                                  Useful when user wants to avoid the highest value being on the top of the chart, to leave some margin.<br>
 *  - setPlotLines( Map... ) - sets the plot lines for the chart. These depending on the axis (X, Y) will be displayed accordingly.<br>
 *  - getHighchartFormatDefinition() - returns the properly formatted Highcharts definition.<br>
 * @return map that mocks the axis object.
 */
Map newAxis() {
  def methods = [:]
  def definition = [:]

  methods = [
      setTitle                    : { String _title ->
        definition.title = [text: _title]
        return methods
      },
      setCategories               : { List<String> _categories ->
        definition.categories = _categories
        return methods
      },
      setMax                      : { BigDecimal _max ->
        definition.max = _max
        return methods
      },
      setMin                      : { BigDecimal _min ->
        definition.min = _min
        return methods
      },
      setLabels                   : { Map _labels ->
        definition.labels = _labels
        return methods
      },
      setOpposite                 : { Boolean _isOpposite ->
        definition.opposite = _isOpposite
        return methods
      },
      setType                     : { String _type ->
        definition.type = _type
        return methods
      },
      setMaxPadding               : { BigDecimal _maxPadding ->
        definition.maxPadding = _maxPadding
        return methods
      },
      setPlotLines                : { Map... _plotLines ->
        definition.plotLines = _plotLines.collect { it.getHighchartFormatDefinition() }
        return methods
      },
      setTickInterval             : { BigDecimal _tickInterval ->
        definition.tickInterval = _tickInterval
        return methods
      },
      setTickAmount               : { BigDecimal _tickAmount ->
        definition.tickAmount = _tickAmount
        return methods
      },
      setGridLineWidth            : { BigDecimal _gridLineWidth ->
        definition.gridLineWidth = _gridLineWidth
        return methods
      },
      setAlignTicks               : { Boolean _isAlignTicks ->
        definition.alignTicks = _isAlignTicks
        return methods
      },
      getHighchartFormatDefinition: {
        return definition
      }
  ]

  return methods
}

/**
 * Used for displaying static lines on chart.
 * <p>Available methods:<br>
 *  - setLabel( Map ) - defines how the label of the plotLine should look like.
 *                      Uses explicit Highcharts format, more information can be found in <a href="https://www.highcharts.com/docs/chart-concepts/labels-and-string-formatting">Highchart API</a>.<br>
 *  - setColor( String ) - sets color of the plot line.<br>
 *  - setValue( BigDecimal ) - sets the value of the plot line.<br>
 *  - setWidth( Integer ) - sets the width of the plot line.<br>
 *  - setZIndex( Integer ) - sets the Z index of the plot line, which defines the display order.<br>
 *  - getHighchartFormatDefinition() - returns the properly formatted Highcharts definition.<br>
 * @return map that mocks the axis plotline object.
 */
Map newPlotLine() {
  def methods = [:]
  def definition = [:]

  methods = [
      setLabel                    : { Map _label ->
        definition.label = _label
        return methods
      },
      setColor                    : { String _color ->
        definition.color = _color
        return methods
      },
      setValue                    : { BigDecimal _value ->
        definition.value = _value
        return methods
      },
      setWidth                    : { Integer _width ->
        definition.width = _width
        return methods
      },
      setZIndex                   : { Integer _index ->
        definition.zIndex = _index
        return methods
      },
      getHighchartFormatDefinition: {
        return definition
      }
  ]

  return methods
}