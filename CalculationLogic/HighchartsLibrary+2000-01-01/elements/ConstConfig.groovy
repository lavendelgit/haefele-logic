import groovy.transform.Field

/**
 * Available zoom types to be used in {@link HighchartsLibrary.elements.BaseChart} setZoomType method.
 */
@Field ZOOM_TYPES = [X : 'x',
                     Y : 'y',
                     XY: 'xy']

/**
 * Available axis types to be used in {@link HighchartsLibrary.elements.Axis} setType method.
 */
@Field AXIS_TYPES = [LINEAR     : 'linear',
                     LOGARITHMIC: 'logarithmic',
                     DATETIME   : 'datetime',
                     CATEGORY   : 'category']

@Field STACKING_MODES = [NORMAL : "normal",
                         PERCENT: "percent"]

/**
 * Format that can be used in various Highchart/Highmaps contexts, for example in pointFormat of {@link HighchartsLibrary.elements.Tooltip}.<br>
 * Used for displaying <b>point.x</b> value in selected format.
 */
@Field TOOLTIP_POINT_X_FORMAT_TYPES = [ABSOLUTE_PRICE     : "{point.x:,#,###.0f}",
                                       PRICE              : "{point.x:,#,###.2f}",
                                       ABSOLUTE_PERCENTAGE: "{point.x:,#,###.0f} %",
                                       PERCENTAGE         : "{point.x:,#,###.2f} %"]

/**
 * Format that can be used in various Highchart/Highmaps contexts, for example in pointFormat of {@link HighchartsLibrary.elements.Tooltip}.<br>
 * Used for displaying <b>point.y</b> value in selected format.
 */
@Field TOOLTIP_POINT_Y_FORMAT_TYPES = [ABSOLUTE_PRICE     : "{point.y:,#,###.0f}",
                                       PRICE              : "{point.y:,#,###.2f}",
                                       ABSOLUTE_PERCENTAGE: "{point.y:,#,###.0f} %",
                                       PERCENTAGE         : "{point.y:,#,###.2f} %"]

/**
 * Available dash styles to be used in {@link HighchartsLibrary.elements.BaseSeries} setDashStyle method.
 */
@Field DASHSTYLES = [DASH           : 'Dash',
                     DASHDOT        : 'DashDot',
                     DOT            : 'Dot',
                     LONGDASH       : 'LongDash',
                     LONGDASHDOT    : 'LongDashDot',
                     LONGDASHDOTDOT : 'LongDashDotDot',
                     SHORTDASH      : 'ShortDash',
                     SHORTDASHDOT   : 'ShortDashDot',
                     SHORTDASHDOTDOT: 'ShortDashDotDot',
                     SHORTDOT       : 'ShortDot',
                     SOLID          : 'Solid']

@Field GRADIENT_COLORS = [CYAN_BLUE   : [HEX: "#7cb5ec", RGB: "rgb(124, 181, 236)"],
                          BLUE_MAGENTA: [HEX: "#434348", RGB: "rgb(67, 67, 72)"],
                          GREEN       : [HEX: "#90ed7d", RGB: "rgb(144, 237, 125)"],
                          ORANGE      : [HEX: "#f7a35c", RGB: "rgb(247, 163, 92)"],
                          BLUE        : [HEX: "#8085e9", RGB: "rgb(128, 133, 233)"],
                          PINK_RED    : [HEX: "#f15c80", RGB: "rgb(241, 92, 128)"],
                          YELLOW      : [HEX: "#e4d354", RGB: "rgb(228, 211, 84)"],
                          CYAN        : [HEX: "#2b908f", RGB: "rgb(43, 144, 143)"],
                          RED         : [HEX: "#f45b5b", RGB: "rgb(244, 91, 91)"],
                          LIGHT_CYAN  : [HEX: "#91e8e1", RGB: "rgb(145, 232, 225)"]]

@Field COLORS = [WHITE    : [HEX: "#ffffff", RGB: "rgb(255, 255, 255)"],
                 CYAN_BLUE: [HEX: "#7cb5ec", RGB: "rgb(124, 181, 236)"]]
