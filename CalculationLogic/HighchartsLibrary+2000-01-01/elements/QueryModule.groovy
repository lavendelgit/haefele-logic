import net.pricefx.formulaengine.DatamartContext

/**
 * Query definition used in this class has strictly defined structure.
 * It is as follows:
 * [
 *      datamartName      : String - name of the datamart to be queried
 *      rollup            : boolean isRollupEnabled - enables or disables the rollup functionality (adds group by)
 *      fields            : [String fieldAlias: String fieldName] - fields used in select statement,
 *                                                             fieldName can consist of aggregation functions SUM, AVG etc.
 *                                                             fieldAlias is used as "as" in select statement
 *                                                             ex. select ProductId as ID is ["ID" : "ProductID"]
 *      whereFilters      : [Filter... ] - regular Filter objects can be passed to be used in where statement
 *                                    ex. where ProductId = "ABC" is Filter.equal("ProductId", "ABC")
 *      havingFilters     : [Filter... ] - regular Filter objects can be passed to be used in having statement
 *                                    ex. having SumQuantity > 1000 is Filter.greaterThan("SumQty", 1000)
 *      orderBy           : String - adds one or more statement into order by, sorting direction ASC/DESC
 *                              ex. BusinessUnit ASC or SumQuantity DESC
 *      maxRows           : Integer - sets maximum amount of retrieved rows for the query
 *      targetCurrencyCode: String - defines the currency in which the data should be fetched, uses CurrencyCodes stored in "ccy" DS (by default),
 *                               requires the datamart to be properly configured.
 * ]
 *
 * Usage example:
 * def queryDef = [datamartName : "Sales_Data",
 *                 rollup       : true,
 *                 fields       : ["BusinessUnit" : "BusinessUnit",
 *                                 "SumQuantity"  : "SUM(Quantity)"],
 *                 havingFilters: Filter.greaterThan("SumQuantity", 1150000),
 *                 orderBy: "BusinessUnit ASC"]
 *
 * def results = queryDatamart(queryDef)
 *
 *
 */

/**
 * Builds DatamartContext.Query definition from provided query definition map.
 * @param datamartCtx current datamart context.
 * @param queryDefinition properly formatted map containing query definition.
 * @return DatamartContext.Query object with fields filled from provided query definition.
 */
def buildDatamartQueryFromDefinition(DatamartContext datamartCtx, Map queryDefinition) {
    if (!queryDefinition.datamartName) {
        api.throwException("Datamart name not provided")
    }

    def rollup = queryDefinition.rollup ?: false
    def query = datamartCtx.newQuery(datamartCtx.getDatamart(queryDefinition.datamartName), rollup)

    return addQueryDefinitionFields(query, queryDefinition)
}

/**
 * Builds DatamartContext.Query definition from provided query definition map.
 * @param queryDefinition properly formatted map containing query definition.
 * @return DatamartContext.Query object with fields filled from provided query definition.
 */
def buildDatamartQueryFromDefinition(Map queryDefinition) {
    def datamartCtx =  api.getDatamartContext()
    return buildDatamartQueryFromDefinition(datamartCtx, queryDefinition)
}

/**
 * Builds DatamartContext.Query for datamarts from base DatamartContext.Query and query definition
 * @param datamartCtx - current datamart context
 * @param baseQuery
 * @param queryDefinition
 * @return New DatamartContext.Query with additional data from query definition
 */
def buildDatamartQueryFromBaseQuery(DatamartContext datamartCtx, DatamartContext.Query baseQuery, Map queryDefinition) {
    if (!baseQuery) {
        api.throwException("BaseQuery not provided")
    }

    def query = datamartCtx.newQuery(baseQuery)

    return addQueryDefinitionFields(query, queryDefinition)
}

/**
 * Builds DatamartContext.Query for datasources definition from provided query definition map.
 * @param datamartCtx - current datamart context
 * @param queryDefinition
 * @return DatamartContext.Query object with fields filled from provided query definition
 */
def buildDatasourceQueryFromDefinition(DatamartContext datamartCtx, Map queryDefinition) {
    if (!queryDefinition.datamartName) {
        api.throwException("Datamart name not provided")
    }

    def rollup = queryDefinition.rollup ?: false
    def query = datamartCtx.newQuery(datamartCtx.getDataSource(queryDefinition.datamartName), rollup)

    return addQueryDefinitionFields(query, queryDefinition)
}

/**
 * Adds "fields", "where filters", "having filters", "order by" and "max row" settings (if present) to a DatamartContext.Query object
 * @param query - DatamartContext.Query object to be appended with fields from query definition
 * @param queryDefinition
 * @return DatamartContext.Query object with new data
 */
protected def addQueryDefinitionFields(DatamartContext.Query query, Map queryDefinition) {
    if (queryDefinition.fields) {
        queryDefinition.fields?.each { _key, _value ->
            if (_key && _value) {
                query.select(_value, _key)
            }
        }
    }

    if (queryDefinition.whereFilters) {
        queryDefinition.whereFilters.each { _filter ->
            if (_filter) {
                query.where(_filter)
            }
        }
    }

    if (queryDefinition.havingFilters) {
        queryDefinition.havingFilters.each { _filter ->
            if (_filter) {
                query.where(_filter)
            }
        }
    }

    if (queryDefinition.orderBy) {
        query.orderBy(queryDefinition.orderBy)
    }

    if (queryDefinition.maxRows) {
        query.setMaxRows(queryDefinition.maxRows)
    }

    if (queryDefinition.targetCurrencyCode) {
        query.setOptions([currency: queryDefinition.targetCurrencyCode])
    }

    return query
}

/**
 * Queries datamart with the provided field, filter, sorting data etc.
 * @param queryDefinition map containing the definition of query data.
 * @return list containing Matrix2D records retrieved from the DM or null.
 */
def queryDatamart(Map queryDefinition) {
    def dmCtx = api.getDatamartContext()
    def buildQuery = buildDatamartQueryFromDefinition(dmCtx, queryDefinition)
    def queryResult = dmCtx.executeQuery(buildQuery)

    return queryResult?.getData()
            ?.collect()
}

/**
 * Queries datamart with the provided field, filter, sorting data etc.
 * @param SQLQuery SQL Query
 * @param queries list of queries to be used in SQL query as source
 * @return list containing Matrix2D records retrieved from the DM or null.
 */
def runSQLQuery(SQLQuery, List queries) {
    def dmCtx = api.getDatamartContext()
    return dmCtx.executeSqlQuery(SQLQuery, *queries)
}
/**
 * Queries datamart with the provided field, filter, sorting data etc.
 * @param queryDefinition - map containing the definition of query data
 * @return List containing Matrix2D records retrieved from the DM or null
 */
def queryDatasource(Map queryDefinition) {
    def dmCtx = api.getDatamartContext()
    def buildQuery = buildDatasourceQueryFromDefinition(dmCtx, queryDefinition)
    def queryResult = dmCtx.executeQuery(buildQuery)

    return queryResult?.getData()
            ?.collect()
}

/**
 * Creates in memory tables based on base query definition and additional per table definitions
 * Fields DM name and rollup can be omitted for table definitions
 * Uses Query Definition format
 * All created tables are named TX, where X is the number (starting from 1) of the table created
 * @param baseQueryDefinition
 * @param inMemoryTableDefinitions
 * @return
 */
void createInMemoryTables(Map baseQueryDefinition, Map... inMemoryTableDefinitions) {
    def tblCtx = api.getTableContext()
    def dmCtx = api.getDatamartContext()
    def baseQuery = buildDatamartQueryFromDefinition(dmCtx, baseQueryDefinition)
    def initialTableCount = tblCtx.getTables().size()

    inMemoryTableDefinitions.eachWithIndex { _queryDefinition, _index ->
        def query = buildDatamartQueryFromBaseQuery(dmCtx, baseQuery, _queryDefinition)
        tblCtx.createTable("T${initialTableCount + _index + 1}", dmCtx.executeQuery(query))
    }
}

/**
 * Creates in memory tables based on base query definition and data slices for per table definitions.
 * Uses Query Definition format.
 * All created tables are named TX, where X is the number (starting from 1) of the table created.
 * @param baseQueryDefinition query definition containing all common elements between the in memory tables.
 * @param inMemoryTableDefinitions additional per table data slice definitions.
 */
void createInMemoryTables(Map baseQueryDefinition, DatamartContext.DataSlice... inMemoryTableDefinitions) {
    def tblCtx = api.getTableContext()
    def dmCtx = api.getDatamartContext()
    def initialTableCount = tblCtx.getTables().size()

    inMemoryTableDefinitions.eachWithIndex { _dataSlice, _index ->
        //TODO: If there will be a way to deep copy the baseQueryDefinition it should be applied here in order to append
        // whereFilters, instead of adding the dataSlice after query creation.
        def query = buildDatamartQueryFromDefinition(dmCtx, baseQueryDefinition)
        query.where(_dataSlice)
        tblCtx.createTable("T${initialTableCount + _index + 1}", dmCtx.executeQuery(query))
    }
}

/**
 * Creates in memory tables based on provided table definitions.
 * Requires full information per table definition.
 * Uses Query Definition format.
 * All created tables are named TX, where X is the number (starting from 1) of the table created.
 * @param inMemoryTableDefinitions query definitions for memory tables.
 */
void createInMemoryTables(Map... inMemoryTableDefinitions) {
    def tblCtx = api.getTableContext()
    def dmCtx = api.getDatamartContext()
    def initialTableCount = tblCtx.getTables().size()

    inMemoryTableDefinitions.eachWithIndex { _queryDefinition, _index ->
        def query = buildDatamartQueryFromDefinition(dmCtx, _queryDefinition)
        tblCtx.createTable("T${initialTableCount + _index + 1}", dmCtx.executeQuery(query))
    }
}

/**
 * Queries previously created in memory table.
 * @param rawSqlStatement raw SQL statement for querying the data, requires FROM TX format.
 * @return results of the SQL statement.
 */
def queryInMemoryTable(String rawSqlStatement) {
    def tblCtx = api.getTableContext()

    return tblCtx.executeQuery(rawSqlStatement).collect()
}

/**
 * Gets dimFilterEntryData and returns a default value based on provided query definition.
 * @param dmColumnName column name from which data should be retrieved.
 * @param queryDefinition parameter defining how to retrieve default value.
 * @return map containing the column data from DM and default value if present.
 */
def getDimFilterEntry(DatamartContext dmContext, String dmColumnName, Map queryDefinition) {
    if (!queryDefinition.datamartName) {
        api.throwException("Datamart name not provided")
    }

    def dm = dmContext.getDatamart(queryDefinition.datamartName)
    def columnData = dm.getColumn(dmColumnName)
    def queryFromDefinition = buildDatamartQueryFromDefinition(dmContext, queryDefinition)
    def queryRes = dmContext.executeQuery(queryFromDefinition)

    return [columnData: columnData, defaultValue: queryRes?.getData()?.collect()]
}