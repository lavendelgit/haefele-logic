/**
 * Mocks a constructor of a Bar series class that contains all bar chart specific functionality. Extends BaseAxisSeries
 * @param isHorizontal - states whether the chart bars should be horizontal (default) or vertical
 * @return Map that mocks the bar chart series object
 */
Map newColumnSeries() {
    Map methods = [:]
    def definition = [:]

    definition.type = "column"
    methods = [
            setHorizontal: { Boolean _isHorizontal ->
                definition.type = getBarChartType(_isHorizontal)
                return methods
            }
    ]
    methods << libs.HighchartsLibrary.BaseSeries.series(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.rawData(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.axis(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stack(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stacking(methods, definition)

    return methods
}

/**
 * Gets the type of the chart depending on the parameters.
 * @param isHorizontal states whether the chart bars should be horizontal (default) or vertical.
 * @return type of the chart to be used in the definition.
 */
protected String getBarChartType(boolean isHorizontal) {
    return isHorizontal ? "bar" : "column"
}