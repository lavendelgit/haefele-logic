/**
 * Mocks a constructor of an "abstract class" that holds all similar features between chart types.
 * <p>Available methods are:<br>
 * - setTitle( String ) - sets the title that is displayed on top of the chart.<br>
 * - setSubtitle( String ) - sets the subtitle that is displayed below the main chart title.<br>
 * - setEnableMouseWheelZoomMapNavigation( boolean ) - sets whether the user can use mouse wheel to zoom in/out the chart.<br>
 * - setCaption( String ) - sets the chart caption that is displayed below the chart.<br>
 * - setZoomType( String ) - sets whether the chart can be zoomed on X/Y or XY axes. Values available are stored in {@link HighchartsLibrary.elements.ConstConfig} ZOOM_TYPE field.<br>
 * - setSeries( Map... ) - sets the series to be displayed on a chart. Requires objects deriving from {@link HighchartsLibrary.elements.BaseSeries} as parameters.<br>
 * - setDrilldownSeries( Map... ) - sets the drilldown series to be displayed on a chart additionally to the main chart series.
 *                                  In order to make drilldown work the drilldownSeries and mainSeries need to have drilldownId set up.<br>
 * - setPlotOptions( Map ) - sets additional plot options for the chart. Explicit format, more information can be found in <a href="https://api.highcharts.com/highcharts/plotOptions">Highchart API</a>.<br>
 * - setTooltip( Map ) - sets tooltip that will be display on all chart series unless overridden in series objects. Requires {@link HighchartsLibrary.elements.Tooltip} object as parameter.<br>
 * - setLegend( Map ) - sets the legend describing series data that will be displayed on the chart. Requires {@link HighchartsLibrary.elements.Legend} object as parameter.<br>
 * - getHighchartFormatDefinition() - returns the properly formatted Highcharts definition to be fed into api.buildHighchart() call.<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return map that mocks the base chart object.
 */
protected Map newChart() {
    Map methods = [:]
    Map definition = [:]

    definition.credits = [enabled: false]
    methods += [
            setTitle                            : { String _title ->
                definition.title = [text: _title]
                return methods
            },
            setSubtitle                         : { String _subTitle ->
                definition.subtitle = [text: _subTitle]
                return methods
            },
            setEnableMouseWheelZoomMapNavigation: { Boolean _enabled ->
                definition.mapNavigation = [enableMouseWheelZoom: _enabled]
                return methods
            },
            setCaption                          : { String _caption ->
                definition.caption = [text: _caption]
                return methods
            },
            setColorAxis                        : { Map _colorAxis ->
                definition.colorAxis = _colorAxis.getHighmapsFormatDefinition()
                return methods
            },
            setZoomType                         : { String _zoomType ->
                definition.chart = [zoomType: _zoomType]
                return methods
            },
            setSeries                           : { Map... _series ->
                List series = _series.collect { it.getHighchartFormatDefinition() }
                definition << getSeriesAxes(series, "xAxis")
                definition << getSeriesAxes(series, "yAxis")
                series = setAxesIds(series, "xAxis")
                series = setAxesIds(series, "yAxis")
                definition.series = series
                return methods
            },
            setDrilldownSeries                  : { Map... _drilldownSeries ->
                definition.drilldown = definition.drilldown ?: [:]
                definition.drilldown.series = _drilldownSeries.collect { it.getHighchartFormatDefinition() }
                return methods
            },
            setPlotOptions                      : { Map _plotOptions ->
                definition.plotOptions = _plotOptions
                return methods
            },
            setTooltip                          : { Map _tooltip ->
                definition.tooltip = _tooltip.getHighchartFormatDefinition()
                return methods
            },
            setLegend                           : { Map _legend ->
                definition.legend = _legend.getHighmapsFormatDefinition()
                return methods
            },
            getHighchartFormatDefinition        : {
                return definition
            }
    ]

    return methods
}

protected Map getSeriesAxes(List series, String axis) {
    Set seriesAxes = series.collect { it.getAt(axis) }.toSet()

    return (seriesAxes.getAt(0)) ? [(axis): seriesAxes] : [:]
}

protected List setAxesIds(List series, String axis) {
    Map axesMap = getAxesIndexesMap(series, axis)

    return series.each {
        it << [(axis): axesMap.getAt(it.getAt(axis))]
    }
}

protected Map getAxesIndexesMap(List series, String axes) {
    Set seriesAxes = series.collect { it.getAt(axes) }.toSet()
    Map axesMap = [:]
    seriesAxes.eachWithIndex { axis, i ->
        axesMap << [(axis): i]
    }

    return axesMap
}