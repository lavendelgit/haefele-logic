/**
 * Mocks a constructor of a "class" that holds all data label related methods.
 * Used for various data labels displayed in the chart, for example on series.
 * <p>Available methods are:<br>
 *  - setEnabled( boolean ) - sets whether the data lables are enabled for the object.<br>
 *  - setFormat( String ) - sets format for displaying the data.
 *                          Explicit format, more information can be found in <a href="https://www.highcharts.com/docs/chart-concepts/labels-and-string-formatting">Highchart API</a>.<br>
 *  - setColor( String ) - sets the color for the data label.<br>
 *  - setFontSize( String ) - sets the font size in px. For example 11px.<br>
 *  - setFontWeight( String ) - sets the font weight. For example bold.<br>
 *  - setTextOutline( String ) - sets the font text outline, used to make text more visible.<br>
 *  - getHighmapsFormatDefinition() - returns the properly formatted Highcharts definition.<br>
 * @return map that mocks the data label object
 */
Map newDataLabel() {
    def methods = [:]
    def definition = [:]

    methods = [
            setEnabled                  : { Boolean _isEnabled ->
                definition.enabled = _isEnabled
                return methods
            },
            setFormat                   : { String _format ->
                definition.format = _format
                return methods
            },
            setColor                    : { String _color ->
                definition.style = definition.style ?: [:]
                definition.style.color = _color
                return methods
            },
            setFontSize                 : { String _fontSize ->
                definition.style = definition.style ?: [:]
                definition.style.fontSize = _fontSize
                return methods
            },
            setFontWeight               : { String _fontWeight ->
                definition.style = definition.style ?: [:]
                definition.style.fontWeight = _fontWeight
                return methods
            },
            setTextOutline              : { String _textOutline ->
                definition.style = definition.style ?: [:]
                definition.style.textOutline = _textOutline
                return methods
            },
            getHighchartFormatDefinition: {
                return definition
            }
    ]

    return methods
}