/**
 * Mocks a constructor of an "abstract class" that holds all similar features between series types.
 * <p>Available methods:<br>
 *  - setName( String ) - sets the name of the series, not displayed anywhere.<br>
 *  - setId( String ) - sets the Id of the series, used in drilldown.<br>
 *  - setIndex( Integer ) - sets the Z index of the chart as well as order in legend.<br>
 *  - setZIndex( Integer ) - sets the Z index of the chart, this states which series appears in front and which in the back.<br>
 *  - setDashStyle ( String ) - sets a name of the dash style to use for the graph, or for some series types the outline of each shape.<br>
 *  - setPrefix( String ) - ( DEPRECATED )<br>
 *  - setSuffix( String ) - ( DEPRECATED )<br>
 *  - setData( List ) - sets the data to be displayed on chart. Can contain various objects - simple numbers or maps.<br>
 *  - setDataLabels( Map ) - sets the data labels that will be displayed on the series. Requires {@link HighchartsLibrary.elements.DataLabel} object as parameter.<br>
 *  - setTooltip( Map ) - sets the tooltip that will be displayed when hovering over series values.
 *                        Requires {@link HighchartsLibrary.elements.Tooltip} object as parameter. Can be used to handle data in Map format.<br>
 *  - setCustomFieldDefinition( fieldName, fieldData ) - can be used to set custom property on the series object. Can be used to display different data on tooltips or data labels.<br>
 *  - getHighchartFormatDefinition() - returns the properly formatted highcharts definition.<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return Map that mocks the base series object.
 */
Map series(Map methods, Map definition) {
    return [
            setName                     : { String _name ->
                definition.name = _name
                return methods
            },
            setId                       : { String _id ->
                definition.id = _id
                return methods
            },
            setIndex                    : { Integer _index ->
                definition.index = _index
                return methods
            },
            setZIndex                   : { Integer _zIndex ->
                definition.zIndex = _zIndex
                return methods
            },
            setColor                    : { String _color ->
                definition.color = _color
                return methods
            },
            setDashStyle                : { String _dashStyle ->
                definition.dashStyle = _dashStyle
                return methods
            },
            setPrefix                   : { String _prefix ->
                definition.prefix = _prefix
                return methods
            },
            setSuffix                   : { String _suffix ->
                definition.suffix = _suffix
                return methods
            },
            setDataLabels               : { Map _dataLabels ->
                definition.dataLabels = _dataLabels.getHighchartFormatDefinition()
                return methods
            },
            setTooltip                  : { Map _tooltip ->
                definition.tooltip = _tooltip.getHighchartFormatDefinition()
                return methods
            },
            setCustomFieldDefinition    : { fieldName, fieldData ->
                definition[(fieldName)] = fieldData
                return methods
            },
            getHighchartFormatDefinition: {
                return definition
            }
    ]
}