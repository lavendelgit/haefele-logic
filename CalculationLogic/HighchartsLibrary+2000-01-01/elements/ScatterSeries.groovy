/**
 * Mocks a constructor of a Scatter series class that contains all scatter chart specific functionality. Extends BaseAxisSeries
 * @return Map that mocks the scatter series object
 */
Map newScatterSeries() {
    Map methods = [:]
    def definition = [:]

    definition.type = "scatter"
    methods << libs.HighchartsLibrary.BaseSeries.series(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.rawData(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.axis(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.marker(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stack(methods, definition)

    return methods
}