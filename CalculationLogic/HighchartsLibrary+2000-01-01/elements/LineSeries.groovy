/**
 * Mocks a constructor of a Line series class that contains all line chart specific functionality. Extends BaseAxisSeries
 * @return Map that mocks the line series object
 */
Map newLineSeries() {
    Map methods = [:]
    def definition = [:]

    definition.type = "line"
    methods << libs.HighchartsLibrary.BaseSeries.series(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.rawData(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.axis(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.marker(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stack(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stacking(methods, definition)

    return methods
}