/**
 * Mocks a constructor of a Waterfall series class that contains all waterfall chart specific functionality. Extends BaseAxisSeries
 * @return Map that mocks the waterfall series object
 */
Map newWaterfallSeries() {
    Map methods = [:]
    def definition = [:]

    definition.type = "waterfall"
    definition.color = "#910000"
    definition.upColor = "#8bbc21"
    methods = [
            setUpColor: { String _upColor ->
                definition.upColor = _upColor
                return methods
            }
    ]
    methods << libs.HighchartsLibrary.BaseSeries.series(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.rawData(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.axis(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.marker(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stack(methods, definition)

    return methods
}