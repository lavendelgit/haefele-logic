/**
 * Mocks a constructor of a "class" that holds all color axis related methods.
 * Used for displaying an color axis on top of the chart (by default)
 * <p>Available methods are:<br>
 * - setType( String ) - sets the color axis type for the map. This is to visualize the colors of different region based on some dimension.
 *                       Displayed as a "legend-like" box. Data taken from {@link HighmapsLibrary.elements.ConstConfig} AXIS_TYPE, more information in <a href="https://api.highcharts.com/highmaps/colorAxis">Highchart API</a>.<br>
 * - setLabels( Map ) -  sets the labels that will be displayed on axis values. Uses explicit Highchart format, more information can be found in <a href="https://api.highcharts.com/highcharts/xAxis.labels">Highchart API</a>.<br>
 * @return map that mocks the colorAxis object.
 */
Map newColorAxis() {
    def methods = [:]
    def definition = [:]

    methods = [
            setType                    : { String _type ->
                definition.type = _type
                return methods
            },
            setLabels                  : { Map _labels ->
                definition.labels = _labels
                return methods
            },
            setMinColor                : { String _minColor ->
                definition.minColor = _minColor
                return methods
            },
            setMaxColor                : { String _maxColor ->
                definition.maxColor = _maxColor
                return methods
            },
            getHighmapsFormatDefinition: {
                return definition
            }
    ]

    return methods
}