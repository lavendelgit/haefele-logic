/**
 * Mocks a constructor of a Spline series class that contains all spline chart specific functionality. Extends BaseAxisSeries
 * @return Map that mocks the spline series object
 */
Map newSplineSeries() {
    Map methods = [:]
    def definition = [:]

    definition.type = "spline"
    methods << libs.HighchartsLibrary.BaseSeries.series(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.rawData(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.axis(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.marker(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.stack(methods, definition)

    return methods
}