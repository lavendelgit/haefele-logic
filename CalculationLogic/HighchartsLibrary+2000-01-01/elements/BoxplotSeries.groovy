/**
 * Mocks a constructor of a Boxplot series class that contains all line chart specific functionality. Extends BaseAxisSeries
 * @return Map that mocks the line series object
 */
Map newBoxplotSeries() {
    Map methods = [:]
    def definition = [:]

    definition.type = "boxplot"
    methods << libs.HighchartsLibrary.BaseSeries.series(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.rawData(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.axis(methods, definition)
    methods << libs.HighchartsLibrary.Interfaces.marker(methods, definition)

    return methods
}