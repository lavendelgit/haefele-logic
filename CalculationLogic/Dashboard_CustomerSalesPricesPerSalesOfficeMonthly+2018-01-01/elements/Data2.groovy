if (api.isSyntaxCheck()) {
    return []
}

def ctx = api.getDatamartContext()

def months = api.getElement("Months")
def sources = api.getElement("Sources")
def customerSector = api.getElement("CustomerSector")
//def visibleSalesOffices = api.getElement("VisibleSalesOffices") as Set
Filter salesOfficeFilter = libs.__LIBRARY__.HaefeleSpecific.getSalesOfficeInlandFilters('Verkaufsbereich3')

def dm1 = ctx.getDatamart("CustomerSalesPricesDM")
def q1 = ctx.newQuery(dm1)
q1.select("SourceID", "sourceId")
q1.select("Material")
q1.select("CustomerId")
q1.select("ValidFrom", "validFrom")
q1.select("ValidTo", "validTo")
q1.select("Verkaufsbereich3", "salesOffice")
q1.where(salesOfficeFilter)

if (customerSector) {
    q1.where(Filter.in("Branche", customerSector))
}

def sql = """
    SELECT m.month, t1.salesOffice, COUNT(t1.material) FROM (
        VALUES ${months.collect{ m -> "('" + m + "')"}.join(", ")}
    ) m (month)
    CROSS JOIN T1
    WHERE t1.validFrom <= CAST(m.month as DATE)
      AND t1.validTo >= CAST(m.month as DATE)
      AND t1.sourceId IN (${sources.collect { s -> "'" + s + "'"}.join(", ")}) 
    GROUP BY m.month, t1.salesOffice
    ORDER By m.month, count DESC
"""

api.trace("sql", "", sql)

def result = ctx.executeSqlQuery(sql, q1)
    ?.collect { r ->
        [
            month: r.month,
            salesOffice: r.salesoffice ?: "Undefined",
            count: r.count
        ]
    }
   // ?.findAll { r -> visibleSalesOffices.contains(r.salesOffice) }

api.trace("result", "", result)

return result
