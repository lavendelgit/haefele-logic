def resultMatrix = api.newMatrix("Sales Office", "Month", "Count of ZPAPs")
resultMatrix.setTitle("Count of ZPAPs per Sales Office")
resultMatrix.setColumnFormat("Count of ZPAPs", FieldFormatType.NUMERIC)
resultMatrix.setPreferenceName("CountOfCustomerSalesPricesPerSalesOfficePerMonth")
resultMatrix.setEnableClientFilter(true)

def data = api.getElement("Data2")

def total = 0

for (row in data) {
    resultMatrix.addRow([
            "Sales Office"  : row.salesOffice,
            "Month"         : row.month,
            "Count of ZPAPs": row.count
    ])
    total += row.count
}
resultMatrix.addRow([
        "Sales Office"  : resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of ZPAPs": resultMatrix.styledCell(total).withCSS("font-weight: bold")
])

return resultMatrix