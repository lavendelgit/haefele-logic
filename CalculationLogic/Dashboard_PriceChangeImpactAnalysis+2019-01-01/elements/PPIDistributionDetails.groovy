def effectiveYear = api.local.firstYear
def effectiveYearParam = "Effective Year"

return api.dashboard("PPIDistribution")
        .setParam(effectiveYearParam, effectiveYear)
        .showEmbedded()
        .andRecalculateOn(api.dashboardWideEvent("PPIDistribution-ED"))
        .withEventDataAttr("EffectiveYear").asParam(effectiveYearParam)