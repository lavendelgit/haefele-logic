import net.pricefx.common.api.FieldFormatType

def getRange(String year) {
    def currentDate = api.targetDate()
    def currentYear = currentDate?.format("yyyy")
    def cal = api.calendar()
    def maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
    def month = currentDate?.format("MM")
    if (year == currentYear) {
        return "01/01/$year to $maxDay/$month/$year"
    }
    return "01/01/$year to 31/12/$year"
}

def columns = ['Analysis Year', 'Data Range for Price Change & Transactions',
               'Total Articles with Price Change', 'Impacted Number Of Articles',
               'Net Loss',
               'Estimated Scope of Improvement',//'Estimated Loss to Business',
               'Estimated Loss Recovery', 'Computation Date']
def columnTypes = [FieldFormatType.INTEGER, FieldFormatType.TEXT,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.MONEY_EUR,
                   FieldFormatType.MONEY_EUR,
                   FieldFormatType.MONEY_EUR, FieldFormatType.DATE]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Summary: Business Impact', columns, columnTypes)

def data = out.SummaryData
def totalImpactColor, recoveryColor, lossValue, totalImpact, analysisYear
final String GREEN = "#2e7d10", RED = "#f44340"
data.each { entry ->
    analysisYear = entry['Year']?.format("yyyy")
    totalImpact = entry['OverallImpact']?:0
    totalImpactColor = (totalImpact >= 0) ? GREEN : RED
    totalImpact = (totalImpact < 0) ? totalImpact * -1 : totalImpact
    lossValue = entry['LossToBusiness']?:0
    lossValue = (lossValue < 0) ? lossValue * -1 : lossValue
    recoveryColor = (entry['LossRecovered'] >= 0) ? GREEN : RED

    resultMatrix.addRow([
            (columns[0]): analysisYear,
            (columns[1]): getRange(analysisYear),
            (columns[2]): entry['TotalArticles'],
            (columns[3]): entry['NumberOfArticles'],
            (columns[4]): dashUtl.boldHighlightWith(resultMatrix, totalImpact, totalImpactColor),
            (columns[5]): dashUtl.boldHighlightWith(resultMatrix, lossValue, RED),
            (columns[6]): dashUtl.boldHighlightWith(resultMatrix, entry['LossRecovered'], recoveryColor),
            (columns[7]): dashUtl.boldHighlightWith(resultMatrix, entry['LastRunDate'])
    ])
}
resultMatrix.setPreferenceName("Summary Price Changes Impact")
resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("PPIDistribution-ED"))
        .withColValueAsEventDataAttr(columns[0], "EffectiveYear")

return resultMatrix
