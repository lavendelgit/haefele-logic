def summaryFilters = [Filter.equal("key1", 'PriceChangeImpact')]
def summaryColumns = ['key3', 'attribute1', 'attribute2', 'attribute3',
                      'attribute4', 'attribute5', 'attribute6', 'attribute7', 'attribute8']
def columnsMeaning = ['Year', 'Range', 'TotalImpact', 'LossToBusiness',
                      'LossRecovery', 'NoOfMaterialForPriceChange', 'SkippedMaterial', 'ComputationDate']
def summaryValues = api.findLookupTableValues("PreCalculatedDashboardField", *summaryFilters).collectEntries {
    [(it[summaryColumns[0]]): (it)]
}


def dbFields = ['Year', 'NumberOfArticles', 'LossToBusiness', 'LossRecovered',
                'OverallImpact', 'TotalTransactions', 'TransactionsParticipated', 'TotalTransactedUnits',
                'TransactionUnitsParticipated', 'TotalArticles', 'LastRunDate']
def fields = ['YEAR'                        : dbFields[0],
              'COUNT(Material)'             : dbFields[1],
              'SUM(PurchasePriceChangeLoss)': dbFields[2],
              'SUM(SalesPriceChangeGain)'   : dbFields[3],
              'SUM(TotalPriceChangeImpact)' : dbFields[4],
              'SUM(TotalTransactions)'      : dbFields[5],
              'SUM(RelevantTransactions)'   : dbFields[6],
              'SUM(TotalTransactedUnits)'   : dbFields[7],
              'SUM(RelevantTransactedUnits)': dbFields[8]
]
def orderByField = ['YEAR']
def filters = []

def dataRows = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'PurchasePriceInefficiencyRecordsDM', filters, orderByField, 'DataMart')
def firstRecordDate = (dataRows.size() > 0) ? dataRows[0][dbFields[0]] : ''
api.local.firstYear = (firstRecordDate instanceof Date) ? firstRecordDate?.format("yyyy") : ''
def year
def curSummaryValue
libs.__LIBRARY__.TraceUtility.developmentTrace('Merged summary data', summaryValues)

def outputData = []
dataRows.each { entry ->
    year = entry[dbFields[0]]?.format("yyyy")
    curSummaryValue = summaryValues[year]
    outputData.add([
            (dbFields[0]) : entry[dbFields[0]],
            (dbFields[1]) : entry[dbFields[1]],
            (dbFields[2]) : entry[dbFields[2]],
            (dbFields[3]) : entry[dbFields[3]],
            (dbFields[4]) : entry[dbFields[4]],
            (dbFields[5]) : entry[dbFields[5]],
            (dbFields[6]) : entry[dbFields[6]],
            (dbFields[7]) : entry[dbFields[7]],
            (dbFields[8]) : entry[dbFields[8]],
            (dbFields[9]) : (curSummaryValue) ? curSummaryValue[summaryColumns[8]]:0,
            (dbFields[10]): (curSummaryValue) ? curSummaryValue[summaryColumns[7]]:''
    ])
}

libs.__LIBRARY__.TraceUtility.developmentTraceRow('Merged summary data', outputData)
return outputData