def sku = api.product("sku")
def dmCtx = api.getDatamartContext()
def table = dmCtx.getTable("TransactionsDM")
def query = dmCtx.newQuery(table)

def currentYear = api.calendar().get(Calendar.YEAR);
def previousYear = currentYear - 1

def filters = [
    Filter.equal("Material", sku),
    Filter.greaterThan("Units", 0),
    Filter.greaterThan("Revenue", 0),
    Filter.equal("InvoiceDateYear", previousYear)
]

query.select("AVG(SalesPricePer100Units)", "price")
query.select("SUM(Units)", "quantity")
query.select("SUM(Revenue)", "revenue")
query.select("InvoiceId", "invoiceId")
query.select("InvoicePos", "invoicePos")
query.where(*filters)
query.orderBy("price")
/*
query.setOptions([
    regression: ["quantity", "price"] // y, x
])
*/
def result = dmCtx.executeQuery(query)
result?.calculateSummaryStatistics()

def summary = result?.getSummary()

//def lra = summary?.projections?.quantity?.LRa
//def lrb = summary?.projections?.price?.LRb

api.trace("summary", "", summary)

def ols = result?.getData()
    ?.linearRegression()
    ?.usingDependentVariable("quantity")
    ?.usingIndependentVariables(["price"])
    ?.run()


def lra = ols?.intercept;
def lrb = ols?.coefficients?.toList()?.first()

api.trace("ols", "", ols)
api.trace("R2", "", ols?.r2)
api.trace("P-value", "", ols?.pvalue)
api.trace("intercept", "", ols?.intercept)
api.trace("coefficients", "", ols?.coefficients?.toList())
api.trace("t-test", "", ols?.ttest?.toList())
api.trace("result", "", ols?.getResult())

def nanToNull(value) {
    return value?.toString() == "NaN" ? null : value
}

return [
    count: summary?.Count,
    minPrice: nanToNull(summary?.projections?.price?.Min),
    maxPrice: nanToNull(summary?.projections?.price?.Max),
    avgPrice: nanToNull(summary?.projections?.price?.Avg),
    lra: lra ?: 0.0,
    lrb: lrb ?: 0.0,
    ols: ols
]
