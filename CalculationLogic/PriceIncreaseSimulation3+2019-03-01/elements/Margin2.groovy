def revenue2 = api.getElement("Revenue2") as BigDecimal
def cost2 = api.getElement("Cost2") as BigDecimal

if (revenue2 == null) {
    api.redAlert("Revenue2")
    return
}

if (cost2 == null) {
    api.redAlert("Cost2")
    return
}

return revenue2 - cost2