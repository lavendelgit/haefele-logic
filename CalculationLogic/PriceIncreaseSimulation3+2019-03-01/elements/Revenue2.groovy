def revenue = api.getElement("Revenue")
def expectedImpactAmount = api.getElement("ExpectedImpactAmount")

if (revenue == null) {
    return 0.0
}

if (expectedImpactAmount == null) {
    return revenue
}

return revenue + expectedImpactAmount