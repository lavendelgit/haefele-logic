def margin = api.getElement("Margin")
def margin2 = api.getElement("Margin2")

if (margin == null || margin2 == null) {
    return 0.0
}

return margin2 - margin