def exceptionalIncreasePerc = api.getElement("ExceptionalIncreasePerc")
def revenueLastYear = api.getElement("Revenue")
def revenueLastYearAfterIncrease = null

if (revenueLastYear != null && exceptionalIncreasePerc != null) {
    revenueLastYearAfterIncrease = revenueLastYear * (1 + exceptionalIncreasePerc)
}

if (api.product("sku").startsWith("999")) {
    api.yellowAlert("Internal article (999). Returning revenue from last year.")
    return revenueLastYear
}

if (!api.getElement("IsGoodRegression")) {
    api.yellowAlert("Linear regression is not good fit. Returning revenue from last year increased by " + api.formatNumber("#.##%", exceptionalIncreasePerc))
    return revenueLastYearAfterIncrease
}

if (api.product("sku").endsWith("X")) {
    api.yellowAlert("X Article. Returning revenue from last year increased by " + api.formatNumber("#.##%", exceptionalIncreasePerc))
    return revenueLastYearAfterIncrease
}

if (api.getElement("IsSamePrice")) {
    api.yellowAlert("Invoice prices are (nearly) the same. Ignoring linear regression " +
                    "and returning revenue from last year increased by " + api.formatNumber("#.##%", exceptionalIncreasePerc))
    return revenueLastYearAfterIncrease

}

if (api.getElement("IsNegativeUnits")) {
    api.yellowAlert("Negative units from linear regression. Returning revenue from last year increased by " + api.formatNumber("#.##%", exceptionalIncreasePerc))
    return revenueLastYearAfterIncrease
}

if (api.getElement("IsExtremeImpact")) {
    api.yellowAlert("The increase would cause an extreme impact (" +
            api.formatNumber("#.##%", api.getElement("PriceIncreaseImpact").expectedImpactPerc) + "). " +
            "Returning revenue from last year increased by " +
            api.formatNumber("#.##%", exceptionalIncreasePerc))
    return revenueLastYearAfterIncrease
}


return api.getElement("PriceIncreaseImpact").newRevenue