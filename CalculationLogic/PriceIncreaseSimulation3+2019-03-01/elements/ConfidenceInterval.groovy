def linReg = api.getElement("LinearRegression")

if (!linReg?.ols) {
        return [:]
}

lib.LinearRegression.confidenceInterval(linReg.ols)