Double scale(BigDecimal value) {
    value.setScale(3, java.math.RoundingMode.CEILING)
}

BigDecimal mean(List values) {
    values.sum() / values.size()
}

Double stdev(List values, int valuesSize, BigDecimal valuesMean) {
    return Math.sqrt(values.collect { num -> Math.pow((num - valuesMean), 2) }.sum() / (valuesSize - 1)) as BigDecimal
}

Map tTest(List values) {
    if (values?.size() > 1) {
        BigDecimal mean = mean(values)
        int valuesSize = values.size()

        def sqrtN = Math.sqrt(valuesSize)
        def standardDeviation = stdev(values, valuesSize, mean)
        def standardError = standardDeviation / sqrtN

        def degreeOfFreedom = valuesSize - 1
        if (degreeOfFreedom >= 100 && degreeOfFreedom <= 149) {
            degreeOfFreedom = 100
        } else if (degreeOfFreedom >= 150 && degreeOfFreedom <= 199) {
            degreeOfFreedom = 150
        } else if (degreeOfFreedom >= 200 && degreeOfFreedom <= 999) {
            degreeOfFreedom = 200
        } else if (degreeOfFreedom >= 1000) {
            degreeOfFreedom = 1000
        }

        BigDecimal criticalValue = getCriticalValue(degreeOfFreedom)
        return [
                lowerLimit: scale(mean - criticalValue * standardError),
                upperLimit: scale(mean + criticalValue * standardError)
        ]
    }

    return [
            lowerLimit: values?.find(),
            upperLimit: values?.find()
    ]
}

BigDecimal getCriticalValue(int degreeOfFreedom) {
    return libs.SharedLib.CacheUtils.getOrSet("T_TEST_STATISTICS", {
        return api.findLookupTableValues("TTestStatisticsTable")?.collectEntries { entry -> [(entry.name): entry.attribute1] }
    })?.get(degreeOfFreedom)
}

