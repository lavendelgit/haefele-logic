api.retainGlobal = true

if (!api.global.customerSectors) {
    api.global.customerSectors = api.findLookupTableValues("CustomerSector").collectEntries { r -> [r.name, r.attribute1]}
}

api.global.customerSectors