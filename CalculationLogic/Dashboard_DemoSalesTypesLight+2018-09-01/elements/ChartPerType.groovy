def data = api.getElement("DataPerType")

def categories = data.collect { d -> d.salesType }
def revenueValues = categories.collect { c -> data.find { d -> d.salesType == c } ?.revenue}
def countValues = categories.collect { c -> data.find { d -> d.salesType == c } ?.count}

api.trace("categories", "", categories)
api.trace("revenueValues", "", revenueValues)
api.trace("countValues", "", countValues)

def chartDef = [
    chart: [
        type: 'column',
        zoomType: 'x'
    ],
    title: [
        text: ''
    ],
    xAxis: [[
                title: [
                    text: "Sales Type"
                ],
                categories: categories
            ]],
    yAxis: [[
                title: [
                    text: "Revenue"
                ]
            ], [
                title: [
                    text: "Count"
                ],
                opposite: true
            ]],
    series: [[
                 name: "Revenue",
                 data: revenueValues,
                 dataLabels: [
                     enabled: true,
                     format: "{point.y:,.0f} €"
                 ],
                 tooltip: [
                     headerFormat: '<b>{series.name}</b><br>{point.key}: ',
                     pointFormat: '{point.y:,.0f} €'
                 ]
             ],[
                 name: "Count",
                 data: countValues,
                 type: "line",
                 yAxis: 1,
                 dataLabels: [
                     enabled: true,
                     format: "{point.y:,.0f}"
                 ],
                 tooltip: [
                     headerFormat: '<b>{series.name}</b><br>{point.key}: ',
                     pointFormat: '{point.y:,.0f}'
                 ]
             ]]
]

api.buildFlexChart("SalesTypesTemplate", chartDef)