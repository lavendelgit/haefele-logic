def dataPerMonth = api.getElement("DataPerMonth")

def salesTypes = api.findLookupTableValues("SalesTypes")
    .collectEntries { pp -> [ (pp.name) : [
        description: pp.attribute1,
        insideOutside: pp.attribute2,
        priceIncreaseAffected: pp.attribute3
    ]]}

api.trace("salesTypes", "", salesTypes)

def salesTypeGroup = salesTypes.collectEntries { salesType, meta -> [salesType, meta.insideOutside] }

salesTypeGroup["Unknown"] = "Unknown"

api.trace("salesTypeGroup", "", salesTypeGroup)

insideOutsideDataPerMonth = [:].withDefault { key -> [:]}

dataPerMonth.forEach { salesType, salesTypeData ->
    def group = salesTypeGroup[salesType] ?: "Undefined!"
    def groupData = insideOutsideDataPerMonth[group];
    salesTypeData.forEach { d ->
        def groupMonthData = groupData[d.yearMonth]
        if (!groupMonthData) {
            groupData[d.yearMonth] = [
                yearMonth: d.yearMonth,
                count: d.count,
                revenue: d.revenue,
                marginAbs: d.marginAbs ?: 0.0,
                salesType: group
            ]
        } else {
            groupMonthData.count += d.count
            groupMonthData.revenue += d.revenue
            groupMonthData.marginAbs += d.marginAbs ?: 0.0
        }
    }
}

return insideOutsideDataPerMonth.collectEntries { key, value ->
    return [key, value.values()]
}