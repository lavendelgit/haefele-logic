def salesPrice = api.getElement("SalesPrice")
def discountAbs = api.getElement("DiscountAbs")

if (!salesPrice) {
    api.redAlert("SalesPrice")
    return null
}

if (discountAbs == null) {
    api.redAlert("DiscountAbs")
    return null
}

return discountAbs / salesPrice


