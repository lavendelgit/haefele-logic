def customerSalesPrice = api.currentItem()?.attribute4 // Sales Price (de: Verkaufspreis)

if (!customerSalesPrice) {
    api.redAlert("Sales Price is missing in PX CustomerSalesPrice")
    return null;
}

return customerSalesPrice