if (api.isDebugMode()) {
    return "Simulation 6"
}
List simulationInputOptions = api.findLookupTableValues("SimulationInputs", api.global, ['name'], 'name')?.name
String selectedSimulation = api.option('Associated Simulation', simulationInputOptions)

if (!selectedSimulation) {
    api.addWarning('No simulation associated with this configuration')
    api.abortCalculation()
}

return selectedSimulation