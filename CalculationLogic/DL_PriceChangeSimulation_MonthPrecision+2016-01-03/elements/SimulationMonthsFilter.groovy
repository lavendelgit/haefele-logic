List simulationSelectedMonths = out.AssociatedSimulationJSON["SimulationMonthsFilter"] ?: Constants.MONTHS_PREFIXES

libs.__LIBRARY__.TraceUtility.developmentTrace("The output of the selected simulation Months", simulationSelectedMonths)

return (simulationSelectedMonths.size() < 12) ? getAllMonths(simulationSelectedMonths) : simulationSelectedMonths

/**
 * MK TODO [libs.HaefeleCommon.Simulation.SIMULATION_INPUT_COLUMNS.SimulationDataFilter.name]
 * add costants in haefele common
 */

protected List getAllMonths(List selectedMonth) {
    List selectedMonths = []
    boolean isFirstMonthAdded = false
    Constants.MONTHS_NAMES_PREFIXES.each { String monthName, String monthPrefix ->
        if (selectedMonth.contains(monthName)) {
            selectedMonths << monthPrefix
        }
    }
    return selectedMonths
}