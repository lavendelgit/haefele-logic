//TODO: Use DBQueries.getDataFromSource () method for the details.
def ctx = api.getDatamartContext()
def dm = ctx.getDatamart(Constants.DM_SIMULATION)
def dmQuery = ctx.newQuery(dm)
List dmColumnsDef = out.DMProjections
dmColumnsDef.each { Map columnDef -> dmQuery.select(columnDef.dmColumn, columnDef.alias)
}

List filters = out.DMFilter ? [out.DMFilter] : []
//List filters = [Filter.equal("Material","000.10.280")]
libs.__LIBRARY__.TraceUtility.developmentTrace('The filter that is configured..', api.jsonEncode(filters))
if (filters) {
    dmQuery.where(*filters)
}

return dmQuery