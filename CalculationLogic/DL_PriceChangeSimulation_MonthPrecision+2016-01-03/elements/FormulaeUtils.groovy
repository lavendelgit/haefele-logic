protected String getMonthlyFieldForCompleteYear(String field, String fieldAlias, List selectedMonthsForSimulationList, String aggregationFunction = '', boolean isRoundingApplicable = false, boolean isFirstSet = false,
                                                boolean isAliasEnclosureRequired = false) {
    return getMonthlyFieldForCompleteYearForSubTable(field, fieldAlias, selectedMonthsForSimulationList, '', aggregationFunction, isRoundingApplicable, isFirstSet, isAliasEnclosureRequired)
}

protected String getMonthlyFieldForCompleteYearForSubTable(String field, String alias, List selectedMonthsForSimulationList, String subTable, String aggregationFunction = '', boolean isRoundingApplicable = false,
                                                           boolean isFirstSet = false, boolean isAliasaEnclosed = false) {
    boolean requiresAliasEnclosure = isAliasaEnclosed || isAliasEnclosureRequired(alias)
    String tableBasedAccess = getSubTableAccessPrefix(subTable)
    Closure monthlyFieldForCompleteYearClosure = { StringBuffer twelveMonthsFields, String monthPrefix, String aliasEnclosure ->
        twelveMonthsFields.append(encloseFunctionInvocation(aggregationFunction, "$tableBasedAccess$monthPrefix$field"))
        twelveMonthsFields.append(getAliasString(aliasEnclosure, "$monthPrefix$alias"))
    }

    return addMonthlyContext(isFirstSet, requiresAliasEnclosure, monthlyFieldForCompleteYearClosure, selectedMonthsForSimulationList)
}

protected boolean isAliasEnclosureRequired(String alias) {
    return alias && alias.contains(' ')
}

protected String getSubTableAccessPrefix(String subTable) {
    return subTable ? "$subTable." : ""
}

protected String encloseFunctionInvocation(String aggregationFunction, String field) {
    if (!aggregationFunction || aggregationFunction == '') {
        return field
    }

    return "$aggregationFunction($field)"
}

protected String getRoundedField(String field) {
    return " ${Constants.ROUND_FUNCTION}(${field}, ${Constants.ROUNDING_DECIMAL_PLACES})"
}

protected String getAliasString(String aliasEnclosure, String alias) {
    return " as $aliasEnclosure$alias$aliasEnclosure"
    return " as $aliasEnclosure$alias$aliasEnclosure"
}

protected String addMonthlyContext(boolean isFirstSet, boolean requiresAliasEnclosure, Closure specificProcessor, List selectedMonthsForSimulationList) {
    String aliasEnclosure = getAliasEnclosure(requiresAliasEnclosure)
    String fieldSeparator = getFirstSeparator(isFirstSet)
    StringBuffer twelveMonthsFields = new StringBuffer()
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->  // TODO instead of Constants.months_prefix pick the values from List
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append("$fieldSeparator\n")
        specificProcessor(twelveMonthsFields, monthPrefix, aliasEnclosure)
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

protected String getFirstSeparator(boolean isFirstSet) {
    return isFirstSet ? '' : ','
}

protected String getAliasEnclosure(String alias, boolean isEnclosureRequired = false) {
    return getAliasEnclosure(isEnclosureRequired || isAliasEnclosureRequired(alias))
}

protected String getAliasEnclosure(boolean hasSpaceInAlias) {
    return hasSpaceInAlias ? "'" : ""
}

protected String getTargetAliasString(String alias) {
    return isAliasEnclosureRequired(alias) ? "'$alias'" : "$alias"
}

protected String getNullIfExpression(String field) {
    return "NULLIF($field, 0)"
}

// TODO check this
protected String getEffectivePercentageValuesPerMonth(Map percentageValues, String exceptionTable, String exceptionPercentageChangeField, String percentageChangeField, List selectedMonthsForSimulationList, boolean isFirstSet = false) {

    StringBuffer percentageValuesPerMonth = new StringBuffer()
    String fieldSeparator = getFirstSeparator(isFirstSet)
    String exceptionTableAccess = getSubTableAccessPrefix(exceptionTable)
    int aliasIndex = 0
    String monthPrefix
    BigDecimal currentEffectiveIncrease = BigDecimal.ZERO
    BigDecimal monthlyPercentagevalue = BigDecimal.ZERO
    Constants.MONTH_NAMES.each { String monthName ->
        monthPrefix = Constants.MONTHS_PREFIXES[aliasIndex]
        //currentEffectiveIncrease = percentageValues[monthName] ? (percentageValues[monthName] as BigDecimal) + currentEffectiveIncrease : currentEffectiveIncrease
        monthlyPercentagevalue = (percentageValues[monthName]) ? (percentageValues[monthName] as BigDecimal) : BigDecimal.ZERO
        currentEffectiveIncrease = monthlyPercentagevalue ? ((1 + monthlyPercentagevalue) * (1 + currentEffectiveIncrease) - 1) : currentEffectiveIncrease
        if (selectedMonthsForSimulationList.contains(monthPrefix)) {
            percentageValuesPerMonth.append("${fieldSeparator}COALESCE($exceptionTableAccess$monthPrefix$exceptionPercentageChangeField, $currentEffectiveIncrease)")
            percentageValuesPerMonth.append(getAliasString('', "$monthPrefix$percentageChangeField"))
            fieldSeparator = ','
        }
        aliasIndex++
    }
    return percentageValuesPerMonth.toString()
}

protected String getMonthlyProductExpectedPocketMargin(String expectedPocketPrice, String baseCost, String alias, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    boolean requiresAliasEnclosure = isAliasEnclosureRequired(alias)
    String currentField, averageField
    Closure monthlyProductExpectedClosure = { StringBuffer twelveMonthsFields, String monthPrefix, String aliasEnclosure ->
        currentField = "$monthPrefix$expectedPocketPrice"
        averageField = "AVG($currentField)"
        twelveMonthsFields.append("AVG($currentField - $monthPrefix$baseCost)/ ${getNullIfExpression(averageField)}")
        twelveMonthsFields.append(getAliasString(aliasEnclosure, "$monthPrefix$alias"))
    }

    return addMonthlyContext(isFirstSet, requiresAliasEnclosure, monthlyProductExpectedClosure, selectedMonthsForSimulationList)
}

protected String getLevel1PricingTypeCalculations(String pricingTypeConst, String calculationType, List selectedMonthsForSimulationList) {
    Map level2 = Constants.LEVEL_2_ALIAS
    String l2RevenueContribution = pricingTypeConst + level2["${calculationType}_CUSTOMER_REVENUE"]
    String l2totalMargin = pricingTypeConst + level2["PT_${calculationType}_TOTAL_MARGIN"]

    Map level1 = Constants.LEVEL_1_ALIAS
    String l1RevenueContribution = level1["PT_${calculationType}_CUSTOMER_REVENUE"] + pricingTypeConst
    String l1totalMargin = level1["PT_${calculationType}_TOTAL_MARGIN"] + pricingTypeConst

    String currentTotalRevenue
    String fieldSeparator = ','
    Closure level1PricingTypeCalculationsClosure = { StringBuffer twelveMonthsFields, String monthPrefix, String aliasEnclosure ->
        currentTotalRevenue = "SUM($monthPrefix$l2RevenueContribution)"
        twelveMonthsFields.append(getLevel1FieldCalculation("$currentTotalRevenue", aliasEnclosure, "$monthPrefix$l1RevenueContribution", fieldSeparator))
        twelveMonthsFields.append(getLevel1FieldCalculation("SUM($monthPrefix$l2totalMargin)", aliasEnclosure, "$monthPrefix$l1totalMargin", ''))
    }

    return addMonthlyContext(false, true, level1PricingTypeCalculationsClosure, selectedMonthsForSimulationList)
}

protected String getLevel1FieldCalculation(String field, String aliasEnclosure, String alias, String fieldSeparator) {
    return "${field} ${getAliasString(aliasEnclosure, alias)}$fieldSeparator"
}

protected String getSimpleKeyValue(String field, String alias, boolean isFirstSet = false, String subTable = '', boolean aliasEnclosureRequested = false) {
    String subTableField = getSubTableAccessPrefix(subTable)
    String fieldSeparator = getFirstSeparator(isFirstSet)
    String aliasEnclosure = getAliasEnclosure(alias, aliasEnclosureRequested)

    return "$fieldSeparator$subTableField$field ${getAliasString(aliasEnclosure, alias)}"
}

protected String getSimpleFunctionValue(String field, String alias, String aggregationFunction, boolean isFirstSet = false) {
    String fieldSeparator = getFirstSeparator(isFirstSet)
    String aliasEncloser = getAliasEnclosure(alias)
    StringBuffer twelveMonthsFields = new StringBuffer(fieldSeparator)
    twelveMonthsFields.append(encloseFunctionInvocation(aggregationFunction, field))
    twelveMonthsFields.append(getAliasString(aliasEncloser, alias))

    return twelveMonthsFields.toString()
}

protected String getMonthlyMultiplyTwoFields(String field1, String field2, String alias, List selectedMonthsForSimulationList, String aggregationFunction = '', boolean isFirstSet = false) {
    Closure multiplyTwoFieldsClosure = { StringBuffer twelveMonthsFields, String monthPrefix, String aliasEncloser ->
        twelveMonthsFields.append(encloseFunctionInvocation(aggregationFunction, "$monthPrefix$field1*$monthPrefix$field2"))
        twelveMonthsFields.append(getAliasString(aliasEncloser, "$monthPrefix$alias"))
    }

    return addMonthlyContext(isFirstSet, isAliasEnclosureRequired(alias), multiplyTwoFieldsClosure, selectedMonthsForSimulationList)
}
//todo
protected String getChangeMarkupPercent(String field1, String field2, String alias, String aggregationFunction, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    String relativeField
    Closure multiplyTwoFieldsClosure = { StringBuffer twelveMonthsFields, String monthPrefix, String aliasEncloser ->
        relativeField = "$monthPrefix$field2"
        twelveMonthsFields.append(encloseFunctionInvocation(aggregationFunction, "($monthPrefix$field1-$relativeField)/${getNullIfExpression(relativeField)}"))
        twelveMonthsFields.append(getAliasString(aliasEncloser, "$monthPrefix$alias"))
    }

    return addMonthlyContext(isFirstSet, isAliasEnclosureRequired(alias), multiplyTwoFieldsClosure, selectedMonthsForSimulationList)
}

protected String getFieldDifference(String field1, String field2, String alias, String aggregationFunction, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    final String functionStart = aggregationFunction && aggregationFunction != '' ? '(' : ''
    final String functionEnd = aggregationFunction && aggregationFunction != '' ? ')' : ''
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    //TODO change the constants to list of month
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        twelveMonthsFields.append("$aggregationFunction$functionStart$monthPrefix$field1-$monthPrefix$field2$functionEnd as $monthPrefix$alias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

protected String getLevel2ExpectedMargin(String grossPrice, String expectedQuantity, String baseCost, String quantity, String alias, String aggregationFunction,
                                         List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    final String functionStart = aggregationFunction && aggregationFunction != '' ? '(' : ''
    final String functionEnd = aggregationFunction && aggregationFunction != '' ? ')' : ''
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    //TODO change the constants to list of month
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        twelveMonthsFields.append("$aggregationFunction$functionStart($monthPrefix$grossPrice*$monthPrefix$expectedQuantity)-($monthPrefix$baseCost*$monthPrefix$quantity)$functionEnd as $monthPrefix$alias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

protected String getLevel2PricingTypeComputationsMonthly(String pricingTypeConst,
                                                         String pricingType,
                                                         String calculation,
                                                         List selectedMonthsForSimulationList,
                                                         boolean isException = false,
                                                         boolean isFirstSet = false) {
    Map level2 = Constants.LEVEL_2_ALIAS
    String revenueContributionAlias = pricingTypeConst + level2["${calculation}_CUSTOMER_REVENUE"]
    String totalMarginContributionAlias = pricingTypeConst + level2["PT_${calculation}_TOTAL_MARGIN"]

    Map level3 = Constants.LEVEL_3_ALIAS
    String discountRevenueContribution = pricingTypeConst + level3["${calculation}_DISCOUNT_REVENUE_CONTRIBUTION"]
    String fieldSeparator = isFirstSet ? '' : ','
    String revenueContributionField
    StringBuffer twelveMonthsFields = new StringBuffer()
    //TODO change the constants to list of month
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        revenueContributionField = "$monthPrefix$discountRevenueContribution"
        twelveMonthsFields.append(fieldSeparator)
        switch (calculation) {
            case Constants.CALCULATIONS.CURRENT:
                twelveMonthsFields.append("AVG($revenueContributionField * $monthPrefix${level3.QUANTITY_SOLD}) as $monthPrefix$revenueContributionAlias,")
                if (isException) {
                    twelveMonthsFields.append("AVG((CASE WHEN $revenueContributionField <> 0 THEN $revenueContributionField - $monthPrefix${level3.BASE_COST} ELSE 0 END) * $monthPrefix${level3.QUANTITY_SOLD})  as $monthPrefix$totalMarginContributionAlias")
                } else {
                    twelveMonthsFields.append("AVG((CASE WHEN PricingType = '$pricingType' THEN $revenueContributionField - $monthPrefix${level3.BASE_COST} ELSE 0 END) * $monthPrefix${level3.QUANTITY_SOLD})  as $monthPrefix$totalMarginContributionAlias")
                }
                break
            case Constants.CALCULATIONS.EXPECTED:
                twelveMonthsFields.append("AVG($revenueContributionField * $monthPrefix${level3.EXPECTED_QUANTITY}) as $monthPrefix$revenueContributionAlias,")
                if (isException) {
                    twelveMonthsFields.append("AVG(CASE WHEN $revenueContributionField <> 0 THEN $revenueContributionField * $monthPrefix${level3.EXPECTED_QUANTITY} - $monthPrefix${level3.EXPECTED_BASE_COST} * $monthPrefix${level3.QUANTITY_SOLD} ELSE 0 END) as $monthPrefix$totalMarginContributionAlias")
                } else {
                    twelveMonthsFields.append("AVG(CASE WHEN PricingType = '$pricingType' THEN $revenueContributionField * $monthPrefix${level3.EXPECTED_QUANTITY} - $monthPrefix${level3.EXPECTED_BASE_COST} * $monthPrefix${level3.QUANTITY_SOLD} ELSE 0 END) as $monthPrefix$totalMarginContributionAlias")
                }
                break
        }
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

protected String getLevel3PricingTypeCalculations(String pricingTypeConst, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    Map level4 = Constants.LEVEL_4_ALIAS
    String currentPricingTypeContribution = "$pricingTypeConst${level4.DISCOUNT_REVENUE_DISTRIBUTION}"
    Map level3 = Constants.LEVEL_3_ALIAS
    String expectedPricingTypeContributionAlias = "$pricingTypeConst${level3.EXPECTED_DISCOUNT_REVENUE_CONTRIBUTION}"
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    //TODO change constants to List of months selected
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        twelveMonthsFields.append("$monthPrefix$currentPricingTypeContribution * $monthPrefix${level4.CHANGE_RATIO} as $monthPrefix$expectedPricingTypeContributionAlias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

protected String getMonthlyPercentages(String marginField, String totalField, String alias, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    return getDivisionResponse(marginField, totalField, alias, selectedMonthsForSimulationList, isFirstSet)
}

protected String getDivisionResponse(String marginField, String totalField, String alias, List selectedMonthsForSimulationList, boolean isRoundingApplicable = false, boolean isFirstSet = false) {
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    String totalMonthlyField
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        totalMonthlyField = "$monthPrefix$totalField"
        twelveMonthsFields.append("$monthPrefix$marginField / ${getNullIfExpression(totalMonthlyField)}")
        twelveMonthsFields.append(" as $monthPrefix$alias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

//TODO check where this function is used.
protected String getCustomerDataFieldMonthlyForYear(String customerId, String summaryField, String alias, boolean isFirstSet = false) {
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    Constants.MONTHS_PREFIXES.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        twelveMonthsFields.append("SUM((CASE WHEN $customerId IS NOT NULL THEN $monthPrefix$summaryField ELSE 0 END)) as $monthPrefix$alias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

protected String getRowNumberOnFields(List fieldsToPartition, String orderByField, String alias, boolean isFirstSet = false) {
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    twelveMonthsFields.append(fieldSeparator)
    twelveMonthsFields.append("ROW_NUMBER() OVER (PARTITION BY ${fieldsToPartition.join(',')} ORDER BY $orderByField) as $alias")

    return twelveMonthsFields.toString()
}

protected String getKeyConstructedUsingDelimeter(List fieldsParticipatingInKey, String alias, String defaultCharacterIfEmpty = "'-'", String delimiterBetweenField = " || '-' || ") {
    return "," + (fieldsParticipatingInKey.collect { String field -> return " coalesce($field, $defaultCharacterIfEmpty)" }
                                          .join(delimiterBetweenField)) + " as $alias"
}

protected String getMonthlyFieldForCustomersWithAndWithoutPricing(String customerId, String source1, String source1Field, String source2, String source2Field, String alias, boolean isFirstSet = false) {
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    String source1ComputedField
    Constants.MONTHS_PREFIXES.each { String monthPrefix ->
        source1ComputedField = "${source1}.$monthPrefix$source1Field"
        twelveMonthsFields.append(fieldSeparator)
        twelveMonthsFields.append("(CASE WHEN $customerId IS NULL THEN $source1ComputedField - NVL(${source2}.$monthPrefix$source2Field,0) ELSE $source1ComputedField END) as $monthPrefix$alias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

/*
Sample Output : (GrossPrice + (GrossPrice * GrossPriceChangePercent)) as TmpGrossPrice
*/

protected String getExpectedkPerMonthlyChangedValue(String baseField, String changedPercentage, String aliasValue, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    String fieldToChange
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        fieldToChange = "$monthPrefix$baseField"
        twelveMonthsFields.append("($fieldToChange + ($fieldToChange*$monthPrefix$changedPercentage)) as $monthPrefix$aliasValue")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

/*
Output: (CASE WHEN ConditionName = 'ZRI' AND ConditionTable = 'A617' AND ZPL IS NOT NULL THEN ZPL - (ZPL * GrossPriceChangePercent) ELSE TmpGrossPrice END) as TmpSpecialGrossPrice
*/

protected String getMonthlySpecialPricesForLevel5(List selectedMonthsForSimulationList) {
    Map level5 = Constants.LEVEL_5_FIELD
    String fieldSeparator = ''
    StringBuffer twelveMonthsFields = new StringBuffer()
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        twelveMonthsFields.append("$monthPrefix${level5.TEMP_GROSS_PRICE} as $monthPrefix${level5.TEMP_SPECIAL_GROSS_PRICE}")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

/*
   Output: CASE
        WHEN PricingType = 'Markup' THEN ExpectedBaseCost * (1 + KBETR)
        WHEN PricingType = 'Net Price' THEN KBETR
        WHEN PricingType = 'Absolute Discount' THEN TmpSpecialGrossPrice - KBETR
        WHEN PricingType = 'Per Discount' THEN TmpSpecialGrossPrice * (1 + KBETR)
        WHEN PricingType = 'INTERNAL' THEN HistoricalPocketPrice
        WHEN PricingType = 'XARTICLE' THEN HistoricalPocketPrice
        WHEN PricingType = 'No Discounts' THEN TmpSpecialGrossPrice
        ELSE TmpSpecialGrossPrice
  END as TmpPocketPrice
 */

protected String getMonthlyPocketPriceForLevel5(List selectedMonthsForSimulationList) {
    Map level5 = Constants.LEVEL_5_FIELD
    Map pricingTypes = Constants.PRICING_TYPES
    String fieldSeparator = ''
    StringBuffer twelveMonthsFields = new StringBuffer()
    String zpl
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        zpl = "${level5.ZPL}"
        twelveMonthsFields.append("(CASE ")
        twelveMonthsFields.append("WHEN ${level5.PRICING_TYPE} = '${pricingTypes.MARKUP}' THEN $monthPrefix${level5.EXPECTED_BASE_COST}*(1+${level5.DISCOUNT}) ")
        twelveMonthsFields.append("WHEN ${level5.PRICING_TYPE} = '${pricingTypes.NET_PRICE}' THEN ${level5.DISCOUNT} ")
        twelveMonthsFields.append("WHEN ${level5.PRICING_TYPE} = '${pricingTypes.ABSOULTE_DISCOUNT}' THEN $monthPrefix${level5.TEMP_SPECIAL_GROSS_PRICE} - ${level5.DISCOUNT} ")
        twelveMonthsFields.append("WHEN ${level5.PRICING_TYPE} = '${pricingTypes.PER_DISCOUNT}' THEN $monthPrefix${level5.TEMP_SPECIAL_GROSS_PRICE} * (1 + ${level5.DISCOUNT}) ")
        twelveMonthsFields.append("WHEN ${level5.PRICING_TYPE} = '${pricingTypes.X_ARTICLE}' THEN $monthPrefix${level5.HISTORICAL_POCKET_PRICE} ")
        twelveMonthsFields.append("WHEN ${level5.PRICING_TYPE} = '${pricingTypes.INTERNAL}' THEN $monthPrefix${level5.HISTORICAL_POCKET_PRICE} ")
        twelveMonthsFields.append("WHEN ${level5.PRICING_TYPE} = '${pricingTypes.NO_DISCOUNT}' THEN $monthPrefix${level5.TEMP_SPECIAL_GROSS_PRICE} ")
        twelveMonthsFields.append("ELSE $monthPrefix${level5.TEMP_SPECIAL_GROSS_PRICE} ")
        twelveMonthsFields.append("END) as $monthPrefix${level5.TEMP_POCKET_PRICE}")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}
/*
Sample Output: ABS(TmpPocketPrice / NULLIF(PocketPrice,0))  as ChangeRatio
*/

protected String getMonthlyAbsoluteDivisionResponse(String modifiedPrice, String actualPrice, String alias, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    String includeSingleQuotes = ""
    if (alias && alias.contains(' ')) {
        includeSingleQuotes = "'"
    }
    String totalMonthlyField
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        totalMonthlyField = "$includeSingleQuotes$monthPrefix$actualPrice$includeSingleQuotes"
        twelveMonthsFields.append("ABS($includeSingleQuotes$monthPrefix$modifiedPrice$includeSingleQuotes / ${getNullIfExpression(totalMonthlyField)})")
        twelveMonthsFields.append(" as $monthPrefix$alias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

/*
Sample Output: ((TotalRevenue / NULLIF(QtySold,0)) - BaseCost) as HistoryPocketMargin
 */

protected String getMonthlyHistoryPocketMargin(String modifiedPrice, String actualPrice, String baseCost, String alias, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    String totalMonthlyField, actualPriceCalculationFormula
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        totalMonthlyField = "$monthPrefix$actualPrice"
        actualPriceCalculationFormula = "($monthPrefix$modifiedPrice / ${getNullIfExpression(totalMonthlyField)})"
        twelveMonthsFields.append("(CASE WHEN NVL($actualPriceCalculationFormula, 0) <> 0 THEN  ($actualPriceCalculationFormula-$monthPrefix$baseCost) ELSE 0 END)")
        twelveMonthsFields.append(" as $monthPrefix$alias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

/*
Sample Output: (CASE WHEN PricingType = 'Markup' THEN HistoryPocketPrice ELSE 0 END)                                           as cur_MarkupDiscountRevenueContribution
 */

protected String getPricingTypeLevel4RevenueContribution(String pricingTypeConst, String pricingType, String priceColumn, List selectedMonthsForSimulationList, boolean isFirstSet = false) {
    String fieldSeparator = isFirstSet ? '' : ','
    StringBuffer twelveMonthsFields = new StringBuffer()
    Map level4 = Constants.LEVEL_4_ALIAS
    String pricingTypeAlias = "$pricingTypeConst${level4.DISCOUNT_REVENUE_DISTRIBUTION}"
    //Constants.MONTHS_PREFIXES.each { String monthPrefix ->
    selectedMonthsForSimulationList.each { String monthPrefix ->
        twelveMonthsFields.append(fieldSeparator)
        twelveMonthsFields.append("(CASE WHEN ${level4.PRICING_TYPE}='$pricingType' THEN $monthPrefix$priceColumn ELSE 0 END)")
        twelveMonthsFields.append(" as $monthPrefix$pricingTypeAlias")
        fieldSeparator = ','
    }

    return twelveMonthsFields.toString()
}

/*
Output:
    (SELECT
    Material AS ExceptionMaterial,
	0.034 AS ExceptionGrossPriceChangePercent,
	0.025 AS ExceptionPurchasePriceChangePercent,
	0.068 AS ExceptionQuantityChangePercent,
	null AS ExceptionRecommendedGrossMargin
    FROM T1 WHERE ProdhIII = '010 Fertigelemente' )
 */

protected String getExceptionUnions(List exceptions, List selectedMonthsForSimulationList) {
    StringBuffer allExceptionQuery = new StringBuffer()
    boolean isNotFirstTime = false
    String queryStarting
    Closure prepareAllExceptionChargeClosure
    Map level5 = Constants.LEVEL_5_FIELD
    Map exceptionColumns = Constants.MERGED_EXCEPTION_CONFIGURATION
    Map exceptionFields = Constants.EXCEPTION_FIELDS
    String currentMonth
    Map grossPricePercentChanges, purchasePricePercentChanges, quantityPercentChanges
    Map subtables = Constants.SUB_TABLE
    exceptions.each { Map exceptionDetails ->
        queryStarting = isNotFirstTime ? ") UNION ( SELECT " : "( SELECT "
        allExceptionQuery.append(queryStarting)
        allExceptionQuery.append("${level5.MATERIAL} ${getAliasString('', exceptionColumns.EXCEPTION_MATERIAL)}")
        grossPricePercentChanges = exceptionDetails[exceptionFields.GROSS_PRICE_CHANGE_CONFIG]
        purchasePricePercentChanges = exceptionDetails[exceptionFields.PURCHASE_PRICE_CHANGE_CONFIG]
        quantityPercentChanges = exceptionDetails[exceptionFields.QUANTITY_CHANGE_PERCENT]
        prepareAllExceptionChargeClosure = { StringBuffer twelveMonthsFields, String monthPrefix, String aliasEncloser ->
            currentMonth = Constants.MONTHS_PREFIXES_NAMES[monthPrefix]
            twelveMonthsFields.append("${grossPricePercentChanges[currentMonth]} ${getAliasString(aliasEncloser, monthPrefix + exceptionColumns.EXCEPTION_GROSS_PRICE_CHANGE_PERCENT)},")
            twelveMonthsFields.append("${purchasePricePercentChanges[currentMonth]} ${getAliasString(aliasEncloser, monthPrefix + exceptionColumns.EXCEPTION_PURCHASE_PRICE_CHANGE_PERCENT)},")
            twelveMonthsFields.append("${quantityPercentChanges[currentMonth]} ${getAliasString(aliasEncloser, monthPrefix + exceptionColumns.EXCEPTION_QUANTITY_CHANGE_PERCENT)}")
        }
        allExceptionQuery.append(addMonthlyContext(false, false, prepareAllExceptionChargeClosure, selectedMonthsForSimulationList))
        allExceptionQuery.append(" From ${subtables.T1} WHERE ${exceptionDetails[exceptionFields.DATA_FILTER]}")
        isNotFirstTime = true
    }
    if (isNotFirstTime) {
        allExceptionQuery.append(")")
    }

    return allExceptionQuery.toString()
}

if (api.isDebugMode()) {
    Map level1 = Constants.LEVEL_1_ALIAS
    Map level2 = Constants.LEVEL_2_ALIAS
    Map level3 = Constants.LEVEL_3_ALIAS
    Map level4 = Constants.LEVEL_4_ALIAS
    Map level5 = Constants.LEVEL_5_FIELD
    Map subTable = Constants.SUB_TABLE
    Map customerDataFields = Constants.CUSTOMER_DATA_FIELDS
    Map pricingTypesConst = Constants.PRICING_TYPES_CONST
    Map pricingTypes = Constants.PRICING_TYPES
    Map exceptionConfigurationFields = Constants.MERGED_EXCEPTION_CONFIGURATION
    def printOutput = libs.__LIBRARY__.TraceUtility
    List selectedSimulationMonths = out.SimulationMonthsFilter
    api.trace("getEffectivePercentageValuesPerMonth", getEffectivePercentageValuesPerMonth(['January': '0', 'February': '1', 'October': '1'], subTable.MERGED_EXCEPTION_CONFIGURATION, exceptionConfigurationFields.EXCEPTION_GROSS_PRICE_CHANGE_PERCENT, 'grossPriceChangePercent', selectedSimulationMonths, true))
    printOutput.developmentTrace("getMonthlyFieldForCompleteYear", getMonthlyFieldForCompleteYear('TotalRevenue', 'HistoryPocketMargin', selectedSimulationMonths, 'AVG', true))
    printOutput.developmentTrace("getEffectivePercentageValuesPerMonth", getEffectivePercentageValuesPerMonth(['January': '0', 'February': '1', 'October': '1'], subTable.MERGED_EXCEPTION_CONFIGURATION, exceptionConfigurationFields.EXCEPTION_GROSS_PRICE_CHANGE_PERCENT, 'grossPriceChangePercent', selectedSimulationMonths, true))
    printOutput.developmentTrace("getMonthlyProductExpectedPocketMargin", getMonthlyProductExpectedPocketMargin(level2.EXPECTED_POCKET_PRICE, level2.AVG_BASE_COST, level1.PT_EXPECTED_TOTAL_MARGIN, selectedSimulationMonths))
    Constants.PRICING_TYPES_CONST.each { String typeKey, String pricingTypeConst ->
        printOutput.developmentTrace(Constants.CALCULATIONS.CURRENT + "getLevel1PricingTypeCalculations" + pricingTypeConst, getLevel1PricingTypeCalculations(pricingTypeConst, Constants.CALCULATIONS.CURRENT, selectedSimulationMonths))
        printOutput.developmentTrace(Constants.CALCULATIONS.EXPECTED + "getLevel1PricingTypeCalculations" + pricingTypeConst, getLevel1PricingTypeCalculations(pricingTypeConst, Constants.CALCULATIONS.EXPECTED, selectedSimulationMonths))
    }
    printOutput.developmentTrace("getSimpleKeyValue", getSimpleKeyValue(level2.MATERIAL, level1.MATERIAL, true))
    printOutput.developmentTrace("getSimpleFunctionValue", getSimpleFunctionValue(level2.TRANSACTION_COUNT, level1.TRANSACTION_COUNT, Constants.FUNCTIONS.SUM))
    //printOutput.developmentTrace("getMonthlyMultiplyTwoFields", getMonthlyMultiplyTwoFields(level3.EXPECTED_BASE_COST, level3.QUANTITY_SOLD, level2.EXPECTED_TOTAL_BASE_COST, Constants.FUNCTIONS.AVERAGE, selectedSimulationMonths))
    printOutput.developmentTrace("getChangeMarkupPercent", getChangeMarkupPercent(level3.EXPECTED_GROSS_PRICE, level3.EXPECTED_BASE_COST, level2.AVERAGE_EXPECTED_MARKUP, Constants.FUNCTIONS.AVERAGE, selectedSimulationMonths))
    printOutput.developmentTrace("getFieldDifference", getFieldDifference(level3.EXPECTED_GROSS_PRICE, level3.EXPECTED_BASE_COST, level2.EXPECTED_MARGIN_PER_UNIT, Constants.FUNCTIONS.AVERAGE, selectedSimulationMonths))
    printOutput.developmentTrace("getLevel2ExpectedMargin", getLevel2ExpectedMargin(level3.EXPECTED_GROSS_PRICE,
                                                                                    level3.EXPECTED_QUANTITY,
                                                                                    level3.EXPECTED_BASE_COST,
                                                                                    level3.QUANTITY_SOLD,
                                                                                    level2.TOTAL_EXPECTED_MARGIN,
                                                                                    Constants.FUNCTIONS.AVERAGE,
                                                                                    selectedSimulationMonths))
    Constants.PRICING_TYPES_CONST.each { String typeKey, String pricingTypeConst ->
        printOutput.developmentTrace(Constants.CALCULATIONS.CURRENT + "getLevel2PricingTypeComputationsMonthly" + pricingTypeConst, getLevel2PricingTypeComputationsMonthly(pricingTypeConst, pricingTypes[typeKey], Constants.CALCULATIONS.CURRENT, selectedSimulationMonths))
        printOutput.developmentTrace(Constants.CALCULATIONS.EXPECTED + "getLevel2PricingTypeComputationsMonthly" + pricingTypeConst, getLevel2PricingTypeComputationsMonthly(pricingTypeConst, pricingTypes[typeKey], Constants.CALCULATIONS.EXPECTED, selectedSimulationMonths))
    }

    Constants.PRICING_TYPES_CONST.each { String typeKey, String pricingTypeConst -> printOutput.developmentTrace("getLevel3PricingTypeCalculations" + pricingTypeConst, getLevel3PricingTypeCalculations(pricingTypeConst, selectedSimulationMonths))
    }
    printOutput.developmentTrace("getMonthlyPercentages", getMonthlyPercentages(level4.HISTORY_POCKET_PRICE, level4.POCKET_PRICE, level3.POCKET_MARGIN_PERCENT, selectedSimulationMonths))
    printOutput.developmentTrace("getCustomerDataFieldMonthlyForYear", getCustomerDataFieldMonthlyForYear(level5.CUSTOMER_ID, level5.QUANTITY_SOLD, Constants.CUSTOMER_DATA_FIELDS.CUSTOMER_QUANTITY_SOLD))
    printOutput.developmentTrace("getRowNumberOnFields", getRowNumberOnFields([level5.MATERIAL, level5.CUSTOMER_ID], level5.CUSTOMER_ID, level5.ROW_NUM))
    printOutput.developmentTrace("getKeyConstructedUsingDelimeter", getKeyConstructedUsingDelimeter([level5.MATERIAL, level5.CUSTOMER_ID, level5.YEAR], level4.TRANSACTION_KEY))
    printOutput.developmentTrace("getMonthlyFieldForCustomersWithAndWithoutPricing", getMonthlyFieldForCustomersWithAndWithoutPricing(level5.CUSTOMER_ID, subTable.T1, level5.TOTAL_REVENUE, subTable.CUSTOMER_DATA, customerDataFields.CUSTOMER_REVENUE, level5.ADJUSTED_REVENUE))
    printOutput.developmentTrace("getMonthlyFIeldForCustomerWithandWithoutPricing", getMonthlyFieldForCustomersWithAndWithoutPricing(level5.CUSTOMER_ID, subTable.T1, level5.QUANTITY_SOLD, subTable.CUSTOMER_DATA, customerDataFields.CUSTOMER_QUANTITY_SOLD, level5.ADUSTED_QUANTITY))
    printOutput.developmentTrace("getExpectedPerMonthlyChangedValue", getExpectedkPerMonthlyChangedValue(level5.GROSS_PRICE, level5.GROSS_PRICE_CHANGE_PERCENT, level5.TEMP_GROSS_PRICE, selectedSimulationMonths))
    printOutput.developmentTrace("getDivisionResponse", getDivisionResponse(level5.ADJUSTED_REVENUE, level5.ADUSTED_QUANTITY, level5.HISTORICAL_POCKET_PRICE, selectedSimulationMonths, true))
    printOutput.developmentTrace("getSpecialPricesForLevel5", getMonthlySpecialPricesForLevel5())
    printOutput.developmentTrace("getAbsoluteDivisionResponse", getMonthlyAbsoluteDivisionResponse(level5.TEMP_POCKET_PRICE, level5.POCKET_PRICE, level5.CHANGE_RATIO, selectedSimulationMonths))
    printOutput.developmentTrace("getMonthlyHistoryPocketMargin", getMonthlyHistoryPocketMargin(level5.TOTAL_REVENUE, level5.QUANTITY_SOLD, level5.BASE_COST, level5.HISTORICAL_POCKET_PRICE, selectedSimulationMonths, true))
    printOutput.developmentTrace("getPricingTypeLevel4RevenueContribution", getPricingTypeLevel4RevenueContribution(pricingTypesConst.PER_DISCOUNT, pricingTypes.PER_DISCOUNT, level4.HISTORICAL_POCKET_PRICE, selectedSimulationMonths))
    List exceptions = [Library.getInitializedExceptionDetails("DefKey1",
                                                              false,
                                                              "LOWER(Material) LIKE LOWER('%123%')",
                                                              ['January': '0', 'February': '0.01', 'October': '0.085'],
                                                              ['January': '0.01', 'February': '0.04', 'October': '0.085'],
                                                              ['January': '0.0', 'September': '0.02']),
                       Library.getInitializedExceptionDetails("DefKey2",
                                                              false,
                                                              "LOWER(Material) LIKE LOWER('%568%')",
                                                              ['March': '0.01', 'October': '0.085'],
                                                              ['May': '0.04', 'September': '0.085'],
                                                              ['January': '0.0', 'August': '0.02']),
                       Library.getInitializedExceptionDetails("DefKey3",
                                                              false,
                                                              "LOWER(Material) = LOWER('%910%')",
                                                              ['January': '0', 'February': '0.01', 'October': '0.085'],
                                                              ['January': '0.01', 'October': '0.085'],
                                                              ['May': '0.01'])]
    printOutput.developmentTrace("getExceptionUnions", getExceptionUnions(exceptions, selectedSimulationMonths))
}

/**
 * baseField : ExpectedBaseCost
 * @param baseField
 * @param changedPercentage
 * @param aliasValue
 * @param selectedMonthsForSimulationList
 * @param isFirstSet
 * @return
 */
protected String getPredictionPerMonthlyChangedValue(String baseCostField,
                                                     String newCostField,
                                                     String baseMarginField,
                                                     String baseGrossPriceField,
                                                     boolean computePrediction,
                                                     BigDecimal targetMargin,
                                                     String simulationType,
                                                     String aliasValue,
                                                     List selectedMonthsForSimulationList) {

    List queryItems = []
    if (computePrediction) {
        Map simulationTypes = Constants.PREDICTION_SIMULATION_OPTIONS
        if (simulationType == simulationTypes.MAINTAIN_CURRENT_MARGIN_PERCENT || simulationType == simulationTypes.ATTAIN_TARGET_MARGIN_PERCENT) {
            if (simulationType == simulationTypes.ATTAIN_TARGET_MARGIN_PERCENT) {
                selectedMonthsForSimulationList.each { String monthPrefix ->
                    String newCost = monthPrefix + newCostField
                    String baseGrossPrice = monthPrefix + baseGrossPriceField
                    queryItems.add("((($newCost / (1-$targetMargin)) - $baseGrossPrice) / NULLIF($baseGrossPrice,0)) as ${monthPrefix + aliasValue}")
                }
            } else {
                selectedMonthsForSimulationList.each { String monthPrefix ->
                    String newCost = monthPrefix + newCostField
                    String baseCost = monthPrefix + baseCostField
                    String baseGrossPrice = monthPrefix + baseGrossPriceField
                    String exceptionColumn = monthPrefix + "CanGrossPriceChange"
                    queryItems.add("CASE WHEN $exceptionColumn = 0 THEN ${baseGrossPrice} ELSE (($newCost - $baseCost)/NULLIF($baseCost,0)) * ${baseGrossPrice} END as ${monthPrefix + aliasValue}")
                }
            }
        } else if (simulationType == simulationTypes.MAINTAIN_CURRENT_MARGIN) {
            selectedMonthsForSimulationList.each { String monthPrefix ->
                String baseCost = monthPrefix + baseCostField
                String newCost = monthPrefix + newCostField
                String baseMargin = monthPrefix + baseMarginField
                String baseGrossPrice = monthPrefix + baseGrossPriceField
                String exceptionColumn = monthPrefix + "CanGrossPriceChange"
                queryItems.add("CASE WHEN $exceptionColumn = 0 THEN ${baseGrossPrice} ELSE (${baseMargin}/NULLIF(${baseGrossPrice},0) + (1 + (${newCost} / NULLIF(${baseCost},0) - 1))/NULLIF((1 + ((${baseGrossPrice} - ${baseCost})/NULLIF(${baseCost},0))),0) - 1) * ${baseGrossPrice} END as ${monthPrefix + aliasValue}")
            }
        }
    } else {
        //If user has selected not to computePrediction, still adding the columns to the query
        //projection to keep the query same for all scenerios
        selectedMonthsForSimulationList.each { String monthPrefix ->
            queryItems.add("0 as ${monthPrefix + aliasValue}")
        }
    }

    return queryItems.join(",\n")
}