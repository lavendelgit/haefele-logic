deleteAllExistingRecords()?.each { Map rowToBeDeleted ->
    libs.__LIBRARY__.DBQueries.insertRecordIntoTargetRowSet(rowToBeDeleted)
}

protected List deleteAllExistingRecords() {
    String simulationDatamart = libs.HaefeleCommon.Simulation.SIMULATION_INPUT_DATAMART_MAP[out.SimulationName]
    def ctx = api.getDatamartContext()
    def table = ctx.getDatamart(simulationDatamart)
    final String numberOfRowsInTable = 'Material'
    def query = ctx.newQuery(table, false)
                   .select('Material', numberOfRowsInTable)
    List rowsToBeDeleted = ctx.executeQuery(query)?.getData()?.collect { row ->
        row.isDeleted = true
        return row
    }
    return rowsToBeDeleted

}