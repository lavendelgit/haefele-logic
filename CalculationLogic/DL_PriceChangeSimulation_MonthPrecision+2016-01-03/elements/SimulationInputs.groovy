List simulationInputs = out.MonthlyPricingSimulationInputs?.PricingMatrix.findAll { it -> it["Pricing Inputs For"] == "Overall Simulation" }
Map grossPriceChangePercentage = [:]
Map purchasePriceChangePercentage = [:]
Map quantityChangePercentage = [:]

Map simulationInputsDetails = [:]
BigDecimal grossPrice
BigDecimal purchasePrice
BigDecimal quantity

Constants.MONTH_NAMES.each { monthName ->
    Map simulationInputRow = simulationInputs.find { it -> it.Month == monthName }
    grossPrice = out.PredictGrossPricePercentChange ? BigDecimal.ZERO : (simulationInputRow?."Gross Price % Change" ?: BigDecimal.ZERO) as BigDecimal
    purchasePrice = (simulationInputRow?."Purchase Price % Change" ?: BigDecimal.ZERO) as BigDecimal
    quantity = (simulationInputRow?."Quantity % Change" ?: BigDecimal.ZERO) as BigDecimal

    grossPriceChangePercentage.put(monthName, grossPrice / 100)
    purchasePriceChangePercentage.put(monthName, purchasePrice / 100)
    quantityChangePercentage.put(monthName, quantity / 100)

}
simulationInputsDetails.put("grossPriceChangePercentage", grossPriceChangePercentage)
simulationInputsDetails.put("purchasePriceChangePercentage", purchasePriceChangePercentage)
simulationInputsDetails.put("quantityPriceChangePercentage", quantityChangePercentage)

return simulationInputsDetails