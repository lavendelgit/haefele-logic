def resultMatrix =  api.getDatamartContext()
                       .executeSqlQuery(
                               out.QueryBuilderInstance?.getCalculatePerRowSQLQuery()+ Constants.LIMIT_CLAUSE,
                               out.BaseDMQuery
                       )?.toResultMatrix()

resultMatrix.setPreferenceName("CalculatePerRowSQL")
return resultMatrix
