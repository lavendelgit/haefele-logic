def resultMatrix =  api.getDatamartContext()
                       .executeSqlQuery(
                               out.QueryBuilderInstance?.getAggregateByMaterialTxnSQLQuery()+ Constants.LIMIT_CLAUSE,
                               out.BaseDMQuery
                       )?.toResultMatrix()

resultMatrix.setPreferenceName("AggregateByMaterialTxnSQL")
return resultMatrix