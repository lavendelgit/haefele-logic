def getCurrentLPGToProcess(Map configs) {
    String value = configs['attribute1']
    value = (value) ? value + "%" : value
    def idList = api.find("PG", 0, 1, null, Filter.ilike("label", "${value}")).collect {
        [it.id, it.status, it.calculationDate]
    }
    if (!idList || idList.size() != 1 || idList[0][1] != "READY") {
        api.abortCalculation()
    }
    libs.__LIBRARY__.TraceUtility.developmentTrace("The LPG ID is...", idList)
    return idList
}

def getRecordsOfInterest(String yearInProcess, Long lpgID) {
    def recordForThatYear = []
    def lpgItems
    def count = 0
    def abortCalculation = false
    try {
        def filters = [Filter.equal("priceGridId", lpgID),
                       Filter.equal("attribute26", libs.__LIBRARY__.HaefeleSpecific.DEF_PPI_NSPI)]
        lpgItems = api.stream("PGI", 'sku', *filters)
        while (lpgItems?.hasNext()) {
            def articleDetails = lpgItems.next()
            recordForThatYear.add(getRecordForThatYear(yearInProcess, articleDetails))
        }
    }
    catch (Exception e) {
        libs.__LIBRARY__.TraceUtility.developmentTrace(".........Error in execution.......", e.getMessage())
        abortCalculation = true
    }
    finally {
        lpgItems?.close()
        if (abortCalculation) {
            api.abortCalculation()
        }
    }
    return recordForThatYear
}

def getRecordForThatYear(String yearInProcess, Map articleDetails, boolean test = true) {
    def dbColumns = ['sku', 'attribute10', 'attribute17', 'attribute27', 'attribute28', 'attribute29', 'attribute32',
                     'attribute33', 'attribute34', 'attribute35', 'attribute36']
    def outputColumns = ['Material', 'PPChangeCount', 'SPChangeCount', 'PurchasePriceChangeLoss', 'SalesPriceChangeGain',
                         'TotalPriceChangeImpact', 'TotalTransactions', 'RelevantTransactions', 'TotalTransactedUnits',
                         'RelevantTransactedUnits', 'EfficiencyCategory']
    def outputRecord = ['Year': yearInProcess]
/*
    if (test) {
        lastRunDetails = api.parseDateTime("yyyy-MM-dd'T'HH:mm:ss", lastRunDetails)
        libs.__LIBRARY__.TraceUtility.developmentTrace(".........The DateTimeThatIsSent..........", lastRunDetails)
    }
    def outputRecord = ['Year': yearInProcess]
*/
    int fieldCount = dbColumns.size()
    int i = 0
    while (i < fieldCount) {
        outputRecord[(outputColumns[i])] = articleDetails[(dbColumns[i])]
        i++
    }
    return outputRecord
}

def lpgToProcess = out.LPGToProcess
def lpgDetails
def outputRecordsToSave = [:]
lpgToProcess.each { entry ->
    lpgDetails = getCurrentLPGToProcess(entry)
    outputRecordsToSave[entry['name']] = getRecordsOfInterest(entry['name'], lpgDetails[0][0])
}
return outputRecordsToSave