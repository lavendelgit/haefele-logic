def getDataFromPriceGridSummaryQuery(def currentPG) {
    if (!currentPG)
        return []

    def query = api.getPriceGridSummaryQuery()
    query.addObjectTypedId(currentPG.typedId)
    query.setDoCount(true)
    query.setItemGroupBy("Inefficiency Status")
    query.addProjection("Impact", "SUM")
    def result = api.runSummaryQuery(query)
    return result
}

//countKey:13177, sum_impact_:-961011.7748597097, totalWeight_sum_impact_:0, groupKey:Needs Sales Price Improvement
def getOutputDataForYear(Map dataMap, def calculationDate, def participatingArticlesCount, def result) {
    def a2TotalImpact = 0, a3NeedsSalesPriceImprovement = 0, a4VeryEffiPricing = 0, a5PriceChangeArticleCount = 0, a6SkippedArtCount = 0
    def impactKey = 'sum_impact_'
    def countKey = 'countKey'
    result.each { entry ->
        switch (entry.groupKey) {
            case 'Needs Sales Price Improvement':
                a3NeedsSalesPriceImprovement = entry[impactKey]
                a5PriceChangeArticleCount += entry[countKey]
                break
            case 'No Inefficiency':
                a5PriceChangeArticleCount += entry[countKey]
                break
            case 'Skipped Article':
                a6SkippedArtCount = entry[countKey]
                break
            case 'Very Efficient Pricing':
                a4VeryEffiPricing = entry[impactKey]
                a5PriceChangeArticleCount += entry[countKey]
                break
            case 'TOTAL':
                a2TotalImpact = entry[impactKey]
                break
        }
    }
    def dateTimeOfRun = (calculationDate) ? (api.parseDateTime("yyyy-MM-dd'T'HH:mm:ss", calculationDate)) : null
    def dateOfRun = (dateTimeOfRun) ? ("${dateTimeOfRun.getYear()}-" +
            ((dateTimeOfRun.getMonthOfYear() < 10) ? "0" + dateTimeOfRun.getMonthOfYear() : dateTimeOfRun.getMonthOfYear()) +
            "-" + ((dateTimeOfRun.getDayOfMonth() < 10) ? "0" + dateTimeOfRun.getDayOfMonth() : dateTimeOfRun.getDayOfMonth()))
            : "Not Available"
    return ['key1'      : 'PriceChangeImpact', 'key2': 'Summary', 'key3': dataMap['name'],
            'attribute1': dataMap['Range'], 'attribute2': a2TotalImpact,
            'attribute3': a3NeedsSalesPriceImprovement, 'attribute4': a4VeryEffiPricing,
            'attribute5': a5PriceChangeArticleCount, 'attribute6': a6SkippedArtCount,
            'attribute7': dateOfRun, 'attribute8': participatingArticlesCount
    ]
}

def outputData = [:]
def pgName, currentPG, calculationDate, result, year, participatingArticlesCount
out.LPGToProcess.each { dataMap ->
    year = dataMap['name']
    pgName = dataMap['attribute1']
    def pgList = api.findPriceGrids(java.util.Date.newInstance(), pgName)
    if (pgList.size() == 1) {
        currentPG = pgList[0]
        calculationDate = currentPG.calculationDate
        participatingArticlesCount = currentPG.numberOfItems
        result = getDataFromPriceGridSummaryQuery(currentPG)
        outputData[year] = getOutputDataForYear(dataMap, calculationDate, participatingArticlesCount, result)
    }
}
libs.__LIBRARY__.TraceUtility.developmentTrace("Printing Data to save", outputData)

return outputData