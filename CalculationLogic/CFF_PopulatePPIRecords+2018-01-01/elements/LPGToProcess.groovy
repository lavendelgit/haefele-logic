def filters = []
def values = api.findLookupTableValues("PPECConfig", *filters)
int currentIndex = 1
values.each { record ->
    lib.TraceUtility.developmentTraceRow("The configuration...${currentIndex} ", record)
    currentIndex++
}
return values