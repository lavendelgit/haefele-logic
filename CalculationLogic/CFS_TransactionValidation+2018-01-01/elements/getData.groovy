def sku = api.currentItem("sku")

def invoiceDate =api.currentItem("invoice_date")


return latestSalesPriceRecord(sku,invoiceDate)

//get latest values of zplp

def latestSalesPriceRecord(String sku, invoiceDate) {
    def filters = [
            Filter.equal("name", "S_ZPLP"),
            Filter.like("sku", sku + "%")//,
      		//Filter.greaterOrEqual("attribute1", invoiceDate),
      		//Filter.lessOrEqual("attribute2", invoiceDate)
    ]

    def orderBy = "-attribute1" // Valid From (de: Gültig von) - descending
    def salesPrices = api.find("PX50", 0, 1, orderBy, *filters)
  api.trace("salesPrices",salesPrices)
    def result = [:]

    if (salesPrices[0]) {
        result = salesPrices[0]
    }

    //api.trace("lib.Find.latestSalesPriceRecord", "sku: ${sku}, additionalFilters: ${additionalFilters}", result)

    return result.attribute3
}