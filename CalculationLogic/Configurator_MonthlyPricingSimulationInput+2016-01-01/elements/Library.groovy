import net.pricefx.common.api.FieldFormatType
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ContextParameter


/**
 * This is a "hack" to save the input value that is not persisted between logic refreshes on changes
 * @param calculationType
 * @return
 */
protected ConfiguratorEntry createInputConfiguratorWithHiddenStoredValue(String pageName) {
    def inputConfigurator = api.createConfiguratorEntry(InputType.HIDDEN, "calculationType")
    if (pageName) {
        inputConfigurator.getFirstInput().setValue(pageName)
    }

    return inputConfigurator
}


private ConfiguratorEntry createDatamartFilterBuilder(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String dmName, List filters, Closure selValueHandler = null) {
    def filterInput = configuratorEntry.createParameter(InputType.DMFILTERBUILDER, fieldName)
    filterInput.addParameterConfigEntry("dmSourceName", dmName)
    filterInput.addParameterConfigEntry("noRefresh", true)
    filterInput.setLabel(fieldLabel)
               .setRequired(false)
               .setReadOnly(false)


    if (filters) {
        Map filterConfig = [:]
        filterConfig['criteria'] = Filter.and(*filters)
        filterInput.setFilter(filterConfig)
    }

    if (selValueHandler) {
        String strFilter = null
        if (filterInput.getValue() instanceof Map) {
            strFilter = api.filterFromMap(filterInput.getValue()).toQueryString()
        }
        selValueHandler.call(strFilter)
    }

    return configuratorEntry
}


protected ConfiguratorEntry createMatrixInput(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, List matrixRows, Map inputs, Closure selValueHandler = null) {
    ContextParameter matrix = configuratorEntry.createParameter(InputType.INPUTMATRIX, fieldName)
    Map COLUMNS = Constants.MATRIX_COLUMNS
    List columnDefs = COLUMNS.values().sort { it.matrixColSeq }
    List hiddenColumns = columnDefs.findAll { Map columnDef ->
        columnDef.type == 'Hidden'
    }.label

    List readOnlyColumns = columnDefs.findAll { Map columnDef ->
        columnDef.matrixColReadOnly == true
    }.label

    String monthColumn = COLUMNS.Month.label
    String typeColumn = COLUMNS.InputType.label
    String exceptionKeyColumn = COLUMNS.ExceptionKey.label
    boolean hasExceptionKeyData = inputs[COLUMNS.ExceptionKey.name] != null
    matrix.setLabel(fieldLabel)
    matrix.addParameterConfigEntry('columns', columnDefs.label)
    matrix.addParameterConfigEntry("canModifyRows", true)
    matrix.addParameterConfigEntry("enableClientFilter", true)
    if (readOnlyColumns) {
        matrix.addParameterConfigEntry("readOnlyColumns", readOnlyColumns)
    }
    matrix.addParameterConfigEntry("columnType", columnDefs.matrixColType)
    if (hiddenColumns) {
        matrix.addParameterConfigEntry("hiddenColumns", hiddenColumns)
    }

    matrixRows = matrix.getValue() ?: []
    boolean addOrUpdateRow = inputs.AddRow
    Map newMatrixRow = [:]
    if (addOrUpdateRow) {
        newMatrixRow = COLUMNS.collectEntries { String columnName, Map columnDef ->
            [(columnDef.label): inputs[columnDef.name]]
        }
        newMatrixRow.RowKey = hasExceptionKeyData ? (newMatrixRow[typeColumn] + "_" + newMatrixRow[exceptionKeyColumn] + "_" + newMatrixRow[monthColumn]) : (newMatrixRow[typeColumn] + "_" + newMatrixRow[monthColumn])
    }


    boolean updateRow = false
    matrixRows = matrixRows.collect { Map matrixRow ->
        //Set data from inputs
        if (addOrUpdateRow && newMatrixRow.RowKey == matrixRow.RowKey) {
            //Has Matching Row
            updateRow = true
            COLUMNS.each { String columnName, Map columnDef ->
                matrixRow[columnDef.label] = inputs[columnDef.name]
            }
            matrixRow.RowKey = newMatrixRow.RowKey
        }
        return matrixRow
    }

    if (!updateRow) {
        matrixRows.add(newMatrixRow)
    }

    matrixRows = matrixRows?.findAll { Map matrixRow ->
        matrixRow[monthColumn] != null && matrixRow[typeColumn] != null
    }

    if (hasExceptionKeyData) {
        matrixRows = matrixRows?.sort { Map matrixRow1, Map matrixRow2 ->
            matrixRow1[typeColumn] <=> matrixRow2[typeColumn] ?: matrixRow1[exceptionKeyColumn] <=> matrixRow2[exceptionKeyColumn] ?: Constants.MONTHS.indexOf(matrixRow1[monthColumn]) <=> Constants.MONTHS.indexOf(matrixRow2[monthColumn])
        }
    } else {
        matrixRows = matrixRows?.sort { Map matrixRow1, Map matrixRow2 ->
            matrixRow1[typeColumn] <=> matrixRow2[typeColumn] ?: Constants.MONTHS.indexOf(matrixRow1[monthColumn]) <=> Constants.MONTHS.indexOf(matrixRow2[monthColumn])
        }
    }

    matrix.setValue(matrixRows)
    if (selValueHandler) {
        selValueHandler.call(matrix.getValue())
    }

    return configuratorEntry
}

protected ConfiguratorEntry createPercentField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, BigDecimal defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.USERENTRY, fieldName)
    input.setValueHint('%')
    if (!input.getValue() && defaultValue != null) {
        input.setValue(defaultValue)
    }
    input.setConfigParameter("dataType", "float")
    input.setConfigParameter("formatType", FieldFormatType.PERCENT)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)

    return configuratorEntry
}

protected ConfiguratorEntry createDropdown(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, List optionValues, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.OPTION, fieldName)
    input.setValueOptions(optionValues)
    if (input.getValue() == null && defaultValue != null) {
        input.setValue(defaultValue)
    }
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    return configuratorEntry
}

protected ConfiguratorEntry createTextField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, String defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.STRINGUSERENTRY, fieldName)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    return configuratorEntry
}

protected ConfiguratorEntry createHiddenField(ConfiguratorEntry configuratorEntry, String fieldName, Object value, Closure setValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.HIDDEN, fieldName)
    input.setValue(value)
    if (setValueHandler) {
        setValueHandler.call(input.getValue())
    }
    return configuratorEntry
}

protected ConfiguratorEntry createBooleanField(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, boolean defaultValue, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.BOOLEANUSERENTRY, fieldName)
    manageFieldAttributes(input, fieldLabel, isRequired, isReadOnly, isNoRefresh, selValueHandler)
    if (!input.getValue() && defaultValue != null) {
        input.setValue(defaultValue)
    }
    return configuratorEntry
}

protected ConfiguratorEntry createButton(ConfiguratorEntry configuratorEntry, String fieldName, String fieldLabel, Closure clickHandler = null) {
    ContextParameter input = configuratorEntry.createParameter(InputType.BUTTON, fieldName)
    input.setLabel(fieldLabel)
    if (clickHandler) {
        clickHandler.call(input.getValue())
    }
    input.setValue(false)
    return configuratorEntry
}

protected void manageFieldAttributes(ContextParameter input, String fieldLabel, Boolean isRequired = false, Boolean isReadOnly = false, Boolean isNoRefresh = false, Closure selValueHandler = null) {
    input.setRequired(isRequired)
         .setReadOnly(isReadOnly)
         .setLabel(fieldLabel)
         .addParameterConfigEntry("noRefresh", isNoRefresh)

    if (selValueHandler) {
        selValueHandler.call(input.getValue())
    }
}

protected boolean hasFieldValueChanged(String fieldName) {
    return api.input(fieldName) != api.input(getStateFieldName(fieldName))
}

protected String getStateFieldName(String fieldName) {
    return "__${fieldName}__"
}

protected def getPrevFieldValue(String fieldName) {
    return api.input(getStateFieldName(fieldName))
}

