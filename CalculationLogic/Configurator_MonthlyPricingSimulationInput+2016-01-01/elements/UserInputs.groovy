import net.pricefx.server.dto.calculation.ConfiguratorEntry

api.local.logs = []
Map inputs = [:]
ConfiguratorEntry inputConfigurator = Library.createInputConfiguratorWithHiddenStoredValue("Exceptions")
String msg = "<span style='font-weight:bold;color:silver;'> MONTHLY PRICING SIMULATION DATA ENTRY</span><hr>"
msg += "<p style='font-style:italic;color:green;font-size:11px'>To ADD / UPDATE entry, please provide value and click on '✅' button. You can additionally edit the Change '%' value in the matrix itself.</p>"
inputConfigurator.setMessage(msg)

Constants.MATRIX_COLUMNS.each { String inputDefName, Map inputDef ->
    if (!inputDef.dependsOn || (inputDef.dependsOn && inputs[inputDef.dependsOn] == inputDef.dependsOnValue)) {
        String inputName = inputDef.name
        switch (inputDef.type) {
            case "DatamartFilter":
                Library.createDatamartFilterBuilder(inputConfigurator, inputName, inputDef.label, Constants.DM_SIMULATION, []) {
                    selValue -> inputs[inputName] = selValue
                }
                break
            case "Percent":
                Library.createPercentField(inputConfigurator, inputName, inputDef.label, 0.0, false, false, inputDef.isNoRefresh) {
                    selValue -> inputs[inputName] = selValue * 100
                }
                break
            case "Text":
                Library.createTextField(inputConfigurator, inputName, inputDef.label, null, false, false, inputDef.isNoRefresh) {
                    selValue -> inputs[inputName] = selValue
                }
                break
            case "Hidden":
                Library.createHiddenField(inputConfigurator, inputName, null) {
                    selValue -> inputs[inputName] = selValue
                }
                break
            case "Option":
                List options = inputDef.options
                if (!options && inputDef.optionsLoader) {
                    options = inputDef.optionsLoader()
                }

                Library.createDropdown(inputConfigurator, inputName, inputDef.label, inputDef.defaultValue ?: inputs[inputName], options, inputDef.isRequired ?: false, false, inputDef.isNoRefresh) {
                    selValue -> inputs[inputName] = selValue
                }
                break
        }
    }
}
Library.createButton(inputConfigurator, "AddRow", " ✅ Add / Update Pricing Simulation Entry ") {
    selValue -> inputs.AddRow = selValue
}

Library.createMatrixInput(inputConfigurator, 'PricingMatrix', '☳ Monthly Pricing Simulation Inputs', null, inputs) {
    selValue -> inputs.PricingMatrix = selValue
}


return inputConfigurator