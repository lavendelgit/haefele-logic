import groovy.transform.Field
import net.pricefx.common.api.FieldFormatType

@Field String DM_SIMULATION = 'PriceChangeSimulationDM_2020'
@Field List MONTHS = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
]

@Field Map INPUT_TYPES = [
        //INTERNAL_ARTICLES: 'Internal Articles',
        EXCEPTIONS: 'Exceptions',
        SIMULATION: 'Overall Simulation'
]

@Field Map MATRIX_COLUMNS = [
        InputType                 : [name: 'InputType', type: 'Option', matrixColType: FieldFormatType.TEXT, matrixColReadOnly: true, matrixColSeq: 1, label: 'Pricing Inputs For', options: INPUT_TYPES.values() as List, isRequired: true, defaultValue: INPUT_TYPES.SIMULATION, isNoRefresh: false],
        ExceptionKey              : [name: 'ExceptionKey', type: 'Text', matrixColType: 'Text', matrixColSeq: 3, label: 'Exception Key', dependsOn: "InputType", dependsOnValue: INPUT_TYPES.EXCEPTIONS, isNoRefresh: true],
        ExceptionFilter           : [name: 'ExceptionFilter', type: 'DatamartFilter', matrixColSeq: 4, matrixColType: 'Text', label: 'Exception Filter', dependsOn: "InputType", dependsOnValue: INPUT_TYPES.EXCEPTIONS, isNoRefresh: true],
        Month                     : [name: 'Month', type: 'Option', matrixColType: FieldFormatType.TEXT, matrixColReadOnly: true, matrixColSeq: 2, label: 'Month', options: MONTHS, defaultValue: 'January', isNoRefresh: true],
        GrossPricePercentChange   : [name: 'GrossPricePercentChange', type: 'Percent', matrixColType: FieldFormatType.PERCENT, matrixColSeq: 5, label: 'Gross Price % Change', isNoRefresh: true],
        PurchasePricePercentChange: [name: 'PurchasePricePercentChange', type: 'Percent', matrixColType: FieldFormatType.PERCENT, matrixColSeq: 6, label: 'Purchase Price % Change', isNoRefresh: true],
        QuantityPercentChange     : [name: 'QuantityPercentChange', type: 'Percent', matrixColType: FieldFormatType.PERCENT, matrixColSeq: 7, label: 'Quantity % Change', isNoRefresh: true],
        RowKey                    : [name: 'RowKey', type: 'Hidden', matrixColType: FieldFormatType.TEXT, matrixColReadOnly: true, matrixColSeq: 8, label: 'RowKey', isNoRefresh: true]
]


