feedObjects()

def feedObjects() {
    def customers = getAllCustomersToProcess()
    def curCustomer
    customers.each { customerId, value ->
        curCustomer = api.customer("id", customerId) as Long
        api.emitPersistedObject("C", curCustomer)
    }
}

def getAllCustomersToProcess() {
    def filters = [
            Filter.equal("name", "CustomerSalesPrice"),
        Filter.in("attribute1",
                      ['0002776424', '0002776142', '0001026062', '0001671165', '0001023892', '0001158671',
                       '0001003467', '0001152670', '0002295445', '0001024340', '0001269475'])
    ]
    def zpapCustomersStream = api.stream("PX", 'attribute1', ["sku", "attribute1"], *filters)
    def zpapCustomers = [:]
    if (zpapCustomersStream) {
        while (zpapCustomersStream.hasNext()) {
            def zpapEntry = zpapCustomersStream.next()
            zpapCustomers.put(zpapEntry.attribute1, zpapEntry.attribute1)
        }
        zpapCustomersStream?.close()
    }

    lib.TraceUtility.developmentTrace("Total number of customers retrieved", (zpapCustomers) ? zpapCustomers.size() + "" : "")
    return zpapCustomers
}