def currentItem = api.getElement("CurrentItem")
def currentSalesPrice = api.getElement("CurrentSalesPrice")
def recommendedPrice = currentItem.attribute1

if (currentSalesPrice == null) {
    api.delete("PX", [
            name: "RecommendedPrice",
            id: currentItem.id
    ])
}

if (recommendedPrice > currentSalesPrice) {
    api.update("PX", [
            name: "RecommendedPrice",
            id: currentItem.id,
            sku: currentItem.sku,
            attribute1: currentSalesPrice
    ])
}