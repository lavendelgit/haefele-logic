def currentItem = api.currentItem()

if (api.isSyntaxCheck()) {
    return [:]
}

if (!currentItem) {
    currentItem = api.productExtension("RecommendedPrice")?.find()
}

api.trace(currentItem)

return currentItem