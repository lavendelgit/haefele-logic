def dmCtx = api.getDatamartContext()
def salesDM = dmCtx.getTable("SalesHistoryDM")
def datamartQuery = dmCtx.newQuery(salesDM)

/*
def filters =
]
*/

datamartQuery.select("SUM(Revenue)", "revenue")
datamartQuery.select("SUM(MarginalContributionAbs)", "margin")
datamartQuery.select("YearMonthMonth", "yearMonth")
datamartQuery.where(api.getElement("CustomerFilter"))

def result = dmCtx.executeQuery(datamartQuery)?.getData()?.collect()

api.trace("result", "", result)

return result