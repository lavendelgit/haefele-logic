def pp = api.findLookupTable("MarginMonthly2")

if (!pp) {
    api.addWarning("Pricing parameter not found (MarginMonthly2)")
    return
}

def salesSummary = api.getElement("SalesSummary")

for (s in salesSummary) {
    def year = s.yearMonth.substring(0,4).toInteger()
    def month = s.yearMonth.substring(6).toInteger()
    def record = [
            "lookupTableId": pp.id,
            "lookupTableName": pp.uniqueName,
            "key1": year,
            "key2": month,
            "attribute1": s.margin/s.revenue,
            "attribute2": s.margin,
            "attribute3": s.revenue
    ]
    api.trace("addOrUpdate", "Year: ${year}, Month: ${month}", record)
    api.addOrUpdate("MLTV2", record)
}

