def filter = api.customerGroupEntry("Customer Filter")
def defaultFilter = Filter.or(
        Filter.equal("Verkaufsbereich2", "Handel"),
        Filter.equal("Verkaufsbereich2", "Lieferant"),
        Filter.equal("Verkaufsbereich3", "Berlin"),
        Filter.equal("Verkaufsbereich3", "Frankfurt"),
        Filter.equal("Verkaufsbereich3", "Hannover"),
        Filter.equal("Verkaufsbereich3", "Kaltenkirchen"),
        Filter.equal("Verkaufsbereich3", "Köln"),
        Filter.equal("Verkaufsbereich3", "München"),
        Filter.equal("Verkaufsbereich3", "Münster"),
        Filter.equal("Verkaufsbereich3", "Naumburg"),
        Filter.equal("Verkaufsbereich3", "Nürnberg"),
        Filter.equal("Verkaufsbereich3", "Stuttgart Airport"),
)

return filter?.asFilter() ?: defaultFilter