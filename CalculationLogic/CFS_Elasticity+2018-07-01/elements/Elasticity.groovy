package CFS_Elasticity.elements

def sku = api.getElement("SKU")
def perfeclyElasticTreshold = api.getElement("PerfectlyElasticThreshold")

def filters = [
        *lib.Filter.customersGermany()
]

return lib.Elasticity.calculate(sku, perfeclyElasticTreshold, *filters)