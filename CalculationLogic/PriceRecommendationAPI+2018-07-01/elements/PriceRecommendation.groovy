package PriceRecommendationAPI.elements

if (api.isSyntaxCheck()) {
    return
}

def sku = api.product("sku")
def newSalesPrice = null

try {
    newSalesPrice = api.getElement("NewSalesPrice")?.toBigDecimal()
} catch (e) {
    return [
            status: "error",
            message: "NewSalesPrice is not a number"
    ]
}

if (newSalesPrice == null) {
    return [
        status: "error",
        message: "NewSalesPrice not specified"
    ]
}

def bepResult = lib.BEP.calculate(sku, newSalesPrice)

api.trace("bepResult", "", bepResult)


if (bepResult.recommendedSalesPrice == null) {
    return [
        status: "not-enough-information",
        message: "Not enough information.",
        details: bepResult.warning
    ]
} else if (bepResult.newSalesPrice < bepResult.recommendedSalesPrice) {
    return [
        status: "rejected",
        minMargin: bepResult.minMargin,
        minSalesPrice: bepResult.minMarginPriceBeforeDiscounts,
        message: "Unfortunately the recommended price has a DB% lower than the required ${bepResult.minMargin * 100}%.\n" +
                 "The minimum sales price for this product is € ${bepResult.minMarginPriceBeforeDiscounts}"
    ]
} else {
    return [
        status: "acceptable",
        lastSixMonthsAvgSalesPrice: bepResult.avgSalesPriceLast6M,
        previousYearAvgSalesPrice: bepResult.avgSalesPrice,
        previousYearQuantity: bepResult.previousYearQuantity,
        quantityCompensatingRevenue: bepResult.quantityCompensatingRevenue,
        quantityCompensatingMargin: bepResult.quantityCompensatingMargin,
        quantityIncreaseBasedOnRevenue: bepResult.quantityIncreaseBasedOnRevenue,
        quantityIncreaseBasedOnMargin: bepResult.quantityIncreaseBasedOnMargin,
        message: "Thank you for your input. Your request has been sent directly to the Pricing Team. " +
                 "In the meantime please observe the following analysis:\n" +
                 "The average unit sales price for this product in the last 6 months is: € ${bepResult.avgSalesPriceLast6M}\n" +
                 "If the recommended price were use:\n" +
                 "Necessary increase in sales (revenue-based): ${bepResult.quantityIncreaseBasedOnRevenue?.setScale(2, BigDecimal.ROUND_HALF_UP) * 100}%\n" +
                 "Necessary increase in sales (marginal contribution-based): ${bepResult.quantityIncreaseBasedOnMargin?.setScale(2, BigDecimal.ROUND_HALF_UP) * 100}%"
    ]
}

