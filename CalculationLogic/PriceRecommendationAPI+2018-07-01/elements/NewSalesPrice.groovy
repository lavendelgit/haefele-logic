package PriceRecommendationAPI.elements

def newSalesPrice = api.userEntry("NewSalesPrice")

api.logInfo("newSalesPrice", newSalesPrice)

return newSalesPrice