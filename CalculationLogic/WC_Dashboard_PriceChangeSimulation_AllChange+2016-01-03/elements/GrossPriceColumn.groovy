if (out.GrossPriceChangePer == null) {
    return ""
}
Map DATA_COLUMNS = Constants.DATA_COLUMNS_DEF
String grossPriceColumn = DATA_COLUMNS.EXPECTED_GROSS_PRICE.name
boolean isGrossPriceChange = !out.GrossPriceChangePer
grossPriceColumn += isGrossPriceChange ? " (with ${out.GrossPriceChangePer * 100} % change)" : ''

return grossPriceColumn