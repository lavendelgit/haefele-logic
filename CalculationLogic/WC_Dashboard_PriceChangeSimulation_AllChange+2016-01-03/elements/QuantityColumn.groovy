if (out.QuantityChangePer == null) {
    return ""
}
Map DATA_COLUMNS = Constants.DATA_COLUMNS_DEF
String quantityColumn = DATA_COLUMNS.EXPECTED_LANDING_COST.name
boolean isQuantityChange = !out.QuantityChangePer
quantityColumn += isQuantityChange ? " (with ${out.QuantityChangePer * 100} % change)" : ''

return quantityColumn