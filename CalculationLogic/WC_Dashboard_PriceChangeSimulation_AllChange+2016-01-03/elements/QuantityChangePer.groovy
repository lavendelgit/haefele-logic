BigDecimal changePercent = out.QuantityChangePercent
return (changePercent != null ? changePercent : Constants.DEFAULT_CHANGE_PERCENT.QUANTITY) / 100