import groovy.transform.Field

@Field Map EXCEPTION_COLUMNS = [
        ExceptionKey           : [name: 'ExceptionKey', type: 'Text', matrixColType: 'Text', label: 'Key'],
        ExceptionFilter        : [name: 'ExceptionFilter', type: 'DatamartFilter', matrixColType: 'Text', label: 'Data Filter'],
        MonthlyExceptionConfiguration: [name: 'MonthlyExceptionConfiguration', type: 'Option', matrixColType: 'Text', label: 'Monthly Exception Configuration']
]

@Field String INPUT_CONFIGURATION_TABLE_NAME = "MonthlyPricingSimulationInput"
@Field String INPUT_CONFIGURATION_EXCEPTION = "Exceptions Inputs"
@Field String INPUT_CONFIGURATION_SIMULATION = "Simulation Inputs"
@Field String INPUT_CONFIGURATION_INTERNAL_ARTICLE = "Internal Articles"

/**
 *
 * @param configurationName
 * @return
 */
protected List getPriceChangeExceptionInputsReferenceName(String configurationName) {
    List exceptionInputsRefNames = api.findLookupTableValues(INPUT_CONFIGURATION_TABLE_NAME, Filter.equal("key1", configurationName))
    return exceptionInputsRefNames?.collect {
        it.key2
    }?.unique().sort()
}

@Field String SIMULATION_1 = 'Simulation 1'
@Field String SIMULATION_2 = 'Simulation 2'
@Field String SIMULATION_3 = 'Simulation 3'
@Field String SIMULATION_4 = 'Simulation 4'
@Field String SIMULATION_5 = 'Simulation 5'
@Field String SIMULATION_6 = 'Simulation 6'

@Field Map SIMULATION_INPUT_DATAMART_MAP = [(SIMULATION_1):'PriceChangeSimulation_Result_V1',
                                            (SIMULATION_2):'PriceChangeSimulation_Result_V2',
                                            (SIMULATION_3):'PriceChangeSimulation_Result_V3',
                                            (SIMULATION_4):'PriceChangeSimulation_Result_V4',
                                            (SIMULATION_5):'PriceChangeSimulation_Result_V5',
                                            (SIMULATION_6):'PriceChangeSimulation_Result_V6']
@Field List ANALYSIS_YEARS = ['2020']

@Field Map SIMULATION_INPUT_COLUMNS = [
        Status              : [name: 'Status', type: 'Hidden', label: 'Status', readOnly: true, hidden: true],
        //SimulationTitle     : [name: 'SimulationTitle', type: 'Text', label: 'Simulation Title'],
        AnalysisYear        : [name: 'AnalysisYear', type: 'Option', label: 'Analysis Year', optionsLoader: {
            return ANALYSIS_YEARS
        }],
        TargetYear          : [name: 'TargetYear', type: 'Option', label: 'Target Year', optionsLoader: {
            return getSimulationTargetYears()
        }],
        SimulationDataFilter: [name: 'SimulationDataFilter', type: 'DatamartFilter', label: 'Include rows for analysis'],
        SimulationPercentageChangeInput        : [name: 'SimulationPercentageChangeInput', type: 'Option', label: 'Simulation Percentage Change', optionsLoader: {
            return getPriceChangeExceptionInputsReferenceName(INPUT_CONFIGURATION_SIMULATION)
        }],
        Exceptions          : [name: 'Exceptions', type: 'PopupConfigurator', label: 'Exceptions', configuratorLogic: 'Configurator_MonthlySimulationExceptionConditions']
]

List getSimulationTargetYears() {
    int year = api.calendar().get(java.util.Calendar.YEAR)
    return (0..2).collect { int idx ->
        (year + idx) + ""
    }
}

void callBoundCallUtilFunction(String simulationName, String simStatus, String simulationOwner, String simulationDescription, Map simulation_inputs) {
    Date today = new Date()
    String notify = (simStatus == "Ready") ? "Yes" : "No"
    Map simulationRow = [status                : simStatus,
                         schedule_date         : today.format("yyyy-MM-dd'T'HH:mm:ss.SSS"),
                         simulation_owner      : simulationOwner,
                         simulation_description: simulationDescription,
                         simulation_inputs     : simulation_inputs,
                         simulation_notify	   : notify]
    libs.HaefeleCommon.BoundCallUtils.addOrUpdateJSONTableData("SimulationInputs", null, "JLTV", [simulationName], [simulationRow])
}

@Field Map SIMULATION_STATUS = [
        NEW       : [name: 'New', label: 'New', allowSimulation: true, info: 'Please provide input for simulation.', confirmAllowSimulation: true],
        SCHEDULED : [name: 'Scheduled', label: 'Scheduled', allowSimulation: false, info: 'Simulation processing has been scheduled. Please try executing the dashboard after sometime.'],
        PROCESSING: [name: 'Processing', label: 'Processing', allowSimulation: false, info: 'Simulation data is being processed. Please try executing the dashboard after sometime.'],
        ERROR     : [name: 'Error', label: 'Error', allowSimulation: true, info: 'Error processing simulation request. Please try again or contact support.', color: 'red', confirmAllowSimulation: true],
        READY     : [name: 'Ready', label: 'Ready', allowSimulation: true, info: "Simulation data is ready. Please click on 'Apply Settings' to see the data.", confirmAllowSimulation: true, color: '#2F7ED8']
]

