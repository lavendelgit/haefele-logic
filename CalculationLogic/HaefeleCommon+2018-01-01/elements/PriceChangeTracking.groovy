import groovy.transform.Field

@Field final String PX_TABLE_FILTER_LABEL = "Pricing Conditions"
@Field final String PX_ZPAP_TABLE = "S_ZPAP"
@Field final String PX_ZPF_TABLE = "S_ZPF"
@Field final String PX_ZPM_TABLE = "S_ZPM"
@Field final String PX_ZPP_TABLE = "S_ZPP"
@Field final String PX_ZPA_TABLE = "S_ZPA"
@Field final String PX_ZPC_TABLE = "S_ZPC"
@Field final String PX_ZPZ_TABLE = "S_ZPZ"
@Field final String PX_ZNRV_TABLE = "S_ZNRV"
@Field final String PX_ZPE_TABLE = "S_ZPE"

@Field final String VALIDATION_NO_PRICE_CHANGE = "No Price Change"
@Field final String VALIDATION_PRICE_REDUCTION = "Price Reduction"
@Field final String VALIDATION_PRICE_INCREASE_ABOVE_REF = "Price Increase > "
@Field final String VALIDATION_PRICE_INCREASE_BELOW_REF = "Price Increase < "
@Field final String VALIDATION_NO_VOILATION = "No Violation"
@Field final String VALIDATION_PRICE_INCREASE = "Price Increase"
@Field String INPUT_CONFIGURATION_NET_PRICE_TRACKING = "Net Price Tracking"
@Field String INPUT_CONFIGURATION_PURCHASE_PRICE_TRACKING = "Purchase Price Tracking"
@Field String INPUT_CONFIGURATION_LANDED_COST_TRACKING = "Landed Cost Tracking"
@Field String INPUT_CONFIGURATION_PURCHASE_PRICE_AND_LANDED_COST_TRACKING = "Purchase Price and Landed Cost Tracking"

/**getMetadataForConditionTables
 *
 * @return
 */
Map getMetadataForConditionTables() {
    Map conditionTableAttributes = api.findLookupTableValues("PriceChangeTrackingMetaConfigurations", "-key1")?.collectEntries { it ->
        Map row = [
                conditionName           : it.attribute1,
                pxTableLabel            : it.key1,
                pxTablename             : it.key2,
                conditionType           : it.attribute2,
                costAttribute           : it.attribute3,
                validFromAttribute      : it.attribute4,
                validToAttribute        : it.attribute9,
                priceChangeTypeAttribute: it.attribute5,
                perDiffAttribute        : it.attribute6,
                keyAttributesName       : it.attribute7,
                keyAttributesColumnName : it.attribute8,
                sortByAttribute         : it.attribute10,
                previousCostAttribute   : it.attribute11,
                tableTypeCode           : it.attribute12,
                ppValidFromAttribute    : it.attribute13,
                ppValidToAttribute      : it.attribute14,
        ]

        [(it.key2): row]
    }
    return conditionTableAttributes
}