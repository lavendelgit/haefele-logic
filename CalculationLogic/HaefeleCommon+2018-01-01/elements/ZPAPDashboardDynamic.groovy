import net.pricefx.common.api.FieldFormatType


def getZPAPAnalysisSummaryEntries(String effectiveYear) {
    def filters = [Filter.equal("key1", 'ZPAPAnalysis'), Filter.equal("key3", effectiveYear)]
    def values = api.findLookupTableValues("PreCalculatedDashboardField", *filters)
    def columns = ['ActiveCustomerCount', 'TransactedMaterialCount', 'CustomerTransacted', 'ZPAPCustomerCount',
                   'ZPAPArticleCount', 'ZPAPCount', 'TotalArticlesCount', 'ZPAPMinDiscountPer', 'ZPAPMaxDiscountPer',
                   'ZPAPAvgDiscountPer', 'ZPAPCountGTPerMaxDiscPer', 'MaterialCountGTPerMaxDiscPer', 'CustomerCountGTPerMaxDiscPer',
                   'ZPAPTransactedMaterialCount']
    boolean isDataAvailable = (values)
    def summaryData = [
            'ActiveCustomerCount'         : ((isDataAvailable) ? getValue(values, columns[0]) : 0),
            'TransactedMaterialCount'     : ((isDataAvailable) ? getValue(values, columns[1]) : 0),
            'CustomerTransacted'          : ((isDataAvailable) ? getValue(values, columns[2]) : 0),
            'ZPAPCustomerCount'           : ((isDataAvailable) ? getValue(values, columns[3]) : 0),
            'ZPAPArticleCount'            : ((isDataAvailable) ? getValue(values, columns[4]) : 0),
            'ZPAPCount'                   : ((isDataAvailable) ? getValue(values, columns[5]) : 0),
            'TotalArticlesCount'          : ((isDataAvailable) ? getValue(values, columns[6]) : 0),
            'ZPAPMinDiscountPer'          : ((isDataAvailable) ? getValue(values, columns[7]) : 0),
            'ZPAPMaxDiscountPer'          : ((isDataAvailable) ? getValue(values, columns[8]) : 0),
            'ZPAPAvgDiscountPer'          : ((isDataAvailable) ? getValue(values, columns[9]) : 0),
            'ZPAPCountGTPerMaxDiscPer'    : ((isDataAvailable) ? getValue(values, columns[10]) : 0),
            'MaterialCountGTPerMaxDiscPer': ((isDataAvailable) ? getValue(values, columns[11]) : 0),
            'CustomerCountGTPerMaxDiscPer': ((isDataAvailable) ? getValue(values, columns[12]) : 0),
            'ZPAPTransactedMaterialCount' : ((isDataAvailable) ? getValue(values, columns[13]) : 0)
    ]
    return summaryData
}

def getZPAPAnalysisSummaryMatrix(Map overviewData, String effectiveYear) {
    def columns = ['Title', 'Total Count', '% of Total Count']
    def columnTypes = [FieldFormatType.TEXT, FieldFormatType.INTEGER, FieldFormatType.PERCENT]
    def dashUtl = libs.__LIBRARY__.DashboardUtility
    def resultMatrix = dashUtl.newResultMatrix('ZPAP Summary (Year-' + effectiveYear + ')', columns, columnTypes) // MK
    def mathLib = libs.__LIBRARY__.MathUtility
    def activeZPAPCount = (overviewData['ZPAPCount']) ?: 0
    resultMatrix.addRow([(columns[0]): "Number of ZPAPs (per customer per material net price) during $effectiveYear year",
                         (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", activeZPAPCount)),
                         (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(activeZPAPCount, activeZPAPCount))])

    def activeCustomerCount = (overviewData['ActiveCustomerCount']) ?: 0
    resultMatrix.addRow([(columns[0]): "Customers with transactions in $effectiveYear",
                         (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", activeCustomerCount)),
                         (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(activeCustomerCount, activeCustomerCount))])

/** This is temporarily commented as customer requested not to remove this code. Reference: C4HAE-276: Modify the ZPAP report based on feedback.
 def countCustomersWithZPAPEntry = overviewData['ZPAPCustomerCount']
 resultMatrix.addRow([(columns[0]): "|- Customers with ZPAP entry for $effectiveYear",
 (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", countCustomersWithZPAPEntry)),
 (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(activeCustomerCount, countCustomersWithZPAPEntry))])

 def countCustTransWithZPAP = overviewData['CustomerTransacted']
 resultMatrix.addRow([(columns[0]): "|- Customers with transactions and ZPAP entry for $effectiveYear",
 (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", countCustTransWithZPAP)),
 (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(activeCustomerCount, countCustTransWithZPAP))])

 def totalArticleCount = overviewData['TransactedMaterialCount']
 resultMatrix.addRow([(columns[0]): "Materials transacted in $effectiveYear",
 (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", totalArticleCount)),
 (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(totalArticleCount, totalArticleCount))])

 def articlesWithZPAP = overviewData['ZPAPArticleCount']
 resultMatrix.addRow([(columns[0]): "|- Materials with ZPAP entry for $effectiveYear year",
 (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", articlesWithZPAP)),
 (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(totalArticleCount, articlesWithZPAP))])

 def zpapMaterialTransacted = overviewData['ZPAPTransactedMaterialCount']
 resultMatrix.addRow([(columns[0]): "|- Materials transacted and have ZPAP entry for $effectiveYear year",
 (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", zpapMaterialTransacted)),
 (columns[2]): dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(totalArticleCount, zpapMaterialTransacted))])
 // ENd of the valid code but not wanting to show it as of now..
 */
    resultMatrix.setPreferenceName("ZPAP Summary View")
    return resultMatrix

}

def getZPAPAnalysisDistributionData(String yearOfAnalysis) {
    def fields = ['MatchType'                : 'MatchType',
                  'COUNT(CustomerSalesPrice)': 'ZPAPCount',
                  'MIN(DiscountPercentage)'  : 'MinDiscountPer',
                  'MAX(DiscountPercentage)'  : 'MaxDiscountPer',
                  'Avg(DiscountPercentage)'  : 'AvgDiscountPer'
    ]
    def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFiltersForAllMatches()
    def validityStartDate = yearOfAnalysis + "-01-01"
    def validityEndDate = yearOfAnalysis + "-12-31"
    def generalLib = libs.__LIBRARY__.GeneralUtility
    filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
    return libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart').collectEntries {
        [(it.MatchType): (it)]
    }
}

def getTypeTitles(String matchType) {
    if (!matchType)
        return "Unknown"

    def maxDiscount = libs.__LIBRARY__.GeneralSettings.percentageValue("MaxDiscountPct") * 100
    switch (matchType) {
        case 'HPL': return "Within Max Permitted Discount($maxDiscount %)"
        case 'HLM': return "> Max Permitted Discount($maxDiscount %)"
        case 'ASP': return "More Than Sales Price"
        case 'NZE': return "Negative ZPAP Price"
    }
    return "Unknown"
}

def getZPAPAnalysisDistributionMatrix(Map zpapDistributionData, String effectiveYear) {
    def columns = ['Type', 'ZPAP Count', '% of Total ZPAP(Year-' + effectiveYear + ')', 'Min Discount %', 'Max Discount %', 'Avg Discount %']
    def columnTypes = [FieldFormatType.TEXT, FieldFormatType.INTEGER, FieldFormatType.PERCENT,
                       FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT]
    def dashUtl = libs.__LIBRARY__.DashboardUtility
    def resultMatrix = dashUtl.newResultMatrix('ZPAP Distribution (Year-' + effectiveYear + ')', columns, columnTypes)

    def zpapTotal = 0, minDiscount, maxDiscount, averDiscount = 0, count = 0
    zpapDistributionData.each { key, zpapEntry ->
        zpapTotal += zpapEntry.ZPAPCount
        minDiscount = (minDiscount && minDiscount < zpapEntry.MinDiscountPer) ? minDiscount : zpapEntry.MinDiscountPer
        maxDiscount = (maxDiscount && maxDiscount > zpapEntry.MaxDiscountPer) ? maxDiscount : zpapEntry.MaxDiscountPer
        averDiscount += zpapEntry.AvgDiscountPer
        count++
    }
    def mathLib = libs.__LIBRARY__.MathUtility
    averDiscount = mathLib.divide(averDiscount as BigDecimal, (count - 1) as BigDecimal) //Excluding Unknown Type
    resultMatrix.addRow([
            "Total ZPAPs (year-$effectiveYear)",
            dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", zpapTotal)),
            dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(zpapTotal, zpapTotal)),
            minDiscount,
            maxDiscount,
            dashUtl.boldHighlightWith(resultMatrix, averDiscount)
    ])

    def orderOfDisplayMatchType = ['HPL', 'HLM', 'ASP', 'NZE', 'Unknown']
    def zpapEntry
    orderOfDisplayMatchType.each { matchType ->
        zpapEntry = zpapDistributionData[matchType]
        resultMatrix.addRow([
                getTypeTitles(matchType),
                dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", (zpapEntry) ? zpapEntry?.ZPAPCount : 0)),
                dashUtl.boldHighlightWith(resultMatrix, mathLib.percentage(zpapTotal, (zpapEntry) ? zpapEntry?.ZPAPCount : 0)),
                zpapEntry?.MinDiscountPer,
                zpapEntry?.MaxDiscountPer,
                dashUtl.boldHighlightWith(resultMatrix, zpapEntry?.AvgDiscountPer)
        ])
    }
    resultMatrix.setPreferenceName("ZPAP Distribution View")
    return resultMatrix
}

def getZPAPAnalysisSummaryPerCG(String yearOfAnalysis, String prodII, String prodIII, String prodIV) {
    def fields = [
            'Verkaufsbereich3'         : 'Verkaufsbereich',
            'BrancheCategory'          : 'BrancheCategory',
            'COUNTDISTINCT(customerId)': 'CustomerCount',
            'COUNTDISTINCT(Material)'  : 'ArticleCount',
            'COUNT(CustomerSalesPrice)': 'ZPAPCount',
            'MIN(DiscountPercentage)'  : 'MinDiscountPer',
            'MAX(DiscountPercentage)'  : 'MaxDiscountPer',
            'Avg(DiscountPercentage)'  : 'AvgDiscountPer'
    ]
    def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFilters()
    def validityStartDate = yearOfAnalysis + "-01-01"
    def validityEndDate = yearOfAnalysis + "-12-31"

    def generalLib = libs.__LIBRARY__.GeneralUtility
    filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
    libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', validityStartDate)
    if (prodII && !(prodII.equals("All")))
        filters.add(Filter.equal('ProdhII', prodII))
    if (prodIII && !(prodIII.equals("All")))
        filters.add(Filter.equal('ProdhIII', prodIII))
    if (prodIV && !(prodIV.equals("All")))
        filters.add(Filter.equal('ProdhIV', prodIV))
    return libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart')
}

def getZPAPAnalysisDashboardPerCG(def zpapEntriesPerPG, def summaryData, String effectiveYear) {
    def columns = ['Verkaufsbuero', 'Branche Category', 'Number of Customers',
                   'Number of Materials with ZPAP(year-' + effectiveYear + ')',
                   'Number of ZPAP(Year-' + effectiveYear + ')', 'Min Discount %', 'Max Discount %', 'Avg Discount %']
    def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT,
                       FieldFormatType.INTEGER, FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                       FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT]
    def dashUtl = lib.DashboardUtility
    def resultMatrix = dashUtl.newResultMatrix('CG: ZPAP Greater Than Maximum Permitted Discount (Year-' + effectiveYear + ')', columns, columnTypes)

    def zpapEntriesToDisplay = zpapEntriesPerPG
    def minDiscountPer, maxDiscountPer, avgDiscountPer = 0, count = 0
    zpapEntriesToDisplay.each {
        minDiscountPer = (minDiscountPer && minDiscountPer < it['MinDiscountPer']) ? minDiscountPer : it['MinDiscountPer']
        maxDiscountPer = (maxDiscountPer && maxDiscountPer > it['MaxDiscountPer']) ? maxDiscountPer : it['MaxDiscountPer']
        avgDiscountPer += it['AvgDiscountPer']
        count++
    }
    avgDiscountPer = libs.__LIBRARY__.MathUtility.divide(avgDiscountPer, count)
    resultMatrix.addRow([
            'All', 'All',
            dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", summaryData['CustomerCountGTPerMaxDiscPer'])),
            dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", summaryData['MaterialCountGTPerMaxDiscPer'])),
            dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", summaryData['ZPAPCountGTPerMaxDiscPer'])),
            minDiscountPer,
            maxDiscountPer,
            dashUtl.boldHighlightWith(resultMatrix, avgDiscountPer)
    ])
    zpapEntriesToDisplay?.each {
        resultMatrix.addRow([
                (columns[0]): it['Verkaufsbereich'],
                (columns[1]): it['BrancheCategory'],
                (columns[2]): dashUtl.boldHighlightWith(resultMatrix, it['CustomerCount']),
                (columns[3]): dashUtl.boldHighlightWith(resultMatrix, it['ArticleCount']),
                (columns[4]): dashUtl.boldHighlightWith(resultMatrix, it['ZPAPCount']),
                (columns[5]): it['MinDiscountPer'],
                (columns[6]): it['MaxDiscountPer'],
                (columns[7]): dashUtl.boldHighlightWith(resultMatrix, it['AvgDiscountPer'])
        ])
    }

    resultMatrix.setPreferenceName("ZPAP CG View")
    resultMatrix
            .onRowSelection()
            .triggerEvent(api.dashboardWideEvent("RefreshZPAPPGDetails"))
            .withColValueAsEventDataAttr(columns[0], "ZA_Verkaufsbuero")
            .withColValueAsEventDataAttr(columns[1], "ZA_BrancheCategory")

    return resultMatrix
}
/**
 * This method gets the details of the product analysis
 * @param yearOfAnalysis
 * @param salesOffice
 * @param brancheCategory
 * @return
 */
def getZPAPAnalysisSummaryPerPG(String yearOfAnalysis, String salesOffice, String brancheCategory) {
    def fields = ['ProdhII'                  : 'ProdhII',
                  'ProdhIII'                 : 'ProdhIII',
                  'ProdhIV'                  : 'ProdhIV',
                  'COUNTDISTINCT(customerId)': 'CustomerCount',
                  'COUNTDISTINCT(Material)'  : 'ArticleCount',
                  'COUNT(CustomerSalesPrice)': 'ZPAPCount',
                  'MIN(DiscountPercentage)'  : 'MinDiscountPer',
                  'MAX(DiscountPercentage)'  : 'MaxDiscountPer',
                  'Avg(DiscountPercentage)'  : 'AvgDiscountPer'
    ]
    def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFilters()
    def validityStartDate = yearOfAnalysis + "-01-01"
    def validityEndDate = yearOfAnalysis + "-12-31"

    def generalLib = libs.__LIBRARY__.GeneralUtility
    filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
    libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', yearOfAnalysis)

    if (salesOffice && !(salesOffice.equals("All")))
        filters.add(Filter.equal('Verkaufsbereich3', salesOffice))
    if (brancheCategory && !(brancheCategory.equals("All")))
        filters.add(Filter.equal('BrancheCategory', brancheCategory))
    if (salesOffice && !brancheCategory)
        filters.add(Filter.isNull('BrancheCategory'))

    return libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart')
}

def getAllCountsForZPAPAnalysisPerPG(String yearOfAnalysis, String salesOffice, String brancheCategory) {
    def fields = ['COUNTDISTINCT(customerId)': 'CustomerCount',
                  'COUNTDISTINCT(Material)'  : 'ArticleCount',
                  'COUNT(CustomerSalesPrice)': 'ZPAPCount',
                  'MIN(DiscountPercentage)'  : 'MinDiscountPer',
                  'MAX(DiscountPercentage)'  : 'MaxDiscountPer',
                  'Avg(DiscountPercentage)'  : 'AvgDiscountPer'
    ]
    def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFilters()

    def validityStartDate = yearOfAnalysis + "-01-01"
    def validityEndDate = yearOfAnalysis + "-12-31"
    def generalLib = libs.__LIBRARY__.GeneralUtility
    filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))

    libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', yearOfAnalysis)
    if (salesOffice && !(salesOffice.equals("All")))
        filters.add(Filter.equal('Verkaufsbereich3', salesOffice))
    if (brancheCategory && !(brancheCategory.equals("All")))
        filters.add(Filter.equal('BrancheCategory', brancheCategory))
    if (salesOffice && !brancheCategory)
        filters.add(Filter.isNull('BrancheCategory'))

    return libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart')
}

/**
 * This method is dedicated oonly for embedded dashboard..
 * @param zpapEntriesPerPG
 * @param summaryData
 * @param effectiveYear
 * @param customerGroup
 * @return
 */
def getZPAPAnalysisSummaryForEmbeddedDashBoardPerPG(def zpapEntriesPerPG, String effectiveYear, List customerGroup) {
    def columns = ['Prodh. II', 'Prodh. III', 'Prodh. IV', 'Number of Customers',
                   'Number of Material with ZPAP(Year-' + effectiveYear + ')', 'Number of ZPAP(Year-' + effectiveYear + ')',
                   'Min Discount %', 'Max Discount %', 'Avg Discount %',
                   'Verkaufsbuero', 'Branche Category']
    def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT,
                       FieldFormatType.INTEGER, FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                       FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT,
                       FieldFormatType.TEXT, FieldFormatType.TEXT]
    def dashUtl = lib.DashboardUtility
    def resultMatrix = dashUtl.newResultMatrix('Drill Down for Selected CG Row (Year-' + effectiveYear + ')', columns, columnTypes)
    def zpapEntriesToDisplay = zpapEntriesPerPG
    def summaryData = getAllCountsForZPAPAnalysisPerPG(effectiveYear, customerGroup[0], customerGroup[1])
    summaryData = (summaryData) ? summaryData[0] : [:]
    resultMatrix.addRow(['All', 'All', 'All',
                         dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", (summaryData) ? summaryData['CustomerCount'] : 0)),
                         dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", (summaryData) ? summaryData['ArticleCount'] : 0)),
                         dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", (summaryData) ? summaryData['ZPAPCount'] : 0)),
                         (summaryData) ? summaryData['MinDiscountPer'] : 0,
                         (summaryData) ? summaryData['MaxDiscountPer'] : 0,
                         dashUtl.boldHighlightWith(resultMatrix, (summaryData) ? summaryData['AvgDiscountPer'] : 0),
                         customerGroup[0], customerGroup[1]
    ])
    zpapEntriesToDisplay?.each {
        resultMatrix.addRow([
                (columns[0]) : it['ProdhII'],
                (columns[1]) : it['ProdhIII'],
                (columns[2]) : it['ProdhIV'],
                (columns[3]) : dashUtl.boldHighlightWith(resultMatrix, (it['CustomerCount'])),
                (columns[4]) : dashUtl.boldHighlightWith(resultMatrix, it['ArticleCount']),
                (columns[5]) : dashUtl.boldHighlightWith(resultMatrix, it['ZPAPCount']),
                (columns[6]) : it['MinDiscountPer'],
                (columns[7]) : it['MaxDiscountPer'],
                (columns[8]) : dashUtl.boldHighlightWith(resultMatrix, it['AvgDiscountPer']),
                (columns[9]) : customerGroup[0],
                (columns[10]): customerGroup[1]
        ])
    }

    resultMatrix.setPreferenceName("ZPAP Embedded PG View")
    resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("RefreshZPAPDetails"))
            .withColValueAsEventDataAttr(columns[9], "Verkaufsbuero")
            .withColValueAsEventDataAttr(columns[10], "BrancheCategory")
            .withColValueAsEventDataAttr(columns[0], "ProdhII")
            .withColValueAsEventDataAttr(columns[1], "ProdhIII")
            .withColValueAsEventDataAttr(columns[2], "ProdhIV")
    return resultMatrix
}

def getAllCountsForZPAPAnalysisPerCG(String yearOfAnalysis, String prodhII, String prodhIII, String prodhIV) {
    def fields = ['COUNTDISTINCT(customerId)': 'CustomerCount',
                  'COUNTDISTINCT(Material)'  : 'ArticleCount',
                  'COUNT(CustomerSalesPrice)': 'ZPAPCount',
                  'MIN(DiscountPercentage)'  : 'MinDiscountPer',
                  'MAX(DiscountPercentage)'  : 'MaxDiscountPer',
                  'Avg(DiscountPercentage)'  : 'AvgDiscountPer'
    ]
    def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFilters()

    def validityStartDate = yearOfAnalysis + "-01-01"
    def validityEndDate = yearOfAnalysis + "-12-31"
    def generalLib = libs.__LIBRARY__.GeneralUtility
    filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))

    libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', yearOfAnalysis)
    if (prodhII && !(prodhII.equals("All")))
        filters.add(Filter.equal('ProdhII', prodhII))
    if (prodhIII && !(prodhIII.equals("All")))
        filters.add(Filter.equal('ProdhIII', prodhIII))
    if (prodhIV && !(prodhIV.equals("All")))
        filters.add(Filter.equal('ProdhIV', prodhIV))

    return libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart')
}


def getZPAPAnalysisSummaryForEmbeddedDashboardPerCG(def zpapEntriesPerPG, String effectiveYear, List productGroup) {
    def columns = ['Verkaufsbuero', 'Branche Category', 'Number of Customers',
                   'Number of Materials with ZPAP(year-' + effectiveYear + ')',
                   'Number of ZPAP(Year-' + effectiveYear + ')', 'Min Discount %', 'Max Discount %', 'Avg Discount %',
                   'Prodh. II', 'Prodh. III', 'Prodh. IV']
    def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT,
                       FieldFormatType.INTEGER, FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                       FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT,
                       FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT]
    def dashUtl = lib.DashboardUtility
    def resultMatrix = dashUtl.newResultMatrix('Drill Down for Selected PG Row (Year-' + effectiveYear + ')', columns, columnTypes)

    def zpapEntriesToDisplay = zpapEntriesPerPG
    def summaryData = getAllCountsForZPAPAnalysisPerCG(effectiveYear, productGroup[0], productGroup[1], productGroup[2])
    summaryData = (summaryData) ? summaryData[0] : [:]
    resultMatrix.addRow([
            'All', 'All',
            dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", (summaryData) ? summaryData['CustomerCount'] : 0)),
            dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", (summaryData) ? summaryData['ArticleCount'] : 0)),
            dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", (summaryData) ? summaryData['ZPAPCount'] : 0)),
            (summaryData) ? summaryData['MinDiscountPer'] : 0,
            (summaryData) ? summaryData['MaxDiscountPer'] : 0,
            dashUtl.boldHighlightWith(resultMatrix, (summaryData) ? summaryData['AvgDiscountPer'] : 0),
            productGroup[0], productGroup[1], productGroup[2]
    ])
    zpapEntriesToDisplay?.each {
        resultMatrix.addRow([
                (columns[0]) : it['Verkaufsbereich'],
                (columns[1]) : it['BrancheCategory'],
                (columns[2]) : dashUtl.boldHighlightWith(resultMatrix, it['CustomerCount']),
                (columns[3]) : dashUtl.boldHighlightWith(resultMatrix, it['ArticleCount']),
                (columns[4]) : dashUtl.boldHighlightWith(resultMatrix, it['ZPAPCount']),
                (columns[5]) : it['MinDiscountPer'],
                (columns[6]) : it['MaxDiscountPer'],
                (columns[7]) : dashUtl.boldHighlightWith(resultMatrix, it['AvgDiscountPer']),
                (columns[8]) : productGroup[0],
                (columns[9]) : productGroup[1],
                (columns[10]): productGroup[2]
        ])
    }

    resultMatrix.setPreferenceName("ZPAP Embedded CG View")
    resultMatrix.onRowSelection()
            .triggerEvent(api.dashboardWideEvent("RefreshZPAPDetails"))
            .withColValueAsEventDataAttr(columns[0], "Verkaufsbuero")
            .withColValueAsEventDataAttr(columns[1], "BrancheCategory")
            .withColValueAsEventDataAttr(columns[8], "ProdhII")
            .withColValueAsEventDataAttr(columns[9], "ProdhIII")
            .withColValueAsEventDataAttr(columns[10], "ProdhIV")

    return resultMatrix
}

def getValue(def values, def valueKey) {
    def result = values.find { it.key2 == valueKey }
    return ((result) ? result?.attribute1 : 0)
}

/**
 *
 * @param zpapEntriesPerPG
 * @param isEmbeddedDashboard
 * @param summaryData
 * @return
 */
def getZPAPSummaryDashBoardPerPG(def zpapEntriesPerPG, def summaryData, String effectiveYear) {
    def columns = ['Prodh. II', 'Prodh. III', 'Prodh. IV', 'Number of Customers',
                   'Number of Material with ZPAP(Year-' + effectiveYear + ')',
                   'Number of ZPAP(Year-' + effectiveYear + ')',
                   'Min Discount %', 'Max Discount %', 'Avg Discount %']
    def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT,
                       FieldFormatType.INTEGER, FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                       FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT]
    def dashUtl = lib.DashboardUtility
    def resultMatrix = dashUtl.newResultMatrix('PG: ZPAP Greater Than Maximum Permitted Discount (Year-' + effectiveYear + ')', columns, columnTypes) // MK
    def totalCustomerCount = 0, totalMaterialCount = 0, totalZPAPCount = 0

    def zpapEntriesToDisplay = zpapEntriesPerPG
    def minDiscountPer, maxDiscountPer, avgDiscountPer = 0, count = 0
    zpapEntriesToDisplay.each {
        minDiscountPer = (minDiscountPer && minDiscountPer < it['MinDiscountPer']) ? minDiscountPer : it['MinDiscountPer']
        maxDiscountPer = (maxDiscountPer && maxDiscountPer > it['MaxDiscountPer']) ? maxDiscountPer : it['MaxDiscountPer']
        avgDiscountPer += it['AvgDiscountPer']
        count++
    }
    avgDiscountPer = libs.__LIBRARY__.MathUtility.divide(avgDiscountPer, count)
    resultMatrix.addRow(['All', 'All', 'All',
                         dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", summaryData['CustomerCountGTPerMaxDiscPer'])),
                         dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", summaryData['MaterialCountGTPerMaxDiscPer'])),
                         dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("###,###,###", summaryData['ZPAPCountGTPerMaxDiscPer'])),
                         minDiscountPer,
                         maxDiscountPer,
                         dashUtl.boldHighlightWith(resultMatrix, avgDiscountPer)
    ])
    zpapEntriesToDisplay?.each {
        resultMatrix.addRow([
                (columns[0]): it['ProdhII'],
                (columns[1]): it['ProdhIII'],
                (columns[2]): it['ProdhIV'],
                (columns[3]): dashUtl.boldHighlightWith(resultMatrix, (it['CustomerCount'])),
                (columns[4]): dashUtl.boldHighlightWith(resultMatrix, it['ArticleCount']),
                (columns[5]): dashUtl.boldHighlightWith(resultMatrix, it['ZPAPCount']),
                (columns[6]): it['MinDiscountPer'],
                (columns[7]): it['MaxDiscountPer'],
                (columns[8]): dashUtl.boldHighlightWith(resultMatrix, it['AvgDiscountPer'])
        ])
    }

    resultMatrix.setPreferenceName("ZPAP PG View")
    resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("RefreshZPAPPGDetails"))
            .withColValueAsEventDataAttr(columns[0], "ZA_ProdhII")
            .withColValueAsEventDataAttr(columns[1], "ZA_ProdhIII")
            .withColValueAsEventDataAttr(columns[2], "ZA_ProdhIV")
    return resultMatrix
}

