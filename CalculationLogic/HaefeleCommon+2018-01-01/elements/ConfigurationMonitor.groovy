import groovy.transform.Field
import net.pricefx.server.dto.calculation.ResultMatrix

Map handleDataMart(def table, def filters) {
    Object dmCtx = api.getDatamartContext()
    Object DM = dmCtx.getDatamart(table)
    Object dmQueryThis = dmCtx.newQuery(DM, true)
    Object queryThis = dmCtx.newQuery(DM, true)
    queryThis.select("MAX(lastUpdateDate)", 'LastUpdateDate')
    dmQueryThis.select("Count(lastUpdateDate)", 'Count')
    dmQueryThis.where([*filters])
    Object count = dmCtx.executeQuery(dmQueryThis).getData()
    Object lastUpdateDate = dmCtx.executeQuery(queryThis).getData()

    return [LastUpdateDate: lastUpdateDate.value?.toString()?.substring(0, 10), Count: count.value]
}

Map handleDataSource(def table, def filters) {
    Object dmCtx = api.getDatamartContext()
    Object DS = dmCtx.getDataSource(table)
    Object dmQueryThis = dmCtx.newQuery(DS, true)
    Object queryThis = dmCtx.newQuery(DS, true)
    queryThis.select("MAX(lastUpdateDate)", 'LastUpdateDate')
    dmQueryThis.select("Count(lastUpdateDate)", 'Count')
    dmQueryThis.where([*filters])
    Object count = dmCtx.executeQuery(dmQueryThis).getData()
    Object lastUpdateDate = dmCtx.executeQuery(queryThis).getData()

    return [LastUpdateDate: lastUpdateDate.value?.toString()?.substring(0, 10), Count: count.value]
}

Map handleProductExtension(def table, def filters) {
    List lastUpdateDate = api.find("PX", 0, 1, '-lastUpdateDate', ['lastUpdateDate'], true,
                                   Filter.equal("name", table))
    int count = api.count('PX', Filter.equal("name", table), *filters)

    return [LastUpdateDate: lastUpdateDate[0]?.lastUpdateDate?.toString()?.substring(0, 10), Count: count]
}

Map handleCustomerExtension(def table, def filters) {
    List lastUpdateDate = api.find("CX", 0, 1, '-lastUpdateDate', ['lastUpdateDate'], true,
                                   Filter.equal("name", table))
    int count = api.count('CX', Filter.equal("name", table), *filters)

    return [LastUpdateDate: lastUpdateDate[0]?.lastUpdateDate?.toString()?.substring(0, 10), Count: count]
}

Map handlePriceParameter(def table, def filters) {
    def id = api.findLookupTable(table)?.id
    api.trace("id", id)
    def lookupTable = api.findLookupTable(table)
    def typeCode = "MATRIX" == lookupTable.type ? "MLTV" + lookupTable.valueType.replace("MATRIX", "") : "LTV"
    List lastUpdateDate = api.find(typeCode, 0, 1, '-lastUpdateDate', ['lastUpdateDate'], true,
                                   Filter.equal("lookupTable.id", id))
    int count = api.count(typeCode, Filter.equal("lookupTable.id", id), *filters)

    return [LastUpdateDate: lastUpdateDate[0]?.lastUpdateDate?.toString()?.substring(0, 10), Count: count]
}

String color( BigDecimal count, BigDecimal minimumCount) {
    def server = api.vLookup("SurchargeConfig","Server")
    return count == 0 ?
           "https://$server/classic/images/library/Traffic_red.png" :
           (count < minimumCount ? "https://$server/classic/images/library/Traffic_yellow.png"
                                 : "https://$server/classic/images/library/Traffic_green.png")
}


String trafficColor(BigDecimal count, BigDecimal minimumCount, def resultMatrix){
    return count == 0 ?
           resultMatrix.libraryImage("Traffic", "red") :
           (count < minimumCount ? resultMatrix.libraryImage("Traffic", "yellow")
                                 : resultMatrix.libraryImage("Traffic", "green"))
}
List findConfigurationToMonitor() {
    return api.findLookupTableValues("MonitoredConfigurations")
}

@Field final String DATAMART = "DataMart"
@Field final String DATASOURCE = "DataSource"
@Field final String PRODUCTEXTENSION = "Product Extension"
@Field final String CUSTOMEREXTENSION = "Customer Extension"
@Field final String PRICEPARAMETER = "Price Parameter"
@Field final String LASTUPDATEDATE = "lastUpdateDate"

Date getTheStartDateForMonthFilter(Date referenceDate) {
    Calendar startDate = api.calendar(referenceDate)
    startDate.set(Calendar.DAY_OF_MONTH, 1)
    return startDate.getTime()
}

Date getTheNextDayDate(Date referenceDate) {
    Calendar endingDate = api.calendar(referenceDate)
    endingDate.add(Calendar.DAY_OF_MONTH, 1)
    return endingDate.getTime()
}

Filter getFilterFromFirstDayToReferenceDay(Date endDate) {
    Date startDate = getTheStartDateForMonthFilter(endDate)
    Date endingDate = getTheNextDayDate(endDate)
    return Filter.and(
            Filter.greaterOrEqual(LASTUPDATEDATE, startDate.format("yyyy-MM-dd")),
            Filter.lessThan(LASTUPDATEDATE, endingDate.format("yyyy-MM-dd")))
}

Date getTheStartDateForWeekFilter(Date referenceDate) {
    Calendar startDate = api.calendar(referenceDate)
    startDate.add(Calendar.DAY_OF_MONTH, -7)
    return startDate.getTime()
}


Filter getFilterFromPreviousWeekToReferenceDay(Date endDate) {
    Date startDate = getTheStartDateForWeekFilter(endDate)
    Date endingDate = getTheNextDayDate(endDate)
    return Filter.and(
            Filter.greaterOrEqual(LASTUPDATEDATE, startDate.format("yyyy-MM-dd")),
            Filter.lessThan(LASTUPDATEDATE, endingDate.format("yyyy-MM-dd")))

}

Map getCountsOfRecordsReceivedForConfiguration(List tableNames,
                                               Date referenceDate,
                                               Map tableType,
                                               Map updateFrequency) {
    List weeklyFilter = [getFilterFromPreviousWeekToReferenceDay(referenceDate)]
    List monthlyFilter = [getFilterFromFirstDayToReferenceDay(referenceDate)]
    List filters
    Map result = [:]
    for (table in tableNames) {
        if (["Weekly - Automated", "Weekly - Manual"].contains(updateFrequency.get(table))) {
            filters = weeklyFilter
        } else {
            filters = monthlyFilter
        }
        def type = tableType[table]
        switch (type) {
            case DATAMART:
                result << [(table): handleDataMart(table, filters)]
                break
            case DATASOURCE:
                result << [(table): handleDataSource(table, filters)]
                break
            case PRODUCTEXTENSION:
                result << [(table): handleProductExtension(table, filters)]
                break
            case CUSTOMEREXTENSION:
                result << [(table): handleCustomerExtension(table, filters)]
                break
            case PRICEPARAMETER:
                result << [(table): handlePriceParameter(table, filters)]
                break
            default:
                throw new RuntimeException('Invalid type specified ' + type)
        }
    }

    return result
}


