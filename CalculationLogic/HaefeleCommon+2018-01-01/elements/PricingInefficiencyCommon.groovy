import groovy.transform.Field

@Field final Map displayOrder = [
        'PPSPIncreaseButPPMoreThanSP'          : 'Both purchase price & sales price increased, however increase in sales price was lesser than respective purchase price increase',
        'PPIncreaseSPNoChange'                 : 'Purchase price increased but no change in sales price',
        'PPIncreaseSPDecrease'                 : 'Purchase price increased and sales price decreased',
        'PPSPDecreasedButSPDecreasedMoreThanPP': 'Both purchase price & sales price decreased, but sales price decrease more than respective purchase price decrease',
        'SPDecreasedButNoChangeInPP'           : 'No change in purchase price but sales price was decreased',
        'PPIncreaseNoInfoOnSP'                 : 'Purchase price increased but no information on sales price'
]
