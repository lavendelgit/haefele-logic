final String deficiencyCol = "Deficiency?"
final String spCol = "Sales Price"
final String validFromCol = "Valid From"
final String validToCol = "Valid To"
final String fgGroupCol = "Focus Group?"
final String minSurchargeCol = "Minimum Surcharge %"
final String actualSurchargePerCol = "Actual Surcharge %"
final String absolDifferenceCol = "Absolute Difference"
final String gapDifferencePercentageCol = "Difference %"

def matrix = api.newMatrix(spCol,
                           actualSurchargePerCol,
                           validFromCol,
                           validToCol,
                           fgGroupCol,
                           minSurchargeCol,
                           deficiencyCol,
                           absolDifferenceCol, 
                           gapDifferencePercentageCol             
                          )
matrix.setEnableClientFilter(true)
matrix.setColumnFormat(deficiencyCol, FieldFormatType.TEXT)
matrix.setColumnFormat(fgGroupCol, FieldFormatType.TEXT)
matrix.setColumnFormat(validFromCol, FieldFormatType.DATE)
matrix.setColumnFormat(validToCol, FieldFormatType.DATE)
matrix.setColumnFormat(minSurchargeCol, FieldFormatType.PERCENT)
matrix.setColumnFormat(absolDifferenceCol, FieldFormatType.MONEY)
matrix.setColumnFormat(spCol, FieldFormatType.MONEY)
matrix.setColumnFormat(actualSurchargePerCol, FieldFormatType.PERCENT)
matrix.setColumnFormat(gapDifferencePercentageCol, FieldFormatType.PERCENT)

def basePrice = api.getElement("BasePrice")
def sku = api.getElement("Sku")
matrix.setTitle("Article: $sku | Base Price: $basePrice | Sales Price (ZPLP)")
def salesPriceResult = api.getElement("SalesPrice")
def focusGroup = api.getElement("FocusGroup")
api.local.zplp = 0
api.local.zplpfg = 0
def surchargeMap = api.getElement("Surcharge")
def surcharge = surchargeMap?.get("ZPLP")?.attribute1
def fcSurcharge = surchargeMap?.get("ZPLP")?.attribute2
api.trace("surcharge",surcharge)
api.local.zplpData = 0
if(basePrice){
salesPriceResult?.each{
  def calculationRow = [:]
  calculationRow.put(validFromCol, it.attribute1)
  calculationRow.put(validToCol, it.attribute2)
  api.local.zplpData = api.local.zplpData+1
  def salesPrice = it.attribute3
  def factor = 1+surcharge
  if(focusGroup){
    factor = 1+fcSurcharge
    calculationRow.put(minSurchargeCol, fcSurcharge)
    calculationRow.put(fgGroupCol,"Yes")
  }
  else{
    calculationRow.put(minSurchargeCol, surcharge)
    calculationRow.put(fgGroupCol, "No")
  }
  
  def diff = salesPrice-(basePrice*factor)
  def actualDiff = salesPrice-basePrice
  calculationRow.put(spCol, salesPrice)
  calculationRow.put(absolDifferenceCol, diff)
  calculationRow.put(actualSurchargePerCol, actualDiff/basePrice)
  calculationRow.put(gapDifferencePercentageCol, diff/basePrice)
  if(basePrice && diff >= 0){
    calculationRow.put(deficiencyCol,"No")
  }
  else if(basePrice){
    calculationRow.put(deficiencyCol,"Yes")
    if(focusGroup){
      api.local.zplpfg = api.local.zplpfg+1
    }
    else{
      api.local.zplp = api.local.zplp+1
    }
  }
  matrix.addRow(calculationRow)
}
if(salesPriceResult){
  return matrix
}
}