final String custIdCol = "Customer ID"
final String cspCol = "Customer Sales Price"
final String validFromCol = "Valid From"
final String validToCol = "Valid To"
final String fgGroupCol = "Focus Group?"
final String minSurCol = "Minimum Surcharge %"
final String deficiencyCol = "Deficiency?"
final String actualSurPerCol = "Actual Surcharge %"
final String absolDiffCol = "Absolute Difference"
final String gapDiffPerCol = "Difference %"

def matrix = api.newMatrix(custIdCol,
                           cspCol,
                           actualSurPerCol,
                           validFromCol,
                           validToCol,
                           fgGroupCol,
                           minSurCol,
                           deficiencyCol,
                           absolDiffCol, 
                           gapDiffPerCol             
                          )
matrix.setEnableClientFilter(true)
matrix.setColumnFormat(custIdCol, FieldFormatType.TEXT)
matrix.setColumnFormat(fgGroupCol, FieldFormatType.TEXT)
matrix.setColumnFormat(validFromCol, FieldFormatType.DATE)
matrix.setColumnFormat(validToCol, FieldFormatType.DATE)
matrix.setColumnFormat(deficiencyCol, FieldFormatType.TEXT)
matrix.setColumnFormat(actualSurPerCol, FieldFormatType.PERCENT)
matrix.setColumnFormat(minSurCol, FieldFormatType.PERCENT)
matrix.setColumnFormat(absolDiffCol, FieldFormatType.MONEY)
matrix.setColumnFormat(cspCol, FieldFormatType.MONEY)
matrix.setColumnFormat(gapDiffPerCol, FieldFormatType.PERCENT)
def basePrice = api.getElement("BasePrice")
def sku = api.getElement("Sku")
matrix.setTitle("Article: $sku | Base Price: $basePrice | Customer Sales Price (ZPAP)")
def customerSalesPriceResult = api.getElement("CustomerSalesPrice")
api.local.zpap = 0
api.local.zpapfg = 0
def surchargeMap = api.getElement("Surcharge")
def surcharge = surchargeMap?.get("ZPAP")?.attribute1
def fcSurcharge = surchargeMap?.get("ZPAP")?.attribute2
api.trace("surcharge",surcharge)
api.local.zpapData = 0
if(basePrice){
customerSalesPriceResult?.each{
  def calculationRow = [:]
  calculationRow.put(custIdCol, it.attribute4)
  calculationRow.put(validFromCol, it.attribute1)
  calculationRow.put(validToCol, it.attribute2)
  api.local.zpapData = api.local.zpapData+1
  def customerSalesPrice = it.attribute3
  def focusGroup = it.attribute38
  def factor = 1+surcharge
  if(focusGroup){
    factor = 1+fcSurcharge
    calculationRow.put(minSurCol, fcSurcharge)
    calculationRow.put(fgGroupCol,"Yes")
  }
  else{
    calculationRow.put(minSurCol, surcharge)
    calculationRow.put(fgGroupCol,"No")
  }
  api.trace("baseprice",basePrice)
  api.trace("factor",factor)
  def diff = customerSalesPrice-(basePrice*factor)  
  def actualDiff = customerSalesPrice-basePrice
  calculationRow.put(actualSurPerCol, actualDiff/basePrice) 
  calculationRow.put(cspCol, customerSalesPrice)
  calculationRow.put(absolDiffCol, diff)
  calculationRow.put(gapDiffPerCol, diff/basePrice)
  if(basePrice && diff >= 0){
    calculationRow.put(deficiencyCol,"No")
  }
  else if(basePrice){
    calculationRow.put(deficiencyCol,"Yes")
    if(focusGroup){
      api.local.zpapfg = api.local.zpapfg+1
    }
    else{
      api.local.zpap = api.local.zpap+1
    }
  }
  matrix.addRow(calculationRow)
}
if(customerSalesPriceResult){
  return matrix
}
}