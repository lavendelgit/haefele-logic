def deficiency = "NA"
if((api.local.zplpData + api.local.zpapData + api.local.zpzData)>0){
  deficiency = ""
  if(api.local.zplp>0){
    deficiency = addDeficiencyType(deficiency, "ZPLP")
  }
  if(api.local.zplpfg>0){
    deficiency = addDeficiencyType(deficiency, "ZPLP FG")
  }
  if(api.local.zpap>0){
    deficiency = addDeficiencyType(deficiency, "ZPAP")
  }
  if(api.local.zpapfg>0){
    deficiency = addDeficiencyType(deficiency, "ZPAP FG")
  }
  if(api.local.zpz>0){
    deficiency = addDeficiencyType(deficiency, "ZPZ")
  }
  if(deficiency==""){
    deficiency = "None"
  }
}
return deficiency

def addDeficiencyType(deficiency, type){
  if(deficiency==""){
    deficiency = deficiency+type
  }
  else{
    deficiency = deficiency+","+type
  }
  return deficiency
}