final String netPriceCol = "Net Price"
final String validFromCol = "Valid From"
final String validToCol = "Valid To"
final String minSurCol = "Minimum Surcharge %"
final String deficiencyCol = "Deficiency?"
final String actualSurPerCol = "Actual Surcharge %"
final String absolDiffCol = "Absolute Difference"
final String gapDiffPerCol = "Difference %"

def matrix = api.newMatrix(netPriceCol,
                           actualSurPerCol,
                           validFromCol,
                           validToCol,
                           minSurCol,
                           deficiencyCol,
                           absolDiffCol, 
                           gapDiffPerCol             
                          )
matrix.setEnableClientFilter(true)
matrix.setTitle("Net Price (ZPZ)")
matrix.setColumnFormat(deficiencyCol, FieldFormatType.TEXT)
matrix.setColumnFormat(validFromCol, FieldFormatType.DATE)
matrix.setColumnFormat(validToCol, FieldFormatType.DATE)
matrix.setColumnFormat(minSurCol, FieldFormatType.PERCENT)
matrix.setColumnFormat(actualSurPerCol, FieldFormatType.PERCENT)
matrix.setColumnFormat(absolDiffCol, FieldFormatType.MONEY)
matrix.setColumnFormat(netPriceCol, FieldFormatType.MONEY)
matrix.setColumnFormat(gapDiffPerCol, FieldFormatType.PERCENT)
def basePrice = api.getElement("BasePrice")
def sku = api.getElement("Sku")
matrix.setTitle("Article: $sku | Base Price: $basePrice | Net Price (ZPZ)")
def netPriceResult = api.getElement("NetPrice")
api.local.zpz = 0
def surchargeMap = api.getElement("Surcharge")
def surcharge = surchargeMap?.get("ZPZ")?.attribute1
api.trace("surcharge",surcharge)
api.local.zpzData = 0
if(basePrice){
netPriceResult?.each{
  def calculationRow = [:]
  calculationRow.put(validFromCol, it.attribute1)
  calculationRow.put(validToCol, it.attribute2)
  api.local.zpzData = api.local.zpzData+1
  def netPrice = it.attribute3
  def factor = 1+surcharge
  def diff = netPrice-(basePrice*factor)
  def actualDiff = netPrice-basePrice
  calculationRow.put(netPriceCol, netPrice)
  calculationRow.put(actualSurPerCol, actualDiff/basePrice) 
  calculationRow.put(absolDiffCol, diff)
  calculationRow.put(gapDiffPerCol, diff/basePrice)
  calculationRow.put(minSurCol, surcharge)
  if(basePrice && diff >= 0){
    calculationRow.put(deficiencyCol, "No")
  }
  else if(basePrice){
    calculationRow.put(deficiencyCol, "Yes")
    api.local.zpz=api.local.zpz+1
  }
  matrix.addRow(calculationRow)
}
if(netPriceResult){
  return matrix
}
}