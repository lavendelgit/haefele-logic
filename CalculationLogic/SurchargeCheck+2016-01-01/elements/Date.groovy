def date = Date.newInstance()
def filters = [Filter.equal("name","Date")]
def dateList = api.findLookupTableValues("SurchargeCheckDate",*filters)
if(dateList){
  def customDate = dateList[0].attribute1
  if(customDate){
    date = customDate
  }
}
return date