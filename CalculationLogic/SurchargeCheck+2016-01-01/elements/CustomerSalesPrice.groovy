def basePrice = api.getElement("BasePrice")
if(basePrice){
def sku = api.getElement("Sku")
def date = api.getElement("Date")?.format("yyyy-MM-dd")
def filters = [Filter.equal("name","S_ZPAP"),
               Filter.equal("sku",sku),
               Filter.lessOrEqual("attribute1",date),
               Filter.greaterOrEqual("attribute2",date)
              ]
def result = api.find("PX50",*filters)
api.trace("result",result)
return result
}