def basePrice = api.getElement("BasePrice")
if(basePrice){
def sku = api.getElement("Sku")
def filters = [Filter.equal("name","FocusGroup"),
              Filter.equal("sku",sku)]
def result = api.find("PX",*filters)
api.trace("result",result)
return result
}