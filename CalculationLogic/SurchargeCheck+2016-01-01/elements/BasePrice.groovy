def sku = api.getElement("Sku")
def date = api.getElement("Date")?.format("yyyy-MM-dd")
def currency = api.getElement("Currency")
def currencyList = currency?.toString()?.split(",")
def filters = [Filter.equal("name","S_ZPE"),
               Filter.equal("sku",sku),
               Filter.lessOrEqual("attribute1",date),
               Filter.greaterOrEqual("attribute2",date),
               Filter.in("attribute16",currencyList)
              ]
def result = api.find("PX50",*filters)
def basePrice = result?result[0]?.attribute3:null
api.trace("basePrice",filters)
return basePrice