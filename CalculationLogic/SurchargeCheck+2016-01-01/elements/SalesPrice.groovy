def basePrice = api.getElement("BasePrice")
if(basePrice){
def sku = api.getElement("Sku")
def date = api.getElement("Date")?.format("yyyy-MM-dd")
def currency = api.getElement("Currency")
def currencyList = currency?.toString()?.split(",")
def filters = [Filter.equal("name","S_ZPLP"),
               Filter.equal("sku",sku),
               Filter.lessOrEqual("attribute1",date),
               Filter.greaterOrEqual("attribute2",date),
               Filter.in("attribute7",currencyList)
              ]
def result = api.find("PX50",*filters)
api.trace("result",result)
return result
}