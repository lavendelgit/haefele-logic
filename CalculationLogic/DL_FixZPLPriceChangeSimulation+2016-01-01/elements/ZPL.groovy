String targetDate = out.TargetDate

if (!api.global.zplPrices)
    api.global.zplPrices = [:]

if (!api.global.zplPrices[out.Material]) {  
  List filters = [
          Filter.equal("attribute8", "EUR"),
          Filter.lessOrEqual("attribute12", targetDate),
          Filter.greaterOrEqual("attribute13", targetDate),
          Filter.or(Filter.equal("attribute6","DEX1"),Filter.equal("attribute6",""))
  ]

  String sku = out.Material

  def ZPLCost = getLatestZPLCost(sku, *filters)?.attribute7

  if (ZPLCost && ZPLCost > 0) {
      api.global.zplPrices[out.Material] = ZPLCost/100
  }
  else
    api.global.zplPrices[out.Material] = ZPLCost
}

return api.global.zplPrices[out.Material]

def getLatestZPLCost(String sku, Filter... additionalFilters) {
    def targetDate = out.TargetDate

    def filters = [
            Filter.equal("name", "S_ZPL"),
            Filter.equal("sku", sku)
    ]

    if (additionalFilters != null) {
        filters.addAll(additionalFilters)
    }
    //api.trace("additionalFilters",additionalFilters as List)
    def orderBy = "-attribute1"
    def ZPLRecord = api.find("PX50", 0, 1, orderBy, *filters)
    def result = [:]

    if (ZPLRecord[0]) {
        result = ZPLRecord[0]
    }

    return result
}