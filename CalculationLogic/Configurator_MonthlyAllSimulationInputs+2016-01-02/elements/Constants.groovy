import groovy.transform.Field

@Field String DM_SIMULATION = 'PriceChangeSimulationDM_2020_Monthly'

@Field Map PAGE_DEF = [name     : 'ExceptionsConfigurator',
                       title    : 'Exceptions',
                       fieldList: [
                               []
                       ]]


@Field Map EXCEPTION_COLUMNS = [
        ExceptionKey                 : [name: 'ExceptionKey', type: 'Text', matrixColType: 'Text', label: 'Key'],
        ExceptionFilter              : [name: 'ExceptionFilter', type: 'DatamartFilter', matrixColType: 'Text', label: 'Data Filter'],
        MonthlyExceptionConfiguration: [name: 'MonthlyExceptionConfiguration', type: 'Option', matrixColType: 'Text', label: 'Monthly Exception Configuration']
]

@Field String INPUT_CONFIGURATION_TABLE_NAME = "MonthlyPricingSimulationInput"
@Field String INPUT_CONFIGURATION_EXCEPTION = "Exceptions Inputs"
@Field String INPUT_CONFIGURATION_SIMULATION = "Simulation Inputs"
@Field String INPUT_CONFIGURATION_INTERNAL_ARTICLE = "Internal Articles"

/**
 *
 * @param configurationName
 * @return
 */
protected List getPriceChangeExceptionInputsReferenceName(String configurationName) {
    List exceptionInputsRefNames = api.findLookupTableValues(INPUT_CONFIGURATION_TABLE_NAME, Filter.equal("key1", configurationName))
    return exceptionInputsRefNames?.collect {
        it.key2
    }?.unique().sort()
}

@Field String SIMULATION_1 = 'Simulation 1'
@Field String SIMULATION_2 = 'Simulation 2'
@Field String SIMULATION_3 = 'Simulation 3'
@Field Map SIMULATION_INPUT_DATAMART_MAP = [(SIMULATION_1): 'PriceChangeSimulation_Result_V1',
                                            (SIMULATION_2): 'PriceChangeSimulation_Result_V2',
                                            (SIMULATION_3): 'PriceChangeSimulation_Result_V3']
@Field List ANALYSIS_YEARS = ['2020']

@Field Map SIMULATION_INPUT_COLUMNS = [
        Status                        : [name: 'Status', type: 'Hidden', label: 'Status', readOnly: true, hidden: true],
        SimulationTitle               : [name: 'SimulationTitle', type: 'Text', label: 'Simulation Title'],
        AnalysisYear                  : [name: 'AnalysisYear', type: 'Option', label: 'Analysis Year', optionsLoader: {
            return ANALYSIS_YEARS
        }],
        TargetYear                    : [name: 'TargetYear', type: 'Option', label: 'Target Year', optionsLoader: {
            return getSimulationTargetYears()
        }],
        SimulationDataFilter          : [name: 'SimulationDataFilter', type: 'DatamartFilter', label: 'Include rows for analysis'],
        SimulationMonthsFilter        : [name: 'SimulationMonthsFilter', type: 'Options', label: 'Include Months for analysis', optionsLoader: {
            return getSimulationMonths()
        }],

        MonthlyPricingSimulationInputs: [name: 'MonthlyPricingSimulationInputs', type: 'PopupConfigurator', label: 'PriceChange %', openButtonLabel: 'Configure Pricing Simulation Inputs', configuratorLogic: 'Configurator_MonthlyPricingSimulationInput']
]

List getSimulationTargetYears() {
    int year = api.calendar().get(java.util.Calendar.YEAR)
    return (0..2).collect { int idx ->
        (year + idx) + ""
    }
}

List getSimulationMonths() {

    return ['January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December']
}

void callBoundCallUtilFunction(String simulationName, String simStatus, Map simulation_inputs) {
    Date today = new Date()
    Map simulationRow = [status                : simStatus,
                         schedule_date         : today.format("yyyy-MM-dd'T'HH:mm:ss.SSS"),
                         simulation_owner      : api.user("loginName"),
                         simulation_description: simulation_inputs.SimulationTitle,
                         simulation_inputs     : simulation_inputs]
    libs.HaefeleCommon.BoundCallUtils.addOrUpdateJSONTableData("SimulationInputs", null, "JLTV", [simulationName], [simulationRow])
}

@Field Map SIMULATION_STATUS = [
        NEW       : [name: 'New', label: 'New', allowSimulation: true, info: 'Please provide input for simulation.', confirmAllowSimulation: true],
        SCHEDULED : [name: 'Scheduled', label: 'Scheduled', allowSimulation: false, info: 'Simulation processing has been scheduled. Please try executing the dashboard after sometime.'],
        PROCESSING: [name: 'Processing', label: 'Processing', allowSimulation: false, info: 'Simulation data is being processed. Please try executing the dashboard after sometime.'],
        ERROR     : [name: 'Error', label: 'Error', allowSimulation: true, info: 'Error processing simulation request. Please try again or contact support.', color: 'red', confirmAllowSimulation: true],
        READY     : [name: 'Ready', label: 'Ready', allowSimulation: true, info: "Simulation data is ready. Please click on 'Apply Settings' to see the data.", confirmAllowSimulation: true, color: '#2F7ED8', showDashoard: true]
]

@Field Map PREDICTION_SIMULATION_OPTIONS = [
        MAINTAIN_CURRENT_MARGIN        : "Absolute : To Maintain Current Target Margin",
        MAINTAIN_CURRENT_MARGIN_PERCENT: "% : To Maintain Current Target Margin %"
]

