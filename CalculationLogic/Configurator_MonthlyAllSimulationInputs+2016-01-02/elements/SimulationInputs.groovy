import net.pricefx.server.dto.calculation.ConfiguratorEntry

String simulationName
//def simulationCommon = libs.HaefeleCommon.Simulation
def simulationCommon = Constants

List simulationInputOptions = api.findLookupTableValues("SimulationInputs", api.global, ['name'], 'name')?.name

ConfiguratorEntry inputConfigurator = Library.createInputConfiguratorWithHiddenStoredValue("AllSimulationInputs")
Library.createDropdown(inputConfigurator, "SimulationName", "Select Simulation", null, simulationInputOptions, false, false, false) { String selectedSimulation ->
    simulationName = selectedSimulation
}
if (api.isDebugMode()) {
    simulationName = "Simulation 6"
}

List configEntries = [inputConfigurator]
//Load additional fields only if simulation is selected
if (simulationName) {
    //initilizing to empty map incase of null
    Map inputs = [:]
    Map allSimulationData = getSavedSimulationData(simulationName)
    Map simStatus = simulationCommon.SIMULATION_STATUS[allSimulationData.attributeExtension___status?.toUpperCase()] ?: simulationCommon.SIMULATION_STATUS.NEW

    String message = ""
    if (!simStatus.allowSimulation) {
        //Library.manageApplySettings(inputConfigurator, true) PLS DON"T DELETE
        message = "<div style='color:#FF0000;font-size: 12px'>? ${simStatus.info}</div><br>"
    } else {
        //Library.manageApplySettings(inputConfigurator, false) PLS DON"T DELETE
        imessage = "<div style='color:${simStatus.color ?: '#004D43'};font-size: 12px'>*** ${simStatus.info}</div><br>"
    }
    message += "<div style='font-size:10px;font-weight:bold;color:silver;letter-spacing: 3px;'>#1 SIMULATION CONFIG</div><hr>"
    inputConfigurator.setMessage(message)

    Map simulationData = allSimulationData?.attributeExtension___simulation_inputs ?: [:]
    simulationData.Status = simStatus.name

    simulationCommon.SIMULATION_INPUT_COLUMNS.each { String inputName, Map inputDef ->
        switch (inputDef.type) {
            case "Text":
                Library.createTextField(inputConfigurator, inputName, inputDef.label, simulationData[inputName], false, inputDef.readOnly ?: false, true)
                break
            case "DatamartFilter":
                Library.createDatamartFilterBuilder(inputConfigurator, inputName, inputDef.label, Constants.DM_SIMULATION, simulationData[inputName])
                break
            case "Option":
                List options = inputDef.options
                if (!options && inputDef.optionsLoader) {
                    options = inputDef.optionsLoader()
                }
                Library.createDropdown(inputConfigurator, inputName, inputDef.label, simulationData[inputName], options, false, false, true)
                break
            case "Options":
                List options = inputDef.options
                if (!options && inputDef.optionsLoader) {
                    options = inputDef.optionsLoader()
                }
                Library.createMultiSelectDropdown(inputConfigurator, inputName, inputDef.label, simulationData[inputName], options, false, false, true)
                break
            case "PopupConfigurator":
                Library.createPopupConfigurator(inputConfigurator, inputName, inputDef.label, inputDef.openButtonLabel, inputDef.configuratorLogic, simulationData[inputName])
                break
            case "Hidden":
                Library.createHiddenField(inputConfigurator, inputName, inputDef.label, simulationData[inputName], false, inputDef.readOnly ?: false, true)
                break
            case "Boolean":
                Library.createBooleanField(inputConfigurator, inputName, inputDef.label, false, false, inputDef.readOnly ?: false, inputDef.isNoReferesh ?: true)
                break
        }
    }


    //Predective Section
    ConfiguratorEntry predictiveConfigurator = Library.createInputConfiguratorWithHiddenStoredValue("PredictiveInputs")
    String predictionMsg = """
            <div style='font-size:10px;font-weight:bold;color:silver;letter-spacing: 3px;'>#2 PROJECTION CONFIG</div><hr>
            <div style='color:blue;font-size: 10px'>Below section allows you to enable the simulation module to compute the expected GrossPrice Change %. You have various simulation types to choose from..</div>
            <div style='color:red;font-size: 10px'>Note :- Any input for GrossPrice Change % in "MONTHLY PRICING SIMULATION DATA ENTRY" popup would be ignored.</div>
        """
    predictiveConfigurator.setMessage(predictionMsg)
    boolean hasSimulationChanged = api.input("SimulationName") != api.input("hdnSimulationName")
    Library.createHiddenField(predictiveConfigurator, "hdnSimulationName", "hdnSimulationName", api.input("SimulationName"), false, false, true)
    def predictGrossPricePercentChange = api.input("PredictGrossPricePercentChange")
    if (hasSimulationChanged) {
        predictGrossPricePercentChange = simulationData["PredictGrossPricePercentChange"]
    }
    predictGrossPricePercentChange = predictGrossPricePercentChange ?: false
    Library.createBooleanField(predictiveConfigurator, "PredictGrossPricePercentChange", "Compute GrossPrice Change %?", predictGrossPricePercentChange, false, false, false) { boolean isChecked ->
        predictGrossPricePercentChange = isChecked
    }
    if (predictGrossPricePercentChange) {
        Map predictionOptions = Constants.PREDICTION_SIMULATION_OPTIONS
        List predictSimulationOptions = predictionOptions.values() as List
        String predictSimulationType
        if (hasSimulationChanged) {
            predictSimulationType = simulationData["PredictForInputType"]
        } else {
            if (api.input("PredictForInputType")) {
                predictSimulationType = api.input("PredictForInputType")
            } else if (!simulationData["PredictForInputType"]) {
                predictSimulationType = simulationData["PredictForInputType"]
            }
        }


        Library.createDropdown(predictiveConfigurator, "PredictForInputType", "Simulation Type", predictSimulationType, predictSimulationOptions, false, false, false) { String selectedSimulation ->
            predictSimulationType = selectedSimulation
        }
    }
    configEntries.add(predictiveConfigurator)

    //ReRun Section
    if (simStatus.confirmAllowSimulation) {
        ConfiguratorEntry reRunConfigurator = Library.createInputConfiguratorWithHiddenStoredValue("ReRunInputs")
        reRunConfigurator.setMessage("<div style='font-size:10px;font-weight:bold;color:silver;letter-spacing: 3px;'>#3 RE-RUN SIMULATION</div><hr>")
        Library.createBooleanField(reRunConfigurator, "RescheduleSimulation", "Re-run Simulation? Existing data would be overriden.", false, false, false, true)
        configEntries.add(reRunConfigurator)
    }
}


return api.createConfiguratorEntryArray(configEntries)


Map getSavedSimulationData(String simulationName) {
    return api.findLookupTableValues("SimulationInputs", Filter.equal('name', simulationName))?.find()
}
