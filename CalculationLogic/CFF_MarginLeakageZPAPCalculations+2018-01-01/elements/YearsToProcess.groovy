if (!api.local.yearsToProcess) {
    def countOfYears = libs.__LIBRARY__.GeneralUtility.getYearCountFrom2018()
    api.local.yearsToProcess = libs.__LIBRARY__.GeneralUtility.getYearsListFromNow(countOfYears)
}

return api.local.yearsToProcess
