if (!api.local.transactionDetails) {
    def fields = ['InvoiceDateYear'          : 'Year',
                  'COUNTDISTINCT(CustomerId)': 'ActiveCustomerCount',
                  'COUNTDISTINCT(Material)'  : 'TransactedMaterialCount'
    ]
    def filters = libs.__LIBRARY__.HaefeleSpecific.getTransactionDMFilters()
    filters.add (Filter.isNotNull('InvoiceDateYear'))
    api.local.transactionDetails = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')
}

return api.local.transactionDetails