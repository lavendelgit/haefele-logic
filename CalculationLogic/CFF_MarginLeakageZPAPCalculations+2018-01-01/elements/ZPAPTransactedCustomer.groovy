def getCustomerCountWhoBoughtZPAPArticles(List yearsOfAnalysis) {
    if (!yearsOfAnalysis) {
        return [:]
    }
    def yearWiseData = [:]
    def fields = ['COUNTDISTINCT(customerId)': 'CustomerTransacted',
                  'COUNTDISTINCT(Material)'  : 'ZPAPTransactedMaterialCount']
    def filters
    def validityStartDate, validityEndDate
    def dbLib = libs.__LIBRARY__.DBQueries
    def generalLib = libs.__LIBRARY__.GeneralUtility
    yearsOfAnalysis.each { yearToInvestigate ->
        filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFilters()
        validityStartDate = yearToInvestigate + "-01-01"
        validityEndDate = yearToInvestigate + "-12-31"
        filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
        filters.add(Filter.isNotNull('Revenue'))
        filters.add(Filter.greaterThan('Revenue', 0))
        libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', yearToInvestigate)
        def results = dbLib.getDataFromSource(fields, 'CustomerSalesPricesDM', filters, 'DataMart')
        results.each { singleRow ->
            yearWiseData[yearToInvestigate] = [
                    'Year'                       : yearToInvestigate,
                    'CustomerTransacted'         : singleRow['CustomerTransacted'],
                    'ZPAPTransactedMaterialCount': singleRow['ZPAPTransactedMaterialCount']
            ]
        }
    }
    return yearWiseData
}

return getCustomerCountWhoBoughtZPAPArticles(out.YearsToProcess)