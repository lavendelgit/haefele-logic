def getCustomerZPAPArticlesCount(List yearsOfAnalysis) {
    if (!yearsOfAnalysis) {
        return [:]
    }
    def yearWiseData = [:]
    def fields = ['COUNTDISTINCT(customerId)': 'ZPAPCustomerCount',
                  'COUNTDISTINCT(Material)'  : 'ZPAPArticleCount',
                  'COUNT(CustomerSalesPrice)': 'ZPAPCount',
                  'MIN(DiscountPercentage)'  : 'ZPAPMinDiscountPer',
                  'MAX(DiscountPercentage)'  : 'ZPAPMaxDiscountPer',
                  'Avg(DiscountPercentage)'  : 'ZPAPAvgDiscountPer'
    ]
    def filters1, filters2
    def validityStartDate, validityEndDate
    def dbLib = libs.__LIBRARY__.DBQueries
    def generalLib = libs.__LIBRARY__.GeneralUtility
    yearsOfAnalysis.each { yearToInvestigate ->
        filters1 =  libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFiltersForAllMatches()
        filters2 =  libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFiltersForAllMatches()
        validityStartDate = yearToInvestigate + "-01-01"
        validityEndDate = yearToInvestigate + "-12-31"
        filters1.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
        filters2.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
        filters2.add(Filter.equal('MatchType', 'HLM'))
        libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', yearToInvestigate)
        def results1 = dbLib.getDataFromSource(fields, 'CustomerSalesPricesDM', filters1, 'DataMart')
        def results2 = dbLib.getDataFromSource(fields, 'CustomerSalesPricesDM', filters2, 'DataMart')
        results1.each { singleRow ->
            yearWiseData[yearToInvestigate] = [
                    'Year'                        : yearToInvestigate,
                    'ZPAPCustomerCount'           : singleRow['ZPAPCustomerCount'],
                    'ZPAPArticleCount'            : singleRow['ZPAPArticleCount'],
                    'ZPAPCount'                   : singleRow['ZPAPCount'],
                    'ZPAPMinDiscountPer'          : singleRow['ZPAPMinDiscountPer'],
                    'ZPAPMaxDiscountPer'          : singleRow['ZPAPMaxDiscountPer'],
                    'ZPAPAvgDiscountPer'          : singleRow['ZPAPAvgDiscountPer'],
                    'ZPAPCountGTPerMaxDiscPer'    : (results2 && results2.size() > 0) ? results2[0]['ZPAPCount'] : 0,
                    'MaterialCountGTPerMaxDiscPer': (results2 && results2.size() > 0) ? results2[0]['ZPAPArticleCount'] : 0,
                    'CustomerCountGTPerMaxDiscPer': (results2 && results2.size() > 0) ? results2[0]['ZPAPCustomerCount'] : 0]
        }
    }

    return yearWiseData
}

return getCustomerZPAPArticlesCount(out.YearsToProcess)