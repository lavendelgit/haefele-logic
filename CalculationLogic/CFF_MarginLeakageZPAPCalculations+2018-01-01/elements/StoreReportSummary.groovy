//insertIntoPPTable(Map fieldMap, String ppTableName, String typeCode)
def getPopulatedRecord(String reportName, String fieldName, def fieldDim, def attribute1) {

    return [
            'key1'      : reportName,
            'key2'      : fieldName,
            'key3'      : fieldDim,
            'attribute1': attribute1
    ]
}

def rows = []

//Active Customers and Transacted Articles
String reportName = 'ZPAPAnalysis'
def transColumns = ['Year', 'ActiveCustomerCount', 'TransactedMaterialCount']
def transactionDetails = out.ComputeTransactionData
transactionDetails.each { transaction ->
    rows.add(getPopulatedRecord(reportName, transColumns[1], transaction[transColumns[0]], transaction[transColumns[1]]))
    rows.add(getPopulatedRecord(reportName, transColumns[2], transaction[transColumns[0]], transaction[transColumns[2]]))
}

//Within Active Customers how many transactions has Material id from customers related ZPAP.
def spdColumns = ['Year', 'CustomerTransacted', 'ZPAPTransactedMaterialCount']
def salesPriceData = out.ZPAPTransactedCustomer
salesPriceData.each { year, salesPriceRecord ->
    rows.add(getPopulatedRecord(reportName, spdColumns[1], year, salesPriceRecord[spdColumns[1]]))
    rows.add(getPopulatedRecord(reportName, spdColumns[2], year, salesPriceRecord[spdColumns[2]]))
}

//Provisioned Customers and Articles participating in ZPAP
def zpapColumns = ['Year', 'ZPAPCustomerCount', 'ZPAPArticleCount', 'ZPAPCount', 'ZPAPMinDiscountPer',
                   'ZPAPMaxDiscountPer', 'ZPAPAvgDiscountPer', 'ZPAPCountGTPerMaxDiscPer',
                   'MaterialCountGTPerMaxDiscPer', 'CustomerCountGTPerMaxDiscPer']
def zpapCount = out.ComputeSalesPriceData
zpapCount.each { year, zpapRecord ->
    rows.add(getPopulatedRecord(reportName, zpapColumns[1], year, zpapRecord[zpapColumns[1]]))
    rows.add(getPopulatedRecord(reportName, zpapColumns[2], year, zpapRecord[zpapColumns[2]]))
    rows.add(getPopulatedRecord(reportName, zpapColumns[3], year, zpapRecord[zpapColumns[3]]))
    rows.add(getPopulatedRecord(reportName, zpapColumns[4], year, zpapRecord[zpapColumns[4]]))
    rows.add(getPopulatedRecord(reportName, zpapColumns[5], year, zpapRecord[zpapColumns[5]]))
    rows.add(getPopulatedRecord(reportName, zpapColumns[6], year, zpapRecord[zpapColumns[6]]))
    rows.add(getPopulatedRecord(reportName, zpapColumns[7], year, zpapRecord[zpapColumns[7]]))
    rows.add(getPopulatedRecord(reportName, zpapColumns[8], year, zpapRecord[zpapColumns[8]]))
    rows.add(getPopulatedRecord(reportName, zpapColumns[9], year, zpapRecord[zpapColumns[9]]))
}

//Active Products counts in system
def totalActiveArticles = out.TotalNumberOfArticles
def years = api.local.yearsToProcess
years.each { year ->
    rows.add(getPopulatedRecord(reportName, 'TotalArticlesCount', year, totalActiveArticles))
}

//Insert Records in PP Table...
final String ppName = 'PreCalculatedDashboardField'
rows.each { row ->
    libs.__LIBRARY__.TraceUtility.developmentTraceRow('Printing rows to be inserted', row)
    libs.__LIBRARY__.DBQueries.insertIntoPPTable(row, ppName, 'MLTV3')
}