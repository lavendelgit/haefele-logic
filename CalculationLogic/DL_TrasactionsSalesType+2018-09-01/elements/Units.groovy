def revenue = api.getElement("Revenue") as BigDecimal
def salesPricePer100Units = api.getElement("SalesPricePer100Units") as BigDecimal

api.trace("revenue", "", revenue)
api.trace("salesPricePer100Units", "", salesPricePer100Units)


if (revenue != null && salesPricePer100Units != null && salesPricePer100Units != 0) {
    return (revenue/(salesPricePer100Units/100)).setScale(0, BigDecimal.ROUND_HALF_UP)
}