def sku = api.getElement("ArticleNumber")
def customerId = api.getElement("CustomerId")
def focusGroup = api.getElement("FocusGroup")
def transactionDate = api.getElement("TransactionDate")
def lookBackDays = api.getElement("SalesPersonDiscountLookbackDays")
def dateToFilter

if (transactionDate) {
    dateToFilter = Date.parse("yyyy-MM-dd", transactionDate)?.minus(lookBackDays)?.format("yyyy-MM-dd")
} else {
    dateToFilter = new Date().minus(lookBackDays)?.format("yyyy-MM-dd")
}
api.trace("Printing the date and difference is indeed 400 days ", "(ToFilter " + dateToFilter + ") Other date" + transactionDate)

if (api.global.salesPersonDiscounts == null) {
    def salesPersonDiscounts = [:]

    def attributes = [
            "attribute4",
            "sku",
            "attribute1",
            "attribute2",
            "attribute3",
            "attribute13"
    ]

    def filters = [
            Filter.equal("name", "S_ZRC"),
            Filter.greaterOrEqual("attribute1", dateToFilter)
    ]

    def stream = api.stream("PX50", "attribute4", attributes, *filters)
    if (!stream) {
        return []
    }

    while (stream.hasNext()) {
        def record = stream.next()
        def customerDiscounts = salesPersonDiscounts[record.attribute4] ?: []
        customerDiscounts.add([
                customerId: record.aatribute4,
                sku       : record.sku,
                focusGroup: record.attribute3,
                validFrom : record.attribute1,
                validTo   : record.attribute2,
                discount  : record.attribute13
        ])
        salesPersonDiscounts[record.attribute4] = customerDiscounts
    }

    stream.close()

    // api.trace("salesPersonDiscounts", "", salesPersonDiscounts)
    api.global.salesPersonDiscounts = salesPersonDiscounts
}
def discountsForThisCustomer = api.global.salesPersonDiscounts[customerId]
api.trace("discountsForThisCustomer", "", discountsForThisCustomer)
def discountAmount = discountsForThisCustomer
        ?.find { spd -> (spd.sku == sku || (spd.focusGroup != null && spd.focusGroup == focusGroup)) /* &&
                         spd.validFrom <= transactionDate &&
                         spd.validTo >= transactionDate*/
        }
        ?.discount
api.trace("discount", discountAmount)
api.trace("salesPersonDiscounts", "(" + customerId + ")" + "(" + sku + ") (" + transactionDate + ")", " List: " + api.global.salesPersonDiscounts[customerId])
discountAmount = (discountAmount) ? new BigDecimal(discountAmount).abs().multiply(100) : 0

return discountAmount