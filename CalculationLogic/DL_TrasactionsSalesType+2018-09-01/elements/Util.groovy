def customerPotentialDiscounts() {
    return [
            R0: 0.0,
            R1: 8.0,
            R2: 13.0,
            R3: 18.0
    ]
}

def equalWithRounding(BigDecimal value1, BigDecimal value2) {
    return lib.Rounding.roundMoney(value1) == lib.Rounding.roundMoney(value2)
}

def equalWithTolerance(BigDecimal value1, BigDecimal value2, BigDecimal tolerance) {
    if (value1 == null && value2 == null) {
        return true
    }
    if (value1 == null || value2 == null) {
        return false
    }
    def diff = (value2 - value1).abs()
    return diff <= tolerance
}

def findPrice(pricesMap, BigDecimal targetPrice) {
    def salesType = pricesMap.find { p -> p.value == targetPrice }?.key

    if (salesType) {
        return [
            salesType: salesType,
            foundBy: "eqaul",
            matchedPrice: pricesMap[salesType]
        ]
    }

    salesType = pricesMap.find { p -> equalWithRounding(p.value as BigDecimal, targetPrice) }?.key

    if (salesType) {
        return [
            salesType: salesType,
            foundBy: "equalWithRounding",
            matchedPrice: pricesMap[salesType]
        ]
    }

    salesType = pricesMap.find { p -> equalWithTolerance(p.value as BigDecimal, targetPrice, 5) }?.key

    if (salesType) {
        return [
            salesType: salesType,
            foundBy: "equalWithTolerance",
            matchedPrice: pricesMap[salesType]
        ]
    }

    return null
}