def inputSalesOffice = api.stringUserEntry("SalesOffice")

if (!api.global.codeSalesOfficeMap) {
  def salesOfficeDetails = api.findLookupTableValues("SalesOffices")
  api.trace("SalesOffice make or break","("+salesOfficeDetails+")")
  def salesOfficeMap = salesOfficeDetails.collectEntries {
    [(it.name): it.value]
  }
  api.trace("SalesOffice Initialize","("+salesOfficeMap.size()+") salesOfficeDetails ("+salesOfficeDetails.size() +")" )
  int count = salesOfficeMap?.size()
  def keys = salesOfficeMap?.keySet()?.asList()
  int i=0
  api.global.codeSalesOfficeMap = [:]
  api.global.salesOfficeCodeMap = [:]
  def key
  def value
  while (i<count) {
    key = keys[i]
    value = salesOfficeMap[key]
    api.global.codeSalesOfficeMap[key] = value
    api.global.salesOfficeCodeMap[value] = key
    i++
  }
}

def isCodeCorrect = api.global.codeSalesOfficeMap[inputSalesOffice]
if (isCodeCorrect) {
  api.trace ("SalesOffice Computation","("+inputSalesOffice+")")
  return inputSalesOffice
}

def isCodeInputCorrect = api.global.salesOfficeCodeMap[inputSalesOffice]
def outputSalesOffice = (isCodeInputCorrect)? isCodeInputCorrect: ""
api.trace ("SalesOffice Text to code Computation", "("+outputSalesOffice+")")
return outputSalesOffice
