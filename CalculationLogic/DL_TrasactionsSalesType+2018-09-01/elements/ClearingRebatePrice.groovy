def regularSalesPrice = api.getElement("RegularSalesPricePer100Units")
def clearingRebateRecord = api.getElement("ClearingRebate")
def clearingRebate = clearingRebateRecord?.attribute4

if (regularSalesPrice != null && clearingRebate != null) {
    return (regularSalesPrice * (1 + clearingRebate)).setScale(2, BigDecimal.ROUND_HALF_UP)
}