def customerPotentialGroup = api.stringUserEntry("CustomerPotentialGroup")
def cpgList = ["R0", "R1", "R2", "R3"]
if (cpgList.contains (customerPotentialGroup))
	return customerPotentialGroup

int i=0
while (i<4) {
  if (customerPotentialGroup.contains(cpgList[i]))
  	return cpgList[i]
  i++
}

return ""