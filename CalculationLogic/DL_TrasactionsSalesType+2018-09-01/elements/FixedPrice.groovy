def sku = api.getElement("ArticleNumber")
def customerId = api.getElement("CustomerId")
def transactionDate = api.getElement("TransactionDate")

lib.Find.fixedPrice(sku, customerId, transactionDate)