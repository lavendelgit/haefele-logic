def sku = api.getElement("ArticleNumber")
def customerId = api.getElement("CustomerId")
def invoiceId = api.getElement("InvoiceId")
def invoicePos = api.getElement("InvoicePos")
def invoiceSalesPricePer100 = api.getElement("SalesPricePer100Units")
def transactionDate = api.getElement("TransactionDate")
def lookBackDays = api.getElement("SalesPersonDiscountLookbackDays")

// cache CRM projects for the same SKU - assumes that the DL target is sorted by SKU
if (api.global.crmProjects?.find()?.sku != sku) {
    api.global.crmProjects = []
    def filters = [
            Filter.equal("name", "CRMProject"),
            Filter.like("sku", sku + "%")
    ]
    def attributes = [
            "attribute5",  // customerId
            "attribute6",  // invoiceId
            "attribute7",  // invoicePos
            "attribute10", // units
            "attribute11"  // revenue
    ]
    def stream = api.stream("PX", "sku" /* sort-by*/, attributes, *filters)

    while (stream?.hasNext()) {
        def crmProject = stream.next();
        def units = (crmProject.attribute10 ?: "0") as BigDecimal
        def revenue = (crmProject.attribute11 ?: "0") as BigDecimal
    	def pricePer100Units
     	if (units != 0) {
            pricePer100Units = revenue / units * 100.0
        }
        api.global.crmProjects.add([
                sku: sku, // deliberately not crmProject.sku
                customerId: crmProject.attribute5,
                invoiceId: crmProject.attribute6,
                invoicePos: crmProject.attribute7,
                units: units,
                revenue: revenue,
                pricePer100Units: pricePer100Units
        ])
    }

    stream?.close()
}

def crmProject = api.global.crmProjects?.find { r ->
    return r.customerId == customerId &&
            (r.invoiceId == invoiceId || r.invoiceId == invoiceId.padLeft(10, "0")) &&
            (r.invoicePos == invoicePos || r.invoicePos == invoicePos.padLeft(6, "0"))
}

if (crmProject) {
    // just use the invoice sales price per 100 for better matching
    // the calculated value is often too far from the invoice value due to rounding
    crmProject.pricePer100Units = invoiceSalesPricePer100
}

return crmProject
