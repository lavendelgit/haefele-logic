def sku = api.getElement("ArticleNumber")
def transactionDate = api.getElement("TransactionDate")

lib.Find.occasionPrice(sku, transactionDate)