def year = api.getElement("InvoiceDate")

if (year) {
  year = year.format("yyyy")
}

return year