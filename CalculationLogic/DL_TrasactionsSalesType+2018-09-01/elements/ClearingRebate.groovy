def sku = api.getElement("ArticleNumber")
def customerId = api.getElement("CustomerId")
def transactionDate = api.getElement("TransactionDate")
def lookBackDays = api.getElement("SalesPersonDiscountLookbackDays")

//return lib.Find.latestClearingRebateRecord2(customerId, sku, transactionDate, lookBackDays)

def clearingRebateRecord = lib.Find.latestClearingRebateRecord(sku,
        Filter.equal("attribute1", customerId),
        Filter.lessOrEqual("attribute2", transactionDate),
        Filter.greaterOrEqual("attribute3", transactionDate)
)