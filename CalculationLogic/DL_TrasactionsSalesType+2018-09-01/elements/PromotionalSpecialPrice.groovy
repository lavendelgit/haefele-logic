def sku = api.getElement("ArticleNumber")
def transactionDate = api.getElement("TransactionDate")

lib.Find.promotionalSpecialPrice(sku, transactionDate)