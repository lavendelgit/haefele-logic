actionBuilder
        .addCalculatedFieldSetAction("PopulateCustomerIdZPF")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("PopulateCustomerIdZPA")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("PopulateCustomerIdZRC")
        .setCalculate(true)

actionBuilder
        .addCalculatedFieldSetAction("PopulateCustomerIdZPAP")
        .setCalculate(true)