String sku = out.Material

List transactionData

if (api.global.currentMaterail != sku) {
    api.global.currentMaterail = sku

    api.global.transactionData = [:]
    BigDecimal totRevenue = 0
    Integer totQuanity = 0

    Map fields = ['Material'                                                 : 'SKU',
                  'SUM(Revenue)'                                             : 'revenue',
                  'SUM(InvoiceQuantity)'                                     : 'quantity',
                  'CustomerId'                                               : 'CustomerId'
    ]

    List filters = [Filter.equal("Material", sku),Filter.equal("InvoiceDateYear",out.AnalysisYear)]
    filters << out.TransactionFilter
  
    transactionData = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')?.collect()

    transactionData.forEach { transactionRow ->
        api.global.transactionData.put(transactionRow.CustomerId, ["revenue":transactionRow.revenue,"quantity":transactionRow.quantity,"isConsidered":false])
        totRevenue += (transactionRow.revenue?:0) as BigDecimal
        totQuanity += transactionRow.quantity?:0
    }
    api.global.transactionData.put(Constants.ALL_CUSTOMERS,["revenue":totRevenue,"quantity":totQuanity])
}