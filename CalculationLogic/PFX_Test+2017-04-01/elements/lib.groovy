boolean compareFCs(String name1, String name2, String[] keys) {
  def ctx = api.getDatamartContext()
  def fc1 = ctx.getFieldCollection(name1)
  def fc2 = ctx.getFieldCollection(name2)
  
  def query1 = ctx.newQuery(fc1)
  query1.selectAll().orderBy(keys)
  def results1 = ctx.streamQuery(query1)  
  
  def query2 = ctx.newQuery(fc2)
  query2.selectAll().orderBy(keys)
  def results2 = ctx.streamQuery(query2)  
  
  
  def i = 0
  def match = true
  while(results1?.next() && results2?.next()) {
    def row1 = results1.get()
    def row2 = results2.get()
    
    if (++i % 10000 == 0) {
      api.logInfo("$i", o)
      def rows = i/1000
      api.trace("streamQuery", "${rows}k rows", o)
    }
    
    if (!compareValues(row1, row2)) {
      match = false
      
      checkIfCompatibleKeys(row1, row2)
      
      break
    }
  }
  results1?.close()
  results2?.close()
  return match
}

def checkIfCompatibleKeys(row1, row2) {
  def keyList1 = row1.collect { it.key }
  def keyList2 = row2.collect { it.key }
  
  for (key1 in keyList1) {
    if (!keyList2.contains(key1)) {
      api.trace("compatibleRows", "missing in FC2", key1)
    }
  }
  
  for (key2 in keyList2) {
    if (!keyList1.contains(key2)) {
      api.trace("compatibleRows", "missing in FC1", key2)
    }
  }
  
}

boolean compareValues(row1, row2) {
  for (entry1 in row1) {
    def value2 = row2.get(entry1.key)
    if (value2 != entry1.value) {
      api.trace("compareValues", "rows differ on ${entry1.key} / ${entry1.value}", row1)
      api.trace("compareValues", "rows differ on ${entry1.key} / ${value2}", row2)
      return false
    }
  }
  return true
}
