def quantity = api.getElement("Quantity") ?: 1
def discount = api.getElement("Discount") ?: 0
def unitPrice = api.getElement("UnitPrice")

return unitPrice * (1 - discount) * quantity