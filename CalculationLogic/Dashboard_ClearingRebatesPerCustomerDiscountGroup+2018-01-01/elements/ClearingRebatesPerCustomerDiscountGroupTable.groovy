package Dashboard_ClearingRebatesPerCustomerDiscountGroup.elements

def resultMatrix = api.newMatrix("Discount Group","Count of Clearing Rebates")
resultMatrix.setTitle("Count of Clearing Rebates per Discount Group")
resultMatrix.setColumnFormat("Count of Clearing Rebates", FieldFormatType.NUMERIC)

def clearingRebatesPerCustomerDiscountGroup = api.getElement("ClearingRebatesPerCustomerDiscountGroup")

def total = 0

for (row in clearingRebatesPerCustomerDiscountGroup) {
    resultMatrix.addRow([
            "Discount Group" : row.key,
            "Count of Clearing Rebates" : row.value
    ])
    total += row.value
}

resultMatrix.addRow([
        "Discount Group": resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of Clearing Rebates": resultMatrix.styledCell(total).withCSS("font-weight: bold")
])

return resultMatrix
