package Dashboard_ClearingRebatesPerCustomerDiscountGroup.elements

def result = []

def stream = api.stream("C", null, ["customerId", "attribute6"]) // attribute6 = customer discount group (R0,R1,R2,R3)

if (!stream) {
    return []
}

while (stream.hasNext()) {
    result.add(stream.next())
}

stream.close()

return result