package Dashboard_ClearingRebatesPerCustomerDiscountGroup.elements

def zpapsPerCustomerDiscountGroup = api.getElement("ClearingRebatesPerCustomerDiscountGroup")

def categories = zpapsPerCustomerDiscountGroup.collect { e -> e.key}
def data = zpapsPerCustomerDiscountGroup.collect { e -> e.value};

def chartDef = [
        chart: [
            type: 'column'
        ],
        title: [
            text: 'Clearing Rebates per Discount Group'
        ],
        xAxis: [[
            title: [
                    text: "Discount Group"
            ],
            categories: categories
        ]],
        yAxis: [[
            title: [
                text: "Number of Clearing Rebates"
            ]
        ]],
        tooltip: [
            headerFormat: 'Discount Group: {point.key}<br/>',
            pointFormat: 'Number of Clearing Rebates: {point.y}'
        ],
        legend: [
            enabled: false
        ],
        series: [[
             data: data,
             dataLabels: [
                 enabled: true,
             ]
        ]]
]

api.buildFlexChart(chartDef)