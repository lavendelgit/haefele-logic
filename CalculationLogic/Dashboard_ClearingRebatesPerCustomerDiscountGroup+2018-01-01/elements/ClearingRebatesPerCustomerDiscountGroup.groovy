package Dashboard_ClearingRebatesPerCustomerDiscountGroup.elements

def customerDiscountGroups = api.getElement("CustomersDiscountGroups")
def clearingRebatesPerCustomer = api.getElement("ClearingRebatesPerCustomer")

return customerDiscountGroups
        .groupBy { c -> c.attribute6 ?: "Undefined" }
        .collectEntries { discountGroup, customers ->
            [discountGroup, customers.collect { c -> clearingRebatesPerCustomer[c.customerId] ?: 0 }.sum()]
        }
        .findAll { e -> e.value > 0}
        .sort { e1, e2 -> e1.key <=> e2.key }