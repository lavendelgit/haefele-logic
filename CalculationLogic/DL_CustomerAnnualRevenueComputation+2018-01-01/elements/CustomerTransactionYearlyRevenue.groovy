// Prepare the details for the output
if (!out.CurrentCustomerToProcess) {
    api.logWarn("Customer Summary Data", "NO Data for Customer Summary Data")
    return true
}

def years = lib.GeneralUtility.getYearsListFromNow(5)
def records = out.CustomerSalesHistoryNTransactionYearlyRevenue
lib.TraceUtility.developmentTrace("records", records)

def sortedData = sortRecordsAccordingToYears(years, records)
lib.TraceUtility.developmentTrace("The details...", sortedData)

def shYearlyRevenueList = [sortedData[0]['t1yr'], sortedData[1]['t1yr'], sortedData[2]['t1yr'], sortedData[3]['t1yr'], sortedData[4]['t1yr']]
def thYearlyRevenueList = [sortedData[0]['t2yr'], sortedData[1]['t2yr'], sortedData[2]['t2yr'], sortedData[3]['t2yr'], sortedData[4]['t2yr']]
lib.TraceUtility.developmentTrace("shYearlyRevenueList ", shYearlyRevenueList)
lib.TraceUtility.developmentTrace("thYearlyRevenueList ", thYearlyRevenueList)
def avgSHRevenue = getAvg(shYearlyRevenueList)
def avgTHRevenue = getAvg(thYearlyRevenueList)

def shTotalYearlyRevenue = getSHTotalRevenueYearly()
def thTotalYearlyRevenue = getTHTotalRevenueYearly()
lib.TraceUtility.developmentTrace("shTotalYearlyRevenue ", shTotalYearlyRevenue)
lib.TraceUtility.developmentTrace("thTotalYearlyRevenue ", thTotalYearlyRevenue)

def shTotalYearlyDB = getSHTotalDBYearly()
def thTotalYearlyDB = getTHTotalDBYearly()
def shDBYearlyList = [sortedData[0]['t1ym'], sortedData[1]['t1ym'], sortedData[2]['t1ym'], sortedData[3]['t1ym'], sortedData[4]['t1ym']]
def thDBYearlyList = [sortedData[0]['t2ym'], sortedData[1]['t2ym'], sortedData[2]['t2ym'], sortedData[3]['t2ym'], sortedData[4]['t2ym']]
def avgSHDB = getAvg(shDBYearlyList)
def avgTHDB = getAvg(thDBYearlyList)
def transactionCountList = getTransactionsCountList(sortedData)

def curPotGrp = out.CustomerInformation["attribute6"][0]
def custName = out.CustomerInformation["name"][0]
def compPotGrp = lib.Find.getMatchingCustomerPotentialGroup(thYearlyRevenueList[0])

def outputRecord = [
        'CustomerId'               : out.CurrentCustomerToProcess,
        'CustomerName'             : custName,
        'SHYear1Umsatz'            : sortedData[0]['t1yr'],
        'SHYear1DB'                : sortedData[0]['t1ym'],
        'THYear1Umsatz'            : sortedData[0]['t2yr'],
        'THYear1DB'                : sortedData[0]['t2ym'],
        'SHYear2Umsatz'            : sortedData[1]['t1yr'],
        'SHYear2DB'                : sortedData[1]['t1ym'],
        'THYear2DB'                : sortedData[1]['t2ym'],
        'THYear2Umsatz'            : sortedData[1]['t2yr'],
        'SHYear3Umsatz'            : sortedData[2]['t1yr'],
        'SHYear3DB'                : sortedData[2]['t1ym'],
        'THYear3Umsatz'            : sortedData[2]['t2yr'],
        'THYear3DB'                : sortedData[2]['t2ym'],
        'SHYear4Umsatz'            : sortedData[3]['t1yr'],
        'SHYear4DB'                : sortedData[3]['t1ym'],
        'THYear4Umsatz'            : sortedData[3]['t2yr'],
        'THYear4DB'                : sortedData[3]['t2ym'],
        'SHYear5Umsatz'            : sortedData[4]['t1yr'],
        'SHYear5DB'                : sortedData[4]['t1ym'],
        'THYear5Umsatzh'           : sortedData[4]['t2yr'],
        'THYear5DB'                : sortedData[4]['t2ym'],
        'SHAvgUmsatz'              : avgSHRevenue,
        'THAvgUmsatz'              : avgTHRevenue,
        'SHAvgDB'                  : avgSHDB,
        'THAvgDB'                  : avgTHDB,
        'SHAgeOfCustomer'          : getCustomerAge(shYearlyRevenueList),
        'THAgeOfCustomer'          : getCustomerAge(thYearlyRevenueList),
        'SHY1PerUmsatzContribution': getRevenuePercentageContribution(shYearlyRevenueList[0], shTotalYearlyRevenue["${years[0]}"]),
        'SHY2PerUmsatzContribution': getRevenuePercentageContribution(shYearlyRevenueList[1], shTotalYearlyRevenue["${years[1]}"]),
        'SHY3PerUmsatzContribution': getRevenuePercentageContribution(shYearlyRevenueList[2], shTotalYearlyRevenue["${years[2]}"]),
        'SHY4PerUmsatzContribution': getRevenuePercentageContribution(shYearlyRevenueList[3], shTotalYearlyRevenue["${years[3]}"]),
        'SHY5PerUmsatzContribution': getRevenuePercentageContribution(shYearlyRevenueList[4], shTotalYearlyRevenue["${years[4]}"]),
        'THY1PerUmsatzContribution': getRevenuePercentageContribution(thYearlyRevenueList[0], thTotalYearlyRevenue["${years[0]}"], false),
        'THY2PerUmsatzContribution': getRevenuePercentageContribution(thYearlyRevenueList[1], thTotalYearlyRevenue["${years[1]}"], false),
        'THY3PerUmsatzContribution': getRevenuePercentageContribution(thYearlyRevenueList[2], thTotalYearlyRevenue["${years[2]}"], false),
        'THY4PerUmsatzContribution': getRevenuePercentageContribution(thYearlyRevenueList[3], thTotalYearlyRevenue["${years[3]}"], false),
        'THY5PerUmsatzContribution': getRevenuePercentageContribution(thYearlyRevenueList[4], thTotalYearlyRevenue["${years[4]}"], false),
        'SHY1PerDBContribution'    : getMarginPercentageContribution(shDBYearlyList[0], shTotalYearlyDB["${years[0]}"]),
        'SHY2PerDBContribution'    : getMarginPercentageContribution(shDBYearlyList[1], shTotalYearlyDB["${years[1]}"]),
        'SHY3PerDBContribution'    : getMarginPercentageContribution(shDBYearlyList[2], shTotalYearlyDB["${years[2]}"]),
        'SHY4PerDBContribution'    : getMarginPercentageContribution(shDBYearlyList[3], shTotalYearlyDB["${years[3]}"]),
        'SHY5PerDBContribution'    : getMarginPercentageContribution(shDBYearlyList[4], shTotalYearlyDB["${years[4]}"]),
        'THY1PerDBContribution'    : getMarginPercentageContribution(thDBYearlyList[0], thTotalYearlyDB["${years[0]}"]),
        'THY2PerDBContribution'    : getMarginPercentageContribution(thDBYearlyList[1], thTotalYearlyDB["${years[1]}"]),
        'THY3PerDBContribution'    : getMarginPercentageContribution(thDBYearlyList[2], thTotalYearlyDB["${years[2]}"]),
        'THY4PerDBContribution'    : getMarginPercentageContribution(thDBYearlyList[3], thTotalYearlyDB["${years[3]}"]),
        'THY5PerDBContribution'    : getMarginPercentageContribution(thDBYearlyList[4], thTotalYearlyDB["${years[4]}"]),
        'THY1TransactionCount'     : transactionCountList[0],
        'THY2TransactionCount'     : transactionCountList[1],
        'THY3TransactionCount'     : transactionCountList[2],
        'THY4TransactionCount'     : transactionCountList[3],
        'THY5TransactionCount'     : transactionCountList[4],
        'SHCAGR'                   : getCAGR(shYearlyRevenueList),
        'THCAGR'                   : getCAGR(thYearlyRevenueList, shYearlyRevenueList),
        'CustomerPotentialGroup'   : curPotGrp,
        'ComputedPotentialGroup'   : compPotGrp,
        'CorrectionType'           : lib.Find.getCorrectionType(curPotGrp, compPotGrp)
]
lib.DBQueries.insertRecordIntoTargetRowSet(outputRecord)



def getSHTotalDBYearly() {
    return api.global.yearlySHRevenue
}

def getTHTotalDBYearly() {
    return api.global.yearlyTHRevenue
}

def getCAGR(List totalYearlyRevenue, List salesYearlyRevenue) {
    int size = totalYearlyRevenue.size()
    int i = size - 1
    def startingValue = 0.0 as BigDecimal, endingValue = 0.0 as BigDecimal
    int numberOfYears = 0
    boolean transactionExist, salesHistoryExist
    while (i > 0) {
        transactionExist = (totalYearlyRevenue[i] != 0.0)
        salesHistoryExist = (salesYearlyRevenue[i] != 0.0)
        if (!startingValue && (transactionExist || salesHistoryExist)) {
            startingValue = (transactionExist) ? totalYearlyRevenue[i] : salesYearlyRevenue[i]
            endingValue = (transactionExist) ? totalYearlyRevenue[i] : salesYearlyRevenue[i]
            numberOfYears++
        } else {
            if (transactionExist || salesHistoryExist) {
                endingValue = (transactionExist) ? totalYearlyRevenue[i] : salesYearlyRevenue[i]
                numberOfYears++
            }
        }
        i--
    }
    lib.TraceUtility.developmentTraceRow(" The CAGR Comoputations ", [endingValue, startingValue, numberOfYears])
    return lib.MathUtility.cagr(endingValue, startingValue, numberOfYears)
}

def getCAGR(List totalYearlyRevenue) {
    int size = totalYearlyRevenue.size()
    int i = size - 1
    def startingValue = 0.0 as BigDecimal, endingValue = 0.0 as BigDecimal
    int numberOfYears = 0
    while (i > 0) {
        if (!startingValue && totalYearlyRevenue[i] != 0.0) {
            startingValue = totalYearlyRevenue[i]
            endingValue = totalYearlyRevenue[i]
            numberOfYears++
        } else {
            if (totalYearlyRevenue[i] != 0.0) {
                endingValue = totalYearlyRevenue[i]
                numberOfYears++
            }
        }
        i--
    }
    lib.TraceUtility.developmentTraceRow(" The CAGR Comoputations ", [endingValue, startingValue, numberOfYears])
    return lib.MathUtility.cagr(endingValue, startingValue, numberOfYears)
}

def getSHTotalRevenueYearly() {
    return api.global.yearlySHRevenue
}

def getTHTotalRevenueYearly() {
    return api.global.yearlyTHRevenue
}

def getRevenuePercentageContribution(def customerContribution, def totalRevenue, boolean isSH = true) {
    lib.TraceUtility.developmentTrace("Printing Input1", customerContribution)
    lib.TraceUtility.developmentTrace("Printing Input2", totalRevenue)
    return lib.MathUtility.percentage(((totalRevenue) ? totalRevenue['YearlyRevenue'] : null), customerContribution)
}

def getMarginPercentageContribution(def customerContribution, def totalMargin, boolean isSH = true) {
    lib.TraceUtility.developmentTrace("Printing Input3", customerContribution)
    lib.TraceUtility.developmentTrace("Printing Input4", totalMargin)
    return lib.MathUtility.percentage(((totalMargin) ? totalMargin['YearlyMargin'] : null), customerContribution)
}

def getAvg(List revPerYear) {
    long countZeroEntries = revPerYear.count(0.0)
    int totalEntries = revPerYear.size()
    if (countZeroEntries == totalEntries || (countZeroEntries == totalEntries - 1 && revPerYear[0] != 0.0))
        return 0.0

    int currentYearCount = (revPerYear[0] == 0.0) ? 0 : 1
    long countNonZero = revPerYear.size() - (currentYearCount + countZeroEntries)
    def avg = lib.MathUtility.divide((revPerYear[1] + revPerYear[2] + revPerYear[3] + revPerYear[4]), countNonZero)
    return avg
}

//Find the data for which we have transactions or values.
def getCustomerAge(List yearlyRevenue) {
    int age = yearlyRevenue.size() - 1
    while (age >= 0 && yearlyRevenue[age] == 0.0) {
        age--
    }
    return (age + 1)
}

def getTransactionsCountList(List listOfTransactionData) {
    def transactionsCountDetails = []
    listOfTransactionData.each { it ->
        transactionsCountDetails.add(it['t2tc'])
    }
    return transactionsCountDetails
}

def sortRecordsAccordingToYears(List years, records) {
    def sortedData = []
    int i = 0
    def currentRecord
    while (i < years.size()) {
        currentRecord = records.find { Integer.parseInt((it.t1y ?: it.t2y)) == years[i] }
        if (currentRecord)
            sortedData.add(currentRecord)
        else {
            sortedData.add(['t1cid': out.CurrentCustomerToProcess, 't1y': "${years[i]}", 't1yr': 0.0, 't1ym': 0.0, 't1yr': 0,
                            't2cid': out.CurrentCustomerToProcess, 't2y': "${years[i]}", 't2yr': 0.0, 't2ym': 0.0, 't2yr': 0, 't2tc':0])
        }
        i++
    }
    lib.TraceUtility.developmentTraceRow("XYZ", sortedData)
    return sortedData
}