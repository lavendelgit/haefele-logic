getYearlyDetailsFromTransactionHistory()

def getYearlyDetailsFromTransactionHistory() {
    if (!api.global.yearlyTHRevenue) {
        def field = ['InvoiceDateYear'  : 'Year',
                     'SUM(p3_Revenue)'  : 'YearlyRevenue',
                     'SUM(p4_MarginAbs)': 'YearlyMargin',
                     'SUM(p6_Material)' : 'TransactionCount']
        def filters = []
        def data = lib.DBQueries.getDataFromSource(field, 'DL_CustomersWithTransactions', filters, 'Rollup')
        api.global.yearlyTHRevenue = data.collectEntries { it ->
            [(it.Year): (it)]
        }
        lib.TraceUtility.developmentTrace("query", api.global.yearlyTHRevenue)
    }

    return api.global.yearlyTHRevenue
}