// Assuming that feeder is going to give you customers, write aggregate query for a data source
//Construct the details for Query from SalesHistory

//TODO: Think of extracting getCorrectValue (),  aggregateValues() and aggregateRecords() to be extracted to DBUtility.
def getCorrectValue(def currentValue, value1) {
    def output = null
    if (!currentValue) {
        output = (value1) ?: 0.0
    } else {
        output = currentValue + ((value1) ?: 0.0)
    }
    return output
}

//t1cid t1y t1yr t1ym t2cid t2y t2yr t2ym t2tc
def aggregateValues(def Values) {
    def transRec = [:]
    boolean isFirstTime = true
    Values.each { it ->
        if (isFirstTime) {
            transRec['t1cid'] = it['t1cid']
            transRec['t1y'] = it['t1y']
            transRec['t2cid'] = it['t2cid']
            transRec['t2y'] = it['t2y']
        }
        transRec['t1yr'] = getCorrectValue(isFirstTime ? null : transRec['t1yr'], it['t1yr'])
        transRec['t1ym'] = getCorrectValue(isFirstTime ? null : transRec['t1ym'], it['t1ym'])
        transRec['t2yr'] = getCorrectValue(isFirstTime ? null : transRec['t2yr'], it['t2yr'])
        transRec['t2ym'] = getCorrectValue(isFirstTime ? null : transRec['t2ym'], it['t2ym'])
        transRec['t2tc'] = getCorrectValue(isFirstTime ? null : transRec['t2tc'], it['t2tc'])
        isFirstTime = false
    }
    return transRec
}

def aggregateRecords(List joinedSummaryData) {
    def mapOfValues = joinedSummaryData.groupBy({ transaction -> transaction.t1y })
    lib.TraceUtility.developmentTraceRow ("Printing the grouped records", mapOfValues)
    def returnList = []
    def currentValue
    mapOfValues.each { key, Values ->
        currentValue = aggregateValues(Values)
        returnList.add(currentValue)
    }
    return returnList
}

def shFields = ['CustomerId'                     : 'CustomerId',
                'YearMonthYear'                  : 'Year',
                'SUM(p1_Revenue)'                : 'YearlyRevenue',
                'SUM(p2_MarginalContributionAbs)': 'YearlyMargin'
]
def shFilters = [
        Filter.equal("CustomerId", out.CurrentCustomerToProcess)
]
//Construct the details for Query from TrasactionHistory
def thFields = ['CustomerId'       : 'CustomerId',
                'InvoiceDateYear'  : 'Year',
                'SUM(p3_Revenue)'  : 'YearlyRevenue',
                'SUM(p4_MarginAbs)': 'YearlyMargin',
                'SUM(p6_Material)' : 'TransactionCounts'
]
def thFilters = [
        Filter.equal("CustomerId", out.CurrentCustomerToProcess)
]
def joinSQL = getInnerJoinForSummaryNDetailTransRec()
def joinSources = [
        ['Source': 'DL_CustomerRevenueUsingSalesHistory', 'SourceType': 'Rollup', 'Fields': shFields, 'Filters': shFilters, 'OrderBy': 'Year'],
        ['Source': 'DL_CustomersWithTransactions', 'SourceType': 'Rollup', 'Fields': thFields, 'Filters': thFilters, 'OrderBy': 'Year'],
]
def toBeAggregatedRecords = lib.DBQueries.executeJoinQueryToFetchResult(joinSources, joinSQL)
return aggregateRecords(toBeAggregatedRecords)

def getInnerJoinForSummaryNDetailTransRec() {
    StringBuilder builder = new StringBuilder('SELECT ')
    builder.append('T1.CustomerId as t1cid, T1.Year as t1y, IsNull(T1.YearlyRevenue, 0.0) as t1yr, IsNull(T1.YearlyMargin, 0.0) as t1ym, ')
    builder.append('IsNull(T2.CustomerId, T1.CustomerId) as t2cid, IsNull(T2.Year, T1.Year) as t2y, IsNull(T2.YearlyRevenue, 0.0) as t2yr, ')
    builder.append('IsNull(T2.YearlyMargin, 0.0) as t2ym, IsNull(T2.TransactionCounts,0) as t2tc ')
    builder.append('FROM T1 LEFT OUTER JOIN T2 ON T1.CustomerId = T2.CustomerId AND T1.Year = T2.Year ')
    builder.append('UNION ')
    builder.append('SELECT ')
    builder.append('IsNull(T1.CustomerId, T2.CustomerId) as t1cid, IsNull(T1.Year, T2.Year) as t1y, IsNull(T1.YearlyRevenue, 0.0) as t1yr, ' +
            'IsNull(T1.YearlyMargin, 0.0) as t1ym, ')
    builder.append('T2.CustomerId as t2cid, T2.Year as t2y, IsNull(T2.YearlyRevenue,0.0) as t2yr, IsNull(T2.YearlyMargin,0.0) as t2ym, T2.TransactionCounts as t2tc ')
    builder.append('FROM T1 RIGHT OUTER JOIN T2 ON T1.CustomerId = T2.CustomerId AND T1.Year = T2.Year')
    return builder.toString()
}