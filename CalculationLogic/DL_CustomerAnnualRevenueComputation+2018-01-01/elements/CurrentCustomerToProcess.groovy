def inputCustomerId = api.currentItem()
if (!inputCustomerId && api.global.isDevelopment) {
    inputCustomerId = ["customerId" : "0001111606"]
}
inputCustomerId = (inputCustomerId) ? inputCustomerId["customerId"] : ""
return inputCustomerId