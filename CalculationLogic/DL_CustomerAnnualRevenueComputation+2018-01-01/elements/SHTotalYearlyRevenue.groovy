getYearlyDetailsFromSalesHistory()

def getYearlyDetailsFromSalesHistory() {
    if (!api.global.yearlySHRevenue) {
        def field = ['YearMonthYear'                  : 'Year',
                     'SUM(p1_Revenue)'                : 'YearlyRevenue',
                     'SUM(p2_MarginalContributionAbs)': 'YearlyMargin']
        def filters = []
        def data = lib.DBQueries.getDataFromSource(field, 'DL_CustomerRevenueUsingSalesHistory', filters, 'Rollup')
        api.global.yearlySHRevenue = data.collectEntries { it ->
            [(it.Year): (it)]
        }
        api.trace("query", "row " + api.global.yearlySHRevenue)
    }

    return api.global.yearlySHRevenue
}