return Constants.DATA_COLUMNS_DEF.values().findAll { Map columnDef ->
    !columnDef.isCalculated
}