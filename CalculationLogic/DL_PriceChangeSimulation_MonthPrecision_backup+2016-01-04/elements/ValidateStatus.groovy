if (out.AssociatedSimulationDetails.attributeExtension___status != 'Scheduled') {
    api.abortCalculation()

    return false
}

return true
