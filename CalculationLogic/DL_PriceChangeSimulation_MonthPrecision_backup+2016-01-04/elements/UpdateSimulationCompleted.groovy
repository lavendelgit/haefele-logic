import org.pojava.datetime.DateTime

String status = out.PersistRecords ? 'Ready' : 'Error'

libs.HaefeleCommon.Simulation.callBoundCallUtilFunction(out.SimulationName,
        status,
        out.SimulationOwner,
		out.SimulationTitle,
        out.AssociatedSimulationDetails.attributeExtension___simulation_inputs)



