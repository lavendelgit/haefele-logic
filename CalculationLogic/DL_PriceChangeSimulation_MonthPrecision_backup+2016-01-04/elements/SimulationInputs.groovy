List simulationInputs = out.MonthlyPricingSimulationInputs?.PricingMatrix.findAll { it -> it["Pricing Inputs For"] == "Overall Simulation" }
Map grossPriceChangePercentage = [:]
Map purchasePriceChangePercentage = [:]
Map quantityChangePercentage = [:]

Map simulationInputsDetails = [:]
BigDecimal grossPrice
BigDecimal purchasePrice
BigDecimal quantity

Constants.MONTH_NAMES.each { monthName ->
    Map simulationInputRow = simulationInputs.find { it -> it.Month == monthName }
    grossPrice = simulationInputRow ? simulationInputRow["Gross Price % Change"] as BigDecimal : BigDecimal.ZERO
    purchasePrice = simulationInputRow ? simulationInputRow["Purchase Price % Change"] as BigDecimal : BigDecimal.ZERO
    quantity = simulationInputRow ? simulationInputRow["Quantity % Change"] as BigDecimal : BigDecimal.ZERO

    grossPriceChangePercentage.put(monthName,  grossPrice/100)
    purchasePriceChangePercentage.put(monthName, purchasePrice/100 )
    quantityChangePercentage.put(monthName, quantity/100 )

}
simulationInputsDetails.put("grossPriceChangePercentage", grossPriceChangePercentage)
simulationInputsDetails.put("purchasePriceChangePercentage", purchasePriceChangePercentage)
simulationInputsDetails.put("quantityPriceChangePercentage", quantityChangePercentage)

return simulationInputsDetails