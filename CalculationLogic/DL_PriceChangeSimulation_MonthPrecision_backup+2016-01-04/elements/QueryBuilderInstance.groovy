List exceptions = out.Exceptions ?: []
List defExceptions = out.PredeterminedExceptions
exceptions.addAll(defExceptions)
Map queryBuilder = queryBuilder().setDataColumns(Constants.DATA_COLUMNS_DEF)
                                 .setGrossPriceChangePercent(out.GrossPriceChangePercent)
                                 .setLandingPriceChangePercent(out.LandingPriceChangePercent)
                                 .setQuantityChangePercent(out.QuantityChangePercent)
                                 .setExceptions(exceptions)

return queryBuilder

protected Map queryBuilder() {
    Map level1 = Constants.LEVEL_1_ALIAS
    Map level2 = Constants.LEVEL_2_ALIAS
    Map level3 = Constants.LEVEL_3_ALIAS
    Map level4 = Constants.LEVEL_4_ALIAS
    Map level5 = Constants.LEVEL_5_FIELD
    Map functions = Constants.FUNCTIONS
    Map subTable = Constants.SUB_TABLE
    Map pricingTypesConst = Constants.PRICING_TYPES_CONST
    Map pricingTypes = Constants.PRICING_TYPES
    Map calculations = Constants.CALCULATIONS
    Map methods = [:]
    Map definition = [exceptions: []]

    methods += [setDataColumns                   : { Map dataColumns ->
        definition.dataColumns = dataColumns
        return methods
    },
                setGrossPriceChangePercent       : { Map changePercent ->
                    definition.grossPriceChangePercent = changePercent
                    definition.isGrossPriceChange = Library.doesNonZeroValueExists(changePercent)
                    return methods
                },
                setLandingPriceChangePercent     : { Map changePercent ->
                    definition.landingPriceChangePercent = changePercent
                    definition.isLandingPriceChange = Library.doesNonZeroValueExists(changePercent)
                    return methods
                },
                setQuantityChangePercent         : { Map changePercent ->
                    definition.quantityChangePercent = changePercent
                    definition.isQuantityChange = Library.doesNonZeroValueExists(changePercent)
                    return methods
                },
                setExceptions                    : { List exceptions ->
                    definition.exceptions = exceptions
                    return methods
                },
                getPriceChangeFragment           : { String builderContext ->
                    String queryFragment = ""
                    switch (builderContext) {
                        case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                            queryFragment = """ 
                                        ${FormulaeUtils.getMonthlyFieldForCompleteYear(level3.EXPECTED_BASE_COST, level2.EXPECTED_BASE_COST, functions.AVERAGE)}
                                        ${FormulaeUtils.getMonthlyFieldForCompleteYear(level3.EXPECTED_QUANTITY, level2.EXPECTED_QUANTITY_SOLD, functions.AVERAGE)}
                                        ${FormulaeUtils.getMonthlyMultiplyTwoFields(level3.EXPECTED_BASE_COST, level3.QUANTITY_SOLD, level2.EXPECTED_TOTAL_BASE_COST, functions.AVERAGE)}
                                        ${FormulaeUtils.getMonthlyMultiplyTwoFields(level3.EXPECTED_POCKET_PRICE, level3.EXPECTED_QUANTITY, level2.EXPECTED_REVENUE, functions.AVERAGE)}
                                        ${FormulaeUtils.getLevel2ExpectedMargin(level3.EXPECTED_GROSS_PRICE, level3.EXPECTED_QUANTITY, level3.EXPECTED_BASE_COST, level3.QUANTITY_SOLD, level2.TOTAL_EXPECTED_MARGIN, functions.AVERAGE)}
                                        ${FormulaeUtils.getFieldDifference(level3.EXPECTED_POCKET_PRICE, level3.EXPECTED_BASE_COST, level2.EXPECTED_POCKET_MARGIN, functions.AVERAGE)}
                                        ${FormulaeUtils.getLevel2ExpectedMargin(level3.EXPECTED_POCKET_PRICE, level3.EXPECTED_QUANTITY, level3.EXPECTED_BASE_COST, level3.QUANTITY_SOLD, level2.EXPECTED_TOTAL_MARGIN, functions.AVERAGE)}
                                        ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.MARKUP, pricingTypes.MARKUP, calculations.EXPECTED)}
                                        ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.NET_PRICE, pricingTypes.NET_PRICE, calculations.EXPECTED, true)}
                                        ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.PER_DISCOUNT, pricingTypes.PER_DISCOUNT, calculations.EXPECTED)}
                                        ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.ABSOULTE_DISCOUNT, pricingTypes.ABSOULTE_DISCOUNT, calculations.EXPECTED)}
                                        ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.X_ARTICLE, pricingTypes.X_ARTICLE, calculations.EXPECTED, true)}
                                        ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.INTERNAL, pricingTypes.INTERNAL, calculations.EXPECTED, true)}
                                        ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.NO_DISCOUNT, pricingTypes.NO_DISCOUNT, calculations.EXPECTED)}"""
                            break
                        case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                            queryFragment = """
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.EXPECTED_REVENUE, level1.EXPECTED_REVENUE, functions.SUM, true, false, true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.EXPECTED_BASE_COST, level1.EXPECTED_BASE_COST, functions.AVERAGE, false, false, true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.EXPECTED_POCKET_MARGIN, level1.EXPECTED_POCKET_MARGIN, functions.AVERAGE, false, false, true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.EXPECTED_QUANTITY_SOLD, level1.EXPECTED_QUANTITY_SOLD, functions.SUM, false, false, true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.EXPECTED_TOTAL_MARGIN, level1.EXPECTED_TOTAL_MARGIN, functions.SUM, false, false, true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.EXPECTED_TOTAL_BASE_COST, level1.EXPECTED_TOTAL_BASE_COST, functions.SUM, false, false, true)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.MARKUP, calculations.EXPECTED)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.NET_PRICE, calculations.EXPECTED)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.PER_DISCOUNT, calculations.EXPECTED)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.ABSOULTE_DISCOUNT, calculations.EXPECTED)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.X_ARTICLE, calculations.EXPECTED)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.INTERNAL, calculations.EXPECTED)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.NO_DISCOUNT, calculations.EXPECTED)}"""
                    }
                    return queryFragment
                },
                getMarginChangeFragment          : { String builderContext -> return ""
                },
                getLevel5Part2Details            : {
                    Map exceptionConfigurationFields = Constants.MERGED_EXCEPTION_CONFIGURATION
                    return """ 
                                                                SELECT 
                                                                ${FormulaeUtils.getSimpleKeyValue(exceptionConfigurationFields.EXCEPTION_MATERIAL, exceptionConfigurationFields.EXCEPTION_MATERIAL, true)}
                                                                ${
                        FormulaeUtils.getMonthlyFieldForCompleteYear(exceptionConfigurationFields.EXCEPTION_GROSS_PRICE_CHANGE_PERCENT,
                                                                     exceptionConfigurationFields.EXCEPTION_GROSS_PRICE_CHANGE_PERCENT,
                                                                     functions.MAX)}
                                                                ${
                        FormulaeUtils.getMonthlyFieldForCompleteYear(exceptionConfigurationFields.EXCEPTION_PURCHASE_PRICE_CHANGE_PERCENT,
                                                                     exceptionConfigurationFields.EXCEPTION_PURCHASE_PRICE_CHANGE_PERCENT,
                                                                     functions.MAX)}
                                                                ${
                        FormulaeUtils.getMonthlyFieldForCompleteYear(exceptionConfigurationFields.EXCEPTION_QUANTITY_CHANGE_PERCENT,
                                                                     exceptionConfigurationFields.EXCEPTION_QUANTITY_CHANGE_PERCENT,
                                                                     functions.MAX)}
                                                                FROM ( 
                                                                    ${FormulaeUtils.getExceptionUnions(definition.exceptions)}
                                                                ) ${subTable.ALL_EXCEPTION_ROWS} GROUP BY ${exceptionConfigurationFields.EXCEPTION_MATERIAL}
                        """
                },
                getLevel4Details                 : {
                    Map exceptionConfigurationFields = Constants.MERGED_EXCEPTION_CONFIGURATION
                    Map grossPriceChangePercent = definition.grossPriceChangePercent
                    Map landingPriceChangePercent = definition.landingPriceChangePercent
                    Map quantityChangePercent = definition.quantityChangePercent
                    String level5Part2Details = methods.getLevel5Part2Details()
                    return """         SELECT
                                                                ${FormulaeUtils.getSimpleKeyValue(level5.MATERIAL, level4.MATERIAL, true)}
                                                                ${FormulaeUtils.getSimpleKeyValue(level5.SECONDARY_KEY, level4.SECONDARY_KEY)}
                                                                ${FormulaeUtils.getSimpleKeyValue(level5.CUSTOMER_ID, level4.CUSTOMER_ID)}
                                                                ${FormulaeUtils.getSimpleKeyValue(level5.CONDITION_NAME, level4.CONDITION_NAME)}
                                                                ${FormulaeUtils.getSimpleKeyValue(level5.PRICING_TYPE, level4.PRICING_TYPE)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.TOTAL_REVENUE, level4.ACTUAL_REVENUE)}
                                                                ${FormulaeUtils.getKeyConstructedUsingDelimeter([level5.MATERIAL, level5.CUSTOMER_ID, level5.YEAR], level4.TRANSACTION_KEY)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.TOTAL_REVENUE, level4.TOTAL_REVENUE)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.QUANTITY_SOLD, level4.QUANTITY_SOLD)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.GROSS_PRICE, level4.GROSS_PRICE)}
                                                                ${FormulaeUtils.getSimpleKeyValue(level5.ZPL, level4.ZPL)}
                                                                ${FormulaeUtils.getSimpleKeyValue(level5.DISCOUNT, level4.DISCOUNT)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.BASE_COST, level4.BASE_COST)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.MARGIN, level4.MARGIN)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.POCKET_PRICE, level4.POCKET_PRICE)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.GROSS_PRICE_CHANGE_PERCENT, level4.GROSS_PRICE_CHANGE_PERCENT)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.LANDING_PRICE_CHANGE_PERCENT, level4.LANDING_PRICE_CHANGE_PERCENT)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.QUANTITY_CHANGE_PERCENT, level4.QUANTITY_CHANGE_PERCENT)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.EXPECTED_BASE_COST, level4.EXPECTED_BASE_COST)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.EXPECTED_QUANTITY, level4.EXPECTED_QUANTITY)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.TEMP_GROSS_PRICE, level4.TEMP_GROSS_PRICE)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.HISTORICAL_POCKET_PRICE, level4.HISTORICAL_POCKET_PRICE)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.TEMP_SPECIAL_GROSS_PRICE, level4.TEMP_SPECIAL_GROSS_PRICE)}
                                                                ${FormulaeUtils.getMonthlyFieldForCompleteYear(level5.TEMP_POCKET_PRICE, level4.TEMP_POCKET_PRICE)}
                                                            FROM T1
                                                            LEFT JOIN (
                                                                 $level5Part2Details
                                                           ) ${subTable.MERGED_EXCEPTION_CONFIGURATION}
                                                            ON T1.${level5.MATERIAL} = ${subTable.MERGED_EXCEPTION_CONFIGURATION}.${exceptionConfigurationFields.EXCEPTION_MATERIAL}
                                                            JOIN LATERAL
                                                            (
                                                                SELECT
                                                                    ${FormulaeUtils.getEffectivePercentageValuesPerMonth(grossPriceChangePercent, subTable.MERGED_EXCEPTION_CONFIGURATION, exceptionConfigurationFields.EXCEPTION_GROSS_PRICE_CHANGE_PERCENT, level5.GROSS_PRICE_CHANGE_PERCENT, true)}
                                                                    ${FormulaeUtils.getEffectivePercentageValuesPerMonth(landingPriceChangePercent, subTable.MERGED_EXCEPTION_CONFIGURATION, exceptionConfigurationFields.EXCEPTION_PURCHASE_PRICE_CHANGE_PERCENT, level5.LANDING_PRICE_CHANGE_PERCENT)}
                                                                    ${FormulaeUtils.getEffectivePercentageValuesPerMonth(quantityChangePercent, subTable.MERGED_EXCEPTION_CONFIGURATION, exceptionConfigurationFields.EXCEPTION_QUANTITY_CHANGE_PERCENT, level5.QUANTITY_CHANGE_PERCENT)}
                                                            ) ${subTable.REVENUE_AND_QUANTITY} 
                                                            JOIN LATERAL
                                                            (
                                                                SELECT
                                                                ${FormulaeUtils.getExpectedkPerMonthlyChangedValue(level5.GROSS_PRICE, level5.GROSS_PRICE_CHANGE_PERCENT, level5.TEMP_GROSS_PRICE, true)}
                                                                ${FormulaeUtils.getExpectedkPerMonthlyChangedValue(level5.BASE_COST, level5.LANDING_PRICE_CHANGE_PERCENT, level5.EXPECTED_BASE_COST)}
                                                                ${FormulaeUtils.getExpectedkPerMonthlyChangedValue(level5.QUANTITY_SOLD, level5.QUANTITY_CHANGE_PERCENT, level5.EXPECTED_QUANTITY)}
                                                                ${FormulaeUtils.getDivisionResponse(level5.TOTAL_REVENUE, level5.QUANTITY_SOLD, level5.HISTORICAL_POCKET_PRICE, true)}
                                                            ) ${subTable.EXPECTED_TABLE_CALCULATIONS}   
                                                            JOIN LATERAL 
                                                            (   SELECT 
                                                                ${FormulaeUtils.getMonthlySpecialPricesForLevel5()}
                                                            ) ${subTable.TABLE_SPECIAL}
                                                            JOIN LATERAL 
                                                            (
                                                                SELECT
                                                                ${FormulaeUtils.getMonthlyPocketPriceForLevel5()}                                                                                                                                   
                                                            )  ${subTable.TABLE_POCKET_PRICE} """
                },
                getCalculatePerRowSQLQuery       : {
                    String level5Query = methods.getLevel4Details()
                    definition.calculatePerRowSQLQuery = """
                                                SELECT
                                                        *
                                                        ${FormulaeUtils.getMonthlyMultiplyTwoFields(level4.HISTORY_POCKET_PRICE, level4.CHANGE_RATIO, level3.EXPECTED_POCKET_PRICE)}
                                                        ${FormulaeUtils.getLevel3PricingTypeCalculations(pricingTypesConst.MARKUP)}
                                                        ${FormulaeUtils.getLevel3PricingTypeCalculations(pricingTypesConst.NET_PRICE)}
                                                        ${FormulaeUtils.getLevel3PricingTypeCalculations(pricingTypesConst.PER_DISCOUNT)}
                                                        ${FormulaeUtils.getLevel3PricingTypeCalculations(pricingTypesConst.ABSOULTE_DISCOUNT)}
                                                        ${FormulaeUtils.getLevel3PricingTypeCalculations(pricingTypesConst.X_ARTICLE)}
                                                        ${FormulaeUtils.getLevel3PricingTypeCalculations(pricingTypesConst.INTERNAL)}
                                                        ${FormulaeUtils.getLevel3PricingTypeCalculations(pricingTypesConst.NO_DISCOUNT)}
                                                        ${FormulaeUtils.getMonthlyMultiplyTwoFields(level4.TEMP_GROSS_PRICE, level4.CHANGE_RATIO, level3.EXPECTED_GROSS_PRICE)}
                                                        ${FormulaeUtils.getMonthlyFieldForCompleteYear(level4.HISTORICAL_POCKET_MARGIN, level3.POCKET_MARGIN, '')}
                                                        ${FormulaeUtils.getMonthlyPercentages(level4.HISTORICAL_POCKET_MARGIN, level4.POCKET_PRICE, level3.POCKET_MARGIN_PERCENT)}
                                                FROM (
                                                         $level5Query
                                                         )
                                                        JOIN LATERAL
                                                        (
                                                                SELECT
                                                                ${FormulaeUtils.getMonthlyAbsoluteDivisionResponse(level4.TEMP_POCKET_PRICE, level4.POCKET_PRICE, level3.CHANGE_RATIO, true)}
                                                                ${FormulaeUtils.getDivisionResponse(level4.TOTAL_REVENUE, level4.QUANTITY_SOLD, level3.HISTORY_POCKET_PRICE, true)}
                                                                ${FormulaeUtils.getMonthlyHistoryPocketMargin(level4.TOTAL_REVENUE, level4.QUANTITY_SOLD, level4.BASE_COST, level3.HISTORY_POCKET_MARGIN)}
                                                        ) ${subTable.TABLE_RATIO}
                                                        JOIN LATERAL
                                                        (
                                                              SELECT
                                                                ${FormulaeUtils.getPricingTypeLevel4RevenueContribution(pricingTypesConst.MARKUP, pricingTypes.MARKUP, level3.HISTORY_POCKET_PRICE, true)}
                                                                ${FormulaeUtils.getPricingTypeLevel4RevenueContribution(pricingTypesConst.NET_PRICE, pricingTypes.NET_PRICE, level3.HISTORY_POCKET_PRICE)}
                                                                ${FormulaeUtils.getPricingTypeLevel4RevenueContribution(pricingTypesConst.PER_DISCOUNT, pricingTypes.PER_DISCOUNT, level3.HISTORY_POCKET_PRICE)}
                                                                ${FormulaeUtils.getPricingTypeLevel4RevenueContribution(pricingTypesConst.ABSOULTE_DISCOUNT, pricingTypes.ABSOULTE_DISCOUNT, level3.HISTORY_POCKET_PRICE)}
                                                                ${FormulaeUtils.getPricingTypeLevel4RevenueContribution(pricingTypesConst.X_ARTICLE, pricingTypes.X_ARTICLE, level3.HISTORY_POCKET_PRICE)}
                                                                ${FormulaeUtils.getPricingTypeLevel4RevenueContribution(pricingTypesConst.INTERNAL, pricingTypes.INTERNAL, level3.HISTORY_POCKET_PRICE)}
                                                                ${FormulaeUtils.getPricingTypeLevel4RevenueContribution(pricingTypesConst.NO_DISCOUNT, pricingTypes.NO_DISCOUNT, level3.HISTORY_POCKET_PRICE)}
                                                        )  ${subTable.TABLE_BREAKUP}                                               """
                },
                getAggregateByMaterialTxnSQLQuery: {
                    String priceChangeQueryFragment = methods.getPriceChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL2)
                    String marginChangeQueryFragment = methods.getMarginChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL2)
                    definition.aggregateByMaterialTxnSQLQuery = """ 
                                    SELECT 
                                       ${FormulaeUtils.getSimpleKeyValue(level3.MATERIAL, level2.MATERIAL, true)}
                                       ${FormulaeUtils.getSimpleKeyValue(level3.TRANSACTION_KEY, level2.TRANSACTION_KEY)}
                                       ${FormulaeUtils.getSimpleFunctionValue(level3.TRANSACTION_KEY, level2.TRANSACTION_COUNT, functions.COUNT)}
                                       ${FormulaeUtils.getMonthlyMultiplyTwoFields(level3.BASE_COST, level3.QUANTITY_SOLD, level2.TOTAL_BASE_COST, functions.AVERAGE)}
                                       ${FormulaeUtils.getMonthlyMultiplyTwoFields(level3.POCKET_PRICE, level3.QUANTITY_SOLD, level2.CURRENT_REVENUE, functions.AVERAGE)}
                                       ${FormulaeUtils.getMonthlyMultiplyTwoFields(level3.MARGIN, level3.QUANTITY_SOLD, level2.CURRENT_MARGIN, functions.AVERAGE)}
                                       ${FormulaeUtils.getMonthlyFieldForCompleteYear(level3.QUANTITY_SOLD, level2.TOTAL_QUANTITY_SOLD, functions.AVERAGE)}
                                       ${FormulaeUtils.getMonthlyFieldForCompleteYear(level3.TOTAL_REVENUE, level2.TOTAL_REVENUE, functions.AVERAGE)}
                                       ${FormulaeUtils.getMonthlyMultiplyTwoFields(level3.POCKET_MARGIN, level3.QUANTITY_SOLD, level2.CURRENT_POCKET_MARGIN, functions.AVERAGE)}
                                       ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.MARKUP, pricingTypes.MARKUP, calculations.CURRENT)}
                                       ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.NET_PRICE, pricingTypes.NET_PRICE, calculations.CURRENT, true)}
                                       ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.PER_DISCOUNT, pricingTypes.PER_DISCOUNT, calculations.CURRENT)}
                                       ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.ABSOULTE_DISCOUNT, pricingTypes.ABSOULTE_DISCOUNT, calculations.CURRENT)}
                                       ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.X_ARTICLE, pricingTypes.X_ARTICLE, calculations.CURRENT, true)}
                                       ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.INTERNAL, pricingTypes.INTERNAL, calculations.CURRENT, true)}
                                       ${FormulaeUtils.getLevel2PricingTypeComputationsMonthly(pricingTypesConst.NO_DISCOUNT, pricingTypes.NO_DISCOUNT, calculations.CURRENT)}                                        
                                       ${priceChangeQueryFragment}
                                       ${marginChangeQueryFragment}
                                    FROM ( ${methods.getCalculatePerRowSQLQuery()} )  LEVEL2 GROUP BY ${level3.MATERIAL} ,${level3.TRANSACTION_KEY}
                        """
                    return definition.aggregateByMaterialTxnSQLQuery
                },
                getSQLQuery                      : {
                    String priceChangeQueryFragement = methods.getPriceChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL1)
                    String marginChangeQueryFragment = methods.getMarginChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL1)
                    return """ 
                                SELECT 
                                    ${FormulaeUtils.getSimpleKeyValue(level2.MATERIAL, level1.MATERIAL, true, '', true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.TOTAL_QUANTITY_SOLD, level1.TOTAL_QUANTITY_SOLD, functions.SUM, false, false, true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.TOTAL_REVENUE, level1.TOTAL_REVENUE, functions.SUM, true, false, true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.CURRENT_POCKET_MARGIN, level1.CURRENT_POCKET_MARGIN, functions.SUM, false, false, true)}
                                    ${FormulaeUtils.getMonthlyFieldForCompleteYear(level2.TOTAL_BASE_COST, level1.TOTAL_BASE_COST, functions.SUM, false, false, true)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.MARKUP, calculations.CURRENT)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.NET_PRICE, calculations.CURRENT)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.PER_DISCOUNT, calculations.CURRENT)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.ABSOULTE_DISCOUNT, calculations.CURRENT)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.X_ARTICLE, calculations.CURRENT)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.INTERNAL, calculations.CURRENT)}
                                    ${FormulaeUtils.getLevel1PricingTypeCalculations(pricingTypesConst.NO_DISCOUNT, calculations.CURRENT)}
                                    ${priceChangeQueryFragement}    
                                    ${marginChangeQueryFragment}                                     
                                FROM 
                                 (  
                                    ${methods.getAggregateByMaterialTxnSQLQuery()}
                                 )  LEVEL1 GROUP BY ${level2.MATERIAL}
                   """
                }]
    return methods
}
