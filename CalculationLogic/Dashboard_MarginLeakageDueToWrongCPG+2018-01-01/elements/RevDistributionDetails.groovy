def chart = api.newChartBuilder()
        .newDataTable()
        .getOptions()
        .setInitialDataGrouping(false)
        .setDisableDrilldown(false)
        .back()
        .addSeries()
        .setLabel('Revenue gain post correction of "Wrong" categorization')
        .setHideDataLabels(true)
        .setDatamart('124.DMR')//46.DMR
        .setCurrency('EUR')
        .addFilters(FilterOperator.AND)
        .addFilter('CCYear', FilterOperator.EQUAL, out.InputDate)
        .back()
        .addDimFilter('CorrectionType', 'Wrong')
        .addGroupBy('CCVerkaufsbereich3')
        .addGroupBy('CBranchCategory')
        .addAdditionalMeasure()
        .setMeasure('p1_CustomerId')
        .withTotal()
        .back()
        .addAdditionalMeasure()
        .setMeasure('p2_CCUnitsPurchased')
        .withTotal()
        .back()
        .addAdditionalMeasure()
        .setMeasure('p4_SubtotalUmsatz')
        .withTotal()
        .back()
        .addAdditionalMeasure()
        .setMeasure('p6_PotentialRevenue')
        .withTotal()
        .back()
        .addAdditionalMeasure()
        .setMeasure('p8_PotentialUmsatzGain')
        .withTotal()
        .back()
        .addAdditionalMeasure()
        .setMeasure('p8_PotentialUmsatzGain')
        .withAggregation('CONTRIB')
        .back()
        .addAdditionalMeasure()
        .setMeasure('p5_SubtotalMargin')
        .withTotal()
        .back()
        .addAdditionalMeasure()
        .setMeasure('p7_PotentialMargin')
        .withTotal()
        .back()
        .addAdditionalMeasure()
        .setMeasure('p9_PotentialMarginGain')
        .withTotal()
        .back()
        .addAdditionalMeasure()
        .setMeasure('p9_PotentialMarginGain')
        .withAggregation('CONTRIB')
        .back()
        //.setGeneratedQueryDto('{"datamart":"46.DMR","source":"46.DMR","name":null,"label":"Revenue gain post correction of \"Wrong\" categorization","rollup":true,' +
        .setGeneratedQueryDto('{"datamart":"124.DMR","source":"124.DMR","name":null,"label":"Revenue gain post correction of \"Wrong\" categorization","rollup":true,' +
                '"projections":{"CCVerkaufsbereich3":{"alias":"CCVerkaufsbereich3","label":"Verkaufsbüro","expression":"CCVerkaufsbereich3"},' +
                '"CBranchCategory":{"alias":"CBranchCategory","label":"Branch Category","expression":"CBranchCategory"},' +
                '"m9":{"alias":"m9","label":"∑Customer Id","expression":"SUM({field})",' +
                '   "name":"p1_CustomerId","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p1_CustomerId"}},' +
                '"m1":{"alias":"m1","label":"∑Menge","expression":"SUM({field})",' +
                '   "name":"p2_CCUnitsPurchased","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p2_CCUnitsPurchased"}},' +
                '"m2":{"alias":"m2","label":"∑Subtotal Umsatz","expression":"SUM({field})",' +
                '   "name":"p4_SubtotalUmsatz","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p4_SubtotalUmsatz"}},' +
                '"m3":{"alias":"m3","label":"∑Potential Revenue","expression":"SUM({field})",' +
                '   "name":"p6_PotentialRevenue","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p6_PotentialRevenue"}},' +
                '"m10":{"alias":"m10","label":"∑Potential Umsatz Gain","expression":"SUM({field})",' +
                '   "name":"p8_PotentialUmsatzGain","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p8_PotentialUmsatzGain"}},' +
                '"m8":{"alias":"m8","label":"Contribution(Potential Umsatz Gain)","expression":"SUM({field})",' +
                '       "name":"p8_PotentialUmsatzGain","advancedProjection":true,"function":"CONTRIB","default":null,"formatString":"Contribution({field})","parameters":{"field":"p8_PotentialUmsatzGain"}},' +
                '"m4":{"alias":"m4","label":"∑Subtotal Margin","expression":"SUM({field})",' +
                '   "name":"p5_SubtotalMargin","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p5_SubtotalMargin"}},' +
                '"m5":{"alias":"m5","label":"∑Potential Margin","expression":"SUM({field})",' +
                '   "name":"p7_PotentialMargin","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p7_PotentialMargin"}},' +
                '"m7":{"alias":"m7","label":"∑Potential Margin Gain","expression":"SUM({field})",' +
                '   "name":"p9_PotentialMarginGain","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p9_PotentialMarginGain"}},' +
                '"m6":{"alias":"m6","label":"Contribution(Potential Margin Gain)","expression":"SUM({field})",' +
                '   "name":"p9_PotentialMarginGain","advancedProjection":true,"function":"CONTRIB","default":null,"formatString":"Contribution({field})","parameters":{"field":"p9_PotentialMarginGain"}}},' +
                '"filter":{"_constructor":"AdvancedCriteria","operator":"and",' +
                '   "criteria":[{"fieldName":"CCYear","operator":"equals","value":"${out.InputDate}"}]},' +
                '   "aggregateFilter":null,"sortBy":["CCVerkaufsbereich3","CBranchCategory"],' +
                '   "options":{"currency":"EUR"}}')
        .back()
        .getDictionary()
        .buildFromOpaqueString('[{"sectionIdx":1,"category":"PROJECTION","key":"CBranchCategory","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization",' +
                '"categoryLabel":"Projection","keyLabel":"Branch Category","defaultValue":"Branch Category"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m1","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 1","defaultValue":"∑Menge"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m10","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 10","defaultValue":"∑Potential Umsatz Gain"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m2","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 2","defaultValue":"∑Subtotal Umsatz"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m3","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 3","defaultValue":"∑Potential Revenue"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m4","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 4","defaultValue":"∑Subtotal Margin"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m5","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 5","defaultValue":"∑Potential Margin"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m6","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 6","defaultValue":"Contribution(Potential Margin Gain)"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m7","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 7","defaultValue":"∑Potential Margin Gain"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m8","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 8","defaultValue":"Contribution(Potential Umsatz Gain)"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"m9","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Measure 9","defaultValue":"∑Customer Id"},' +
                '{"sectionIdx":1,"category":"PROJECTION","key":"CCVerkaufsbereich3","sectionLabel":"Revenue gain post correction of \"Wrong\" categorization","categoryLabel":"Projection","keyLabel":"Verkaufsbüro","defaultValue":"Verkaufsbüro"}]')
        .back()
        .build()
