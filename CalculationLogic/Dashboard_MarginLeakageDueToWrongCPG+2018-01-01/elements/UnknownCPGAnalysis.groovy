def selectedYear = ' (Year-' + out.YearForAnalysis + ')'
def chart = api.newChartBuilder()
		.newDataTable()
		.getOptions()
		.setInitialDataGrouping(true)
		.setDisableDrilldown(false)
		.back()
		.addSeries()
		.setLabel('Customer Distribution For Unknown CPG')
		.setHideDataLabels(true)
		.setDatamart('124.DMR')//46.DMR
		.setCurrency('EUR')
		.addFilters(FilterOperator.AND)
		.addFilter('CCKundePotentialGruppe', FilterOperator.CONTAINS, 'Unknown')
		.addFilter('CCYear', FilterOperator.EQUAL, out.InputDate)
		.back()
		.addGroupBy('CCVerkaufsbereich3')
		.addGroupBy('CBranchCategory')
		.addAdditionalMeasure()
		.setMeasure('p1_CustomerId')
		.setId(1)
		.withTotal()
		.back()
		.addAdditionalMeasure()
		.setMeasure('p4_SubtotalUmsatz')
		.setId(2)
		.withTotal()
		.back()
		.addAdditionalMeasure()
		.setMeasure('p4_SubtotalUmsatz')
		.setId(3)
		.withAggregation('CONTRIB')
		.back()
		.addAdditionalMeasure()
		.setMeasure('p5_SubtotalMargin')
		.setId(4)
		.withTotal()
		.back()
		.addAdditionalMeasure()
		.setMeasure('p5_SubtotalMargin')
		.setId(5)
		.withAggregation('CONTRIB')
		.back()
		//.setGeneratedQueryDto('{"datamart":"46.DMR","source":"46.DMR","name":null,"label":"Customer Distribution For Unknown CPG","rollup":true,"projections":{"CCVerkaufsbereich3":{"alias":"CCVerkaufsbereich3","label":"Verkaufsburo","expression":"CCVerkaufsbereich3","format":null},"CBranchCategory":{"alias":"CBranchCategory","label":"Branche Category","expression":"CBranchCategory","format":null},"m1":{"alias":"m1","label":"Number of Customers ","expression":"SUM({field})","format":null,"name":"p1_CustomerId","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p1_CustomerId"}},"m2":{"alias":"m2","label":"Umsatz $selectedYear","expression":"SUM({field})","format":null,"name":"p4_SubtotalUmsatz","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p4_SubtotalUmsatz"}},"m3":{"alias":"m3","label":"Umsatz %","expression":"SUM({field})","format":null,"name":"p4_SubtotalUmsatz","advancedProjection":true,"function":"CONTRIB","default":null,"formatString":"Contribution({field})","parameters":{"field":"p4_SubtotalUmsatz"}},"m4":{"alias":"m4","label":"DB ${selectedYear}  ","expression":"SUM({field})","format":null,"name":"p5_SubtotalMargin","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p5_SubtotalMargin"}},"m5":{"alias":"m5","label":"% of Total Unknown CPG DB ${selectedYear}  ","expression":"SUM({field})","format":null,"name":"p5_SubtotalMargin","advancedProjection":true,"function":"CONTRIB","default":null,"formatString":"Contribution({field})","parameters":{"field":"p5_SubtotalMargin"}}},"filter":{"_constructor":"AdvancedCriteria","operator":"and","criteria":[{"fieldName":"CCKundePotentialGruppe","operator":"iContains","value":"Unknown"},{"fieldName":"CCYear","operator":"equals","value":"${out.InputDate}"}]},"aggregateFilter":null,"sortBy":["CCVerkaufsbereich3","CBranchCategory"],"options":{"currency":"EUR"}}')
		.setGeneratedQueryDto('{"datamart":"124.DMR","source":"124.DMR","name":null,"label":"Customer Distribution For Unknown CPG","rollup":true,"projections":{"CCVerkaufsbereich3":{"alias":"CCVerkaufsbereich3","label":"Verkaufsburo","expression":"CCVerkaufsbereich3","format":null},"CBranchCategory":{"alias":"CBranchCategory","label":"Branche Category","expression":"CBranchCategory","format":null},"m1":{"alias":"m1","label":"Number of Customers ","expression":"SUM({field})","format":null,"name":"p1_CustomerId","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p1_CustomerId"}},"m2":{"alias":"m2","label":"Umsatz $selectedYear","expression":"SUM({field})","format":null,"name":"p4_SubtotalUmsatz","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p4_SubtotalUmsatz"}},"m3":{"alias":"m3","label":"Umsatz %","expression":"SUM({field})","format":null,"name":"p4_SubtotalUmsatz","advancedProjection":true,"function":"CONTRIB","default":null,"formatString":"Contribution({field})","parameters":{"field":"p4_SubtotalUmsatz"}},"m4":{"alias":"m4","label":"DB ${selectedYear}  ","expression":"SUM({field})","format":null,"name":"p5_SubtotalMargin","advancedProjection":true,"function":null,"default":null,"formatString":"∑{field}","parameters":{"field":"p5_SubtotalMargin"}},"m5":{"alias":"m5","label":"% of Total Unknown CPG DB ${selectedYear}  ","expression":"SUM({field})","format":null,"name":"p5_SubtotalMargin","advancedProjection":true,"function":"CONTRIB","default":null,"formatString":"Contribution({field})","parameters":{"field":"p5_SubtotalMargin"}}},"filter":{"_constructor":"AdvancedCriteria","operator":"and","criteria":[{"fieldName":"CCKundePotentialGruppe","operator":"iContains","value":"Unknown"},{"fieldName":"CCYear","operator":"equals","value":"${out.InputDate}"}]},"aggregateFilter":null,"sortBy":["CCVerkaufsbereich3","CBranchCategory"],"options":{"currency":"EUR"}}')
		.back()
		.getDictionary()
/*		.buildFromOpaqueString("[{'sectionIdx':1,'category':'PROJECTION','key':'CBranchCategory','sectionLabel':'Customer Distribution For Unknown CPG','categoryLabel':'Projection','keyLabel':'Branche Category','defaultValue':'Branche Category','value':'Branche Category'}," +
				"{'sectionIdx':1,'category':'PROJECTION','key':'m1','sectionLabel':'Customer Distribution For Unknown CPG','categoryLabel':'Projection','keyLabel':'Measure 1','defaultValue':'∑Customer Id','value':'Number of Customers ${selectedYear}'}," +
				"{'sectionIdx':1,'category':'PROJECTION','key':'m2','sectionLabel':'Customer Distribution For Unknown CPG','categoryLabel':'Projection','keyLabel':'Measure 2','defaultValue':'∑Total Umsatz','value':'Umsatz ${selectedYear}','format':'€###,###,###.##'},{'sectionIdx':1,'category':'PROJECTION','key':'m3','sectionLabel':'Customer Distribution For Unknown CPG','categoryLabel':'Projection','keyLabel':'Measure 3','defaultValue':'Contribution(Total Umsatz)','value':'% of Total Unknown CPG Umsatz ${selectedYear}','format':'#.##%'},{'sectionIdx':1,'category':'PROJECTION','key':'m4','sectionLabel':'Customer Distribution For Unknown CPG','categoryLabel':'Projection','keyLabel':'Measure 4','defaultValue':'∑Total DB','format':'€###,###,###.##','value':'DB ${selectedYear}'},{'sectionIdx':1,'category':'PROJECTION','key':'m5','sectionLabel':'Customer Distribution For Unknown CPG','categoryLabel':'Projection','keyLabel':'Measure 5','defaultValue':'Contribution(Total DB)','format':'#.##%','value':'% of Total Unknown CPG DB ${selectedYear}  '},{'sectionIdx':1,'category':'PROJECTION','key':'CCVerkaufsbereich3','sectionLabel':'Customer Distribution For Unknown CPG','categoryLabel':'Projection','keyLabel':'Verkaufsbuero','defaultValue':'Verkaufsbuero','value':'Verkaufsburo'},{'sectionIdx':1,'category':'PROJECTION','key':'m6','sectionLabel':'Series1','categoryLabel':'Projection','keyLabel':'Measure 6','defaultValue':'Contribution(Menge)','value':'Menge %','format':'#.##%'},{'sectionIdx':1,'category':'PROJECTION','key':'m7','sectionLabel':'Series1','categoryLabel':'Projection','keyLabel':'Measure 7','defaultValue':'∑Customer Id','value':'Customer Count'}]")
*/
//		.buildFromOpaqueString('[{"sectionIdx":1,"category":"PROJECTION","key":"CBranchCategory","sectionLabel":"Customer Distribution For Unknown CPG","categoryLabel":"Projection","keyLabel":"Branche Category","defaultValue":"Branche Category","value":"Branche Category"},' +
//				'{"sectionIdx":1,"category":"PROJECTION","key":"m1","sectionLabel":"Customer Distribution For Unknown CPG","categoryLabel":"Projection","keyLabel":"Measure 1","defaultValue":"∑Customer Id","value":"Number of Customers "},' +
//				'{"sectionIdx":1,"category":"PROJECTION","key":"m2","sectionLabel":"Customer Distribution For Unknown CPG","categoryLabel":"Projection","keyLabel":"Measure 2","defaultValue":"∑Total Umsatz","value":"Umsatz ","format":"€###,###,###"},{"sectionIdx":1,"category":"PROJECTION","key":"m3","sectionLabel":"Customer Distribution For Unknown CPG","categoryLabel":"Projection","keyLabel":"Measure 3","defaultValue":"Contribution(Total Umsatz)","value":"% of Total Unknown CPG Umsatz ","format":"#.##%"},{"sectionIdx":1,"category":"PROJECTION","key":"m4","sectionLabel":"Customer Distribution For Unknown CPG","categoryLabel":"Projection","keyLabel":"Measure 4","defaultValue":"∑Total DB","format":"€###,###,###.##","value":"DB "},{"sectionIdx":1,"category":"PROJECTION","key":"m5","sectionLabel":"Customer Distribution For Unknown CPG","categoryLabel":"Projection","keyLabel":"Measure 5","defaultValue":"Contribution(Total DB)","format":"#.##%","value":"% of Total Unknown CPG DB "},{"sectionIdx":1,"category":"PROJECTION","key":"CCVerkaufsbereich3","sectionLabel":"Customer Distribution For Unknown CPG","categoryLabel":"Projection","keyLabel":"Verkaufsbuero","defaultValue":"Verkaufsbuero","value":"Verkaufsburo"},{"sectionIdx":1,"category":"PROJECTION","key":"m6","sectionLabel":"Series1","categoryLabel":"Projection","keyLabel":"Measure 6","defaultValue":"Contribution(Menge)","value":"Menge %","format":"#.##%"},{"sectionIdx":1,"category":"PROJECTION","key":"m7","sectionLabel":"Series1","categoryLabel":"Projection","keyLabel":"Measure 7","defaultValue":"∑Customer Id","value":"Customer Count"}]')
		.back()
		.build()

return chart