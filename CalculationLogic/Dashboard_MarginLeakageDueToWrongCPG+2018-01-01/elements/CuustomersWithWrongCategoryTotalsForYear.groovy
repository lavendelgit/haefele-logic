def field = ['CCYear'  				: 'Year',
             'SUM(p1_CustomerId)'  	: 'CustomerCount',
             'SUM(p4_SubtotalUmsatz)': 'YearlyRevenue',
             'SUM(p5_SubtotalMargin)': 'YearlyMargin']
def filters = [
        Filter.equal('CCYear', out.InputDate),
        Filter.equal('CorrectionType', 'Wrong')
]

lib.TraceUtility.developmentTrace("Printing date ", out.InputDate)
return lib.DBQueries.getDataFromSource(field, 'DM_CustomerCountPerSalesOfficePerBrachForCorrection', filters, 'Rollup')
//return lib.DBQueries.getDataFromSource(field, 'DB_CustomerCountPerSalesOfficePerBrachForCorrection', filters, 'Rollup')