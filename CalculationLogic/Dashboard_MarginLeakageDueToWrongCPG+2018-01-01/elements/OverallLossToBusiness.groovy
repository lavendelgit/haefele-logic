def field = ['CCYear'                : 'Year',
             'CorrectionType'        : 'CorrectionType',
             'p1_CustomerId'         : 'CustomerCount',
             'p3_CCTotalRevenue'     : 'YearlyRevenue',
             'p4_CCTotalMargin'      : 'YearlyMargin',
             'p7_PotentialRevenue'   : 'YearlyPotentialRevenue',
             'p8_PotentialUmsatzGain': 'YearlPotentialRevenueLoss',
             'p5_PotentialMargin'    : 'YearlyPotentialMargin',
             'p6_PotentialMarginGain': 'YearlyPotentialMarginLoss'
]
def filters = [
        Filter.equal('CCYear', out.InputDate)
]
//def data = lib.DBQueries.getDataFromSource(field, 'DB_OverallCustomerCountPerYearPerCorrectionType', filters, 'Rollup')
def data = lib.DBQueries.getDataFromSource(field, 'DM_OverallCustomerCountPerYearPerCorrectionType', filters, 'Rollup')

def totalCustomerCount = 0, totalRev = 0.0, totalPotentialRev = 0.0, yearlyPotRevLoss = 0.0, yearlMar = 0.0, yearPotentialMar = 0.0, yearlyPotentialMarLoss = 0.0
data.each { row ->
    totalCustomerCount += row['CustomerCount']
    totalRev += row['YearlyRevenue']
    yearlMar += row['YearlyMargin']
    if (row['CorrectionType'] == 'Wrong') {
        totalPotentialRev += row['YearlyPotentialRevenue']
        yearlyPotRevLoss += row['YearlPotentialRevenueLoss']
        yearPotentialMar += row['YearlyPotentialMargin']
        yearlyPotentialMarLoss += row['YearlyPotentialMarginLoss']
    } else {
        totalPotentialRev += row['YearlyRevenue']
        yearPotentialMar += row['YearlyMargin']
    }
}

def selectedYear = ' (Year-' + out.YearForAnalysis + ')'
def columns = ['Total Active Customers' + selectedYear, 'Total Umsatz' + selectedYear, 'Potential Total Umsatz' + selectedYear,
               'Total Umsatz Scope Of Improvement' + selectedYear, 'Total DB' + selectedYear, 'Potential Total DB' + selectedYear,
               'Total DB Scope Of Improvement' + selectedYear]
def columnTypes = [FieldFormatType.NUMERIC, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR,
                   FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR,
                   FieldFormatType.MONEY_EUR]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Summary: Scope of Improvement', columns, columnTypes)
resultMatrix.addRow([
        (columns[0]): totalCustomerCount,
        (columns[1]): api.formatNumber("€###,###,###",totalRev),
        (columns[2]): api.formatNumber("€###,###,###",totalPotentialRev),
        (columns[3]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("€###,###,###",yearlyPotRevLoss), "#fc5c65"),
        (columns[4]): yearlMar,
        (columns[5]): yearPotentialMar,
        (columns[6]): dashUtl.boldHighlightWith(resultMatrix, yearlyPotentialMarLoss, "#fc5c65")
])

resultMatrix.setPreferenceName("Business Loss Summary")

return resultMatrix