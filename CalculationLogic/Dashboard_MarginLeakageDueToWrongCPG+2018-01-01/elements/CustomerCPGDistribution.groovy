import net.pricefx.common.api.FieldFormatType

def getRevenueRange(def cpgDetails, String notApplicable) {
    if (!cpgDetails)
        return notApplicable

    if ((cpgDetails[2] || cpgDetails[2] == 0) && cpgDetails[3])
        return cpgDetails[2] + " - " + cpgDetails[3]
    return ">= " + cpgDetails[2]
}


def field = ['p1_CustomerId'         : 'CustomerCount',
             'CCKundePotentialGruppe': 'CPG',
             'CCYear'                : 'Year',
             'p3_CCTotalRevenue'     : 'TotalRevenue',
             'p2_CCTotalMargin'      : 'TotalMargin',
]
def filters = [
        Filter.equal('CCYear', out.InputDate)
]

def data = lib.DBQueries.getDataFromSource(field, 'DM_OverallCustomerCountPerYearPerCPG', filters, 'Rollup')
//def data = lib.DBQueries.getDataFromSource(field, 'DB_OverallCustomerCountPerYearPerCPG', filters, 'Rollup')

long totalCustomers = 0
BigDecimal totalRevenue = 0.0 as BigDecimal
BigDecimal totalMargin = 0.0 as BigDecimal
data?.each {
    totalCustomers += it.CustomerCount
    totalRevenue += it.TotalRevenue
    totalMargin += it.TotalMargin
}
def selectedYear = ' (Year-' + out.YearForAnalysis + ')'
def columns = ['Customer Potential Group(CPG)', 'Umsatz Range', 'Discount % per CPG', 'Number of Customers' + selectedYear, 'Umsatz' + selectedYear,
               '% of Total Active Customers Umsatz' + selectedYear, 'DB' + selectedYear, '% of Total Active Customers DB' + selectedYear]
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.MONEY_EUR, FieldFormatType.PERCENT, FieldFormatType.NUMERIC, FieldFormatType.MONEY_EUR,
                   FieldFormatType.PERCENT, FieldFormatType.MONEY_EUR, FieldFormatType.PERCENT]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Customer Distribution Per Potential Group', columns, columnTypes)
def notApplicable = '- NA -'
resultMatrix.addRow(['Total Active Customers', notApplicable, notApplicable, totalCustomers, api.formatNumber("€###,###,###",totalRevenue), 1.00, totalMargin, 1.00])

def cpgDetails = "", rangeValue
for (row in data) {
    cpgDetails = lib.Find.findCustomerPotentialGroup(row.CPG)
    rangeValue = getRevenueRange(cpgDetails, notApplicable)
    cpgDetails = (cpgDetails) ?: ['', notApplicable, '', '', '', notApplicable]
    resultMatrix.addRow([
            (columns[0]): row.CPG,
            (columns[1]): rangeValue,
            (columns[2]): cpgDetails[5],
            (columns[3]): row.CustomerCount,
            (columns[4]): api.formatNumber("€###,###,###",row.TotalRevenue),
            (columns[5]): dashUtl.highlightCellWithBlue(resultMatrix, lib.MathUtility.divide(row.TotalRevenue, totalRevenue)),
            (columns[6]): row.TotalMargin,
            (columns[7]): lib.MathUtility.divide(row.TotalMargin, totalMargin)
    ])
}

resultMatrix.setPreferenceName ("CPG Distribution")

return resultMatrix