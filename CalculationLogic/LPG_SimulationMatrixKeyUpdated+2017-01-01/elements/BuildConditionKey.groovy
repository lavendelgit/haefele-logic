if (api.isSyntaxCheck()) return

def sku = api.product("sku")

def conditionTable 	= api.input("ConditionTable")
def condiListRows 	= api.findLookupTableValues("ConditionMaster",Filter.equal("attribute3","Yes"))

def conditionTableRow = api.findLookupTableValues("ConditionTableMaster",
        Filter.and(Filter.equal("name",conditionTable),Filter.equal("attribute2","Yes")))?.find()

def conditionKeyList = []

if (conditionTableRow){  
  condiListRows.each { it->
      conditionList = getConditionKeys(sku, it, conditionTable, conditionKeyList, conditionTableRow)
  }
}
api.trace("conditionKeyList",conditionKeyList)
return conditionKeyList


def getConditionKeys (String sku, Map conditionRow, String conditionTable, List conditionKeyList, Map conditionTableRow)
{
  def conditionName = conditionRow.name
  def pgFilter		= conditionTableRow.attribute4
  def conditionRecords = Library.getConditionRecords(sku, conditionRow, conditionTable, conditionKeyList, conditionTableRow)
  
  //api.trace("conditionRecords",conditionRecords)
  
  def skuFilter = conditionTableRow?.attribute3
  if (skuFilter && skuFilter != "*" )
  {
  	conditionRecords = Library.filterSkuBasedRecords(conditionName, conditionTableRow, conditionRecords, sku)
  }
  
  //api.trace("skuFilteredRecords",conditionRecords)

  def filteredRecords = [:]

  if (conditionRecords) {
    if (pgFilter == "Yes") {
      filteredRecords = Library.filterPgRecords(conditionRecords,sku)
    } else {
      filteredRecords = conditionRecords
    }
  }
  
  Library.buildSecondaryKeys(filteredRecords, conditionTableRow, conditionKeyList,conditionName)
  
  
}

