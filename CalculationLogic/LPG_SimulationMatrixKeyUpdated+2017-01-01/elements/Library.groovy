def buildConditionFilter (String conditionTable, Map conditionRow, Map conditionTableRow, String sku)
{
    def conditionName 	= conditionRow.name
    def pxtablename 	= conditionRow.attribute1
  	def generalLib 		= libs.__LIBRARY__.GeneralUtility
  	
    //def pgFilter		= conditionTableRow.attribute4

    
    def targetDate    = lib.Find.targetDateFormatted()

    /*def productHirechary1 = api.product("attribute11")?.split(" ")[0]
    def productHirechary2 = api.product("attribute12")?.split(" ")[0]
    def productHirechary3 = api.product("attribute13")?.split(" ")[0]
    def productHirechary4 = api.product("attribute14")?.split(" ")[0]
    def productHirechary5 = api.product("attribute15")?.split(" ")[0]
    def productHirechary6 = api.product("attribute16")?.split(" ")[0]
    def productHirechary7 = api.product("attribute17")?.split(" ")[0]
     */

    def conditionFilter = [
            Filter.equal("KOTAB",conditionTable),
            Filter.isNotNull('KBETR')
    ]    

    if (conditionName != "ZRA")
        conditionFilter.add(Filter.equal("name", pxtablename))

    //conditionFilter.add(generalLib.getFilterConditionForDateRange("DATAB", "DATBI", targetDate, targetDate))
    /*
    def prodHierarchyNo = 11
    for (int j=0;j<7;j++) {
        def prodHierarchyColumn = api.product("attribute"+"$prodHierarchyNo",sku)?.split(" ")[0]//out["ProdH"+ (j+1)]?.split(" ")[0]
        if (prodHierarchyColumn){
            prodHierarchyColumnValues[j] = prodHierarchyColumn
        }
        prodHierarchyNo++
    }

    if (pgFilter && pgFilter == "Yes") {
      conditionFilter << Filter.or(Filter.equal("ZZPRODH1",productHirechary1),Filter.isNull("ZZPRODH1"))
      conditionFilter << Filter.or(Filter.equal("ZZPRODH2",productHirechary1),Filter.isNull("ZZPRODH2"))
      conditionFilter << Filter.or(Filter.equal("ZZPRODH3",productHirechary1),Filter.isNull("ZZPRODH3"))
      conditionFilter << Filter.or(Filter.equal("ZZPRODH4",productHirechary1),Filter.isNull("ZZPRODH4"))
      conditionFilter << Filter.or(Filter.equal("ZZPRODH5",productHirechary1),Filter.isNull("ZZPRODH5"))
      conditionFilter << Filter.or(Filter.equal("ZZPRODH6",productHirechary1),Filter.isNull("ZZPRODH6"))
      conditionFilter << Filter.or(Filter.equal("ZZPRODH7",productHirechary1),Filter.isNull("ZZPRODH7"))
    }
    def filteredRecordsTemp = filteredRecords.findAll { it -> it["ZZPRODH$prodHierarchyNo"] == prodHierarchyColumnValues[i]}
    */

    //api.trace("conditionFilter",conditionFilter)

    return conditionFilter
}



def filterPgRecords(condiResult,sku)
{
    condiResult.sort
    //api.trace("condiResult",condiResult)

    def prodHierarchyColumnValues = []
    def prodHierarchyNo = 11

    for (int j=0;j<7;j++) {
        def prodHierarchyColumn = api.product("attribute"+"$prodHierarchyNo",sku)?.split(" ")[0]//out["ProdH"+ (j+1)]?.split(" ")[0]
        if (prodHierarchyColumn){
            prodHierarchyColumnValues[j] = prodHierarchyColumn
        }
        prodHierarchyNo++
    }

    def filteredRecords = condiResult
    for (int i=0;i<prodHierarchyColumnValues.size();i++) {
        if (prodHierarchyColumnValues[i]) {
            prodHierarchyNo = i+1
            def filteredRecordsTemp = filteredRecords.findAll { it -> it["ZZPRODH$prodHierarchyNo"] == prodHierarchyColumnValues[i]}
            if (filteredRecordsTemp)
                filteredRecords = filteredRecordsTemp
            else {
                filteredRecords = null
                break
            }
        }
    }
  
    def fr = condiResult.findAll { it -> it.ZZPRODH1 == null}
    if (fr && fr.size()) {
        //api.trace("here",fr.size())
        //api.trace("fr",fr)
        if (filteredRecords)
            filteredRecords.addAll(fr)
        else
            filteredRecords = fr
    }
  
    return filteredRecords
}

def getConditionRecords (String sku, Map conditionRow, String conditionTable, List conditionKeyList, Map conditionTableRow)
{
  	def conditionName 	= conditionRow.name
    def tableList 		= conditionRow.attribute2
  
  if (tableList.contains(conditionTable)) {
    api.logInfo("************getConditionRecordsFromCache","trying to access cache getConditionRecordsFromCache")
  	return libs.SharedLib.CacheUtils.getOrSetMap(api.global, conditionName, [sku, conditionRow, conditionTable, conditionKeyList, conditionTableRow], { _sku, _conditionRow, _conditionTable, _conditionKeyList, _conditionTableRow ->
        //get bulk material from product master
        try{
            return getConditionRecordsFromCache( _sku, _conditionRow, _conditionTable, _conditionKeyList, _conditionTableRow)
        }
        catch (any) {
            //api.trace("getConditionRecordsFromCache", "error", any)
            return null
        }
    })
	}
}

def getConditionRecordsFromCache (sku, conditionRow, conditionTable, conditionKeyList, conditionTableRow)
{
  api.logInfo("************getConditionRecordsFromCache","creating getConditionRecordsFromCache")
  def result = []
  def conditionName = conditionRow.name
  def pgFilter		= conditionTableRow.attribute4
  
  def conditionFilter = buildConditionFilter (conditionTable, conditionRow, conditionTableRow, sku)
  def fields
  
  if (conditionName != "ZRA") {
      fields = (pgFilter && pgFilter=="Yes")?["sku","KOTAB","KBETR","ZZPRODH1","ZZPRODH2","ZZPRODH3","ZZPRODH4","ZZPRODH5","ZZPRODH6","ZZPRODH7"]:["sku","KOTAB","KBETR"]
    } else {
      fields = (pgFilter && pgFilter == "Yes")?["MATNR","KOTAB","KBETR", "ZZPRODH1","ZZPRODH2","ZZPRODH3","ZZPRODH4","ZZPRODH5","ZZPRODH6","ZZPRODH7"]:["MATNR","KOTAB","KBETR"]
    }
  
  conditionTableRow?.attribute1?.split(",")?.each{ cols->
            fields << cols
          }
      	//api.trace("fields",fields)
  
    if (conditionName != "ZRA") {
      //pick data from PX table
      def start = 0
      while (recs = api.find("PX50", start, api.getMaxFindResultsLimit(), "sku",fields, true ,*conditionFilter)) {
        result.addAll(recs)
        start += recs.size()
      }

    } else {
      def ctx = api.getDatamartContext()
      def ds = ctx.getDataSource("ZRA")

      def query = ctx.newQuery(ds, false)
      for (key in fields) {
        query.select(key)
      }
      query.where(*conditionFilter)
      //api.trace("query",query)
      result = ctx.executeQuery(query)?.getData()?.collect()
    }
	
  //api.trace("result",result)
  return result
    
}

def filterSkuBasedRecords (conditionName, conditionTableRow, conditionRecords, sku)
{
  //filter sku base records
    def skuFieldName 	= (conditionName != "ZRA")?"sku":"MATNR"    
    def skuFilterList 	= conditionTableRow?.attribute3?.split(",")
    //api.trace("skuFilterList.size()",skuFilterList.size())
    if (skuFilterList.size() == 2) {
      skuFilteredRecords = conditionRecords.findAll { it -> it["$skuFieldName"] == sku || it["$skuFieldName"] == "*"}
    } else {
      skuFilteredRecords = conditionRecords.findAll { it -> it["$skuFieldName"] == sku }
    }
  
  return skuFilteredRecords
}
def buildSecondaryKeys(filteredRecords, conditionTableRow, conditionKeyList, conditionName)
{
  
  // build the keys
  if (filteredRecords && filteredRecords.size() > 0) {
      if (conditionTableRow.attribute1){
        def keyColumns = conditionTableRow?.attribute1?.split(",")

        api.trace("**********keyColumns",keyColumns as List)
        filteredRecords.collect { condiTabFilterRecord ->
          def key = conditionName
          api.trace("************",condiTabFilterRecord)
          keyColumns.each{ column ->
            if (condiTabFilterRecord["$column"])
            key += "~"+ condiTabFilterRecord["$column"]
          }
          key += "~"+condiTabFilterRecord["KBETR"]
          conditionKeyList << key
        }
      }

      else{
        api.trace("filteredRecords",filteredRecords)
        conditionKeyList << conditionName+"~"+filteredRecords["KBETR"][0]
      }
    }
}