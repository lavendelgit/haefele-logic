if (!api.global.allMaterialList) {

  List result = Library.getArticlesWithRevenue()
  api.global.allMaterialList = result.Material
  api.trace("out.Flag90Percent",out.Flag90Percent)
  api.trace("result",result.Material.size())
  
  if (out.Flag90Percent) {
	BigDecimal totRevenue = Library.getTotalRevenue()

  	Map skuPercentageRevenueMap=[:]

    result?.each { skuDetails->
        def sku = skuDetails.Material
        def skuRevenue = skuDetails?.Revenue as BigDecimal
      if (skuRevenue) {
        def skuRevenuePercentage = (skuRevenue/totRevenue) * 100
        skuPercentageRevenueMap << ["$sku" : skuRevenuePercentage]
      }
    }

    def skuPercentageRevenueMapSorted = skuPercentageRevenueMap.sort {a, b -> b.value <=> a.value}
    def totPercentageRevenue = 0
    def skuList = []

    for (def skuDetails in skuPercentageRevenueMapSorted) { 
      String sku = skuDetails.key.toUpperCase()

      if (sku.contains("X")) {
          api.trace("sku",skuDetails.key)
          skuList << skuDetails.key
      } else {
        totPercentageRevenue += skuDetails.value
        skuList << skuDetails.key

        if (totPercentageRevenue >= 90) {
              break
        }
      }

    }
    //api.trace("totPercentageRevenue",totPercentageRevenue)
  //api.trace("skuList",skuList)
  api.global.transactionSkusList = skuList
  }

  
}
