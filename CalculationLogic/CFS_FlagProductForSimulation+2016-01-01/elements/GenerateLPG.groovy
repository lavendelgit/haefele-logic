String sku 				= api.currentItem()?.sku

return out.Flag90Percent ? (api.global.transactionSkusList.contains("$sku")?"Yes":"No") : (api.global.allMaterialList.contains(sku) ? "Inland 2020": null)
