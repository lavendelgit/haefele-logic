
BigDecimal getTotalRevenue () {
  
  
  List filters 		= [Filter.equal("InvoiceDateYear", out.AnalysisYear)]
  
  if (out.OnlyInlandArticles) filters << (out.TransactionInladFilter)
  
  def dmCtx 	= api.getDatamartContext()
  def dm 		= dmCtx.getDatamart("TransactionsDM")
  def dmQuery 	= dmCtx.newQuery(dm,true)
                  .select("SUM(Revenue)", "revenue")
                  .where(*filters)

  BigDecimal totRevenue =  dmCtx.executeQuery(dmQuery)?.getData()?.collect()?.revenue[0]
  
  return totRevenue
  
}

List getArticlesWithRevenue () {
  
  List filters = [Filter.equal("InvoiceDateYear", out.AnalysisYear)]//, Filter.notIn("Revenue",[0,null])]
  
  if (out.OnlyInlandArticles) filters << (out.TransactionInladFilter)
  api.trace("filters",filters)
    def dmCtx = api.getDatamartContext()
    def dm 	  = dmCtx.getDatamart("TransactionsDM")
    dmQuery   = dmCtx.newQuery(dm,true)
                .select("Material")
                .select("SUM(Revenue)", "Revenue")
                .where(*filters)

  List result = dmCtx.executeQuery(dmQuery)?.getData()?.collect()
 
  return result
}

List getAnalysisYears() {
    //TODO : Read from DM
    return ['2016', '2017', '2018', '2019', '2020']
}