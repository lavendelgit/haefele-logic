/**
 * Creates a combo-box or a multi-select box
 * @param name
 * @param label
 * @param values
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object multiSelectBoxEntry(String name, String label, List values, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.options(name, values)
    }
}

/**
 * Creates a single-select box
 * @param name
 * @param label
 * @param values
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object selectBoxEntry(String name, String label, List values, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.option(name, values)
    }
}

/**
 * Creates a single-select box with Map as one of the input to support Key/Value pair
 * @param name
 * @param label
 * @param valuesTextMap
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object selectBoxEntry(String name, String label, Map valuesTextMap, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        List values = valuesTextMap?.keySet() as List
        def input = api.option(name, values)
        api.getParameter(inputName)?.setConfigParameter("labels", valuesTextMap)
        return input
    }
}

/**
 * Creates a boolean user entry
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @return
 */
Object booleanEntry(String name, String label, boolean isRequired = false, defaultValue = null) {
    return newInput(name, label, isRequired, defaultValue) { String inputName ->
        return api.booleanUserEntry(inputName)
    }
}


/**
 * Creates a input entry
 * @param name
 * @param label
 * @param isRequired
 * @param defaultValue
 * @param inputCreator
 * @return
 */
Object newInput(String name, String label, boolean isRequired, defaultValue, Closure inputCreator) {
    def input = inputCreator.call(name)
    def param = api.getParameter(name)
    if (param) {
        param.setLabel(label)
        param.setRequired(isRequired)
        if (param.getValue() == null && defaultValue != null) {
            param.setValue(defaultValue)
        }
    }
    return input
}