if (!out.ShowScatterChart) {
    return
}
def hLib = libs.HighchartsLibrary
Map DATA_COLUMNS = Constants.DATA_COLUMNS_DEF
def legend = hLib.Legend.newLegend().setEnabled(false)
def chartData = api.local.MatrixData

def revenueAxis = hLib.Axis.newAxis().setTitle("Revenue")

def marginAxis = hLib.Axis.newAxis().setTitle("Margin")

def dataLabels = libs.HighchartsLibrary.DataLabel.newDataLabel().setEnabled(false)
                     .setFormat("{point.name}")

def bubbleSeriesData = chartData.collect { Map row ->
    [name: row[DATA_COLUMNS.ARTICLE_NUMBER.name],
     x   : row[DATA_COLUMNS.EXPECTED_REVENUE.name],
     y   : row[DATA_COLUMNS.EXPECTED_MARGIN.name],
     z   : row[DATA_COLUMNS.TOTAL_QTY_SOLD.name]]
}

def bubbleSeries = hLib.BubbleSeries.newBubbleSeries().setName("Revenue to Margin")
                       .setData(bubbleSeriesData)
                       .setDataLabels(dataLabels)
                       .setXAxis(revenueAxis)
                       .setYAxis(marginAxis)

def pointFormat = '<tr><th colspan="2"><h3>{point.name}</h3></th></tr>' +
        '<tr><th>Σ Revenue:</th><td>{point.x:,#,###.2f} €</td></tr>' +
        '<tr><th>Σ Margin:</th><td>{point.y:,#,###.2f} €</td></tr>' +
        '<tr><th>Σ Quantity:</th><td>{point.z}</td></tr>'

def tooltip = hLib.Tooltip.newTooltip().setPointFormat(pointFormat)
                  .setUseHTML(true)
                  .setHeaderFormat("<table>")
                  .setFooterFormat("</table")

def chartDef = hLib.Chart.newChart().setTitle("Material Distribution by Revenue / Margin / Qty")
                   .setSeries(bubbleSeries)
                   .setLegend(legend)
                   .setTooltip(tooltip)
                   .getHighchartFormatDefinition()

return api.buildHighchart(chartDef).showDataTab()