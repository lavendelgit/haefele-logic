Map queryBuilder = queryBuilder().setGrossPriceColumn(out.GrossPriceColumn)
        .setLandingPriceColumn(out.LandingPriceColumn)
        .setQuantityColumn(out.QuantityColumn)
        .setDataColumns(Constants.DATA_COLUMNS_DEF)
        .setDecimalPlaceMap(Constants.DECIMAL_PLACE)
        .setGrossPriceChangePercent(out.GrossPriceChangePer)
        .setLandingPriceChangePercent(out.LandingPriceChangePer)
        .setQuantityChangePercent(out.QuantityChangePer)
        .setRecommendedGrossMargin(out.ExpectedMarginPer)

List exceptions = out.Exceptions
List defExceptions = [
  					["Key":"DefKey1","selected":false,"Data Filter":"LOWER(Material) LIKE LOWER('%X%')","Gross Price Change Percent":"0","Purchase Price Change Percent":out.LandingPriceChangePer*100,"Quantity Change Percent":"0"],
  					["Key":"DefKey2","selected":false,"Data Filter":"LOWER(Material) LIKE LOWER('%999.11%')","Gross Price Change Percent":"0","Purchase Price Change Percent":out.LandingPriceChangePer*100,"Quantity Change Percent":"0"]
  ]
if (!exceptions) exceptions = []
exceptions.addAll(defExceptions)


Library.LOG("########### exceptions ", api.jsonEncode(exceptions))
Map EXCEPTION_COLUMNS = Constants.EXCEPTION_COLUMNS

exceptions.each { Map exception ->
    def grossPriceChangePercent = exception[EXCEPTION_COLUMNS.GrossPriceChangeByPercent.label]
    def purchasePriceChangePercent = exception[EXCEPTION_COLUMNS.PurchasePriceChangeByPercent.label]
    def quantityChangePercent = exception[EXCEPTION_COLUMNS.QuantityChangeByPercent.label]
    String filter = exception[EXCEPTION_COLUMNS.ExceptionFilter.label]
    if (filter && (grossPriceChangePercent != null || purchasePriceChangePercent != null || quantityChangePercent != null)) {
        queryBuilder.newException()
                .setDataFilter(filter)
                .setGrossPriceChangePercent(grossPriceChangePercent)
                .setPurchasePriceChangePercent(purchasePriceChangePercent)
                .setQuantityChangePercent(quantityChangePercent)
                .addException()
    }
}

return queryBuilder


/**
 * QueryBuilder for exceptions
 * @return
 */
protected Map exceptionsQueryBuilder() {
    Map methods = [:]
    Map definition = [exceptions: []]

    methods += [
            forExceptions                     : { List exceptions ->
                definition.exceptions = exceptions
                return methods
            },
            withBaseGrossPriceChangePercent   : { BigDecimal baseChangePercent ->
                definition.baseGrossPriceChangePercent = baseChangePercent
                return methods
            },
            withBasePurchasePriceChangePercent: { BigDecimal baseChangePercent ->
                definition.basePurchasePriceChangePercent = baseChangePercent
                return methods
            },
            withBaseQuantityChangePercent     : { BigDecimal baseChangePercent ->
                definition.baseQuantityChangePercent = baseChangePercent
                return methods
            },
            withBaseRecommendedGrossMargin    : { BigDecimal baseRecommendedGrossMargin ->
                definition.baseRecommendedGrossMargin = baseRecommendedGrossMargin
                return methods
            },
            forDataSourceName                 : { String dataSourceName ->
                definition.dataSourceName = dataSourceName
                return methods
            },
            addBaseException                  : {
                Map baseException = exception().setChangePercent(definition.baseChangePercent)
                        .setRecommendedGrossMargin(definition.baseRecommendedGrossMargin)
                        .setType('B')
                        .getDefinition()
                definition.exceptions += [baseException]

                return methods
            },
            getAggregationExpr                : { String columnName ->
                return "MAX(${columnName})"
            },
            getBaseChangePercent              : { String baseChangePercentName ->
                return definition[baseChangePercentName]
            },
            getExceptionQuery                 : { Map exception ->
                api.trace("exception", exception)
                api.trace("definition", definition)
                StringBuilder query = new StringBuilder('SELECT ').append("\n\t")
                        .append('Material AS ExceptionMaterial,').append("\n\t")
                        .append(exception.grossPriceChangePercent != null ? exception.grossPriceChangePercent : methods.getBaseChangePercent('baseGrossPriceChangePercent')).append(' AS ExceptionGrossPriceChangePercent,').append("\n\t")
                        .append(exception.purchasePriceChangePercent != null ? exception.purchasePriceChangePercent : methods.getBaseChangePercent('basePurchasePriceChangePercent')).append(' AS ExceptionPurchasePriceChangePercent,').append("\n\t")
                        .append(exception.quantityChangePercent != null ? exception.quantityChangePercent : methods.getBaseChangePercent('baseQuantityChangePercent')).append(' AS ExceptionQuantityChangePercent,').append("\n\t")
                        .append(exception.recommendedGrossMargin != null ? exception.recommendedGrossMargin : methods.getBaseChangePercent('baseRecommendedGrossMargin')).append(' AS ExceptionRecommendedGrossMargin').append("\n")
                        .append(' FROM ').append(definition.dataSourceName).append("\n")
                if (exception.dataFilter) {
                    query.append(' WHERE ').append(exception.dataFilter).append("\n")
                }
                return query.toString()
            },
            getQueryString                    : {
                if (definition.exceptions) {
                    //methods.addBaseException()
                    StringBuilder query = new StringBuilder('SELECT ').append('\n\t')
                            .append('ExceptionMaterial AS ExceptionMaterial,').append("\n\t")
                            .append(methods.getAggregationExpr('ExceptionGrossPriceChangePercent')).append(' AS ExceptionGrossPriceChangePercent,').append("\n\t")
                            .append(methods.getAggregationExpr('ExceptionPurchasePriceChangePercent')).append(' AS ExceptionPurchasePriceChangePercent,').append("\n\t")
                            .append(methods.getAggregationExpr('ExceptionQuantityChangePercent')).append(' AS ExceptionQuantityChangePercent,').append("\n\t")
                            .append(methods.getAggregationExpr('ExceptionRecommendedGrossMargin')).append(' AS ExceptionRecommendedGrossMargin').append("\n")
                            .append(' FROM ((').append("\n\t\t")
                            .append(
                                    definition.exceptions.collect { Map exception ->
                                        methods.getExceptionQuery(exception)
                                    }?.findAll { String strQuery ->
                                        strQuery != null
                                    }?.join(' ) UNION ( ')
                            ).append("\n")
                            .append(' )) ExceptionRows GROUP BY ExceptionMaterial').append("\n")

                    return query.toString()
                }
            }
    ]
    return methods
}

/**
 * Map to hold exception data
 * @return
 */
protected Map exception() {
    Map methods = [:]
    Map definition = [type: 'E']

    methods += [
            forParent                    : { Map parent ->
                definition.parent = parent
                return methods
            },
            setDataFilter                : { String dataFilter ->
                definition.dataFilter = dataFilter
                return methods
            },
            asBigDecimal                 : { Object value ->
                return (value instanceof String) ? (value.isBigDecimal() ? value.toBigDecimal() : null) : value
            },
            toPercent                    : { Object percentValue ->
                percentValue = methods.asBigDecimal(percentValue)
                return (percentValue != null ? percentValue / 100 : null)
            },
            setGrossPriceChangePercent   : { Object changePercent ->
                api.trace("setGrossPriceChangePercent ", changePercent)
                definition.grossPriceChangePercent = methods.toPercent(changePercent)
                return methods
            },
            setPurchasePriceChangePercent: { Object changePercent ->
                api.trace("setPurchasePriceChangePercent", changePercent)
                definition.purchasePriceChangePercent = methods.toPercent(changePercent)
                return methods
            },
            setQuantityChangePercent     : { Object changePercent ->
                api.trace("setQuantityChangePercent", changePercent)
                definition.quantityChangePercent = methods.toPercent(changePercent)
                return methods
            },
            setRecommendedGrossMargin    : { Object recommendedGrossMargin ->
                api.trace("setRecommendedGrossMargin", changePercent)
                definition.recommendedGrossMargin = methods.asBigDecimal(recommendedGrossMargin)
                return methods
            },
            setType                      : { String type ->
                definition.type = type ?: 'E'
                return methods
            },
            addException                 : {
                return definition.parent.addException(methods.getDefinition())
            },
            getDefinition                : {
                return definition.findAll { param -> param.key != 'parent' }
            }
    ]
    return methods
}

/**
 * Base query builder
 * @return
 */
protected Map queryBuilder() {
    Map methods = [:]
    Map definition = [exceptions: []]

    methods += [
            getBaseDataSourceName                  : {
                return 'T1'
            },
            setGrossPriceColumn                    : { String grossPriceColumn ->
                definition.grossPriceColumn = grossPriceColumn
                return methods
            },
            setLandingPriceColumn                  : { String landingPriceColumn ->
                definition.landingPriceColumn = landingPriceColumn
                return methods
            },
            setQuantityColumn                      : { String quantityColumn ->
                definition.quantityColumn = quantityColumn
                return methods
            },
            setDataColumns                         : { Map dataColumns ->
                definition.dataColumns = dataColumns
                return methods
            },
            setDecimalPlaceMap                     : { Map decimalPlaceMap ->
                definition.decimalPlaceMap = decimalPlaceMap
                return methods
            },
            setGrossPriceChangePercent             : { BigDecimal changePercent ->
                definition.grossPriceChangePercent = changePercent
                definition.isGrossPriceChange = definition.grossPriceChangePercent != null && definition.grossPriceChangePercent != 0
                return methods
            },
            setLandingPriceChangePercent           : { BigDecimal changePercent ->
                definition.landingPriceChangePercent = changePercent
                definition.isLandingPriceChange = definition.landingPriceChangePercent != null && definition.landingPriceChangePercent != 0
                return methods
            },
            setQuantityChangePercent               : { BigDecimal changePercent ->
                definition.quantityChangePercent = changePercent
                definition.isQuantityChange = definition.quantityChangePercent != null && definition.quantityChangePercent != 0
                return methods
            },
            setExceptions                          : { List exceptions ->
                definition.exceptions = exceptions
                return methods
            },
            addException                           : { Map exception ->
                definition.exceptions.add(exception)
                return methods
            },
            getExceptions                          : {
                return definition.exceptions
            },
            hasExceptions                          : {
                return definition.exceptions?.size() > 0
            },
            newException                           : {
                Map exception = exception().forParent(methods)
                return exception
            },
            getExceptionQueryString                : {
                return exceptionsQueryBuilder().forExceptions(methods.getExceptions())
                        .forDataSourceName(methods.getBaseDataSourceName())
                        .withBaseGrossPriceChangePercent(definition.grossPriceChangePercent)
                        .withBasePurchasePriceChangePercent(definition.landingPriceChangePercent)
                        .withBaseQuantityChangePercent(definition.quantityChangePercent)
                        .withBaseRecommendedGrossMargin(definition.recommendedGrossMargin)
                        .getQueryString()
            },
            setRecommendedGrossMargin              : { BigDecimal recommendedGrossMargin ->
                definition.recommendedGrossMargin = recommendedGrossMargin
                return methods
            },
            setShowBreakupByDiscountType           : { boolean showBreakupByDiscountType ->
                definition.showBreakupByDiscountType = showBreakupByDiscountType
                return methods
            },
            getCurrentBaseCostExpr                 : {
                return 'BaseCost'
            },
            getExpectedBaseCostExpr                : {
                if (!definition.expectedBaseCostExpr) {
                    definition.expectedBaseCostExpr = definition.isLandingPriceChange ? "(BaseCost + (BaseCost * ${methods.getLandingPriceChangePercent()}))" : "BaseCost"
                }
                return definition.expectedBaseCostExpr
            },
            getRecommendedBaseCostExpr             : {
                if (definition.recommendedGrossMargin != null && !definition.recommendedBaseCostExpr) {
                    definition.recommendedBaseCostExpr = definition.isLandingPriceChange ? "(BaseCost + (BaseCost * ${methods.getLandingPriceChangePercent()}))" : "BaseCost"
                }
                return definition.recommendedBaseCostExpr
            },
            getCurrentGrossPriceExpr               : {
                return "GrossPrice"
            },
            getExpectedGrossPriceExprForPocketPrice: {
                if (!definition.expectedGrossPriceExprForPocketPrice) {
                    definition.expectedGrossPriceExprForPocketPrice = definition.isGrossPriceChange ? """
                                                        (CASE WHEN ConditionName = 'ZRI' AND ConditionTable = 'A617' AND ZPL IS NOT NULL THEN ZPL - (ZPL * ${definition.grossPriceChangePercent}) 
                                                              ELSE (GrossPrice + (GrossPrice * ${methods.getGrossPriceChangePercent()})) 
                                                        END)
                                                """ : "GrossPrice"
                }
                return definition.expectedGrossPriceExprForPocketPrice
            },
            getExpectedGrossPriceExpr              : {
                if (!definition.expectedGrossPriceExpr) {
                    definition.expectedGrossPriceExpr = definition.isGrossPriceChange ? "(GrossPrice + (GrossPrice * ${methods.getGrossPriceChangePercent()}))" : "GrossPrice"
                }
                return definition.expectedGrossPriceExpr
            },
            getRecommendedGrossPriceExpr           : {
                if (definition.recommendedGrossMargin != null && !definition.recommendedGrossPriceExpr) {
                    definition.recommendedGrossPriceExpr = "(${methods.getExpectedBaseCostExpr()}/NULLIF(1-${definition.recommendedGrossMargin},0))"
                }
                return definition.recommendedGrossPriceExpr
            },
            getExpectedQuantityExpr                : {
                if (!definition.expectedQuantityExpr) {
                    definition.expectedQuantityExpr = definition.isQuantityChange ? "(QtySold + (QtySold * ${methods.getQuantityChangePercent()}))" : "QtySold"
                }
                return definition.expectedQuantityExpr
            },
            getContributionColumnName              : { String discountType, String calculationType ->
                String calculationTypePrefix = calculationType.substring(0, 3).toLowerCase()
                return "${calculationTypePrefix}_${discountType}DiscountRevenueContribution"
            },
            getPocketPriceExprByDiscountType       : { String pricingType, String calculationType ->
                String queryExpr = ""
                if (calculationType == Constants.CALCULATION_TYPES.CURRENT) {
                    switch (pricingType) {
                        case Constants.PRICING_TYPES.MARKUP:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.MARKUP}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.NET_PRICE:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.NET_PRICE}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.ABSOULTE_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.ABSOULTE_DISCOUNT}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.PER_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.PER_DISCOUNT}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.X_ARTICLE:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.X_ARTICLE}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.INTERNAL:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.INTERNAL}' THEN HistoryPocketPrice"
                            break
                        case Constants.PRICING_TYPES.NO_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.NO_DISCOUNT}' THEN HistoryPocketPrice"
                            break
                    }
                } else {
                    String baseCostExpr = null
                    String grossPriceExpr = null
                    switch (calculationType) {
                        case Constants.CALCULATION_TYPES.CURRENT:
                            baseCostExpr = methods.getCurrentBaseCostExpr()
                            grossPriceExpr = methods.getCurrentGrossPriceExpr()
                            break
                        case Constants.CALCULATION_TYPES.EXPECTED:
                        case Constants.CALCULATION_TYPES.TEMPORARY:
                            baseCostExpr = methods.getExpectedBaseCostExpr()
                            grossPriceExpr = methods.getExpectedGrossPriceExprForPocketPrice()
                            break
                        case Constants.CALCULATION_TYPES.RECOMMEND:
                            baseCostExpr = methods.getRecommendedBaseCostExpr()
                            grossPriceExpr = methods.getRecommendedGrossPriceExpr()
                            break
                    }
                    switch (pricingType) {
                        case Constants.PRICING_TYPES.MARKUP:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.MARKUP}' THEN ${baseCostExpr} * (1 + KBETR)"
                            break
                        case Constants.PRICING_TYPES.NET_PRICE:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.NET_PRICE}' THEN KBETR"
                            break
                        case Constants.PRICING_TYPES.ABSOULTE_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.ABSOULTE_DISCOUNT}' THEN ${grossPriceExpr} - KBETR"
                            break
                        case Constants.PRICING_TYPES.PER_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.PER_DISCOUNT}' THEN ${grossPriceExpr} * (1 + KBETR)"
                            break
                        case Constants.PRICING_TYPES.X_ARTICLE:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.X_ARTICLE}' THEN CASE WHEN QtySold <> 0 THEN ROUND(TotalRevenue / QtySold, ${Constants.ROUNDING_DECIMAL_PLACES}) ELSE TotalRevenue END "
                            break
                        case Constants.PRICING_TYPES.INTERNAL:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.INTERNAL}' THEN CASE WHEN QtySold <> 0 THEN ROUND(TotalRevenue / QtySold, ${Constants.ROUNDING_DECIMAL_PLACES}) ELSE TotalRevenue END "
                            break
                        case Constants.PRICING_TYPES.NO_DISCOUNT:
                            queryExpr = "WHEN PricingType = '${Constants.PRICING_TYPES.NO_DISCOUNT}' THEN ${grossPriceExpr}"
                            break
                    }
                }
                return queryExpr
            },
            getContributionQuery                   : { String builderContext, String calculationType, String discountType, String pricingType ->
                String queryFragment = ""
                String level3RevColName = methods.getContributionColumnName(discountType, calculationType)
                String level2PocketPriceColName = methods.getContributionColumnName("AvgPP${discountType}", calculationType)
                String level2RevenueColName = methods.getContributionColumnName("AvgRev${discountType}", calculationType)
                String level2PocketMarginColName = methods.getContributionColumnName("Margin${discountType}", calculationType)
                String level2TotalPocketMarginColName = methods.getContributionColumnName("TotalMargin${discountType}", calculationType)
                String pCaseDiscountType = discountType.uncapitalize()
                String uCaseDiscountType = discountType.toUpperCase()
                String calculationTypePrefix = calculationType.substring(0, 3).toLowerCase()
                String uCaseCalculationType = calculationType.toUpperCase()
                String costColumnName = Constants.CALCULATION_TYPE_COSTS[calculationType]
                String qtyColumnName = Constants.CALCULATION_TYPE_QUANTITY[calculationType]
                String qtyColumnNameCurrent = Constants.CALCULATION_TYPE_QUANTITY["CURRENT"]

                switch (builderContext) {
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL3:
                        String attributeName = "${calculationTypePrefix}${pCaseDiscountType}DiscountContributionPerRowQuery"
                        if (!definition[attributeName]) {
                            definition[attributeName] = "(CASE ${methods.getPocketPriceExprByDiscountType(pricingType, calculationType)} ELSE 0 END) as ${level3RevColName}"
                        }
                        queryFragment = definition[attributeName]
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                        String attributeName = "${calculationTypePrefix}${pCaseDiscountType}DiscountContributionAggQuery"
                        //String pocketMarginExpr = "CASE WHEN ${level3RevColName} <> 0 THEN ${level3RevColName} - ${costColumnName} ELSE 0 END"
                        String pocketMarginExpr = pricingType in [Constants.PRICING_TYPES.X_ARTICLE, Constants.PRICING_TYPES.INTERNAL, Constants.PRICING_TYPES.NET_PRICE] ?
                                "CASE WHEN ${level3RevColName} <> 0 THEN ${level3RevColName} - ${costColumnName} ELSE 0 END" :
                                "CASE WHEN PricingType = '${pricingType}' THEN ${level3RevColName} - ${costColumnName} ELSE 0 END"
                  		String expectedPocketMarginExpr = pricingType in [Constants.PRICING_TYPES.X_ARTICLE, Constants.PRICING_TYPES.INTERNAL, Constants.PRICING_TYPES.NET_PRICE] ?
                                "CASE WHEN ${level3RevColName} <> 0 THEN ${level3RevColName} * ${qtyColumnName} - ${costColumnName} * QtySold ELSE 0 END" :
                                "CASE WHEN PricingType = '${pricingType}' THEN ${level3RevColName} * ${qtyColumnName} - ${costColumnName} * QtySold ELSE 0 END"
                        String level2TotalPocketMarginExpr = (calculationType == Constants.CALCULATION_TYPES.EXPECTED) ?
                                """AVG(${expectedPocketMarginExpr})""" :
                                """AVG((${pocketMarginExpr}) *  ${qtyColumnName})"""
                        if (!definition[attributeName]) {
                            definition[attributeName] =
                                    """AVG(${level3RevColName})                                                                    as ${level2PocketPriceColName},
                                        AVG(${level3RevColName} * ${qtyColumnName})                                                 as ${level2RevenueColName},
                                        AVG(${pocketMarginExpr})                                                                    as ${level2PocketMarginColName},
                                        ${level2TotalPocketMarginExpr}                                                              as ${level2TotalPocketMarginColName}"""
                        	}
                        queryFragment = definition[attributeName]
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                        String attributeName = "${calculationTypePrefix}${pCaseDiscountType}DiscountContributionQuery"
                        int decimalPlace = Constants.ROUNDING_DECIMAL_PLACES
                        Map DATA_COLUMNS = definition.dataColumns
                        if (!definition[attributeName]) {
                            definition[attributeName] =
                                    """ROUND(AVG(${level2PocketPriceColName}),${decimalPlace})                                                as '${DATA_COLUMNS[uCaseCalculationType + '_POCKET_PRICE_BY_' + uCaseDiscountType]?.name}',    
                                    ROUND(SUM(${level2RevenueColName}),${decimalPlace})                                                    as '${DATA_COLUMNS[uCaseCalculationType + '_REVENUE_BY_' + uCaseDiscountType]?.name}',    
                                    ROUND(AVG(${level2PocketMarginColName}),${decimalPlace})                                               as '${DATA_COLUMNS[uCaseCalculationType + '_POCKET_MARGIN_BY_' + uCaseDiscountType]?.name}',    
                                    ROUND(SUM(${level2TotalPocketMarginColName}),${decimalPlace})                                          as '${DATA_COLUMNS[uCaseCalculationType + '_TOTAL_POCKET_MARGIN_BY_' + uCaseDiscountType]?.name}',    
                                    ROUND(SUM(${level2TotalPocketMarginColName}) / NULLIF(SUM(${level2RevenueColName}),0),${decimalPlace}) as '${DATA_COLUMNS[uCaseCalculationType + '_POCKET_MARGIN_PERCENT_BY_' + uCaseDiscountType]?.name}'"""
                        }
                        queryFragment = definition[attributeName]
                        break
                }
                return queryFragment
            },
            getMarkupDiscountContributionQuery     : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.MARKUP, Constants.PRICING_TYPES.MARKUP)
            },
            getNetPriceDiscountContributionQuery   : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.NET_PRICE, Constants.PRICING_TYPES.NET_PRICE)
            },
            getAbsoluteDiscountContributionQuery   : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.ABSOULTE_DISCOUNT, Constants.PRICING_TYPES.ABSOULTE_DISCOUNT)
            },
            getPercentDiscountContributionQuery    : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.PER_DISCOUNT, Constants.PRICING_TYPES.PER_DISCOUNT)
            },
            getXArticleContributionQuery           : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.X_ARTICLE, Constants.PRICING_TYPES.X_ARTICLE)
            },
            getInternalContributionQuery           : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.INTERNAL, Constants.PRICING_TYPES.INTERNAL)
            },
            getNonDiscountContributionQuery        : { String builderContext, String calculationType ->
                return methods.getContributionQuery(builderContext, calculationType, Constants.DISCOUNT_TYPES.NO_DISCOUNT, Constants.PRICING_TYPES.NO_DISCOUNT)
            },
            getGrossPriceChangePercent             : {
                return methods.hasExceptions() ? "COALESCE(SIMULATION_DATA.ExceptionGrossPriceChangePercent,${definition.grossPriceChangePercent})" : definition.grossPriceChangePercent
            },
            getLandingPriceChangePercent           : {
                return methods.hasExceptions() ? "COALESCE(SIMULATION_DATA.ExceptionPurchasePriceChangePercent,${definition.landingPriceChangePercent})" : definition.landingPriceChangePercent
            },
            getQuantityChangePercent               : {
                return methods.hasExceptions() ? "COALESCE(SIMULATION_DATA.ExceptionQuantityChangePercent,${definition.quantityChangePercent})" : definition.quantityChangePercent
            },
            getPriceChangeFragment                 : { String builderContext ->
                String queryFragment = ""
                def grossPriceChangePercent = methods.getGrossPriceChangePercent()
                def landingPriceChangePercent = methods.getLandingPriceChangePercent()
                def quantityChangePercent = methods.getQuantityChangePercent()
                if (grossPriceChangePercent == null && landingPriceChangePercent == null && quantityChangePercent == null) {
                    return queryFragment
                }
                switch (builderContext) {
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL3:
                        String expectedBaseCostExpr = methods.getExpectedBaseCostExpr()
                        String expectedQuantityExpr = methods.getExpectedQuantityExpr()
                        queryFragment = """     ,
                                                ${grossPriceChangePercent}                                                                                                              as GrossPriceChangePercent,  
                                                ${landingPriceChangePercent}                                                                                                            as LandingPriceChangePercent,
                                                ${quantityChangePercent}                                                                                                                as QuantityChangePercent,
                                                ${expectedBaseCostExpr}                                                                                                                 as ExpectedBaseCost,
                                                ${expectedQuantityExpr}                                                                                                                 as ExpectedQuantity,
                                                ${methods.getExpectedGrossPriceExpr()}                                                                                                  as TmpGrossPrice,              
                                                (CASE
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.MARKUP, Constants.CALCULATION_TYPES.TEMPORARY)}
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.NET_PRICE, Constants.CALCULATION_TYPES.TEMPORARY)} 
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.ABSOULTE_DISCOUNT, Constants.CALCULATION_TYPES.TEMPORARY)} 
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.PER_DISCOUNT, Constants.CALCULATION_TYPES.TEMPORARY)}
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.INTERNAL, Constants.CALCULATION_TYPES.TEMPORARY)} 
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.X_ARTICLE, Constants.CALCULATION_TYPES.TEMPORARY)}
                                                        ${methods.getPocketPriceExprByDiscountType(Constants.PRICING_TYPES.NO_DISCOUNT, Constants.CALCULATION_TYPES.TEMPORARY)}    
                                                ELSE 
                                                        ${methods.getExpectedGrossPriceExprForPocketPrice()}
                                                END)                                                                                                        as TmpPocketPrice,
                                                ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)},
                                                ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL3, Constants.CALCULATION_TYPES.TEMPORARY)}"""
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                        queryFragment = """ ,
                                        AVG(GrossPriceChangePercent)                                                                    as AvgGrossPriceChangePercent,
                                        AVG(LandingPriceChangePercent)                                                                  as AvgLandingPriceChangePercent,
                                        AVG(QuantityChangePercent)                                                                      as AvgQuantityChangePercent,
                                        AVG(ExpectedGrossPrice)                                                                         as AvgExpectedGrossPrice,
                                        AVG(ExpectedBaseCost)                                                                           as AvgExpectedBaseCost,
                                        AVG(ExpectedQuantity)                                                                           as AvgExpectedQuantitySold,
                                        AVG(ExpectedBaseCost * QtySold)                                                        as TotalExpectedBaseCost,
                                        AVG((ExpectedGrossPrice - ExpectedBaseCost) / NULLIF(ExpectedBaseCost,0))                       as AvgExpectedMarkup,
                                        AVG(ExpectedGrossPrice - ExpectedBaseCost)                                                      as AvgExpectedMargin,                                
                                        AVG(ExpectedPocketPrice)                                                                        as AvgExpectedPocketPrice,
                                        AVG(ExpectedPocketPrice * ExpectedQuantity)                                                     as ExpectedRevenue,
                                        AVG((ExpectedGrossPrice * ExpectedQuantity) - (ExpectedBaseCost * QtySold))                                 as ExpectedMargin,
                                        AVG(ExpectedPocketPrice - ExpectedGrossPrice)                                                   as AvgExpectedDiscount,                                
                                        AVG(ExpectedPocketPrice - ExpectedBaseCost)                                                     as AvgExpectedPocketMargin,
                                        AVG((ExpectedPocketPrice * ExpectedQuantity) - (ExpectedBaseCost * QtySold))                                as ExpectedPocketMargin,
                                        ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)},
                                        ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.EXPECTED)}"""
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                        Map DECIMAL_PLACE = definition.decimalPlaceMap
                        Map DATA_COLUMNS = definition.dataColumns
                        String grossPriceColumn = definition.grossPriceColumn
                        String landingPriceColumn = definition.landingPriceColumn
                        queryFragment = """,
                                    AVG(AvgGrossPriceChangePercent)                                                 as '${DATA_COLUMNS.PRICE_CHANGE_PERCENT.name}',
                                    AVG(AvgLandingPriceChangePercent)                                               as '${DATA_COLUMNS.LANDING_PRICE_CHANGE_PERCENT.name}',
                                    AVG(AvgQuantityChangePercent)                                                   as '${DATA_COLUMNS.QTY_CHANGE_PERCENT.name}',
                                    AVG(AvgExpectedGrossPrice)                                                      as '${grossPriceColumn}',
                                    AVG(AvgExpectedDiscount)                                                        as '${DATA_COLUMNS.EXPECTED_DISCOUNT.name}',
                                    AVG(AvgExpectedPocketPrice)                                                     as '${DATA_COLUMNS.EXPECTED_POCKET_PRICE.name}',    
                                    ROUND(SUM(ExpectedRevenue), ${Constants.ROUNDING_DECIMAL_PLACES})               as '${DATA_COLUMNS.EXPECTED_REVENUE.name}',  
                                    AVG(AvgExpectedBaseCost)                                                        as '${landingPriceColumn}',   
                                    AVG(AvgExpectedMargin)                                                          as '${DATA_COLUMNS.EXPECTED_MARGIN_PER_UNIT.name}',    
                                    SUM(ExpectedMargin)                                                             as '${DATA_COLUMNS.EXPECTED_MARGIN.name}',                                        
                                    AVG(AvgPocketMargin)                                                            as '${DATA_COLUMNS.POCKET_MARGIN.name}',
                                    AVG(AvgExpectedPocketMargin)                                                    as '${DATA_COLUMNS.EXPECTED_POCKET_MARGIN_PER_UNIT.name}',
                                    AVG(AvgExpectedPocketPrice - AvgBaseCost)/NULLIF(AVG(AvgExpectedPocketPrice),0) as '${DATA_COLUMNS.EXPECTED_POCKET_MARGIN_PERCENT.name}',
                                    SUM(AvgExpectedQuantitySold)                                                    as '${DATA_COLUMNS.TOTAL_EXPECTED_QUANTITY.name}',
                                    SUM(ExpectedPocketMargin)                                                       as '${DATA_COLUMNS.EXPECTED_POCKET_MARGIN.name}',
                                    SUM(TotalExpectedBaseCost)                                                      as '${DATA_COLUMNS.TOTAL_EXPECTED_LANDING_COST.name}',
                                    ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)},
                                    ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.EXPECTED)}"""
                }
                return queryFragment
            },
            getMarginChangeFragment                : { String builderContext ->
                String queryFragment = ""
                BigDecimal recommendedGrossMargin = definition.recommendedGrossMargin
                if (recommendedGrossMargin == null) {
                    return queryFragment
                }
                switch (builderContext) {
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL3:
                        String recommendedGrossPriceExpr = methods.getRecommendedGrossPriceExpr()
                        String recommendedBaseCostExpr = methods.getRecommendedBaseCostExpr()
                        queryFragment = """
                                        ,
                                        ${recommendedGrossMargin}                                                   as RecMargin,
                                        (CASE WHEN PricingType = '${Constants.PRICING_TYPES.PER_DISCOUNT}' THEN ${recommendedGrossPriceExpr}/(1 + KBETR)
                                              ELSE ${recommendedGrossPriceExpr}
                                        END)                                                                        as RecGrossPrice,    
                                        ${recommendedGrossPriceExpr}                                                as RecPocketPrice,
                                        ${recommendedBaseCostExpr}                                                  as RecBaseCost                            
                            """
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                        queryFragment = """,
                                        AVG(RecMargin)                                                                          as AvgRecMargin,
                                        AVG(RecGrossPrice)                                                                      as AvgRecGrossPrice,
                                        AVG(RecPocketPrice)                                                                     as AvgRecPocketPrice,
                                        AVG(RecPocketPrice * QtySold)                                                           as AvgRecRevenue,
                                        AVG((RecPocketPrice * QtySold) - TotalRevenue)                                          as AvgRecRevenueImpact,
                                        AVG((RecPocketPrice - RecBaseCost) * QtySold)                                           as AvgRecPocketMargin,
                                        AVG(TotalRevenue / NULLIF(RecPocketPrice,0))                                            as AvgRecQty"""
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                        Map DECIMAL_PLACE = definition.decimalPlaceMap
                        Map DATA_COLUMNS = definition.dataColumns
                        queryFragment = """,
                                    AVG(AvgRecMargin)               as '${DATA_COLUMNS.RECOMMENDED_MARGIN_PERCENT.name}',
                                    AVG(AvgRecGrossPrice)           as '${DATA_COLUMNS.RECOMMENDED_GROSS_PRICE.name}',
                                    AVG(AvgRecPocketPrice)          as '${DATA_COLUMNS.RECOMMENDED_POCKET_PRICE.name}',
                                    SUM(AvgRecRevenue)              as '${DATA_COLUMNS.REVENUE_USING_RECOMMENDED_PRICE.name}',
                                    SUM(AvgRecRevenueImpact)        as '${DATA_COLUMNS.REVENUE_DELTA_WITH_RECOMMENDED_PRICE.name}',                                    
                                    SUM(AvgRecQty)                  as '${DATA_COLUMNS.EXPECTED_QTY_WITH_RECOMMENDED_PRICE.name}',
                                    SUM(AvgRecPocketMargin)         as '${DATA_COLUMNS.POCKET_MARGIN_USING_RECOMMENDED_PRICE.name}'"""
                        break
                }
                return queryFragment
            },
            getBreakupByDiscountTypeFragment       : { String builderContext ->
                String queryFragment = ""
                if (!definition.showBreakupByDiscountType) {
                    return queryFragment
                }
                switch (builderContext) {
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL3:
                        String recommendedGrossPriceExpr = methods.getRecommendedGrossPriceExpr()
                        String recommendedBaseCostExpr = methods.getRecommendedBaseCostExpr()
                        queryFragment = """
                                        ,
                                        ${recommendedGrossMargin}                                                       as RecMargin,
                                        (CASE WHEN PricingType = '${Constants.PRICING_TYPES.PER_DISCOUNT}' THEN ${recommendedGrossPriceExpr}/(1 + KBETR)
                                              ELSE ${recommendedGrossPriceExpr}
                                        END)                                                                            as RecGrossPrice,    
                                        ${recommendedGrossPriceExpr}                                                    as RecPocketPrice,
                                        ${recommendedBaseCostExpr}                                                      as RecBaseCost                            
                            """
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL2:
                        queryFragment = """,
                                        AVG(RecMargin)                                                                  as AvgRecMargin,
                                        AVG(RecGrossPrice)                                                              as AvgRecGrossPrice,
                                        AVG(RecPocketPrice)                                                             as AvgRecPocketPrice,
                                        AVG(RecPocketPrice * QtySold)                                                   as AvgRecRevenue,
                                        AVG((RecPocketPrice * QtySold) - TotalRevenue)                                  as AvgRecRevenueImpact,
                                        AVG((RecPocketPrice - RecBaseCost) * QtySold)                                   as AvgRecPocketMargin,
                                        AVG(TotalRevenue / NULLIF(RecPocketPrice,0))                                    as AvgRecQty"""
                        break
                    case Constants.QUERY_BUILDER_CONTEXT.LEVEL1:
                        Map DECIMAL_PLACE = definition.decimalPlaceMap
                        Map DATA_COLUMNS = definition.dataColumns
                        queryFragment = """,
                                    (AVG(AvgRecMargin) * 100) || ' %'               as '${DATA_COLUMNS.RECOMMENDED_MARGIN_PERCENT.name}',
                                    AVG(AvgRecGrossPrice)                           as '${DATA_COLUMNS.RECOMMENDED_GROSS_PRICE.name}',
                                    AVG(AvgRecPocketPrice)                          as '${DATA_COLUMNS.RECOMMENDED_POCKET_PRICE.name}',
                                    SUM(AvgRecRevenue)                              as '${DATA_COLUMNS.REVENUE_USING_RECOMMENDED_PRICE.name}',
                                    SUM(AvgRecRevenueImpact)                        as '${DATA_COLUMNS.REVENUE_DELTA_WITH_RECOMMENDED_PRICE.name}',                                    
                                    SUM(AvgRecQty)                                  as '${DATA_COLUMNS.EXPECTED_QTY_WITH_RECOMMENDED_PRICE.name}',
                                    SUM(AvgRecPocketMargin)                         as '${DATA_COLUMNS.POCKET_MARGIN_USING_RECOMMENDED_PRICE.name}'"""
                        break
                }
                return queryFragment
            },
            getCalculatePerRowSQLQuery             : {
                String grossPriceChangePercent = methods.getGrossPriceChangePercent()
                String landingPriceChangePercent = methods.getLandingPriceChangePercent()
                String quantityChangePercent = methods.getQuantityChangePercent()
                int decimalPlace = Constants.ROUNDING_DECIMAL_PLACES
                String exceptionQuery = methods.hasExceptions() ?
                        """
                                                             LEFT JOIN (
                                                                ${methods.getExceptionQueryString()}
                                                             ) SIMULATION_DATA
                                                             ON T1.Material = SIMULATION_DATA.ExceptionMaterial   
                        """ : ""
                definition.calculatePerRowSQLQuery = """
                                                SELECT
                                                        *,
                                                        HistoryPocketPrice  * ChangeRatio                                                                                       as ExpectedPocketPrice,
                                                        cur_MarkupDiscountRevenueContribution  * ChangeRatio                                                                    as exp_MarkupDiscountRevenueContribution,
                                                        cur_NetPriceDiscountRevenueContribution  * ChangeRatio                                                                  as exp_NetPriceDiscountRevenueContribution,
                                                        cur_AbsoluteDiscountRevenueContribution  * ChangeRatio	                                                                as exp_AbsoluteDiscountRevenueContribution,
                                                        cur_PercentDiscountRevenueContribution  * ChangeRatio		                                                            as exp_PercentDiscountRevenueContribution,
                                                        cur_XARTICLEDiscountRevenueContribution  * ChangeRatio	                                                                as exp_XARTICLEDiscountRevenueContribution,
                                                        cur_INTERNALDiscountRevenueContribution  * ChangeRatio	                                                                as exp_INTERNALDiscountRevenueContribution,
                                                        cur_NoDiscountsDiscountRevenueContribution  * ChangeRatio                                                               as exp_NoDiscountsDiscountRevenueContribution,
                                                        TmpGrossPrice  * ChangeRatio																		                    as ExpectedGrossPrice,
                                                        HistoryPocketMargin                                                                                                     as PocketMargin,
                                                        (HistoryPocketMargin / NULLIF(PocketPrice,0))                                                                           as PocketMarginPercent
                                                FROM (
                                                            SELECT
                                                                Material                                                                                                        as Material,
                                                                ProdhII                                                                                                         as ProdhII,
                                                                ProdhIII                                                                                                        as ProdhIII,
                                                                prodh_cm1                                                                                                       as prodh_cm1,
                                                                prodh_cm2                                                                                                       as prodh_cm2,
                                                                SecondaryKey                                                                                                    as SecondaryKey,
                                                                CustomerId                                                                                                      as CustomerId,
                                                                ConditionName                                                                                                   as ConditionName,
                                                                PricingType                                                                                                     as PricingType,
                                                                TotalRevenue                                                                                                    as ActualRevenue,
                                                                coalesce(Material,'-') || '-' || coalesce(CustomerId,'-') || '-' || coalesce(Year,'-')                          as TxnKey,
                                                                AdjustedRevenue                                                                                                 as TotalRevenue,
                                                                AdjustedQtySold                                                                                                 as QtySold,
                                                                CUST_DATA.CustomerPocketMargin                                                                                  as CustomerPocketMargin,
                                                                AvgGrossPrice                                                                                                   as AvgGrossPrice,
                                                                GrossPrice                                                                                                      as GrossPrice,
                                                                ZPL                                                                                                             as ZPL,
                                                                KBETR                                                                                                           as KBETR,
                                                                BaseCost                                                                                                        as BaseCost,
                                                                Margin                                                                                                          as Margin,
                                                                PocketPrice                                                                                                     as PocketPrice,
                                                                GrossPriceChangePercent                                                                                         as GrossPriceChangePercent,
                                                                LandingPriceChangePercent                                                                                       as LandingPriceChangePercent,
                                                                QuantityChangePercent                                                                                           as QuantityChangePercent,  
                                                                ExpectedBaseCost                                                                                                as ExpectedBaseCost,
                                                                ExpectedQuantity                                                                                                as ExpectedQuantity,
                                                                TmpGrossPrice                                                                                                   as TmpGrossPrice,
                                                                HistoricalPocketPrice                                                                                           as HistoricalPocketPrice,              
                                                                TmpSpecialGrossPrice                                                                                            as TmpSpecialGrossPrice,
                                                                TmpPocketPrice                                                                                                  as TmpPocketPrice,
                                                                (CASE WHEN PricingType = 'Markup' THEN TmpPocketPrice ELSE 0 END)                                               as tem_MarkupDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'Net Price' THEN TmpPocketPrice ELSE 0 END)                                            as tem_NetPriceDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'Absolute Discount' THEN TmpPocketPrice ELSE 0 END)                                    as tem_AbsoluteDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'Per Discount' THEN TmpPocketPrice ELSE 0 END)                                         as tem_PercentDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'XARTICLE' THEN TmpPocketPrice ELSE 0 END)                                             as tem_XARTICLEDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'INTERNAL' THEN TmpPocketPrice ELSE 0 END)                                             as tem_INTERNALDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'No Discounts' THEN TmpPocketPrice ELSE 0 END)                                         as tem_NoDiscountsDiscountRevenueContribution
                                                            FROM T1
                                                            LEFT JOIN (
                                                                SELECT
                                                                    Material                                                                                                    as CustomerMaterial,
                                                                    SUM((CASE WHEN CustomerId IS NOT NULL THEN QtySold ELSE 0 END))                                             as CustomerQtySold,
                                                                    SUM((CASE WHEN CustomerId IS NOT NULL THEN TotalRevenue ELSE 0 END))                                        as CustomerRevenue,
                                                                    SUM((CASE WHEN CustomerId IS NOT NULL THEN PocketMargin ELSE 0 END))                                        as CustomerPocketMargin
                                                                FROM (
                                                                        SELECT 
                                                                                T1.*, 
                                                                                ROW_NUMBER() OVER (PARTITION BY Material,CustomerId ORDER BY CustomerId) rownum
                                                                        FROM T1
                                                                ) T1
                                                                WHERE
                                                                    rownum = 1
                                                                GROUP BY
                                                                    Material
                                                            ) CUST_DATA
                                                            ON T1.Material = CUST_DATA.CustomerMaterial
                                                            ${exceptionQuery}       
                                                            JOIN LATERAL
                                                            (
                                                                SELECT
                                                                    (CASE WHEN CustomerId IS NULL THEN T1.TotalRevenue - CUST_DATA.CustomerRevenue ELSE T1.TotalRevenue END)    as AdjustedRevenue,
                                                                    (CASE WHEN CustomerId IS NULL THEN T1.QtySold - CUST_DATA.CustomerQtySold ELSE T1.QtySold END)              as AdjustedQtySold,
                                                                    ${grossPriceChangePercent}                                                                                  as GrossPriceChangePercent,
                                                                    ${landingPriceChangePercent}                                                                                as LandingPriceChangePercent,
                                                                    ${quantityChangePercent}                                                                                    as QuantityChangePercent                                                                                                             
                                                            ) TblRevenueAndQty 
                                                            JOIN LATERAL
                                                            (
                                                                SELECT
                                                                    (BaseCost + (BaseCost * LandingPriceChangePercent))                                                         as ExpectedBaseCost,
                                                                    (AdjustedQtySold + (AdjustedQtySold * QuantityChangePercent))                                               as ExpectedQuantity,
                                                                    (GrossPrice + (GrossPrice * GrossPriceChangePercent))                                                       as TmpGrossPrice,
                                                                    ROUND(AdjustedRevenue / NULLIF(AdjustedQtySold,0),${decimalPlace})                                                       as HistoricalPocketPrice                
                                                            ) TblExpected   
                                                            JOIN LATERAL 
                                                            (
                                                                SELECT
                                                                    (
                                                                        CASE
                                                                            WHEN ConditionName = 'ZRI' AND ConditionTable = 'A617' AND ZPL IS NOT NULL THEN ZPL - (ZPL * GrossPriceChangePercent)
                                                                            ELSE TmpGrossPrice
                                                                        END
                                                                    )                                                                                                           as TmpSpecialGrossPrice
                                                            )  TblSpecial  
                                                            JOIN LATERAL 
                                                            (
                                                                SELECT
                                                                    (
                                                                        CASE
                                                                            WHEN PricingType = 'Markup' THEN ExpectedBaseCost * (1 + KBETR)
                                                                            WHEN PricingType = 'Net Price' THEN KBETR
                                                                            WHEN PricingType = 'Absolute Discount' THEN TmpSpecialGrossPrice - KBETR                               
                                                                            WHEN PricingType = 'Per Discount' THEN TmpSpecialGrossPrice * (1 + KBETR)
                                                                            WHEN PricingType = 'INTERNAL' THEN HistoricalPocketPrice
                                                                            WHEN PricingType = 'XARTICLE' THEN HistoricalPocketPrice
                                                                            WHEN PricingType = 'No Discounts' THEN TmpSpecialGrossPrice
                                                                            ELSE TmpSpecialGrossPrice
                                                                        END
                                                                    )                                                                                                            as TmpPocketPrice                                                                                                                                          
                                                            )  TblPocketPrice 
                                                        )
                                                        JOIN LATERAL
                                                        (
                                                                SELECT
                                                                    ABS(TmpPocketPrice / NULLIF(PocketPrice,0))                                                                 as ChangeRatio,
                                                                    ROUND((TotalRevenue / NULLIF(QtySold,0)),${decimalPlace})                                                   as HistoryPocketPrice,
                                                                    ((TotalRevenue / NULLIF(QtySold,0)) - BaseCost)                                                             as HistoryPocketMargin
                                                        ) TblRatio
                                                        JOIN LATERAL
                                                        (
                                                              SELECT
                                                                (CASE WHEN PricingType = 'Markup' THEN HistoryPocketPrice ELSE 0 END)                                           as cur_MarkupDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'Net Price' THEN HistoryPocketPrice ELSE 0 END)                                        as cur_NetPriceDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'Absolute Discount' THEN HistoryPocketPrice ELSE 0 END)                                as cur_AbsoluteDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'Per Discount' THEN HistoryPocketPrice ELSE 0 END)                                     as cur_PercentDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'XARTICLE' THEN HistoryPocketPrice ELSE 0 END)                                         as cur_XARTICLEDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'INTERNAL' THEN HistoryPocketPrice ELSE 0 END)                                         as cur_INTERNALDiscountRevenueContribution,
                                                                (CASE WHEN PricingType = 'No Discounts' THEN HistoryPocketPrice ELSE 0 END)                                     as cur_NoDiscountsDiscountRevenueContribution
                                                        ) TblBreakup
                                                """
            },
            getAggregateByMaterialTxnSQLQuery      : {
                String priceChangeQueryFragement = methods.getPriceChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL2)
                String marginChangeQueryFragment = methods.getMarginChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL2)
                definition.aggregateByMaterialTxnSQLQuery = """ 
                                    SELECT 
                                        Material,
                                        ProdhII,
                                        ProdhIII,
                                        prodh_cm1,
                                        prodh_cm2,
                                        TxnKey,   
                                        COUNT(TxnKey)                                                                                               as TxnCount,                             
                                        AVG(GrossPrice)                                                                                             as AverageGrossPrice,       
                                        AVG(PocketPrice - GrossPrice)                                                                               as AverageDiscount,
                                        AVG(BaseCost)                                                                                               as AvgBaseCost,
                                        AVG(BaseCost * QtySold)                                                                                     as TotalBaseCost,
                                        AVG(Margin)                                                                                                 as AvgMargin,
                                        AVG(PocketPrice)                                                                                            as AvgPocketPrice,
                                        AVG(PocketMargin)                                                                                           as AvgPocketMargin,
                                        AVG(PocketPrice * QtySold)                                                                                  as CurrentRevenue,
                                        AVG(Margin * QtySold)                                                                                       as CurrentMargin,                             
                                        AVG(QtySold)                                                                                                as TotalQuantitySold,
                                        AVG(TotalRevenue)                                                                                           as TotalRevenue,
                                        AVG(PocketMargin * QtySold)                                                                                 as CurrentPocketMargin,
                                        ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)},
                                        ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL2, Constants.CALCULATION_TYPES.CURRENT)}
                                        ${priceChangeQueryFragement}
                                        ${marginChangeQueryFragment}
                                    FROM ( ${methods.getCalculatePerRowSQLQuery()} )  LEVEL2 GROUP BY Material,TxnKey, ProdhII, ProdhIII, prodh_cm1, prodh_cm2
                        """
                return definition.aggregateByMaterialTxnSQLQuery
            },
            getSQLQuery                            : {
                Map DECIMAL_PLACE = definition.decimalPlaceMap
                Map DATA_COLUMNS = definition.dataColumns
                String priceChangeQueryFragement = methods.getPriceChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL1)
                String marginChangeQueryFragment = methods.getMarginChangeFragment(Constants.QUERY_BUILDER_CONTEXT.LEVEL1)
                return """ 
                                SELECT 
                                    Material                                                            as '${DATA_COLUMNS.ARTICLE_NUMBER.name}',
                                    ProdhII                                                             as '${DATA_COLUMNS.PRODH_II.name}',
                                    ProdhIII                                                            as '${DATA_COLUMNS.PRODH_III.name}',
                                    prodh_cm1                                                           as '${DATA_COLUMNS.PRODH_CM1.name}',
                                    prodh_cm2                                                           as '${DATA_COLUMNS.PRODH_CM2.name}',
                                    SUM(TxnCount)                                                       as '${DATA_COLUMNS.TOTAL_TRANSACTIONS.name}',                 
                                    SUM(TotalQuantitySold)                                              as '${DATA_COLUMNS.TOTAL_QTY_SOLD.name}',
                                    ROUND(SUM(TotalRevenue), ${Constants.ROUNDING_DECIMAL_PLACES})      as '${DATA_COLUMNS.TOTAL_REVENUE.name}',                        
                                    AVG(AverageGrossPrice)                                              as '${DATA_COLUMNS.GROSS_PRICE.name}',
                                    AVG(AverageDiscount)                                                as '${DATA_COLUMNS.DISCOUNT.name}',
                                    AVG(AvgPocketPrice)                                                 as '${DATA_COLUMNS.POCKET_PRICE.name}',  
                                    ROUND(SUM(TotalRevenue), ${Constants.ROUNDING_DECIMAL_PLACES})      as '${DATA_COLUMNS.CURRENT_REVENUE.name}',
                                    AVG(AvgBaseCost)                                                    as '${DATA_COLUMNS.LANDING_COST.name}',
                                    AVG(AvgMargin)                                                      as '${DATA_COLUMNS.MARGIN.name}',            
                                    SUM(CurrentMargin)                                                  as '${DATA_COLUMNS.CURRENT_MARGIN.name}',                            
                                    AVG(AvgPocketMargin)                                                as '${DATA_COLUMNS.POCKET_MARGIN.name}',
                                    AVG(AvgPocketPrice - AvgBaseCost)/NULLIF(AVG(AvgPocketPrice),0)     as '${DATA_COLUMNS.POCKET_MARGIN_PERCENT.name}',
                                    SUM(CurrentPocketMargin)                                            as '${DATA_COLUMNS.CURRENT_POCKET_MARGIN.name}',
                                    SUM(TotalBaseCost)                                                  as '${DATA_COLUMNS.TOTAL_LANDING_COST.name}',
                                    ${methods.getMarkupDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getNetPriceDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getAbsoluteDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getPercentDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getXArticleContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getInternalContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)},
                                    ${methods.getNonDiscountContributionQuery(Constants.QUERY_BUILDER_CONTEXT.LEVEL1, Constants.CALCULATION_TYPES.CURRENT)}
                                    ${priceChangeQueryFragement}    
                                    ${marginChangeQueryFragment}                                     
                                FROM 
                                 (  
                                    ${methods.getAggregateByMaterialTxnSQLQuery()}
                                 )  LEVEL1 GROUP BY Material, ProdhII, ProdhIII, prodh_cm1, prodh_cm2
                   """
            }
    ]
    return methods
}