List filters = []

def dmFilter = out.DMFilter
if (dmFilter) {
    filters.add(dmFilter)
}

filters.add(Filter.equal(Constants.DM_COLUMNS.YEAR, out.Year))
//filters.add(Filter.equal('Material','433.33.320'))
//filters.add(Filter.in('Material', '000.10.280', '000.30.210', '342.42.6601', '342.42.663', '833.76.342', '924.01.2031'))
//filters.add(Filter.in('Material','356.04.312','356.04.512','356.04.712','637.76.3331'))

if (out.IncludeRowsWithQty) {
    filters.add(Filter.and(Filter.isNotNull(Constants.DM_COLUMNS.QUANTITY), Filter.isNotEmpty(Constants.DM_COLUMNS.QUANTITY)))
}

return filters