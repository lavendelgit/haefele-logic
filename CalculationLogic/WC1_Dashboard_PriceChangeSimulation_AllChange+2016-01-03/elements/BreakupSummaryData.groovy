if (!api.local.SummaryData) {
    return
}

def htmlPortlet = api.newController()
Map summaryData = api.local.SummaryData ?: [:]
StringBuilder htmlBuilder = new StringBuilder()
//htmlBuilder.append(getHeaderString())
htmlBuilder.append("<div style='overflow-x: auto;'>")
htmlBuilder.append(generateSection(summaryData, 'REVENUE BREAKUP', 'actual v/s expected', 'totalRevenue', 'Revenue Leakage', 48))
htmlBuilder.append(generateSection(summaryData, 'MARGIN BREAKUP', 'actual v/s expected', 'totalMargin', 'Margin Leakage', 48))
htmlBuilder.append("</div>")
htmlPortlet.addHTML(htmlBuilder.toString())
addCalculatedEKWertDataToSummaryMap()
return htmlPortlet

protected String getHeaderString() {
    return '<h2>DISCOUNT-TYPE BREAKUP SUMMARY</h2>'
}


protected String generateSection(Map summaryData, String title, String subtitle, String fieldName, String leakageColumnHeader, int width) {
    api.trace("summaryData", summaryData)
    Map currentMap = summaryData.CURRENT_BREAKUP
    Map expectedMap = summaryData.EXPECTED_BREAKUP
    if (currentMap && expectedMap) {
        Map COLORS = Constants.SUMMARY_COLORS
        BigDecimal currentTotal = 0
        BigDecimal expectedTotal = 0
        BigDecimal idealDeltaTotal = 0
        BigDecimal totalValue = Constants.DISCOUNT_TYPES.collect { String discountTypeCode, String discountTypeName ->
            currentMap[discountTypeName][fieldName] ?: BigDecimal.ZERO
        }?.sum()


        StringBuilder htmlBuilder = new StringBuilder()
        htmlBuilder.append("<div style='float: left;width: ${width}%;padding: 5px;height: 100%;margin: 3px;background-color:${COLORS.BG_COLOR};'>")
        htmlBuilder.append("<h3 style='color:${COLORS.TITLE}'>${title} ( ${subtitle} )</h3>")
        htmlBuilder.append("<table style='border: 2px solid #FFFFFF;width: 100%;text-align: center;border-collapse: collapse;'>")
        addHeader(htmlBuilder, leakageColumnHeader)
        Constants.DISCOUNT_TYPES.each { String discountTypeCode, String discountTypeName ->
            BigDecimal current = currentMap[discountTypeName][fieldName] ?: BigDecimal.ZERO
            BigDecimal expected = expectedMap[discountTypeName][fieldName] ?: BigDecimal.ZERO
            BigDecimal idealDelta = expectedMap[discountTypeName][fieldName + "IdealDelta"] ?: BigDecimal.ZERO
            addRow(htmlBuilder, discountTypeCode, current, expected, idealDelta, totalValue)
            currentTotal += current
            expectedTotal += expected
            idealDeltaTotal += idealDelta
        }

        addBreakupSummaryDataToGlobalSummaryMap(title, currentTotal, expectedTotal)

        addSummary(htmlBuilder, currentTotal, expectedTotal, idealDeltaTotal)
        htmlBuilder.append("</table>")
        htmlBuilder.append("</div>")        

        return htmlBuilder.toString()
    }
}

protected void addBreakupSummaryDataToGlobalSummaryMap(String title, BigDecimal currentTotal, BigDecimal expectedTotal) {
    if (title == 'REVENUE BREAKUP') {
        api.local.SummaryData.Revenue.actualFromBreakUp = currentTotal
        api.local.SummaryData.Revenue.expectedFromBreakUp = expectedTotal
        api.local.SummaryData.Revenue.actualInMillionFromBreakUp = toMillion(currentTotal)
        api.local.SummaryData.Revenue.expectedInMillionFromBreakUp = toMillion(expectedTotal)

    }
    if (title == 'MARGIN BREAKUP') {
        api.local.SummaryData.PocketMargin.actualFromBreakUp = currentTotal
        api.local.SummaryData.PocketMargin.expectedFromBreakUp = expectedTotal
        api.local.SummaryData.PocketMargin.actualInMillionFromBreakUp = toMillion(currentTotal)
        api.local.SummaryData.PocketMargin.expectedInMillionFromBreakUp = toMillion(expectedTotal)
    }
}


protected void addCalculatedEKWertDataToSummaryMap() {
    BigDecimal actualTotalRevenue = api.local.SummaryData.Revenue.actualFromBreakUp
    BigDecimal expectedTotalRevenue = api.local.SummaryData.Revenue.expectedFromBreakUp
    BigDecimal actualTotalMargin = api.local.SummaryData.PocketMargin.actualFromBreakUp
    BigDecimal expectedTotalMargin = api.local.SummaryData.PocketMargin.expectedFromBreakUp
  
    BigDecimal actualEKWert = actualTotalRevenue - actualTotalMargin
    BigDecimal expectedEKWert = expectedTotalRevenue - expectedTotalMargin
  
  	api.local.SummaryData.Cost.actualFromBreakUp = actualEKWert?:0
    api.local.SummaryData.Cost.expectedFromBreakUp = expectedEKWert?:0

    api.local.SummaryData.Cost.actualInMillionFromBreakUp = toMillion(actualEKWert?:0)
    api.local.SummaryData.Cost.expectedInMillionFromBreakUp = toMillion(expectedEKWert?:0)
}

protected void addHeader(StringBuilder htmlBuilder, String leakageColumnHeader) {
    Map COLORS = Constants.SUMMARY_COLORS
    String rowStyle = "font-size: 11px;font-weight: bold;color: ${COLORS.LABEL};text-align: center;padding: 3px 4px;"
    htmlBuilder.append("<thead style='border: 1px solid #FFFFFF;padding: 3px 4px;border-bottom: 4px solid ${COLORS.LABEL};'><tr>")
    htmlBuilder.append("<td style='${rowStyle}'>Discount Type</td>")
    htmlBuilder.append("<td style='${rowStyle}'>Contrib. %</td>")
    htmlBuilder.append("<td style='${rowStyle}'>Actual</td>")
    htmlBuilder.append("<td style='${rowStyle}'>Expected</td>")
    htmlBuilder.append("<td style='${rowStyle}'>Delta</td>")
    htmlBuilder.append("<td style='${rowStyle}'>${leakageColumnHeader}</td>")
    htmlBuilder.append("</tr></thead>")
}

protected void addRow(StringBuilder htmlBuilder, String discountTypeCode, BigDecimal current, BigDecimal expected, BigDecimal idealDelta, BigDecimal totalValue) {
    def roundingUtils = libs.SharedLib.RoundingUtils
    Map COLORS = Constants.SUMMARY_COLORS
    String rowStyle = "background: ${COLORS.BG_COLOR};font-size: 12px;border: 1px solid #FFFFFF;padding: 3px 4px;color: ${COLORS.VALUE};"
    htmlBuilder.append("<tr>")
    htmlBuilder.append("<td style='${rowStyle};text-align: left;font-weight:bold'>").append(Constants.PRICING_TYPE_LABELS[discountTypeCode]).append("</td>")
    htmlBuilder.append("<td style='${rowStyle};text-align: right'>").append(getPercent(current, totalValue)).append("</td>")
    htmlBuilder.append("<td style='${rowStyle};text-align: right'>").append(Library.formatMoney(current)).append("</td>")
    htmlBuilder.append("<td style='${rowStyle};text-align: right'>").append(Library.formatMoney(expected)).append("</td>")
    htmlBuilder.append("<td style='${rowStyle};text-align: right' nowrap>").append(Library.formatMoney(expected - current)).append(" (").append(getChangePercent(current, expected)).append("%)</td>")
    htmlBuilder.append("<td style='${rowStyle};text-align: right' nowrap>").append(Library.formatMoney(idealDelta)).append(getIndicator(idealDelta)).append(getPercent(idealDelta, current)).append("</td>")
    htmlBuilder.append("</tr>")
}


protected void addSummary(StringBuilder htmlBuilder, BigDecimal currentTotal, BigDecimal expectedTotal, BigDecimal idealDeltaTotal) {
    def roundingUtils = libs.SharedLib.RoundingUtils
    Map COLORS = Constants.SUMMARY_COLORS
    String rowStyle = "font-size: 12px;text-align: right;padding: 3px 4px;"
    //htmlBuilder.append("<tfoot style='font-size: 14px;font-weight: bold;color: ${COLORS.VALUE};border-top: 4px solid ${COLORS.LABEL};'>")
    htmlBuilder.append("<tr style='font-size: 14px;font-weight: bold;color: ${COLORS.VALUE};border-top: 4px solid ${COLORS.LABEL};'>")
    htmlBuilder.append("<td style='${rowStyle};text-align: left;'>SUMMARY</td>")
    htmlBuilder.append("<td style='${rowStyle}'>100 %</td>")
    htmlBuilder.append("<td style='${rowStyle}'>").append(Library.formatMoney(currentTotal)).append("</td>")
    htmlBuilder.append("<td style='${rowStyle}'>").append(Library.formatMoney(expectedTotal)).append("</td>")
    htmlBuilder.append("<td style='${rowStyle}' nowrap>").append(Library.formatMoney(expectedTotal - currentTotal)).append(" (").append(getChangePercent(currentTotal, expectedTotal)).append("%)</td>")
    htmlBuilder.append("<td style='${rowStyle}' nowrap>").append(Library.formatMoney(idealDeltaTotal)).append(getIndicator(idealDeltaTotal)).append(getPercent(idealDeltaTotal, currentTotal)).append("</td>")
    htmlBuilder.append("</tr>")
    //htmlBuilder.append("</tfoot>")
}

protected String getIndicator(BigDecimal value) {
    value = libs.SharedLib.RoundingUtils.round(value, 0)
    Map indicator = value == 0 ? Constants.INDICATOR.equal : ((value < 0) ? Constants.INDICATOR.negative : Constants.INDICATOR.positive)
    return "<span style='color:${indicator.textColor};font-weight:${indicator.weight}'>${indicator.value.prefix}</span>"
}

protected BigDecimal getChangePercent(BigDecimal current, BigDecimal expected) {
    if (current) {
        return libs.SharedLib.RoundingUtils.round(((expected - current) / current) * 100, Constants.DECIMAL_PLACE.PERCENT)
    }
    return 0
}

protected String getPercent(BigDecimal current, BigDecimal totalValue) {
    if (totalValue) {
        return Library.toPercent((current / Math.abs(totalValue)) * 100)
    }
    return '0%'
}

protected String toMillion(moneyValue) {
  return (moneyValue != null) ? libs.SharedLib.RoundingUtils.round((moneyValue / 1000000), Constants.DECIMAL_PLACE.MONEY) + ' M' : ''
}