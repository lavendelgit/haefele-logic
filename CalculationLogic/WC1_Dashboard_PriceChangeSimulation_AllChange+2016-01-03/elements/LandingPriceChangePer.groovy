BigDecimal changePercent = out.LandingPriceChangePercent
return (changePercent != null ? changePercent : Constants.DEFAULT_CHANGE_PERCENT.LANDING_PRICE) / 100