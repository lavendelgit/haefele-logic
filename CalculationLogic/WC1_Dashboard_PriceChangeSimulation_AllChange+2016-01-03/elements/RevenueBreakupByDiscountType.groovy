Map summaryData = api.local.SummaryData
if (!summaryData) {
    return
}


List chartData = summaryData.EXPECTED_BREAKUP.collect { String discountTypeName, Map dtSummaryData ->
    [name         : discountTypeName,
     y            : dtSummaryData.totalRevenue,
     totalRevenue : dtSummaryData.totalRevenue,
     totalMargin  : dtSummaryData.totalMargin,
     marginPercent: dtSummaryData.marginPercent]
}


return ChartUtils.geteratePieChart("Revenue Breakup By Discount Type", chartData, "Revenue")