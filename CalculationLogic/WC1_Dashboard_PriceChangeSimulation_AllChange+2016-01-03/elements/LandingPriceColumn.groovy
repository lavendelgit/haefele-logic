if (out.LandingPriceChangePer == null) {
    return ""
}
Map DATA_COLUMNS = Constants.DATA_COLUMNS_DEF
String landingPriceColumn = DATA_COLUMNS.EXPECTED_LANDING_COST.name
boolean isLandingPriceChange = !out.LandingPriceChangePer
landingPriceColumn += isLandingPriceChange ? " (with ${out.LandingPriceChangePer * 100} % change)" : ''

return landingPriceColumn