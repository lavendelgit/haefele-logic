BigDecimal changePercent = out.GrossPriceChangePercent
return (changePercent != null ? changePercent : Constants.DEFAULT_CHANGE_PERCENT.GROSS_PRICE) / 100