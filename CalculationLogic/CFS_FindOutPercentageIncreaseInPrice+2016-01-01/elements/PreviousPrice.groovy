String pxTableName = out.PxTable

List filters = []
String orderBy
BigDecimal previousPrice

List previousPriceRow = out.GetPreviousPriceRow

if (previousPriceRow) {

  switch (pxTableName) {

    case Constants.PX_ZPL_TABLE :
      previousPrice = previousPriceRow[0]?.attribute7
      break

    case Constants.PX_ZPLP_TABLE :
      previousPrice = previousPriceRow[0]?.attribute3
      break

    case Constants.PX_COMPOUND_PRICE_TABLE :
      previousPrice = previousPriceRow[0]?.attribute1
      break

    case Constants.PX_ZNRV_TABLE :
      previousPrice = previousPriceRow[0]?.attribute4
      break
    
    case Constants.PX_ZPE_TABLE :
      previousPrice = previousPriceRow[0]?.attribute3
      break
  }

  if (previousPrice) {
      return previousPrice
  }
}

return out.CurrentPrice