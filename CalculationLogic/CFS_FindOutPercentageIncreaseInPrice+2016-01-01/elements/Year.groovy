//return api.currentItem("attribute1")?.format("yyyy")
String pxTableName = out.PxTable

switch (pxTableName) {
  
  case Constants.PX_ZPL_TABLE :
  	return api.currentItem("attribute12")?.format("yyyy")
  
  case Constants.PX_ZPLP_TABLE :
  	return api.currentItem("attribute1")?.format("yyyy")
  
  case Constants.PX_COMPOUND_PRICE_TABLE :
  	return api.currentItem("attribute6")?.format("yyyy")
  
  case Constants.PX_ZNRV_TABLE :
  	return api.currentItem("attribute1")?.format("yyyy")
  
  case Constants.PX_ZPE_TABLE :
  	return api.currentItem("attribute1")?.format("yyyy")
  
}