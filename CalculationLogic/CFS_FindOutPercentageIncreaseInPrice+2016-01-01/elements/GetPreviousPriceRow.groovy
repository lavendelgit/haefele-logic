String pxTableName = out.PxTable

List filters = []
String orderBy
List previousPrices = []

switch (pxTableName) {
  
  case Constants.PX_ZPL_TABLE :
  	previousPrices = getPreviousPrice(out.SKU, out.PxTable, "attribute12", "-attribute12")
  	break
  
  case Constants.PX_ZPLP_TABLE :
  	previousPrices = getPreviousPrice(out.SKU, out.PxTable, "attribute1", "-attribute1")
  	break
  
  case Constants.PX_COMPOUND_PRICE_TABLE :
    previousPrices = getPreviousPrice(out.SKU, out.PxTable, "attribute6", "-attribute6")
    break
  
  case Constants.PX_ZNRV_TABLE :
  	previousPrices = getPreviousPrice(out.SKU, out.PxTable, "attribute1", "-attribute1")
  	break
  
  case Constants.PX_ZPE_TABLE :
  	previousPrices = getPreviousPrice(out.SKU, out.PxTable, "attribute1", "-attribute1")
  	break
}

if (previousPrices) {
	return previousPrices
}

List getPreviousPrice(String sku, String pxTableName, String validFromAttribute, String orderBy)
{
  List filters = [Filter.equal("sku",sku),
                Filter.lessThan("$validFromAttribute",out.ValidFromDate),
                 Filter.equal("name",pxTableName)]
  
  List previousPrices
  
  if (pxTableName == Constants.PX_ZPL_TABLE)
  	previousPrices = api.find("PX50", 0, 1, orderBy, *filters)
  else
    previousPrices = api.find("PX10", 0, 1, orderBy, *filters)
  
  
  return previousPrices
}