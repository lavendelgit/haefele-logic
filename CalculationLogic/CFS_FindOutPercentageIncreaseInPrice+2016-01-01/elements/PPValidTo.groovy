String pxTableName = out.PxTable

def previousPricevalidTodate

List previousPriceRow = out.GetPreviousPriceRow
switch (pxTableName) {

  case Constants.PX_ZNRV_TABLE :
  	previousPricevalidToDate = previousPriceRow ? (previousPriceRow[0]?.attribute2) : api.currentItem("attribute2")
  	break
  
  case Constants.PX_ZPE_TABLE :
  	previousPricevalidToDate = previousPriceRow ? (previousPriceRow[0]?.attribute2) : api.currentItem("attribute2")
  	break
  
}

return previousPricevalidToDate