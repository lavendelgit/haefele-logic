String pxTableName = out.PxTable

switch (pxTableName) {
  
  case Constants.PX_ZPL_TABLE :
  return api.currentItem("attribute7")
  
  case Constants.PX_ZPLP_TABLE :
  return api.currentItem("attribute3")
  
  case Constants.PX_COMPOUND_PRICE_TABLE :
  return api.currentItem("attribute1")
  
  case Constants.PX_ZNRV_TABLE :
  return api.currentItem("attribute4")
  
  case Constants.PX_ZPE_TABLE :
  return api.currentItem("attribute3")
  
}