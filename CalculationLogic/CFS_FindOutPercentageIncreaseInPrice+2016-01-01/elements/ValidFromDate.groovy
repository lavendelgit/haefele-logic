String pxTableName = out.PxTable

if (api.isDebugMode()) {
  	
    return ""
}

switch (pxTableName) {
  
  case Constants.PX_ZPL_TABLE :
  return api.currentItem("attribute12")?.format("yyyy-MM-dd")
  
  case Constants.PX_ZPLP_TABLE :
  return api.currentItem("attribute1")?.format("yyyy-MM-dd")
  
  case Constants.PX_COMPOUND_PRICE_TABLE :
  return api.currentItem("attribute6")?.format("yyyy-MM-dd")
  
  case Constants.PX_ZNRV_TABLE :
  return api.currentItem("attribute1")?.format("yyyy-MM-dd")
  
  case Constants.PX_ZPE_TABLE :
  return api.currentItem("attribute1")?.format("yyyy-MM-dd")
  
}

