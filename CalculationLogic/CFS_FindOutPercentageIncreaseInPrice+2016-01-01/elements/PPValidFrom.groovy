String pxTableName = out.PxTable

def previousPricevalidFromdate

List previousPriceRow = out.GetPreviousPriceRow

switch (pxTableName) {

  case Constants.PX_ZNRV_TABLE :
  	previousPricevalidFromdate = previousPriceRow ? (previousPriceRow[0]?.attribute1) : api.currentItem("attribute1")
  	break
  
  case Constants.PX_ZPE_TABLE :
  	previousPricevalidFromdate = previousPriceRow ? (previousPriceRow[0]?.attribute1) : api.currentItem("attribute1")
  	break
  
}

return previousPricevalidFromdate