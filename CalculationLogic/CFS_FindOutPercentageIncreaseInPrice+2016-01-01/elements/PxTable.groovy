def filterSelector = [Constants.PX_ZPL_TABLE,Constants.PX_ZPLP_TABLE,Constants.PX_COMPOUND_PRICE_TABLE,Constants.PX_ZNRV_TABLE, Constants.PX_ZPE_TABLE]

def param = api.option(Constants.PX_TABLE_FILTER_LABEL,filterSelector)

def r = api.getParameter(Constants.PX_TABLE_FILTER_LABEL)

if (r != null && r.getValue() == null) {
    r.setValue(Constants.PX_ZPLP_TABLE)
    r.setRequired(true)
}

return param