import groovy.transform.Field

@Field String PX_TABLE_FILTER_LABEL 	= "Select PX Table"
@Field String PX_ZPL_TABLE 				= "S_ZPL"
@Field String PX_ZPLP_TABLE 			= "SalesPrice"
@Field String PX_COMPOUND_PRICE_TABLE 	= "CompoundPrice"
@Field String PX_ZNRV_TABLE 			= "SurchargeCostPrice"
@Field String PX_ZPE_TABLE 				= "BaseCost"

