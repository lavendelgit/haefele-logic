import net.pricefx.common.api.FieldFormatType
import net.pricefx.formulaengine.scripting.Matrix2D

Matrix2D resultData = out.MatrixData
net.pricefx.server.dto.calculation.ResultMatrix resultMatrix
if (resultData) {
    if (out.ApplyFormatting) {
        resultMatrix = getFormattedResultMatrix(resultData, grossPriceColumn, landingPriceColumn, DATA_COLUMNS)
    } else {
        resultMatrix = resultData.toResultMatrix()
    }
    Constants.COLUMN_DATATYPES.each { String columnName, FieldFormatType columnFormat ->
        resultMatrix.setColumnFormat(columnName, columnFormat)
    }
    resultMatrix.setColumnFormat(out.GrossPriceColumn, FieldFormatType.MONEY)
    resultMatrix.setColumnFormat(out.LandingPriceColumn, FieldFormatType.MONEY)
    resultMatrix.setEnableClientFilter(true)
    resultMatrix.setDisableSorting(false)
    resultMatrix.setTitle("Price Change Analysis (${out.PriceChangeName})")
    resultMatrix.setPreferenceName("DashboardPriceChangeSimulationMatrix")

    extractSummaryData(resultMatrix, out.ApplyFormatting ?: false)
}

return resultMatrix


protected net.pricefx.server.dto.calculation.ResultMatrix getFormattedResultMatrix(Matrix2D resultData, String grossPriceColumn, String landingPriceColumn, DATA_COLUMNS) {
    net.pricefx.server.dto.calculation.ResultMatrix resultMatrix = api.newMatrix(resultData.getColumnLabels())
    List matrixRows = resultData.collect()
    matrixRows.each { Map matrixRow ->
        matrixRow[(grossPriceColumn)] = numberStyledCell(resultMatrix, matrixRow[grossPriceColumn], matrixRow[DATA_COLUMNS.GROSS_PRICE.name])
        matrixRow[(landingPriceColumn)] = numberStyledCell(resultMatrix, matrixRow[landingPriceColumn], matrixRow[DATA_COLUMNS.LANDING_COST.name])
        matrixRow[DATA_COLUMNS.EXPECTED_POCKET_PRICE.name] = numberStyledCell(resultMatrix, matrixRow[DATA_COLUMNS.EXPECTED_POCKET_PRICE.name], matrixRow[DATA_COLUMNS.POCKET_PRICE.name])
        matrixRow[DATA_COLUMNS.EXPECTED_REVENUE.name] = numberStyledCell(resultMatrix, matrixRow[DATA_COLUMNS.EXPECTED_REVENUE.name], matrixRow[DATA_COLUMNS.TOTAL_REVENUE.name])
        matrixRow[DATA_COLUMNS.EXPECTED_POCKET_MARGIN.name] = numberStyledCell(resultMatrix, matrixRow[DATA_COLUMNS.EXPECTED_POCKET_MARGIN.name], matrixRow[DATA_COLUMNS.POCKET_MARGIN.name])
        matrixRow[DATA_COLUMNS.EXPECTED_MARGIN.name] = numberStyledCell(resultMatrix, matrixRow[DATA_COLUMNS.EXPECTED_MARGIN.name], matrixRow[DATA_COLUMNS.MARGIN.name])
        matrixRow[DATA_COLUMNS.EXPECTED_POCKET_MARGIN_PERCENT.name] = numberStyledCell(resultMatrix, matrixRow[DATA_COLUMNS.EXPECTED_POCKET_MARGIN_PERCENT.name], matrixRow[DATA_COLUMNS.POCKET_MARGIN_PERCENT.name])
        resultMatrix.addRow(matrixRow)
    }
    resultMatrix
}


protected numberStyledCell(net.pricefx.server.dto.calculation.ResultMatrix resultMatrix, BigDecimal numberValue, BigDecimal inComparisionWith) {
    if (numberValue < inComparisionWith) {
        resultMatrix.styledCell(numberValue, 'red', null)
    } else {
        resultMatrix.styledCell(numberValue, 'green', null)
    }
}

protected void extractSummaryData(net.pricefx.server.dto.calculation.ResultMatrix resultMatrix, boolean formattingEnabled = false) {
    Map summaryData = [:]
    List entries = resultMatrix?.getEntries()
    if (entries) {
        Map DATA_COLUMNS = Constants.DATA_COLUMNS_DEF
        summaryData.totalTransactions = entries[DATA_COLUMNS.TOTAL_TRANSACTIONS.name]?.sum() as BigDecimal
        summaryData.totalQuantity = entries[DATA_COLUMNS.TOTAL_QTY_SOLD.name]?.sum()
        summaryData.totalMaterials = entries.size()
        summaryData.materialsWithNegativePocketMargin = entries.count { entry ->
            (formattingEnabled ? entry[DATA_COLUMNS.EXPECTED_POCKET_MARGIN.name]?.value : entry[DATA_COLUMNS.EXPECTED_POCKET_MARGIN.name]) < 0
        }
        addSeriesAttributes(summaryData, entries, 'Revenue', DATA_COLUMNS.TOTAL_REVENUE.name, DATA_COLUMNS.CURRENT_REVENUE.name, DATA_COLUMNS.EXPECTED_REVENUE.name, DATA_COLUMNS.REVENUE_USING_RECOMMENDED_PRICE.name, formattingEnabled)
        addSeriesAttributes(summaryData, entries, 'Cost', DATA_COLUMNS.TOTAL_LANDING_COST.name, DATA_COLUMNS.TOTAL_LANDING_COST.name, DATA_COLUMNS.TOTAL_EXPECTED_LANDING_COST.name, DATA_COLUMNS.TOTAL_EXPECTED_LANDING_COST.name, formattingEnabled)
        addSeriesAttributes(summaryData, entries, 'PocketMargin', DATA_COLUMNS.CURRENT_POCKET_MARGIN.name, DATA_COLUMNS.CURRENT_POCKET_MARGIN.name, DATA_COLUMNS.EXPECTED_POCKET_MARGIN.name, DATA_COLUMNS.POCKET_MARGIN_USING_RECOMMENDED_PRICE.name, formattingEnabled)


        Constants.DISCOUNT_TYPES.each { String discountTypeCode, String discountType ->
            addBreakupByDiscountTypeSummary(summaryData, entries, discountType, "CURRENT")
            addBreakupByDiscountTypeSummary(summaryData, entries, discountType, "EXPECTED")
        }
    }
    api.local.SummaryData = summaryData
}

protected void addSeriesAttributes(Map summaryData,
                                   List entries,
                                   String seriesName,
                                   String actualColumnName,
                                   String currentColumnName,
                                   String expectedColumnName,
                                   String recommendedColumnName,
                                   boolean formattingEnabled = false) {
    summaryData[seriesName] = [
            actual  : entries[actualColumnName]?.sum() as BigDecimal,
            current : entries[currentColumnName]?.sum() as BigDecimal,
            expected: (formattingEnabled ? entries[expectedColumnName]?.value?.sum() : entries[expectedColumnName]?.sum()) as BigDecimal
    ]
    if (out.CalculateRecommended) {
        summaryData[seriesName].recommended = entries[recommendedColumnName]?.sum() as BigDecimal
    }
    addCalculatedAttributes(summaryData[seriesName])
}

protected void addCalculatedAttributes(Map seriesSummaryData) {
    seriesSummaryData.actualInMillion = toMillion(seriesSummaryData.actual)
    seriesSummaryData.currentDelta = (seriesSummaryData.current ?: 0) - (seriesSummaryData.actual ?: 0)
    seriesSummaryData.currentDeltaInMillion = toMillion(seriesSummaryData.currentDelta)
    seriesSummaryData.currentDeltaPercent = ((seriesSummaryData.current - seriesSummaryData.actual) / seriesSummaryData.actual) * 100
    seriesSummaryData.currentInMillion = toMillion(seriesSummaryData.current)
    seriesSummaryData.currentChangePercent = (seriesSummaryData.current / seriesSummaryData.actual) * 100
    seriesSummaryData.expectedDelta = (seriesSummaryData.expected ?: 0) - (seriesSummaryData.current ?: 0)
    seriesSummaryData.expectedDeltaInMillion = toMillion(seriesSummaryData.expectedDelta)
    seriesSummaryData.expectedDeltaPercent = ((seriesSummaryData.expected - seriesSummaryData.current) / seriesSummaryData.current) * 100
    seriesSummaryData.expectedInMillion = toMillion(seriesSummaryData.expected)
    seriesSummaryData.expectedChangePercent = (seriesSummaryData.expected / seriesSummaryData.current) * 100
    if (out.CalculateRecommended) {
        seriesSummaryData.recommendedInMillion = toMillion(seriesSummaryData.recommended)
        seriesSummaryData.recommendedDelta = (seriesSummaryData.recommended ?: 0) - (seriesSummaryData.expected ?: 0)
        seriesSummaryData.recommendedDeltaInMillion = toMillion(seriesSummaryData.recommendedDelta)
        seriesSummaryData.recommendedDeltaPercent = ((seriesSummaryData.recommended - seriesSummaryData.expected) / seriesSummaryData.expected) * 100
        seriesSummaryData.recommendedInMillion = toMillion(seriesSummaryData.recommended)
        seriesSummaryData.recommendedChangePercent = (seriesSummaryData.recommended / seriesSummaryData.expected) * 100
    }
}

protected String toMillion(moneyValue) {
    return (moneyValue != null) ? libs.SharedLib.RoundingUtils.round((moneyValue / 1000000), Constants.DECIMAL_PLACE.MONEY) + ' M' : ''
}

protected void addBreakupByDiscountTypeSummary(Map summaryData, List entries, String discountType, String outputType) {
    String uCaseDiscountType = discountType.toUpperCase()
    String revenueColumnName = Constants.DATA_COLUMNS_DEF[outputType + "_REVENUE_BY_" + uCaseDiscountType]?.name
    String marginColumnName = Constants.DATA_COLUMNS_DEF[outputType + "_TOTAL_POCKET_MARGIN_BY_" + uCaseDiscountType]?.name
    if (revenueColumnName && marginColumnName) {
        Map sectionMap = summaryData[outputType + "_BREAKUP"] ?: [:]
        sectionMap[discountType] = [
                totalRevenue: entries[revenueColumnName]?.sum() as BigDecimal,
                totalMargin : entries[marginColumnName]?.sum() as BigDecimal
        ]
        Map discountTypeMap = sectionMap[discountType]

        if (outputType == "EXPECTED") {
            BigDecimal currentTotalRevenue = summaryData["CURRENT_BREAKUP"][discountType]?.totalRevenue
            BigDecimal currentTotalMargin = summaryData["CURRENT_BREAKUP"][discountType]?.totalMargin
            BigDecimal priceChangePercent = out.PriceChangePer as BigDecimal
            if (out.IsGrossPriceChange && priceChangePercent != null) {
                BigDecimal revenueDelta = currentTotalRevenue * priceChangePercent
                discountTypeMap.totalRevenueIdeal = currentTotalRevenue + revenueDelta
                discountTypeMap.totalMarginIdeal = currentTotalMargin + revenueDelta
                discountTypeMap.priceChangePercent = priceChangePercent
            } else if (out.IsLandingPriceChange && out.PriceChangePer != null) {
                BigDecimal marginDelta = currentTotalMargin * priceChangePercent
                discountTypeMap.totalRevenueIdeal = currentTotalRevenue
                discountTypeMap.totalMarginIdeal = currentTotalMargin + marginDelta
                discountTypeMap.costChangePercent = priceChangePercent
            }
            discountTypeMap.totalRevenueIdealDelta = (discountTypeMap.totalRevenue ?: 0) - (discountTypeMap.totalRevenueIdeal ?: 0)
            discountTypeMap.totalMarginIdealDelta = (discountTypeMap.totalMargin ?: 0) - (discountTypeMap.totalMarginIdeal ?: 0)
        }

        discountTypeMap.totalRevenueInMillion = toMillion(discountTypeMap.totalRevenue)
        discountTypeMap.totalMarginInMillion = toMillion(discountTypeMap.totalMargin)
        discountTypeMap.marginPercent = discountTypeMap.totalRevenue ? (discountTypeMap.totalMargin / discountTypeMap.totalRevenue) : 0

        summaryData[outputType + "_BREAKUP"] = sectionMap
    }
}
