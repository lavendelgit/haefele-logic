List filters = []

def dmFilter = out.DMFilter
if (dmFilter) {
    filters.add(dmFilter)
}

filters.add(Filter.equal(Constants.DM_COLUMNS.YEAR, out.Year))
if (out.IncludeRowsWithQty) {
    filters.add(Filter.and(Filter.isNotNull(Constants.DM_COLUMNS.QUANTITY), Filter.isNotEmpty(Constants.DM_COLUMNS.QUANTITY)))
}

return filters