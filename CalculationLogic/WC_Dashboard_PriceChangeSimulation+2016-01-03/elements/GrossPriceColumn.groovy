if (out.PriceChangePer == null) {
    return ""
}
Map DATA_COLUMNS = Constants.DATA_COLUMNS_DEF
String grossPriceColumn = DATA_COLUMNS.EXPECTED_GROSS_PRICE.name
boolean isGrossPriceChange = out.IsGrossPriceChange
grossPriceColumn += isGrossPriceChange ? " (with ${out.PriceChangePer * 100} % change)" : ''

return grossPriceColumn