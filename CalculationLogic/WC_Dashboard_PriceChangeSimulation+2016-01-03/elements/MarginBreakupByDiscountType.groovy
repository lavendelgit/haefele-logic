Map summaryData = api.local.SummaryData
if (!summaryData) {
    return
}

List chartData = summaryData.EXPECTED_BREAKUP.collect { String discountTypeName, Map dtSummaryData ->
    [name         : discountTypeName,
     y            : dtSummaryData.totalMargin,
     totalRevenue : dtSummaryData.totalRevenue,
     totalMargin  : dtSummaryData.totalMargin,
     marginPercent: dtSummaryData.marginPercent]
}

return ChartUtils.geteratePieChart("Customer Margin Breakup By Discount Type", chartData, "Customer Margin")