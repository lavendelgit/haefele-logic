if (out.PriceChangePer == null) {
    return ""
}
Map DATA_COLUMNS = Constants.DATA_COLUMNS_DEF
String landingPriceColumn = DATA_COLUMNS.EXPECTED_LANDING_COST.name
boolean isLandingPriceChange = out.IsLandingPriceChange
landingPriceColumn += isLandingPriceChange ? " (with ${out.PriceChangePer * 100} % change)" : ''

return landingPriceColumn