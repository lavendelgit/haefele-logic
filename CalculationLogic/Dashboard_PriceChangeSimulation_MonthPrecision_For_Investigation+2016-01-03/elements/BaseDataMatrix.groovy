def resultMatrix =  api.getDatamartContext()
                       .executeQuery(
                               out.BaseDMQuery
                       )?.getData().toResultMatrix()

resultMatrix.setPreferenceName("BaseDMQuery")
return resultMatrix