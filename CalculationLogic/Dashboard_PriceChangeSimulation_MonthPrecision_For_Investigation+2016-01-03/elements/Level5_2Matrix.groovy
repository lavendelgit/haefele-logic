def resultMatrix =  api.getDatamartContext()
                       .executeSqlQuery(
                               out.QueryBuilderInstance?.getLevel5Part2Details() + Constants.LIMIT_CLAUSE,
                               out.BaseDMQuery
                       )?.toResultMatrix()

resultMatrix.setPreferenceName("Level5_2")
return resultMatrix
