protected List getWaterfallSeriesData(String seriesName, String year, Map summaryData) {
    Map COLORS = libs.HighchartsLibrary.ConstConfig.GRADIENT_COLORS
    Map colors = [series: COLORS.CYAN_BLUE.HEX, positive: COLORS.GREEN.HEX, negative: COLORS.RED.HEX]
    return [
            [
                    "name"         : "Total ${seriesName} ( ${year} )",
                    "label"        : "Total ${seriesName} ( ${year} )",
                    "y"            : summaryData.actual,
                    "changePercent": 100,
                    "color"        : colors.series
            ],
            [
                    "name"         : "${seriesName} Delta ( ${year} v/s current  )",
                    "label"        : "${seriesName} Delta ( ${year} v/s current  )",
                    "y"            : summaryData.currentDelta,
                    "changePercent": summaryData.currentDeltaPercent,
                    "color"        : summaryData.currentDelta > 0 ? colors.positive : colors.negative
            ],
            [
                    "name"         : "Total ${seriesName} ( with current price )",
                    "label"        : "Total ${seriesName} ( with current price )",
                    "y"            : summaryData.current,
                    "color"        : colors.series,
                    "changePercent": summaryData.currentChangePercent,
                    "isSum"        : true
            ],
            [
                    "name"         : "${seriesName} Delta ( current v/s expected )",
                    "label"        : "${seriesName} Delta ( current v/s expected )",
                    "y"            : summaryData.expectedDelta,
                    "changePercent": summaryData.expectedDelatPercent,
                    "color"        : summaryData.revenueDelta > 0 ? colors.positive : colors.negative
            ],
            [
                    "name"         : "Total ${seriesName} ( with increased price )",
                    "label"        : "Total ${seriesName} ( with increased price )",
                    "y"            : summaryData.expected,
                    "color"        : colors.series,
                    "changePercent": summaryData.expectedChangePercent,
                    "isSum"        : true
            ]
    ]
}

def generateWaterfallChart(String chartTitle, String yAxisTitle, List chartData, Boolean enableDataLabels, String seriesName) {
    if (chartData) {
        def hLib = libs.HighchartsLibrary
        def legend = hLib.Legend.newLegend().setEnabled(false)
        def xAxis = hLib.Axis.newAxis().setType(hLib.ConstConfig.AXIS_TYPES.CATEGORY)
        def yAxis = hLib.Axis.newAxis().setTitle(yAxisTitle)
                        .setMaxPadding(0.1)
        String displayFormat = hLib.ConstConfig.TOOLTIP_POINT_Y_FORMAT_TYPES.ABSOLUTE_PRICE
        Map plotOptions = [series: [borderWidth: 0,
                                    dataLabels : [borderWidth: 0,
                                                  color      : 'BLACK',
                                                  enabled    : enableDataLabels,
                                                  format     : displayFormat,
                                                  style      : [textOutline: 0],
                                                  inside     : false]]]


        def waterfallSeries = hLib.ColumnSeries.newColumnSeries().setName(seriesName)
                                  .setData(chartData)
                                  .setXAxis(xAxis)
                                  .setYAxis(yAxis)

        def tooltip = hLib.Tooltip.newTooltip().setPointFormat("{point.label} : <b>{point.y:,#,###.0f}</b><br>{point.label} % : <b>{point.changePercent:,#,###.0f} %</b>")
        def chart = hLib.Chart.newChart().setTitle(chartTitle)
                        .setTooltip(tooltip)
                        .setLegend(legend)
                        .setSeries(waterfallSeries)
                        .setPlotOptions(plotOptions)
        def chartDef = chart.getHighchartFormatDefinition()

        return api.buildHighchart(chartDef).showDataTab()
    }
}


def geteratePieChart(String chartTitle, List chartData, String seriesName) {
    def hLib = libs.HighchartsLibrary
    def constants = hLib.ConstConfig
    def legend = hLib.Legend.newLegend().setEnabled(false)
    def seriesTooltip = hLib.Tooltip.newTooltip().setPointFormat("<span style=\"color:{point.color}\">●</span> {series.name}: <b>${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PRICE} €</b><br/>")


    def seriesDataLabels = hLib.DataLabel.newDataLabel().setEnabled(true)
                               .setFormat("<b>{point.name}: ${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PRICE}</b><br>")

    def seriesData = hLib.PieSeries.newPieSeries().setName(seriesName)
                         .setData(chartData)
                         .setTooltip(seriesTooltip)
                         .setDataLabels(seriesDataLabels)
                         .setCustomFieldDefinition('size', '150')

    def chartDef = hLib.Chart.newChart().setTitle(chartTitle)
                       .setSeries(seriesData)
                       .setLegend(legend)
                       .getHighchartFormatDefinition()

    return api.buildHighchart(chartDef).showDataTab()
}
