List userConfiguredExceptions = out.AdditionalExceptions?.ExceptionMatrix
Map userConfigurationAccessKeys = Constants.EXCEPTION_COLUMNS

return userConfiguredExceptions.collect { Map exceptionEntry ->
    Library.getInitializedException(exceptionEntry[userConfigurationAccessKeys.MonthlyExceptionConfiguration.label],
                                    exceptionEntry[userConfigurationAccessKeys.ExceptionKey.label],
                                    true,
                                    exceptionEntry[userConfigurationAccessKeys.ExceptionFilter.label])
}