//TODO: Use DBQueries.getDataFromSource () method for the details.
def ctx = api.getDatamartContext()
def dm = ctx.getDatamart(Constants.DM_SIMULATION)
def dmQuery = ctx.newQuery(dm)
List dmColumnsDef = out.DMProjections
dmColumnsDef.each { Map columnDef ->
    dmQuery.select(columnDef.dmColumn, columnDef.alias)
}

if (out.DMFilter) {
    dmQuery.where(*out.DMFilter)
}

return dmQuery