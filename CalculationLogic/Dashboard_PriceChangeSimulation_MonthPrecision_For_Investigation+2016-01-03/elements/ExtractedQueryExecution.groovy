String extractedQuery = """ SELECT
                Material  as Material
                ,ProdhII  as ProdhII
                ,ProdhIII  as ProdhIII
                ,prodh_cm1  as prodh_cm1
                ,prodh_cm2  as prodh_cm2
                ,SecondaryKey  as SecondaryKey
                ,CustomerId  as CustomerId
                ,ConditionName  as ConditionName
                ,PricingType  as PricingType
                ,coalesce(Material, '-') || '-' ||  coalesce(CustomerId, '-') || '-' ||  coalesce(Year, '-') as TxnKey
                ,NovAdjustedRevenue as NovTotalRevenue
                ,NovAdjustedQtySold as NovQtySold
                ,NovXARTICLECurrentDiscountRevenueContribution as NovXARTICLECurrentDiscountRevenueContribution
                FROM T1 LEFT JOIN (
                SELECT Material  as CustomerMaterial,
                SUM(CASE WHEN CustomerId IS NOT NULL THEN NovTotalRevenue ELSE 0 END) as NovCustomerRevenue,
                SUM(CASE WHEN CustomerId IS NOT NULL THEN NovQtySold ELSE 0 END) as NovCustomerQtySold
                FROM (
                        SELECT T1.Material  as Material,
                               T1.CustomerId  as CustomerId,
                               T1.NovTotalRevenue as NovTotalRevenue,
                               T1.NovQtySold as NovQtySold,
                               ROW_NUMBER() OVER (PARTITION BY Material,CustomerId ORDER BY CustomerId) as RowNum
                               FROM T1) T1 WHERE RowNum = 1 
                GROUP BY Material) CUST_DATA 
                ON T1.Material = CUST_DATA.CustomerMaterial
                JOIN LATERAL (SELECT 
                                (CASE WHEN CustomerId IS NULL THEN NovTotalRevenue - NVL(CUST_DATA.NovCustomerRevenue,0) ELSE NovTotalRevenue END) as NovAdjustedRevenue,
                                (CASE WHEN CustomerId IS NULL THEN NovQtySold - NVL(CUST_DATA.NovCustomerQtySold, 0) ELSE NovQtySold END) as NovAdjustedQtySold 
                ) TblRevenueAndQty
                JOIN LATERAL (SELECT (ROUND( NovTotalRevenue / NULLIF(NovQtySold, 0), 8)) as NovHistoryPocketPrice) TblRatio 
                JOIN LATERAL (SELECT (CASE WHEN PricingType='XARTICLE' THEN NovHistoryPocketPrice ELSE 0 END) as NovXARTICLECurrentDiscountRevenueContribution) TblBreakup
                ${Constants.LIMIT_CLAUSE}  
"""

def resultMatrix =  api.getDatamartContext()
                       ?.executeSqlQuery( extractedQuery, out.BaseDMQuery)
                      ?.toResultMatrix()

resultMatrix.setPreferenceName("extracted data")
return resultMatrix
