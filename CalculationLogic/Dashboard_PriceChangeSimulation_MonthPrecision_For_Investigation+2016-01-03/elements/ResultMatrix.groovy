import net.pricefx.common.api.FieldFormatType
import net.pricefx.formulaengine.scripting.Matrix2D

Matrix2D resultData = api.getDatamartContext().executeSqlQuery(out.QueryBuilderInstance.getSQLQuery(), out.BaseDMQuery)
//Matrix2D resultData = api.getDatamartContext().executeSqlQuery(out.QueryBuilderInstance.getCalculatePerRowSQLQuery(), out.BaseDMQuery)

//return resultData.toResultMatrix()

api.local.MatrixData = resultData
net.pricefx.server.dto.calculation.ResultMatrix resultMatrix = null
if (resultData) {
    if (out.ComputeResultMatrix) {
        resultMatrix = resultData.toResultMatrix()
        Constants.COLUMN_DATATYPES.each { String columnName, FieldFormatType columnFormat -> resultMatrix.setColumnFormat(columnName, columnFormat)
        }
// There is a separate element we are building to highlight the gross price and landing price... And since its per month
//
//        resultMatrix.setColumnFormat(out.GrossPriceColumn, FieldFormatType.MONEY)
//        resultMatrix.setColumnFormat(out.LandingPriceColumn, FieldFormatType.MONEY)
        resultMatrix.setEnableClientFilter(true)
        resultMatrix.setDisableSorting(false)
        resultMatrix.setTitle("Monthly Price Change Simulation Results")
        resultMatrix.setPreferenceName("DashboardPriceChangeSimulationMatrixMonthly")
        extractSummaryData(resultMatrix)
    } else {
        extractSummaryData(resultData.toResultMatrix())
    }
}

//api.trace("The summary Data Matrix", " Values--" + api.jsonEncode(api.local.SummaryData))

return resultMatrix

protected void extractSummaryData(net.pricefx.server.dto.calculation.ResultMatrix resultMatrix, boolean formattingEnabled = false) {
    Map summaryData = [:]
    List entries = resultMatrix?.getEntries()
    if (entries) {
        Map level1 = Constants.LEVEL_1_ALIAS
        summaryData.totalTransactions = getSummationAcrossMonths(level1.TRANSACTION_COUNT, entries)
        summaryData.totalQuantity = [actual  : getSummationAcrossMonths(level1.TOTAL_QUANTITY_SOLD, entries),
                                     expected: getSummationAcrossMonths(level1.EXPECTED_QUANTITY_SOLD, entries)]
        summaryData.totalMaterials = entries.size()
        addSeriesAttributes(summaryData, entries, 'Revenue', level1.TOTAL_REVENUE, level1.TOTAL_REVENUE, level1.EXPECTED_REVENUE)
        addSeriesAttributes(summaryData, entries, 'Cost', level1.TOTAL_BASE_COST, level1.TOTAL_BASE_COST, level1.EXPECTED_TOTAL_BASE_COST)
        addSeriesAttributes(summaryData, entries, 'PocketMargin', level1.CURRENT_POCKET_MARGIN, level1.CURRENT_POCKET_MARGIN, level1.EXPECTED_TOTAL_MARGIN)

        api.trace("The Entries details are", api.jsonEncode(entries))
        Constants.PRICING_TYPES.each { String discountTypeCode, String pricingType ->
            addBreakupByDiscountTypeSummary(summaryData, entries, pricingType, "Current")
            addBreakupByDiscountTypeSummary(summaryData, entries, pricingType, "Expected")
        }
        api.trace("Summary Data " , api.jsonEncode(summaryData))

    }
    api.local.SummaryData = summaryData
}

protected BigDecimal getSummationAcrossMonths(String field, List dataSets) {
    BigDecimal totalValue = BigDecimal.ZERO

    Constants.MONTHS_PREFIXES.each { String prefix ->
        totalValue += (dataSets["$prefix$field"]?.sum {
            it ?: BigDecimal.ZERO
        } as BigDecimal)
    }
    //api.trace("The value for dataSets" + field, totalValue)

    return totalValue
}

protected void addSeriesAttributes(Map summaryData,
                                   List entries,
                                   String seriesName,
                                   String actualColumnName,
                                   String currentColumnName,
                                   String expectedColumnName) {
    summaryData[seriesName] = [actual  : getSummationAcrossMonths(actualColumnName, entries),
                               current : getSummationAcrossMonths(currentColumnName, entries),
                               expected: getSummationAcrossMonths(expectedColumnName, entries)]

    addCalculatedAttributes(summaryData[seriesName])
}

protected void addCalculatedAttributes(Map seriesSummaryData) {
    def mathUtils = libs.__LIBRARY__.MathUtility
    seriesSummaryData.actualInMillion = toMillion(seriesSummaryData.actual)
    seriesSummaryData.currentDelta = (seriesSummaryData.current ?: 0) - (seriesSummaryData.actual ?: 0)
    seriesSummaryData.currentDeltaInMillion = toMillion(seriesSummaryData.currentDelta)
    seriesSummaryData.currentDeltaPercent = mathUtils.divide((seriesSummaryData.current - seriesSummaryData.actual), seriesSummaryData.actual) * 100
    seriesSummaryData.currentInMillion = toMillion(seriesSummaryData.current)
    seriesSummaryData.currentChangePercent = mathUtils.divide(seriesSummaryData.current, seriesSummaryData.actual) * 100
    seriesSummaryData.expectedDelta = (seriesSummaryData.expected ?: 0) - (seriesSummaryData.current ?: 0)
    seriesSummaryData.expectedDeltaInMillion = toMillion(seriesSummaryData.expectedDelta)
    seriesSummaryData.expectedDeltaPercent = mathUtils.divide((seriesSummaryData.expected - seriesSummaryData.current), seriesSummaryData.current) * 100
    seriesSummaryData.expectedInMillion = toMillion(seriesSummaryData.expected)
    seriesSummaryData.expectedChangePercent = mathUtils.divide(seriesSummaryData.expected, seriesSummaryData.current) * 100
}

protected String toMillion(moneyValue) {
    return (moneyValue != null) ? libs.SharedLib.RoundingUtils.round((moneyValue / 1000000), Constants.DECIMAL_PLACE.MONEY) + ' M' : ''
}

protected void addBreakupByDiscountTypeSummary(Map summaryData, List entries, String pricingType, String outputType) {
    Map level1 = Constants.LEVEL_1_ALIAS
    String upperCaseOutputType = outputType.toUpperCase()
    String revenueColumnPrefix = level1["PT_${upperCaseOutputType}_CUSTOMER_REVENUE"]
    //CURRENT_CUSTOMER_REVENUE or EXPECTED_CUSTOMER_REVENUE
    String revenueColumnName = "$revenueColumnPrefix ($pricingType)"
    String marginColumnPrefix = level1["PT_${upperCaseOutputType}_TOTAL_MARGIN"]
    //PT_CURRENT_TOTAL_MARGIN or PT_EXPECTED_TOTAL_MARGIN
    String marginColumnName = "$marginColumnPrefix ($pricingType)"
    //api.trace("Tracing the revenue and margin....." + pricingType, revenueColumnName + " " + marginColumnName)
    if (revenueColumnName && marginColumnName) {
        Map sectionMap = summaryData[upperCaseOutputType + "_BREAKUP"] ?: [:]
        sectionMap[pricingType] = [totalRevenue: getSummationAcrossMonths(revenueColumnName, entries),
                                   totalMargin : getSummationAcrossMonths(marginColumnName, entries)]
        Map discountTypeMap = sectionMap[pricingType]

        if (upperCaseOutputType == "EXPECTED") {
            BigDecimal currentTotalRevenue = summaryData["CURRENT_BREAKUP"][pricingType]?.totalRevenue
            BigDecimal currentTotalMargin = summaryData["CURRENT_BREAKUP"][pricingType]?.totalMargin
            discountTypeMap.totalRevenueIdeal = currentTotalRevenue
            discountTypeMap.totalMarginIdeal = currentTotalMargin
            if (out.GrossPriceChangePercent) {
                Map priceChangePercent = getEffectivePercentageValues(out.GrossPriceChangePercent)
                BigDecimal revenueDelta = getPercentageChangeAmount(revenueColumnName, priceChangePercent, entries)
                discountTypeMap.totalRevenueIdeal = currentTotalRevenue + revenueDelta
                discountTypeMap.totalMarginIdeal = currentTotalMargin + revenueDelta
                discountTypeMap.priceChangePercent = priceChangePercent
            }
            if (out.LandingPriceChangePercent) {
                Map priceChangePercent = getEffectivePercentageValues(out.LandingPriceChangePercent)
                BigDecimal marginDelta = getPercentageChangeAmount(marginColumnName, priceChangePercent, entries)
                discountTypeMap.totalMarginIdeal += marginDelta
                discountTypeMap.costChangePercent = priceChangePercent
            }
            discountTypeMap.totalRevenueIdealDelta = (discountTypeMap.totalRevenue ?: 0) - (discountTypeMap.totalRevenueIdeal ?: 0)
            discountTypeMap.totalMarginIdealDelta = (discountTypeMap.totalMargin ?: 0) - (discountTypeMap.totalMarginIdeal ?: 0)
        }

        discountTypeMap.totalRevenueInMillion = toMillion(discountTypeMap.totalRevenue)
        discountTypeMap.totalMarginInMillion = toMillion(discountTypeMap.totalMargin)
        discountTypeMap.marginPercent = discountTypeMap.totalRevenue ? (discountTypeMap.totalMargin / discountTypeMap.totalRevenue) : 0

        summaryData[upperCaseOutputType + "_BREAKUP"] = sectionMap
    }
}

protected Map getEffectivePercentageValues(Map percentageChanges) {
    BigDecimal percentageTillDate = BigDecimal.ZERO
    Map updatedPercentageMap = Constants.MONTH_NAMES.collectEntries { String month ->
        percentageTillDate += percentageChanges[month]
        return [(month): percentageTillDate]
    }
    //api.trace(" The month values....", api.jsonEncode(updatedPercentageMap))

    return updatedPercentageMap
}

protected BigDecimal getPercentageChangeAmount(String field, Map monthlyPercentChange, List dataSets) {
    BigDecimal totalValue = BigDecimal.ZERO
    BigDecimal currentPercent
    Constants.MONTHS_PREFIXES.each { String prefix ->
        currentPercent = monthlyPercentChange[Constants.MONTHS_PREFIXES_NAMES[prefix]] ?: BigDecimal.ZERO
        totalValue += currentPercent * extactValueFromContainer(dataSets, "$prefix$field")
    }
    //api.trace("The Change amount for dataSets" + field, totalValue)

    return totalValue
}

protected BigDecimal extactValueFromContainer(List dataSet, String key) {
    if (!dataSet || !dataSet.contains(key)) {
        return BigDecimal.ZERO
    }
    return dataSet[key] as BigDecimal
}

