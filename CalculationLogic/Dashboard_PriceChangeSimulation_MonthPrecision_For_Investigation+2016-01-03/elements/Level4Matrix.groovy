def resultMatrix =  api.getDatamartContext()
                       .executeSqlQuery(
                               out.QueryBuilderInstance?.getLevel4Details()+ Constants.LIMIT_CLAUSE,
                               out.BaseDMQuery
                       )?.toResultMatrix()

resultMatrix.setPreferenceName("Level4")
return resultMatrix
