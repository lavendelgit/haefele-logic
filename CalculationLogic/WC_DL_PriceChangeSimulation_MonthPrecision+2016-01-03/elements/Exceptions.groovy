List exceptionKeys = out.AdditionalExceptions["Exception Key"]?.unique()
Map grossPriceChangePercentage = [:]
Map purchasePriceChangePercentage = [:]
Map quantityChangePercentage = [:]
List exceptionInputsDetails = []
BigDecimal grossPrice
BigDecimal purchasePrice
BigDecimal quantity

exceptionKeys?.each { exceptionKey ->
    List exceptionInputRowsForKeys = out.AdditionalExceptions.findAll { row -> row["Exception Key"] == exceptionKey}
    String dataFilter = exceptionInputRowsForKeys[0]["Exception Filter"]
    Constants.MONTH_NAMES.each { monthName ->
        Map exceptionInputRow = exceptionInputRowsForKeys.find { it-> it.Month == monthName}
        grossPrice = exceptionInputRow ? exceptionInputRow["Gross Price % Change"] as BigDecimal : BigDecimal.ZERO
        purchasePrice = exceptionInputRow ? exceptionInputRow["Purchase Price % Change"] as BigDecimal : BigDecimal.ZERO
        quantity = exceptionInputRow ? exceptionInputRow["Quantity % Change"] as BigDecimal : BigDecimal.ZERO

        grossPriceChangePercentage.put(monthName, grossPrice/100)
        purchasePriceChangePercentage.put(monthName, purchasePrice/100)
        quantityChangePercentage.put(monthName, quantity/100)

    }
    Map exceptionInputsRow = Library.getInitializedExceptionDetails(exceptionKey, true, dataFilter,
            grossPriceChangePercentage,
            purchasePriceChangePercentage,
            quantityChangePercentage)
    exceptionInputsDetails.add(exceptionInputsRow)
}

return exceptionInputsDetails