if (1 == 0 && api.isDebugMode()) {
    return [
            "version"                               : 0,
            "typedId"                               : "1189.JLTV",
            "lastUpdateByName"                      : "vivek.thakare",
            "createdByName"                         : "vivek.thakare",
            "name"                                  : "Simulation 6",
            "createDate"                            : "2021-11-16T14:58:09",
            "createdBy"                             : 5319,
            "lastUpdateDate"                        : "2021-11-16T14:58:09",
            "lastUpdateBy"                          : 5319,
            "attributeExtension___status"           : "Scheduled",
            "attributeExtension___schedule_date"    : "2021-11-16T14:58:08.768",
            "attributeExtension___simulation_owner" : "meenakshi.kamra",
            "attributeExtension___simulation_inputs": [
                    "calculationType"               : "AllSimulationInputs",
                    "SimulationName"                : "Simulation 6",
                    "Status"                        : "Ready",
                    "SimulationTitle"               : "Nov Monthly PP and Qty increase, GP 3.7 and 4.9 without Blum no Exceptions",
                    "AnalysisYear"                  : "2020",
                    "TargetYear"                    : "2021",
                    "SimulationMonthsFilter"        : [],
                    "MonthlyPricingSimulationInputs": [
                            "calculationType"           : "Exceptions",
                            "InputType"                 : "Overall Simulation",
                            "Month"                     : "October",
                            "GrossPricePercentChange"   : 0,
                            "PurchasePricePercentChange": 0,
                            "QuantityPercentChange"     : 0,
                            "AddRow"                    : false,
                            "PricingMatrix"             : [[
                                                                   "Pricing Inputs For"     : "Exceptions",
                                                                   "Exception Key"          : "Blum April",
                                                                   "Exception Filter"       : "Category = 'Blum Category'",
                                                                   "Month"                  : "April",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": 0,
                                                                   "Quantity % Change"      : 0,
                                                                   "RowKey"                 : "Exceptions_Blum April_April"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Exceptions",
                                                                   "Exception Key"          : "Blum April_October",
                                                                   "Exception Filter"       : "Category = 'Blum Category'",
                                                                   "Month"                  : "October",
                                                                   "Gross Price % Change"   : 6.5,
                                                                   "Purchase Price % Change": 0,
                                                                   "Quantity % Change"      : 0,
                                                                   "RowKey"                 : "Exceptions_Blum April_October"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "January",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": "-0.27",
                                                                   "Quantity % Change"      : "7",
                                                                   "RowKey"                 : "Overall Simulation_January"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "February",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": "7.66",
                                                                   "Quantity % Change"      : "10.69",
                                                                   "RowKey"                 : "Overall Simulation_February"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "March",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": "20.18",
                                                                   "Quantity % Change"      : "19.55",
                                                                   "RowKey"                 : "Overall Simulation_March"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "April",
                                                                   "Gross Price % Change"   : 3.7,
                                                                   "Purchase Price % Change": "14.43",
                                                                   "Quantity % Change"      : "27.22",
                                                                   "RowKey"                 : "Overall Simulation_April",
                                                                   "selected"               : false
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "May",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": "25.26",
                                                                   "Quantity % Change"      : "12.90",
                                                                   "RowKey"                 : "Overall Simulation_May"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "June",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": "22.73",
                                                                   "Quantity % Change"      : "31.86",
                                                                   "RowKey"                 : "Overall Simulation_June"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "July",
                                                                   "Gross Price % Change"   : "4.9",
                                                                   "Purchase Price % Change": "2.41",
                                                                   "Quantity % Change"      : "-3.82",
                                                                   "RowKey"                 : "Overall Simulation_July"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "August",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": "17.62",
                                                                   "Quantity % Change"      : "11.55",
                                                                   "RowKey"                 : "Overall Simulation_August"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "September",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": "3.84",
                                                                   "Quantity % Change"      : "-4.85",
                                                                   "RowKey"                 : "Overall Simulation_September"
                                                           ], [
                                                                   "Pricing Inputs For"     : "Overall Simulation",
                                                                   "Month"                  : "October",
                                                                   "Gross Price % Change"   : 0,
                                                                   "Purchase Price % Change": "5.38",
                                                                   "Quantity % Change"      : "10.14",
                                                                   "RowKey"                 : "Overall Simulation_October"
                                                           ]]
                    ],
                    "RescheduleSimulation"          : true
            ],
            "attributeExtension___simulation_notify": "No",
            "id"                                    : 1189
    ]
}

return api.findLookupTableValues("SimulationInputs", Filter.equal('name', out.SimulationName))?.find()