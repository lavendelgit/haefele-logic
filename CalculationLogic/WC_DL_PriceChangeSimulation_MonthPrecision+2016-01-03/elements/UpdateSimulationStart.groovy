libs.__LIBRARY__.TraceUtility.developmentTrace('Printing the object to be saved', api.jsonEncode(out.AssociatedSimulationDetails.attributeExtension___simulation_inputs))
return
libs.HaefeleCommon.Simulation.callBoundCallUtilFunction(out.SimulationName,
                                                        'Processing',
                                                        out.SimulationOwner,
                                                        out.SimulationTitle,
                                                        out.AssociatedSimulationDetails.attributeExtension___simulation_inputs)