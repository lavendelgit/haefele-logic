Map filterDetails = out.AssociatedSimulationJSON[libs.HaefeleCommon.Simulation.SIMULATION_INPUT_COLUMNS.SimulationDataFilter.name]
libs.__LIBRARY__.TraceUtility.developmentTrace("The output of the filterDetails", filterDetails)

return filterDetails// ? api.filterFromMap(filterDetails) : null

