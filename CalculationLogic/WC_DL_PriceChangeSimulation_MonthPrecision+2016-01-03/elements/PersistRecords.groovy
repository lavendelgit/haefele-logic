List entriesToInsert = out.ResultMatrix?.getEntries()

libs.__LIBRARY__.TraceUtility.developmentTrace("Printing the records 666666666666666666666666666666666666", api.jsonEncode(entriesToInsert[0]))

boolean couldSave = false
try {
    entriesToInsert.each { Map rowToInsert ->
        api.trace("############################### rowToInsert", rowToInsert)
        libs.__LIBRARY__.TraceUtility.developmentTrace("Printing the records", api.jsonEncode(rowToInsert))
        rowToInsert.isDeleted = false
        libs.__LIBRARY__.DBQueries.insertRecordIntoTargetRowSet(rowToInsert)
        couldSave = true
    }
} catch (ex) {
    api.logInfo("PersistRecords", "error", ex)
}

return couldSave