api.trace("out.QueryBuilderInstance?.getAggregateByMaterialTxnSQLQuery()", out.QueryBuilderInstance?.getAggregateByMaterialTxnSQLQuery())
def resultMatrix = api.getDatamartContext()
                      .executeSqlQuery(
                              out.QueryBuilderInstance?.getAggregateByMaterialTxnSQLQuery() + Constants.LIMIT_CLAUSE,
                              out.BaseDMQuery
                      )?.toResultMatrix()


resultMatrix.setPreferenceName("AggregateByMaterialTxnSQL")
return resultMatrix