def resultMatrix = api.getDatamartContext()
                      .executeSqlQuery(
                              out.QueryBuilderInstance?.getLevel4Details() + Constants.LIMIT_CLAUSE,
                              out.BaseDMQuery
                      )?.toResultMatrix()
api.trace("out.QueryBuilderInstance?.getLevel4Details()", out.QueryBuilderInstance?.getLevel4Details())
resultMatrix.setPreferenceName("Level4")
return resultMatrix
