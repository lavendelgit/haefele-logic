def resultMatrix = api.getDatamartContext()
                      .executeSqlQuery(
                              out.QueryBuilderInstance?.getCalculatePerRowSQLQuery() + Constants.LIMIT_CLAUSE,
                              out.BaseDMQuery
                      )?.toResultMatrix()

api.trace("out.QueryBuilderInstance?.getCalculatePerRowSQLQuery()", out.QueryBuilderInstance?.getCalculatePerRowSQLQuery())
resultMatrix.setPreferenceName("CalculatePerRowSQL")
return resultMatrix
