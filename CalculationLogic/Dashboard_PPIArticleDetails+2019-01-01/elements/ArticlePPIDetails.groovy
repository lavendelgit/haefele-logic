import net.pricefx.common.api.FieldFormatType

def validityYear = api.input("Effective Year")
def efficiencyCategory = api.input("Efficiency Category")
//validityYear = (validityYear) ?: "2019" // Only for testing...
def dbFields = ['Article', 'EfficiencyCategory', 'LossToBusiness', 'LossRecovered',
                'OverallImpact', 'TotalTransactions', 'TransactionsParticipated', 'TotalTransactedUnits', 'TransactionUnitsParticipated']
def fields = [
        'Material'               : dbFields[0],
        'EfficiencyCategory'     : dbFields[1],
        'PurchasePriceChangeLoss': dbFields[2],
        'SalesPriceChangeGain'   : dbFields[3],
        'TotalPriceChangeImpact' : dbFields[4],
        'TotalTransactions'      : dbFields[5],
        'RelevantTransactions'   : dbFields[6],
        'TotalTransactedUnits'   : dbFields[7],
        'RelevantTransactedUnits': dbFields[8]
]
def filters = [
        Filter.equal("Year", validityYear + "-01-01")
]
if (efficiencyCategory && efficiencyCategory != 'All') {
    def filterEfficiencyCategory = libs.HaefeleCommon.PricingInefficiencyCommon?.displayOrder.find { it.value == efficiencyCategory }?.key
    filters.add(Filter.equal("EfficiencyCategory", filterEfficiencyCategory))
}
def dataRows = libs.__LIBRARY__.DBQueries.getDataFromSource(fields, 'PurchasePriceInefficiencyRecordsDM', filters, 'DataMart')

def yearDetails = " (Year-$validityYear) "
def columns = ['Article', 'Price Change Scenario',
               'Estimated Scope of Improvement' + yearDetails,
//               'Estimated Loss Recovered' + yearDetails, 'Net Loss' + yearDetails,
               'Number of Transactions' + yearDetails, 'Impacted Number Of Transactions' + yearDetails,
               'Number Of Transacted Units' + yearDetails, 'Impacted Number Of Transaction Units' + yearDetails
]
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT,
                   FieldFormatType.MONEY_EUR,
//                   FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER,
                   FieldFormatType.INTEGER, FieldFormatType.INTEGER]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Details: Purchase Price Change Inefficiencies at Article Level' + yearDetails,
        columns, columnTypes)
def netLoss
def netLossColor
dataRows?.each { currentEntry ->
    netLoss = (currentEntry[dbFields[4]])
    netLossColor = (netLoss && netLoss < 0) ? "#fc5c65" : "#2e7d10"
    netLoss = (netLoss)? netLoss * -1: 0.0
    resultMatrix.addRow([
            (columns[0]): currentEntry[dbFields[0]],
            (columns[1]): libs.HaefeleCommon.PricingInefficiencyCommon?.displayOrder[(currentEntry[dbFields[1]])] ?: 'Unknown',
            (columns[2]): dashUtl.boldHighlightWith(resultMatrix, (currentEntry) ? currentEntry[dbFields[2]]: 0.0, "#fc5c65"),
//            (columns[3]): (currentEntry[dbFields[3]]) ?: 0.0,
//            (columns[4]): dashUtl.boldHighlightWith(resultMatrix, netLoss, netLossColor),
            (columns[3]): (currentEntry[dbFields[5]]) ?: 0,
            (columns[4]): (currentEntry[dbFields[6]]) ?: 0,
            (columns[5]): (currentEntry[dbFields[7]]) ?: 0,
            (columns[6]): (currentEntry[dbFields[8]]) ?: 0
    ])
}
resultMatrix.setPreferenceName("Article Level PPI View")

return resultMatrix