/**
 * Construct column chart.
 * This chart contains three vertical axes, two of them are on the left hand side, one is on the right.
 * Margin adjustment is done implicitly due to default partition settings.
 */
def hLib = libs.HighchartsLibrary

def constants = hLib.ConstConfig
def legend = hLib.Legend.newLegend().setEnabled(true)

def quantityTooltip = hLib.Tooltip.newTooltip().setPointFormat("<b>Σ Quantity: </b>${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PRICE}")
def revenueTooltip = hLib.Tooltip.newTooltip().setPointFormat("<b>Σ Revenue: </b>${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PRICE}")
def marginTooltip = hLib.Tooltip.newTooltip().setPointFormat("<b>Margin %: </b>${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PERCENTAGE}")

def chartData = DataManagement.columnAndLineData()

def quartersAxis = hLib.Axis.newAxis().setTitle("Quarters")
        .setType(constants.AXIS_TYPES.CATEGORY)
        .setCategories(chartData.PricingDateQuarter)

def quantityAxis = hLib.Axis.newAxis().setTitle("Quantity")
def revenueAxis = hLib.Axis.newAxis().setTitle("Revenue")
def marginAxis = hLib.Axis.newAxis().setTitle("Margin %")
        .setOpposite(true)

def quantitySeries = hLib.ColumnSeries.newColumnSeries().setName("Quantity")
        .setData(chartData.Quantity)
        .setXAxis(quartersAxis)
        .setYAxis(quantityAxis)
        .setTooltip(quantityTooltip)

def revenueSeries = hLib.ColumnSeries.newColumnSeries().setName("Revenue")
        .setData(chartData.Revenue)
        .setXAxis(quartersAxis)
        .setYAxis(revenueAxis)
        .setTooltip(revenueTooltip)

def marginPercentSeries = hLib.SplineSeries.newSplineSeries().setName("Margin %")
        .setData(chartData.MarginPercent)
        .setXAxis(quartersAxis)
        .setYAxis(marginAxis)
        .setTooltip(marginTooltip)

def chartDef = hLib.Chart.newChart().setTitle("Column chart example")
        .setSeries(revenueSeries, quantitySeries, marginPercentSeries)
        .setLegend(legend)
        .getHighchartFormatDefinition()

chartDef << [chart: [marginLeft: 150]]

return api.buildHighchart(chartDef)