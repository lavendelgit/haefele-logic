/**
 * Construct bubble chart.
 * Demonstrates how the data can be grouped into "bubbles".
 */
def hLib = libs.HighchartsLibrary

def legend = hLib.Legend.newLegend().setEnabled(true)
def chartData = DataManagement.bubbleData()

def revenueAxis = hLib.Axis.newAxis().setTitle("Revenue")

def marginAxis = hLib.Axis.newAxis().setTitle("Margin")

def dataLabels = libs.HighchartsLibrary.DataLabel.newDataLabel().setEnabled(true)
        .setFormat("{point.name}")

def bubbleSeriesData = chartData.collect {
    [name: it.name ?: "Other",
     x   : it.Revenue,
     y   : it.Margin,
     z   : it.Quantity]
}

def bubbleSeries = hLib.BubbleSeries.newBubbleSeries().setName("Revenue to Margin")
        .setData(bubbleSeriesData)
        .setDataLabels(dataLabels)
        .setXAxis(revenueAxis)
        .setYAxis(marginAxis)

def pointFormat = '<tr><th colspan="2"><h3>{point.name}</h3></th></tr>' +
        '<tr><th>Σ Revenue:</th><td>{point.x:,#,###.2f} €</td></tr>' +
        '<tr><th>Σ Margin:</th><td>{point.y:,#,###.2f} €</td></tr>' +
        '<tr><th>Σ Quantity:</th><td>{point.z}</td></tr>'

def tooltip = hLib.Tooltip.newTooltip().setPointFormat(pointFormat)
        .setUseHTML(true)
        .setHeaderFormat("<table>")
        .setFooterFormat("</table")

def chartDef = hLib.Chart.newChart().setTitle("Bubble chart example")
        .setSeries(bubbleSeries)
        .setLegend(legend)
        .setTooltip(tooltip)
        .getHighchartFormatDefinition()

return api.buildHighchart(chartDef)