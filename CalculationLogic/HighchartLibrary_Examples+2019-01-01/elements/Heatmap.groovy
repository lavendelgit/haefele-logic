/**
 * Construct heatmap chart.
 */
def hLib = libs.HighchartsLibrary
def legend = hLib.Legend.newLegend().setEnabled(true)

def revenueTooltip = hLib.Tooltip.newTooltip().setPointFormat("<b>Σ Revenue: </b>{point.value:,#,###.2f}M")

def chartData = DataManagement.heatmapData()

def transformedData = transformData(chartData)
def seriesData = transformedData.findAll { it.Revenue < 100 } // We're filtering out too-big data points to fix scaling for demo purposes on this dataset
        .collect { [it.Week, it.Year, it.Revenue] }

Map colors = hLib.ConstConfig.COLORS
def colorAxis = hLib.ColorAxis.newColorAxis().setMinColor(colors.WHITE.HEX)
        .setMaxColor(colors.CYAN_BLUE.HEX)

def heatmapSeries = hLib.HeatmapSeries.newHeatmapSeries().setData(seriesData)

def chartDef = hLib.Chart.newChart().setTitle("Heatmap chart example - Total Revenue in each week of each year")
        .setTooltip(revenueTooltip)
        .setSeries(heatmapSeries)
        .setColorAxis(colorAxis)
        .setLegend(legend)
        .getHighchartFormatDefinition()

return api.buildHighchart(chartDef)

def transformData(List chartData) {
    chartData.each {
        it.Week = Integer.parseInt(it.Week.substring(it.Week.size() - 2, it.Week.size()))
        it.Year = Integer.parseInt(it.Year)
        it.Revenue = (it.Revenue / 1_000_000)
    }

    def weekRange = (chartData.Week.min())..(chartData.Week.max())
    def yearRange = (chartData.Year.min())..(chartData.Year.max())

    def groupedData = chartData.groupBy { [it.Year, it.Week] }
    weekRange.each { week ->
        yearRange.each { year ->
            if (!groupedData.keySet().contains([year, week])) {
                groupedData << [[year, week]: [[Year: year, Week: week, Revenue: 0]]]
            }
        }
    }

    return groupedData.values().collect { it?.getAt(0) }
}