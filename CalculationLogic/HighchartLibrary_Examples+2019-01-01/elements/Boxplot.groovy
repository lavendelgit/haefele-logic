/**
 * Construct Boxplot chart.
 * Demonstrates how some statistical information about data can be produced.
 */
def hLib = libs.HighchartsLibrary

def constants = hLib.ConstConfig
def legend = hLib.Legend.newLegend().setEnabled(true)

def chartData = DataManagement.boxplotData()

def uniqueCategories = chartData.Country.toSet().toList()

def countryAxis = hLib.Axis.newAxis().setTitle("Country")
        .setType(constants.AXIS_TYPES.CATEGORY)
        .setCategories(uniqueCategories)

def revenueAxis = hLib.Axis.newAxis().setTitle("Revenue metrics")

def seriesData = chartData.collect { it.values() }

def boxplotSeries = hLib.BoxplotSeries.newBoxplotSeries().setName("Country data")
        .setXAxis(countryAxis)
        .setYAxis(revenueAxis)
        .setData(seriesData)

def chartDef = hLib.Chart.newChart().setTitle("Boxplot chart example")
        .setSeries(boxplotSeries)
        .setLegend(legend)
        .getHighchartFormatDefinition()

return api.buildHighchart(chartDef)