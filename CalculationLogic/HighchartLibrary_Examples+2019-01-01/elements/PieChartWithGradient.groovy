/**
 * Construct pie chart with gradient coloring.
 * This chart has drilldown functionality. That's why each high level data has to have "drilldown" field set to the ID
 * that is then set in each low level series.
 */
def hLib = libs.HighchartsLibrary

def constants = hLib.ConstConfig
def legend = hLib.Legend.newLegend().setEnabled(false)

def drilldownDataLabels = libs.HighchartsLibrary.DataLabel.newDataLabel().setEnabled(true)
        .setFormat("{point.name}<br>")
        .setColor("contrast")
        .setFontSize("11px")
        .setFontWeight("normal")
        .setTextOutline("1px contrast")

def seriesTooltip = hLib.Tooltip.newTooltip().setPointFormat("<span style=\"color:{point.color}\">●</span> {series.name}: <b>${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PRICE} €</b><br/>")

def pieChartData = DataManagement.pieData()

def drilldownSeries = pieChartData.collect { String regionName, List regionData ->
    def drilldownData = regionData.collect {
        return [name: it.Country ?: "Other",
                y   : it.Revenue]
    }

    return hLib.PieSeries.newPieSeries().setName("Country revenue")
            .setData(drilldownData)
            .setTooltip(seriesTooltip)
            .setId(regionName)
            .setDataLabels(drilldownDataLabels)
}

def topLevelSeriesData = pieChartData.collect { String regionName, List regionData ->
    [name     : regionName,
     y        : regionData.sum { it.Revenue },
     drilldown: regionName]
}

def topLevelSeriesDataLabels = hLib.DataLabel.newDataLabel().setEnabled(true)
        .setFormat("<b>{point.name}: ${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PRICE} €</b><br>")

def topLevelSeries = hLib.PieSeries.newPieSeries().setName("Regional revenue")
        .setData(topLevelSeriesData)
        .setTooltip(seriesTooltip)
        .setDataLabels(topLevelSeriesDataLabels)

def gradientColors = hLib.PlotOptionsUtil.getGradientColors(0.65)
def plotOptions = [pie: [colors: gradientColors]]

def chartDef = hLib.Chart.newChart().setTitle("Pie chart with gradient example")
        .setSeries(topLevelSeries)
        .setDrilldownSeries(*drilldownSeries)
        .setLegend(legend)
        .setPlotOptions(plotOptions)
        .getHighchartFormatDefinition()

return api.buildHighchart(chartDef)