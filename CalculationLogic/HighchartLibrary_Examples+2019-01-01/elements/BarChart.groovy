/**
 * Construct bar chart.
 * This is one of the simplest implementations of the chart.
 */
def hLib = libs.HighchartsLibrary

def constants = hLib.ConstConfig
def legend = hLib.Legend.newLegend().setEnabled(false)

def marginTooltip = hLib.Tooltip.newTooltip().setPointFormat("<b>Margin %: </b>${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PERCENTAGE}")

def chartData = DataManagement.barData()

def countryAxis = hLib.Axis.newAxis().setTitle("Country")
        .setType(constants.AXIS_TYPES.CATEGORY)
        .setCategories(chartData.Country)

def marginAxis = hLib.Axis.newAxis().setTitle("Margin %")

def marginPercentSeries = hLib.ColumnSeries.newColumnSeries().setName("Margin %")
        .setHorizontal(true)
        .setData(chartData.MarginPercent)
        .setTooltip(marginTooltip)
        .setXAxis(countryAxis)
        .setYAxis(marginAxis)

def chartDef = hLib.Chart.newChart().setTitle("Bar chart example")
        .setSeries(marginPercentSeries)
        .setLegend(legend)
        .getHighchartFormatDefinition()

chartDef << [chart: [marginLeft: 175]]

return api.buildHighchart(chartDef)