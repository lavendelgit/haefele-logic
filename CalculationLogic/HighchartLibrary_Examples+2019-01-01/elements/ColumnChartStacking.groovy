/**
 * Construct column chart with stacking.
 * This chart contains three vertical axes, two of them are on the left hand side, one is on the right.
 * Margin adjustment is done implicitly due to default partition settings.
 */
def hLib = libs.HighchartsLibrary

def constants = hLib.ConstConfig
def legend = hLib.Legend.newLegend().setEnabled(true)
def revenueTooltip = hLib.Tooltip.newTooltip().setPointFormat("<b>Σ Revenue: </b>${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PRICE}")

def chartData = DataManagement.stackingData()
def usaData = chartData.findAll { it.Country == "USA" }
def chinaData = chartData.findAll { it.Country == "China" }

def quarterAxis = hLib.Axis.newAxis().setTitle("Quarters")
        .setType(constants.AXIS_TYPES.CATEGORY)
        .setCategories(chartData.PricingDateQuarter)

def usaSeries = hLib.ColumnSeries.newColumnSeries().setName("USA")
        .setData(usaData.Revenue)
        .setXAxis(quarterAxis)
        .setTooltip(revenueTooltip)
        .setStacking(constants.STACKING_MODES.NORMAL)

def chinaSeries = hLib.ColumnSeries.newColumnSeries().setName("China")
        .setData(chinaData.Revenue)
        .setXAxis(quarterAxis)
        .setTooltip(revenueTooltip)
        .setStacking(constants.STACKING_MODES.NORMAL)

def chartDef = hLib.Chart.newChart().setTitle("Column stacking example")
        .setSeries(chinaSeries, usaSeries)
        .setLegend(legend)
        .getHighchartFormatDefinition()

chartDef << [chart: [marginLeft: 150]]

return api.buildHighchart(chartDef)