/**
 * Get data for scatter chart.
 * Use case: display Revenue vs Margin% grouped by Product Group and band by Customer Group
 * Query results format:
 * ["CustomerGroup":"Würter Wurst","SUMInvoicePrice":2502.92724203166145,"MarginPercent":37.67,"ProductGroup":"Toppings"]
 * Group the data by CustomerGroup for further processing.
 * @return Map consisting the data for further processing.
 */
def scatterData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [CustomerGroup  : "CustomerGroup",
                                   SUMInvoicePrice: "SUM(InvoicePrice)",
                                   MarginPercent  : "ROUND(100*SUM(GrossMargin)/SUM(InvoicePrice),2)",
                                   ProductGroup   : "ProductGroup"]]

    def queryResults = hLib.QueryModule.queryDatamart(queryDef)
    def groupedElements = queryResults?.groupBy { it.CustomerGroup }

    return groupedElements
}

/**
 * Get data for column and line charts (they share same data set)
 * Use case: display correlations between Revenue, Quantity and Margin% in different Quarters
 * Query results format:
 * ["PricingDateQuarter":"2019-Q1","Revenue":5189721.6541818393594046,"Quantity":1705521,"MarginPercent":39.07]
 * @return Map consisting the data for further processing.
 */
def columnAndLineData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [PricingDateQuarter: "PricingDateQuarter",
                                   Revenue           : "SUM(InvoicePrice)",
                                   Quantity          : "SUM(Quantity)",
                                   MarginPercent     : "ROUND(100*SUM(GrossMargin)/SUM(InvoicePrice),2)"]]

    return hLib.QueryModule.queryDatamart(queryDef)
}

/**
 * Get data for pie chart.
 * Use case: display Revenue per Region with per Country drilldown.
 * Query results format:
 * ["Region":"Oceania","Country":"Australia","Revenue":2402193.4217199407124086]
 * @return Map consisting the data for further processing.
 */
def pieData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [Region : "Region",
                                   Country: "Country",
                                   Revenue: "SUM(InvoicePrice)"]]

    def queryResults = hLib.QueryModule.queryDatamart(queryDef)
    def groupedElements = queryResults.findAll { it.Region != null }
            .groupBy { it.Region }

    return groupedElements
}

/**
 * Get data for bar chart.
 * Use case: display Margin% per Country
 * Query results format:
 * ["Country":"USA","MarginPercent":41.40]
 * @return Map consisting the data for further processing.
 */
def barData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [Country      : "Country",
                                   MarginPercent: "ROUND(100*SUM(GrossMargin)/SUM(InvoicePrice),2)"]]

    return hLib.QueryModule.queryDatamart(queryDef)
}

/**
 * Get data for stacking chart.
 * Use case: display Revenue per Country
 * Query results format:
 * ["Country":"USA","Period":"15/09/2019","Revenue":41.40]
 * @return Map consisting the data for further processing.
 */
def stackingData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [Country: "Country",
                                   Period : "PricingDateQuarter",
                                   Revenue: "SUM(InvoicePrice)"]]

    return hLib.QueryModule.queryDatamart(queryDef)
}

/**
 * Get data for heatmap chart.
 * Use case: Display total Revenue in each week of each year
 * Query results format:
 * ["Week":"2015-W01","Year":"2015","Revenue":41.40]
 * @return Map consisting the data for further processing.
 */
def heatmapData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [Week   : "PricingDateWeek",
                                   Year   : "PricingDateYear",
                                   Revenue: "SUM(InvoicePrice)"]]

    return hLib.QueryModule.queryDatamart(queryDef)
}

/**
 * Get data for histogram and bell curve chart.
 * Query results format:
 * ["Week":1]
 * @return Map consisting the data for further processing.
 */
def histogramAndBellCurveData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [Week: "PricingDateWeek"]]

    return hLib.QueryModule.queryDatamart(queryDef)
}

/**
 * Get data for boxplot chart.
 * Use case: Display statistical information about revenue data per country
 * Query results format:
 * ["Country":"USA","Min":41.40,"Lower":81.40,"Mean":141.40,"Upper":241.40,"Max":341.40]
 * @return Map consisting the data for further processing.
 */
def boxplotData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [Country: "Country",
                                   Min    : "(MIN(InvoicePrice)+AVG(InvoicePrice))/4",
                                   Lower  : "(MIN(InvoicePrice)+AVG(InvoicePrice))/2",
                                   Mean   : "AVG(InvoicePrice)",
                                   Upper  : "(MAX(InvoicePrice)+AVG(InvoicePrice))/2",
                                   Max    : "MAX(InvoicePrice)"]]

    return hLib.QueryModule.queryDatamart(queryDef)
}

/**
 * Get data for bubble chart.
 * Use case: display Revenue and Margin per country
 * Query results format:
 * ["name":"USA", "Revenue":41.40, "Margin":81.40, "Quantity":141]
 * @return Map consisting the data for further processing.
 */
def bubbleData() {
    def hLib = libs.HighchartsLibrary

    def queryDef = [datamartName: "Standard_Sales_Data",
                    rollup      : true,
                    fields      : [name    : "Country",
                                   Revenue : "SUM(InvoicePrice)",
                                   Margin  : "SUM(GrossMargin)",
                                   Quantity: "SUM(Quantity)"]]

    return hLib.QueryModule.queryDatamart(queryDef)
}