/**
 * Construct scatter chart.
 * This chart has plotlines on 10th percentile of each dimension.
 */
def hLib = libs.HighchartsLibrary

def constants = hLib.ConstConfig
def legend = hLib.Legend.newLegend().setEnabled(false)

def tooltip = hLib.Tooltip.newTooltip().setHeaderFormat('<b>{series.name}</b><br>')
        .setPointFormat("Product Group: <b>{point.name}<b><br>Invoice Price Σ: ${constants.TOOLTIP_POINT_X_FORMAT_TYPES.PRICE} Σ <br>Margin Percent: ${constants.TOOLTIP_POINT_Y_FORMAT_TYPES.PERCENTAGE} %<br>")

BigDecimal maxSumInvoicePrice = 0, minMarginPercent = 100, maxMarginPercent = 0
def series = DataManagement.scatterData().collect { String customerGroup, List data ->
    def prepData = data.collect {
        if (it.SUMInvoicePrice > maxSumInvoicePrice) {
            maxSumInvoicePrice = it.SUMInvoicePrice
        }
        if (it.MarginPercent > maxMarginPercent) {
            maxMarginPercent = it.MarginPercent
        }
        if (it.MarginPercent < minMarginPercent) {
            minMarginPercent = it.MarginPercent
        }

        return [x   : it.SUMInvoicePrice,
                y   : it.MarginPercent,
                name: it.ProductGroup]
    }

    return hLib.ScatterSeries.newScatterSeries().setData(prepData)
            .setName(customerGroup)
}

BigDecimal marginPercentDifference = maxMarginPercent - minMarginPercent
BigDecimal plotLinePercentile = 0.1G
def revenuePercentilePlotLine = hLib.Axis.newPlotLine().setValue(maxSumInvoicePrice * plotLinePercentile)
        .setWidth(1)
        .setColor("red")
        .setLabel([text: "10th percentile of Revenue"])

def marginPercentilePlotLine = hLib.Axis.newPlotLine().setValue(minMarginPercent + marginPercentDifference * plotLinePercentile)
        .setWidth(1)
        .setColor("red")
        .setLabel([text: "10th percentile of Margin % difference"])

def revenueAxis = hLib.Axis.newAxis().setTitle("Revenue")
        .setPlotLines(revenuePercentilePlotLine)
        .setLabels([format: "{value} %"])

def marginAxis = hLib.Axis.newAxis().setTitle("Margin")
        .setPlotLines(marginPercentilePlotLine)

def allSeries = series.collect { it.setXAxis(revenueAxis).setYAxis(marginAxis) }

def chartDef = hLib.Chart.newChart().setTitle("Scatter chart example")
        .setSeries(*allSeries)
        .setLegend(legend)
        .setTooltip(tooltip)
        .getHighchartFormatDefinition()

return api.buildHighchart(chartDef)