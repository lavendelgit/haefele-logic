def sku = api.getElement("FindProductSkuRelationship")
if (sku == null) return null

def skus = []

if (api.getElement("IsProductInDatamart")) {
  def ps = api.getElement("ProductSKU")
  def values = api.findLookupTableValues("product.sku", api.filter("value", sku))
  //api.trace("values", null, values)
  if (values) {
    for (v in values) {
      if (v.name != ps) skus.add(v.name)  
    }
  }
}

skus