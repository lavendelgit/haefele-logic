def customerId = api.getElement("CustomerId")
def sourceName = api.getElement("SourceClearingTable")

if (api.getElement("IsProductInDatamart")) {
  def skus = api.getElement("FindRelatedSkus")
  if (skus) {
    def values = null
    for (sku in skus) {
      //api.trace("sku", null, sku)
      values = api.findLookupTableValues(sourceName, api.filter("key1", customerId), api.filter("key2", api.getElement("ProductSKU")))
      //api.trace("values", null, values)
      if (values) {
        // looks like we can never get two records with same customer/product combination
        def minimum = values.min { it.attribute1 }?.attribute1
        //api.trace("minimum", null, minimum)
        def ppTableId = api.getElement("TargetPPTableId")
        if (ppTableId != null) {
	      api.addOrUpdate("MLTV2", ["lookupTableId" : ppTableId, "key1": customerId, "key2": sku, "attribute1" : minimum])          
        }
      }
    }
  }
}