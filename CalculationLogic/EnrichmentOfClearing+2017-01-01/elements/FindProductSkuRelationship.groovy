def sku = api.getElement("ProductSKU")
if (sku == null) return null

if (api.getElement("IsProductInDatamart")) {
  def values = api.findLookupTableValues("product.sku", api.filter("name", sku))
  //api.trace("values", null, values)
  if (values) {
    def productSku = values[0]?.value
    //api.trace("product.sku", null, productSku)
    return productSku
  }
}

return null