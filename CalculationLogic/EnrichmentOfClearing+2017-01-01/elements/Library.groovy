/*

Logic as such follows this:

1)	It should be possible to trigger enrichment of Clearing PP table (referred to as PP1) by user action
2)	Enrichment of PP1 is a based on content of temporary clearing table PP2. Precondition: PP2 is freshly loaded with clearing data, PP1 is empty
3)	The Enrichment logic is for each item in PP2 going to do:
3.0) push the item unchanged from PP2 to PP1 -> effectively copying the values 
3.1	) take the productsku, customerId  (reffered to as R2) and find if it exists in TX_DM (last year data only)
3.2)  If no, skip this row processing
3.3)  If yes,  go to the product.sku table and find all products that have same “product.sku”. Let’s call this group of products G2
3.4) For each product in G2:
SKIP 3.4.1)  Check if the product exist in TX_DM (last year only)   // we are skipping this based on Nathalie suggestion
SKIP 3.4.2) If yes, then skip it // we are skipping this based on Nathalie suggestion -> 3.4.3 happens all the time
3.4.3) If no, then put new record in PP1. PP1 clearing factor should be the minimum value of clearing factor found in PP2 for current R2 combination

*/

def getSlice(fieldName, productId, customerId) {
  def startDate = new DateTime(api.targetDate()).minusYears(1)
  //api.trace("startDate", null, startDate)
  def endDate = new DateTime(api.targetDate())
  //api.trace("endDate", null, endDate)

  def period = new TimePeriod(startDate.toDate(), endDate.toDate(), TimeUnit.DAY)
  //api.trace("timeperiod","", period)

  
  def slice = api.newDatamartSlice(fieldName, period, api.filter("Sku", productId), api.filter("CustomerID", customerId))
  return slice
}

def isProductInDM(productId, customerId) {
  def dmCtx = api.getDatamartContext()
  //api.trace("dmCtx", null, dmCtx)

  def txDM = dmCtx.getTable("TX_DM")

  //api.trace("txDM", null, txDM)

  def q  = dmCtx.newQuery(txDM)
  q.select("Count(sku)","C");
  def slice = getSlice("Datum", productId, customerId)
  //api.trace("slice", null, slice)
  q.where(slice)

  def result = dmCtx.executeQuery(q)
  //api.trace("result", null, result)

  def data = result.getData()
  def numberOfRecords = data?.getRowCount()
  //api.trace("numberOfRecords", null, numberOfRecords)
  //api.trace("data", null, data)
  
  def row = null
  for (def r=0; r<data.getRowCount(); r++) {
    row = data.getRowValues(r)
    //api.trace("row.C","", row.C)
    if (row.C > 0) {
      return true
    }
  }

  return false
}

