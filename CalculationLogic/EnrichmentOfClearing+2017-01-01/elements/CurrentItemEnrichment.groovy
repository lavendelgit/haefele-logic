def sku = api.getElement("ProductSKU")
def customerId = api.getElement("CustomerId")


def clearing = api.currentItem("Clearingfaktor")

def ppTableId = api.getElement("TargetPPTableId")
if (ppTableId != null) {
  api.addOrUpdate("MLTV2", ["lookupTableId" : ppTableId, "key1": customerId, "key2": sku, "attribute1" : clearing])          
}