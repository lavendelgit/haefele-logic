def yearOfAnalysis = api.input("Effective Year")
def prodII = api.input("ProdhII")
def prodIII = api.input("ProdhIII")
def prodIV = api.input("ProdhIV")

def dataList = libs.HaefeleCommon.ZPAPDashboardDynamic.getZPAPAnalysisSummaryPerCG (yearOfAnalysis, prodII, prodIII, prodIV)
return libs.HaefeleCommon.ZPAPDashboardDynamic.getZPAPAnalysisSummaryForEmbeddedDashboardPerCG(dataList, yearOfAnalysis, [prodII, prodIII, prodIV])