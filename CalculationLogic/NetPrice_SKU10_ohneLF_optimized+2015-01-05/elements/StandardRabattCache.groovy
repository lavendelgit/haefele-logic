def customerClass = api.getElement("Kunde_Klasse")
def key = "${customerClass}"

api.trace("key", null, key)
api.trace("api.global.standardRebateCache", null, api.global.standardRebateCache)

def cache = api.global.standardRebateCache
def result = cache.get(key)

if (result == null) {
  result = api.vLookup("Standardrabatt", customerClass)
  if (result == null) {
    result = -1;
  }
  cache.put(key, result)
}

api.trace("api.global.standardRebateCache", null, api.global.standardRebateCache)
if (result == -1) return null
result

