if (api.isSyntaxCheck()) return null

if (api.global.init == null || api.global.init == false) {
  api.global.plPriceCache = [:]
  api.global.standardRebateCache = [:]
  api?.global?.init = true
}