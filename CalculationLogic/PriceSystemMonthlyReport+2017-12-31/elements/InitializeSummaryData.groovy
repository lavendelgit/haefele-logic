if (!api.local.allColumnsPostFirstColumn) {
	def filters = [
    	api.local.salesOfficeFilter,
  		Filter.isNotNull("yearMonth"),
  		api.getElement("DateFilterForData")
	]

	def dmCtx = api.getDatamartContext()
	def datamartQuery = dmCtx.newQuery(dmCtx.getTable(api.local.datamart))
	datamartQuery.select("YearMonth", "yearMonth")
	datamartQuery.select("COUNT(CustomerId)", "count")
	datamartQuery.select("SUM(Revenue)", "revenue")
	datamartQuery.select("SUM(MarginAbs)", "marginAbs")
	datamartQuery.orderBy("yearMonth asc")
	datamartQuery.where(*filters)
	def result = dmCtx.executeQuery(datamartQuery)
	if (result == null) {
    	api.throwException("Could not get result for Sales query.")
	}

	def yearAgo = api.getElement("YearAgo")
  	def startingDate = yearAgo
	def latestDate = yearAgo
	def secondLatestDate = yearAgo
	def columnList = []
	result?.getData()
	   	  ?.collect{ it ->
         	def currentDate = it.yearMonth
         	if (!(currentDate instanceof Date)) {
              	api.throwException("Error", "Expected fetched YearMonth field to be of type Date, as this is basis of logic. Since its not date, aborting the logic...")
         	}
         	currentDate = Util.getGermanMonthName(currentDate)
	        if (latestDate < it.yearMonth) {
		   		secondLatestDate = latestDate
           		latestDate = it.yearMonth
         	}
            
            if (startingDate > it.yearMonth) {
              	startingDate = it.yearMonth
            }
         	columnList.add(currentDate)
		}

  	api.local.lastDateColumn = Util.getGermanMonthName(latestDate)
	api.local.secondLastDateColumn = Util.getGermanMonthName(secondLatestDate)
	api.local.lastColumnName = "Abweichung zum Vormonat ($api.local.lastDateColumn vs $api.local.secondLastDateColumn)"
  	api.local.startDateForData = startingDate
  	columnList.add(api.local.lastColumnName.toString())
  	api.trace ("Printing the content of column Names", api.local.lastColumnName + "          +" + columnList)
  	api.local.allColumnsPostFirstColumn = columnList
}
return api.local.allColumnsPostFirstColumn