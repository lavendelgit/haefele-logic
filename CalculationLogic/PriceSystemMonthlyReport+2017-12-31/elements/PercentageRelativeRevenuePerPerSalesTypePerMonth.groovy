import net.pricefx.common.api.FieldFormatType

def data = api.getElement("DataPerSalesTypeGroupPerMonth")
return CommonTableData.renderTable ("Umsatz zu Rabattstaffel (Relativ)", "Umsatz zu Rabattstaffel (Relativ)", data, "revenuePer", "%", FieldFormatType.PERCENT, "PSD_1_UR_REL")