def summaryData = api.getElement("PricePerMonthSummaryData")

final String header = "Hauptzahlen"
def columnHeader = [header]
def row1 =[:]
def row2 =[:]
def row3 =[:] 
row1[header] = "Umsatz HDE Inland"
row2[header] = "DB(€)"
row3[header] = "DB(%)"

summaryData.each {key, value -> api.trace (""+key, ""+ value)
	columnHeader.add (key)
  	def appendChar = (key.contains(api.local.lastColumnName))?"%" : "€"
  	row1[key] = value.revenue +  appendChar
  	row2[key] = value.margin + appendChar
  	row3[key] = value.marginper + "%"
    }

def resultMatrix = api.newMatrix (columnHeader)
boolean isFirst = true
for(column in columnHeader) {
  if (isFirst) {
      resultMatrix.setColumnFormat(column, FieldFormatType.TEXT)
      isFirst = false
  } else {
	  resultMatrix.setColumnFormat(column, FieldFormatType.MONEY)
  }
}
resultMatrix.setPreferenceName("PSD_1_Hauptzahlen")
resultMatrix.addRow (row1)
resultMatrix.addRow (row2)
resultMatrix.addRow (row3)

return resultMatrix