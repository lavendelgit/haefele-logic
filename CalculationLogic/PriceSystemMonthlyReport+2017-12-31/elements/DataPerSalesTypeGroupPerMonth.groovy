def salesTypes = api.getElement("SalesType")
def salesTypeGroup = salesTypes.collectEntries { salesType, meta -> [salesType, meta.labelInsideOutside] }

def insideOutsideDataPerMonth = [:].withDefault { key -> [:] }
def perYearMonthRevenueSumTotalAcrossSalesType = [:]
def perYearMonthCountSumTotalAcrossSalesType = [:]

def dataPerMonth = api.getElement("DataPerSalesTypePerMonth")
def currentDate
//Iterate over each month per sales type, compute the sub-salesType Grouping related statistics and totals across
//sales type group per yearMonth 
dataPerMonth.forEach { salesType, salesTypeData ->
    def group = salesTypeGroup[salesType] ?: "Undefined!"
    if (group == "Undefined!") {
        return
    }
    def groupData = insideOutsideDataPerMonth[group]
    salesTypeData.forEach { d ->
        currentDate = d.yearMonth
        currentDate = Util.getGermanMonthName(currentDate)
        def groupMonthData = groupData[currentDate]
        if (!groupMonthData) {
            groupData[currentDate] = [
                    yearMonth : currentDate,
                    countAbs  : d.count,
                    revenueAbs: (d.revenue) ?: 0.0,
                    countPer  : 0,
                    revenuePer: 0,
                    marginAbs : d.marginAbs ?: 0.0,
                    salesType : group
            ]
            if (perYearMonthRevenueSumTotalAcrossSalesType[currentDate] == null) {
                perYearMonthRevenueSumTotalAcrossSalesType[currentDate] = 0
                perYearMonthCountSumTotalAcrossSalesType[currentDate] = 0
            }

            perYearMonthRevenueSumTotalAcrossSalesType[currentDate] += d.revenue
            perYearMonthCountSumTotalAcrossSalesType[currentDate] += d.count
        } else {
            groupMonthData.countAbs += d.count
            groupMonthData.revenueAbs += (d.revenue) ?: 0.0
            groupMonthData.marginAbs += d.marginAbs ?: 0.0
            perYearMonthRevenueSumTotalAcrossSalesType[currentDate] += d.revenue
            perYearMonthCountSumTotalAcrossSalesType[currentDate] += d.count
        }
    }
}

//Iterate over each sales Type group per month compute the individual percentage
def totalRevenue, totalCount
insideOutsideDataPerMonth.forEach { group, groupData ->
    groupData.forEach { key, value ->
        totalRevenue = perYearMonthRevenueSumTotalAcrossSalesType[key]
        totalCount = perYearMonthCountSumTotalAcrossSalesType[key]
        value.countPer = new BigDecimal((value.countAbs / totalCount) * 100).setScale(2, BigDecimal.ROUND_HALF_UP)
        value.revenuePer = new BigDecimal((value.revenueAbs / totalRevenue) * 100).setScale(2, BigDecimal.ROUND_HALF_UP)
    }
}
//Construct the last column computing the difference for the latest two entries in percentages
def absRevDiff = [:]
def perRevDiff = [:]
def absCountDiff = [:]
def perCountDiff = [:]
def groups = []

int i = 0
def latestRecord, secondLastestRecord
/*
api.trace("Post details... Structure of insideOutsideDataPerMonth", " " + insideOutsideDataPerMonth)
api.trace("The last Date Column ", "api.local.lastDateColumn " + api.local.lastDateColumn + " api.local.secondLastDateColumn " + api.local.secondLastDateColumn)
*/

insideOutsideDataPerMonth.forEach { group, groupData ->
    latestRecord = groupData[api.local.lastDateColumn]
    secondLastestRecord = groupData[api.local.secondLastDateColumn]
    groups[i] = group
    if (latestRecord && secondLastestRecord) {
        absRevDiff[group] = (((latestRecord.revenueAbs - secondLastestRecord.revenueAbs) / secondLastestRecord.revenueAbs * 100) as BigDecimal).setScale(2, BigDecimal.ROUND_HALF_UP)
        perRevDiff[group] = latestRecord.revenuePer - secondLastestRecord.revenuePer
        absCountDiff[group] = (((latestRecord.countAbs - secondLastestRecord.countAbs) / secondLastestRecord.countAbs * 100) as BigDecimal).setScale(2, BigDecimal.ROUND_HALF_UP)
        perCountDiff[group] = latestRecord.countPer - secondLastestRecord.countPer
    } else {
        absRevDiff[group] = BigDecimal.ZERO
        perRevDiff[group] = 0
        absCountDiff[group] = BigDecimal.ZERO
        perCountDiff[group] = 0
    }
    i++
}

def tempGroup
for (group in groups) {
    tempGroup = insideOutsideDataPerMonth[group]
    tempGroup[api.local.lastColumnName] = [
            yearMonth : api.local.lastColumnName,
            countAbs  : absCountDiff[group],
            revenueAbs: absRevDiff[group],
            countPer  : perCountDiff[group],
            revenuePer: perRevDiff[group],
            marginAbs : 0.0,
            salesType : group
    ]
}

return insideOutsideDataPerMonth.collectEntries { key, value ->
    return [key, value.values()]
}