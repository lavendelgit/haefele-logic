if (!api.local.dataDateRange) {
	def currentCalender = api.calendar()
	currentCalender.set(Calendar.DAY_OF_MONTH, 1)
	currentCalender.set(Calendar.HOUR, 0)
	currentCalender.set(Calendar.MINUTE, 0)
	currentCalender.set(Calendar.SECOND, 0)
	def endMonth = currentCalender.getTime()
	def startMonth = api.local.startDateForData
	def monthList = []
	api.trace("Dates..did you find it correct ", startMonth?.format("dd/MM/YYYY hh:mm:ss") + " " + endMonth?.format("dd/MM/YYYY hh:mm:ss"))

	while (endMonth >= startMonth) {
  		monthList.add(currentCalender.getTime())
  		currentCalender.add(Calendar.MONTH, -1)
  		endMonth = currentCalender.getTime()
	}
  	api.local.dataDateRange = monthList
}

//api.trace("Dates... for your reference" + , " "+ api.local.dataDateRange)

return api.local.dataDateRange
