import net.pricefx.common.api.FieldFormatType

def data = api.getElement("DataPerSalesTypeGroupPerMonth")
return CommonTableData.renderTable ("Bestellpositionen zu Rabattstaffel (Relativ)", "Bestellpositionen zu Rabattstaffel (Relativ)", data, "countPer", "%", FieldFormatType.PERCENT,"PSD_1_BR_REL")