def filters = [
    api.local.salesOfficeFilter,
    Filter.greaterThan("Revenue", 0),
    Filter.greaterThan("MarginAbs", 0),
 	Filter.isNotNull("yearMonth"),
  	Filter.isNotNull("SalesType"),
    api.getElement("DateFilterForData")
]

def datamartQuery = api.local.dmCtx.newQuery(api.local.transactionTableContext)
datamartQuery.select("YearMonth", "yearMonth")
datamartQuery.select("SalesType", "salesType")
datamartQuery.select("COUNT(CustomerId)", "count")
datamartQuery.select("SUM(Revenue)", "revenue")
datamartQuery.select("SUM(MarginAbs)", "marginAbs")
datamartQuery.orderBy("yearMonth asc")
datamartQuery.where(*filters)

def result = api.local.dmCtx.executeQuery(datamartQuery)
if (result == null) {
    api.throwException("Could not get result for Sales Type Group query.")
}

api.trace("result.summary", "", result?.getSummary())
result = result?.getData()
	?.collect ()
	?.groupBy { r -> r.salesType ?: "Unknown"}

api.trace("result Structure", " result value" + result)
return result