//get the columns
def columnHeader = Util.getColumnHeader ("Abgespeicherte Preise")
//get data for each of the row
def zpapRowInput = api.getElement("ZPAPHistory")
def clearingRebateRowInput = api.getElement("ClearingRebateHistory")

def zpapRow = [:]
def clearingRebateRow = [:]
def isFirst = true
def resultMatrix = api.newMatrix (columnHeader)
resultMatrix.setPreferenceName("PSD_1_AP")
for(column in columnHeader) {
  	zpapRow[column] = zpapRowInput[column]?:0
  	clearingRebateRow[column] = clearingRebateRowInput[column]?:0
    if (isFirst) {
      		resultMatrix.setColumnFormat(column, FieldFormatType.TEXT)
	    	isFirst = false
  	} else {
        if (column.contains(api.local.lastColumnName)) 
		    resultMatrix.setColumnFormat(column, FieldFormatType.PERCENT)    
   		else
	  		resultMatrix.setColumnFormat(column, FieldFormatType.NUMERIC)
    }
}
resultMatrix.addRow (zpapRow)    
resultMatrix.addRow (clearingRebateRow)    

return resultMatrix