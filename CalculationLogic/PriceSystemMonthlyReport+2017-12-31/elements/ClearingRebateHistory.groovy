//Logic: Get the details of the calendar date range that we have to show
def monthList = api.getElement("MonthsForData")
def resultValue = [:]

//For each month query the count of effective ZPAPs using the query for a particular table.
def resultMap = [:]
final String fromDate = "attribute2"
final String toDate = "attribute3"
def keyGermanDate, currentDate, nextMonthDate, crCount
def fromDateIfEarlierThanStartToDateShouldOverlap, fromDateIfFurtherThanStartShouldOverlap
monthList.each { 
  currentDate = it.format("yyyy-MM-dd")
  nextMonthDate = Util.getNextMonth(it)
  fromDateIfEarlierThanStartToDateShouldOverlap = Filter.and(Filter.lessOrEqual(fromDate, currentDate), Filter.greaterOrEqual(toDate, currentDate))
  fromDateIfFurtherThanStartShouldOverlap = Filter.and(Filter.greaterThan(fromDate, currentDate), Filter.lessThan(fromDate, nextMonthDate))
  def crfilters = [
      Filter.equal("name", "ClearingRebate"),
      Filter.or(fromDateIfEarlierThanStartToDateShouldOverlap, 		
      			fromDateIfFurtherThanStartShouldOverlap
               )
     ]
  crCount = api.count("PX", *crfilters)
  keyGermanDate = Util.getGermanMonthName(it)
  resultMap[keyGermanDate] = crCount

  api.trace ("Clearing Rebate...", keyGermanDate + " result (" + crCount +")")
}
//Create the row Index Column 
resultMap[api.local.configTableFirstColumn] = api.local.countOfClearingRebateConfigRowLabel
//Create the final column which highlights the difference of the last row.
Util.populateTheLatestValueDifferenceForARow (resultMap)
api.trace ("The final result ", " " + resultMap)
return resultMap