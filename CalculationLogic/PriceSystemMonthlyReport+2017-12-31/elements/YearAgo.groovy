if (!api.local.yearAgoDate) {
	def calendar = api.calendar()
	calendar.add(Calendar.MONTH, -12)
	calendar.set(Calendar.DAY_OF_MONTH, 1)
	api.local.yearAgoDate = calendar.getTime()  
}

return api.local.yearAgoDate