//Logic: Get the details of the calendar date range that we have to show
def monthList = api.getElement("MonthsForData")
def resultValue = [:]

//For each month query the count of effective ZPAPs using the query for a particular table.
def resultMap = [:]
final String fromDate = "attribute2"
final String toDate = "attribute3"
def keyGermanDate, currentDate, nextMonthDate, zpapCount
def fromDateIfEarlierThanStartToDateShouldOverlap, fromDateIfFurtherThanStartShouldOverlap
monthList.each {
  zpapCount = 0
  currentDate = it.format("yyyy-MM-dd")
  nextMonthDate = Util.getNextMonth(it)
  fromDateIfEarlierThanStartToDateShouldOverlap = Filter.and(Filter.lessOrEqual(fromDate, currentDate), Filter.greaterOrEqual(toDate, currentDate))
  fromDateIfFurtherThanStartShouldOverlap = Filter.and(Filter.greaterThan(fromDate, currentDate), Filter.lessThan(fromDate, nextMonthDate))
  def zPAPfilters = [
      Filter.equal("name", "CustomerSalesPrice"),
      Filter.or(fromDateIfEarlierThanStartToDateShouldOverlap, 		
      			fromDateIfFurtherThanStartShouldOverlap
               ),
      Filter.in("attribute12",["Kaltenkirchen", "Hannover","Berlin","Münster","Köln","Frankfurt","Naumburg","Nürnberg","Stuttgart Airport","München","Handel"])
     ]
  zpapCount = api.count("PX", *zPAPfilters)
  keyGermanDate = Util.getGermanMonthName(it)
  resultMap[keyGermanDate] = zpapCount

  api.trace ("Counts..." + api.local.pxZPAPProductExtension , keyGermanDate + " result (" + zpapCount +") ("+ currentDate + ") (" + nextMonthDate +")")
}
//Create the row Index Column 
resultMap[api.local.configTableFirstColumn] = api.local.countOfZPAPConfigRowLabel
//Create the final column which highlights the difference of the last row.
Util.populateTheLatestValueDifferenceForARow (resultMap)

//Construct the row for it.
api.trace ("The final result ", " " + resultMap)
return resultMap