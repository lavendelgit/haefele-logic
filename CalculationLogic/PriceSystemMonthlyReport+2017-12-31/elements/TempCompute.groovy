
//"dd/MM/yyyy").parse("01/06/2019") 
def currentDate = api.calendar()
currentDate.set(Calendar.MONTH, 5)
currentDate.set(Calendar.DAY_OF_MONTH, 1)
currentDate.set(Calendar.YEAR, 2019)
currentDate = currentDate.getTime()

def nextMonthDate = Util.getNextMonth(currentDate)
currentDate = currentDate.format("yyyy-MM-dd")

final String fromDate = "attribute2"
final String toDate = "attribute3"
def fromDateIfEarlierThanStartToDateShouldOverlap = Filter.and(Filter.lessOrEqual(fromDate, currentDate), Filter.greaterOrEqual(toDate, currentDate))
def fromDateIfFurtherThanStartShouldOverlap = Filter.and(Filter.greaterThan(fromDate, currentDate), Filter.lessThan(fromDate, nextMonthDate))
def zPAPfilters = [
     Filter.equal("name", "CustomerSalesPrice"),
     Filter.or(fromDateIfEarlierThanStartToDateShouldOverlap, 		
      			fromDateIfFurtherThanStartShouldOverlap
      ),
     Filter.in("attribute12",["Kaltenkirchen", "Hannover","Berlin","Münster","Köln","Frankfurt","Naumburg","Nürnberg","Stuttgart Airport","München","Handel"])
    ]

zpapCount = api.count("PX", *zPAPfilters)
api.trace ("Counting Count", zpapCount + " " + currentDate + " " + nextMonthDate)
return zpapCount