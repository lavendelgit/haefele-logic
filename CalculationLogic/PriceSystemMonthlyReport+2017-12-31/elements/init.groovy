api.local.lastOneYear = "Last 12 months"
api.local.monthGermanNames = ["January":"Januar", "February":"Februar", "March":"März","April":"April","May":"Mai", "June":"Juni", "July":"Juli", "August":"August", "September":"September", "October":"Oktober", "November":"November", "December":"Dezember"]

api.local.dateformat = "yyyy-MM-dd"


api.local.salesOfficeFilter = Filter.in("SalesOffice","DE01", "DE02", "DE03", "DE04", "DE05", "DE06", "DE07", "DE08", "DE09", "DE10", "DE20")

api.local.datamart = "TransactionsDM"
api.local.dmCtx = api.getDatamartContext()
api.local.dm = api.local.dmCtx.getDatamart(api.local.datamart)
api.local.tableContext = api.getTableContext()
api.local.transactionTableContext = api.local.dmCtx.getTable(api.local.datamart)

api.local.ppZPAPHistoryTable = "MonthlyZPAPsPerSalesOffice"
api.local.ppZPAPTempTable = "ZPAPSummaryPerMonth"
api.local.ppClearingRebatesHistoryTable = "ClearingRebatesPerSalesOfficeHistory"
api.local.ppClearingRebatesHistoryTempTable = "ClearingRebatesTempHistory"

api.local.countOfClearingRebateConfigRowLabel = "Clearing Rabatte"
api.local.countOfZPAPConfigRowLabel = "Nettopreis (ZPAP - DE01-DE10, DE20)"
api.local.configTableFirstColumn = "Abgespeicherte Preise"

api.local.startDateForData 
api.local.dataDateRange = []
api.local.pxZPAPProductExtension = "CustomerSalesPrice"