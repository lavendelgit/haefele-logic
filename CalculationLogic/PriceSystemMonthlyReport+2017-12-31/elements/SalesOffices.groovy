if (!api.local.salesOfficeNames) {
    def salesOfficeMap = api.findLookupTableValues("SalesOffices").collectEntries {
         [(it.name): it.value]
   	}
  	def salesOfficeList = []  
  	salesOfficeMap.each { key, value ->
    	salesOfficeList.add(value)
  	}
  	api.local.salesOfficeNames = salesOfficeList
}

return api.local.salesOfficeNames
