def getGermanMonthName(Date inputDate) {
  return api.local.monthGermanNames[inputDate?.format("MMMM")] + "-" + inputDate?.format("yyyy")
}

def getColumnHeader (firstColumnHeader) {
	if (!api.local.columnHeaderStartingFromFirstToLast || api.local.columnHeaderStartingFromFirstToLast[0] != firstColumnHeader) {
  		def columnHeader = [firstColumnHeader]
		columnHeader.addAll(api.local.allColumnsPostFirstColumn)
    	api.local.columnHeaderStartingFromFirstToLast = columnHeader
   	}
	return api.local.columnHeaderStartingFromFirstToLast
}

def populateTheLatestValueDifferenceForARow (inputDataMap) {
  def lastValue = inputDataMap[api.local.lastDateColumn]?:0
  def secondLastValue = inputDataMap[api.local.secondLastDateColumn]?:0
  inputDataMap[api.local.lastColumnName] = (secondLastValue > 0) ? ((new BigDecimal(((lastValue - secondLastValue)/secondLastValue) +"")).setScale(2, BigDecimal.ROUND_HALF_UP)) : "100%"
}

def getNextMonth (currentDate) {
  def nextMonthStartDate = api.calendar()
  nextMonthStartDate.setTime(currentDate)
  nextMonthStartDate.add(Calendar.MONTH, 1)
  nextMonthStartDate = nextMonthStartDate?.getTime()
  return nextMonthStartDate.format("yyyy-MM-dd")
}
