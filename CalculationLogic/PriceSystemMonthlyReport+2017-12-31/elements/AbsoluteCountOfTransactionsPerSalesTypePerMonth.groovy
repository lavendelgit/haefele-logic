import net.pricefx.common.api.FieldFormatType

def data = api.getElement("DataPerSalesTypeGroupPerMonth")
return CommonTableData.renderTable ("Bestellpositionen zu Rabattstaffel (Absolut)", "Bestellpositionen zu Rabattstaffel (Absolut)", data, "countAbs", "", FieldFormatType.NUMERIC,"PSD_1_BR_ABS")