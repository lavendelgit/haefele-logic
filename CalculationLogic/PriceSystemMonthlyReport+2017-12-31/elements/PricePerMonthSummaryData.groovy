if (!api.local.pricePerMonthSummaryData) {
	def filters = [
    	api.local.salesOfficeFilter,
  		Filter.isNotNull("yearMonth"),
  		api.getElement("DateFilterForData")
	]

	def dmCtx = api.getDatamartContext()
	def datamartQuery = dmCtx.newQuery(dmCtx.getTable("TransactionsDM"))
	datamartQuery.select("YearMonth", "yearMonth")
	datamartQuery.select("COUNT(CustomerId)", "count")
	datamartQuery.select("SUM(Revenue)", "revenue")
	datamartQuery.select("SUM(MarginAbs)", "marginAbs")
	datamartQuery.orderBy("yearMonth asc")
	datamartQuery.where(*filters)

	def result = dmCtx.executeQuery(datamartQuery)
	if (result == null) {
    	api.throwException("Could not get result for Sales query.")
	}

	def yearAgo = api.getElement("YearAgo")
	def resultMap = [:]
	result?.getData()
	   	  ?.collect{ it ->
         	def currentDate = it.yearMonth
         	currentDate = Util.getGermanMonthName(currentDate)
         	def marginPercentage =0
         	if (it.revenue >0) {
           		marginPercentage = new BigDecimal(it.marginAbs/it.revenue).setScale(2, BigDecimal.ROUND_HALF_UP)
	        }
         	resultMap[currentDate] = [
              "month":currentDate,
              "revenue":new BigDecimal(it.revenue).setScale(2, BigDecimal.ROUND_HALF_UP),
              "margin":new BigDecimal(it.marginAbs).setScale(2, BigDecimal.ROUND_HALF_UP),
              "marginper":marginPercentage
            ]
	}

	def latestMonthRec = resultMap[api.local.lastDateColumn]
	def previousToLatestMonthRec = resultMap[api.local.secondLastDateColumn]
	def revenuePer = new BigDecimal((latestMonthRec?.revenue - previousToLatestMonthRec?.revenue)/previousToLatestMonthRec?.revenue *100).setScale(2, BigDecimal.ROUND_HALF_UP)
	def margin = new BigDecimal((latestMonthRec?.margin - previousToLatestMonthRec?.margin)/previousToLatestMonthRec?.margin *100).setScale(2, BigDecimal.ROUND_HALF_UP)
	def marginPer = new BigDecimal((latestMonthRec?.marginper - previousToLatestMonthRec?.marginper)).setScale(2, BigDecimal.ROUND_HALF_UP) 
	resultMap[api.local.lastColumnName] = [
  			month:api.local.lastColumnName,
			revenue:revenuePer,
  			margin:margin,
  			marginper:marginPer
	]
	api.local.pricePerMonthSummaryData = resultMap  
}
return api.local.pricePerMonthSummaryData