import net.pricefx.common.api.FieldFormatType

def renderTable (title, firstColumnLabel, data, property, suffix, dataColumnFormat, preferencefName) {
  	def rows = data.keySet()
  	if (rows.size() == 0)
  		api.throwException ("Invalid data supplied for rendering table ("+title+")") 
	
	final String header = title
	def columnHeader = [firstColumnLabel]
  	columnHeader.addAll (api.local.allColumnsPostFirstColumn)
  	def firstRow = true
  	def renderedTableRows = []
  
  	api.trace ("Printing input data", "Title("+title+") data ("+data+") firstColumnLabel ("+ firstColumnLabel+") property("+property+")")
  	def columnSuffix = false
  	data.each { rowKey, tableData -> 
			def rowData = [:]
			rowData[firstColumnLabel] = rowKey
      		if (firstRow) {
        		firstRow = false
              	tableData.each { columnValues ->
                  	columnSuffix = (columnValues.yearMonth.contains(api.local.lastColumnName))? "%":suffix
                  	rowData[columnValues.yearMonth] = columnValues.get(property) + columnSuffix
                }
            } else {
	            tableData.each { columnValues ->
                  	columnSuffix = (columnValues.yearMonth.contains(api.local.lastColumnName))? "%":suffix
                  	rowData[columnValues.yearMonth] = columnValues.get(property) + columnSuffix
            	}
            }
            renderedTableRows.add (rowData)
	} 
  	
  	def resultMatrix = api.newMatrix (columnHeader)
	resultMatrix.setPreferenceName(preferencefName)
	def isFirst = true
	for(column in columnHeader) {
      	if (isFirst) {
      			resultMatrix.setColumnFormat(column, FieldFormatType.TEXT)
		    	isFirst = false
  		} else {
              	if (column.contains(api.local.lastColumnName)) {
		    		resultMatrix.setColumnFormat(column, FieldFormatType.PERCENT)    
   				} else {
	  				resultMatrix.setColumnFormat(column, dataColumnFormat)
      			}
          }
    }
  	renderedTableRows.each { currentRow ->
		resultMatrix.addRow (currentRow)    
    }
 
	return resultMatrix 
}
