def timeFrame = api.getElement("TimeFrame")
if (timeFrame == api.local.lastOneYear) {
  	def yearAgo = api.getElement("YearAgo")
    return Filter.greaterOrEqual("InvoiceDate", yearAgo?.format(api.local.dateformat))
} else {
	return Filter.isNotNull("InvoiceDate")  
}
