import net.pricefx.common.api.FieldFormatType

def data = api.getElement("DataPerSalesTypeGroupPerMonth")
return CommonTableData.renderTable ("Umsatz zu Rabattstaffel (Absolut)", "Umsatz zu Rabattstaffel (Absolut)", data, "revenueAbs", "€", FieldFormatType.MONEY, "PSD_1_UR_ABS")