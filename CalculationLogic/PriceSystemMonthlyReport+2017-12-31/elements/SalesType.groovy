api.findLookupTableValues("SalesTypes").collectEntries { pp -> [ (pp.name) : 
   [    description: pp.attribute1,
        insideOutside: pp.attribute2,
        priceIncreaseAffected: pp.attribute3,
  		labelInsideOutside: pp.attribute4
    ]]}