package Dashboard_ClearingRebatesPerCustomer.elements

def clearingRebates = api.getElement("ClearingRebates")
def treshold = api.getElement("ShowCountTreshold");

return clearingRebates
        .countBy { clearingRebate -> clearingRebate.attribute1 }
        .findAll { e -> e.value > treshold }
        .sort { e1, e2 -> e2.value <=> e1.value }