package Dashboard_ClearingRebatesPerCustomer.elements

def resultMatrix = api.newMatrix("Customer Number","Count of Clearing Rebates")
resultMatrix.setTitle("Count of Clearing Rebates per Customer")
resultMatrix.setColumnFormat("Count of Clearing Rebates", FieldFormatType.NUMERIC)

def clearingRebatesPerCustomer = api.getElement("ClearingRebatesPerCustomer")

for (row in clearingRebatesPerCustomer) {
    resultMatrix.addRow([
            "Customer Number" : row.key,
            "Count of Clearing Rebates" : row.value
    ])
}

return resultMatrix
