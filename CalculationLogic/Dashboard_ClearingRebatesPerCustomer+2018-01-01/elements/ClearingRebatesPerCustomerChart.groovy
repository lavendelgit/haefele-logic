package Dashboard_ClearingRebatesPerCustomer.elements

def clearingRebatesPerCustomer = api.getElement("ClearingRebatesPerCustomer");
def top1000 = clearingRebatesPerCustomer.take(1000) // turboTreshold

def categories = top1000.collect { e -> e.key}
def data = top1000.collect { e -> e.value};

def chartDef = [
        chart: [
            type: "column",
            zoomType: "x"
        ],
        title: [
            text: "Clearing Rebates per Customer"
        ],
        xAxis: [[
            title: [
                    text: "Customer Number"
            ],
            categories: categories
        ]],
        yAxis: [[
            title: [
                text: "Number of Clearing Rebates"
            ]
        ]],
        tooltip: [
            headerFormat: "Customer Number: {point.key}<br/>",
            pointFormat: "Number of Clearing Rebates: {point.y}"
        ],
        legend: [
            enabled: false
        ],
        series: [[
             data: data
        ]]
]

if (clearingRebatesPerCustomer?.size() > 1000) {
    chartDef.subtitle = [
        text: "Top 1000"
    ]
}

api.buildFlexChart(chartDef)