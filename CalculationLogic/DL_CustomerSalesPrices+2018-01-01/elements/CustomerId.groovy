def inputCustomerId = api.currentItem()
if (!inputCustomerId && api.global.isDevelopment) {
    inputCustomerId = ["customerId" : "0001000217"]
}
inputCustomerId = (inputCustomerId) ? inputCustomerId["customerId"] : ""
return inputCustomerId