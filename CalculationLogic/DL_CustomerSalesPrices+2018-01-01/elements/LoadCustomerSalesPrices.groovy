def productSuppliers = api.global.productSuppliers

def getMatchType(def customerSalesPrice, def discountPercentage) {
    if (!discountPercentage || !customerSalesPrice) {
        return 'Unknown'
    }
    BigDecimal custSales = BigDecimal.ZERO
    if (!customerSalesPrice.toString().trim().equals("")) {
        custSales = customerSalesPrice.toBigDecimal()
    }
    if (custSales < 0) {
        return 'NZE' //Negative ZPAP Entry
    }
    String matchType
    BigDecimal valueToCompare = BigDecimal.ZERO
    if (!discountPercentage.toString().trim().equals("")) {
        valueToCompare = discountPercentage.toBigDecimal()
    }
    if (!api.global.MaxDiscountPer) {
        api.global.MaxDiscountPer = lib.GeneralSettings.percentageValue("MaxDiscountPct")
    }
    BigDecimal maxDiscountPerc = api.global.MaxDiscountPer
    //HLM- Highly Loss Making, ASP- Above Sales Price, HPL- Higher Percentage Discount within Limit
    matchType = (valueToCompare > maxDiscountPerc) ? 'HLM' : ((valueToCompare < 0) ? 'ASP' : 'HPL')

    return matchType
}

def getConsolidatedTransactions(String customerId, String material, String validFrom, String validTo) {
    def transactions = [
            'Revenue': 0.0 as BigDecimal,
            'Margin' : 0.0 as BigDecimal,
            'Units'  : 0
    ]
    if (!validFrom || !validTo) {
        return transactions
    }

    def customerTransList = api.global.customerTransactions
    // def customerTransList = TransactionsForCustomer.getTransactionForCustomer (customerId)
    Date validStrDt = (validFrom && (validFrom instanceof Date)) ? validFrom : Date.parse("yyyy-MM-dd", validFrom)
    Date validEndDt = (validTo && (validTo instanceof Date)) ? validTo : Date.parse("yyyy-MM-dd", validTo)
    customerTransList.each { entry ->
        if (entry.Article == material && entry.InvoiceDate >= validStrDt && entry.InvoiceDate <= validEndDt) {
            transactions.Revenue += entry.Revenue
            transactions.Margin += entry.Margin
            transactions.Units += entry.Units
        }
    }
    return transactions
}

def uploadBatch(int count, List target, boolean isFinal = false) {
    int batchLimit = 100000
    if (count > batchLimit || isFinal) {
        lib.DBQueries.storeData(target, "CustomerSalesPrice")
        count = 0
    }
    return count
}

List target = []
def result = null
def isTesting = api.isDebugMode()
try {
    result = api.stream("PX", 'attribute4' /* sort-by*/,
                        ["sku", "attribute4", "attribute1", "attribute2", "attribute11", "attribute3", "attribute25", "attribute26", "attribute28"],
                        Filter.equal("name", "S_ZPAP"),
                        Filter.equal("attribute4", out.CustomerId),
                                  Filter.like("attribute1", "____-__-__"))
   // api.trace("2",result)
    if (!result) {
        return false
    }

    int count = 0
    def isContinue = true
    def zpap, currentTransaction, row
    while (isContinue && result.hasNext()) {
        zpap = result.next()
        currentTransaction = getConsolidatedTransactions(zpap.attribute4, zpap.sku, zpap.attribute1, zpap.attribute2)
       api.trace("curr",currentTransaction)
        row = ["SourceID"          : zpap.attribute11,
               "Material"          : zpap.sku,
               "customerId"        : zpap.attribute4,
               "ValidFrom"         : zpap.attribute1,
               "ValidTo"           : zpap.attribute2,
               "CustomerSalesPrice": zpap.attribute3,
               "SupplierId"        : productSuppliers[zpap.sku] ?: "00000000",
               "DiscountABS"       : (zpap.attribute25) ?: 0.0,
               "DiscountPercentage": (zpap.attribute26) ?: 0.0,
               "MatchType"         : getMatchType(zpap.attribute3, zpap.attribute26),
               "SalesPrice"        : (zpap.attribute28) ?: 0.0,
               "Revenue"           : currentTransaction?.Revenue,
               "Margin"            : currentTransaction?.Margin,
               "Units"             : currentTransaction?.Units
        ]
        api.trace("3",row)
        target.add(row)
        count++
        isContinue = (isTesting && count > 20) ? false : isContinue
        count = uploadBatch(count, target)

    }
    uploadBatch(count, target, true)
} catch (Exception e) {
    lib.TraceUtility.developmentTraceException("Exception while computing ", e)
} finally {
    if (result) {
        result.close()
    }
}

if (isTesting) {
    lib.TraceUtility.developmentTraceList("Printing Values", target)
}
return true

