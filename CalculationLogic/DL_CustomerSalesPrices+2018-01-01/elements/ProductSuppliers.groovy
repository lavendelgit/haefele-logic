api.retainGlobal = true;

if (!api.global.productSuppliers) {
    def productStream = api.stream("P", null /* sort-by*/, ["sku", "attribute8"])
    def productSuppliers = [:]

    if (productStream) {
        while (productStream.hasNext()) {
            def product = productStream.next()
            productSuppliers.put(product.sku, product.attribute8)
        }

        productStream.close()
    }

    def compoundProductStream = api.stream("PX", null /* sort-by*/, ["sku", "attribute4"], Filter.equal("name", "CompoundPrice"))

    if (compoundProductStream) {
        while (compoundProductStream.hasNext()) {
            def product = compoundProductStream.next()
            productSuppliers.put(product.sku, product.attribute4)
        }
        compoundProductStream.close()
    }

    api.global.productSuppliers = productSuppliers
}

return null