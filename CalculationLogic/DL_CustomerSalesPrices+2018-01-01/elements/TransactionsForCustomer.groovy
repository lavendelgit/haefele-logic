api.retainGlobal = true

if (!api.global.customerTransactions) {
    def fields = ['Material'      : 'Article',
                  'CustomerId'    : 'CustomerId',
                  'InvoiceDate'   : 'InvoiceDate',
                  'SUM(Revenue)'  : 'Revenue',
                  'SUM(MarginAbs)': 'Margin',
                  'SUM(Units)'    : 'Units']
    def filters = [
            Filter.equal('CustomerId', out.CustomerId)
    ]

    lib.TraceUtility.developmentTrace("Printing date ", out.CustomerId)
    api.global.customerTransactions = lib.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')
}
return api.global.customerTransactions

/*
def getTransactionForCustomer (String customerId) {
    if (api.global.customerId && (customerId == api.global.customerId))
        return api.global.customerTransactions


    def fields = ['Material'      : 'Article',
                  'CustomerId'    : 'CustomerId',
                  'InvoiceDate'   : 'InvoiceDate',
                  'SUM(Revenue)'  : 'Revenue',
                  'SUM(MarginAbs)': 'Margin',
                  'SUM(Units)'    : 'Units']
    def filters = [
            Filter.equal('CustomerId', customerId)
    ]

    lib.TraceUtility.developmentTrace("Printing Customer ID ", customerId)
    api.global.customerTransactions = lib.DBQueries.getDataFromSource(fields, 'TransactionsDM', filters, 'DataMart')
    lib.TraceUtility.developmentTrace("Transaction List ", ((api.global.customerTransactions)?api.global.customerTransactions.size() : 0))
    api.global.customerId = customerId
    return api.global.customerTransactions
}
return true

 */