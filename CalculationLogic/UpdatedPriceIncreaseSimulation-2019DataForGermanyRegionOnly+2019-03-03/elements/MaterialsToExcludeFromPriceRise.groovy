if (!api.global.materialToExcludeSalesPriceIncrease) {
    api.global.materialToExcludeSalesPriceIncrease = api.findLookupTableValues("ExcludeMaterialFromPriceIncrease")
                                          .collectEntries { pp -> [ (pp.name) : [
                                                materialId: pp.name,
                                                increaseSalesPrice: pp.attribute1
                                          ]]}
  
    api.global.supplierMaterialToExcludeSalesPriceIncrease = api.findLookupTableValues("ExcludeMaterialForSupplierFromPriceIncrease")
                                          .collectEntries { pp -> [ (pp.value) : [
                                                supplierName: pp.name,
                                                supplierId: pp.value
                                          ]]}
    
}

api.trace("api.global.materialToExcludeSalesPriceIncrease","",api.global.materialToExcludeSalesPriceIncrease)
api.trace("api.global.supplierMaterialToExcludeSalesPriceIncrease","",api.global.supplierMaterialToExcludeSalesPriceIncrease)


return api.global.materialToExcludeSalesPriceIncrease