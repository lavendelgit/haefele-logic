def supplierId = api.getElement("SupplierId")
def productH2 = api.getElement("ProductH2") ?: "Not defined"
def materialStatusWerk = api.getElement("MaterialStatusWerk")

if (api.global.materialStatus[materialStatusWerk]?.priceIncreaseAffected == "No") {
    api.yellowAlert("Material Status not affected by sales price increase");
    return 0.0
}

if (api.global.supplierPriceIncrease[supplierId]?.increaseSalesPrice == "No") {
    api.yellowAlert("Supplier not affected by sales price increase");
    return 0.0
}

def isMaterialExcludedFromSalesPrice = api.getElement("IsMaterialShieldedFromPriceRise")
if (isMaterialExcludedFromSalesPrice) {
    api.yellowAlert("Material not affected by sales price increase through exclusion list configuration");
    return 0.0
}

def isTopCoProduct = api.getElement("IsTopCoProduct")
if (isTopCoProduct) {
    api.yellowAlert("Material not affected by sales price increase as its a Top Co Product");
    return 0.0
}

api.global.salesPriceIncrease[productH2]?.priceIncrease ?: 0.0