def priceIncreaseImpact = api.getElement("PriceIncreaseImpact")
def negativeExtremeImpacts = -api.getElement("ExtremeImpactThreshold")
def positiveExtremeImpact = +api.getElement("ExtremeImpactThreshold")

if (priceIncreaseImpact?.expectedImpactPerc == null) {
    return null
}

return priceIncreaseImpact.expectedImpactPerc < negativeExtremeImpacts || priceIncreaseImpact.expectedImpactPerc > positiveExtremeImpact