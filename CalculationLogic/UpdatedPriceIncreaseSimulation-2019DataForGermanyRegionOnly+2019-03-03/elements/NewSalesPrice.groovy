if (api.product("sku").endsWith("X")) {
    api.redAlert("X Article. Ignoring increased price since the average price form last year may be inaccurate.")
}

if (api.getElement("IsSamePrice")) {
    api.yellowAlert("Invoice prices are (nearly) the same. Ignoring the increase.")
}

if (api.getElement("IsNegativeUnits")) {
    api.yellowAlert("Negative units from linear regression. Ignoring the increase.")
}


return api.getElement("PriceIncreaseImpact").newSalesPrice