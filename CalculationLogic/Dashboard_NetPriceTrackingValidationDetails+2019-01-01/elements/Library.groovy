import net.pricefx.common.api.FieldFormatType

protected List getPxTableFields(Map pxTablesAttributes) {
    List fields = ["sku", pxTablesAttributes["costAttribute"], pxTablesAttributes["validFromAttribute"],
                   pxTablesAttributes["validToAttribute"], pxTablesAttributes["priceChangeTypeAttribute"],
                   pxTablesAttributes["perDiffAttribute"], pxTablesAttributes["previousCostAttribute"],
                   pxTablesAttributes["ppValidFromAttribute"], pxTablesAttributes["ppValidToAttribute"]]
    pxTablesAttributes["keyAttributesName"]?.split(",")?.each { cols ->
        fields << cols
    }
    return fields
}

protected List createPxFilter(String entity, Map pxTablesAttributes,
                              String attributePerDiff, String attributePriceChangeType) {

    List validationFilters = [Filter.equal("name", entity)]
    String attributeValidFrom = pxTablesAttributes["validFromAttribute"]
    String attributeValidTo = pxTablesAttributes["validToAttribute"]

    BigDecimal refPercentage = out.RefPercentage
    BigDecimal refPercentageValue = refPercentage / 100
    BigDecimal refPercentageGreaterThanValue = (refPercentage + 0.0005) / 100
    BigDecimal refPercentageLessThanValue = (refPercentage - 0.0005) / 100

    if (out.ValidFrom) {
        validationFilters << Filter.greaterOrEqual(attributeValidTo, out.ValidFrom)
    }
    /*if (out.ValidTo) {
        validationFilters << Filter.lessOrEqual(attributeValidFrom, out.ValidTo)
    }*/
    validationFilters << Filter.isNotNull(attributePriceChangeType)

    //validationFilters << Filter.in("sku", ['000.11.100','999.11.647','013.30.320'])// DON'T DELETE THIS REQUIRED FOR DEBUG

    switch (out.ValidationType?.trim()) {

        case libs.HaefeleCommon.PriceChangeTracking.VALIDATION_NO_PRICE_CHANGE:
            validationFilters << Filter.equal(attributePerDiff, 0.00)
            break

        case libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_REDUCTION:
            validationFilters << Filter.lessThan(attributePerDiff, 0)
            break

        case libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_INCREASE_ABOVE_REF + refPercentage.toString() + "%":
            validationFilters << Filter.greaterThan(attributePerDiff, refPercentageGreaterThanValue)
            break

        case libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_INCREASE_BELOW_REF + refPercentage.toString() + "%":
            validationFilters << Filter.lessThan(attributePerDiff, refPercentageLessThanValue)
            validationFilters << Filter.greaterThan(attributePerDiff, 0)
            break

        case libs.HaefeleCommon.PriceChangeTracking.VALIDATION_NO_VOILATION:
            validationFilters << Filter.equal(attributePriceChangeType, libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_INCREASE)
            validationFilters << Filter.greaterOrEqual(attributePerDiff, refPercentageLessThanValue)
            validationFilters << Filter.lessThan(attributePerDiff, refPercentageGreaterThanValue)
            break
    }

    return validationFilters
}

protected Map getMatrixColumns() {
    Map columnNamesAndTypes = [:]

    if (!out.PXTablesAttributes["keyAttributesName"]) {
        columnNamesAndTypes = ['Material Number'          : FieldFormatType.TEXT,
                               'KBETR'                    : FieldFormatType.NUMERIC,
                               'KBETR Valid From'         : FieldFormatType.DATE,
                               'KBETR Valid To'           : FieldFormatType.DATE,
                               'Previous KBETR'           : FieldFormatType.NUMERIC,
                               'Previous KBETR Valid From': FieldFormatType.DATE,
                               'Previous KBETR Valid To'  : FieldFormatType.DATE,
                               'Difference'               : FieldFormatType.NUMERIC,
                               '% Difference'             : FieldFormatType.PERCENT]
    } else {
        columnNamesAndTypes = ['Material Number': FieldFormatType.TEXT]

        columnNamesAndTypes << getKeyColumnsNamesAndTypes()

        Map kbetrColNamesAndTypes = ['KBETR'                    : FieldFormatType.NUMERIC,
                                     'KBETR Valid From'         : FieldFormatType.DATE,
                                     'KBETR Valid To'           : FieldFormatType.DATE,
                                     'Previous KBETR'           : FieldFormatType.NUMERIC,
                                     'Previous KBETR Valid From': FieldFormatType.DATE,
                                     'Previous KBETR Valid To'  : FieldFormatType.DATE,
                                     'Difference'               : FieldFormatType.NUMERIC,
                                     '% Difference'             : FieldFormatType.PERCENT]

        columnNamesAndTypes << kbetrColNamesAndTypes
    }
    return columnNamesAndTypes
}

protected Map getKeyColumnsNamesAndTypes() {
    Map keyColumnNamesAndTypes = [:]
    List keyColumnNames = out.PXTablesAttributes["keyAttributesColumnName"]?.split(",") as List

    Integer i = 0
    keyColumnNames.each {
        keyColumnNamesAndTypes << [(keyColumnNames[i]): FieldFormatType.TEXT]
        i++
    }
    return keyColumnNamesAndTypes
}

protected ResultMatrix addRowsToResultMatrix(String entity, List validationRecords) {

    BigDecimal currentPrice = 0.00
    BigDecimal previousPrice = 0.00
    def dashUtl = lib.DashboardUtility

    Map columnNamesAndTypes = getMatrixColumns()
    List cols = columnNamesAndTypes?.keySet() as List
    List colNames = columnNamesAndTypes?.values() as List
    def resultMatrix = dashUtl.newResultMatrix('Details: Validation Records Details', cols, colNames)

    if (!out.PXTablesAttributes["keyAttributesName"]) {
        validationRecords?.each { validationRecord ->
            currentPrice = (validationRecord[out.PXTablesAttributes["costAttribute"]] ?: 0) as BigDecimal
            previousPrice = (validationRecord[out.PXTablesAttributes["previousCostAttribute"]] ?: 0) as BigDecimal
            resultMatrix.addRow([
                    (cols[0]): validationRecord.sku,
                    (cols[1]): currentPrice,
                    (cols[2]): validationRecord[out.PXTablesAttributes["validFromAttribute"]],
                    (cols[3]): validationRecord[out.PXTablesAttributes["validToAttribute"]],
                    (cols[4]): previousPrice,
                    (cols[5]): validationRecord[out.PXTablesAttributes["ppValidFromAttribute"]],
                    (cols[6]): validationRecord[out.PXTablesAttributes["ppValidToAttribute"]],
                    (cols[7]): (currentPrice - previousPrice),
                    (cols[8]): (validationRecord[out.PXTablesAttributes["perDiffAttribute"]] as BigDecimal) //libs.SharedLib.RoundingUtils.round((validationRecord.attribute20) as BigDecimal,2)
                    //(columns[6]): recCount
            ])
        }
    } else {
        List keyColumnNames = out.PXTablesAttributes["keyAttributesName"]?.split(",") as List
        validationRecords?.each { validationRecord ->
            currentPrice = (validationRecord[out.PXTablesAttributes["costAttribute"]] ?: 0) as BigDecimal
            previousPrice = (validationRecord[out.PXTablesAttributes["previousCostAttribute"]] ?: 0) as BigDecimal
            def row = [:]
            Integer k = 0
            row << [(cols[k]): validationRecord.sku]
            for (Integer j = 0; j < keyColumnNames.size(); j++) {
                row << [(cols[j + 1]): validationRecord[keyColumnNames[j]]]
                k++
            }
            row << [(cols[k + 1]): currentPrice]
            row << [(cols[k + 2]): validationRecord[out.PXTablesAttributes["validFromAttribute"]]]
            row << [(cols[k + 3]): validationRecord[out.PXTablesAttributes["validToAttribute"]]]
            row << [(cols[k + 4]): previousPrice]
            row << [(cols[k + 5]): validationRecord[out.PXTablesAttributes["ppValidFromAttribute"]]]
            row << [(cols[k + 6]): validationRecord[out.PXTablesAttributes["ppValidToAttribute"]]]
            row << [(cols[k + 7]): (currentPrice - previousPrice)]
            row << [(cols[k + 8]): (validationRecord[out.PXTablesAttributes["perDiffAttribute"]] as BigDecimal)]
            resultMatrix.addRow(row)
        }
    }
    return resultMatrix
}