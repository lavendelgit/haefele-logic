import groovy.transform.Field

@Field final String PX_TABLE_FILTER_LABEL = "Pricing Conditions"
@Field final String PX_ZPAP_TABLE = "S_ZPAP"
@Field final String PX_ZPF_TABLE = "S_ZPF"
@Field final String PX_ZPM_TABLE = "S_ZPM"
@Field final String PX_ZPP_TABLE = "S_ZPP"
@Field final String PX_ZPA_TABLE = "S_ZPA"
@Field final String PX_ZPC_TABLE = "S_ZPC"
@Field final String PX_ZPZ_TABLE = "S_ZPZ"
@Field final String PX_ZNRV_TABLE = "S_ZNRV"
@Field final String PX_ZPE_TABLE = "S_ZPE"

@Field Map PX_TABLE_VALUES = [
        "ZPAP": PX_ZPAP_TABLE,
        "ZPF" : PX_ZPF_TABLE,
        "ZPM" : PX_ZPM_TABLE,
        "ZPP" : PX_ZPP_TABLE,
        "ZPA" : PX_ZPA_TABLE,
        "ZPC" : PX_ZPC_TABLE,
        "ZPZ" : PX_ZPZ_TABLE,
        "ZNRV": PX_ZNRV_TABLE,
        "ZPE" : PX_ZPE_TABLE
]