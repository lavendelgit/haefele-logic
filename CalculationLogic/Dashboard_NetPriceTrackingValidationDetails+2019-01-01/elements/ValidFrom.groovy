if (api.isDebugMode()) {
    return new Date().parse("yyyy-MM-dd", "2021-04-01")
}

return api.input("Report Date")