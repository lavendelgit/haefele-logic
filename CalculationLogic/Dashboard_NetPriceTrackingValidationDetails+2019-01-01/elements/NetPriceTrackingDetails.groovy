import net.pricefx.common.api.FieldFormatType

BigDecimal refPercentage = out.RefPercentage

String typeCode = out.PXTablesAttributes["tableTypeCode"]
String pxTableName = out.PXTablesAttributes["pxTablename"]
String attributePerDiff = out.PXTablesAttributes["perDiffAttribute"]
String attributePriceChangeType = out.PXTablesAttributes["priceChangeTypeAttribute"]

BigDecimal refPercentageValue = refPercentage / 100
BigDecimal refPercentageGreaterThanValue = (refPercentage + 0.0005) / 100
BigDecimal refPercentageLessThanValue = (refPercentage - 0.0005) / 100

List validationFilters = Library.createPxFilter(pxTableName, out.PXTablesAttributes,
        attributePerDiff, attributePriceChangeType)
api.logInfo("************ Price % report Details validationFilters", validationFilters)
def startRow = 0
List validationRecords = null
def pxStream = null

List fields = Library.getPxTableFields(out.PXTablesAttributes)
//api.logInfo("*****************fields",fields)

try {
    pxStream = api.stream(typeCode, "sku", fields, false, *validationFilters)
    validationRecords = pxStream?.collect()
} catch (ex) {
    api.logInfo("Exception in generateArticleIdsList", ex.getMessage())
} finally {
    pxStream?.close()
}
//api.logInfo("************validationRecords", validationRecords.size())
def resultMatrix
if (validationRecords) resultMatrix = Library.addRowsToResultMatrix(out.Entity, validationRecords)
resultMatrix?.setPreferenceName("Net Price Tracking Validation Details")

return resultMatrix