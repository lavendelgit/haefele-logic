def create(groupedRecords, property, type) {
    def stacking = [
            grouped: null,
            stacked: "normal",
            percent: "percent"
    ]
    def dataLabelsFormat = [
            count_grouped: "{point.y:,.0f}",
            count_stacked: "{point.y:,.0f}",
            count_percent: "{point.percentage:.2f}%",
            revenue_grouped: "{point.y:,.0f} €",
            revenue_stacked: "{point.y:,.0f} €",
            revenue_percent: "{point.percentage:.2f}%"
    ]
    def tooltipPointFormat = [
            count_grouped: 'Month: {point.name} <br/>' +
                    'Count: {point.y} <br/>',
            count_stacked: 'Month: {point.name} <br/>' +
                    'Count: {point.y} <br/>',
            count_percent: 'Month: {point.name} <br/>' +
                    'Count: {point.y} ({point.percentage:.1f}%) <br/>',
            revenue_grouped: 'Month: {point.name} <br/>' +
                    'Revenue: {point.y} EUR<br/> ',
            revenue_stacked: 'Month: {point.name} <br/> ' +
                    'Revenue: {point.y} EUR ({point.percentage:.1f}%)<br/>',
            revenue_percent: 'Month: {point.name} <br/>' +
                    'Revenue: {point.y} EUR ({point.percentage:.1f}%) <br/>',
    ]
    def yAxisTitle = [
            count: "Count of transactions",
            revenue: "Revenue"
    ]

    def salesTypeDetails = api.getElement("SalesTypes")

    def categories = groupedRecords.values()*.yearMonth.flatten().toSet().toSorted().takeRight(12)

    def series = groupedRecords
            .collect { salesType, records -> [
            name: salesType + (salesTypeDetails[salesType] ? ": " + salesTypeDetails[salesType] : ""),
            data: categories.collect { c ->
                def r = records.find {  r -> r.yearMonth == c }
                return [
                        name: c,
                        y:  r?.get(property),
                        marginAbs: r?.marginAbs,
                        marginPerc: r?.marginAbs?.div(r?.revenue)?.multiply(100)
                ]
            },
            dataLabels: [
                    enabled: true,
                    format: dataLabelsFormat["${property}_${type}"]
            ]
    ]}

    api.trace("categories", "", categories)
    api.trace("series", "", series)

    def chartDef = [
            chart: [
                    type: "column",
                    zoomType: "xy"
            ],
            credits: [
                    enabled: false
            ],
            colors: lib.Colors.palette(),
            title: [
                    text: ""
            ],
            xAxis: [[
                            title: [
                                    text: "Month"
                            ],
                            categories: categories
                    ]],
            yAxis: [[
                            title: [
                                    text: yAxisTitle[property]
                            ],
                            stackLabels: [
                                    enabled: true,
                                    allowOverlap: true
                            ]
                    ]],
            plotOptions: [
                    column: [
                            stacking: stacking[type]
                    ]
            ],
            tooltip: [
                    headerFormat: 'Sales Type: {series.name}<br/>',
                    pointFormat: tooltipPointFormat["${property}_${type}"]
            ],
            legend: [
                    enabled: true,
                    layout: "vertical",
                    align: "right",
                    verticalAlign: "middle"
            ],
            series: series
    ]

    return api.buildHighchart(chartDef)
}