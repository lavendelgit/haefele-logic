def today = api.targetDate()?.format("yyyy-MM-dd")

api.count("PX",
    Filter.equal("name", "ClearingRebate"),
    Filter.lessOrEqual("attribute2", today),   // attribute2 = valid from
    Filter.greaterOrEqual("attribute3", today) // attribute3 = valid to)
);