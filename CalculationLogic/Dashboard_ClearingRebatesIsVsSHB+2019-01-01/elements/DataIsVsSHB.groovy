def data2 = api.getElement("Data")

def result = [:]

def clearingRebateTypes = api.getElement("ClearingRebateTypes")

data2?.forEach { salesType, salesTypeData ->
    salesTypeData.forEach { d ->
        def monthData = result[d.yearMonth]
        if (!monthData) {
            monthData = [
                    month: d.yearMonth,
                    isRevenue: 0,
                    shbRevenue: 0,
                    isCount: 0,
                    shbCount: 0
            ]
            result[d.yearMonth] = monthData
        }

        if (clearingRebateTypes.contains(salesType)) {
            monthData.isRevenue += d.revenue
            monthData.isCount += d.count
        }
        monthData.shbRevenue += (d.shbRevenue ?: 0.0)
        monthData.shbCount += d.count
    }
}

return result.values()