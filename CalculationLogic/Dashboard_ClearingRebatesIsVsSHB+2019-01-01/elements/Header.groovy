if (api.isSyntaxCheck()) return;

def controller = api.newController()
def activeClearingRebatesCount = api.getElement("ActiveClearingRebatesCount")
def isVsShbLastMonth = api.getElement("DataIsVsSHB")?.last()

controller.addHTML(
"""
<div style='font-size: 16px; font-weight: bold; margin: 8px 0;'>
        Active Clearing Rebates: ${activeClearingRebatesCount}
</div>
""");

controller.addHTML(
        """
<div style='font-size: 12px;'>
        Number of transactions in ${isVsShbLastMonth?.month}: 
        <ul>
            <li>which <u>should have used</u> Clearing Rebate: <b>${isVsShbLastMonth?.shbCount}</b></li>
            <li>which <u>did use</u> Clearing Rebate: <b>${isVsShbLastMonth?.isCount}</b></li>
        </ul>
</div>
""");

return controller