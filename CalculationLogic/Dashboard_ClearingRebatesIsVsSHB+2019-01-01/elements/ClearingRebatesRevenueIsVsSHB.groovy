def data = api.getElement("DataIsVsSHB")

def categories = data*.month
def isRevenue = data*.isRevenue
def shbRevenue = data*.shbRevenue
def impact = [isRevenue, shbRevenue].transpose().collect { pair -> pair[0] - pair[1]}

def isRevenueWithPercentage = [isRevenue, shbRevenue]
        .transpose()
        .collect { pair -> [
                y: pair[0],
                percentOfSHB: pair[0]/pair[1] * 100
        ]}


def chartDef = [
        chart: [
                type: "column",
                zoomType: "xy"
        ],
        credits: [
                enabled: false
        ],
        colors: ['#a6cee3','#1f78b4','#e31a1c'],
        title: [
                text: ""
        ],
        xAxis: [[
                        title: [
                                text: "Month"
                        ],
                        categories: categories
                ]],
        yAxis: [[
                        title: [
                                text: "Revenue"
                        ],
                        stackLabels: [
                                enabled: true,
                                allowOverlap: true
                        ]
                ]],
        legend: [
                enabled: true,
                layout: "vertical",
                align: "right",
                verticalAlign: "middle"
        ],
        tooltip: [
                pointFormat: "{series.name}: <b>{point.y:,.0f} €</b><br/>"
        ],
        series: [[
                name: "Is Revenue",
                data: isRevenueWithPercentage,
                dataLabels: [
                        enabled: true,
                        format: "{point.y:,.0f} €"
                ],
                tooltip: [
                        pointFormat: "{series.name}: <b>{point.y:,.0f} €</b><br/>" +
                                "Percent of SHB: {point.percentOfSHB:.2f}%"
                ]
        ], [
                name: "Should Have Been Revenue",
                data: shbRevenue,
                dataLabels: [
                        enabled: true,
                        format: "{point.y:,.0f} €"
                ]
        ], [
                name: "Impact to the business",
                data: impact,
                dataLabels: [
                        enabled: true,
                        format: "{point.y:,.0f} €"
                ]
        ]]
]

return api.buildHighchart(chartDef)
