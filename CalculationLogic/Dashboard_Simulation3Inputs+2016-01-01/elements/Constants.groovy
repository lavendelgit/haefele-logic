import groovy.transform.Field

//Move to a common library
@Field String BOUND_PARTITION_NAME = "haefele"
@Field String JSON_BOUND_CALL_ADDORUPDATE_URI = "/loaddata/"

@Field Map SIMULATION_STATUS = [
        NEW       : [name: 'New', label: 'New', allowSimulation: true, info: 'Please provide input for simulation.'],
        SCHEDULED : [name: 'Scheduled', label: 'Scheduled', allowSimulation: false, info: 'Simulation processing has been scheduled. Please try executing the dashboard after sometime.'],
        PROCESSING: [name: 'Processing', label: 'Processing', allowSimulation: false, info: 'Simulation data is being processed. Please try executing the dashboard after sometime.'],
        ERROR     : [name: 'Error', label: 'Error', allowSimulation: true, info: 'Error processing simulation request. Please try again or contact support.', color: 'red'],
        READY     : [name: 'Ready', label: 'Ready', allowSimulation: true, info: "Simulation data is ready. Please click on 'Apply Settings' to see the data.", confirmAllowSimulation: true, color: '#2F7ED8']
]

@Field Map EXCEPTION_COLUMNS = [ExceptionKey                 : [name: 'ExceptionKey', type: 'Text', matrixColType: 'Text', label: 'Key'],
                                ExceptionFilter              : [name: 'ExceptionFilter', type: 'DatamartFilter', matrixColType: 'Text', label: 'Data Filter'],
                                MonthlyExceptionConfiguration: [name: 'MonthlyExceptionConfiguration', type: 'Options', matrixColType: 'Text', label: 'Monthly Exception Configuration']]
