//def params = [:]
//params.put("SimulationName", "Simulation2")

api.logInfo("################## SimulationInput ############ ", api.input("SimulationInputConfigurator"))

def simulationInput = api.input("SimulationInputConfigurator")
if (simulationInput && simulationInput instanceof Map) {
    simulationInput = persistSimulationInputs(simulationInput)
}
api.logInfo("########## SimulationInput", "updated")

simulationInput = simulationInput ?: [:]

return inlineConfiguratorEntry("Configurator_MonthlyAllSimulationInputs", "SimulationInputConfigurator", "Simulation Input Configurator", simulationInput)

Object inlineConfiguratorEntry(String configuratorLogic, String configuratorName, String label, Map params) {
    if (!configuratorName || !configuratorLogic) {
        api.throwException("Configurator name / logic not specified")
    }

    def configurator = api.inlineConfigurator(configuratorName, configuratorLogic)

    params.Status = "Scheduled"
    def conf = api.getParameter(configuratorName)
    if (conf != null) { // && conf.getValue() == null) {
        params["configurator"] = configuratorName
        conf.setLabel(label)
        conf.setValue(params)
    }
    api.logInfo("########## SimulationInput : inlineConfiguratorEntry", api.jsonEncode(params))
    return configurator
}


//if (ConfigurationInputChanged()) {

//}

Map persistSimulationInputs(Map simulation_inputs) {
    String simulationName = simulation_inputs.SimulationName
    if (simulationName) {
        Map simStatus = Constants.SIMULATION_STATUS[simulation_inputs?.Status?.toUpperCase()] ?: Constants.SIMULATION_STATUS.NEW
        //out.SimulationStatus ?: Constants.SIMULATION_STATUS.NEW
        //simNameList?.size() >= 2 ? Constants.SIMULATION_STATUS[simNameList?.getAt(1)] : Constants.SIMULATION_STATUS.NEW
        if (api.local.editTitleSimulation) {
            callBoundCallUtilFunction(simulationName, simStatus.name, simulation_inputs)
            simulation_inputs.Status = simStatus.name
        } else if (simStatus?.allowSimulation && !(simStatus?.confirmAllowSimulation && !simulation_inputs.RescheduleSimulation)) {
            callBoundCallUtilFunction(simulationName, Constants.SIMULATION_STATUS.SCHEDULED.name, simulation_inputs)
            simulation_inputs.Status = simStatus.name
        } else {
            api.throwException('Error initiating simulation. Please try after sometime.')
        }
    }
    return simulation_inputs
}

protected callBoundCallUtilFunction(String simulationName, String simStatus, Map simulation_inputs) {
    api.logInfo("+++++++++++++++++++simulation_inputs+++++++", api.jsonEncode(simulation_inputs))
    Date today = new Date()
    Map simulationRow = [status                : simStatus,
                         schedule_date         : today.format("yyyy-MM-dd'T'HH:mm:ss.SSS"),
                         simulation_owner      : api.user("loginName"),
                         simulation_description: simulation_inputs.SimulationTitle,
                         simulation_inputs     : simulation_inputs]
    BoundCallUtils.addOrUpdateJSONTableData("SimulationInputs", null, "JLTV", [simulationName], [simulationRow])
}
