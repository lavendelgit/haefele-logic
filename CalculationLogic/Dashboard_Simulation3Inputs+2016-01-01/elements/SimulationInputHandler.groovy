return
//if (ConfigurationInputChanged()) {
persistSimulationInputs(out.SimulationInputs)
//}

def persistSimulationInputs(Map simulation_inputs) {
    String simulationName = out.SimulationName
    if (simulationName) {
        Map simStatus = out.SimulationStatus ?: Constants.SIMULATION_STATUS.NEW
        //simNameList?.size() >= 2 ? Constants.SIMULATION_STATUS[simNameList?.getAt(1)] : Constants.SIMULATION_STATUS.NEW
        if (api.local.editTitleSimulation) {
            callBoundCallUtilFunction(simulationName, simStatus.name, simulation_inputs)
        } else if (simStatus?.allowSimulation && !(simStatus?.confirmAllowSimulation && !simulation_inputs.RescheduleSimulation)) {
            callBoundCallUtilFunction(simulationName, Constants.SIMULATION_STATUS.SCHEDULED.name, simulation_inputs)
        } else {
            api.throwException('Error initiating simulation. Please try after sometime.')
        }
    }
}

protected callBoundCallUtilFunction(String simulationName, String simStatus, Map simulation_inputs) {
    api.logInfo("+++++++++++++++++++simulation_inputs+++++++", api.jsonEncode(simulation_inputs))
    Date today = new Date()
    Map simulationRow = [status                : simStatus,
                         schedule_date         : today.format("yyyy-MM-dd'T'HH:mm:ss.SSS"),
                         simulation_owner      : api.user("loginName"),
                         simulation_description: simulation_inputs.SimulationTitle,
                         simulation_inputs     : simulation_inputs]
    BoundCallUtils.addOrUpdateJSONTableData("SimulationInputs", null, "JLTV", [simulationName], [simulationRow])
}

protected Boolean ConfigurationInputChanged() {
    Map simulationSavedInput = api.findLookupTableValues("SimulationInputs", Filter.equal("name", out.SimulationName))?.find()
    Map savedSimulationConfiguration = simulationSavedInput.attributeExtension___simulation_inputs
    String status = simulationSavedInput.attributeExtension___status
  
    if (status != Constants.SIMULATION_STATUS.SCHEDULED.name) {

      Map currentSimulationConfigurations = out.SimulationInputs
      Map smilationFilterSaved = savedSimulationConfiguration?.SimulationDataFilter
      Map smilationFilterCurrent = currentSimulationConfigurations?.SimulationDataFilter

      String simulationPercentageInputConfiguration = out.UserSelectedPercentageChangeInput
      String currentSimulationPercentageInputConfiguration = savedSimulationConfiguration?.SimulationPercentageChangeInput

      Map exceptionSaved = savedSimulationConfiguration?.Exceptions
      Map exceptionCurrent = currentSimulationConfigurations?.Exceptions

      Boolean filterConfigurationsChanges = (!smilationFilterSaved.equals(smilationFilterCurrent) || !exceptionSaved.equals(exceptionCurrent) || !simulationPercentageInputConfiguration.equals(currentSimulationPercentageInputConfiguration))
      Boolean dateConfigurationsChanges = ((out.SimulationInputs?.AnalysisYear != savedSimulationConfiguration?.AnalysisYear) || (out.SimulationInputs?.TargetYear != savedSimulationConfiguration?.TargetYear))
      if (filterConfigurationsChanges || dateConfigurationsChanges) {
          return true
      }

      if (out.SimulationInputs?.SimulationTitle?.trim() != savedSimulationConfiguration.SimulationTitle?.trim()) {
          api.local.editTitleSimulation = true
          return true
      }
    }

    return false
}

/*protected Boolean ConfigurationInputChanged() {
    Map simulationSavedInput = api.findLookupTableValues("SimulationInputs", Filter.equal("name", out.SimulationName))?.find()
    Map savedSimulationConfiguration = simulationSavedInput.attributeExtension___simulation_inputs
    Map currentSimulationConfigurations = out.SimulationInputs
    currentSimulationConfigurations.remove("DisableApply")

    return (savedSimulationConfiguration.equals(currentSimulationConfigurations))
}
*/