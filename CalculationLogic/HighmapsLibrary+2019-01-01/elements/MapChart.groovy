/**
 * Mocks a constructor of an "abstract class" that holds all similar features between map types.
 * <p>Available methods are:<br>
 *  - setTitle( String ) - sets the title that is displayed on top of the chart.<br>
 *  - setSubtitle( String ) - sets the subtitle that is displayed below the main chart title.<br>
 *  - setCaption( String ) - sets the chart caption that is displayed below the chart.<br>
 *  - setZoomType( String ) - sets whether the chart can be zoomed on X/Y or XY axes. Values available are stored in {@link HighmapsLibrary.elements.ConstConfig} ZOOM_TYPE.<br>
 *  - setMap( String ) - sets the mapData object for all series. Usually contains a path definition and optional parameters for joinBy.<br>
 *  - setBorderWidth( Integer ) - sets the outer border width of the map in px.<br>
 *  - setSeries( Map... ) - sets the series to be displayed on a chart. Requires objects deriving from {@link HighmapsLibrary.elements.BaseSeries} as parameters.<br>
 *  - setPlotOptions( Map ) - sets additional plot options for the chart. Explicit format, more information in <a href="https://api.highcharts.com/highmaps/plotOptions">Highchart API</a>.<br>
 *  - setColorAxis( Map ) - sets the color axis. Requires {@link HighmapsLibrary.elements.ColorAxis} object as parameter.<br>
 *  - setDataLabels( Map ) - sets the data labels that will be displayed on the series. Requires {@link HighmapsLibrary.elements.DataLabel} object as parameter.<br>
 *  - setTooltip( Map ) - sets tooltip that will be display on all chart series unless overridden in series objects. Requires {@link HighmapsLibrary.elements.Tooltip} object as parameter.<br>
 *  - setLegend( Map ) - sets the legend describing series data that will be displayed on the chart. Requires {@link HighmapsLibrary.elements.Legend} object as parameter.<br>
 *  - setMapNavigation( Map ) - sets the map navigation options. Provided explicity, more information in <a href="https://api.highcharts.com/highmaps/mapNavigation">Highchart API</a>.<br>
 *  - getHighchartFormatDefinition() - returns the properly formatted Highmap definition to be fed into api.buildHighmap() call.<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return map that mocks the base map object.
 */
Map newMapChart() {
    Map methods = [:]
    Map definition = [:]

    definition.credits = [enabled: false]
    definition.chart = [:]
    methods += [
            setTitle                   : { String _title ->
                definition.title = [text: _title]
                return methods
            },
            setSubtitle                : { String _subTitle ->
                definition.subtitle = [text: _subTitle]
                return methods
            },
            setCaption                 : { String _caption ->
                definition.caption = [text: _caption]
                return methods
            },
            setMap                     : { String _map ->
                definition.chart << [map: _map]
                return methods
            },
            setBorderWidth             : { Integer _borderWidth ->
                definition.chart << [borderWidth: _borderWidth]
                return methods
            },
            setSeries                  : { Map... _series ->
                definition.series = _series.collect { it.getHighmapsFormatDefinition() }
                return methods
            },
            setPlotOptions             : { Map _plotOptions ->
                definition.plotOptions = _plotOptions
                return methods
            },
            setColorAxis               : { Map _colorAxis ->
                definition.colorAxis = _colorAxis.getHighmapsFormatDefinition()
                return methods
            },
            setDataLabels              : { Map _dataLabels ->
                definition.dataLabels = _dataLabels.getHighmapsFormatDefinition()
                return methods
            },
            setTooltip                 : { Map _tooltip ->
                definition.dataLabels = _tooltip.getHighmapsFormatDefinition()
                return methods
            },
            setLegend                  : { Map _legend ->
                definition.legend = _legend.getHighmapsFormatDefinition()
                return methods
            },
            setMapNavigation           : { Map _mapNavigation ->
                definition.mapNavigation = _mapNavigation
                return methods
            },
            getHighmapsFormatDefinition: {
                return definition
            }
    ]

    return methods
}