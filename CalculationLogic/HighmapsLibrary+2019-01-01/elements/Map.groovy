/**
 * Mocks a constructor of a World continent series class that contains all world continent chart specific functionality. Extends BaseSeries.
 * @return map that mocks the world continent series object.
 */
Map newMap() {
    Map methods = [:]
    def definition = [:]

    methods << libs.HighmapsLibrary.BaseSeries.series(methods, definition)

    return methods
}