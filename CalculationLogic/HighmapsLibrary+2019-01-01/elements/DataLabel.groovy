/**
 * Mocks a constructor of a "class" that holds all data label related methods.
 * Used for various data labels displayed in the chart, for example on series.
 * <p>Available methods are:<br>
 *  - setEnabled( boolean ) - sets whether the data lables are enabled for the object.<br>
 *  - setFormat( String ) - sets format for displaying the data.
 *                          Format can be found here: https://www.highcharts.com/docs/chart-concepts/labels-and-string-formatting.<br>
 *  - setColor( String ) - sets the color for the data label.<br>
 *  - getHighmapsFormatDefinition() - returns the properly formatted Highmaps definition.<br>
 * @return map that mocks the data label object.
 */
Map newDataLabel() {
    def methods = [:]
    def definition = [:]

    methods = [
            setIsEnabled               : { Boolean _enabled ->
                definition.enabled = _enabled
                return methods
            },
            setFormat                  : { String _format ->
                definition.format = _format
                return methods
            },
            setColor                   : { String _color ->
                definition.color = _color
                return methods
            },
            getHighmapsFormatDefinition: {
                return definition
            }
    ]

    return methods
}