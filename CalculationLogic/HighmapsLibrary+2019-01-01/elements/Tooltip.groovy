/**
 * Mocks a constructor of a "class" that holds all tooltip related methods.
 * Used for various tooltips displayed in the chart, for example on series.
 * <p>Available methods are:<br>
 *  - setHeaderFormat( String ) - sets the tooltip header format, this is displayed on top of the tooltip box.<br>
 *  - setPointFormat( String ) - sets the tooltip for point, this is the data specific tooltip part.<br>
 *  - setFooterFormat( String ) - sets the tooltip footer format, this is displayed on bottom of the tooltip box.<br>
 *  - setShared( Boolean ) - sets whether the tooltip is shared, when true, moving mouse on whole plot area will make the tooltip appear, useful for single series charts.<br>
 *  - setUseHTML( Boolean ) - setting this to true allows for advanced formatting options like tables or images.<br>
 *  - getHighmapsFormatDefinition() - returns the properly formatted Highmaps definition.<br>
 * @return map that mocks the tooltip object.
 */
Map newTooltip() {
    def methods = [:]
    def definition = [:]

    methods = [
            setHeaderFormat            : { String _format ->
                definition.headerFormat = _format
                return methods
            },
            setPointFormat             : { String _format ->
                definition.pointFormat = _format
                return methods
            },
            setFooterFormat            : { String _format ->
                definition.footerFormat = _format
                return methods
            },
            setShared                  : { Boolean _shared ->
                definition.shared = _shared
                return methods
            },
            setUseHTML                 : { Boolean _usingHTML ->
                definition.useHTML = _usingHTML
                return methods
            },
            getHighmapsFormatDefinition: {
                return definition
            }
    ]

    return methods
}