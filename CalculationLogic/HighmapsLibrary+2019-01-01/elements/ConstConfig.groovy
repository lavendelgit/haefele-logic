/**
 * Available axis types to be used in {@link HighmapsLibrary.elements.BaseMap} setColorAxisType method.
 */
@Field AXIS_TYPES = [
        LINEAR     : 'linear',
        LOGARITHMIC: 'logarithmic'
]

/**
 * Map code translation used for various world regions.
 */
@Field MAP_CODES = [
        AMERICA: 'custom/north-america',
        EUROPE : 'custom/europe',
        ASIA   : 'custom/asia',
        OCEANIA: 'custom/oceania',
        AFRICA : 'custom/africa',
        WORLD  : 'custom/world-continents'
]