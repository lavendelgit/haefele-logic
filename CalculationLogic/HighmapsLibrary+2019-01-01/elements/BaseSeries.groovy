/**
 * Mocks a constructor of an "abstract class" that holds all similar features between series types.
 * <p>Available methods:<br>
 *  - setName( String ) - sets the name of the series, not displayed anywhere.<br>
 *  - setJoinBy( ) - sets to what property to join the map data to the value data. Can be a List [ mapDataKey, dataKey ]
 *                   When map has many data points, set null, data will then be joined by position in the array, which has much better performance.<br>
 *  - setData( List ) - sets the data to be displayed on chart. Can contain various objects - simple numbers or maps.<br>
 *  - setDataLabels( Map ) - sets the data labels that will be displayed on the series. Requires {@link HighmapsLibrary.elements.DataLabel} object as parameter.<br>
 *  - setTooltip( Map ) - sets the tooltip that will be displayed when hovering over series values.
 *                        Requires {@link HighmapsLibrary.elements.Tooltip} object as parameter. Can be used to handle data in Map format.<br>
 *  - getHighmapsFormatDefinition() - returns the properly formatted highcmaps definition.<br>
 * @param methods methods map from the deriving "class".
 * @param definition existing definition map from the deriving "class".
 * @return map that mocks the base series object.
 */
Map series(Map methods, Map definition) {
    return [
            setName                    : { String _name ->
                definition.name = _name
                return methods
            },
            setJoinBy                  : { _joinBy ->
                definition.joinBy = _joinBy
                return methods
            },
            setData                    : { _data ->
                definition.data = _data
                return methods
            },
            setDataLabels              : { Map _dataLabels ->
                definition.dataLabels = _dataLabels.getHighmapsFormatDefinition()
                return methods
            },
            setTooltip                 : { Map _tooltip ->
                definition.tooltip = _tooltip.getHighmapsFormatDefinition()
                return methods
            },
            getHighmapsFormatDefinition: {
                return definition
            }
    ]
}