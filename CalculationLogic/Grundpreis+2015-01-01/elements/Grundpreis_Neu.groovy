def grundpreis = api.product("Grundpreis_Neu")
api.global.computed = api.product("Grundpreis_Computed")

if ("Haefele" == api.global.computed) {
  // don't recompute Haefele prices
  return grundpreis
}

def sku = api.product("sku")
if (sku.length() == 11) {
  // don't compute the grundpreis for skus with 11 digits, it will be computed in another CFS
  api.trace("11-digit product, Grundpreis_Neu not computed", null, null)
  return grundpreis
}

// now it's clear we're going to compute something so reset the flag
api.global.computed = null

def status = api.product("Status")

if ("ZR Ausverkaufen + löschen".equals(status)) {
  def ek = api.product("EK")
  def vk = api.product("VK")
  def export = api.product("Export")
  
  if (ek && (vk || export)) {
    def dmCtx = api.getDatamartContext()
    def txDM = dmCtx.getTable("TX_DM")
    def datamartQuery = dmCtx.newQuery(txDM, false)

    // compute the unit price within the select clause
    datamartQuery.select("Umsatz/Menge", "U")
    datamartQuery.where(Filter.equal("Sku", api.product("sku")))
    datamartQuery.orderBy("Umsatz/Menge")

    def result = dmCtx.executeQuery(datamartQuery)
    def data = result?.getData()

    api.global.computed = "ZR"

    if (data) {
      def rows = data.getRowCount()
      if (rows > 0) {
        api.trace("Row count", null, rows)

        for (row = 0; row < rows; ++row) {
          api.trace("Row " + (row + 1), null, data.getValue(row, 0))
        }

        // compute the row at which the .75 percentile is 
        def q75 = Math.max(0, Math.ceil(rows * 0.75) - 1) as Integer

        // the new price is always per 100 ST
        grundpreis = data.getValue(q75, 0) * 100

        api.trace("Q.75", "Row " + (q75 + 1), grundpreis)
        
        // make the price at least EK + 65 %
        def limitPrice = ek * 1.65  
        if (grundpreis < limitPrice) {
          grundpreis = limitPrice
        }
      }
    } else {
    	grundpreis = null
    }
  } else {
    api.global.computed = "ZR-Fail"
    api.trace("Grundpreis_Neu not computed, required EK and (VK or Export)", "EK | VK | Export", ek + " | " + vk + " | " + export)
  }
}

// if not Haefele prices and grundpreis still not found
if (!grundpreis) {
  // look into PLs if this part is a Composite
  def pli = api.pricelistItem(api.getElement("Pricelist"))
  if (pli && pli.attribute3 == "Yes") {
    api.global.computed = "Composite"
  } else {
    // look into the Liefereinschraenkung PX
    def px = api.productExtension("Liefereinschraenkung")
    if (px) {
      api.global.computed = "Liefereinschraenkung"
    }
  }
}

return grundpreis