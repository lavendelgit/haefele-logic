ResultMatrix getResultMatrix(List validationCountMap, ResultMatrix resultMatrix, String conditionName) {

    def dashUtl = lib.DashboardUtility
    if (!resultMatrix) resultMatrix = dashUtl.newResultMatrix('Summary: Validations Count', Constants.RESULT_MATRIX_COLUMN_NAMES, Constants.RESULT_MATRIX_COLUMN_TYPES)

    validationCountMap?.each { it ->
        resultMatrix.addRow([
                (Constants.RESULT_MATRIX_COLUMN_NAMES[0]): conditionName,
                (Constants.RESULT_MATRIX_COLUMN_NAMES[1]): it.validationName,
                (Constants.RESULT_MATRIX_COLUMN_NAMES[2]): it.validationCount
        ])
    }

    return resultMatrix
}

protected List createPxFilter(String entity, Map pxTablesAttributes) {

    List validationFilters = [Filter.equal("name", entity)]
    String attributeValidFrom = pxTablesAttributes["validFromAttribute"]
    String attributeValidTo = pxTablesAttributes["validToAttribute"]
    String attributePriceChangeType = pxTablesAttributes["priceChangeTypeAttribute"]

    if (out.ValidFrom) validationFilters << Filter.greaterOrEqual(attributeValidTo, out.ValidFrom)
    //if (out.ValidTo) validationFilters << Filter.lessOrEqual(attributeValidFrom, out.ValidTo)
    validationFilters << Filter.isNotNull(attributePriceChangeType)
    return validationFilters
}

protected List getPxRecords(List validationFilters, Map pxTablesAttributes, String attributePerDiff, String attributePreviousCost, String attributePriceChangeType, String attributeSortBy) {

    String typeCode = pxTablesAttributes["tableTypeCode"]
    List validationRecords = null
    def pxStream = null

    List fields = attributePriceChangeType ? [attributePerDiff, attributePreviousCost, attributePriceChangeType] : [attributePerDiff, attributePreviousCost]

    try {
        pxStream = api.stream(typeCode, attributeSortBy, fields, false, *validationFilters)
        validationRecords = pxStream?.collect()
    } catch (ex) {
        api.logInfo("Exception in generateArticleIdsList", ex.getMessage())
    } finally {
        pxStream?.close()
    }

    return validationRecords
}

protected List createValidationCountMap(List validationRecords,
                                        String attributePriceChangeType,
                                        String attributePerDiff) {
    Map ValidationGroupMap
    BigDecimal refPercentage = out.ReferencePercentage
    BigDecimal refPercentageValue = out.ReferencePercentage ? out.ReferencePercentage / 100 : 0
    BigDecimal refPercentageGreaterThanValue = (refPercentage + 0.0005) / 100
    BigDecimal refPercentageLessThanValue = (refPercentage - 0.0005) / 100

    ValidationGroupMap = validationRecords.groupBy({ row -> row[attributePriceChangeType] })

    Map validationsCount = ValidationGroupMap?.collectEntries { String key, List groupedValues ->
        [(key): groupedValues.size()]
    }

    List priceIncreaseMap = ValidationGroupMap.get("Price Increase") as List
    Integer totalCount = validationRecords.size()
    Integer noPriceChange = validationRecords.findAll { it -> (it[attributePerDiff] as BigDecimal) == 0.00 }?.size()
    Integer priceReduction = validationRecords.findAll { it -> (it[attributePerDiff] as BigDecimal) < 0 && it[attributePerDiff] != null }?.size()
    Integer priceIncreaseGreaterThanRef = priceIncreaseMap.findAll { it -> (it[attributePerDiff] as BigDecimal) > refPercentageGreaterThanValue }?.size()
    // Integer priceIncreaseLessThanRef = priceIncreaseMap.findAll { it -> (it[attributePerDiff] as BigDecimal) < refPercentageLessThanValue && (it[attributePerDiff] as BigDecimal) > 0 }?.size()
    Integer priceIncreaseLessThanRef = priceIncreaseMap.findAll { it -> (it[attributePerDiff] as BigDecimal) < refPercentageLessThanValue }?.size()
    Integer priceIncreaseEqualToRef = priceIncreaseMap.findAll { it -> (it[attributePerDiff] as BigDecimal) >= refPercentageLessThanValue && (it[attributePerDiff] as BigDecimal) <= refPercentageGreaterThanValue }?.size()
    //Integer priceIncreaseEqualToRef = priceIncreaseMap.findAll { it -> (it[attributePerDiff] as BigDecimal) >= refPercentageLessThanValue}?.size()
    String priceIncreaseAboveRefPerString = libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_INCREASE_ABOVE_REF + refPercentage.toString() + "%"
    String priceIncreaseBelowRefPerString = libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_INCREASE_BELOW_REF + refPercentage.toString() + "%"

    List validationCountMap = []
    validationCountMap << ["validationName": libs.HaefeleCommon.PriceChangeTracking.VALIDATION_NO_PRICE_CHANGE, "validationCount": noPriceChange ?: 0]
    validationCountMap << ["validationName": libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_REDUCTION, "validationCount": priceReduction ?: 0]
    //validationCountMap << ["validationName": libs.HaefeleCommon.PriceChangeTracking.VALIDATION_NO_PRICE_CHANGE, "validationCount": validationsCount[libs.HaefeleCommon.PriceChangeTracking.VALIDATION_NO_PRICE_CHANGE] ?: 0]
    //validationCountMap << ["validationName": libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_REDUCTION, "validationCount": validationsCount[libs.HaefeleCommon.PriceChangeTracking.VALIDATION_PRICE_REDUCTION] ?: 0]
    validationCountMap << ["validationName": priceIncreaseAboveRefPerString, "validationCount": priceIncreaseGreaterThanRef]
    validationCountMap << ["validationName": priceIncreaseBelowRefPerString, "validationCount": priceIncreaseLessThanRef]
    validationCountMap << ["validationName": libs.HaefeleCommon.PriceChangeTracking.VALIDATION_NO_VOILATION, "validationCount": priceIncreaseEqualToRef]
    validationCountMap << ["validationName": "All", "validationCount": totalCount]

    return validationCountMap
}