import groovy.transform.Field

@Field List RESULT_MATRIX_COLUMN_NAMES = ["Condition Name",'Price Change Type', 'Count']
@Field List RESULT_MATRIX_COLUMN_TYPES = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.INTEGER]

