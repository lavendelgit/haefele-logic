import net.pricefx.common.api.FieldFormatType

def resultMatrix
Map ValidationGroupMap

out.Entity?.each { pxTableName ->
    Map pxTablesAttributes = api.global.conditionTableAttributes[pxTableName]
    String conditionName = pxTablesAttributes["conditionName"]
    String attributeCost = pxTablesAttributes["costAttribute"]
    String attributeSortBy = pxTablesAttributes["sortByAttribute"]
    String attributePerDiff = pxTablesAttributes["perDiffAttribute"]
    String attributePreviousCost = pxTablesAttributes["previousCostAttribute"]
    String attributePriceChangeType = pxTablesAttributes["priceChangeTypeAttribute"]

    List validationFilters = Library.createPxFilter(pxTableName, pxTablesAttributes)
api.logInfo("************** Summary DB validationFilters *********",validationFilters)
    List validationRecords = Library.getPxRecords(validationFilters, pxTablesAttributes, attributePerDiff, attributePreviousCost, attributePriceChangeType, attributeSortBy)
    if (validationRecords) {
        List validationCountMap = Library.createValidationCountMap(validationRecords, attributePriceChangeType, attributePerDiff)
        resultMatrix = Library.getResultMatrix(validationCountMap, resultMatrix, conditionName)
    }
}


if (resultMatrix) {
    resultMatrix.setPreferenceName("Summary Validation Count Details")

    resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("NetPriceTrackingDetails-ED"))
            .withColValueAsEventDataAttr(Constants.RESULT_MATRIX_COLUMN_NAMES[0], "ConditionName")
            .withColValueAsEventDataAttr(Constants.RESULT_MATRIX_COLUMN_NAMES[1], "ValidationType")

}

return resultMatrix
