if (out.Entity) {
    Map pxTablesAttributes = api.global.conditionTableAttributes[out.Entity[0]]
    String conditionName = pxTablesAttributes["conditionName"]

    api.dashboard("NetTrackingDetails")
            .setParam("Condition Name", conditionName)
            .setParam("Report Date", out.ValidFrom)
    //.setParam("Valid To", out.ValidTo)
            .setParam("RefPercentage", out.ReferencePercentage)
            .setParam("Price Change Type", "No Price Change")
            .showEmbedded()
            .andRecalculateOn(api.dashboardWideEvent("NetPriceTrackingDetails-ED"))
            .withEventDataAttr("ConditionName").asParam("Condition Name")
            .withEventDataAttr("ValidationType").asParam("Price Change Type")
}