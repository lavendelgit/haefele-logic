if (api.isSyntaxCheck()) {

    def pxTableOptions = api.findLookupTableValues("PriceChangeTrackingMetaConfigurations", "key1",
            Filter.equal("attribute15", libs.HaefeleCommon.PriceChangeTracking.INPUT_CONFIGURATION_NET_PRICE_TRACKING)).collectEntries {
        [(it.key2): it.attribute1]
    }

    api.options("Entity", pxTableOptions.keySet() as List, pxTableOptions)
    def pxTableParam = api.getParameter("Entity")
    pxTableParam.setLabel(libs.HaefeleCommon.PriceChangeTracking.PX_TABLE_FILTER_LABEL)
    pxTableParam.setRequired(true)
    return
} else {
    // Executed in NON-syntax check mode
    if (!input.Entity) {
        api.addWarning("Select PX table")
        return null
    }

    return input.Entity
}