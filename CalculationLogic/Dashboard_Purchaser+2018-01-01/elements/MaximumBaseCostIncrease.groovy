if (api.syntaxCheck) {
    return []
}

def result = []
def productFilter = api.getElement("ProductFilter")

def productSKUs(productFilter) {
    if (!productFilter) {
        return []
    }
    def skus = []
    def productsStream = api.stream("P", "sku", ["sku"], productFilter.asFilter())
    while (productsStream.hasNext()) {
        skus.add(productsStream.next().sku)
    }
    productsStream.close()
    api.logInfo("Number of Products", skus.size())
    return skus
}

def stream = api.stream("PGI", "sku" /* sort-by*/,
        ["sku","attribute73"],
        Filter.equal("priceGridId", 160),
        Filter.in("sku", productSKUs(productFilter)))

if (!stream) {
    return []
}

while (stream.hasNext()) {
    result.add(stream.next())
}

stream.close()

return result