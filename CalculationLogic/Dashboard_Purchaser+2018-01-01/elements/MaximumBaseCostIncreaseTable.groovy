import net.pricefx.common.api.FieldFormatType

def resultMatrix = api.newMatrix("Article Number","Max Acceptable Base Cost Increase")
resultMatrix.setPreferenceName("MaxAcceptableBaseCostIncreaseTablePreferences")
resultMatrix.setTitle("Max Acceptable Base Cost Increase")
resultMatrix.setColumnFormat("Max Acceptable Base Cost Increase", FieldFormatType.PERCENT)

def records = api.getElement("MaximumBaseCostIncrease")

for (r in records) {
    resultMatrix.addRow([
            "Article Number" : r.sku,
            "Max Acceptable Base Cost Increase" : r.attribute73 as BigDecimal // api.formatNumber("#.00%", r.attribute73)
    ])
}

return resultMatrix
