def groupedRecords = api.getElement("CustomerSalesPricesPerCustomerClassHistory")

def series = groupedRecords
    .collect { customerClass, records -> [
        name: customerClass,
        data: records.collect { r -> [r.date.getTime(), r.count]}
    ]}

api.trace("series", "", series)

def chartDef = [
        chart: [
                type: "column",
                zoomType: "x"
        ],
        title: [
                text: "ZPAPs per Customer Class History"
        ],
        xAxis: [[
                        title: [
                                text: "Customer Class"
                        ],
                        type: "datetime"
                ]],
        yAxis: [[
                        title: [
                                text: "Number of ZPAPs"
                        ]
                ]],
        tooltip: [
                headerFormat: 'Customer Class: {series.name}<br/>',
                pointFormat: 'Date: {point.x:%Y-%m-%d} <br/>Number of ZPAPs: {point.y}'
        ],
        legend: [
                enabled: true
        ],
        series: series
]

api.buildFlexChart(chartDef)
