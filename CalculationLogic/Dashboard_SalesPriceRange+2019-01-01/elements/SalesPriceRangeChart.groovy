def transactions = api.getElement("Transactions")
def salesPriceRecord = api.getElement("SalesPriceRecord")
def groupBy = api.getElement("GroupBy")
def colors = api.getElement("Colors")

def series = transactions?.data
        ?.groupBy { r -> r.groupBy}
        ?.collect { groupName, records -> [
            name: groupName ?: "Unknown",
            data: records.collect { r -> [r.invoiceDate.getTime(), r.salesPricePer100Units]},
            marker: [
                    radius: 3,
                    symbol: "circle"
            ]
        ]}
        ?.sort { s -> s.name }

def chartDef = [
        chart: [
                type: "scatter",
                zoomType: "xy"
        ],
        colors: colors,
        title: [
                text: "Sales Price Range"
        ],
        xAxis: [[
                        title: [
                                text: "Invoice Date"
                        ],
                        type: "datetime",
                ]],
        yAxis: [[
                        title: [
                                text: "Sales Price"
                        ]
                ]],
        tooltip: [
                headerFormat: 'Date: {point.x:%Y-%m-%d} <br/>',
                pointFormat: 'Sales Price: {point.y:,.2f} € <br/>' +
                              groupBy + ': {series.name}<br/>'
        ],
        legend: [
                enabled: true
        ],
        series: series
]

if (salesPriceRecord) {
    def baseSalesPrice = salesPriceRecord?.salesPrice
    def minSalesPrice = baseSalesPrice * 0.7
    def avgSalesPrice = transactions?.summary?.projections?.salesPricePer100Units?.Avg;
    def maxSalesPrice = transactions?.summary?.projections?.salesPricePer100Units?.Max;
    chartDef.yAxis[0].plotLines = [[
            value: baseSalesPrice,
            color: '#EE7D31',
            width: 2,
            label: [
                    text: 'Grundpreis (' + salesPriceRecord.type + ': ' + api.formatNumber('#.00', baseSalesPrice) + ')'
            ],
            zIndex: 4
    ], [
            value: minSalesPrice,
            color: '#FFC000',
            width: 2,
            label: [
                    text: '<span style="text-shadow: 1px 1px 1px #eee">Mind. VK-Preis (' + api.formatNumber('#.00', minSalesPrice)  + ')</span>',
                    useHTML: true,
                    y: 14
            ],
            zIndex: 4
    ]]

    if (avgSalesPrice != null) {
        chartDef.yAxis[0].plotLines << [
                value: avgSalesPrice,
                color: '#A5A5A5',
                width: 2,
                label: [
                        text: '<span style="text-shadow: 1px 1px 1px #eee">Mittelwert (' + api.formatNumber('#.00', avgSalesPrice) + ')</span>',
                        useHTML: true
                ],
                zIndex: 4
        ]
    }

    if (maxSalesPrice != null && maxSalesPrice < baseSalesPrice) {
        chartDef.yAxis[0].max = baseSalesPrice * 1.1
    }
}

api.buildHighchart(chartDef)
