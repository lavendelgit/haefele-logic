def multipleColors = api.booleanUserEntry("Colors")

if (multipleColors) {
    return lib.Colors.palette()
} else {
    ['#a6cee3']
}
