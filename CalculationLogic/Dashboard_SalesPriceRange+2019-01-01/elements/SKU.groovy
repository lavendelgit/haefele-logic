if (api.isSyntaxCheck()) {
    return
}

def productFilter = api.getElement("ProductFilter")

if (productFilter == null) {
    return api.product("sku") // for test run only
}

if (!productFilter?.productFilterCriteria?.isEmpty()) {
    def skus = api.find("P",0,2,"sku",["sku"], productFilter.asFilter())
    if (skus.size() > 1) {
        api.throwException("Product filter matches more than one article.")
    }
    if (skus.size() == 0) {
        api.throwException("Product filter does not match any articles.")
    }
    return skus[0].sku
}

if (productFilter?.productFieldName != "sku") {
    api.throwException("Select product by Article Number")
}

return productFilter?.productFieldValue