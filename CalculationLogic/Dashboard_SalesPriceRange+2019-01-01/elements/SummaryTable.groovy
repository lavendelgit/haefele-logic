def resultMatrix = api.newMatrix("Group","Count", "Percentage")
resultMatrix.setTitle("Price Range Summary")
resultMatrix.setColumnFormat("Count", FieldFormatType.NUMERIC)
resultMatrix.setColumnFormat("Percentage", FieldFormatType.PERCENT)

def transactionsData = api.getElement("Transactions")?.data
def baseSalesPrice = api.getElement("SalesPriceRecord")?.salesPrice

if (transactionsData == null || transactionsData.size() == 0 || baseSalesPrice == null) {
    return resultMatrix;
}

def totalCount = transactionsData.size()
def minSalesPrice = baseSalesPrice * 0.7

def summary = transactionsData.inject([
    aboveBasePrice: 0,
    belowMinPrice: 0,
    between: 0
]) { result, row ->
    if (row.salesPricePer100Units > baseSalesPrice) {
        result.aboveBasePrice++
    } else if (row.salesPricePer100Units < minSalesPrice) {
        result.belowMinPrice++
    } else {
        result.between++
    }
    return result
}

resultMatrix.addRow([
        "Group": "Above Base Price",
        "Count": summary.aboveBasePrice,
        "Percentage": summary.aboveBasePrice / totalCount
])

resultMatrix.addRow([
        "Group": "Between Base Price and Min Price",
        "Count": summary.between,
        "Percentage": summary.between / totalCount
])

resultMatrix.addRow([
        "Group": "Below Min Price",
        "Count": summary.belowMinPrice,
        "Percentage": summary.belowMinPrice / totalCount
])

return resultMatrix