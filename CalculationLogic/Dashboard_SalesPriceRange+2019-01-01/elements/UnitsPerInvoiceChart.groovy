def transactions = api.getElement("Transactions")
def groupBy = api.getElement("GroupBy")
def colors = api.getElement("Colors")

def series = transactions?.data
        ?.groupBy { r -> r.groupBy}
        ?.collect { groupName, records -> [
        name: groupName ?: "Unknown",
        data: records.collect { r -> [r.units, r.salesPricePer100Units]},
        marker: [
                radius: 3,
                symbol: "circle"
        ]
]}

def chartDef = [
        chart: [
                type: "scatter",
                zoomType: "x"
        ],
        colors: colors,
        title: [
                text: "Units per Invoice"
        ],
        xAxis: [[
                        title: [
                                text: "Units"
                        ]
                ]],
        yAxis: [[
                        title: [
                                text: "Sales Price"
                        ]
                ]],
        tooltip: [
                headerFormat: 'Units: {point.x} <br/>',
                pointFormat: 'Sales Price: {point.y:,.2f} € <br/>' +
                             groupBy + ': {series.name}<br/>'
        ],
        legend: [
                enabled: true
        ],
        series: series
]

api.buildHighchart(chartDef)
