def field = ['CCYear'                : 'Year',
             'p1_CustomerId'         : 'TotalCustomerCount',
             'p3_CCTotalRevenue'     : 'YearlyRevenue',
             'p2_CCTotalMargin'      : 'YearlyMargin']
def filters = [
        Filter.equal('CCYear', out.InputDate)
]

return lib.DBQueries.getDataFromSource(field, 'DM_OverallCustomerCountPerYearPerCPG', filters, 'Rollup')
