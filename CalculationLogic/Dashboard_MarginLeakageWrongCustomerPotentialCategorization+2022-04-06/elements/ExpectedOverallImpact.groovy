import net.pricefx.common.api.FieldFormatType

def overallTotals = out.CustomerTotalsForYear
def wrongCategorizationTotals = out.CuustomersWithWrongCategoryTotalsForYear


def columns = ['Total number of Customers','Haefele Total Umsatz', 'Total number of customers With Wrong Potential Group','Umsatz Of Customers With Wrong Potential Group', '% Of Total Umsatz For Customers With Wrong Potential Group',
               'Haefele Total DB', 'DB Of Customers With Wrong Potential Group', '% Of Total DB For Customers With Wrong Potential Group']
def columnTypes = [FieldFormatType.NUMERIC,FieldFormatType.MONEY_EUR, FieldFormatType.NUMERIC, FieldFormatType.MONEY_EUR,  FieldFormatType.PERCENT, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.PERCENT]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Revenue and Margin Impacted By Wrong Potential Categorization for ' + out.YearForAnalysis, columns, columnTypes)

def totalYrRev = (overallTotals.size())? overallTotals[0]['YearlyRevenue'] : 0.0
def totalMargin = (overallTotals.size())?overallTotals[0]['YearlyMargin'] : 0.0
def totalCustomerCount = (overallTotals.size())?overallTotals[0]['TotalCustomerCount'] : 0.0
def totalCustomerCountWithWrongPotential = (wrongCategorizationTotals.size())?wrongCategorizationTotals[0]['CustomerCount'] : 0.0
def wrongCatRev = (wrongCategorizationTotals.size())?wrongCategorizationTotals[0]['YearlyRevenue'] : 0.0
def wrongCatMargin = (wrongCategorizationTotals.size())?wrongCategorizationTotals[0]['YearlyMargin'] : 0.0

resultMatrix.addRow([
        (columns[0]): dashUtl.boldHighlightWith(resultMatrix, totalCustomerCount),
        (columns[1]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("€###,###,###",totalYrRev)),
        (columns[2]): dashUtl.boldHighlightWith(resultMatrix, totalCustomerCountWithWrongPotential),
        (columns[3]): dashUtl.boldHighlightWith(resultMatrix, api.formatNumber("€###,###,###",wrongCatRev)),
        (columns[4]): dashUtl.boldHighlightWith(resultMatrix, lib.MathUtility.divide(wrongCatRev, totalYrRev)),
        (columns[5]): dashUtl.boldHighlightWith(resultMatrix, totalMargin),
        (columns[6]): dashUtl.boldHighlightWith(resultMatrix, wrongCatMargin),
        (columns[7]): dashUtl.boldHighlightWith(resultMatrix, lib.MathUtility.divide(wrongCatMargin, totalMargin))
])

resultMatrix.setPreferenceName("Expected Impact If Customers With Wrong Potential Stops Buying")
return resultMatrix