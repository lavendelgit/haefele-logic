if (api.isSyntaxCheck()) {
    api.abortCalculation()
}

def fields = [
        'Verkaufsbereich3'     : 'Verkaufsbereich3',
        'BrancheCategory'      : 'BrancheCategory',
        'SUM(p11_CustomerId)'  : 'CustomerCount',
        'SUM(p1_THYear1Umsatz)': 'Year1Revenue',
        'SUM(p3_THYear2Umsatz)': 'Year2Revenue',
        'SUM(p5_THYear3Umsatz)': 'Year3Revenue',
        'SUM(p6_THYear1DB)'    : 'Year1DB',
        'SUM(p7_THYear2DB)'    : 'Year2DB',
        'SUM(p8_THYear3DB)'    : 'Year3DB'
]
def filters = []
return lib.DBQueries.getDataFromSource(fields, 'DB_CustomersWithCorrections', filters, 'Rollup')