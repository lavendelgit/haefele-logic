def selectedYear = out.YearForAnalysis
selectedYear = ((selectedYear)?:"2019") + "-01-01"
return selectedYear