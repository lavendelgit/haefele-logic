import net.pricefx.common.api.FieldFormatType

int currentYear = Integer.parseInt(new java.util.Date().format("yyyy"))
int yearIndex = currentYear-Integer.parseInt(out.YearForAnalysis)

def columns = ['Verkaufsbuero', 'Branche Category', 'Number of Customers', 'Umsatz ' + out.YearForAnalysis,
               'DB ' + out.YearForAnalysis]
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.NUMERIC, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR]


/*
def yearList = lib.GeneralUtility.getYearsListFromNow(3)


api.trace(yearList[2]+"  "+yearList[2]  +" "+yearList[1])
def columns = ['Verkaufsbuero', 'Branche Category', 'Number of Customers', 'Umsatz ' + yearList[2], 'Umsatz ' + yearList[1], 'Umsatz ' + yearList[0],
               'DB ' + yearList[2], 'DB ' + yearList[1], 'DB ' + yearList[0],]
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.NUMERIC, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR,
                   FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR]

 */
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.newResultMatrix('Umsatz & DB Distribution for Customers With Wrong Customer Potential Group',
        columns, columnTypes)

def summaryInfo = out.SummaryDataInformation
// Insert the first row with the All Options...
def aggregatedSummary = [0, 0.0, 0.0]
summaryInfo?.each {
    aggregatedSummary[0] += it['CustomerCount']
    aggregatedSummary[1] += it["Year"+(yearIndex+1)+"Revenue"]
    aggregatedSummary[2] += it["Year"+(yearIndex+1)+"DB"]
}
resultMatrix.setPreferenceName("MLRecToAWCPG_WrongCustPotGroup")
resultMatrix.addRow(['All',
                     'All',
                     dashUtl.boldHighlightWith(resultMatrix, aggregatedSummary[0]),
                     aggregatedSummary[1],
                     dashUtl.boldHighlightWith(resultMatrix, aggregatedSummary[2])
                    ])

summaryInfo?.each { it ->
    resultMatrix.addRow([
            (columns[0]): it['Verkaufsbereich3'],
            (columns[1]): it['BrancheCategory'],
            (columns[2]): dashUtl.boldHighlightWith(resultMatrix, it['CustomerCount']),
            (columns[3]): it["Year"+(yearIndex+1)+"Revenue"],
            (columns[4]): it["Year"+(yearIndex+1)+"DB"]])
}
resultMatrix.onRowSelection().triggerEvent(api.dashboardWideEvent("SalesOffBrancheChange-ED")).withColValueAsEventDataAttr(columns[0], "Verkaufsbuero").withColValueAsEventDataAttr(columns[1], "BrancheCategory")

return resultMatrix

//api.formatNumber("€###,###,###.##", getNotNullRevenue(it['Year5Revenue'], it['SYear5Revenue']))