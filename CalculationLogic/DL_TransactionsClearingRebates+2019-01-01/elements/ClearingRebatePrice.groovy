def sku = api.getElement("ArticleNumber")
def customerId = api.getElement("CustomerId")
def invoiceDate = api.getElement("InvoiceDate")
def regularSalesPrice = api.getElement("RegularSalesPricePer100")

def clearingRebateRecord = lib.Find.latestClearingRebateRecord(sku,
        Filter.equal("attribute1", customerId),
        Filter.lessOrEqual("attribute2", invoiceDate),
        Filter.greaterOrEqual("attribute3", invoiceDate)
)

def clearingRebate = clearingRebateRecord?.attribute4

if (regularSalesPrice != null && clearingRebate != null) {
    return (regularSalesPrice * (1 + clearingRebate)).setScale(2, BigDecimal.ROUND_HALF_UP)
}