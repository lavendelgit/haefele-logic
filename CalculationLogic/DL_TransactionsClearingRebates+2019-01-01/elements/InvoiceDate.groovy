def invoiceDate = api.dateUserEntry("InvoiceDate")

if (invoiceDate instanceof java.util.Date) {
    invoiceDate = invoiceDate.format("yyyy-MM-dd")
}

return invoiceDate
