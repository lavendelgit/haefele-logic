def sku = api.getElement("ArticleNumber")
def invoiceDate = api.getElement("InvoiceDate")

def salesPriceRecord = lib.Find.latestSalesPriceRecord(sku,
        Filter.lessOrEqual("attribute1", invoiceDate),
        Filter.greaterOrEqual("attribute2", invoiceDate))

def regularSalesPricePer100Units = salesPriceRecord.attribute3

if (regularSalesPricePer100Units == null) {
    regularSalesPricePer100Units = lib.Find.compoundPriceRecord(sku)?.attribute1
}

return regularSalesPricePer100Units