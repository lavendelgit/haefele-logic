if (!api.global.materialStatus) {
    api.global.materialStatus = api.findLookupTableValues("MaterialStatus")
        .collectEntries { pp -> [ (pp.name) : [
                priceIncreaseAffected: pp.attribute1
        ]]}
}

api.trace("api.global.materialStatus","",api.global.materialStatus)

return api.global.materialStatus