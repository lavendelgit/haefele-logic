def transactionsCount = api.getElement("TransactionsCount")
def minimumTransactionsCount = api.getElement("MinimumTransactionsCount")
def unitsSoldLastYear = api.getElement("UnitsSold")

if (api.product("sku").startsWith("999")) {
    api.yellowAlert("Internal article (999). Returning units from last year.")
    return unitsSoldLastYear
}

if (api.product("sku").endsWith("X")) {
    api.yellowAlert("X Article. Returning units from last year.")
    return unitsSoldLastYear
}

if (transactionsCount < minimumTransactionsCount) {
    api.yellowAlert("Too few transactions. Ignoring linear regression and returning units from last year.")
    return unitsSoldLastYear
}

if (api.getElement("IsSamePrice")) {
    api.yellowAlert("Invoice prices are (nearly) the same. Ignoring linear regression and returning units from last year.")
    return unitsSoldLastYear
}

if (api.getElement("IsNegativeUnits")) {
    api.yellowAlert("Negative units from linear regression. Returning units from last year.")
    return unitsSoldLastYear
}

if (api.getElement("IsExtremeImpact")) {
    api.yellowAlert("The increase would cause an extreme impact (" +
            api.formatNumber("#.##%", api.getElement("PriceIncreaseImpact").expectedImpactPerc) + "). " +
            "Returning revenue from last year.")
    return unitsSoldLastYear
}


return api.getElement("PriceIncreaseImpact").currentUnits
