def calculate(transactionsSummary, linReg, supplierPriceIncreasePerc, salesPriceIncreasePerc) {
    def revenue = transactionsSummary?.revenue
    def transactionsCount = linReg?.count
    def currentSalesPrice = linReg?.avgPrice

    if (revenue == null || transactionsCount == null || currentSalesPrice == null) {
        return [
                currentSalesPrice: null,
                currentUnits: null,
                currentRevenue: null,
                newUnits: null,
                newSalesPrice: null,
                newRevenue: null,
                expectedImpactAmount: null,
                expectedImpactPerc: null
        ]
    }

    def currentUnits = (linReg.lra + currentSalesPrice * linReg.lrb) * transactionsCount
    def currentRevenue = currentUnits * currentSalesPrice / 100
    def newSalesPrice = currentSalesPrice * (1 + salesPriceIncreasePerc)
    def newUnits = (linReg.lra + newSalesPrice * linReg.lrb) * transactionsCount
    def newRevenue = newUnits * newSalesPrice / 100
    def expectedImpactAmount = newRevenue - currentRevenue

    return [
        currentSalesPrice: currentSalesPrice as BigDecimal,
        currentUnits: currentUnits as BigDecimal,
        currentRevenue: currentRevenue as BigDecimal,
        newUnits: newUnits as BigDecimal,
        newSalesPrice: newSalesPrice as BigDecimal,
        newRevenue: newRevenue as BigDecimal,
        expectedImpactAmount: expectedImpactAmount as BigDecimal,
        expectedImpactPerc: (revenue > 0 ? expectedImpactAmount / revenue : 0.0) as BigDecimal
    ]
}

return calculate(
        api.getElement("TransactionsSummary"),
        api.getElement("LinearRegression"),
        api.getElement("SupplierPriceIncreasePerc"),
        api.getElement("SalesPriceIncreasePerc")
)