def cost1 = api.getElement("Cost1")
def units1 = api.getElement("UnitsSold")
def newUnits = api.getElement("NewUnits")

if (cost1 == null || !units1 || newUnits == null) {
    return
}

def unitCost = cost1 / units1

return newUnits * unitCost