def linReg = api.getElement("LinearRegression")

def resultMatrix = api.newMatrix("Name", "Value")
resultMatrix.setColumnFormat("Value", FieldFormatType.NUMERIC)
resultMatrix.setPreferenceName("LinearRegressionInfo")

resultMatrix.addRow("Intercept (LRa)", linReg.lra)
resultMatrix.addRow("Slope (LRb)", linReg.lrb)
resultMatrix.addRow("OLS Intercept", linReg.ols?.intercept)
resultMatrix.addRow("OLS Slope", linReg.ols?.coefficients?.toList()?.get(0))
resultMatrix.addRow("OLS R-sqaured", linReg.ols?.r2)
resultMatrix.addRow("OLS Intercept P-Value", linReg.ols?.ttest?.toList()?.get(1)?.toList()?.get(3))
resultMatrix.addRow("OLS Price P-Value", linReg.ols?.ttest?.toList()?.get(0)?.toList()?.get(3))


return resultMatrix

