def transactionsCount = api.getElement("TransactionsCount")
def minimumTransactionsCount = api.getElement("MinimumTransactionsCount")
def revenueLastYear = api.getElement("Revenue")

if (api.product("sku").startsWith("999")) {
    api.yellowAlert("Internal article (999). Returning revenue from last year.")
    return revenueLastYear
}

if (api.product("sku").endsWith("X")) {
    api.yellowAlert("X Article. Returning revenue from last year.")
    return revenueLastYear
}

if (transactionsCount < minimumTransactionsCount) {
    api.yellowAlert("Too few transactions. Ignoring linear regression and returning revenue from last year.")
    return revenueLastYear
}

if (api.getElement("IsSamePrice")) {
    api.yellowAlert("Invoice prices are (nearly) the same. Ignoring linear regression and returning revenue from last year.")
    return revenueLastYear
}

if (api.getElement("IsNegativeUnits")) {
    api.yellowAlert("Negative units from linear regression. Returning revenue from last year.")
    return revenueLastYear
}

if (api.getElement("IsExtremeImpact")) {
    api.yellowAlert("The increase would cause an extreme impact (" +
            api.formatNumber("#.##%", api.getElement("PriceIncreaseImpact").expectedImpactPerc) + "). " +
            "Returning revenue from last year.")
    return revenueLastYear
}


return api.getElement("PriceIncreaseImpact").currentRevenue