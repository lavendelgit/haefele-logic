def pgList = api.findPriceGrids(Date.newInstance(), "SurchargeCheck")
api.trace("pg",pgList)
def pg = pgList[0]
def latestStartDate = pg?.calculationDate
latestStartDate = latestStartDate?.replace("T"," ")
def status = pg?.status
def lookupTableId = api.findLookupTable("SurchargeCheckDate")?.id
def filters = [
  Filter.equal("lookupTable.id", lookupTableId),
  Filter.equal("name","Last Process Date")
]
def lookupTableRow = api.find("MLTV", *filters)
def lastProcessDate = lookupTableRow[0]?.attribute1
lastProcessDate = lastProcessDate?.replace("T"," ")
api.trace("latestStartDate",latestStartDate)
api.trace("lastprocess",lastProcessDate)
if((!lastProcessDate || Date.parse("yyyy-MM-dd HH:mm:ss",latestStartDate) > Date.parse("yyyy-MM-dd HH:mm:ss",lastProcessDate)) && status=="READY"){
  def metalist = api.find("PGIM", Filter.equal("priceGridId", pg?.id))
  def meta = metalist?.find{it.elementName=="Deficiency"}
  def fieldName = meta?.fieldName
  api.trace("field",fieldName)
  def email = api.vLookup("SurchargeConfig", "Email")
  def server = api.vLookup("SurchargeConfig","Server")
  def typedId = pg?.typedId
  def query = api.getPriceGridSummaryQuery()
  query.addObjectTypedId(typedId)
  query.setDoCount(true)
  query.setItemFilter(Filter.like(fieldName,"%Z%"))
  query.addProjection("Base Price", "SUM")
  def result = api.runSummaryQuery(query)
  def count = result[0]?.countKey
  api.trace("count",count)
  def message = "No articles have smaller surcharges."
  if(count>0){
    message = "Surcharges are too small for $count articles."
  }
  def noReply = "<i>Please do not reply to this email as it is an automated email from Pricefx</i>"
  def url = "https://$server/classic/priceFxWeb.html?targetPage=priceGridPage&targetPageState="+typedId
  def link = "<a href=$url>View LPG</a>"
  def emailList = email?.split(",")
  emailList?.each{
    def user = api.find("U", Filter.equal("email", it))
    def userName = user[0]?.firstName
    userName = userName?:""
    def emailTemplate = "Dear $userName<br><br>"+message+"<br><br>$link<br><br><br>$noReply<br><br>Best Regards,<br>Your Pricing Team!"
    api.sendEmail(it, "Surcharge Summary Report", emailTemplate)
    api.trace("email",emailTemplate)
  }

  def entry = [
    lookupTableId : lookupTableId,
    name : "Last Process Date",
    attribute1 : latestStartDate
  ]
  api.addOrUpdate("MLTV", entry)
}