return persistSimulationInputsAndReRunSimulation(out.SimulationInputs)

protected Boolean persistSimulationInputsAndReRunSimulation(Map simulation_inputs) {
    String simulationName = out.SimulationName
    if (simulationName) {
        Map simStatus = out.SimulationStatus ?: Constants.SIMULATION_STATUS.NEW
        //if ((simStatus?.allowSimulation && !(simStatus?.confirmAllowSimulation && !simulation_inputs.RescheduleSimulation)) || (out.SimulationStatus?.name == "Error")) {
        if ((simStatus?.allowSimulation && simulation_inputs.RescheduleSimulation)) {
            Date today = new Date()
            Map simulationRow = [status           : Constants.SIMULATION_STATUS.SCHEDULED.name,
                                 schedule_date    : today.format("yyyy-MM-dd'T'HH:mm:ss.SSS"),
                                 simulation_owner : api.user("loginName"),
                                 simulation_description: simulation_inputs.SimulationTitle,
                                 simulation_inputs: simulation_inputs,
                                 simulation_notify: "No"]
            BoundCallUtils.addOrUpdateJSONTableData("SimulationInputs", null, "JLTV", [simulationName], [simulationRow])
            return true
        }

        return false
    }
}