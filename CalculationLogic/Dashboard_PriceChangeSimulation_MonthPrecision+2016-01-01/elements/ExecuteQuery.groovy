import net.pricefx.formulaengine.scripting.Matrix2D

Matrix2D resultData = (out.ShowDashboard) ? api.getDatamartContext().executeQuery(QueryBuilderInstance.getSQLQuery())?.getData() : null
api.local.MatrixData = resultData

api.trace("resultData", api.jsonEncode(resultData))

return