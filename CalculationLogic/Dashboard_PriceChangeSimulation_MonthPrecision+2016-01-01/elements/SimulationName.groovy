String simulationName = out.SimulationInputs?.SimulationName

if (api.isDebugMode()) {
    return "Simulation 5"
}

if (simulationName == null) {
    api.abortCalculation()
}

return simulationName