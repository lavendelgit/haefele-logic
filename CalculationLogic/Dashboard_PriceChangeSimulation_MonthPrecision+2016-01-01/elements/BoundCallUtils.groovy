/**
 * Generic method to add or update data to PricingParameter table. This uses api.boundCall for
 * better performance
 * @param tableName : Name of the PP table
 * @param tableId : TableId
 * @param typeCode : TypeCode of the table
 * @param fields
 * @param options
 * @return
 */
def addOrUpdatePPTableData(tableName, tableId, typeCode, fields, options) {
    if (tableId == null) {
        tableId = api.findLookupTable(tableName)?.id
    }
    def dataToSend = [] as List

    options.each { option ->
        data = []
        data.add(tableId)
        data.addAll(option)
        dataToSend.add(data)
    }

    def headerList = [] as List
    headerList.add("lookupTable")
    headerList.addAll(fields)

    def req = [data: [
            header: headerList,
            data  : dataToSend
    ]]


    def body = api.jsonEncode(req)

    def result = api.boundCall(
            libs.__LIBRARY__.Constants.BOUND_PARTITION_NAME,
            "/loaddata/" + typeCode,
            body,
            true)

    if ("200".equals(result?.statusCode)) {
        libs.__LIBRARY__.LoggerUtils.LOG("addOrUpdatePPTableData", "INFO", "Successfully add / update table $tableName : rowcount " + options?.size())
    } else {
        api.throwException("Unable to add / update table $tableName with error response : " + api.jsonEncode(result))
    }
}

/**
 * Common method to remove all rows from PP table
 * @param tableName
 * @param tableType
 * @return
 */
def clearAllRows(tableName, tableType) {
    if (!tableName) {
        return null
    }
    def table = api.findLookupTable(tableName)
    if (table) {
        try {
            def partitionName = libs.__LIBRARY__.Constants.BOUND_PARTITION_NAME
            //Delete all records
            def deleteRequest = """ {"data":{"filterCriteria":{}}} """
            def strUrl = "lookuptablemanager.delete/" + table.id + "/batch"
            def deleteResponse = api.boundCall(partitionName, strUrl, deleteRequest)
            if ("200".equals(deleteResponse?.statusCode)) {
                libs.__LIBRARY__.LoggerUtils.LOG("clearAllRows", "INFO", "Deleted $tableName rows successfully.")
            } else {
                api.throwException("Unable to delete rows for $tableName with error response : " + api.jsonEncode(deleteResponse))
            }
        } catch (java.lang.Exception ex) {
            libs.__LIBRARY__.LoggerUtils.LOG("delete", "EXCEPTION", ex)
        }
    }
}

/**
 * Generic method to add or update data to Data Source
 * This uses api.boundCall for better performance
 * @param tableName : Name of the Data Source
 * @param data to be inserted (a List of Map)
 * @return
 */
def addOrUpdateDSData(tableName, data) {
    addOrUpdateDataToDMTable(tableName, libs.__LIBRARY__.Constants.DATASOURCE_TYPECODE, data)
}

/**
 * Generic method to add or update data to Datamart
 * This uses api.boundCall for better performance
 * @param tableName : Name of the Datamart
 * @param data to be inserted (a List of Map)
 * @return
 */
def addOrUpdateDMData(tableName, data) {
    addOrUpdateDataToDMTable(tableName, libs.__LIBRARY__.Constants.DATAMART_TYPECODE, data)
}

/**
 * Generic method to add or update data to DataManager table like Data Source
 * and Datamart.  This uses api.boundCall for better performance
 * @param tableName : Name of the data source or mart
 * @param tableId : TableId
 * @param typeCode : TypeCode of the table (DMDS for datasource, DM for datamart)
 * @param data to be inserted - a list of Map
 * @return
 */
def addOrUpdateDataToDMTable(tableName, typeCode, data) {
    if (tableName && typeCode) {
        def alldata = [] as List
        if (data && data.size() > 0) {
            for (oneItem in data) {
                api.global.dmdsHeader = oneItem?.keySet()
                // data to be send are only values in the same order as columns defined above
                def dataToSend = oneItem?.values()
                alldata.add(dataToSend)
            }
        }

        def tableId = retrieveDMDSTableId(tableName, typeCode)
        def actualData = api.jsonEncode(["data": ["header": api.global.dmdsHeader, "data": alldata]])
        def boundAddPath = libs.__LIBRARY__.Constants.RESTAPI_DMDS_LOADDATA + tableId
        def result = doBoundCall(boundAddPath, actualData)
        def statusCode = result?.statusCode;

        if ("200".equals(statusCode)) {
            libs.__LIBRARY__.LoggerUtils.LOG("addOrUpdateDataToDMTable", "INFO", "Successfully add / update table $tableName : rowcount " + data?.size())
        } else {
            api.throwException("Unable to add / update $typeCode - $tableName with error response : " + api.jsonEncode(result))
        }
    }
}

/*
 * A utility method that makes the api.boundCall and returns the actual server response
 * @param boundPath : The REST Api relative URL
 * @param actualData : Contains actual data to be used as part of the REST API
 * @return
 */

def doBoundCall(boundPath, actualData) {
    def copyResponse = api.boundCall(libs.__LIBRARY__.Constants.BOUND_PARTITION_NAME, boundPath, actualData)
    return copyResponse
}

/*
 * A utility method that returns the table Id of a DS or DM
 * @param tableName : Name of the data source or data mart
 * @param typeCode : Type code representing DS or DM (possible values DM or DMDS)
 * @return
 */

def retrieveDMDSTableId(tableName, typeCode) {
    def dataSrc = api.find(typeCode, Filter.equal("uniqueName", tableName))
    return dataSrc[0]?.typedId
}

/**
 * Generic method to add or update data to JSON based PricingParameter table. This uses api.boundCall for
 * better performance
 * @param tableName : Name of the JSON PP table
 * @param tableId : TableId
 * @param typeCode : TypeCode of the table
 * @param keyData (include key for each row of data below)
 * @param options (actual data list in JSON format)
 * @return
 */
def addOrUpdateJSONTableData(String tableName, String tableId, String typeCode, List keyData, List options) {
    if (!tableId) {
        tableId = api.findLookupTable(tableName)?.id
    }
    List dataToSend = []
    options.eachWithIndex { Map option, int index ->
        List row = []
        row.add(tableId)
        row.add((index < keyData.size()) ? keyData.get(index) : "EmptyKey")
        row.add(api.jsonEncode(option))
        dataToSend.add(row)
    }

    Map req = [data: [
            header: ["lookupTable", "name", "attributeExtension"],
            data  : dataToSend
    ]]

    String body = api.jsonEncode(req)

    api.trace("body", body)

    def result = api.boundCall(
            Constants.BOUND_PARTITION_NAME,
            Constants.JSON_BOUND_CALL_ADDORUPDATE_URI + typeCode,
            body,
            true)

    if ("200".equals(result?.statusCode)) {
        api.logInfo("############# addOrUpdateJSONTableData", "Successfully add / update table $tableName : rowcount " + options?.size())
    } else {
        api.throwException("Unable to add / update table $tableName with error response : " + api.jsonEncode(req))
    }
}