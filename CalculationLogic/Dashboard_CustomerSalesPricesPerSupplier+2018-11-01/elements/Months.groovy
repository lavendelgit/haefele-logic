def months = []
def currentMonth = api.calendar()

currentMonth.set(Calendar.DAY_OF_MONTH, 1)
//Start:::: To Delete these lines after the demo for Vivek G is done...
currentMonth.set(Calendar.YEAR, 2019)
currentMonth.set(Calendar.MONTH, 11)
//End:::: To Delete these lines above this after Vivek G is done...
for (def i in (1..12)) {
    months.add(currentMonth.getTime().format("yyyy-MM-dd"))
    currentMonth.add(Calendar.MONTH, -1)
}

return months.reverse()