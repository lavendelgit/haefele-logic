def sources = api.find("PX50", 0, 0, "attribute11", ["attribute11"], true, Filter.equal("name", "S_ZPAP")).attribute11;

def input = api.options("Source", sources) ?: sources
lib.TraceUtility.developmentTrace ("Source Printing", input)
def output = (input instanceof List)?input:[input]
return output