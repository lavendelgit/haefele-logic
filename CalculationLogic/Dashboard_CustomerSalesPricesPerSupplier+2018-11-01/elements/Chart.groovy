def create(type) {
    def months = api.getElement("Months")
    def data = api.getElement("Data")

    def stacking = [
        grouped: null,
        stacked: "normal",
        percent: "percent"
    ]

    def dataLabelsFormat = [
        grouped: "{point.y:,.0f}",
        stacked: "{point.y:,.0f}",
        percent: "{point.percentage:.2f}%",
    ]

    def series = data
        .groupBy { r -> r.supplierId }
        .collect { supplier, records -> [
            name: supplier,
            data: records.collect { r -> [y: r.count, name: r.supplierName]},
            color: supplier == 'Sonstige' ? '#d1d8e0' : null,
            dataLabels: [
                enabled: true,
                format: dataLabelsFormat[type]
            ]
        ]}

    api.trace("series", "", series)

    def chartDef = [
        chart: [
            type: "column",
            zoomType: "xy"
        ],
        title: [
            text: ""
        ],
        xAxis: [[
                    title: [
                        text: "Month"
                    ],
                    categories: months
                ]],
        yAxis: [[
                    title: [
                        text: "Count"
                    ],
                    stackLabels: [
                        enabled: true
                    ]
                ]],
        plotOptions: [
            column: [
                stacking: stacking[type]
            ]
        ],
        tooltip: [
            headerFormat: 'Supplier: {series.name}<br/>',
            pointFormat: 'Name: {point.name} <br/>Count: {point.y}'
        ],
        legend: [
            enabled: true,
            layout: "vertical",
            align: "right",
            verticalAlign: "middle"
        ],
        series: series
    ]

    api.buildFlexChart(chartDef)

}