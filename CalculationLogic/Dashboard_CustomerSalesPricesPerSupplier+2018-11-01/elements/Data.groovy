if (api.isSyntaxCheck()) {
    return []
}

def ctx = api.getDatamartContext()

def months = api.getElement("Months")
def sources = api.getElement("Sources")
def topSuppliersCount = (api.getElement("Top") ?: 40) as Integer;
def customerSector = api.getElement("CustomerSector")

def dm1 = ctx.getDatamart("CustomerSalesPricesDM")
def q1 = ctx.newQuery(dm1)
q1.select("SourceID", "sourceId")
q1.select("Material")
q1.select("CustomerId")
q1.select("ValidFrom", "validFrom")
q1.select("ValidTo", "validTo")
//q1.select("Regellieferant Nr", "supplierId")
q1.select("SupplierId", "supplierId")

if (customerSector) {
    q1.where(Filter.in("Branche", customerSector))
}

def thisMonth = months.last()

def topSuppliersSQL = """
    SELECT t1.supplierId, count(1) FROM T1
    WHERE t1.validFrom <= CAST('${thisMonth}' as DATE)
      AND t1.validTo >= CAST('${thisMonth}' as DATE)
    GROUP BY t1.supplierId
    ORDER BY count desc
    LIMIT ${topSuppliersCount}
"""


def sql = """
    SELECT m.month, t1.supplierid, COUNT(1) FROM (
        VALUES ${months.collect{ m -> "('" + m + "')"}.join(", ")}
    ) m (month)
    CROSS JOIN T1
    WHERE t1.supplierId IN (
        SELECT supplierId FROM (${topSuppliersSQL})
    )
    AND t1.sourceId IN ('${sources.join("','")}') 
    AND t1.validFrom <= CAST(m.month as DATE)
    AND t1.validTo >= CAST(m.month as DATE)
    GROUP BY m.month, t1.supplierId

    UNION ALL

    SELECT m.month, 'Sonstige' as supplierid, COUNT(1) FROM (
        VALUES ${months.collect{ m -> "('" + m + "')"}.join(", ")}
    ) m (month)
    CROSS JOIN T1
    WHERE t1.supplierId NOT IN (
            SELECT supplierId FROM (${topSuppliersSQL})
        )
    AND t1.sourceId IN ('${sources.join("','")}')
    AND t1.validFrom <= CAST(m.month as DATE)
    AND t1.validTo >= CAST(m.month as DATE)
    GROUP BY m.month

    ORDER BY m.month, count DESC
    LIMIT ${(topSuppliersCount + 1) * months.size()}
"""

api.trace("sql", "", sql)

def supplierNames = api.getElement("SupplierNames")

def result = ctx.executeSqlQuery(sql, q1)?.collect { r ->
    [
        month: r.month,
        supplierId: r.supplierid,
        supplierName: r.supplierid == 'Sonstige' ? 'Sonstige' : supplierNames?.get(r.supplierid),
        count: r.count
    ]
}
?.sort { r1, r2 ->
    // first, order by month
    (r1.month <=> r2.month) ?:
    // then by count, but push 'Sonstige' to the bottom
    (r2.supplierName != 'Sonstige' ? r2.count : 0) <=> (r1.supplierName != 'Sonstige' ? r1.count : 0)
}

api.trace("result", "", result)

return result
