api.findLookupTableValues("SupplierPriceIncrease")
    ?.collectEntries{ r -> [r.name, r.attribute1] } ?: [:]

