def resultMatrix = api.newMatrix("Supplier ID","Supplier Name","Month","Count of ZPAPs")
resultMatrix.setTitle("Count of ZPAPs per Supplier")
resultMatrix.setColumnFormat("Count of ZPAPs", FieldFormatType.NUMERIC)
resultMatrix.setPreferenceName("CountOfCustomerSalesPricesPerSupplierPerMonth")
resultMatrix.setEnableClientFilter(true)

def data = api.getElement("Data")


for (row in data) {
    resultMatrix.addRow([
        "Supplier ID": row.supplierId,
        "Supplier Name": row.supplierName,
        "Month": row.month,
        "Count of ZPAPs": row.count
    ])
}

return resultMatrix
