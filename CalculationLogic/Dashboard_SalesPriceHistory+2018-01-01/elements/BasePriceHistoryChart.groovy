def baseCosts = api.getElement("BaseCosts");

def chartDef = [
        chart: [
            type: 'scatter'
        ],
        title: [
            text: 'Base Cost History'
        ],
        xAxis: [[
                type: 'datetime'
        ]],
        tooltip: [
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%Y-%m-%d}: {point.y:.2f}'
        ],
        series: [[
             name: 'Base Cost',
             data: baseCosts.collect{r -> [
                     api.parseDate("yyyy-MM-dd", r.attribute1),
                     r.attribute3?.toBigDecimal()
             ]}
        ]]
]

api.buildFlexChart(chartDef)