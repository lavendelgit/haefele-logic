return api.getElement("Transactions")
        ?.groupBy { t -> t.units }
        ?.collectEntries { units, ts ->
            return [units, ts*.salesPricePer100Units.sum() / ts.size()]
        }
        ?.sort()
