def transactions = api.getElement("AvgSalesPricePerUnits")

def resultMatrix = api.newMatrix("Menge", "VK per 100")
resultMatrix.setTitle("Average Sales Price for a specific number of units")
resultMatrix.setColumnFormat("Menge", FieldFormatType.NUMERIC)
resultMatrix.setColumnFormat("VK per 100", FieldFormatType.MONEY)

transactions.forEach { units, avgSalesPrice ->
    resultMatrix.addRow([
            "Menge": units,
            "VK per 100": avgSalesPrice
    ])
}

return resultMatrix