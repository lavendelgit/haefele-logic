if (api.isSyntaxCheck()) {
    return [:]
}

def timeFrame = api.getElement("TimeFrame")

def dmCtx = api.getDatamartContext()
def datamartQuery = dmCtx.newQuery(dmCtx.getTable("TransactionsDM"),false)


datamartQuery.select("SalesPricePer100Units", "salesPricePer100Units")
datamartQuery.select("Units", "units")


datamartQuery.where(Filter.isNotNull("SalesPricePer100Units"))
datamartQuery.where(Filter.notEqual("SalesPricePer100Units", 0))
datamartQuery.where(Filter.equal("Material", api.product("sku")))

if (timeFrame == "Last 12 months") {
    def yearAgo = lib.Date.yearAgoFirstOfMonth()
    datamartQuery.where(Filter.greaterOrEqual("InvoiceDate", yearAgo))
}

def result = dmCtx.executeQuery(datamartQuery)

result?.calculateSummaryStatistics()

return  result?.getData()?.collect()

