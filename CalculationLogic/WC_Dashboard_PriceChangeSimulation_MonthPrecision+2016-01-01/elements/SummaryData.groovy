def htmlPortlet = api.newController()
Map summaryData = api.local.SummaryData ?: [:]
api.trace("summaryData", summaryData)
StringBuilder htmlBuilder = new StringBuilder()
Map COLORS = Constants.SUMMARY_COLORS

boolean showDashboard = out.ShowDashboard

if (showDashboard) {
    htmlBuilder.append("<div style='float: left;width: 100%;padding: 5px;height: 100%;margin: 5px;background-color:${COLORS.BG_COLOR};'>")
    Map simulationSavedInput = api.findLookupTableValues("SimulationInputs", Filter.equal("name", out.SimulationName))?.find()
    def berlinTimeZone = api.getTimeZone("Europe/Berlin")
    def runDateForSimulation = api.parseDateTime("yyyy-MM-dd'T'HH:mm:ss.SSS", simulationSavedInput.attributeExtension___schedule_date)
    String simulationRunTime = runDateForSimulation?.toDateTime(berlinTimeZone)
                                                   ?.toString("yyyy-MM-dd HH:mm:ss")

    htmlBuilder.append("<h3 style='color:${COLORS.TITLE}'>${out.SimulationName} results calculated on ${simulationRunTime}</h3>")
    htmlBuilder.append("</div>")
    htmlBuilder.append("<div>")
    BigDecimal actualRevenue = summaryData.Revenue.actualFromBreakUp != 0 ? summaryData.Revenue.actualFromBreakUp : 1
    BigDecimal expectedRevenue = summaryData.Revenue.expectedFromBreakUp != 0 ? summaryData.Revenue.expectedFromBreakUp : 1
    htmlBuilder.append(generateSection('ACTUAL', 'SALES DATA',
                                       ['Analysis Year'        : out.Year ?: '2020',
                                        'Total Revenue'        : summaryData.Revenue.actualInMillionFromBreakUp,
                                        'Total EK Wert'        : summaryData.Cost.actualInMillion,
                                        'Total Absolute Margin': summaryData.PocketMargin.actualInMillionFromBreakUp,
                                        'Total % Margin'       : libs.SharedLib.RoundingUtils.round(((summaryData.PocketMargin.actualFromBreakUp / actualRevenue) * 100), Constants.DECIMAL_PLACE.PERCENT) + " %",
                                        'Total Quantity Sold'  : Library.formatMoney(libs.SharedLib.RoundingUtils.round(summaryData.totalQuantity.actual, 0)),
                                        'Total No. of Articles': summaryData.totalMaterials,
                                        'Simulation Type'      : summaryData.simulationType ?: "n/a"], 49))

    htmlBuilder.append(generateSection('OUTPUT ANALYSIS', 'OUTPUT ANALYSIS',
                                       ['Target Year'                        : out.TargetYear ?: '2021',
                                        'Total Revenue'                      : summaryData.Revenue.expectedInMillionFromBreakUp,
                                        'Total EK Wert'                      : summaryData.Cost.expectedInMillion,
                                        'Total Absolute Margin'              : summaryData.PocketMargin.expectedInMillionFromBreakUp,
                                        'Total % Margin'                     : libs.SharedLib.RoundingUtils.round(((summaryData.PocketMargin.expectedFromBreakUp / expectedRevenue) * 100), Constants.DECIMAL_PLACE.PERCENT) + " %",
                                        'Total Quantity Expected'            : Library.formatMoney(libs.SharedLib.RoundingUtils.round(summaryData.totalQuantity.expected, 0)),
                                        'Total No. of Articles'              : summaryData.totalMaterials,
                                        'Projected Avg. Gross Price Change %': summaryData.avgGrossPriceChangePercent], 49))
} else {
    htmlBuilder.append("<div>")
    htmlPortlet.addHTML("${out.SimulationName} dashboard data will be available soon.")
}
htmlBuilder.append("</div>")
htmlPortlet.addHTML(htmlBuilder.toString())
return htmlPortlet

protected String generateSection(String title, String subtitle, Map contents, int width) {
    StringBuilder htmlBuilder = new StringBuilder()

    Map COLORS = Constants.SUMMARY_COLORS
    htmlBuilder.append("<div style='float: left;width: ${width}%;padding: 5px;height: 100%;margin: 5px;background-color:${COLORS.BG_COLOR};'>")
    htmlBuilder.append("<h5 style='color:${COLORS.TITLE}'>${title}</h5>")
    htmlBuilder.append("<h3 style='color:${COLORS.TITLE}'>${subtitle}</h3>")
    htmlBuilder.append("<table style='border: 2px solid #FFFFFF;width: 100%;text-align: center;border-collapse: collapse;'>")

    String rowStyle = "background: ${COLORS.BG_COLOR};font-size: 14px;border: 1px solid #FFFFFF;padding: 3px 4px;color: ${COLORS.VALUE};"
    contents.each { String contentTitle, contentValue ->
        htmlBuilder.append("<tr>")
        htmlBuilder.append("<td style='${rowStyle};text-align: left;font-weight:bold'>").append(contentTitle).append("</td>")
        htmlBuilder.append("<td style='${rowStyle};text-align: right'>").append(contentValue).append("</td>")
        htmlBuilder.append("</tr>")
    }
    htmlBuilder.append("</table>")
/*
    contents.each { String contentTitle, contentValue ->
        String bgColor = contentTitle?.startsWith("#") ? COLORS.REC_BG_COLOR : COLORS.BG_COLOR
        contentTitle = contentTitle?.startsWith("#") ? contentTitle.substring(2) : contentTitle
        htmlBuilder.append("<div style='padding:5px;border-style:solid;border-width:1px;border-color:${COLORS.BORDER_COLOR};background-color:${bgColor};'>")
        htmlBuilder.append("<div style='padding:2px;overflow: auto;white-space: nowrap;color:${COLORS.LABEL};background-color:${bgColor};'><b>${contentTitle}</b></div>")
        htmlBuilder.append("<div style='padding:2px;overflow: auto;white-space: nowrap;font-weight:bold;color:${COLORS.VALUE};font-size:16px;background-color:${bgColor};'>${contentValue ?: '-'}</div>")
        htmlBuilder.append("</div>")
    }
*/
    htmlBuilder.append("</div>")
    return htmlBuilder.toString()
}
