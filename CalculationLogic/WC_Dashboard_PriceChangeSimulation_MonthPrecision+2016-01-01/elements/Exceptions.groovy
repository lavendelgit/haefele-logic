List allExceptionKeys = out.UserSelectedPercentageChangeInput?.findAll { it -> it["Pricing Inputs For"] == "Exceptions" }
List exceptionKeys = allExceptionKeys ? allExceptionKeys["Exception Key"]?.unique() : []

Map grossPriceChangePercentage = [:]
Map purchasePriceChangePercentage = [:]
Map quantityChangePercentage = [:]
List exceptionInputsDetails = []
BigDecimal grossPrice
BigDecimal purchasePrice
BigDecimal quantity

exceptionKeys?.each { exceptionKey ->
    List exceptionInputRowsForKeys = out.UserSelectedPercentageChangeInput.findAll { row -> row["Exception Key"] == exceptionKey}
    String dataFilter = exceptionInputRowsForKeys["Exception Filter"]
  
    Constants.MONTH_NAMES.each { monthName ->
        Map exceptionInputRow = exceptionInputRowsForKeys.find { it -> it.Month == monthName }
      
        grossPrice = exceptionInputRow ? exceptionInputRow["Gross Price % Change"] as BigDecimal : BigDecimal.ZERO
        purchasePrice = exceptionInputRow ? exceptionInputRow["Purchase Price % Change"] as BigDecimal : BigDecimal.ZERO
        quantity = exceptionInputRow ? exceptionInputRow["Quantity % Change"] as BigDecimal : BigDecimal.ZERO
      
        grossPriceChangePercentage.put(monthName, grossPrice/100)
      	purchasePriceChangePercentage.put(monthName, purchasePrice/100)
        quantityChangePercentage.put(monthName, quantity/100)
    }
    Map exceptionInputsRow = Library.getInitializedExceptionDetails(exceptionKey, true, dataFilter,
            grossPriceChangePercentage,
            purchasePriceChangePercentage,
            quantityChangePercentage)
    exceptionInputsDetails.add(exceptionInputsRow)
}

return exceptionInputsDetails