def LOG(arg1, arg2, arg3) {
    api.trace(arg1, arg2, arg3)
    api.logInfo("########### PCS : ${arg1} : ${arg2}", arg3)
}

def LOG(arg1, arg2) {
    api.trace(arg1, arg2)
    api.logInfo("########### PCS : ${arg1}", arg2)
}
/**
 * Helper method to get a map of keys and labels for a option input. It works for SIMPLE / MLTV1 type of PP table.
 * @param customerId
 * @return
 */
Map getKeyLabelMap(String ppTableName, String keyField, String valueField, List filters = null) {
    def data = filters ? api.findLookupTableValues(ppTableName, *filters) : api.findLookupTableValues(ppTableName)
    if (data) {
        return data?.collectEntries {
            [(it.get(keyField)): it.get(valueField)]
        }
    }
}

/**
 * Helper method to get a map of keys and labels for a option input. It works for SIMPLE / MLTV1 type of PP table.
 * @param customerId
 * @return
 */
Map getKeyLabelMap(String ppTableName, List filters = null) {
    def data = filters ? api.findLookupTableValues(ppTableName, *filters) : api.findLookupTableValues(ppTableName)
    if (data) {
        def column = data[0]?.type == 'SIMPLE' ? 'value' : 'attribute1' // what column values is needed
        return data?.collectEntries {
            [(it.name): it.get(column)]
        }
    }
}

/**
 * Helper method to get a list of keys for a option input. It works for SIMPLE / MLTV1 type of PP table.
 * @param customerId
 * @return
 */
List getKeyList(String ppTableName, String keyField, List filters = null) {
    def data = filters ? api.findLookupTableValues(ppTableName, *filters) : api.findLookupTableValues(ppTableName)
    return data?.collect {
        it.get(keyField)
    }?.unique()
}

/**
 * Helper method to get a list of keys for a option input. It works for SIMPLE / MLTV1 type of PP table.
 * @param customerId
 * @return
 */
List getKeyList(String ppTableName, List filters = null) {
    def data = filters ? api.findLookupTableValues(ppTableName, *filters) : api.findLookupTableValues(ppTableName)
    return data?.collect {
        it.name
    }
}

/**
 * Utility method to read all rows
 * @param finder
 * @return
 * Example engine usage:
 *
 * List products = readAllRows() { int startRow, int maxRow ->
 *             api.find("P", startRow, maxRow, 'sku', ['sku'], true, articleFilter)
 *}
 */
List readAllRows(Closure finder) {
    int startRow = 0
    List allRows = []
    int maxRow = api.getMaxFindResultsLimit()
    while (result = finder.call(startRow, maxRow)) {
        startRow += result.size()
        allRows.addAll(result)
    }
    return allRows
}

List getArticlesForFilter(articleFilter) {
    if (articleFilter) {
        List products = readAllRows() { int startRow, int maxRow -> api.find("P", startRow, maxRow, 'sku', ['sku'], true, articleFilter)
        }
        return products ? products.collect { product -> product.sku
        } : []
    }
    return []
}

List getAnalysisYears() {
    //TODO : Read from DM
    return ['2016', '2017', '2018', '2019', '2020']
}

List getTargetYears() {
    def currentYear = api.calendar().get(Calendar.YEAR)
    List targetYears = []
    for (int i = 0; i < 3; i++) {
        targetYears << currentYear + i
    }
    return targetYears
}

List getConditionTypes() {
    List filters = [Filter.equal('attribute3', 'Yes')]
    def data = api.findLookupTableValues(Constants.TABLE_CONDITION_MASTER, *filters)
    return data?.collect { row ->
        List conditionNameTableList = []
        String conditionName = row.name?.trim()
        String conditionTables = row.attribute2
        if (conditionTables) {
            conditionNameTableList = conditionTables.split(',').collect { String conditionTableName -> conditionName + '-' + conditionTableName?.trim()
            }
        }
        return conditionNameTableList
    }?.flatten()?.unique()
}

protected String toPercent(BigDecimal numberValue) {
    return (numberValue != null) ? libs.SharedLib.RoundingUtils.round(numberValue, Constants.DECIMAL_PLACE.PERCENT) + ' %' : ''
}

protected String formatMoney(BigDecimal moneyValue) {
    def roundingUtils = libs.SharedLib.RoundingUtils
    return moneyValue ? api.formatNumber("###,###,###.##", roundingUtils.round(moneyValue, 0)) : ""
}

protected Boolean doesNonZeroValueExists(Map changePercent) {
    return changePercent.findAll { String month, BigDecimal percentageChange -> percentageChange != 0
    }.size() > 0
}

protected Map getInitializedException(String configurationName, String key, boolean selected, String dataFilter) {
    List exceptionMonthlyPercentageConfigurations = getPriceChangePercentageConfiguration(configurationName)
    String grossPriceChangeField = Constants.PRICE_SIMULATION_FIELDS[1]
    String purchasePriceChangeField = Constants.PRICE_SIMULATION_FIELDS[2]
    String quantityChangeField = Constants.PRICE_SIMULATION_FIELDS[3]

    return getInitializedExceptionDetails(key,
                                          selected,
                                          dataFilter,
                                          getPerMonthPercentageValue(exceptionMonthlyPercentageConfigurations, grossPriceChangeField),
                                          getPerMonthPercentageValue(exceptionMonthlyPercentageConfigurations, purchasePriceChangeField),
                                          getPerMonthPercentageValue(exceptionMonthlyPercentageConfigurations, quantityChangeField))
}

protected List getPriceChangePercentageConfiguration(String configurationName) {
    List filters = [Filter.equal("key2", configurationName)]

    return api.findLookupTableValues(Constants.INPUTS.SIMULATION_INPUT, Constants.PRICE_SIMULATION_FIELDS, null, *filters)
}

protected Map getPerMonthPercentageValue(List listOfAllPercentageValues, String fieldToFetch) {
    String monthNameField = Constants.PRICE_SIMULATION_FIELDS[0]

    return listOfAllPercentageValues.collectEntries { Map fields -> [(fields[monthNameField]): getPercentageValue(fields[fieldToFetch])]
    }
}

protected Float getPercentageValue(String percentageValue) {
    return percentageValue ? (percentageValue as BigDecimal)  : BigDecimal.ZERO
}

protected Map getInitializedExceptionDetails(String key,
                                             boolean selected,
                                             String dataFilter,
                                             Map monthlyGrossPriceChangePercent,
                                             Map monthlyPurchasePriceChangePercent,
                                             Map monthlyQuantityPriceChangePercent) {
    Map exceptionFields = Constants.EXCEPTION_FIELDS

    return [(exceptionFields.KEY)                         : key,
            (exceptionFields.SELECTED)                    : selected,
            (exceptionFields.DATA_FILTER)                 : dataFilter,
            (exceptionFields.GROSS_PRICE_CHANGE_CONFIG)   : monthlyGrossPriceChangePercent,
            (exceptionFields.PURCHASE_PRICE_CHANGE_CONFIG): monthlyPurchasePriceChangePercent,
            (exceptionFields.QUANTITY_CHANGE_PERCENT)     : monthlyQuantityPriceChangePercent]
}
