def params = [:]

return inlineConfiguratorEntry("Configurator_MonthlyAllSimulationInputs", "SimulationInputConfigurator", "Simulation Input Configurator", params)

Object inlineConfiguratorEntry(String configuratorLogic, String configuratorName, String label, Map params) {
    if (!configuratorName || !configuratorLogic) {
        api.throwException("Configurator name / logic not specified")
    }

    def configurator = api.inlineConfigurator(configuratorName, configuratorLogic)

    def conf = api.getParameter(configuratorName)
    if (conf != null && conf.getValue() == null) {
        params["configurator"] = configuratorName
        conf.setLabel(label)
        conf.setValue(params)
    }
    return configurator
}

/*Object inlineConfiguratorEntry(String configuratorLogic, String configuratorName, String label, Map params) {
    if (!configuratorName || !configuratorLogic) {
        api.throwException("Configurator name / logic not specified")
    }

    def configurator = api.inlineConfigurator(configuratorName, configuratorLogic)

    params.Status = "Scheduled"
    def conf = api.getParameter(configuratorName)
    if (conf != null) {
        params["configurator"] = configuratorName
        conf.setLabel(label)
        conf.setValue(params)
    }
    return configurator
}
*/