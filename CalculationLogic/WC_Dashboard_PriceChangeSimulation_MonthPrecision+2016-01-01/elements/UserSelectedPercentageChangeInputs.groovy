def dashUtl = lib.DashboardUtility
Map rowToInsert = [:]

BigDecimal grossPrice
BigDecimal purchasePrice
BigDecimal quantity

String title = "Configuration Inputs (${out.SimulationName})"
List columns = ['Configuration Type', 'Exception Key', 'Month', 'Gross Price % Change', 'Purchase Price % Change', 'Quantity % Change']
List columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.PERCENT, FieldFormatType.PERCENT, FieldFormatType.PERCENT]

def resultMatrix = dashUtl.newResultMatrix(title, columns, columnTypes)

List simulationInputRows = out.UserSelectedPercentageChangeInput?.findAll { simulationRows -> simulationRows["Pricing Inputs For"] == "Overall Simulation" }

simulationInputRows?.each { simulationInputRow ->
    grossPrice = out.PredictGrossPricePercentChange ? BigDecimal.ZERO : simulationInputRow["Gross Price % Change"] as BigDecimal
    purchasePrice = simulationInputRow["Purchase Price % Change"] as BigDecimal
    quantity = simulationInputRow["Quantity % Change"] as BigDecimal

    resultMatrix = addMatrixRow(resultMatrix, simulationInputRow["Pricing Inputs For"], " ", simulationInputRow["Month"], grossPrice, purchasePrice, quantity, "000000")
}

List exceptionInputRows = out.UserSelectedPercentageChangeInput?.findAll { simulationRows -> simulationRows["Pricing Inputs For"] == "Exceptions" }

exceptionInputRows?.each { exceptionInputRow ->
    grossPrice = out.PredictGrossPricePercentChange ? BigDecimal.ZERO : exceptionInputRow["Gross Price % Change"] as BigDecimal
    purchasePrice = exceptionInputRow["Purchase Price % Change"] as BigDecimal
    quantity = exceptionInputRow["Quantity % Change"] as BigDecimal

    resultMatrix = addMatrixRow(resultMatrix, exceptionInputRow["Pricing Inputs For"], exceptionInputRow["Exception Key"], exceptionInputRow["Month"], grossPrice, purchasePrice, quantity, "#0000CD")
}

return resultMatrix

protected ResultMatrix addMatrixRow(ResultMatrix resultMatrix, String configurationName, String exceptionKey, String month, BigDecimal grossPrice, BigDecimal purchasePrice, BigDecimal quantity, String colour) {
    List columns = ['Configuration Type', 'Exception Key', 'Month', 'Gross Price % Change', 'Purchase Price % Change', 'Quantity % Change']
    Map rowToInsert = [:]
    rowToInsert = [(columns[0]): resultMatrix.styledCell(configurationName, colour, "transparent", "light"),
                   (columns[1]): resultMatrix.styledCell(exceptionKey, colour, "transparent", "light"),
                   (columns[2]): resultMatrix.styledCell(month, colour, "transparent", "light"),
                   (columns[3]): resultMatrix.styledCell(api.formatNumber("##.##%", grossPrice / 100), colour, "transparent", "light"),
                   (columns[4]): resultMatrix.styledCell(api.formatNumber("##.##%", purchasePrice / 100), colour, "transparent", "light"),
                   (columns[5]): resultMatrix.styledCell(api.formatNumber("##.##%", quantity / 100), colour, "transparent", "light")]
    resultMatrix.addRow(rowToInsert)

    return resultMatrix
}