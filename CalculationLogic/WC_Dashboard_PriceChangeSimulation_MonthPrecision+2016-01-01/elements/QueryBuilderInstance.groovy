protected def getSQLQuery() {
    def ctx = api.getDatamartContext()
    def dm = ctx.getDatamart(libs.HaefeleCommon.Simulation.SIMULATION_INPUT_DATAMART_MAP[out.SimulationName])
    def dmQuery = ctx.newQuery(dm)
    Map fields = Constants.DB_FIELDS
    //dmQuery.select(fields.MATERIAL, fields.MATERIAL)
    addSumMonthlyOutputField(dmQuery, fields.EXPECTED_REVENUE, fields.EXPECTED_REVENUE)
    addSumMonthlyOutputField(dmQuery, fields.EXPECTED_BASE_COST, fields.EXPECTED_BASE_COST)
    addSumMonthlyOutputField(dmQuery, fields.EXPECTED_POCKET_MARGIN, fields.EXPECTED_POCKET_MARGIN)
    addSumMonthlyOutputField(dmQuery, fields.EXPECTED_QUANTITY_SOLD, fields.EXPECTED_QUANTITY_SOLD)
    addSumMonthlyOutputField(dmQuery, fields.EXPECTED_TOTAL_MARGIN, fields.EXPECTED_TOTAL_MARGIN)
    addSumMonthlyOutputField(dmQuery, fields.EXPECTED_TOTAL_BASE_COST, fields.EXPECTED_TOTAL_BASE_COST)
    addSumMonthlyOutputField(dmQuery, fields.TOTAL_QUANTITY_SOLD, fields.TOTAL_QUANTITY_SOLD)
    addSumMonthlyOutputField(dmQuery, fields.TOTAL_REVENUE, fields.TOTAL_REVENUE)
    addSumMonthlyOutputField(dmQuery, fields.CURRENT_POCKET_MARGIN, fields.CURRENT_POCKET_MARGIN)
    addSumMonthlyOutputField(dmQuery, fields.TOTAL_BASE_COST, fields.TOTAL_BASE_COST)
    addSumPricingTypeMonthlyField(dmQuery, fields.PT_CURRENT_CUSTOMER_REVENUE, fields.PT_CURRENT_CUSTOMER_REVENUE)
    addSumPricingTypeMonthlyField(dmQuery, fields.PT_CURRENT_TOTAL_MARGIN, fields.PT_CURRENT_TOTAL_MARGIN)
    addSumPricingTypeMonthlyField(dmQuery, fields.PT_EXPECTED_CUSTOMER_REVENUE, fields.PT_EXPECTED_CUSTOMER_REVENUE)
    addSumPricingTypeMonthlyField(dmQuery, fields.PT_EXPECTED_TOTAL_MARGIN, fields.PT_EXPECTED_TOTAL_MARGIN)
    addMonthlyOutputField(dmQuery, fields.PREDICTED_GROSS_PRICE, fields.PREDICTED_GROSS_PRICE)
    addMonthlyOutputField(dmQuery, fields.GROSS_PRICE, fields.GROSS_PRICE)
/*    Use this filter if you want to investigate individual article or group of articles result. */

    /*if (api.isDebugMode()) {
        List filters = [Filter.equal(fields.MATERIAL, '007.59.300')]//'000.30.900')]
        dmQuery.where(*filters)
    }
    api.trace("dmQuery", dmQuery)*/

    return dmQuery
}

protected void addMonthlyOutputField(def dmQuery, String fieldToFetch, String alias) {
    boolean requiresAliasEnclosure = true
    String fieldMonthlyAlias
    Closure monthlyFieldForCompleteYearClosure = { String monthPrefix, String aliasEnclosure ->
        fieldMonthlyAlias = "$monthPrefix$alias"
        dmQuery.select("$monthPrefix$fieldToFetch", "${getEnclosedAlias(aliasEnclosure, fieldMonthlyAlias)}")
    }

    addMonthlyContext(requiresAliasEnclosure, monthlyFieldForCompleteYearClosure)
}

protected void addSumMonthlyOutputField(def dmQuery, String fieldToFetch, String alias) {
    String fieldMonthlyAlias
    List monthlyFields = []
    Constants.MONTHS_PREFIXES.each { String monthPrefix ->
        fieldMonthlyAlias = "$monthPrefix$alias"
        monthlyFields.add(fieldMonthlyAlias)
    }
    dmQuery.select("SUM(${monthlyFields.join(" + ")})", alias)
}

protected void addAvgMonthlyOutputField(def dmQuery, String fieldToFetch, String alias) {
    String fieldMonthlyAlias
    List monthlyFields = []
    Constants.MONTHS_PREFIXES.each { String monthPrefix ->
        fieldMonthlyAlias = "$monthPrefix$alias"
        monthlyFields.add(fieldMonthlyAlias)
    }
    dmQuery.select("SUM(${monthlyFields.join(" + ")})", alias)
}

protected String getEnclosedAlias(String aliasEnclosure, String alias) {
    return alias
}

protected void addMonthlyContext(boolean requiresAliasEnclosure, Closure specificProcessor) {
    String aliasEnclosure = getAliasEnclosure(requiresAliasEnclosure)
    Constants.MONTHS_PREFIXES.each { String monthPrefix ->
        specificProcessor(monthPrefix, aliasEnclosure)
    }
}

protected String getFirstSeparator(boolean isFirstSet) {
    return isFirstSet ? '' : ','
}

protected String getAliasEnclosure(boolean hasSpaceInAlias) {
    return hasSpaceInAlias ? Constants.ALIAS_ENCLOSER : ""
}

protected void addPricingTypeMonthlyField(def dmQuery, String fieldToFetch, String alias) {
    boolean requiresAliasEnclosure = true
    String fieldMonthlyAlias
    Closure monthlyFieldForCompleteYearClosure = { String monthPrefix, String aliasEnclosure ->
        Constants.PRICING_TYPES_CONST.each { String key, String pricingType ->
            fieldMonthlyAlias = "$monthPrefix$alias$pricingType"
            dmQuery.select("$monthPrefix$fieldToFetch$pricingType", "${getEnclosedAlias(aliasEnclosure, fieldMonthlyAlias)}")
        }
    }
    addMonthlyContext(requiresAliasEnclosure, monthlyFieldForCompleteYearClosure)
}


protected void addSumPricingTypeMonthlyField(def dmQuery, String fieldToFetch, String alias) {
    String fieldMonthlyAlias
    Constants.PRICING_TYPES_CONST.each { String key, String pricingType ->
        List monthlyFields = []
        Constants.MONTHS_PREFIXES.each { String monthPrefix ->
            fieldMonthlyAlias = "$monthPrefix$alias$pricingType"
            monthlyFields.add(fieldMonthlyAlias)
        }
        dmQuery.select("SUM(${monthlyFields.join(" + ")})", alias + pricingType)
    }
}

/*
protected void addSumPricingTypeMonthlyField(def dmQuery, String fieldToFetch, String alias) {
    boolean requiresAliasEnclosure = true
    String fieldMonthlyAlias
    Closure monthlyFieldForCompleteYearClosure = { String monthPrefix, String aliasEnclosure ->
        Constants.PRICING_TYPES_CONST.each { String key, String pricingType ->
            fieldMonthlyAlias = "$monthPrefix$alias$pricingType"
            dmQuery.select("$monthPrefix$fieldToFetch$pricingType", "${getEnclosedAlias(aliasEnclosure, fieldMonthlyAlias)}")
        }
    }
    addMonthlyContext(requiresAliasEnclosure, monthlyFieldForCompleteYearClosure)
}*/