import net.pricefx.formulaengine.scripting.Matrix2D

Matrix2D resultData = api.local.MatrixData
api.local.SummaryData = [:]
net.pricefx.server.dto.calculation.ResultMatrix resultMatrix = null


if (resultData && resultData.size() > 0) {
    if (out.ComputeResultMatrix) {
        resultMatrix = resultData.toResultMatrix()
        resultMatrix.setEnableClientFilter(true)
        resultMatrix.setDisableSorting(false)
        resultMatrix.setTitle("Monthly Price Change Simulation Results")
        resultMatrix.setPreferenceName("DashboardPriceChangeSimulationMatrixMonthly")
        extractSummaryData(resultMatrix)
    } else {
        extractSummaryData(resultData.toResultMatrix())
    }
} else {
    Map DUMMY_VALUES_OVERALL = ['actual'                : 0.0,
                                'current'               : 0.0,
                                'expected'              : 0.0,
                                'actualInMillion'       : "0.000 M",
                                'currentDelta'          : 0.00,
                                'currentDeltaInMillion' : "0.000 M",
                                'currentDeltaPercent'   : 0.00,
                                'currentInMillion'      : "0.000 M",
                                'currentChangePercent'  : 0.0,
                                'expectedDelta'         : 0.0,
                                'expectedDeltaInMillion': "0.000 M",
                                'expectedDeltaPercent'  : 0.0,
                                'expectedInMillion'     : "0.000 M",
                                'expectedChangePercent' : 0.0]
    Map DUMMY_BREAKUP_REVENUE = ['totalRevenue'         : 0,
                                 'totalMargin'          : 0,
                                 'totalRevenueInMillion': "0.000 M",
                                 'totalMarginInMillion' : "0.000 M",
                                 'marginPercent'        : 0]
    Map DUMMY_PERCENTAGE_CHANGE = ['January'  : 0.0,
                                   'February' : 0.0,
                                   'March'    : 0.0,
                                   'April'    : 0.0,
                                   'May'      : 0.0,
                                   'June'     : 0.0,
                                   'July'     : 0.0,
                                   'August'   : 0.0,
                                   'September': 0.0,
                                   'October'  : 0.0,
                                   'November' : 0.0,
                                   'December' : 0.0]
    Map DUMMY_EXPECTED_BREAKUP = ['totalRevenue'          : 0,
                                  'totalMargin'           : 0,
                                  'totalRevenueIdeal'     : 0,
                                  'totalMarginIdeal'      : 0,
                                  'priceChangePercent'    : DUMMY_PERCENTAGE_CHANGE,
                                  'costChangePercent'     : DUMMY_PERCENTAGE_CHANGE,
                                  'totalRevenueIdealDelta': 0,
                                  'totalMarginIdealDelta' : 0,
                                  'totalRevenueInMillion' : "0.000 M",
                                  'totalMarginInMillion'  : "0.000 M",
                                  'marginPercent'         : 0]

    api.local.SummaryData = ['totalTransactions'         : 0,
                             'totalQuantity'             : ['actual': 0, 'expected': 0],
                             'totalMaterials'            : 0,
                             'tagretMarginPercent'       : 0.0,
                             'avgGrossPriceChangePercent': 0.0,
                             'Revenue'                   : DUMMY_VALUES_OVERALL,
                             'Cost'                      : DUMMY_VALUES_OVERALL,
                             'PocketMargin'              : DUMMY_VALUES_OVERALL,
                             'CURRENT_BREAKUP'           : ['Markup'     : DUMMY_BREAKUP_REVENUE,
                                                            'NetPrice'   : DUMMY_BREAKUP_REVENUE,
                                                            'PerDiscount': DUMMY_BREAKUP_REVENUE,
                                                            'AbsDiscount': DUMMY_BREAKUP_REVENUE,
                                                            'XARTICLE'   : DUMMY_BREAKUP_REVENUE,
                                                            'INTERNAL'   : DUMMY_BREAKUP_REVENUE,
                                                            'NoDiscounts': DUMMY_BREAKUP_REVENUE],
                             'EXPECTED_BREAKUP'          : ['Markup'     : DUMMY_EXPECTED_BREAKUP,
                                                            'NetPrice'   : DUMMY_EXPECTED_BREAKUP,
                                                            'PerDiscount': DUMMY_EXPECTED_BREAKUP,
                                                            'AbsDiscount': DUMMY_EXPECTED_BREAKUP,
                                                            'XARTICLE'   : DUMMY_EXPECTED_BREAKUP,
                                                            'INTERNAL'   : DUMMY_EXPECTED_BREAKUP,
                                                            'NoDiscounts': DUMMY_EXPECTED_BREAKUP]]
}

api.trace("api.local.SummaryData", api.local.SummaryData)
return resultMatrix

protected void extractSummaryData(net.pricefx.server.dto.calculation.ResultMatrix resultMatrix) {
    Map summaryData = [:]
    List entries = resultMatrix?.getEntries()
    if (entries) {
        Map level1 = Constants.DB_FIELDS
        summaryData.totalTransactions = 0
        summaryData.tagretMarginPercent = libs.SharedLib.RoundingUtils.round(out.SimulationInputs.TargetMargin ?: 0.0, 2) + " %"
        summaryData.simulationType = out.SimulationInputs.PredictForInputType
        summaryData.totalQuantity = [actual  : getSummationAcrossMonths(level1.TOTAL_QUANTITY_SOLD, entries),
                                     expected: getSummationAcrossMonths(level1.EXPECTED_QUANTITY_SOLD, entries)]
        summaryData.totalMaterials = entries.size()
        summaryData.avgGrossPriceChangePercent = out.SimulationInputs.PredictGrossPricePercentChange ? libs.SharedLib.RoundingUtils.round((getChangePercent(level1.GROSS_PRICE, level1.PREDICTED_GROSS_PRICE, entries) ?: 0.0) * 100, 2) + " %" : "n/a"
        addSeriesAttributes(summaryData, entries, 'Revenue', level1.TOTAL_REVENUE, level1.EXPECTED_REVENUE)
        addSeriesAttributes(summaryData, entries, 'Cost', level1.TOTAL_BASE_COST, level1.EXPECTED_TOTAL_BASE_COST)
        addSeriesAttributes(summaryData, entries, 'PocketMargin', level1.CURRENT_POCKET_MARGIN, level1.EXPECTED_TOTAL_MARGIN)
    }
    api.local.SummaryData = summaryData
    libs.__LIBRARY__.TraceUtility.developmentTrace("Summary Data formed is printed", api.jsonEncode(summaryData))
}

protected BigDecimal getSummationAcrossMonths(String field, List dataSets) {
    api.trace("getSummationAcrossMonths : field", field)
    return dataSets?.getAt(field)?.findAll {
        it != null
    }?.sum() ?: 0.0
}

protected BigDecimal getChangePercent(String baseField, String compField, List dataSets) {
    BigDecimal baseTotal = getAggregate(baseField, dataSets) ?: 0.0
    BigDecimal compTotal = getAggregate(compField, dataSets) ?: 0.0

    return baseTotal ? (compTotal / baseTotal) : 0.0
}

protected BigDecimal getAggregate(String field, List dataSets) {
    api.trace("getAggregate $field", dataSets)
    List monthKeys = Constants.MONTHS_PREFIXES.collect { String prefix ->
        prefix + field
    }

    return (dataSets.collect { Map row ->
        average(row.subMap(monthKeys)?.values()?.findAll { it })
    }?.findAll {
        it
    })?.sum()
}

protected average(List listOfNumbers) {
    return listOfNumbers ? (listOfNumbers.sum() / listOfNumbers.size()) : 0.0
}

protected void addSeriesAttributes(Map summaryData,
                                   List entries,
                                   String seriesName,
                                   String currentColumnName,
                                   String expectedColumnName) {
    BigDecimal currentSummation = getSummationAcrossMonths(currentColumnName, entries)
    summaryData[seriesName] = [actual  : currentSummation,
                               current : currentSummation,
                               expected: getSummationAcrossMonths(expectedColumnName, entries)]

    addCalculatedAttributes(summaryData[seriesName])
}

protected void addCalculatedAttributes(Map seriesSummaryData) {
    def mathUtils = libs.__LIBRARY__.MathUtility
    seriesSummaryData.actualInMillion = toMillion(seriesSummaryData.actual)
    seriesSummaryData.currentDelta = (seriesSummaryData.current ?: 0) - (seriesSummaryData.actual ?: 0)
    seriesSummaryData.currentDeltaInMillion = toMillion(seriesSummaryData.currentDelta)
    seriesSummaryData.currentDeltaPercent = mathUtils.divide((seriesSummaryData.current - seriesSummaryData.actual), seriesSummaryData.actual) * 100
    seriesSummaryData.currentInMillion = toMillion(seriesSummaryData.current)
    seriesSummaryData.currentChangePercent = mathUtils.divide(seriesSummaryData.current, seriesSummaryData.actual) * 100
    seriesSummaryData.expectedDelta = (seriesSummaryData.expected ?: 0) - (seriesSummaryData.current ?: 0)
    seriesSummaryData.expectedDeltaInMillion = toMillion(seriesSummaryData.expectedDelta)
    seriesSummaryData.expectedDeltaPercent = mathUtils.divide((seriesSummaryData.expected ?: 0.0) - (seriesSummaryData.current ?: 0.0), seriesSummaryData.current) * 100
    seriesSummaryData.expectedInMillion = toMillion(seriesSummaryData.expected)
    seriesSummaryData.expectedChangePercent = mathUtils.divide(seriesSummaryData.expected, seriesSummaryData.current) * 100
}

protected String toMillion(moneyValue) {
    return (moneyValue != null) ? libs.SharedLib.RoundingUtils.round((moneyValue / 1000000), Constants.DECIMAL_PLACE.MONEY) + ' M' : ''
}
