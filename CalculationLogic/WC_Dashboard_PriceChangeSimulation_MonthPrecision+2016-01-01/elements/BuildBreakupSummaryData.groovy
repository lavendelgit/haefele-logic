if (!api.local.SummaryData) {
    return
}

List entries = api.local.MatrixData?.toResultMatrix()?.getEntries()
if (entries) {
    Constants.PRICING_TYPES_CONST.each { String discountTypeCode, String pricingType ->
        addBreakupByDiscountTypeSummary(api.local.SummaryData, entries, pricingType, "Current")
        addBreakupByDiscountTypeSummary(api.local.SummaryData, entries, pricingType, "Expected")
    }
}

protected void addBreakupByDiscountTypeSummary(Map summaryData, List entries, String pricingType, String outputType) {
    Map level1 = Constants.DB_FIELDS
    String upperCaseOutputType = outputType.toUpperCase()
    String revenueColumnPrefix = level1["PT_${upperCaseOutputType}_CUSTOMER_REVENUE"]
    //CURRENT_CUSTOMER_REVENUE or EXPECTED_CUSTOMER_REVENUE
    String revenueColumnName = "$revenueColumnPrefix$pricingType"
    String marginColumnPrefix = level1["PT_${upperCaseOutputType}_TOTAL_MARGIN"]
    //PT_CURRENT_TOTAL_MARGIN or PT_EXPECTED_TOTAL_MARGIN
    String marginColumnName = "$marginColumnPrefix$pricingType"
    if (revenueColumnName && marginColumnName) {
        Map sectionMap = summaryData[upperCaseOutputType + "_BREAKUP"] ?: [:]
        sectionMap[pricingType] = [totalRevenue: getSummationAcrossMonths(revenueColumnName, entries),
                                   totalMargin : getSummationAcrossMonths(marginColumnName, entries)]
        Map discountTypeMap = sectionMap[pricingType]

        if (upperCaseOutputType == "EXPECTED") {
            //libs.__LIBRARY__.TraceUtility.developmentTrace("The current think which is getting executed.. =====> ", upperCaseOutputType)
            String currentRevenueColumnName = level1["PT_CURRENT_CUSTOMER_REVENUE"] + pricingType
            BigDecimal currentTotalRevenue = summaryData["CURRENT_BREAKUP"][pricingType]?.totalRevenue
            BigDecimal currentTotalMargin = summaryData["CURRENT_BREAKUP"][pricingType]?.totalMargin
            discountTypeMap.totalRevenueIdeal = currentTotalRevenue
            discountTypeMap.totalMarginIdeal = currentTotalMargin
            if (out.GrossPriceChangePercent) {
                Map priceChangePercent = getEffectivePercentageValues(out.GrossPriceChangePercent)
                //libs.__LIBRARY__.TraceUtility.developmentTrace("Percentage for revenue changes =====> ", api.jsonEncode(priceChangePercent))
                BigDecimal revenueDelta = getPercentageChangeAmount(currentRevenueColumnName, priceChangePercent, entries)
                discountTypeMap.totalRevenueIdeal = currentTotalRevenue + revenueDelta
                discountTypeMap.totalMarginIdeal = currentTotalMargin + revenueDelta
                discountTypeMap.priceChangePercent = priceChangePercent
            }
            if (out.LandingPriceChangePercent) {
                String currentMarginColumnName = level1["PT_CURRENT_TOTAL_MARGIN"] + pricingType
                Map priceChangePercent = getEffectivePercentageValues(out.LandingPriceChangePercent)
                //libs.__LIBRARY__.TraceUtility.developmentTrace("Percentage for margin changes =====> ", api.jsonEncode(priceChangePercent))
                BigDecimal marginDelta = getPercentageChangeAmount(currentMarginColumnName, priceChangePercent, entries)
                discountTypeMap.totalMarginIdeal += marginDelta
                discountTypeMap.costChangePercent = priceChangePercent
            }
            discountTypeMap.totalRevenueIdealDelta = (discountTypeMap.totalRevenue ?: 0) - (discountTypeMap.totalRevenueIdeal ?: 0)
            discountTypeMap.totalMarginIdealDelta = (discountTypeMap.totalMargin ?: 0) - (discountTypeMap.totalMarginIdeal ?: 0)
        }

        discountTypeMap.totalRevenueInMillion = toMillion(discountTypeMap.totalRevenue)
        discountTypeMap.totalMarginInMillion = toMillion(discountTypeMap.totalMargin)
        discountTypeMap.marginPercent = discountTypeMap.totalRevenue ? (discountTypeMap.totalMargin / discountTypeMap.totalRevenue) : 0

        summaryData[upperCaseOutputType + "_BREAKUP"] = sectionMap
    }
}

protected BigDecimal getSummationAcrossMonths(String field, List dataSets) {
    api.trace("getSummationAcrossMonths : $field", dataSets)
    return dataSets?.getAt(field)?.findAll {
        it != null
    }?.sum() ?: 0.0
}

protected Map getEffectivePercentageValues(Map percentageChanges) {
    BigDecimal percentageTillDate = BigDecimal.ZERO
    Map updatedPercentageMap = Constants.MONTH_NAMES.collectEntries { String month ->
        percentageTillDate += percentageChanges[month] as BigDecimal
        return [(month): percentageTillDate]
    }

    return updatedPercentageMap
}

protected BigDecimal getPercentageChangeAmount(String field, Map monthlyPercentChange, List dataSets) {
    BigDecimal totalValue = BigDecimal.ZERO
    BigDecimal currentPercent
    Constants.MONTHS_PREFIXES.each { String prefix ->
        currentPercent = monthlyPercentChange[Constants.MONTHS_PREFIXES_NAMES[prefix]] ?: BigDecimal.ZERO
        totalValue += currentPercent * extactValueFromContainer(dataSets, "$prefix$field")
    }

    return totalValue
}

protected String toMillion(moneyValue) {
    return (moneyValue != null) ? libs.SharedLib.RoundingUtils.round((moneyValue / 1000000), Constants.DECIMAL_PLACE.MONEY) + ' M' : ''
}

protected BigDecimal extactValueFromContainer(List dataSet, String key) {
    if (!dataSet) {
        return BigDecimal.ZERO
    }
    return dataSet[key]?.sum {
        it ?: BigDecimal.ZERO
    } as BigDecimal
}
