def validityYear = api.input("Effective Year")
def verkaufsbuero = api.input("Verkaufsbuero")
def brancheCategory = api.input("Branche Category")
def prodII = api.input("Prodh. II")
def prodIII = api.input("Prodh. III")
def prodIV = api.input("Prodh. IV")

def fields = ['SourceID'          : 'Source',
              'Material'          : 'Material',
              'Materialkurztext'  : 'MaterialText',
              'customerId'        : 'CustomerId',
              'name'              : 'CustomerName',
              'ValidFrom'         : 'ValidFrom',
              'ValidTo'           : 'ValidTo',
              'SalesPrice'        : 'SalesPrice',
              'MaxDiscountedPrice': 'MaxDiscountedPrice',
              'CustomerSalesPrice': 'CustomerSalesPrice',
              'DiscountABS'       : 'DiscountABS',
              'DiscountPercentage': 'DiscountPercentage',
              'LossPerZPAP'       : 'LossPerZPAP',
              'LossPerPerZPAP'    : 'LossPerPerZPAP']
def filters = libs.__LIBRARY__.HaefeleSpecific.getCustomerSalesPriceFilters()

def validityStartDate = validityYear + "-01-01"
def validityEndDate = validityYear + "-12-31"
def generalLib = libs.__LIBRARY__.GeneralUtility
filters.add(generalLib.getFilterConditionForDateRange('ValidFrom', 'ValidTo', validityStartDate, validityEndDate))
libs.__LIBRARY__.TraceUtility.developmentTrace('Value to Print', validityYear)
filters.add(Filter.isNotNull('SalesPrice')) //Have those rows where Sales Price is available for comparison..
if (verkaufsbuero && !(verkaufsbuero.equals("All")))
    filters.add(Filter.equal('Verkaufsbereich3', verkaufsbuero))
if (brancheCategory && !(brancheCategory.equals("All")))
    filters.add(Filter.equal('BrancheCategory', brancheCategory))
if (verkaufsbuero && !brancheCategory)
    filters.add(Filter.isNull('BrancheCategory'))
if (prodII && !(prodII.equals("All")))
    filters.add(Filter.equal('ProdhII', prodII))
if (prodIII && !(prodIII.equals("All")))
    filters.add(Filter.equal('ProdhIII', prodIII))
if (prodIV && !(prodIV.equals("All")))
    filters.add(Filter.equal('ProdhIV', prodIV))

def orderBy = ['DiscountPercentage DESC']
Integer maxRows = 20000 as Integer
return libs.__LIBRARY__.DBQueries.getFirstNDataRows(fields, 'CustomerSalesPricesDM', filters, maxRows, 'DataMart', orderBy)