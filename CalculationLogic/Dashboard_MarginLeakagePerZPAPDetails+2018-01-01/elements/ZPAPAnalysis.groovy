import net.pricefx.common.api.FieldFormatType

def effectiveYear = api.input("Effective Year")
def maxDiscount = libs.__LIBRARY__.GeneralSettings.percentageValue("MaxDiscountPct") * 100
def columns = ['Source', 'Material', 'Materialkurztext', 'Customer', 'Valid From', 'Valid To', 'Grundpreis',
               'Verkaufspreis with ' + maxDiscount + '% Discount', 'NettoPreis', 'ZPAP Absolute Discount', 'Discount %',
               'Loss Per ZPAP', 'Loss % Per ZPAP']
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.DATE, FieldFormatType.DATE,
                   FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.PERCENT,
                   FieldFormatType.MONEY_EUR, FieldFormatType.PERCENT]
def dashUtl = lib.DashboardUtility
/*def resultMatrix = dashUtl.newResultMatrix('Possible Loss Per Unit Transaction For ZPAP Entries Violating Maximum Allowed Discount (Year-' + effectiveYear + ')',
        columns, columnTypes)
MK
 */
def resultMatrix = dashUtl.newResultMatrix('ZPAP Entries Greater Than Maximum Permitted Discount (Year-' + effectiveYear + ')',
        columns, columnTypes)
def zpapEntriesToDisplay = out.ZPAPEntries
zpapEntriesToDisplay?.each {
    resultMatrix.addRow([
            (columns[0]) : it['Source'],
            (columns[1]) : it['Material'],
            (columns[2]) : it['MaterialText'],
            (columns[3]) : dashUtl.boldHighlightWith(resultMatrix, (it['CustomerName'] + '-' + it['CustomerId'])),
            (columns[4]) : it['ValidFrom'],
            (columns[5]) : it['ValidTo'],
            (columns[6]) : it['SalesPrice'],
            (columns[7]) : it['MaxDiscountedPrice'],
            (columns[8]) : it['CustomerSalesPrice'],
            (columns[9]) : it['DiscountABS'],
            (columns[10]): dashUtl.boldHighlightWith(resultMatrix, it['DiscountPercentage']),
            (columns[11]): dashUtl.boldHighlightWith(resultMatrix, it['LossPerZPAP']),
            (columns[12]): dashUtl.boldHighlightWith(resultMatrix, it['LossPerPerZPAP'])
    ])
}
resultMatrix.setPreferenceName("ZPAP Detail View")

return resultMatrix
