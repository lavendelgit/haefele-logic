package Dashboard_CustomerSalesPricesPerCustomer.elements

def treshold = api.getElement("ShowCountTreshold") ?: 0
def customerSector = api.getElement("CustomerSector")
def targetDate = api.targetDate()?.format("yyyy-MM-dd")

def dmCtx = api.getDatamartContext()
def dmQuery = dmCtx.newQuery(dmCtx.getDatamart("CustomerSalesPricesDM"))
Filter salesOfficeFilter = libs.__LIBRARY__.HaefeleSpecific.getSalesOfficeInlandFilters('Verkaufsbereich3')
def filters = [Filter.lessOrEqual("ValidFrom", targetDate),
              Filter.greaterOrEqual("ValidTo", targetDate),
              salesOfficeFilter]

if (api.isUserInGroup("ProductGroupManager", api.user().loginName)) {
    def pgm = api.findLookupTableValues("ProductGroupManager")?.find()?.value
    filters.add(Filter.equal("SM", pgm))
}

if (customerSector) {
    filters.add(Filter.in("Branche", customerSector))
}

dmQuery.select("customerId")
dmQuery.select("name", "CustomerName")
dmQuery.select("COUNT(Material)", "count")
dmQuery.where(*filters)
dmQuery.orderBy("count DESC")
dmQuery.having(Filter.greaterThan("count", treshold))

return dmCtx.executeQuery(dmQuery)?.getData()?.collect()

