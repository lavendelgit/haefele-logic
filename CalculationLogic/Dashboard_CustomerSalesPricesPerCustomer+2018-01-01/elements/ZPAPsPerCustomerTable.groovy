package Dashboard_CustomerSalesPricesPerCustomer.elements

def resultMatrix = api.newMatrix("Customer Number", "Customer Name", "Count of ZPAPs")
resultMatrix.setTitle("Number of ZPAPs per Customer - table")
resultMatrix.setPreferenceName("MLZAPOverviewPerCust_Table")
resultMatrix.setColumnFormat("Count of ZPAPs", FieldFormatType.NUMERIC)

def zpapsPerCustomer = api.getElement("ZPAPsPerCustomer")

def total = 0;

for (row in zpapsPerCustomer) {
    resultMatrix.addRow([
            "Customer Number": row.customerId,
            "Customer Name"  : row.CustomerName,
            "Count of ZPAPs" : row.count
    ])
    total += row.count;
}
resultMatrix.addRow([
        "Customer Number": resultMatrix.styledCell("Total").withCSS("font-weight: bold"),
        "Count of ZPAPs" : resultMatrix.styledCell(total).withCSS("font-weight: bold")
])


return resultMatrix
