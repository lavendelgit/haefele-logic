package Dashboard_CustomerSalesPricesPerCustomer.elements

def zpapsPerCustomer = api.getElement("ZPAPsPerCustomer");
def top1000 = zpapsPerCustomer?.take(1000) // turboTreshold

def categories = top1000.collect { e -> e.CustomerName}
def data = top1000.collect { e -> e.count};

def chartDef = [
        chart: [
            type: 'column',
            zoomType: 'x'
        ],
        title: [
            text: 'ZPAPs per Customer'
        ],
        xAxis: [[
            title: [
                    text: "Customer Name"
            ],
            categories: categories
        ]],
        yAxis: [[
            title: [
                text: "Number of ZPAPs"
            ]
        ]],
        tooltip: [
            headerFormat: 'Customer Name: {point.key}<br/>',
            pointFormat: 'Number of ZPAPs: {point.y}'
        ],
        legend: [
            enabled: false
        ],
        series: [[
             data: data
        ]]
]


if (zpapsPerCustomer?.size() > 1000) {
    chartDef.subtitle = [
        text: 'Top 1000'
    ]
}

api.buildFlexChart(chartDef)