def resultMatrix = api.newMatrix("Article Number","Count of Clearing Rebates")
resultMatrix.setTitle("Count of Clearing Rebates per Product")
resultMatrix.setColumnFormat("Count of Clearing Rebates", FieldFormatType.NUMERIC)

def clearingRebatesPerProduct= api.getElement("ClearingRebatesPerProduct")

for (row in clearingRebatesPerProduct) {
    resultMatrix.addRow([
            "Article Number" : row.key,
            "Count of Clearing Rebates" : row.value
    ])
}

return resultMatrix
