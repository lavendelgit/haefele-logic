def clearingRebatesPerProduct = api.getElement("ClearingRebatesPerProduct");
def top1000 = clearingRebatesPerProduct.take(1000) // turboTreshold

def categories = top1000.collect { e -> e.key}
def data = top1000.collect { e -> e.value};

def chartDef = [
        chart: [
                type: "column",
                zoomType: "x"
        ],
        title: [
                text: "Clearing Rebates per Product"
        ],
        subtitle: [
                text: "Top 1000"
        ],
        xAxis: [[
                        title: [
                                text: "Article Number"
                        ],
                        categories: categories
                ]],
        yAxis: [[
                        title: [
                                text: "Number of Clearing Rebates"
                        ]
                ]],
        tooltip: [
                headerFormat: "Article Number: {point.key}<br/>",
                pointFormat: "Number of Clearing Rebates: {point.y}"
        ],
        legend: [
                enabled: false
        ],
        series: [[
                         data: data
                 ]]
]

if (clearingRebatesPerProduct?.size() > 1000) {
    chartDef.subtitle = [
            text: "Top 1000"
    ]
}

api.buildFlexChart(chartDef)