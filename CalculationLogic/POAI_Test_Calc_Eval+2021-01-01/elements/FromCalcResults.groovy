def controller = api.newController()

controller.addHTML("""
<h1>Result from this step's calculation</h1>
<ul>
<li>expected: 'a result of the calc'</li>
<li>got: '${model.outputs('twotab', 'calc1').Result}'</li>
</ul>
""")

return controller
