api.stringUserEntry("StringEntry", "Enter something :)", "A String Entry")

api.option(
        "OptionEntry",
        ["blue_pill", "red_pill"],
        [blue_pill: "Blue pill", red_pill: "Red Pill"]
)
def option = api.getParameter("OptionEntry")
if (option != null) {
    option.setLabel("A required option entry:")
    option.setRequired(true)
    option.setFormattingOption("placeholder", "Select an option :)")
}

return
