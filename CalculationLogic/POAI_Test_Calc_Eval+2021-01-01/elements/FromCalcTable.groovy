def chart = api.newChartBuilder().newBarLine()

chart.getOptions()
        .setTitle("my_table")
        .setXLabel("A text field")
        .setYLabel("A numeric field")

chart.addSeries()
        .setLabel("Current")
        .setDatamart(model.table("my_table").getSourceName())
        .setAxisX("a_text_field")
        .setAxisY("a_numeric_field")
        .withExpression()
        .setExpression("SUM(a_numeric_field)")

return chart.build()
