def sku = api.getElement("SKU")
def yearlySalesHistory = api.getElement("YearlySalesHistory")

yearlySalesHistory.forEach { sh ->
    api.trace("sh","",sh)
    api.addOrUpdate("PX", [
            "name": "SalesHistory",
            "sku": sku,
            "attribute1": sh.year,
            "attribute2": sh.customersCount,
            "attribute3": sh.unitsSold,
            "attribute4": sh.revenue,
            "attribute5": sh.marginAbs,
            "attribute6": sh.marginPerc
    ])
}
