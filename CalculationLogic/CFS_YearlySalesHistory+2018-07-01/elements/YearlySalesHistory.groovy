def sku = api.getElement("SKU")

def filters = [
        Filter.equal("Sku", sku),
        *lib.Filter.customersGermany()
]

def dmCtx = api.getDatamartContext()
def salesDM = dmCtx.getTable("SalesHistoryDM")
def datamartQuery = dmCtx.newQuery(salesDM)

datamartQuery.select("SUM(Revenue)", "revenue")
datamartQuery.select("SUM(UnitsSold)", "unitsSold")
datamartQuery.select("SUM(MarginalContributionAbs)", "marginAbs")
datamartQuery.select("SUM(MarginalContributionAbs)/SUM(Revenue)", "marginPerc")
datamartQuery.select("COUNTDISTINCT(CustomerId)", "customersCount")
datamartQuery.select("YearMonthYear", "year")
datamartQuery.where(filters)

def result = dmCtx.executeQuery(datamartQuery)

def records = result?.getData()?.collect{ r -> r << [year: r.year as Integer]} ?: []

return records