def force = api.getElement("Force")

if (force) {
    return true;
}

def salesHistoryPxLastUpdateDate() {
    def lastUpdateDateStr = api.find("PX8", 0, 1, "-lastUpdateDate", Filter.equal("name", "SalesHistory")).find()?.lastUpdateDate
    return api.parseDateTime("yyyy-MM-dd'T'HH:mm:ss", lastUpdateDateStr)
}

def salesHistoryDsLastUpdateDate() {
    def dmCtx = api.getDatamartContext()
    def salesDS = dmCtx.getDataSource("SalesHistory2")
    def query = dmCtx.newQuery(salesDS, false)

    query.select("lastUpdateDate")
    query.orderBy("lastUpdateDate DESC")
    query.setMaxRows(1)

    return dmCtx.executeQuery(query)?.getData()?.collect()?.find()?.lastUpdateDate
}

if (api.global.updateNeeded == null) {
    def salesHistoryPxLastUpdateDate = salesHistoryPxLastUpdateDate()
    def salesHistoryDsLastUpdateDate = salesHistoryDsLastUpdateDate()


    api.trace("salesHistoryPxLastUpdateDate", "", salesHistoryPxLastUpdateDate)
    api.trace("salesHistoryDsLastUpdateDate", "", salesHistoryDsLastUpdateDate)

    api.global.updateNeeded = salesHistoryPxLastUpdateDate == null || salesHistoryPxLastUpdateDate.isBefore(salesHistoryDsLastUpdateDate)

    api.logInfo("[CFS_YearlySalesHistory][UpdateNeeded]", api.global.updateNeeded)
}

return api.global.updateNeeded

