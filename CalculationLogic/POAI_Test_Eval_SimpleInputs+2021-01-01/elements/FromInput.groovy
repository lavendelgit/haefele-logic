def controller = api.newController()

controller.addHTML("""
<h1>Input from this tab</h1>
<ul>
<li>string entry: '${api.input("StringEntry")}'</li>
<li>option entry: '${api.input("OptionEntry")}'</li> 
</ul>
""")

return controller
