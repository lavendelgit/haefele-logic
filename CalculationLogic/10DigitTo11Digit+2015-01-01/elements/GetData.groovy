api.global.grundpreis = api.product("Grundpreis_Neu")
api.global.comm = api.product("Comm_Spec")
api.global.haupt = api.product("Haupt_Neben")
api.global.computed = api.product("Grundpreis_Computed")

def sku = api.product("sku")

if (sku.length() == 11 && api.global.computed != "Haefele") {
  if (!api.global.grundpreis) {
    api.global.grundpreis = api.product("Grundpreis_Neu", sku.substring(0, 10))
    if (api.global.grundpreis) {
    	api.global.computed = "Sub"
    	api.trace("11-digit sku, getting Grundpreis_Neu from | value", sku.substring(0, 10), api.global.grundpreis)
    }
  }
  if (!api.global.comm) {
    api.global.comm = api.product("Comm_Spec", sku.substring(0, 10))
    api.trace("11-digit sku, getting Comm_Spec from | value", sku.substring(0, 10), api.global.comm)
  }
  if (!api.global.haupt) {
    api.global.haupt = api.product("Haupt_Neben", sku.substring(0, 10))
    api.trace("11-digit sku, getting Haupt_Neben from | value", sku.substring(0, 10), api.global.haupt)
  }
}