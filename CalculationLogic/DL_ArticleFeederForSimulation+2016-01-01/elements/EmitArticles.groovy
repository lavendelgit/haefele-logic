import groovy.transform.Field

@Field String FIELD_MATERIAL_STATUS = 'attribute4'
@Field String FIELD_GENERATE_LPG = 'attribute30'
@Field String FIELD_SKU = 'sku'
@Field String PRODUCT_TYPE_CODE = 'P'
@Field String FIELD_ID = 'id'

List articlesFilter = generateArticlesFilter()
List articleIdsList = generateArticleIdsList(articlesFilter)
emitArticles(articleIdsList)

return

protected List generateArticlesFilter() {
    List articleFilters = [
            //Filter.or(Filter.equal(FIELD_MATERIAL_STATUS, 'ZR'), Filter.equal(FIELD_MATERIAL_STATUS, ' '), Filter.isNull(FIELD_MATERIAL_STATUS)),
            Filter.equal(FIELD_GENERATE_LPG, "Inland 2020")
            //Filter.in(FIELD_SKU, ["000.33.431", "000.57.001", "801.41.026", "008.52.101", "209.99.160", "001.35.001", "002.14.190"])
    ]
    def productFilter = out.ProductFilter
    if (productFilter) {
        articleFilters.add(productFilter)
        api.logInfo("############ generateArticlesFilter : productFilter", productFilter)
    } /*else {
        //api.logInfo("############ generateArticlesFilter", "######productFilter is null")
        //articleFilters.add(Filter.in(FIELD_SKU, ["000.33.431", "000.57.001", "801.41.026", "008.52.101", "209.99.160", "001.35.001", "002.14.190"]))
      	articleFilters.add(Filter.equal(FIELD_SKU, "987.29.000"))
    }*/

    return articleFilters
}

protected List generateArticleIdsList(List articlesFilter) {
    List materialIdsList = null
    def productStream = null
    try {
        productStream = api.stream(PRODUCT_TYPE_CODE, FIELD_ID, [FIELD_ID], *articlesFilter)
        materialIdsList = productStream?.collect { product ->
            product?.get(FIELD_ID)
        }
    } catch (ex) {
        api.logWarn("Exception in generateArticleIdsList", ex.getMessage())
    } finally {
        productStream?.close()
    }

    return materialIdsList
}

protected void emitArticles(List articleIdsList) {
    api.trace("############# articleIdsList ", articleIdsList)
    articleIdsList?.each { Long productId ->
        //api.logInfo("############# Emitting productId ", productId)
        api.emitPersistedObject(PRODUCT_TYPE_CODE, productId)
    }
}
/*001.33.210
000.57.050
000.30.210
001.22.485
000.57.000
*/
//Filter.in(FIELD_SKU, ["000.33.431", "000.30.210","000.57.001","801.41.026", "008.52.101", "209.99.160", "001.35.001", "002.14.190"])
//Filter.in(FIELD_SKU, ["000.33.431","000.30.210"])