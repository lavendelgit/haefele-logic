package CFF_StoreMontlyZPAPsCount.elements

def zpapCustomerIds = api.getElement("ZPAPsPerCustomer").keySet()
def customersIds = api.getElement("Customers")*.customerId.toSet()

zpapCustomerIds.findAll { id -> !customersIds.contains(id) }.toSorted()

