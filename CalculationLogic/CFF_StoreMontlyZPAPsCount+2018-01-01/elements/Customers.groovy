package CFF_StoreMontlyZPAPsCount.elements

def result = []

def stream = api.stream("C", null, [
        "customerId",
        lib.Attrs.customer("Sales Office"),
])

if (!stream) {
    return []
}

while (stream.hasNext()) {
    result.add(stream.next())
}

stream.close()

return result