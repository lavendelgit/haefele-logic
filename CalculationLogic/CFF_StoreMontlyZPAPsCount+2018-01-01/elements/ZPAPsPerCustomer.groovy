package CFF_StoreMontlyZPAPsCount.elements

def zpaps = api.getElement("ZPAPs")

return zpaps.countBy { zpap -> zpap.attribute1.padLeft(10,"0") }.sort()