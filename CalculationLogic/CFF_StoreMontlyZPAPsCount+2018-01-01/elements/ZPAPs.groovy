package CFF_StoreMontlyZPAPsCount.elements

def targetDate = api.getElement("TargetMonthFormatted")

lib.Find.customerSalesPrices(targetDate)

/*
def result = []

def stream = api.stream("PX8", "sku", // sort-by
        ["sku","attribute1"],
        Filter.equal("name", "OldCustomerSalesPrice"),
        Filter.equal("attribute5", "ZPAPs 02.05.2018.xlsx"),
        Filter.lessOrEqual("attribute2", targetDate),    // attribute2 = valid from
        Filter.greaterOrEqual("attribute3", targetDate)) // attribute3 = valid to

if (!stream) {
    return []
}

while (stream.hasNext()) {
    result.add(stream.next())
}

stream.close()

return result
*/