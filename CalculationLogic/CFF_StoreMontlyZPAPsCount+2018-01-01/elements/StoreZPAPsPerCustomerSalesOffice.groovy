package CFF_StoreMontlyZPAPsCount.elements

def zpapsPerCustomerSalesOffice = api.getElement("ZPAPsPerCustomerSalesOffice")
def pp = api.findLookupTable("MonthlyZPAPsPerSalesOffice")

if (!pp) {
    api.addWarning("Pricing parameter not found (MonthlyZPAPsPerSalesOffice)")
    return
}

def targetDate = api.getElement("TargetMonthFormatted")

def total = 0

zpapsPerCustomerSalesOffice.each { salesOffice, count ->
    def record = [
            "lookupTableId": pp.id,
            "lookupTableName": pp.uniqueName,
            "key1": salesOffice,
            "key2": targetDate,
            "attribute1": count,
    ]
    api.logInfo("Storing ZPAPs count ${count} for sales office", salesOffice)
    api.addOrUpdate("MLTV2", record)
    total += count
}

api.trace("total count", "", total)