package CFS_Elasticity.elements

def sku = api.getElement("SKU")
def elasticity = api.getElement("Elasticity")

api.addOrUpdate("PX3", [
        "name": "Elasticity",
        "sku": sku,
        "attribute1": elasticity
])