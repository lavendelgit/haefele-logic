package CFS_Elasticity.elements

def force = api.getElement("Force")

if (force) {
    return true;
}

def elasticityLastUpdateDate() {
    def lastUpdateDateStr = api.find("PX3", 0, 1, "-lastUpdateDate", Filter.equal("name", "Elasticity")).find()?.lastUpdateDate
    return api.parseDateTime("yyyy-MM-dd'T'HH:mm:ss", lastUpdateDateStr)
}

def salesHistoryLastUpdateDate() {
    def dmCtx = api.getDatamartContext()
    def salesDS = dmCtx.getDataSource("SalesHistory2")
    def query = dmCtx.newQuery(salesDS, false)

    query.select("lastUpdateDate")
    query.orderBy("lastUpdateDate DESC")
    query.setMaxRows(1)

    return dmCtx.executeQuery(query)?.getData()?.collect()?.find()?.lastUpdateDate
}

if (api.global.updateNeeded == null) {
    def elasticityLastUpdateDate = elasticityLastUpdateDate()
    def salesHistoryLastUpdateDate = salesHistoryLastUpdateDate()


    api.trace("elasticityLastUpdateDate", "", elasticityLastUpdateDate)
    api.trace("salesHistoryLastUpdateDate", "", salesHistoryLastUpdateDate)

    api.global.updateNeeded = elasticityLastUpdateDate == null || elasticityLastUpdateDate.isBefore(salesHistoryLastUpdateDate)

    api.logInfo("[CFS_Elasticity][UpdateNeeded]", api.global.updateNeeded)
}

return api.global.updateNeeded

