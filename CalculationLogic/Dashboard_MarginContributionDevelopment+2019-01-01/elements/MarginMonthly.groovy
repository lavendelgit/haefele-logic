def marginMonthly = api.findLookupTableValues("MarginMonthly")
        .collect { r -> [
                year: r.key1,
                month: r.key2,
                marginPerc: r.attribute1,
                marginAbs: r.attribute2,
                revenue: r.attribute3
        ]}
        .groupBy { it.year.toString() }
        .collectEntries { year, margins -> [year, margins.collectEntries{ [it.month, it] }] }

return marginMonthly