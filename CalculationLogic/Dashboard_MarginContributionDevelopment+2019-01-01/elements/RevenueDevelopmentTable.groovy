import net.pricefx.common.api.FieldFormatType

def marginMonthly = api.getElement("MarginMonthly")
def plannedMargin = api.getElement("PlannedMargin")
def months = api.getElement("Months")

def years = marginMonthly.keySet().toList().sort()
def currentYear = years.last()
def firstYear = years.first()
def pastYears = years.take(years.size() - 1)
def lastYear = pastYears.last()

def currentYearCalcRevenueHeader = currentYear + " Hochgerechnet"
def resultMatrix = api.newMatrix(["Monat", *pastYears, "CAGR", currentYearCalcRevenueHeader,
                                  "Ist", "Ist vs Soll (%)", "Trend Ist vs Soll",
                                  "Plan", "Ist vs Plan (%)", "Trend Ist vs Plan"])
resultMatrix.setPreferenceName("DBUE")
pastYears.forEach { year -> resultMatrix.setColumnFormat(year, FieldFormatType.MONEY_EUR )}
resultMatrix.setColumnFormat("CAGR", FieldFormatType.PERCENT )
resultMatrix.setColumnFormat(currentYearCalcRevenueHeader, FieldFormatType.MONEY_EUR)
resultMatrix.setColumnFormat("Ist", FieldFormatType.MONEY_EUR)
resultMatrix.setColumnFormat("Ist vs Soll (%)", FieldFormatType.PERCENT)
resultMatrix.setColumnFormat("Plan", FieldFormatType.MONEY_EUR)
resultMatrix.setColumnFormat("Ist vs Plan (%)", FieldFormatType.PERCENT)

for (month in months) {
    row = [
        "Monat": month.value,
    ]
    years.forEach { year -> row[year] = marginMonthly.get(year)?.get(month.key)?.revenue}
    def firstYearRevenue = marginMonthly.get(firstYear)?.get(month.key)?.revenue
    def lastYearRevenue = marginMonthly.get(lastYear)?.get(month.key)?.revenue
    def currentYearRevenue = (marginMonthly.get(currentYear)?.get(month.key)?.revenue)?:0.0
    if (firstYearRevenue != null & firstYearRevenue != 0 && lastYearRevenue != null) {
        def cagr = Math.pow(lastYearRevenue/firstYearRevenue, 1/(pastYears.size() - 1)) - 1
        def currentYearCalculatedRevenue = lastYearRevenue * (1 + cagr)
        def plannedRevenue = (plannedMargin?.get(month.key)?.revenue)?:0.0
        row["CAGR"] = cagr
        row[currentYear + " Hochgerechnet"] = currentYearCalculatedRevenue
        row["Plan"] = plannedRevenue
        if (currentYearRevenue) {
            row["Ist"] = currentYearRevenue
            row["Ist vs Soll (%)"] = (currentYearRevenue - currentYearCalculatedRevenue) / currentYearCalculatedRevenue
            row["Trend Ist vs Soll"] = Util.trendIcon(resultMatrix, currentYearRevenue, currentYearCalculatedRevenue)
            row["Ist vs Plan (%)"] = (currentYearRevenue - plannedRevenue) / ((plannedRevenue != 0.0)?plannedRevenue:1)
            row["Trend Ist vs Plan"] = Util.trendIcon(resultMatrix, currentYearRevenue, plannedRevenue)
        }
    }

    resultMatrix.addRow(row)
}

return resultMatrix