def trendIcon(resultMatrix, currentValue, expectedValue) {
    def server = api.vLookup("SurchargeConfig","Server")
    if (currentValue > expectedValue) {
        return resultMatrix.imageCell("https://$server/classic/images/library/Arrow_green.png")
    } else if (currentValue == expectedValue) {
        return resultMatrix.imageCell("https://$server/classic/images/library/Arrow_yellow.png")
    } else {
        return resultMatrix.imageCell("https://$server/classic/images/library/Arrow_red.png")
    }
}
