import net.pricefx.common.api.FieldFormatType

def marginMonthly = api.getElement("MarginMonthly")
def plannedMargin = api.getElement("PlannedMargin")
def months = api.getElement("Months")

def years = marginMonthly.keySet().toList().sort()
def currentYear = years.last()
def firstYear = years.first()
def pastYears = years.take(years.size() - 1)
def lastYear = pastYears.last()

def currentYearCalcMarginHeader = currentYear + " Hochgerechnet"
def resultMatrix = api.newMatrix(["Monat", *pastYears, "CAGR", currentYearCalcMarginHeader,
                                  "Ist", "Ist vs Soll (%)", "Trend Ist vs Soll",
                                  "Plan", "Ist vs Plan (%)", "Trend Ist vs Plan"])
resultMatrix.setPreferenceName("DBEPound")
pastYears.forEach { year -> resultMatrix.setColumnFormat(year, FieldFormatType.MONEY_EUR )}
resultMatrix.setColumnFormat("CAGR", FieldFormatType.PERCENT )
resultMatrix.setColumnFormat(currentYearCalcMarginHeader, FieldFormatType.MONEY_EUR)
resultMatrix.setColumnFormat("Ist", FieldFormatType.MONEY_EUR)
resultMatrix.setColumnFormat("Ist vs Soll (%)", FieldFormatType.PERCENT)
resultMatrix.setColumnFormat("Plan", FieldFormatType.MONEY_EUR)
resultMatrix.setColumnFormat("Ist vs Plan (%)", FieldFormatType.PERCENT)

for (month in months) {
    row = [
        "Monat": month.value,
    ]
    years.forEach { year -> row[year] = marginMonthly.get(year)?.get(month.key)?.marginAbs}
    def firstYearMargin = marginMonthly.get(firstYear)?.get(month.key)?.marginAbs
    def lastYearMargin = marginMonthly.get(lastYear)?.get(month.key)?.marginAbs
    def currentYearMargin = (marginMonthly.get(currentYear)?.get(month.key)?.marginAbs)?:0.0
    if (firstYearMargin != null & firstYearMargin != 0 && lastYearMargin != null) {
        def cagr = Math.pow(lastYearMargin/firstYearMargin, 1/(pastYears.size() - 1)) - 1
        def currentYearCalculatedMargin = lastYearMargin * (1 + cagr)
        def plannedMarginAbs = (plannedMargin?.get(month.key)?.marginAbs)?:0.0
        row["CAGR"] = cagr
        row[currentYear + " Hochgerechnet"] = currentYearCalculatedMargin
        row["Plan"] = plannedMarginAbs
        if (currentYearMargin) {
            row["Ist"] =  currentYearMargin
            row["Ist vs Soll (%)"] =  (currentYearMargin - currentYearCalculatedMargin) / currentYearCalculatedMargin
            row["Trend Ist vs Soll"] = Util.trendIcon(resultMatrix, currentYearMargin, currentYearCalculatedMargin)
            row["Ist vs Plan (%)"] = (currentYearMargin - plannedMarginAbs) / ((plannedMarginAbs != 0.0)?plannedMarginAbs : 1)
            row["Trend Ist vs Plan"] = Util.trendIcon(resultMatrix, currentYearMargin, plannedMarginAbs)
        }
    }

    resultMatrix.addRow(row)
}

return resultMatrix