def currentYear = api.calendar().get(Calendar.YEAR)
def plannedMargin = api.findLookupTableValues("PlannedMargin")
        .collect { r -> [
                year: r.key1,
                month: r.key2,
                marginAbs: r.attribute1,
                marginPerc: r.attribute1 / r.attribute2,
                revenue: r.attribute2
        ]}
        .findAll { r -> r.year == currentYear}
        .collectEntries { r -> [r.month, r] }

return plannedMargin