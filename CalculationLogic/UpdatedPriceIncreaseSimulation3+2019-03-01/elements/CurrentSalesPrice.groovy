if (api.isSyntaxCheck()) return

if (api.product("sku").endsWith("X")) {
    api.redAlert("X Article. Ignoring average price since it may be inaccurate.")
}

return api.getElement("PriceIncreaseImpact").currentSalesPrice