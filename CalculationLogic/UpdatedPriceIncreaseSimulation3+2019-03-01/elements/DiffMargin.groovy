def margin1 = api.getElement("Margin1") as BigDecimal
def margin2 = api.getElement("Margin2") as BigDecimal

if (margin1 == null) {
    api.redAlert("Margin1")
    return
}

if (margin2 == null) {
    api.redAlert("Margin2")
    return
}

return margin2 - margin1