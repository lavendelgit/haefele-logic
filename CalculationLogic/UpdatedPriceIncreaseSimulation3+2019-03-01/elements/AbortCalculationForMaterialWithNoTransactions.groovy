def countOfTransactions = api.getElement("TransactionsSummary")?.count
 
if (countOfTransactions ==null || countOfTransactions==0) {
    api.trace("Aborting the calculation as the material does not have corresponding Transactions")
    api.redAlert("Aborting the calculation as the material ("+ api.product("sku") +") does not have corresponding Transactions")
	api.abortCalculation()
}