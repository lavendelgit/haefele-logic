def sku = api.product("sku")
def dmCtx = api.getDatamartContext()
def table = dmCtx.getTable("TransactionSummaryPerArticleDM")
def query = dmCtx.newQuery(table)

def currentYear = api.calendar().get(Calendar.YEAR);
def previousYear = currentYear - 1

def filters = [
    Filter.equal("Material", sku),
    //Filter.greaterThan("Units", 0),
    //Filter.greaterThan("Revenue", 0),
    Filter.equal("InvoiceYear", previousYear)
]

query.select("COUNT(1)", "count")
query.select("p1_SalesPricePer100Units", "avgPrice")
query.select("p5_SalesPricePer100Units", "minPrice")
query.select("p6_SalesPricePer100Units", "maxPrice")
query.select("p4_Revenue", "revenue")
query.select("p2_Units", "quantity")
query.select("p3_MarginAbs", "marginAbs")
query.where(*filters)

dmCtx.executeQuery(query)?.getData()?.collect()?.find()
