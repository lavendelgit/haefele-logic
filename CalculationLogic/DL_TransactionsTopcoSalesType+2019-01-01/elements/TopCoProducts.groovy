api.retainGlobal = true

if (!api.global.topco) {
    api.global.topco = api.findLookupTableValues("TopCoProducts")*.name as Set
}

api.global.topco