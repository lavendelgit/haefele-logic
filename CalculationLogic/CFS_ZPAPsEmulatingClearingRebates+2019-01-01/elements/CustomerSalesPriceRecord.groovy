def clearingRebate = api.getElement("CurrentItem")
def first = api.find("PX50", 0, 50, "attribute1", ["sku", "attribute4", "attribute1", "attribute2", "attribute3"],
                     Filter.equal("name", "S_ZPAP"),
                     Filter.equal("sku", clearingRebate?.sku),
                     Filter.equal("attribute4", clearingRebate?.attribute1),  // Customer ID
                     Filter.greaterOrEqual("attribute1", clearingRebate?.attribute2), // ZPAP Valid From >= Clearing Rebate Valid From
                     )?.find() ?: [:]