def currentItem = api.currentItem()

if (!currentItem) { // testdef currentItem = api.currentItem()

    currentItem = api.find("PX", 0, 1, "-attribute2",
                           Filter.equal("name", "ClearingRebate"),
                           Filter.equal("sku", api.product("sku")),
                           // Filter.equal("attribute1", "0001173860")

                           )?.find()
}

return currentItem ?: [:]