def tab1 = model.inputs('onetab', 'tab1')
def stringEntry = tab1.StringEntry
def optionEntry = tab1.OptionEntry

if (optionEntry == null) {
    api.throwException("Cannot read state from input set in step 'One working tab' and 'Simple' tab. Please fill the required option entry there and save it.")
}

def controller = api.newController()

controller.addHTML("""
<h1>Input from 'One working tab' step, 'Simple' tab</h1>
<ul>
<li>string entry: '${stringEntry}'</li>
<li>option entry: '${optionEntry}'</li>
</ul>
""")

return controller
