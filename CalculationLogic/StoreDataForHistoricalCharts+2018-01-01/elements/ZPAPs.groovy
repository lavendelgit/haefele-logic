def result = []

def stream = api.stream("PX8", "sku" /* sort-by*/,
        ["sku","attribute1"],
        Filter.equal("name", "CustomerSalesPrice"))

if (!stream) {
    return []
}

while (stream.hasNext()) {
    result.add(stream.next())
}

stream.close()

return result