def zpapsPerCustomerClass = api.getElement("ZPAPsPerCustomerClass")
def pp = api.findLookupTable("CustomerSalesPricesPerCustomerClassHistory")

if (!pp) {
    api.addWarning("Pricing parameter not found (CustomerSalesPricesPerCustomerClassHistory)")
    return
}

def targetDate = api.targetDate()?.format("yyyy-MM-dd");

zpapsPerCustomerClass.each { customerClass, count ->
    def record = [
            "lookupTableId": pp.id,
            "lookupTableName": pp.uniqueName,
            "key1": customerClass,
            "key2": targetDate,
            "attribute1": count,
    ]
    api.logInfo("Storing ZPAPs count ${count} for customer class", customerClass)
    api.addOrUpdate("MLTV2", record)
}