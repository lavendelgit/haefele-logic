def clearingRebatesPerCustomerSalesOffice = api.getElement("ClearingRebatesPerCustomerSalesOffice")
def pp = api.findLookupTable("ClearingRebatesPerSalesOfficeHistory")

if (!pp) {
    api.addWarning("Pricing parameter not found (ClearingRebatesPerSalesOfficeHistory)")
    return
}

def targetDate = api.targetDate()?.format("yyyy-MM-dd");

clearingRebatesPerCustomerSalesOffice.each { salesOffice, count ->
    def record = [
            "lookupTableId": pp.id,
            "lookupTableName": pp.uniqueName,
            "key1": salesOffice,
            "key2": targetDate,
            "attribute1": count,
    ]
    api.logInfo("Storing Clearing Rebates count ${count} for sales office", salesOffice)
    api.addOrUpdate("MLTV2", record)
}