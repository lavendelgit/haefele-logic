def clearingRebates = api.getElement("ClearingRebates")

return clearingRebates.countBy { clearingRebate -> clearingRebate.attribute1 }.sort()