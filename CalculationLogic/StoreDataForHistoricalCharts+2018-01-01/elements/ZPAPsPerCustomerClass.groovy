def customerClasses = api.getElement("Customers")
def zpapsPerCustomer = api.getElement("ZPAPsPerCustomer")

return customerClasses
        .groupBy { c -> c.attribute5 ?: "Undefined" }  // attribute5 = Customer ABC Class
        .collectEntries { customerClass, customers ->
            [customerClass, customers.collect { c -> zpapsPerCustomer[c.customerId] ?: 0 }.sum()]
        }
        .findAll { e -> e.value > 0}
        .sort { e1, e2 -> e1.key <=> e2.key }