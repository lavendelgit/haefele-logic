def clearingRebatesPerCustomerClass = api.getElement("ClearingRebatesPerCustomerClass")
def pp = api.findLookupTable("ClearingRebatesPerCustomerClassHistory")

if (!pp) {
    api.addWarning("Pricing parameter not found (ClearingRebatesPerCustomerClassHistory)")
    return
}

def targetDate = api.targetDate()?.format("yyyy-MM-dd");

clearingRebatesPerCustomerClass.each { customerClass, count ->
    def record = [
            "lookupTableId": pp.id,
            "lookupTableName": pp.uniqueName,
            "key1": customerClass,
            "key2": targetDate,
            "attribute1": count,
    ]
    api.logInfo("Storing ClearingRebates count ${count} for customer class", customerClass)
    api.addOrUpdate("MLTV2", record)
}