def zpaps = api.getElement("ZPAPs")

return zpaps.countBy { zpap -> zpap.attribute1 }.sort()