def clearingRebatesPerCustomerDiscountGroup = api.getElement("ZPAPsPerCustomerDiscountGroup")
def pp = api.findLookupTable("ClearingRebatesPerCustomerDiscountGroupHistory")

if (!pp) {
    api.addWarning("Pricing parameter not found (ClearingRebatesPerCustomerClassHistory)")
    return
}

def targetDate = api.targetDate()?.format("yyyy-MM-dd");

clearingRebatesPerCustomerDiscountGroup.each { discountGroup, count ->
    def record = [
            "lookupTableId": pp.id,
            "lookupTableName": pp.uniqueName,
            "key1": discountGroup,
            "key2": targetDate,
            "attribute1": count,
    ]
    api.logInfo("Storing Clearing Rebates count ${count} for customer class", discountGroup)
    api.addOrUpdate("MLTV2", record)
}