def zpapsPerCustomerDiscountGroup = api.getElement("ZPAPsPerCustomerDiscountGroup")
def pp = api.findLookupTable("CustomerSalesPricesPerCustomerDiscountGroupHistory")

if (!pp) {
    api.addWarning("Pricing parameter not found (CustomerSalesPricesPerCustomerClassHistory)")
    return
}

def targetDate = api.targetDate()?.format("yyyy-MM-dd");

zpapsPerCustomerDiscountGroup.each { discountGroup, count ->
    def record = [
            "lookupTableId": pp.id,
            "lookupTableName": pp.uniqueName,
            "key1": discountGroup,
            "key2": targetDate,
            "attribute1": count,
    ]
    api.logInfo("Storing ZPAPs count ${count} for customer class", discountGroup)
    api.addOrUpdate("MLTV2", record)
}