def customerSalesOffices = api.getElement("Customers")
def zpapsPerCustomer = api.getElement("ZPAPsPerCustomer")

return customerSalesOffices
        .groupBy { c -> c.attribute3 ?: "Undefined" } // attribute3 = sales office
        .collectEntries { salesOffice, customers ->
            [salesOffice, customers.collect { c -> zpapsPerCustomer[c.customerId] ?: 0 }.sum()]
        }
        .sort { e1, e2 -> e1.key <=> e2.key }