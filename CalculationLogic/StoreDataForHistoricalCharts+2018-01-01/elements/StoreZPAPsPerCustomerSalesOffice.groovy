def zpapsPerCustomerSalesOffice = api.getElement("ZPAPsPerCustomerSalesOffice")
def pp = api.findLookupTable("CustomerSalesPricesPerSalesOfficeHistory")

if (!pp) {
    api.addWarning("Pricing parameter not found (CustomerSalesPricesPerSalesOfficeHistory)")
    return
}

def targetDate = api.targetDate()?.format("yyyy-MM-dd");

zpapsPerCustomerSalesOffice.each { salesOffice, count ->
    def record = [
            "lookupTableId": pp.id,
            "lookupTableName": pp.uniqueName,
            "key1": salesOffice,
            "key2": targetDate,
            "attribute1": count,
    ]
    api.logInfo("Storing ZPAPs count ${count} for sales office", salesOffice)
    api.addOrUpdate("MLTV2", record)
}