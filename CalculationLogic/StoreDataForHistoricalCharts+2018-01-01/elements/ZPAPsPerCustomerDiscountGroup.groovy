def customerDiscountGroups = api.getElement("Customers")
def zpapsPerCustomer = api.getElement("ZPAPsPerCustomer")

return customerDiscountGroups
        .groupBy { c -> c.attribute6 ?: "Undefined" } // attribute6 = customer discount group
        .collectEntries { discountGroup, customers ->
            [discountGroup, customers.collect { c -> zpapsPerCustomer[c.customerId] ?: 0 }.sum()]
        }
        .findAll { e -> e.value > 0}
        .sort { e1, e2 -> e1.key <=> e2.key }