def salesPriceZRC = api.getElement("SalesPrice_ZRC")
def discountOrder = lib.GeneralSettings.percentageValue("Discount_Order_Pct")
def minMarginPrice = api.getElement("MinMarginPrice");

if (salesPriceZRC == null) {
    api.redAlert("SalesPrice_ZRC")
    return null
}

if (discountOrder == null) {
    api.redAlert("Discount_Order_Pct")
    return null
}

if (minMarginPrice == null) {
    api.redAlert("MinMarginPrice")
}

def salesPriceOrder = lib.Rounding.roundMoney(salesPriceZRC * (1 - discountOrder))

if (salesPriceOrder < minMarginPrice) {
    api.local.discountWarning = api.local.discountWarning ?: "SalesPrice_Order"
    api.redAlert("SalesPrice_Order is below MinMarginPrice")
}

return salesPriceOrder