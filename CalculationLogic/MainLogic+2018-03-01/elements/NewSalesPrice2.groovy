def newBaseCost = api.getElement("NewBaseCost")
def oldSurchargePerc = api.getElement("OldSurchargePerc")

if (newBaseCost == null) {
    api.redAlert("NewBaseCost")
    return null
}

if (oldSurchargePerc == null) {
    api.redAlert("OldSurchargePerc")
    return null
}

def oldBaseCostValidFrom = api.getElement("OldBaseCostValidFrom")
def newBaseCostValidFrom = api.getElement("NewBaseCostValidFrom")

if (oldBaseCostValidFrom == newBaseCostValidFrom) {
    api.redAlert("OldBaseCostValidFrom is the same as NewBaseCostValidFrom")
    return null;
}

return lib.Rounding.roundMoney(newBaseCost * (oldSurchargePerc + 1))