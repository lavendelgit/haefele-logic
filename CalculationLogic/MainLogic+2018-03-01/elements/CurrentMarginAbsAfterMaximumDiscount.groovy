def currentSalesPrice = api.getElement("CurrentSalesPrice")
def currentBaseCost = api.getElement("CurrentBaseCost")
def maxDiscountPerc = api.getElement("MaxDiscountPerc");

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (currentBaseCost == null) {
    api.redAlert("CurrentBaseCost")
    return null
}

if (maxDiscountPerc == null) {
    api.redAlert("MaxDiscountPerc")
    return null
}

return currentSalesPrice * (1 - maxDiscountPerc) - currentBaseCost;
