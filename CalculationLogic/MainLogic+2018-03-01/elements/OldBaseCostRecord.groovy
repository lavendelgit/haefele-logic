def salesPriceIncreaseDate = api.getElement("SalesPriceIncreaseDate");

if (!salesPriceIncreaseDate) {
    api.redAlert("SalesPriceIncreaseDate")
    return null
}

def oldBaseCostRecord = lib.Find.latestBaseCostRecord(api.product("sku"), Filter.lessThan("attribute1", salesPriceIncreaseDate))

if (oldBaseCostRecord?.isEmpty() && api.getElement("GetOldPricesAfterSalesPriceIncreaseDate")) {
    oldBaseCostRecord = lib.Find.earliestBaseCostRecord(api.product("sku"), Filter.greaterOrEqual("attribute1", salesPriceIncreaseDate))
}

return oldBaseCostRecord