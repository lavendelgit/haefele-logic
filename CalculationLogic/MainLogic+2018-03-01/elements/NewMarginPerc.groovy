def newMarginAbs = api.getElement("NewMarginAbs")
def salesPrice = api.getElement("CurrentSalesPrice")

if (newMarginAbs == null) {
    api.redAlert("NewMarginAbs")
    return null
}

if (!salesPrice) {
    api.redAlert("CurrentSalesPrice")
    return null
}

return newMarginAbs / salesPrice;
