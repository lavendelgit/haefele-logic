def marginAbs2 = api.getElement("MarginAbs2")
def newSalesPrice2 = api.getElement("NewSalesPrice2")

if (marginAbs2 == null) {
    api.redAlert("MarginAbs2")
    return null
}

if (!newSalesPrice2) {
    api.redAlert("NewSalesPrice2")
    return null
}

return marginAbs2 / newSalesPrice2;
