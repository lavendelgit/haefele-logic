def latestCompetitorPrices = api.getElement("LatestCompetitorPrices")

if (latestCompetitorPrices.size() > 0) {
    return latestCompetitorPrices.sum { p -> p.price} / latestCompetitorPrices.size()
} else {
    api.redAlert("No Competition data");
    return null;
}