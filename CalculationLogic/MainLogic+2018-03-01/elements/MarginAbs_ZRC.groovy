def salesPriceZRC = api.getElement("SalesPrice_ZRC")
def baseCost = api.getElement("CurrentBaseCost")

if (salesPriceZRC == null) {
    api.redAlert("SalesPrice_ZRC")
    return null
}

if (baseCost == null) {
    api.redAlert("CurrentBaseCost")
    return null
}

return salesPriceZRC - baseCost