def oldMarginAbs = api.getElement("OldMarginAbs")
def oldSalesPrice = api.getElement("OldSalesPrice")

if (oldMarginAbs == null) {
    api.redAlert("OldMarginAbs")
    return null
}

if (oldSalesPrice == null || oldSalesPrice == 0) {
    api.redAlert("OldSalesPrice")
    return null
}

return oldMarginAbs / oldSalesPrice;
