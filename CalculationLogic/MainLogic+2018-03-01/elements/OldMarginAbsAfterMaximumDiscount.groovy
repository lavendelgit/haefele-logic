def oldSalesPrice = api.getElement("OldSalesPrice")
def oldBaseCost = api.getElement("OldBaseCost")
def maxDiscountPerc = api.getElement("MaxDiscountPerc");

if (oldSalesPrice == null) {
    api.redAlert("OldSalesPrice")
    return null
}

if (oldBaseCost == null) {
    api.redAlert("OldBaseCost")
    return null
}

if (maxDiscountPerc == null) {
    api.redAlert("MaxDiscountPerc")
    return null
}

return oldSalesPrice * (1 - maxDiscountPerc) - oldBaseCost;
