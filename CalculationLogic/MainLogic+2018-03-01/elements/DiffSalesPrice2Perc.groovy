def currentSalesPrice = api.getElement("CurrentSalesPrice")
def diffSalesPrice2Abs = api.getElement("DiffSalesPrice2Abs")

if (!currentSalesPrice) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (diffSalesPrice2Abs == null) {
    api.redAlert("DiffSalesPrice2Abs")
    return null
}

return diffSalesPrice2Abs / currentSalesPrice