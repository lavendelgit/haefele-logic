def salesPrice = api.getElement("CurrentSalesPriceRecord")?.salesPrice

if (salesPrice == null) {
    api.redAlert("Current Sales Price is missing.")
}

return salesPrice