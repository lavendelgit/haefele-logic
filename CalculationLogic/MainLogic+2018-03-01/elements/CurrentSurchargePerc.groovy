def currentSalesPrice = api.getElement("CurrentSalesPrice")
def currentBaseCost = api.getElement("CurrentBaseCost")

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (!currentBaseCost) {
    api.redAlert("CurrentBaseCost")
    return null
}

return currentSalesPrice / currentBaseCost - 1
