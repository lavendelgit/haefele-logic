def marginAbs1 = api.getElement("MarginAbs1")
def newSalesPrice1 = api.getElement("NewSalesPrice1")

if (marginAbs1 == null) {
    api.redAlert("MarginAbs1")
    return null
}

if (!newSalesPrice1) {
    api.redAlert("NewSalesPrice1")
    return null
}

return marginAbs1 / newSalesPrice1;
