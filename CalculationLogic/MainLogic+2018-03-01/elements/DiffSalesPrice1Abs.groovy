def currentSalesPrice = api.getElement("CurrentSalesPrice")
def newSalesPrice1 = api.getElement("NewSalesPrice1")

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (newSalesPrice1 == null) {
    api.redAlert("NewSalesPrice1")
    return null
}

return newSalesPrice1 - currentSalesPrice