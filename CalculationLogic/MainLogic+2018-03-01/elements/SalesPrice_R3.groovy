def salesPrice = api.getElement("CurrentSalesPrice")
def discountR3 = lib.GeneralSettings.percentageValue("Discount_R3_Pct")
def minMarginPrice = api.getElement("MinMarginPrice");

if (salesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (discountR3 == null) {
    api.redAlert("Discount_R3_Pct")
    return null
}

if (minMarginPrice == null) {
    api.redAlert("MinMarginPrice")
}

def salesPriceR3 = lib.Rounding.roundMoney(salesPrice * (1 - discountR3))

if (salesPriceR3 < minMarginPrice) {
    api.local.discountWarning = api.local.discountWarning ?: "SalesPrice_R3"
    api.redAlert("SalesPrice_R3 is below MinMarginPrice")
}

return salesPriceR3