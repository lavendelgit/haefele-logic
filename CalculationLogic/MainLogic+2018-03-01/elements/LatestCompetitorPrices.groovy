api.getElement("CompetitorPrices")
        .groupBy { cp -> cp.competitor }
        .collect { competitor, prices -> prices.max { p -> p.validFrom} }
