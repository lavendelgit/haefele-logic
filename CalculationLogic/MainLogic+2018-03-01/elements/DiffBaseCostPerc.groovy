def oldBaseCost = api.getElement("OldBaseCost")
def currentBaseCost = api.getElement("CurrentBaseCost")

if (!oldBaseCost) {
    api.redAlert("Old Base Cost is missing.")
    return
}

if (currentBaseCost == null) {
    api.redAlert("New Base Cost is missing.")
    return
}

return (currentBaseCost - oldBaseCost) / oldBaseCost