def currentBaseCostRecord = api.getElement("CurrentBaseCostRecord");

def currentBaseCost =  null

if (currentBaseCostRecord) {
    currentBaseCost = currentBaseCostRecord.attribute3
}

if (!currentBaseCost) {
    api.redAlert("Current Base Cost is missing in PX BaseCost")
}

return currentBaseCost