def currentSalesPrice = api.getElement("CurrentSalesPrice")
def newBaseCost = api.getElement("NewBaseCost")

if (currentSalesPrice == null) {
    api.redAlert("CurrentSalesPrice")
    return null
}

if (!newBaseCost) {
    api.redAlert("NewBaseCost")
    return null
}

return currentSalesPrice / newBaseCost - 1;
