def salesPriceR3 = api.getElement("SalesPrice_R3")
def marginAbsR3 = api.getElement("MarginAbs_R3")

if (!salesPriceR3) {
    api.redAlert("SalesPrice_R3")
    return null
}

if (marginAbsR3 == null) {
    api.redAlert("MarginAbs_R3")
    return null
}

return marginAbsR3 / salesPriceR3