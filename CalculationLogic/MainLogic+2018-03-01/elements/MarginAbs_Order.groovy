def salesPriceOrder = api.getElement("SalesPrice_Order")
def baseCost = api.getElement("CurrentBaseCost")

if (salesPriceOrder == null) {
    api.redAlert("SalesPrice_Order")
    return null
}

if (baseCost == null) {
    api.redAlert("CurrentBaseCost")
    return null
}

return salesPriceOrder - baseCost