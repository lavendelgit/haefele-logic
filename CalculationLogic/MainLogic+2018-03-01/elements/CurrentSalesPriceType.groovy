def salesPriceType = api.getElement("CurrentSalesPriceRecord")?.type

if (salesPriceType == null) {
    api.redAlert("Sales Price Type is missing.")
}

return salesPriceType