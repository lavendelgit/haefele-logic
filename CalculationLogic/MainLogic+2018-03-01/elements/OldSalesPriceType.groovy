def oldSalesPriceType = api.getElement("OldSalesPriceRecord")?.type

if (oldSalesPriceType == null) {
    api.redAlert("Old Sales Price Type is missing.")
}

return oldSalesPriceType