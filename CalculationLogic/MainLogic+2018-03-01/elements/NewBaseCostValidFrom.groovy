def newBaseCostRecord = api.getElement("NewBaseCostRecord");

def newBaseCostValidFrom = null

if (newBaseCostRecord) {
    newBaseCostValidFrom = newBaseCostRecord.validFrom
}

if (!newBaseCostValidFrom) {
    api.redAlert("Valid From is missing in PX BaseCost")
}

return newBaseCostValidFrom