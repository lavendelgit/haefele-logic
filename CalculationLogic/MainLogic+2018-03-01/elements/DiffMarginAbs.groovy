def currentMarginAbs = api.getElement("CurrentMarginAbs")
def oldMarginAbs = api.getElement("OldMarginAbs")

if (currentMarginAbs == null) {
    api.redAlert("CurrentMarginAbs")
    return null
}

if (oldMarginAbs == null) {
    api.redAlert("OldMarginAbs")
    return null
}

return currentMarginAbs - oldMarginAbs