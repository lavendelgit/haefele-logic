def salesPriceZRC = api.getElement("SalesPrice_ZRC")
def baseCost = api.getElement("CurrentBaseCost")

if (!salesPriceZRC) {
    api.redAlert("SalesPrice_ZRC")
    return null
}

if (!baseCost) {
    api.redAlert("CurrentBaseCost")
    return null
}

return salesPriceZRC / baseCost - 1