def salesPriceIncreaseDate = api.getElement("SalesPriceIncreaseDate");

if (!salesPriceIncreaseDate) {
    api.redAlert("SalesPriceIncreaseDate")
    return null
}

def oldSalesPriceRecord = lib.Find.latestSalesPriceAnyType(api.product("sku"), Filter.lessThan("attribute1", salesPriceIncreaseDate))

if (oldSalesPriceRecord?.isEmpty() && api.getElement("GetOldPricesAfterSalesPriceIncreaseDate")) {
    oldSalesPriceRecord = lib.Find.earliestSalesPriceAnyType(api.product("sku"), Filter.greaterOrEqual("attribute1", salesPriceIncreaseDate))
}

return oldSalesPriceRecord