def elasticityValue = api.getElement("Elasticity")
def perfectlyElasticThreshold = api.getElement("PerfectlyElasticThreshold")

return api.attributedResult(lib.Elasticity.name(elasticityValue, perfectlyElasticThreshold))
        .withBackgroundColor(lib.Elasticity.color(elasticityValue, perfectlyElasticThreshold))