def sku = api.product("sku")
def filters = [
        Filter.equal("name", "Competition"),
        Filter.equal("sku", sku),
]
def orderBy = "attribute4" // Valid From (de: Gültig ab) - ascending
def fields = ["attribute1","attribute3","attribute4"]


def competitorPrices = api.find("PX10", 0, 1000, orderBy, fields, false, *filters)

api.trace("competitorPrices", "", competitorPrices)

if (competitorPrices != null) {
    return competitorPrices.collect { r -> [
            competitor: r.attribute1,
            price: r.attribute3?.toBigDecimal(),
            validFrom: api.parseDate("yyyy-MM-dd", r.attribute4)
    ]}
} else {
    return [];
}