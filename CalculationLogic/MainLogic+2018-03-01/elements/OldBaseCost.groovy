def oldBaseCostRecord = api.getElement("OldBaseCostRecord");

def oldBaseCost =  null

if (oldBaseCostRecord) {
    oldBaseCost = oldBaseCostRecord.attribute3
}

if (!oldBaseCost) {
    api.redAlert("Old Base Cost is missing in PX BaseCost")
}

return oldBaseCost