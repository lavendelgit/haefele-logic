def sku = api.product("sku")
def currentYear = api.calendar().get(Calendar.YEAR);
def numberOfYears = 3;
def fromYear = currentYear - numberOfYears + 1;

def filters = [
        Filter.greaterOrEqual("attribute1", fromYear),
]

def records = lib.Find.yearlySalesHistoryV2(sku, *filters) ?: []

return [
        "y0": records.find{rec -> rec.year == currentYear}     ?: [:],
        "y1": records.find{rec -> rec.year == currentYear - 1} ?: [:],
        "y2": records.find{rec -> rec.year == currentYear - 2} ?: [:]
]

