if (!api.global.minSurcharge) {
    def minMarginPerc = lib.GeneralSettings.percentageValue("MinMarginPct")

    if (minMarginPerc == null) {
        api.redAlert("MinMarginPct")
        return null;
    }

    api.global.minSurcharge = 1 / (1 - minMarginPerc) - 1
}

return api.global.minSurcharge