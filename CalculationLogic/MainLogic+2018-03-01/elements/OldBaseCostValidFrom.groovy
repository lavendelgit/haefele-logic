def oldBaseCostRecord = api.getElement("OldBaseCostRecord");

def oldBaseCostValidFrom = null

if (oldBaseCostRecord) {
    oldBaseCostValidFrom = oldBaseCostRecord.attribute1
}

if (!oldBaseCostValidFrom) {
    api.redAlert("Valid From is missing in PX BaseCost")
}

return oldBaseCostValidFrom