def maxDiscountPerc = api.getElement("MaxDiscountPerc")
def minMarginPrice = api.getElement("MinMarginPrice")

if (minMarginPrice == null) {
    api.redAlert("MinMarginPrice")
    return null
}

if (maxDiscountPerc == null || maxDiscountPerc == 1) {
    api.redAlert("MaxDiscountPerc")
    return null
}

return minMarginPrice / (1 - maxDiscountPerc)