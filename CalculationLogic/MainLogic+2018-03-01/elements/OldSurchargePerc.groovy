def oldSalesPrice = api.getElement("OldSalesPrice")
def oldBaseCost = api.getElement("OldBaseCost")

if (oldSalesPrice == null) {
    api.redAlert("OldSalesPrice")
    return null
}

if (!oldBaseCost) {
    api.redAlert("OldBaseCost")
    return null
}

return oldSalesPrice / oldBaseCost - 1
