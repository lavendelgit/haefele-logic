def latestCompetitorPrices = api.getElement("LatestCompetitorPrices")

if (latestCompetitorPrices.size() > 0) {
    return latestCompetitorPrices.max { p -> p.price }.price
} else {
    api.redAlert("No Competition data");
    return null;
}