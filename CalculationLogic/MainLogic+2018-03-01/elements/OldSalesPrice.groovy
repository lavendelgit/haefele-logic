def oldSalesPrice = api.getElement("OldSalesPriceRecord")?.salesPrice

if (oldSalesPrice == null) {
    api.redAlert("Old Sales Price is missing.")
}

return oldSalesPrice