def oldMargin = api.getElement("OldMarginAbs")
def newBaseCost = api.getElement("NewBaseCost")

if (oldMargin == null) {
    api.redAlert("OldMarginAbs")
    return null
}

if (newBaseCost == null) {
    api.redAlert("NewBaseCost")
    return null
}

def oldBaseCostValidFrom = api.getElement("OldBaseCostValidFrom")
def newBaseCostValidFrom = api.getElement("NewBaseCostValidFrom")

if (oldBaseCostValidFrom == newBaseCostValidFrom) {
    api.redAlert("OldBaseCostValidFrom is the same as NewBaseCostValidFrom")
    return null;
}

return lib.Rounding.roundMoney(newBaseCost + oldMargin)