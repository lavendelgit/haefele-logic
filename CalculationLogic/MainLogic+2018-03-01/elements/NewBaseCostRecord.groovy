def sku = api.product("sku")
def futureBaseCostRecord = lib.Find.futureBaseCostRecord(sku, api.targetDate());

if (futureBaseCostRecord) {
    return futureBaseCostRecord
}

def currentBaseCostRecord = api.getElement("CurrentBaseCostRecord");

if (!currentBaseCostRecord) {
    return [:]
}

return [
    baseCost: currentBaseCostRecord.attribute3, // Base Cost (de: Einstandspreis)
    validFrom: currentBaseCostRecord.attribute1  // Valid From (de: Gültig von)
]