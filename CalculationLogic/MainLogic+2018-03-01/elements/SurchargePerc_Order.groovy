def salesPriceOrder = api.getElement("SalesPrice_Order")
def baseCost = api.getElement("CurrentBaseCost")

if (!salesPriceOrder) {
    api.redAlert("SalesPrice_Order")
    return null
}

if (!baseCost) {
    api.redAlert("BaseCost")
    return null
}

return salesPriceOrder / baseCost - 1