def sku = api.product("sku")
def filters = [
        Filter.equal("name", "S_ZPAP"),
        Filter.equal("sku", sku)
]
def orderBy = "attribute1" // Valid From (de: Gültig von) - ascending
def fields = [
        "attribute3": "AVG"
]


def avgSalesPrice = api.find("PX", 0, 1, orderBy, fields, false, *filters)

if (avgSalesPrice[0] && avgSalesPrice[0].attribute3) {
    return avgSalesPrice[0].attribute3
} else {
    api.redAlert("No rows in CustomerSalesPrice");
    return null;
}