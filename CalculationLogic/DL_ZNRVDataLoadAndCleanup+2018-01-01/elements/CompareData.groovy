// api.global.currentZNRVEntries
def alreadyExist = false
def dateMap = api.global.currentZNRVEntries[out.sku]
api.trace ("Output of 1..", " Values "+ dateMap)
if (dateMap) {
    def dateSpecificEntry = dateMap[out.FromDate]
    alreadyExist = (dateSpecificEntry)?true:false
    api.trace ("Output of 2..", " Values ${alreadyExist} ======== ${out.FromDate} " )
}
return alreadyExist