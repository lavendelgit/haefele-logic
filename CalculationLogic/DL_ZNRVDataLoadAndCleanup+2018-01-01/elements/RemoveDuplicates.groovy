def duplicates = api.global.duplicatePxRecords
libs.__LIBRARY__.HaefeleSpecific.deleteDuplicateZNRVEntry(duplicates)
libs.__LIBRARY__.TraceUtility.developmentTrace("Testing..", "Total number of duplicates records" + duplicates.size())