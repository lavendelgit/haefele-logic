if (!out.CompareData) {
    def recordToInsert = [
            "sku"       : "${out.sku}",
            "attribute1": "${out.FromDate}",
            "attribute2": "${out.ToDate}",
            "attribute3": "${out.Currency}",
            "attribute4": "${out.Price}",
            "attribute5": "${out.Source}",
            "attribute6": "${out.Package}"
    ]
    String pxTableName = 'SurchargeCostPrice'
    String pxType = 'PX10'
    libs.__LIBRARY__.DBQueries.insertIntoPXTable(recordToInsert, pxTableName, pxType)
	return true
}
return false