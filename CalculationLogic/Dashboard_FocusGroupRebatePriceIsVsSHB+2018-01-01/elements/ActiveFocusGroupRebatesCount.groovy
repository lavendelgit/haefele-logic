def today = api.targetDate()?.format("yyyy-MM-dd")
def filters = [
     Filter.lessOrEqual("key4", today),
     Filter.greaterOrEqual("attribute1", today)
]
def userSelectedSalesOffice = api.getElement("UserSelectedSalesOffice")
if (userSelectedSalesOffice && userSelectedSalesOffice != "All") {
  	filters.add(Filter.equal("key1", userSelectedSalesOffice))
}
def filter = [
		Filter.lessOrEqual("attribute1", today),
		Filter.greaterOrEqual("attribute2", today)
]
def activeCount = api.find("PX50",Filter.equal("name","S_ZRPG"), *filter)
return activeCount.size()