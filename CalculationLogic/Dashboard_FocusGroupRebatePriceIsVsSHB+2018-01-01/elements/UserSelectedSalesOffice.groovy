def salesOffices = api.getElement("SalesOffices")
def salesOfficeKeyValue = api.findLookupTableValues("SalesOffices").collectEntries{[(it.name):it.value]}

return api.option("SalesOffice", ["All"] + salesOffices, salesOfficeKeyValue) ?: "All"