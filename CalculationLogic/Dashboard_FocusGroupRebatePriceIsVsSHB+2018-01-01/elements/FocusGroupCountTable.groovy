def monthWiseData = api.getElement("DataIsVsSHB")
api.trace ("The object properly show", monthWiseData)

final firstCol = "Month"
final isCol = "Used Focus Group Rebate #"
final shbCol = "Should Have Used Focus Group Rebate #"
final perIsVsShb = "Usage Percentage"
def resultMatrix = api.newMatrix([firstCol, isCol, shbCol, perIsVsShb])
resultMatrix.setColumnFormat(perIsVsShb, FieldFormatType.PERCENT)
def row
def countIs
def countSHB
def usagePercentage
int reverseOrder = monthWiseData?.size()-1
(reverseOrder..0).each { index -> 
  countIs = monthWiseData[index]?.isCount?:0
  countSHB = monthWiseData[index]?.shbCount?:0
  usagePercentage = (countSHB > 0) ? (countIs / countSHB) : 0 
  row  = [
            monthWiseData[index]?.month,
      		countIs,
      		countSHB,
    		usagePercentage
    	]
    resultMatrix.addRow(row)
}
return resultMatrix