if (api.isSyntaxCheck()) return;

def controller = api.newController()
def activeFocusGroupRebatesCount = api.getElement("ActiveFocusGroupRebatesCount")
def isVsShbData = api.getElement("DataIsVsSHB");
def isVsShbLastMonth = (isVsShbData && isVsShbData.size() > 0)? isVsShbData.last(): null
def numberOfTransactionMsg = (isVsShbLastMonth)? "Number of transactions in "+isVsShbLastMonth.month : "No transactions available"
def ShbLastMonthCount = (isVsShbLastMonth)? isVsShbLastMonth.shbCount: "0"
def isVsShbLastMonthCount = (isVsShbLastMonth)? isVsShbLastMonth.isCount: "0"

controller.addHTML(
"""
<div style='font-size: 16px; font-weight: bold; margin: 8px 0;'>
        Active Focus Group Rebates: ${activeFocusGroupRebatesCount}
</div>
""");

controller.addHTML(
        """
<div style='font-size: 12px;'>
        ${numberOfTransactionMsg}: 
        <ul>
            <li>which <u>should have used</u> Focus Group Rebate: <b>${ShbLastMonthCount}</b></li>
            <li>which <u>did use</u> Focus Group Rebate: <b>${isVsShbLastMonthCount}</b></li>
        </ul>
</div>
""");

return controller