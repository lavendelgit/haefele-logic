def data2 = api.getElement("Data")

def result = [:]
def focusGroupRebateTypes = api.getElement("FocusGroupRebateTypes")

data2?.forEach { salesType, salesTypeData ->
    salesTypeData.forEach { d ->
        def monthData = result[d.yearMonth]
        if (!monthData) {
            monthData = [
                    month: d.yearMonth,
                    isRevenue: 0,
                    shbRevenue: 0,
                    isCount: 0,
                    shbCount: 0
            ]
            result[d.yearMonth] = monthData
        }

        if (focusGroupRebateTypes.contains(salesType)) {
            monthData.isRevenue += d.revenue
            monthData.isCount += d.count
        }
        monthData.shbRevenue += (d.shbRevenue ?: 0.0)
        monthData.shbCount += d.count
    }
}

result = result?.sort { e1, e2 -> e1.key <=> e2.key }
return result.values()