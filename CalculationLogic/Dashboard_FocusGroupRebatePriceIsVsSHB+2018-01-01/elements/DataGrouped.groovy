def data = api.getElement("Data")
def focusGroupRebateTypes = api.getElement("FocusGroupRebateTypes")

def salesTypes = api.findLookupTableValues("SalesTypes")
   .collectEntries { pp -> [ (pp.name) : [
        description: pp.attribute1,
        insideOutside: pp.attribute2,
        priceIncreaseAffected: pp.attribute3
    ]]}

def salesTypeGroup = salesTypes.collectEntries { salesType, meta ->
    if (focusGroupRebateTypes.contains(salesType)) {
        return [salesType, "Focus Group Rebate"]
    } else {
        [salesType, meta.insideOutside]
    }
}

salesTypeGroup["Unknown"] = "Unknown"

api.trace("salesTypeGroup", "", salesTypeGroup)

groupedData = [:].withDefault { key -> [:]}

data?.forEach { salesType, salesTypeData ->
    def group = salesTypeGroup[salesType] ?: "Undefined!"
    def groupData = groupedData[group];
    salesTypeData.forEach { d ->
        def groupMonthData = groupData[d.yearMonth]
        if (!groupMonthData) {
            groupData[d.yearMonth] = [
                    yearMonth: d.yearMonth,
                    count: d.count,
                    revenue: d.revenue,
                    salesType: group
            ]
        } else {
            groupMonthData.count += d.count
            groupMonthData.revenue += d.revenue
        }
    }
}

api.trace("The Grouped Data"," " + groupedData.size(), groupedData)

return groupedData
        .collectEntries { key, value ->
            return [key, value.values()]
        }