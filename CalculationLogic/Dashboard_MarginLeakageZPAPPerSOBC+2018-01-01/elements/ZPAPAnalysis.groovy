import net.pricefx.common.api.FieldFormatType

def yearList = lib.GeneralUtility.getYearsListFromNow(5)
def columns = ['Customer Name', 'Verkaufsbuero', 'Branche Category', 'Umsatz ' + yearList[4], 'Umsatz ' + yearList[3], 'Umsatz ' + yearList[2],
               'Umsatz ' + yearList[1], 'Umsatz ' + yearList[0], 'CAGR', 'Customer Contribution to Hafelle Umsatz(%)',
               'Current Potential Group', 'Actual Potential Group']
def columnTypes = [FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.TEXT, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR,
                   FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.MONEY_EUR, FieldFormatType.PERCENT, FieldFormatType.PERCENT,
                   FieldFormatType.TEXT, FieldFormatType.TEXT]
def dashUtl = lib.DashboardUtility
def resultMatrix = dashUtl.createNewResultMatrix('All Customers Recommended For Correction Of Potential Categorization',
        columns, columnTypes)
/*zpapEntriesToDisplay?.each {
    resultMatrix.addRow([
            (columns[0]) : dashUtl.getBoldHighLightedValue(resultMatrix, (it['CustomerName'] + '-' + it['CustomerId'])),
            (columns[1]) : it['Verkaufsbereich3'],
            (columns[2]) : it['BrancheCategory'],
            (columns[3]) : getNotNullRevenue(it['Year5Revenue'], it['SYear5Revenue']),
            (columns[4]) : getNotNullRevenue(it['Year4Revenue'], it['SYear4Revenue']),
            (columns[5]) : getNotNullRevenue(it['Year3Revenue'], it['SYear3Revenue']),
            (columns[6]) : getNotNullRevenue(it['Year2Revenue'], it['SYear2Revenue']),
            (columns[7]) : getNotNullRevenue(it['Year1Revenue'], it['SYear1Revenue']),
            (columns[8]) : dashUtl.getBoldHighLightedValue(resultMatrix, it['CAGR']),
            (columns[9]) : it['Year2RevContrib'],
            (columns[10]): dashUtl.getBoldHighLightedValue(resultMatrix, it['CustomerPotentialGroup']),
            (columns[11]): dashUtl.getBoldHighLightedValue(resultMatrix, it['ComputedPotentialGroup'])
    ])
}

 */

return resultMatrix
