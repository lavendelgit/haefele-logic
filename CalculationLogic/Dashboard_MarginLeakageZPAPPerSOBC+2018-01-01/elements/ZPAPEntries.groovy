def validityDate = api.input("Effective Date")
def verkaufsbuero = api.input("Verkaufsbuero")
def brancheCategory = api.input("Branche Category")
def prodII = api.input("Prodh. II")
def prodIII = api.input("Prodh. III")
def prodIV = api.input("Prodh. IV")
def fields = ['SourceID'          : 'Source',
              'Material'          : 'Material',
              'Materialkurztext'  : 'MaterialName',
              'customerId'        : 'CustomerID',
              'name'              : 'CustomerName',
              'ValidFrom'         : 'ValidFrom',
              'ValidTo'           : 'ValidTo',
              'SalesPrice'        : 'SalesPrice',
              'MaxDiscountedPrice': 'MaxDiscountedPrice',
              'CustomerSalesPrice': 'CustomerSalesPrice',
              'DiscountABS'       : 'DiscountABS',
              'DiscountPercentage': 'DiscountPercentage',
              'LossPerZPAP'       : 'LossPerZPAP',
              'LossPerPerZPAP'    : 'LossPerPerZPAP']
def filters = []
validityDate = (validityDate) ? ((validityDate instanceof Date) ? validityDate?.format('yyyy-MM-dd') : validityDate) : api.targetDate()?.format('yyyy-MM-dd')
//filters.add(Filter.and(Filter.greaterOrEqual('ValidFrom', validityDate), Filter.lessOrEqual('ValidTo', validityDate)))
libs.__LIBRARY__.TraceUtility.developmentTrace ('Value to Print', validityDate)

if (verkaufsbuero && !(verkaufsbuero.equals("All")))
    filters.add(Filter.equal('Verkaufsbereich3', verkaufsbuero))
if (brancheCategory && !(brancheCategory.equals("All")))
    filters.add(Filter.equal('BrancheCategory', brancheCategory))
if (verkaufsbuero && !brancheCategory)
    filters.add(Filter.isNull('BrancheCategory'))
if (prodII && !(prodII.equals("All")))
    filters.add(Filter.equal('ProdhII', prodII))
if (prodIII && !(prodIII.equals("All")))
    filters.add(Filter.equal('ProdhIII', prodIII))
if (prodIV && !(prodIV.equals("All")))
    filters.add(Filter.equal('prodIV', prodIV))

def orderBy = ['LossPerZPAP DESC', 'CustomerSalesPrice DESC']
Integer maxRows = 20000 as Integer
def zpapEntriesToDisplay = libs.__LIBRARY__.DBQueries.getFirstNDataRows(fields, 'CustomerSalesPricesDM', filters, maxRows, 'DataMart', orderBy)
return zpapEntriesToDisplay









