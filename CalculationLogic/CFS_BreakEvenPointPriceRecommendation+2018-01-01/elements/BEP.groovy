package CFS_BreakEvenPointPriceRecommendation.elements

if (api.isSyntaxCheck()) return [:]

def sku = api.getElement("CurrentItem")?.sku
def newSalesPrice = api.getElement("NewSalesPrice")

return lib.BEP.calculate(sku, newSalesPrice)