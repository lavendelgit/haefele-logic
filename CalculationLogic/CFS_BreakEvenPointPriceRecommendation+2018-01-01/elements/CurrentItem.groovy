package CFS_BreakEvenPointPriceRecommendation.elements

def currentItem = api.currentItem()

if (currentItem == null) {
    currentItem = (api.productExtension("BreakEvenPointPriceRecommendation") ?: [[:]]).first()
}

api.logInfo("[BreakEvenPointPriceRecommendation][CurrentItem] currentItem", currentItem)

return currentItem