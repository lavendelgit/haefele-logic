package CFS_BreakEvenPointPriceRecommendation.elements

def newSalesPrice = api.getElement("CurrentItem")?.attribute1
def baseCost = api.getElement("BaseCost")
def surcharge = api.getElement("CurrentItem")?.attribute2
def calculatedNewSalesPrice = null

api.trace("newSalesPrice", "", newSalesPrice)
api.trace("baseCost", "", baseCost)
api.trace("surcharge", "", surcharge)

if (newSalesPrice == null && surcharge == null) {
    api.local.warning = "You have to provide New Sales Price or New Surcharge. "
    return
}

if (baseCost == null && surcharge != null) {
    api.local.warning = "Missing Base Cost. Unable to calculate New Sales Price based on Surcharge. "
    return null
}

if (surcharge != null) {
    calculatedNewSalesPrice = baseCost * (1 + surcharge)
}

if (newSalesPrice != null && surcharge != null && newSalesPrice != calculatedNewSalesPrice) {
    api.local.warning = "Detected conflict between New Sales Price and New Surcharge. Specify only one of the two. "
    return
}

api.trace("calculatedNewSalesPrice", "", calculatedNewSalesPrice)

return newSalesPrice ?: calculatedNewSalesPrice

