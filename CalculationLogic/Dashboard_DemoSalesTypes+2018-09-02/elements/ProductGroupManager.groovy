def pgms = api.find("P", 0, 0, "attribute5", ["attribute5"], true, Filter.isNotNull("attribute5")).collect { p -> p.attribute5 }

api.trace("pgms", "", pgms)

api.option("PGM", pgms)