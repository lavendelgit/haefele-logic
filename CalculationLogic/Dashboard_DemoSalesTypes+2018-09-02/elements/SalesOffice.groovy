def salesOffices = [
        "Berlin",
        "Frankfurt",
        "Handel",
        "Hannover",
        "Kaltenkirchen",
        "Köln",
        "München",
        "Münster",
        "Naumburg",
        "Nürnberg",
        "Stuttgart Airport"
]

api.option("Sales Office", ["All"] + salesOffices) ?: "All"