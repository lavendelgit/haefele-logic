def salesType = api.getElement("SalesType")
def insideOutside = api.getElement("InsideOutside")

if (salesType) {
    return insideOutside[salesType] ? "Inside" : "Outside"
} else {
    return "Unknown"
}
