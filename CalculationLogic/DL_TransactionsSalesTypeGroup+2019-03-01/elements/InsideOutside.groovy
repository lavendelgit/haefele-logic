api.retainGlobal = true

if (!api.global.insideOutside) {
    api.global.insideOutside = api.findLookupTableValues("SalesTypes")
                                  .collectEntries { pp -> [pp.name, pp.attribute2 == "Inside"]}
}

return api.global.insideOutside