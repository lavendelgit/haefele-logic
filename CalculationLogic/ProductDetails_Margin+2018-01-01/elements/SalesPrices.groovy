def sku = api.product("sku")

def filters = [
        Filter.equal("name", "S_ZPLP"),
        Filter.equal("sku", sku),
]

def orderBy = "-attribute1" // Valid From (de: Gültig von) - descending

def result = (api.find("PX50", 0, orderBy, *filters) ?: [])
    .collect { r -> [
            validFrom: api.parseDate("yyyy-MM-dd", r.attribute1),
            value: r.attribute3,
            currency: r.attribute7
        ]
    }

return result
