import net.pricefx.common.api.FieldFormatType

def supplierId = api.product(lib.Attrs.product("Supplier Id"))
def supplierName = api.product(lib.Attrs.product("Supplier Name"))

def resultMatrix = api.newMatrix(
        "Einstandpreis",
        "E. Währung",
        "Verkaufspreis",
        "V. Währung",
        "Einkaufs&#47;Verkaufspries",
        "gültig von",
        "Aufschlag (%)",
        "DB (%)",
        "Lieferant",
        "Lieferantsname"
)
resultMatrix.setPreferenceName("SurchargeThroughoutTime")
resultMatrix.setTitle("Surcharge throughout time")

resultMatrix.setColumnFormat("Einstandpreis", FieldFormatType.MONEY)
resultMatrix.setColumnFormat("Verkaufspreis", FieldFormatType.MONEY)
resultMatrix.setColumnFormat("gültig von", FieldFormatType.DATE)
resultMatrix.setColumnFormat("Aufschlag (%)", FieldFormatType.PERCENT)
resultMatrix.setColumnFormat("DB (%)", FieldFormatType.PERCENT)

def baseCosts = api.getElement("BaseCosts")
def salesPrices = api.getElement("SalesPrices")

Set dates = (baseCosts.collect{r -> r.validFrom} + salesPrices.collect{r -> r.validFrom}).sort()

def earliestBaseCost = baseCosts.min { r -> r.validFrom }
def earliestSalesPrice = salesPrices.min { r -> r.validFrom }

def datesReversed = dates.toList().reverse()

for (date in datesReversed) {
    def baseCost = baseCosts.find { r -> !date.before(r.validFrom) }
    def salesPrice = salesPrices.find { r -> !date.before(r.validFrom) }

    def baseCostOrEarliest = baseCost ?: earliestBaseCost
    def salesPriceOrEarliest = salesPrice ?: earliestSalesPrice
//api.trace("base",baseCost)
   def type = baseCost== date ? "E" : "V"

    def marginAbs = (salesPriceOrEarliest.value - baseCostOrEarliest.value)
    def markup = marginAbs / baseCostOrEarliest.value
    def margin = marginAbs / salesPriceOrEarliest.value

    resultMatrix.addRow([
            "Einstandpreis": baseCost?.value,
            "E. Währung": baseCost?.currency,
            "Verkaufspreis": salesPrice?.value,
            "V. Währung": salesPrice?.currency,
            "Einkaufs&#47;Verkaufspries": type,
            "gültig von": date,
            "Aufschlag (%)": markup,
            "DB (%)": margin,
            "Lieferant": supplierId,
            "Lieferantsname":  supplierName
    ])
}
return resultMatrix
