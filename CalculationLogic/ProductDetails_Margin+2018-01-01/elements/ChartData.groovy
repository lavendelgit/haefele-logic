def baseCosts = api.getElement("BaseCosts")
def salesPrices = api.getElement("SalesPrices")

Set dates = (baseCosts.collect{r -> r.validFrom} + salesPrices.collect{r -> r.validFrom} + [api.targetDate()]).sort()

api.trace("dates", "", dates)

def extendedBaseCosts = []
def extendedSalesPrices = []
def range = []

def earliestBaseCost = baseCosts.min { r -> r.validFrom }
def earliestSalesPrice = salesPrices.min { r -> r.validFrom }

for (date in dates) {
    def baseCost = baseCosts.find { r -> !date.before(r.validFrom) } ?: earliestBaseCost
    def salesPrice = salesPrices.find { r -> !date.before(r.validFrom) } ?: earliestSalesPrice
    extendedBaseCosts.push([date, baseCost?.value])
    extendedSalesPrices.push([date, salesPrice?.value])
    range.push([date, baseCost?.value, salesPrice?.value])
}

return [
    baseCosts: extendedBaseCosts,
    salesPrices: extendedSalesPrices,
    range: range
]