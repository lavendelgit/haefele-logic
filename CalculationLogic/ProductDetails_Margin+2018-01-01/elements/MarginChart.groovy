def chartData = api.getElement("ChartData")

def chartDef = [
        chart: [
                type: "line",
                zoomType: "xy"
        ],
        title: [
                text: "Contribution Margin"
        ],
        xAxis: [[
                        type: 'datetime',
                        title: [
                                text: "Date"
                        ]
                ]],
        yAxis: [[
                        title: [
                                text: "Price"
                        ]
                ]],
        tooltip: [
                headerFormat: "",
                //pointFormat: "Units Sold: {point.y:.2f}<br>Price: {point.x:.2f}<br>Month: {point.name}",
                crosshairs: [[width: 1]],
                shared: true
        ],
        plotOptions: [
                line: [
                    step: true
                ]
        ],
        series: [[
                     name: "Margin",
                     type: "arearange",
                     data: chartData.range,
                     "showInLegend": false,
                 ], [
                      "name" : "ZPE",
                      "data" : chartData.baseCosts,
                      "color": "#058DC7"
                 ], [
                    "name" : "ZPLP",
                    "data" : chartData.salesPrices,
                 ],]
]

api.buildFlexChart("ProductMarginTemplate", chartDef)