api.global.printTrace = true
callFlowerBracketExample()

def callFlowerBracketExample() {
    def field = ['InvoiceDateYear': 'Year',
                 'SUM(p3_Revenue)': 'YearlyRevenue']
    def filters = []
    def data = lib.DBQueries.getDataFromSource(field, 'DL_CustomersWithTransactions', filters, 'Rollup')
    lib.TraceUtility.developmentTraceRow("query", data.find{it.Year=='2019'})
}
/*
def shFields = ['CustomerId'                     : 'CustomerId',
                'YearMonthYear'                  : 'Year',
                'SUM(p1_Revenue)'                : 'YearlyRevenue',
                'SUM(p2_MarginalContributionAbs)': 'YearlyMargin'
]
def shFilters = [
        Filter.equal("CustomerId", out.CurrentCustomerToProcess)
]
//Construct the details for Query from TrasactionHistory
def thFields = ['CustomerId'       : 'CustomerId',
                'InvoiceDateYear'  : 'Year',
                'SUM(p3_Revenue)'  : 'YearlyRevenue',
                'SUM(p4_MarginAbs)': 'YearlyMargin',
                'SUM(p6_Material)' : 'TransactionCounts'
]
def thFilters = [
        Filter.equal("CustomerId", "0001000003")
]
def joinSQL = getInnerJoinForSummaryNDetailTransRec()
def joinSources = [
        ['Source': 'CustomerRevenueUsingSalesHistory', 'SourceType': 'Rollup', 'Fields': shFields, 'Filters': shFilters, 'OrderBy': 'Year'],
        ['Source': 'DL_CustomersWithTransactions', 'SourceType': 'Rollup', 'Fields': thFields, 'Filters': thFilters, 'OrderBy': 'Year'],
]
def output = lib.DBQueries.executeJoinQueryToFetchResult(joinSources, joinSQL)
lib.TraceUtility.developmentTrace("query", output)
def resultOfFind = output?.find {it.t1y == '2018'}
lib.TraceUtility.developmentTrace("result of find", resultOfFind)

def getInnerJoinForSummaryNDetailTransRec() {
    StringBuilder builder = new StringBuilder('SELECT ')
    builder.append('T1.CustomerId as t1cid, T1.Year as t1y, T1.YearlyRevenue as t1yr, T1.YearlyMargin as t1ym, ')
    builder.append('IsNull(T2.CustomerId, T1.CustomerId) as t2cid, IsNull(T2.Year, T1.Year) as t2y, IsNull(T2.YearlyRevenue, 0.0) as t2yr, ')
    builder.append('IsNull(T2.YearlyMargin, 0.0) as t2ym, IsNull(T2.TransactionCounts,0) as t2tc ')
    builder.append('FROM T1 LEFT OUTER JOIN T2 ON T1.CustomerId = T2.CustomerId AND T1.Year = T2.Year ')
    builder.append('UNION ')
    builder.append('SELECT ')
    builder.append('IsNull(T1.CustomerId, T2.CustomerId) as t1cid, IsNull(T1.Year, T2.Year) as t1y, IsNull(T1.YearlyRevenue, 0.0) as t1yr, ' +
            'IsNull(T1.YearlyMargin, 0.0) as t1ym, ')
    builder.append('T2.CustomerId as t2cid, T2.Year as t2y, T2.YearlyRevenue as t2yr, T2.YearlyMargin as t2ym, T2.TransactionCounts as t2tc ')
    builder.append('FROM T1 RIGHT OUTER JOIN T2 ON T1.CustomerId = T2.CustomerId AND T1.Year = T2.Year')
    return builder.toString()
}
 */