def invoiceYear = out.InvoiceDate

if (invoiceYear) {
	invoiceYear = invoiceYear.format("yyyy")
}

return invoiceYear