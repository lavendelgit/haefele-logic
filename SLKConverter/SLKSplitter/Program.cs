﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Office.Interop;

namespace SLKSplitter
{
    class Program
    {
        static void Main(string[] args)
        {
            string inFile = ConfigurationManager.AppSettings["InputFile"];
            string outFile = "";
            string oldOutFile = "";
            string outFileFixed = ConfigurationManager.AppSettings["OutputFile"];
            int fileNum = 0;
            int rowNum;
            int newMaxRow = Convert.ToInt32(ConfigurationManager.AppSettings["MaxRow"]);
            int maxSupportNumber = Convert.ToInt32(ConfigurationManager.AppSettings["MaxSupportedRowNumber"]);
            string rowNumber = "";
            bool ConvertToCSV = Convert.ToBoolean(ConfigurationManager.AppSettings["ConvertToCSV"]);
            bool AddHeaderToAllFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["AddHeaderToAllFiles"]);

            int rowNumIncrementVal = 0;

            List<string> fileNames = new List<string>();

            StringBuilder sb = new StringBuilder();
            sb.Append("ID");
            sb.Append(Environment.NewLine);

            StringBuilder headerSb = new StringBuilder();

            Console.WriteLine("Processing started at {0}", DateTime.Now);

            using (StreamReader sr = new StreamReader(inFile))
            {
                string line = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    var splitLine = line.Split(';');

                    if (splitLine.Length > 1)
                    {
                        rowNumber = splitLine[1].Replace("Y", "");
                        int.TryParse(rowNumber, out rowNum);

                        if (AddHeaderToAllFiles)
                            rowNumIncrementVal = 1;
                        else
                            rowNumIncrementVal = 0;

                        if (AddHeaderToAllFiles && rowNum == 1)
                        {
                            headerSb.Append(line);
                            headerSb.Append(Environment.NewLine);
                        }


                        outFile = outFileFixed.Replace(".SLK", string.Format("{0}.SLK", (fileNum + 1)));

                        if (rowNum <= newMaxRow * (fileNum + 1))
                        {
                            if (rowNum > maxSupportNumber)
                            {
                                line = line.Replace("Y" + rowNum, "Y" + (Convert.ToInt32(rowNumber) - (maxSupportNumber * fileNum) + rowNumIncrementVal));
                            }

                            sb.Append(line);
                            sb.Append(Environment.NewLine);
                        }
                        else if (rowNum > newMaxRow * (fileNum + 1))
                        {
                            fileNames.Add(outFile);
                            CreateFileAndWrite(outFile, oldOutFile, sb);
                            sb.Clear();
                            sb.Append("ID");
                            sb.Append(Environment.NewLine);

                            if (AddHeaderToAllFiles)
                                sb.Append(headerSb.ToString());

                            oldOutFile = outFile;
                            fileNum++;

                            if (rowNum > maxSupportNumber)
                            {
                                line = line.Replace("Y" + rowNum, "Y" + (Convert.ToInt32(rowNumber) - (maxSupportNumber * fileNum) + rowNumIncrementVal));
                            }
                            sb.Append(line);
                            sb.Append(Environment.NewLine);
                        }
                    }
                }

                fileNames.Add(outFile);
                CreateFileAndWrite(outFile, oldOutFile, sb);
                sb.Clear();
            }

            if (ConvertToCSV)
            {
                Console.WriteLine("Converting SLK to CSV is enabled. Converting all split files to CSV now.");
                foreach (var fileName in fileNames)
                {
                    Console.WriteLine("Converting {0} to a CSV file of same name.", fileName);
                    ConvertSLKToCSV(fileName);
                }
                Console.WriteLine("Conversion to CSV completed.");
            }
            else
            {
                Console.WriteLine("Converting SLK to CSV is disabled. Please change configuration in the app settings to enable this feature.");
            }

            Console.WriteLine("Processing completed at {0}", DateTime.Now);
            Console.ReadLine();
        }

        private static void WriteToExistingFile(string outFile, string line)
        {
            using (var tw = new StreamWriter(outFile, true))
            {
                tw.WriteLine(line);
            }
        }

        private static void CreateFileAndWrite(string outFile, string oldFile, StringBuilder line)
        {
            Console.WriteLine("Creating file {0}", outFile);
            using (StreamWriter sw = File.CreateText(outFile))
            {
                line.Append("E");
                sw.WriteLine(line.ToString());
            }
        }

        private static void ConvertSLKToCSV(string fileName)
        {
            string csvFileName = fileName.Replace(".SLK", ".CSV");

            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook wbWorkbook = app.Workbooks.Open(fileName);
            Microsoft.Office.Interop.Excel.Sheets wsSheet = wbWorkbook.Worksheets;
            Microsoft.Office.Interop.Excel.Worksheet CurSheet = (Microsoft.Office.Interop.Excel.Worksheet)wsSheet[1];

            /*Below line will produce comma delimited CSV*/
            //wbWorkbook.SaveAs(csvFileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            /*Below line will produce tab delimited CSV*/
            //wbWorkbook.SaveAs(csvFileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlTextWindows, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            /*Below line will produce a CSV file taking delimiter from region setting on control panel*/
            wbWorkbook.SaveAs(csvFileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            wbWorkbook.Close(false, fileName, false);
            app.Workbooks.Close();
            app.Quit();
            Marshal.FinalReleaseComObject(app);
        }
    }
}
